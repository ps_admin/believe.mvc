Imports log4net

Public Class CCreditCard

#Region "Credit Card Functions"

    Private ApplicationPath As String

    Public Sub New()

        ApplicationPath = System.IO.Path.GetDirectoryName(New System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).AbsolutePath)

        ApplicationPath = ApplicationPath.Replace("%20", " ")
        ApplicationPath = ApplicationPath.Replace("\", "\\")  'Replace with a \\ for folder structure

        InitialiseLogging()

        LogMessage("Initialised Logging")
    End Sub

    Public Function ProcessCreditCard(ByVal sCard As String, ByVal sExpiry As String, ByVal sCVC2 As String, ByVal sName As String, ByVal dAmount As Decimal, ByRef sComment As String, ByRef sBankAccountClientID As String, ByRef sBankAccountCertificateName As String, ByRef sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean

        Dim bSuccess As Boolean
        Dim bApproved As Boolean
        Dim bExecute As Boolean
        Dim bStatus As Boolean

        Dim iRetryCount As Integer
        Dim iMaxRetries As Integer = 10

        Dim sResponseCode As String
        Dim cardType As CardType = GetCardType(sCard)
        Dim logger As ILog = Nothing
        LogMessage("Processing Credit Card")
        logger = LogManager.GetLogger("GeneralLogger")

        logger.Info("Processing creditcard")

        Dim wp As webpay.client.Webpay
        Try
            logger.Info("Initializing webpay server")

            ApplicationPath = System.IO.Path.GetDirectoryName(New System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).AbsolutePath)
            ApplicationPath = ApplicationPath.Replace("%20", " ")
            ApplicationPath = ApplicationPath.Replace("\", "\\") 'Replace with a \\ for folder structure

            LogMessage(".pfx path is " + String.Concat(ApplicationPath, "\\", sBankAccountCertificateName))

            wp = New webpay.client.Webpay(sBankAccountClientID,
                                           String.Concat(ApplicationPath, "\\", sBankAccountCertificateName),
                                           sBankAccountCertificatePassPhrase)
        Catch ex As Exception
            logger.Error("Exception Initialising webpay object: " + ex.Message)
            Throw ex
        End Try

        logger.Info("Setting credit card values")

        wp.setPort("3016")   'Bank changed gateway port on 5-May-2017'
        wp.setServers("www.gwipg.stgeorge.com.au")
        wp.setValue("DEBUG", "ON")
        wp.setValue("INTERFACE", "CREDITCARD")
        wp.setValue("TRANSACTIONTYPE", "PURCHASE")
        wp.setValue("TOTALAMOUNT", Strings.Format(dAmount, "#0.00"))
        wp.setValue("CARDDATA", sCard)
        wp.setValue("CARDEXPIRYDATE", sExpiry)
        wp.setValue("DESCRIPTION", sComment)

        If sCVC2 <> "" Then
            If (cardType = CCreditCard.CardType.Visa Or cardType = CCreditCard.CardType.MasterCard) Then
                wp.setValue("CVC2", sCVC2)
            Else
                wp.setValue("CVC2", "")
            End If
        End If

        wp.setValue("CLIENTID", sBankAccountClientID)
        wp.setValue("TERMINALTYPE", "0")
        wp.setValue("CLIENTREF", sName)
        wp.setValue("COMMENT", sComment)

        bExecute = True

        While bExecute Or bStatus

            If iRetryCount >= iMaxRetries Then
                sResponse = "Unable to process your credit card at this present time. Please try again later"
                logger.Error("retry count reached max" + sResponse)
                Return False
            End If

            If bStatus = True Then

                wp.setValue("TRANSACTIONTYPE", "STATUS")
                System.Threading.Thread.Sleep(1500)
                iRetryCount = iRetryCount + 1
            Else
                wp.setValue("TRANSACTIONTYPE", "PURCHASE")
                logger.Info("Transaction Type  set to Purchase")
            End If

            Try
                logger.Info("Attempting to execute payment")
                wp.execute()
                bSuccess = True
                logger.Info("Payment completed")

            Catch connectEx As webpay.client.ConnectException
                'Connect Error - it's safe to retry the transaction
                logger.Error("Connection Exception " + connectEx.Message)
            Catch executeException As Exception
                If (TypeOf executeException Is TimeoutException) Then
                    ' Do timeout specific things here
                    logger.Error("Execute call has timed out.")
                End If
                'Some other exception occurred - the transaction may or may not be processing at the back end
                logger.Error("Exception Executing Transaction: " + executeException.Message)
            End Try

            bExecute = False
            bStatus = False

            sTxRef = wp.getValue("TXNREFERENCE")
            logger.Info("Transaction Reference Number [" + wp.getValue("TXNREFERENCE") + "]" & vbCrLf)

            logger.Info("Description : " + wp.getValue("DESCRIPTION"))

            If Not bSuccess Then
                If sTxRef Is Nothing Then
                    bApproved = False
                    sResponse = "Unable to process credit card at this present time. Please try again later."
                Else
                    bStatus = True
                End If
            Else
                sResponseCode = wp.getValue("RESPONSECODE")
                Select Case sResponseCode
                    Case "IP"             'In Progress. Check status of transaction
                        bStatus = True
                    Case "00", "08", "77" 'Approved
                        bApproved = True
                        sResponse = "Approved"
                    Case "91"             'Bank not available
                        bApproved = False
                        sResponse = "Unable to process credit card. Please try again later."
                    Case "AC", "33", "54" 'Card Expired
                        bApproved = False
                        sResponse = "Credit card has expired."
                    Case "01", "05"       'Cust contact their bank'
                        bApproved = False
                        sResponse = "Unable to process your credit card. Please check your credit card details, or try again later."
                    Case "IN"             'In Progress
                        bApproved = False
                        sResponse = "Unable to process credit card. Please try again later."
                    Case "51"             'Insufficient Funds
                        bApproved = False
                        sResponse = "Unable to process credit card. You have insufficient funds."
                    Case "VA"
                        bApproved = False
                        sResponse = "Credit card number is invalid"
                    Case "A6"
                        bApproved = False
                        sResponse = "Server is too busy to process card. Please try again later."
                    Case "-1"
                        bApproved = False
                        sResponse = "Unable to process credit card. Please try again later."
                    Case Else
                        bApproved = False
                        sResponse = wp.getValue("RESPONSETEXT")
                End Select
                logger.Info("Status Transaction Returned : Response Code [ " + sResponseCode + " ]")
            End If

        End While

        Return bApproved

    End Function

    Public Function ProcessRefund(ByVal sName As String, ByVal sCCTransactionRef As String, ByVal dAmount As Decimal, ByRef sComment As String, ByRef sBankAccountClientID As String, ByRef sBankAccountCertificateName As String, ByRef sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean

        Dim bSuccess As Boolean
        Dim bApproved As Boolean
        Dim bExecute As Boolean
        Dim bStatus As Boolean

        Dim iRetryCount As Integer
        Dim iMaxRetries As Integer = 10

        Dim sResponseCode As String

        Dim wp As New webpay.client.Webpay(sBankAccountClientID,
                                           String.Concat(ApplicationPath, "\", sBankAccountCertificateName),
                                           sBankAccountCertificatePassPhrase)

        wp.setValue("DEBUG", "ON")

        wp.setPort("3016")   'Bank changed gateway port on 5-May-2017
        'wp.setPort("3017")   'Sandbox Testing Port Number
        wp.setServers("www.gwipg.stgeorge.com.au")
        wp.setValue("INTERFACE", "CREDITCARD")
        wp.setValue("TRANSACTIONTYPE", "PURCHASE")
        wp.setValue("TOTALAMOUNT", Strings.Format(dAmount, "#0.00"))
        'wp.setValue("CARDDATA", sCard)
        'wp.setValue("CARDEXPIRYDATE", sExpiry)
        'If sCVC2 <> "" Then wp.setValue("CVC2", sExpiry)
        wp.setValue("CLIENTID", sBankAccountClientID)
        wp.setValue("TERMINALTYPE", "0")
        wp.setValue("CLIENTREF", sName)
        wp.setValue("ORIGINALTXNREF", sCCTransactionRef)
        wp.setValue("COMMENT", sComment)

        bExecute = True

        While bExecute Or bStatus

            If iRetryCount >= iMaxRetries Then
                sResponse = "Unable to process your credit card at this present time. Please try again later."
                Return False
            End If

            If bStatus = True Then
                wp.setValue("TRANSACTIONTYPE", "STATUS")
                System.Threading.Thread.Sleep(1500)
                iRetryCount = iRetryCount + 1
            Else
                wp.setValue("TRANSACTIONTYPE", "REFUND")
            End If

            Try
                wp.execute()
                bSuccess = True
            Catch ex As Exception
                bSuccess = False
            End Try

            bExecute = False
            bStatus = False

            sTxRef = wp.getValue("TXNREFERENCE")

            If Not bSuccess Then
                If sTxRef Is Nothing Then
                    bApproved = False
                    sResponse = "Unable to process credit card at this present time. Please try again later. Response Code: " & wp.getValue("RESPONSECODE")
                Else
                    bStatus = True
                End If
            Else
                sResponseCode = wp.getValue("RESPONSECODE")
                Select Case sResponseCode
                    Case "IP"             'In Progress. Check status of transaction
                        bStatus = True
                    Case "00", "08", "77" 'Approved
                        bApproved = True
                        sResponse = "Approved"
                    Case "91"             'Bank not available
                        bApproved = False
                        sResponse = "Unable to process credit card. Please try again later."
                    Case "AC", "33", "54" 'Card Expired
                        bApproved = False
                        sResponse = "Credit card has expired."
                    Case "01", "05"       'Cust contact their bank'
                        bApproved = False
                        sResponse = "Unable to process credit card at this present time. Please try again later."
                    Case "IN"             'In Progress
                        bApproved = False
                        sResponse = "Unable to process credit card. Please try again later."
                    Case "51"             'Insufficient Funds
                        bApproved = False
                        sResponse = "Unable to process credit card. You have insufficient funds."
                    Case "VA"
                        bApproved = False
                        sResponse = "Credit card number is invalid"
                    Case "A6"
                        bApproved = False
                        sResponse = "Server is too busy to process card. Please try again later."
                    Case "-1"
                        bApproved = False
                        sResponse = "Unable to process credit card. Please try again later."
                    Case Else
                        bApproved = False
                        sResponse = wp.getValue("RESPONSETEXT")
                End Select
            End If

        End While

        Return bApproved

    End Function

    Private Function GetCardType(ByVal CardNo As String) As CardType
        'Remove all spaces and dashes from the passed string
        CardNo = Replace(Replace(CardNo, " ", ""), "-", "")

        'Check that the minimum length of the string isn't less
        'than fourteen characters and -is- numeric
        If Len(CardNo) < 14 Or Not IsNumeric(CardNo) Then Exit Function

        'Check the first two digits first
        Select Case CInt(Left(CardNo, 2))
            Case 34, 37
                Return CardType.Amex
            Case 36
                Return CardType.Diners
            Case 38
                Return CardType.CarteBlanche
            Case 51 To 55
                Return CardType.MasterCard
            Case Else

                'None of the above - so check the
                'first four digits collectively
                Select Case CInt(Left(CardNo, 4))

                    Case 2014, 2149
                        Return CardType.EnRoute
                    Case 2131, 1800
                        Return CardType.JCB
                    Case 6011
                        Return CardType.Discover
                    Case Else

                        'None of the above - so check the
                        'first three digits collectively
                        Select Case CInt(Left(CardNo, 3))
                            Case 300 To 305
                                Return CardType.Diners
                            Case Else

                                'None of the above -
                                'so simply check the first digit
                                Select Case CInt(Left(CardNo, 1))
                                    Case 3
                                        Return CardType.JCB
                                    Case 4
                                        Return CardType.Visa
                                End Select

                        End Select

                End Select

        End Select
    End Function

    Public Sub InitialiseLogging()

        log4net.Config.XmlConfigurator.Configure(New IO.FileInfo(ApplicationPath & "\log4net.config"))

        Dim log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod.DeclaringType)

    End Sub

    Public Sub LogMessage(ByVal sMessage As String)

        Dim log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod.DeclaringType)

        log.Debug(sMessage)

    End Sub

    Private Enum CardType As Integer
        Amex = 1
        Diners = 2
        CarteBlanche = 3
        Discover = 4
        EnRoute = 5
        JCB = 6
        MasterCard = 7
        Visa = 8
    End Enum
#End Region

End Class
