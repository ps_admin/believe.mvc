using System;
using System.Web.Services.Protocols;
using System.IO;
using System.Diagnostics;
using NZlib.Zip;

namespace WebServiceCompression
{

    public class WebServiceCompress : SoapExtension
    {
        #region Privates

        private CompressionLevels _compressionLevel = CompressionLevels.High;  // defaults to high
        private Stream oldStream;
        private MemoryStream newStream;

        #endregion

        #region Constructor
        public WebServiceCompress() { }
        #endregion

        #region Chainstream
        public override Stream ChainStream(Stream stream)
        {
            oldStream = stream;
            newStream = new MemoryStream();
            return newStream;
        }
        #endregion

        #region ProcessMessage
        public override void ProcessMessage(SoapMessage message)
        {
            switch (message.Stage)
            {
                case SoapMessageStage.AfterDeserialize:
                    break;
                case SoapMessageStage.AfterSerialize:
                    Compress();
                    break;
                case SoapMessageStage.BeforeDeserialize:
                    Decompress();
                    break;
                case SoapMessageStage.BeforeSerialize:
                    WebServiceCompressHeader wsh = new WebServiceCompressHeader();
                    wsh.CompressionLevel = _compressionLevel;
                    message.Headers.Add(wsh);
                    break;
            }
        }
        #endregion

        #region Compression and Decompression routines

        private void Compress()
        {
            //Create a Streamwriter object so we can write the output to the network buffer.
            StreamWriter writer = new StreamWriter(oldStream);

            newStream.Position = 0;

            if (_compressionLevel != CompressionLevels.None)
            {
                MemoryStream tmpMem = new MemoryStream();

                ZipOutputStream zip = new ZipOutputStream(tmpMem);
                zip.SetLevel(Convert.ToInt32(_compressionLevel));
                ZipEntry entry = new ZipEntry("SoapBody");
                zip.PutNextEntry(entry);

                // Buffered stream will contain what has been currently written to the stream by the runtime
                // so we write it to our temp stream for manipulation (can't manipulate the runtime stream).


                zip.Write(newStream.GetBuffer(), 0, (int)newStream.Length);
                zip.Finish();

                string compStr = Convert.ToBase64String(tmpMem.GetBuffer(), 0, (int)tmpMem.Length);
                zip.Close();

                //Write decompressed string to buffer
                writer.WriteLine(compStr);
            }
            else
            {
                // No Compression, plain text. Just transmit the original stream.
                StreamReader sr = new StreamReader(newStream);
                writer.WriteLine(sr.ReadToEnd());
            }

            writer.Flush();

        }

        private void Decompress()
        {
            //Create a Streamwriter object so we can write the output to the network buffer.
            StreamWriter writer = new StreamWriter(newStream);

            if (_compressionLevel != CompressionLevels.None)
            {
                // Create a stream reader to give us a string representation of the network stream
                StreamReader sr = new StreamReader(oldStream, System.Text.Encoding.UTF8);
                string netStr = sr.ReadToEnd();

                //Debug.Print(netStr.Length.ToString());
                sr.Close();

                // Get the encoded bytes from the Base64 string.
                byte[] tmpB = Convert.FromBase64String(netStr);

                // Make a tmporary memory stream to work on
                // (failure to do this i.e working directly on the network stream can have adverse effects)
                MemoryStream tmpMem = new MemoryStream(tmpB);

                // Use the Zip compression libraries to decompress the bytes
                ZipInputStream zip = new ZipInputStream(tmpMem);
                ZipEntry entry = zip.GetNextEntry();
                // We need to read in the compress stream chunks at a time. The 'Read' method of the
                // ZipInputStream can only read a max of 23191 bytes (I think thats its max)
                int size = 1;
                byte[] buf = new byte[2048];
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                while (size > 0)
                {
                    size = zip.Read(buf, 0, 2048);
                    if (size > 0)
                        sb.Append(System.Text.Encoding.UTF8.GetString(buf, 0, size));
                }
                zip.Close();  // should implicitly close our memory stream.

                //Write decompressed string to buffer
                writer.WriteLine(sb.ToString());
            }
            else
            {
                // No Compression, plain text. Just transmit the original stream.
                StreamReader sr = new StreamReader(oldStream);
                writer.WriteLine(sr.ReadToEnd());
            }

            writer.Flush();
            newStream.Position = 0;  // If we dont do this, nothing is returned!
        }
        #endregion

        #region Initialise method overrides
        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            CompressionLevels level = ((WebServiceCompressionAttribute)attribute).CompressionLevel;
            return level;
        }

        public override object GetInitializer(Type type)
        {
            return null;
        }

        public override void Initialize(object initializer)
        {
            // Sets our internval values
            _compressionLevel = (CompressionLevels)initializer;
        }

        #endregion

    }
}
