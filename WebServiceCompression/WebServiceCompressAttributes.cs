using System;
using System.Web.Services.Protocols;

namespace WebServiceCompression
{
    #region Compression Type attribute

    /// <summary>
    /// WebServiceCompression Soap extension. This class does the work indicated by the associated WebServiceCompression attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class WebServiceCompressionAttribute : SoapExtensionAttribute
    {
        #region Privates
        private int _priority = 0;
        private CompressionLevels _comrpessionLevel = CompressionLevels.None;
        #endregion

        #region Properties
        #region ExtensionType
        // Return the type of WebServiceCompress.
        public override Type ExtensionType
        {
            get
            {
                return typeof(WebServiceCompress);
            }
        }
        #endregion

        #region Priority
        public override int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }
        #endregion

        #region CompressionLevel
        public CompressionLevels CompressionLevel
        {
            get { return _comrpessionLevel; }
            set { _comrpessionLevel = value; }
        }
        #endregion
        #endregion

        #region Constructors
        public WebServiceCompressionAttribute()
        {
        }

        public WebServiceCompressionAttribute(CompressionLevels level)
        {
            _comrpessionLevel = level;
        }
        #endregion

    }
    #endregion

    #region WebServiceCompress Header
    /// <summary>
    /// Summary description for WebServiceCompressHeader.
    /// </summary>
    public class WebServiceCompressHeader : SoapHeader
    {
        /// <summary>
        /// Header passed through to indicate whether compression is enabled and if so, how much.
        /// </summary>
        public CompressionLevels CompressionLevel = CompressionLevels.None;
    }
    #endregion

}
