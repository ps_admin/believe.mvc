using System;

namespace WebServiceCompression
{
	/// <summary>
	/// Summary description for Gloabs.
	/// </summary>
	public class Globals
	{
		private Globals() {}
	}

	#region CompressionLevel enumeration
	/// <summary>
	/// Specifies the compression level. High provides better compression but is more CPU intensive
	/// </summary>
	public enum CompressionLevels
	{
		None = 0,
		Low = 3,
		Medium = 6,
		High = 9
	}
	#endregion
}
