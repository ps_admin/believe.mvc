﻿using System;

namespace WebServiceCompression
{
    public class WebServiceAuthHeaderValidation
	{
		public static bool Validate(WebServiceAuthHeader soapHeader)
		{

			if (soapHeader == null)
                throw new NullReferenceException("PS Database Auth Error: No soap header was specified.");
            if (soapHeader.UserName == null)
                throw new NullReferenceException("PS Database Auth Error: Username was not supplied for authentication in SoapHeader.");
            if (soapHeader.Password == null)
                throw new NullReferenceException("PS Database Auth Error: Password was not supplied for authentication in SoapHeader.");

			if (soapHeader.UserName != "{2BA6A61F-D3F8-4186-A565-B0106B521EB5}" | soapHeader.Password != "{A7BF2F55-8E50-4d25-B059-F6E33671A8FF}") {
                throw new Exception("PS Database Auth Error: Please provide the proper username and password for this service.");
			}

			return true;

		}

	}

}