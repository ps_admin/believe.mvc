Namespace External

    <CLSCompliant(True)> _
    Public Class CLists

        Private moChurchInvolvement As Hashtable
        Private moEvents As Events.CLists
        Private moRevolution As Revolution.CLists

        Public Sub New()
            moEvents = New Events.CLists
            moRevolution = New Revolution.CLists
        End Sub

        Public ReadOnly Property ChurchInvolvement() As Hashtable
            Get
                If moChurchInvolvement Is Nothing Then
                    Dim oDataRow As DataRow

                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM External_ChurchInvolvement ORDER BY SortOrder")

                    moChurchInvolvement = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oChurchInvolvement As New CChurchInvolvement(oDataRow)
                        moChurchInvolvement.Add(oChurchInvolvement.ID, oChurchInvolvement)
                    Next
                End If
                Return moChurchInvolvement
            End Get
        End Property

        Public Function GetChurchInvolvementByID(ByVal iID As Integer) As CChurchInvolvement
            Return ChurchInvolvement(iID)
        End Function

        Public ReadOnly Property Events() As Events.CLists
            Get
                Return moEvents
            End Get
        End Property
        Public ReadOnly Property Revolution() As Revolution.CLists
            Get
                Return moRevolution
            End Get
        End Property

    End Class

End Namespace