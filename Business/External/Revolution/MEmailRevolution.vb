Imports System.text

Namespace External.Revolution

    Public Module MEmailRevolution

        Public Function RevolutionSubscriptionConfirmation(ByVal oContact As CContact, ByVal oContactSubscription As CContactSubscription) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Dear " & oContact.FirstName & ", <br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Thanks so much for becoming a member of <strong>The Planetshakers Revolution.</strong><br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("We're so excited about partnering with you to reach and impact a generation with the gospel of Jesus Christ. We know you are going to be so blessed by this resource.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Here is your receipt of purchase.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>TAX INVOICE</strong><br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Ministries International Inc.<br />")
            sEmail.AppendLine("ABN: 79 480 989 066<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Revolution Membership Subscription    $" & Format(oContactSubscription.SubscriptionCost, "#0.00") & "*<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("*GST: Price quoted for subscription is GST inclusive.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<a href=""http://revolution.planetshakers.com"">Click here to access your subscription</a>, and sign in using the email address and password you specified when creating your account.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("We're excited to have you on board.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("God Bless, <br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>The Planetshakers Revolution Team</strong><br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Ministries International Inc.<br />")
            sEmail.AppendLine("ABN: 79 480 989 066<br />")
            sEmail.AppendLine("Post: PO Box 5171, South Melbourne Vic 3205<br />")
            sEmail.AppendLine("P: 1300 88 33 21 / +61 3 9896 7999<br />")
            sEmail.AppendLine("F: +61 3 9830 7683<br />")
            sEmail.AppendLine("E: revolution@planetshakers.com <br />")
            sEmail.AppendLine("W: www.planetshakers.com<br />")

            Dim oEmail As New CEmailRevolution
            oEmail.EmailText = sEmail.ToString
            oEmail.Subject = "Planetshakers Revolution Subscription"
            Return oEmail.SendEmail(oContact.Email, oContact.Name, sError)

        End Function

        Public Function RevolutionEndOfTrial(ByVal oContact As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("<span style=""font-family: Verdana, Arial;""><br />")
            sEmail.AppendLine("Hi " & oContact.FirstName & ", <br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("We hope you're enjoying your 1 Month Revolution Membership Trial! Thanks so much for checking out our Revolution resource.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("There is just 1 week until this trial runs out, so to become a Full Planetshakers Revolution Member all you need to do is <a href=""http://accounts.planetshakers.com/signin.aspx?RevolutionRenew=1&ID=" & oContact.GUID & """>click here</a> and you can sign up today.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Next month's content is going to be incredible and you won't want to miss it!<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("For more details or to have any questions answered please visit revolution.planetshakers.com or drop us a line at <a href=""mailto:revolution@planethshakers.com"">revolution@planethshakers.com</a>.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Looking forward to hearing from you soon!<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Blessings,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>The Planetshakers Revolution Team</strong><br />")
            sEmail.AppendLine("Planetshakers Ministries International Inc<br />")
            sEmail.AppendLine("Post: PO Box 5171, South Melbourne Vic 3205<br />")
            sEmail.AppendLine("P: 1300 88 33 21 / +61 3 9896 7999<br />")
            sEmail.AppendLine("F: +61 3 9830 7683<br />")
            sEmail.AppendLine("E: revolution@planetshakers.com<br />")
            sEmail.AppendLine("W: www.planetshakers.com<br />")
            sEmail.AppendLine("</div>")

            Dim oEmail As New CEmailRevolution
            oEmail.EmailText = sEmail.ToString
            oEmail.Subject = "Planetshakers Revolution Trial"
            Return oEmail.SendEmail(oContact.Email, oContact.Name, sError)

        End Function

    End Module

End Namespace