Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CReferredBy

        Private miReferredByID As Integer
        Private msReferredBy As String
        Private mbRequireOtherInformation As Boolean

        Public Sub New()

            miReferredByID = 0
            msReferredBy = String.Empty
            mbRequireOtherInformation = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miReferredByID = oDataRow("ReferredBy_ID")
            msReferredBy = oDataRow("ReferredBy").ToString
            mbRequireOtherInformation = oDataRow("RequireOtherInformation")

        End Sub

        Public Property ID() As Integer
            Get
                Return miReferredByID
            End Get
            Set(ByVal value As Integer)
                miReferredByID = value
            End Set
        End Property

        Public Property ReferredBy() As String
            Get
                Return msReferredBy
            End Get
            Set(ByVal value As String)
                msReferredBy = value
            End Set
        End Property
        Public Property RequireOtherInformation() As Boolean
            Get
                Return mbRequireOtherInformation
            End Get
            Set(ByVal value As Boolean)
                mbRequireOtherInformation = value
            End Set
        End Property

    End Class

End Namespace