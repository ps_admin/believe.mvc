Namespace External.Revolution

    Public Module MRevolution

        Public Function GetContent(ByVal oContact As CContact, ByVal oCategory As CContentCategory, ByVal oSubCategory As CContentSubCategory) As CArrayList(Of CContent)

            Dim oArrayList As New CArrayList(Of CContent)
            Dim oContentPack As CContentPack = GetLatestContentPack()

            Dim oContent As CContent
            Dim oContentContentCategory As CContentContentCategory
            Dim bCategoryIncluded As Boolean
            Dim dCurrentDate As Date = GetDateTime

            'If we are not on the live system, make the current content pack to be the one for the following month
            If CurrentWebDomain.Contains("dev.revolution.planetshakers.com") Or CurrentWebDomain.Contains("localhost") Then
                dCurrentDate = DateAdd(DateInterval.Month, 1, dCurrentDate)
            End If

            For Each oContent In Lists.External.Revolution.Content.Values

                'Check if the content is visible...
                If oContent.Visible Then

                    'Check if the content pack is released yet...
                    If CDate(oContent.ContentPack.StartDate) <= dCurrentDate Then

                        bCategoryIncluded = False

                        If oCategory Is Nothing Then 'If no category is specified, allow it to be added to the list.
                            bCategoryIncluded = True
                        Else
                            For Each oContentContentCategory In oContent.ContentCategories
                                If oContentContentCategory.ContentCategory.ID = ObjectID(oCategory) Then
                                    If oContentContentCategory.ContentSubCategory.ID = ObjectID(oSubCategory) Or oSubCategory Is Nothing Then
                                        bCategoryIncluded = True
                                    End If
                                End If
                            Next
                        End If

                        If bCategoryIncluded Then
                            If oContent.AuthenticationRequired = False Then
                                oArrayList.Add(oContent)
                            ElseIf oContact IsNot Nothing Then
                                If oContact.External.Revolution.ContentAuthorised(oContent) Then
                                    oArrayList.Add(oContent)
                                End If
                            End If
                        End If

                    End If

                End If

            Next

            oArrayList.Sort(New CSorterContent)
            Return oArrayList

        End Function

        Public Function GetLatestContentPackContent() As CArrayList(Of CContent)

            Dim oArrayList As New CArrayList(Of CContent)
            Dim oContentPack As CContentPack = GetLatestContentPack()
            Dim oContent As CContent

            For Each oContent In Lists.External.Revolution.Content.Values
                If oContent.Visible Then
                    If oContent.ContentPack.ID = ObjectID(oContentPack) Then
                        oArrayList.Add(oContent)
                    End If
                End If
            Next

            oArrayList.Sort(New CSorterContent)
            Return oArrayList

        End Function

        Private Function GetLatestContentPack() As CContentPack

            Dim oContentPack As CContentPack
            Dim oSelectedContentPack As CContentPack = Nothing
            Dim dCurrentDate As Date = GetDateTime

            'If we are not on the live system, make the current content pack to be the one for the following month
            If CurrentWebDomain.Contains("dev.revolution.planetshakers.com") Or CurrentWebDomain.Contains("localhost") Then
                dCurrentDate = DateAdd(DateInterval.Month, 1, dCurrentDate)
            End If

            For Each oContentPack In Lists.External.Revolution.ContentPack.Values
                'Bonus Content Packs cannot be the 'New Release' content pack
                If oContentPack.BonusContentPack = False Then
                    If CDate(oContentPack.StartDate) <= dCurrentDate Then
                        If oSelectedContentPack Is Nothing Then
                            oSelectedContentPack = oContentPack
                        Else
                            If CDate(oContentPack.StartDate) > CDate(oSelectedContentPack.StartDate) Then
                                oSelectedContentPack = oContentPack
                            End If
                        End If
                    End If
                End If
            Next

            Return oSelectedContentPack

        End Function

        Public Function IsAuthorisedToDownload(ByVal oContent As CContent, ByVal oContact As CContact) As Boolean
            Dim oCheckContent As CContent
            For Each oCheckContent In GetContent(oContact, Nothing, Nothing)
                If oCheckContent.ID = oContent.ID Then
                    Return True
                End If
            Next
            Return False
        End Function

        Public Function IsOverDownloadLimit(ByVal oContentFile As CContentFile, ByVal oContact As CContact) As Boolean

            If oContact Is Nothing Then Return False

            If oContentFile.Content.RestrictedDownload And oContentFile.ContentFileType.RestrictedDownload Then
                If ContentFileDownloadCount(oContentFile, oContact) >= Options.PremiumDownloadLimit Then
                    Return True
                End If
            End If
            Return False

        End Function

        Public Function ContentFileDownloadCount(ByVal oContentFile As CContentFile, ByVal oContact As CContact) As Integer

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", oContact.ID))
            oParameters.Add(New SqlClient.SqlParameter("@ContentFile_ID", oContentFile.ID))

            Return DataAccess.GetValue("sp_Revolution_ContentDownloadCheck", oParameters)

        End Function

        Public Function ReportBanking(ByVal sStartDate As String, ByVal sEndDate As String) As DataSet

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Data.Common.DataTableMapping("Table", "Payments"))
            oTableMappings.Add(New Data.Common.DataTableMapping("Table1", "Subscriptions"))

            Return DataAccess.GetDataSet("EXEC sp_Revolution_ReportBanking " & _
                                                MSQLUtilities.SQL(sStartDate) & ", " & _
                                                MSQLUtilities.SQL(sEndDate), oTableMappings)

        End Function

        Public Function ReportContentDownload(ByVal sStartDate As String, _
                                              ByVal sEndDate As String, _
                                              ByRef oContentCategory As CContentCategory, _
                                              ByRef oContentFileType As CContentFileType, _
                                              ByVal iShowRestrictedContent As Integer, _
                                              ByVal iShowMediaPlayerDownloads As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", sStartDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", sEndDate))
            oParameters.Add(New SqlClient.SqlParameter("@ContentCategory_ID", SQLObject(oContentCategory)))
            oParameters.Add(New SqlClient.SqlParameter("@ContentFileType_ID", SQLObject(oContentFileType)))
            oParameters.Add(New SqlClient.SqlParameter("@ShowRestrictedContent", iShowRestrictedContent))
            oParameters.Add(New SqlClient.SqlParameter("@ShowMediaPlayerDownloads", iShowMediaPlayerDownloads))

            Return DataAccess.GetDataTable("sp_Revolution_ReportContentDownload", oParameters)

        End Function

        Public Function ReportSubscription(ByVal iReportType As EnumRevolutionSubscriptionReportType, _
                                           ByVal iStartMonth As Integer, _
                                           ByVal iStartYear As Integer, _
                                           ByVal iMonths As Integer, _
                                           ByVal sSubscriptions As String) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ReportType", iReportType))
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@Months", iMonths))
            oParameters.Add(New SqlClient.SqlParameter("@Subscriptions", sSubscriptions))

            Return DataAccess.GetDataTable("sp_Revolution_ReportSubscription", oParameters)

        End Function

        Public Sub LogContentDownload(ByVal oContact As CContact, _
                                        ByVal oContentFile As CContentFile, _
                                        ByVal sClientIP As String, _
                                        ByVal bMediaPlayerDownload As Boolean)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", SQLObject(oContact)))
            oParameters.Add(New SqlClient.SqlParameter("@ContentFile_ID", SQLObject(oContentFile)))
            oParameters.Add(New SqlClient.SqlParameter("@ClientIP", sClientIP))
            oParameters.Add(New SqlClient.SqlParameter("@MediaPlayerDownload", bMediaPlayerDownload))

            DataAccess.ExecuteCommand("sp_Revolution_ContentDownloadInsert", oParameters)

        End Sub

        Public Sub ActivateSubscriptionCardRange(ByVal iSerialNumberStart As Integer, ByVal iSerialNumberEnd As Integer, ByVal oUser As CUser)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@SerialNumberStart", iSerialNumberStart))
            oParameters.Add(New SqlClient.SqlParameter("@SerialNumberEnd", iSerialNumberEnd))
            oParameters.Add(New SqlClient.SqlParameter("@ActivatedBy_ID", SQLObject(oUser)))

            DataAccess.ExecuteCommand("sp_Revolution_ActivationCodeActivate", oParameters)

        End Sub

        Public Sub NightlyRevolutionEndOfTrialEmail()

            Dim oDataTable As DataTable
            Dim oDataRow As DataRow
            Dim oContactSubscription As CContactSubscription

            oDataTable = DataAccess.GetDataTable("sp_Revolution_NightlyEndOfTrialEmail")

            For Each oDataRow In oDataTable.Rows
                Dim oContact As New CContact
                If oContact.LoadByID(oDataRow("Contact_ID")) Then
                    RevolutionEndOfTrial(oContact)
                    For Each oContactSubscription In oContact.External.Revolution.Subscription
                        If oContactSubscription.ID = oDataRow("ContactSubscription_ID") Then
                            oContactSubscription.EndOfTrialEmailSent = True
                            oContact.Save(Lists.GetUserByID(EnumSystemUserNames.Administrator))
                            Exit For
                        End If
                    Next
                End If
            Next

        End Sub

    End Module

End Namespace
