Namespace External

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactExternal

        Private miContactID As Integer

        Private msChurchName As String
        Private miDenominationID As Integer
        Private msSeniorPastorName As String
        Private msChurchAddress1 As String
        Private msChurchAddress2 As String
        Private msChurchSuburb As String
        Private msChurchPostcode As String
        Private miChurchStateID As Integer
        Private msChurchStateOther As String
        Private miChurchCountryID As Integer
        Private msChurchWebsite As String
        Private msLeaderName As String
        Private msChurchNumberOfPeople As String
        Private msChurchPhone As String
        Private msChurchEmail As String

        Private mbContactDetailsVerified As Boolean

        Private mbDeleted As Boolean
        Private mbInactive As Boolean

        Private mcChurchInvolvement As CArrayList(Of CContactChurchInvolvement)

        Private moEvents As External.Events.CContactExternalEvents
        Private moRevolution As External.Revolution.CContactExternalRevolution

        Public mbChanged As Boolean

        Public Sub New()

            msChurchName = String.Empty
            miDenominationID = EnumDenomination.NA
            msSeniorPastorName = String.Empty
            msChurchAddress1 = String.Empty
            msChurchAddress2 = String.Empty
            msChurchSuburb = String.Empty
            msChurchPostcode = String.Empty
            miChurchStateID = 0
            msChurchStateOther = String.Empty
            miChurchCountryID = 0
            msChurchWebsite = String.Empty
            msLeaderName = String.Empty
            msChurchNumberOfPeople = String.Empty
            msChurchPhone = String.Empty
            msChurchEmail = String.Empty

            mbContactDetailsVerified = True 'New Contacts are verified be default
            mbDeleted = False
            mbInactive = False

            mcChurchInvolvement = New CArrayList(Of CContactChurchInvolvement)

            mbChanged = False

        End Sub

        Public Function LoadID(ByVal iContactID As Integer, ByVal oContact As CContact) As Boolean

            Dim oDataSet As DataSet

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "ContactExternal"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "ContactExternalEvents"))
            oTableMappings.Add(New Common.DataTableMapping("Table2", "ContactExternalRevolution"))
            oTableMappings.Add(New Common.DataTableMapping("Table3", "ContactChurchInvolvement"))

            oDataSet = DataAccess.GetDataSet("sp_Common_ContactExternalRead " & iContactID.ToString, oTableMappings)

            oDataSet.Relations.Add("ContactExternalEvents", oDataSet.Tables("ContactExternal").Columns("Contact_ID"), _
                                                  oDataSet.Tables("ContactExternalEvents").Columns("Contact_ID"))

            oDataSet.Relations.Add("ContactExternalRevolution", oDataSet.Tables("ContactExternal").Columns("Contact_ID"), _
                                                  oDataSet.Tables("ContactExternalRevolution").Columns("Contact_ID"))

            oDataSet.Relations.Add("ContactChurchInvolvement", oDataSet.Tables("ContactExternal").Columns("Contact_ID"), _
                                                  oDataSet.Tables("ContactChurchInvolvement").Columns("Contact_ID"))

            miContactID = iContactID

            If oDataSet.Tables("ContactExternal").Rows.Count = 0 Then
                Return False
            Else
                Return LoadDataRow(oDataSet.Tables("ContactExternal").Rows(0))
            End If

        End Function

        Public Function LoadDataRow(ByRef oDataRow As DataRow) As Boolean

            miContactID = oDataRow("Contact_ID")
            msChurchName = oDataRow("ChurchName")
            miDenominationID = oDataRow("Denomination_ID")
            msSeniorPastorName = oDataRow("SeniorPastorName")
            msChurchAddress1 = oDataRow("ChurchAddress1")
            msChurchAddress2 = oDataRow("ChurchAddress2")
            msChurchSuburb = oDataRow("ChurchSuburb")
            msChurchPostcode = oDataRow("ChurchPostcode")
            miChurchStateID = IsNull(oDataRow("ChurchState_ID"), 0)
            msChurchStateOther = oDataRow("ChurchStateOther")
            miChurchCountryID = IsNull(oDataRow("ChurchCountry_ID"), 0)
            msChurchWebsite = oDataRow("ChurchWebsite")
            msLeaderName = oDataRow("LeaderName")
            msChurchNumberOfPeople = oDataRow("ChurchNumberOfPeople")
            msChurchPhone = oDataRow("ChurchPhone")
            msChurchEmail = oDataRow("ChurchEmail")

            mbContactDetailsVerified = oDataRow("ContactDetailsVerified")
            mbDeleted = oDataRow("Deleted")
            mbInactive = oDataRow("Inactive")

            Dim oContactChurchInvolvementRow As DataRow
            For Each oContactChurchInvolvementRow In oDataRow.GetChildRows("ContactChurchInvolvement")
                mcChurchInvolvement.Add(New CContactChurchInvolvement(oContactChurchInvolvementRow))
            Next

            'Check if this dataset contains any other 'External' data
            If oDataRow.Table.DataSet IsNot Nothing Then
                If oDataRow.GetParentRow("ContactExternal") IsNot Nothing Then
                    If oDataRow.GetParentRow("ContactExternal").GetChildRows("ContactExternalEvents").Length > 0 Then
                        moEvents = New External.Events.CContactExternalEvents(oDataRow.GetParentRow("ContactExternal").GetChildRows("ContactExternalEvents")(0))
                    End If
                    If oDataRow.GetParentRow("ContactExternal").GetChildRows("ContactExternalRevolution").Length > 0 Then
                        moRevolution = New External.Revolution.CContactExternalRevolution(oDataRow.GetParentRow("ContactExternal").GetChildRows("ContactExternalRevolution")(0))
                    End If
                End If
            End If

            mbChanged = False

            Return True

        End Function

        Public Sub Save(ByVal oUser As CUser)

            Dim oParameters As New ArrayList

            If mbChanged Or _
                (moEvents IsNot Nothing AndAlso moEvents.Changed) Or _
                (moRevolution IsNot Nothing AndAlso moRevolution.Changed) Then

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchName", ChurchName))
                oParameters.Add(New SqlClient.SqlParameter("@Denomination_ID", SQLObject(Denomination)))
                oParameters.Add(New SqlClient.SqlParameter("@SeniorPastorName", SeniorPastorName))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchAddress1", ChurchAddress1))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchAddress2", ChurchAddress2))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchSuburb", ChurchSuburb))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchPostcode", ChurchPostcode))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchState_ID", SQLNull(miChurchStateID)))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchStateOther", ChurchStateOther))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchCountry_ID", SQLNull(miChurchCountryID)))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchWebsite", ChurchWebsite))
                oParameters.Add(New SqlClient.SqlParameter("@LeaderName", LeaderName))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchNumberOfPeople", ChurchNumberOfPeople))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchPhone", ChurchPhone))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchEmail", ChurchEmail))
                oParameters.Add(New SqlClient.SqlParameter("@ContactDetailsVerified", mbContactDetailsVerified))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", Deleted))
                oParameters.Add(New SqlClient.SqlParameter("@Inactive", Inactive))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", ouser.id))

                DataAccess.ExecuteCommand("sp_Common_ContactExternalUpdate", oParameters)

                mbChanged = False

            End If

            SaveChurchInvolvement(oUser)
            mcChurchInvolvement.ResetChanges()

            If moEvents IsNot Nothing AndAlso moEvents.Changed Then
                moEvents.ContactID = miContactID
                moEvents.Save(oUser)
            End If

            If moRevolution IsNot Nothing AndAlso moRevolution.Changed Then
                moRevolution.ContactID = miContactID
                moRevolution.Save(oUser)
            End If

        End Sub

        Private Sub SaveChurchInvolvement(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oContactChurchInvolvement As CContactChurchInvolvement

            'Remove the Deleted Church Involvments
            For Each oContactChurchInvolvement In mcChurchInvolvement.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContactChurchInvolvement_ID", oContactChurchInvolvement.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_External_ContactChurchInvolvementDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oContactChurchInvolvement In mcChurchInvolvement
                oContactChurchInvolvement.ContactID = miContactID
                oContactChurchInvolvement.Save(oUser)
            Next

        End Sub

        ReadOnly Property ChurchInvolvement() As CArrayList(Of CContactChurchInvolvement)
            Get
                Return mcChurchInvolvement
            End Get
        End Property

        ReadOnly Property Events() As Events.CContactExternalEvents
            Get
                If moEvents Is Nothing Then
                    moEvents = New Events.CContactExternalEvents()
                    moEvents.LoadID(miContactID)
                End If
                Return moEvents
            End Get
        End Property

        ReadOnly Property Revolution() As Revolution.CContactExternalRevolution
            Get
                If moRevolution Is Nothing Then
                    moRevolution = New Revolution.CContactExternalRevolution
                    moRevolution.LoadID(miContactID)
                End If
                Return moRevolution
            End Get
        End Property

        Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal Value As Integer)
                If miContactID <> Value Then mbChanged = True
                miContactID = Value
            End Set
        End Property
        Property ChurchName() As String
            Get
                Return msChurchName
            End Get
            Set(ByVal Value As String)
                If msChurchName <> Value Then mbChanged = True
                msChurchName = Value
            End Set
        End Property
        Property DenominationID() As Integer
            Get
                Return miDenominationID
            End Get
            Set(ByVal Value As Integer)
                If miDenominationID <> Value Then mbChanged = True
                miDenominationID = Value
            End Set
        End Property
        ReadOnly Property Denomination() As CDenomination
            Get
                Return Lists.GetDenominationByID(miDenominationID)
            End Get
        End Property
        Property SeniorPastorName() As String
            Get
                Return msSeniorPastorName
            End Get
            Set(ByVal Value As String)
                If msSeniorPastorName <> Value Then mbChanged = True
                msSeniorPastorName = Value
            End Set
        End Property
        Property ChurchAddress1() As String
            Get
                Return msChurchAddress1
            End Get
            Set(ByVal Value As String)
                If msChurchAddress1 <> Value Then mbChanged = True
                msChurchAddress1 = Value
            End Set
        End Property
        Property ChurchAddress2() As String
            Get
                Return msChurchAddress2
            End Get
            Set(ByVal Value As String)
                If msChurchAddress2 <> Value Then mbChanged = True
                msChurchAddress2 = Value
            End Set
        End Property
        Property ChurchSuburb() As String
            Get
                Return msChurchSuburb
            End Get
            Set(ByVal Value As String)
                If msChurchSuburb <> Value Then mbChanged = True
                msChurchSuburb = Value
            End Set
        End Property
        Property ChurchPostcode() As String
            Get
                Return msChurchPostcode
            End Get
            Set(ByVal Value As String)
                If msChurchPostcode <> Value Then mbChanged = True
                msChurchPostcode = Value
            End Set
        End Property
        Property ChurchStateID() As Integer
            Get
                Return miChurchStateID
            End Get
            Set(ByVal Value As Integer)
                If miChurchStateID <> Value Then mbChanged = True
                miChurchStateID = Value
            End Set
        End Property
        ReadOnly Property ChurchState() As CState
            Get
                Return Lists.GetStateByID(miChurchStateID)
            End Get
        End Property
        Property ChurchStateOther() As String
            Get
                Return msChurchStateOther
            End Get
            Set(ByVal Value As String)
                If msChurchStateOther <> Value Then mbChanged = True
                msChurchStateOther = Value
            End Set
        End Property
        Public ReadOnly Property ChurchStateText() As String
            Get
                If ChurchState IsNot Nothing Then
                    If ChurchState.Name.ToLower <> "other" Then
                        Return ChurchState.Name
                    Else
                        Return ChurchStateOther
                    End If
                Else
                    Return String.Empty
                End If
            End Get
        End Property
        Property ChurchCountryID() As Integer
            Get
                Return miChurchCountryID
            End Get
            Set(ByVal Value As Integer)
                If miChurchCountryID <> Value Then mbChanged = True
                miChurchCountryID = Value
            End Set
        End Property
        ReadOnly Property ChurchCountry() As CCountry
            Get
                Return Lists.GetCountryByID(miChurchCountryID)
            End Get
        End Property
        Public ReadOnly Property ChurchCountryText() As String
            Get
                If ChurchCountry IsNot Nothing Then
                    Return ChurchCountry.Name
                Else
                    Return String.Empty
                End If
            End Get
        End Property
        Property ChurchWebsite() As String
            Get
                Return msChurchWebsite
            End Get
            Set(ByVal Value As String)
                If msChurchWebsite <> Value Then mbChanged = True
                msChurchWebsite = Value
            End Set
        End Property
        Property LeaderName() As String
            Get
                Return msLeaderName
            End Get
            Set(ByVal Value As String)
                If msLeaderName <> Value Then mbChanged = True
                msLeaderName = Value
            End Set
        End Property
        Property ChurchNumberOfPeople() As String
            Get
                Return msChurchNumberOfPeople
            End Get
            Set(ByVal Value As String)
                If msChurchNumberOfPeople <> Value Then mbChanged = True
                msChurchNumberOfPeople = Value
            End Set
        End Property
        Property ChurchPhone() As String
            Get
                Return msChurchPhone
            End Get
            Set(ByVal Value As String)
                If msChurchPhone <> Value Then mbChanged = True
                msChurchPhone = Value
            End Set
        End Property
        Property ChurchEmail() As String
            Get
                Return msChurchEmail
            End Get
            Set(ByVal Value As String)
                If msChurchEmail <> Value Then mbChanged = True
                msChurchEmail = Value
            End Set
        End Property
        Property ContactDetailsVerified() As Boolean
            Get
                Return mbContactDetailsVerified
            End Get
            Set(ByVal value As Boolean)
                If mbContactDetailsVerified <> value Then mbChanged = True
                mbContactDetailsVerified = value
            End Set
        End Property
        Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                mbDeleted = value
            End Set
        End Property
        Property Inactive() As Boolean
            Get
                Return mbInactive
            End Get
            Set(ByVal value As Boolean)
                mbInactive = value
            End Set
        End Property

        Public Property Changed() As Boolean
            Get

                If mbChanged Then Return True

                If moEvents IsNot Nothing Then
                    If moEvents.Changed Then Return True
                End If
                If moRevolution IsNot Nothing Then
                    If moRevolution.Changed Then Return True
                End If

                Return False

            End Get
            Set(ByVal value As Boolean)
                mbChanged = value
            End Set
        End Property

        Public Sub ResetChanges()
            mbChanged = False

            moEvents.ResetChanges()
            moRevolution.ResetChanges()

        End Sub

    End Class

End Namespace