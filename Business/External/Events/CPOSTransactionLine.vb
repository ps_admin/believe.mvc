Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CPOSTransactionLine

        Private miPOSTransactionLineID As Integer
        Private moProduct As CProduct
        Private mdPrice As Decimal
        Private mdDiscount As Decimal
        Private miQuantity As Integer
        Private miQuantitySupplied As Integer
        Private miQuantityAssigned As Integer               'Does not get saved. This is a temporary field used for fulfilling Session Orders
        Private miQuantityReturned As Integer
        Private miOriginalPOSTransactionLineID As Integer
        Private msTitle As String

        Public Sub New()
            NewPOSTransactionLine()
        End Sub

        Private Sub NewPOSTransactionLine()

            ID = 0
            moProduct = Nothing
            mdPrice = 0
            mdDiscount = 0
            miQuantity = 0
            miQuantitySupplied = 0
            miQuantityAssigned = 0
            miQuantityReturned = 0
            miOriginalPOSTransactionLineID = 0
            msTitle = ""

        End Sub

        Public Sub NewPOSTransactionLine(ByRef oDataRow As DataRow)

            miPOSTransactionLineID = oDataRow("POSTransactionLine_ID")
            moProduct = Lists.External.Events.GetProductByID(oDataRow("Product_ID"))
            mdPrice = oDataRow("Price")
            mdDiscount = oDataRow("Discount")
            miQuantity = oDataRow("Quantity")
            miQuantitySupplied = oDataRow("QuantitySupplied")
            miQuantityReturned = oDataRow("QuantityReturned")
            miOriginalPOSTransactionLineID = IsNull(oDataRow("OriginalPOSTransactionLine_ID"), 0)
            msTitle = oDataRow("Title")

        End Sub

        Public Property ID() As Integer
            Get
                Return miPOSTransactionLineID
            End Get
            Set(ByVal Value As Integer)
                miPOSTransactionLineID = Value
            End Set
        End Property
        Public Property Product() As CProduct
            Get
                Return moProduct
            End Get
            Set(ByVal Value As CProduct)
                moProduct = Value
            End Set
        End Property
        Public Property Price() As Decimal
            Get
                Return mdPrice
            End Get
            Set(ByVal Value As Decimal)
                mdPrice = Value
            End Set
        End Property
        Public Property Discount() As Decimal
            Get
                Return mdDiscount
            End Get
            Set(ByVal Value As Decimal)
                mdDiscount = Value
            End Set
        End Property
        Public Property Quantity() As Integer
            Get
                Return miQuantity
            End Get
            Set(ByVal Value As Integer)
                miQuantity = Value
            End Set
        End Property
        Public Property QuantitySupplied() As Integer
            Get
                Return miQuantitySupplied
            End Get
            Set(ByVal Value As Integer)
                miQuantitySupplied = Value
            End Set
        End Property
        Public Property QuantityAssigned() As Integer
            Get
                Return miQuantityAssigned
            End Get
            Set(ByVal Value As Integer)
                miQuantityAssigned = Value
            End Set
        End Property
        Public Property QuantityReturned() As Integer
            Get
                Return miQuantityReturned
            End Get
            Set(ByVal Value As Integer)
                miQuantityReturned = Value
            End Set
        End Property
        Public Property OriginalPOSTransactionLineID() As Integer
            Get
                Return miOriginalPOSTransactionLineID
            End Get
            Set(ByVal Value As Integer)
                miOriginalPOSTransactionLineID = Value
            End Set
        End Property
        Public Property Title() As String
            Get
                Return msTitle
            End Get
            Set(ByVal Value As String)
                msTitle = Value
            End Set
        End Property

        Public ReadOnly Property Net() As Decimal
            Get
                Net = Math.Round(Price * ((100 - Discount) / 100), 2)
                Net = Math.Floor(Net * 20) / 20
                Net = Net * Quantity
            End Get
        End Property
        Public ReadOnly Property NetUnit() As Decimal
            Get
                NetUnit = Math.Round(Price * ((100 - Discount) / 100), 2)
                NetUnit = Math.Floor(NetUnit * 20) / 20
            End Get
        End Property

    End Class

End Namespace