Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CGroupConcise

        Dim miGroupID As Integer
        Dim msGroupName As String

        Public Sub New(Optional ByVal iGroupID As Integer = 0, Optional ByVal sGroupName As String = "")
            ID = iGroupID
            GroupName = sGroupName
        End Sub

        Public Property ID() As Integer
            Get
                Return miGroupID
            End Get
            Set(ByVal Value As Integer)
                miGroupID = Value
            End Set
        End Property
        Public Property GroupName() As String
            Get
                Return msGroupName
            End Get
            Set(ByVal Value As String)
                msGroupName = Value
            End Set
        End Property
        Public Overrides Function toString() As String
            Return msGroupName
        End Function
    End Class

End Namespace