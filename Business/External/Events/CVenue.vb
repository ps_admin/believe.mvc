Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CVenue

        Private miVenueID As Integer
        Private miConferenceID As Integer
        Private msVenueName As String
        Private msVenueLocation As String
        Private miStateID As Integer
        Private mdConferenceStartDate As Date
        Private mdConferenceEndDate As Date
        Private miCurrentRegistrants As Integer
        Private miMaxRegistrants As Integer
        Private mbAllowMultipleRegistrations As Boolean
        Private mbIsClosed As Boolean
        Private mbAccommodationClosed As Boolean
        Private mbCateringClosed As Boolean
        Private mbPlanetkidsEarlyChildhoodClosed As Boolean
        Private mbPlanetkidsPrimaryClosed As Boolean
        Private mbLeadershipBreakfastClosed As Boolean

        Private msHasStarted As String
        Private msHasEnded As String
        Private mdVenueTimesRefreshed As Date   'Date the "HasStarted" and "HasEnded" variables were refreshed

        Private mbChanged As Boolean

        Public Sub New()

            miVenueID = 0
            miConferenceID = 0
            msVenueName = String.Empty
            msVenueLocation = String.Empty
            miStateID = 0
            mdConferenceStartDate = Nothing
            mdConferenceEndDate = Nothing
            miMaxRegistrants = 0
            miCurrentRegistrants = 3
            mbAllowMultipleRegistrations = False
            mbIsClosed = False
            mbAccommodationClosed = False
            mbCateringClosed = False
            mbPlanetkidsEarlyChildhoodClosed = False
            mbPlanetkidsPrimaryClosed = False
            mbLeadershipBreakfastClosed = False
            msHasStarted = String.Empty
            msHasEnded = String.Empty

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()
            LoadDataRow(oDataRow)
        End Sub

        Public Sub LoadDataRow(ByRef oDataRow As DataRow)

            miVenueID = oDataRow("Venue_ID")
            miConferenceID = oDataRow("Conference_ID")
            msVenueName = oDataRow("VenueName")
            msVenueLocation = oDataRow("VenueLocation")
            miStateID = IsNull(oDataRow("State_ID"), 0)
            mdConferenceStartDate = oDataRow("ConferenceStartDate")
            mdConferenceEndDate = oDataRow("ConferenceEndDate")
            miMaxRegistrants = oDataRow("MaxRegistrants")
            miCurrentRegistrants = oDataRow("CurrentRegistrants")
            mbAllowMultipleRegistrations = oDataRow("AllowMultipleRegistrations")
            mbIsClosed = oDataRow("IsClosed")
            mbAccommodationClosed = oDataRow("AccommodationClosed")
            mbCateringClosed = oDataRow("CateringClosed")
            mbPlanetkidsEarlyChildhoodClosed = oDataRow("PlanetkidsEarlyChildhoodClosed")
            mbPlanetkidsPrimaryClosed = oDataRow("PlanetkidsPrimaryClosed")
            mbLeadershipBreakfastClosed = oDataRow("LeadershipBreakfastClosed")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", miConferenceID))
                oParameters.Add(New SqlClient.SqlParameter("@VenueName", msVenueName))
                oParameters.Add(New SqlClient.SqlParameter("@VenueLocation", msVenueLocation))
                oParameters.Add(New SqlClient.SqlParameter("@State_ID", miStateID))
                oParameters.Add(New SqlClient.SqlParameter("@ConferenceStartDate", SQLDate(mdConferenceStartDate)))
                oParameters.Add(New SqlClient.SqlParameter("@ConferenceEndDate", SQLDate(mdConferenceEndDate)))
                oParameters.Add(New SqlClient.SqlParameter("@MaxRegistrants", miMaxRegistrants))
                oParameters.Add(New SqlClient.SqlParameter("@AllowMultipleRegistrations", mbAllowMultipleRegistrations))
                oParameters.Add(New SqlClient.SqlParameter("@IsClosed", mbIsClosed))
                oParameters.Add(New SqlClient.SqlParameter("@AccommodationClosed", mbAccommodationClosed))
                oParameters.Add(New SqlClient.SqlParameter("@CateringClosed", mbCateringClosed))
                oParameters.Add(New SqlClient.SqlParameter("@PlanetkidsEarlyChildhoodClosed", mbPlanetkidsEarlyChildhoodClosed))
                oParameters.Add(New SqlClient.SqlParameter("@PlanetkidsPrimaryClosed", mbPlanetkidsPrimaryClosed))
                oParameters.Add(New SqlClient.SqlParameter("@LeadershipBreakfastClosed", mbLeadershipBreakfastClosed))

                If Me.ID = 0 Then
                    miVenueID = DataAccess.GetValue("sp_Events_VenueInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@Venue_ID", miVenueID))
                    DataAccess.ExecuteCommand("sp_Events_VenueUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miVenueID
            End Get
        End Property
        Public Property ConferenceID() As Integer
            Get
                Return miConferenceID
            End Get
            Set(ByVal Value As Integer)
                If miConferenceID <> Value Then mbChanged = True
                miConferenceID = Value
            End Set
        End Property
        Public ReadOnly Property Conference() As CConference
            Get
                Return Lists.External.Events.GetConferenceByID(miConferenceID)
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msVenueName
            End Get
            Set(ByVal value As String)
                If msVenueName <> value Then mbChanged = True
                msVenueName = value
            End Set
        End Property
        Public Property VenueLocation() As String
            Get
                Return msVenueLocation
            End Get
            Set(ByVal Value As String)
                If msVenueLocation <> Value Then mbChanged = True
                msVenueLocation = Value
            End Set
        End Property
        Property StateID() As Integer
            Get
                Return miStateID
            End Get
            Set(ByVal Value As Integer)
                If miStateID <> Value Then mbChanged = True
                miStateID = Value
            End Set
        End Property
        ReadOnly Property State() As CState
            Get
                Return Lists.GetStateByID(miStateID)
            End Get
        End Property
        Public Property ConferenceStartDate() As Date
            Get
                Return mdConferenceStartDate
            End Get
            Set(ByVal Value As Date)
                If mdConferenceStartDate <> Value Then mbChanged = True
                mdConferenceStartDate = Value
            End Set
        End Property
        Public Property ConferenceEndDate() As Date
            Get
                Return mdConferenceEndDate
            End Get
            Set(ByVal Value As Date)
                If mdConferenceEndDate <> Value Then mbChanged = True
                mdConferenceEndDate = Value
            End Set
        End Property
        Public ReadOnly Property CurrentRegistrants() As Integer
            Get
                Return miCurrentRegistrants
            End Get
        End Property
        Public Property MaxRegistrants() As Integer
            Get
                Return miMaxRegistrants
            End Get
            Set(ByVal Value As Integer)
                If miMaxRegistrants <> Value Then mbChanged = True
                miMaxRegistrants = Value
            End Set
        End Property
        Public Property AllowMultipleRegistrations() As Boolean
            Get
                Return mbAllowMultipleRegistrations
            End Get
            Set(ByVal value As Boolean)
                If mbAllowMultipleRegistrations <> value Then mbChanged = True
                mbAllowMultipleRegistrations = value
            End Set
        End Property
        Public Property IsClosed() As Boolean
            Get
                Return mbIsClosed
            End Get
            Set(ByVal Value As Boolean)
                If mbIsClosed <> Value Then mbChanged = True
                mbIsClosed = Value
            End Set
        End Property
        Public Property AccommodationClosed() As Boolean
            Get
                Return mbAccommodationClosed
            End Get
            Set(ByVal Value As Boolean)
                If mbAccommodationClosed <> Value Then mbChanged = True
                mbAccommodationClosed = Value
            End Set
        End Property
        Public Property CateringClosed() As Boolean
            Get
                Return mbCateringClosed
            End Get
            Set(ByVal Value As Boolean)
                If mbCateringClosed <> Value Then mbChanged = True
                mbCateringClosed = Value
            End Set
        End Property
        Property PlanetkidsEarlyChildhoodClosed() As Boolean
            Get
                Return mbPlanetkidsEarlyChildhoodClosed
            End Get
            Set(ByVal Value As Boolean)
                If mbPlanetkidsEarlyChildhoodClosed <> Value Then mbChanged = True
                mbPlanetkidsEarlyChildhoodClosed = Value
            End Set
        End Property
        Property PlanetkidsPrimaryClosed() As Boolean
            Get
                Return mbPlanetkidsPrimaryClosed
            End Get
            Set(ByVal Value As Boolean)
                If mbPlanetkidsPrimaryClosed <> Value Then mbChanged = True
                mbPlanetkidsPrimaryClosed = Value
            End Set
        End Property
        Public Property LeadershipBreakfastClosed() As Boolean
            Get
                Return mbLeadershipBreakfastClosed
            End Get
            Set(ByVal Value As Boolean)
                If mbLeadershipBreakfastClosed <> Value Then mbChanged = True
                mbLeadershipBreakfastClosed = Value
            End Set
        End Property
        Public ReadOnly Property IsActive() As Boolean
            Get
                'Venue is active is it's not closed and it's conference is still active
                Return Not IsClosed And Conference.ConferenceActive
            End Get
        End Property
        Public ReadOnly Property HasEnded() As Boolean
            Get
                Return DateAdd(DateInterval.Minute, 1, mdConferenceEndDate) < GetDateTime
            End Get
        End Property
        Public ReadOnly Property HasStarted() As Boolean
            Get
                Return DateAdd(DateInterval.Minute, 1, mdConferenceStartDate) < GetDateTime
            End Get
        End Property
        Public ReadOnly Property IsFull() As Boolean
            Get
                Return CurrentRegistrants >= MaxRegistrants
            End Get
        End Property
        Public Overrides Function ToString() As String
            Return msVenueName
        End Function
        Public ReadOnly Property VenueDescriptionLong() As String
            Get
                Return Conference.ConferenceNameShort & " - " & msVenueName
            End Get
        End Property

    End Class

End Namespace