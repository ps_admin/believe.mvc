Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CGroupPromoRequest

        Private miGroupPromoRequestID As Integer
        Private miGroupLeaderID As Integer
        Private miConferenceID As Integer
        Private miGroupPromoID As Integer
        Private mdDateAdded As Date
        Private miUserID As Integer
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miGroupPromoRequestID = 0
            miGroupLeaderID = 0
            miConferenceID = 0
            miGroupPromoID = 0
            mdDateAdded = Nothing
            miUserID = 0
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miGroupPromoRequestID = oDataRow("GroupPromoRequest_ID")
            miGroupLeaderID = oDataRow("GroupLeader_ID")
            miConferenceID = oDataRow("Conference_ID")
            miGroupPromoID = oDataRow("GroupPromo_ID")
            mdDateAdded = oDataRow("DateAdded")
            miUserID = oDataRow("User_ID")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@GroupLeader_ID", miGroupLeaderID))
                oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", miConferenceID))
                oParameters.Add(New SqlClient.SqlParameter("@GroupPromo_ID", miGroupPromoID))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", miUserID))

                If Me.ID = 0 Then
                    miGroupPromoRequestID = DataAccess.GetValue("sp_Events_GroupPromoRequestInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@GroupPromoRequest_ID", miGroupPromoRequestID))
                    DataAccess.ExecuteCommand("sp_Events_GroupPromoRequestUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub
        Public ReadOnly Property ID() As Integer
            Get
                Return miGroupPromoRequestID
            End Get
        End Property
        Public Property GroupLeaderID() As Integer
            Get
                Return miGroupLeaderID
            End Get
            Set(ByVal value As Integer)
                miGroupLeaderID = value
            End Set
        End Property
        Public Property ConferenceID() As Integer
            Get
                Return miConferenceID
            End Get
            Set(ByVal value As Integer)
                miConferenceID = value
            End Set
        End Property
        Public ReadOnly Property Conference() As CConference
            Get
                Return Lists.External.Events.GetConferenceByID(miConferenceID)
            End Get
        End Property
        Public Property GroupPromoID() As Integer
            Get
                Return miGroupPromoID
            End Get
            Set(ByVal value As Integer)
                miGroupPromoID = value
            End Set
        End Property
        Public ReadOnly Property GroupPromo() As CGroupPromo
            Get
                Return Lists.External.Events.GetGroupPromoByID(miGroupPromoID)
            End Get
        End Property
        Public Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal value As Date)
                If mdDateAdded <> value Then mbChanged = True
                mdDateAdded = value
            End Set
        End Property
        Public Property UserID() As Integer
            Get
                Return miUserID
            End Get
            Set(ByVal value As Integer)
                miUserID = value
            End Set
        End Property
        Public ReadOnly Property User() As CUser
            Get
                Return Lists.GetUserByID(miUserID)
            End Get
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace