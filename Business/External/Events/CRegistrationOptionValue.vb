Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistrationOptionValue

        Private miRegistrationOptionValueID As Integer
        Private moRegistrationOptionName As CRegistrationOptionName
        Private msName As String
        Private msDescription As String
        Private miValue As Integer
        Private moOtherInformationRegistrationOptionName As CRegistrationOptionName
        Private mbAvailableOnline As Boolean
        Private mbIsDefault As Boolean
        Private miSortOrder As Integer

        Public Sub New()

            miRegistrationOptionValueID = 0
            moRegistrationOptionName = Nothing
            msName = String.Empty
            msDescription = String.Empty
            miValue = 0
            moOtherInformationRegistrationOptionName = Nothing
            mbAvailableOnline = False
            mbIsDefault = False
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegistrationOptionValueID = oDataRow("RegistrationOptionValue_ID")
            moRegistrationOptionName = Lists.External.Events.GetRegistrationOptionNameByID(oDataRow("RegistrationOptionName_ID"))
            msName = oDataRow("Name")
            msDescription = oDataRow("Description")
            miValue = oDataRow("Value")
            moOtherInformationRegistrationOptionName = Lists.External.Events.GetRegistrationOptionNameByID(IsNull(oDataRow("OtherInformation_RegistrationOptionName_ID"), 0))
            mbAvailableOnline = oDataRow("AvailableOnline")
            mbIsDefault = oDataRow("IsDefault")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miRegistrationOptionValueID
            End Get
        End Property
        Public Property RegistrationOptionName() As CRegistrationOptionName
            Get
                Return moRegistrationOptionName
            End Get
            Set(ByVal Value As CRegistrationOptionName)
                moRegistrationOptionName = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property
        Public Property Description() As String
            Get
                Return msDescription
            End Get
            Set(ByVal Value As String)
                msDescription = Value
            End Set
        End Property
        Public Property Value() As Integer
            Get
                Return miValue
            End Get
            Set(ByVal Value As Integer)
                miValue = Value
            End Set
        End Property
        Public Property OtherInformationRegistrationOptionName() As CRegistrationOptionName
            Get
                Return moOtherInformationRegistrationOptionName
            End Get
            Set(ByVal value As CRegistrationOptionName)
                moOtherInformationRegistrationOptionName = value
            End Set
        End Property
        Public Property AvailableOnline() As Boolean
            Get
                Return mbAvailableOnline
            End Get
            Set(ByVal Value As Boolean)
                mbAvailableOnline = Value
            End Set
        End Property
        Public Property IsDefault() As Boolean
            Get
                Return mbIsDefault
            End Get
            Set(ByVal Value As Boolean)
                mbIsDefault = Value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal Value As Integer)
                miSortOrder = Value
            End Set
        End Property

        Public Overrides Function toString() As String
            Return msName
        End Function

    End Class

End Namespace