Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CPOSTransactionPayment

        Private miPOSTransactionPaymentID As Integer
        Private moPaymentType As CPaymentType
        Private mdPaymentAmount As Decimal
        Private mdPaymentTendered As Decimal
        Private msChequeDrawer As String
        Private msChequeBank As String
        Private msChequeBranch As String
        Private moCreditCardType As CCreditCardType
        Private msGUID As String

        Public Sub New()

            miPOSTransactionPaymentID = 0
            moPaymentType = Nothing
            mdPaymentAmount = 0
            mdPaymentTendered = 0
            msChequeDrawer = String.Empty
            msChequeBank = String.Empty
            msChequeBranch = String.Empty
            moCreditCardType = Lists.GetCreditCardTypeByID(1)
            msGUID = System.Guid.NewGuid.ToString

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miPOSTransactionPaymentID = oDataRow("POSTransactionPayment_ID")
            moPaymentType = Lists.GetPaymentTypeByID(oDataRow("PaymentType"))
            mdPaymentAmount = oDataRow("PaymentAmount")
            mdPaymentTendered = oDataRow("PaymentTendered")
            msChequeDrawer = oDataRow("ChequeDrawer")
            msChequeBank = oDataRow("ChequeBank")
            msChequeBranch = oDataRow("ChequeBranch")
            moCreditCardType = Lists.GetCreditCardTypeByID(oDataRow("CreditCardType"))

        End Sub

        Public Property ID() As Integer
            Get
                Return miPOSTransactionPaymentID
            End Get
            Set(ByVal Value As Integer)
                miPOSTransactionPaymentID = Value
            End Set
        End Property
        Property PaymentType() As CPaymentType
            Get
                Return moPaymentType
            End Get
            Set(ByVal Value As CPaymentType)
                moPaymentType = Value
            End Set
        End Property
        Property PaymentAmount() As Decimal
            Get
                Return mdPaymentAmount
            End Get
            Set(ByVal Value As Decimal)
                mdPaymentAmount = Value
            End Set
        End Property
        Property PaymentTendered() As Decimal
            Get
                Return mdPaymentTendered
            End Get
            Set(ByVal Value As Decimal)
                mdPaymentTendered = Value
            End Set
        End Property
        Property ChequeDrawer() As String
            Get
                Return msChequeDrawer
            End Get
            Set(ByVal Value As String)
                msChequeDrawer = Value
            End Set
        End Property
        Property ChequeBank() As String
            Get
                Return msChequeBank
            End Get
            Set(ByVal Value As String)
                msChequeBank = Value
            End Set
        End Property
        Property ChequeBranch() As String
            Get
                Return msChequeBranch
            End Get
            Set(ByVal Value As String)
                msChequeBranch = Value
            End Set
        End Property
        Property CreditCardType() As CCreditCardType
            Get
                Return moCreditCardType
            End Get
            Set(ByVal Value As CCreditCardType)
                moCreditCardType = Value
            End Set
        End Property
        Property GUID() As String
            Get
                Return msGUID
            End Get
            Set(ByVal Value As String)
                msGUID = Value
            End Set
        End Property

    End Class

End Namespace