Namespace External.Events

    <CLSCompliant(True)> _
    <Serializable()> _
    Public Class CAccommodationVenue

        Private miAccommodationVenueID As Integer
        Private msVenueName As String
        Private msVenueLocation As String
        Private miVenueID As Integer
        Private miMaxPeople As Integer

        Private mbChanged As Boolean

        Public Sub New()

            miAccommodationVenueID = 0
            msVenueName = String.Empty
            msVenueLocation = String.Empty
            miVenueID = 0
            miMaxPeople = 0
            mbChanged = False

        End Sub

        Public Sub New(ByRef oAccommodation As DataRow)

            Me.New()

            miAccommodationVenueID = oAccommodation("Accommodation_ID")
            msVenueName = oAccommodation("AccommodationVenueName")
            msVenueLocation = oAccommodation("AccommodationVenueLocation")
            miVenueID = oAccommodation("Venue_ID")
            miMaxPeople = oAccommodation("MaxPeople")

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@AccommodationVenueName", msVenueName))
                oParameters.Add(New SqlClient.SqlParameter("@AccommodationVenueLocation", msVenueLocation))
                oParameters.Add(New SqlClient.SqlParameter("@Venue_ID", miVenueID))
                oParameters.Add(New SqlClient.SqlParameter("@MaxPeople", miMaxPeople))

                If Me.ID = 0 Then
                    miAccommodationVenueID = DataAccess.GetValue("sp_Events_AccommodationInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@Accommodation_ID", miAccommodationVenueID))
                    DataAccess.ExecuteCommand("sp_Events_AccommodationUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public Shadows Sub Delete()

            DataAccess.ExecuteCommand("sp_Events_AccommodationDelete " & MSQLUtilities.SQL(ID))

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miAccommodationVenueID
            End Get
        End Property
        Public Property VenueName() As String
            Get
                Return msVenueName
            End Get
            Set(ByVal Value As String)
                If msVenueName <> Value Then mbChanged = True
                msVenueName = Value
            End Set
        End Property
        Public Property VenueLocation() As String
            Get
                Return msVenueLocation
            End Get
            Set(ByVal Value As String)
                If msVenueLocation <> Value Then mbChanged = True
                msVenueLocation = Value
            End Set
        End Property
        Public Property VenueID() As Integer
            Get
                Return miVenueID
            End Get
            Set(ByVal Value As Integer)
                If miVenueID <> Value Then mbChanged = True
                miVenueID = Value
            End Set
        End Property
        Public ReadOnly Property Venue() As CVenue
            Get
                Return Lists.External.Events.GetVenueByID(miVenueID)
            End Get
        End Property
        Public Property MaxPeople() As Integer
            Get
                Return miMaxPeople
            End Get
            Set(ByVal Value As Integer)
                If miMaxPeople <> Value Then mbChanged = True
                miMaxPeople = Value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property

        Public Overrides Function ToString() As String
            ToString = VenueName
        End Function

    End Class

End Namespace