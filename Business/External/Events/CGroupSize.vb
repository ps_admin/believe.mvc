Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CGroupSize

        Private miGroupSizeID As Integer
        Private msName As String
        Private miSortOrder As Integer

        Public Sub New()

            miGroupSizeID = 0
            msName = String.Empty
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miGroupSizeID = oDataRow("GroupSize_ID")
            msName = oDataRow("Name")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public Property ID() As Integer
            Get
                Return miGroupSizeID
            End Get
            Set(ByVal Value As Integer)
                miGroupSizeID = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

        Public Overrides Function ToString() As String
            Return Me.Name
        End Function
    End Class

End Namespace