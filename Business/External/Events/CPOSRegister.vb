Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CPOSRegister

        Private miPOSRegisterID As Integer
        Private msPOSRegister As String

        Public Sub New()

            miPOSRegisterID = 0
            msPOSRegister = String.Empty

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            ID = oDataRow("POSRegister_ID")
            POSRegister = oDataRow("POSRegister")

        End Sub


        Public Sub Save()
            If ID = 0 Then
                ID = DataAccess.GetValue("sp_Events_POSRegisterInsert " & MSQLUtilities.SQL(msPOSRegister))
            Else
                DataAccess.ExecuteCommand("sp_Events_POSRegisterUpdate " & MSQLUtilities.SQL(ID) & ", " & MSQLUtilities.SQL(msPOSRegister))
            End If
        End Sub

        Public Property ID() As Integer
            Get
                Return miPOSRegisterID
            End Get
            Set(ByVal Value As Integer)
                miPOSRegisterID = Value
            End Set
        End Property
        Public Property POSRegister() As String
            Get
                Return msPOSRegister
            End Get
            Set(ByVal Value As String)
                msPOSRegister = Value
            End Set
        End Property

        Public Overrides Function toString() As String
            Return msPOSRegister
        End Function

    End Class

End Namespace