Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CProductPrice

        Private miProductPriceID As Integer
        Private moCountry As CCountry
        Private mdPrice As Decimal
        Private msGUID As String

        Public Sub New()
            NewProductPrice()
        End Sub
        Private Sub NewProductPrice()

            miProductPriceID = 0
            moCountry = Nothing
            mdPrice = 0
            msGUID = System.Guid.NewGuid.ToString

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miProductPriceID = oDataRow("ProductPrice_ID")
            moCountry = Lists.GetCountryByID(oDataRow("Country_ID"))
            mdPrice = oDataRow("Price")

        End Sub

        Property ID() As Integer
            Get
                Return miProductPriceID
            End Get
            Set(ByVal Value As Integer)
                miProductPriceID = Value
            End Set
        End Property
        Property Country() As CCountry
            Get
                Return moCountry
            End Get
            Set(ByVal Value As CCountry)
                moCountry = Value
            End Set
        End Property
        Property Price() As Decimal
            Get
                Return mdPrice
            End Get
            Set(ByVal Value As Decimal)
                mdPrice = Value
            End Set
        End Property
        Property GUID() As String
            Get
                Return msGUID
            End Get
            Set(ByVal Value As String)
                msGUID = Value
            End Set
        End Property

    End Class

End Namespace