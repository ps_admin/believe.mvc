Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistrationOptionName

        Private miRegistrationOptionNameID As Integer
        Private moConference As CConference
        Private msName As String
        Private msDescription As String
        Private msFieldType As String
        Private miSortOrder As Integer
        Private mbIncludeFieldInReport As Boolean

        Public Sub New()

            miRegistrationOptionNameID = 0
            moConference = Nothing
            msName = String.Empty
            msDescription = String.Empty
            msFieldType = String.Empty
            miSortOrder = 0
            mbIncludeFieldInReport = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegistrationOptionNameID = oDataRow("RegistrationOptionName_ID")
            moConference = Lists.External.Events.GetConferenceByID(oDataRow("Conference_ID"))
            msName = oDataRow("Name")
            msDescription = oDataRow("Description")
            msFieldType = oDataRow("FieldType")
            miSortOrder = oDataRow("SortOrder")
            mbIncludeFieldInReport = oDataRow("IncludeFieldInReport")

        End Sub

        Public ReadOnly Property HasValues() As Boolean
            Get
                Dim oRegistrationOptionValue As CRegistrationOptionValue
                For Each oRegistrationOptionValue In Lists.External.Events.RegistrationOptionValues.Values
                    If oRegistrationOptionValue.RegistrationOptionName.ID = miRegistrationOptionNameID Then
                        Return True
                    End If
                Next
                Return False
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miRegistrationOptionNameID
            End Get
        End Property
        Public Property Conference() As CConference
            Get
                Return moConference
            End Get
            Set(ByVal Value As CConference)
                moConference = Value
            End Set
        End Property

        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property
        Public Property Description() As String
            Get
                Return msDescription
            End Get
            Set(ByVal Value As String)
                msDescription = Value
            End Set
        End Property
        Public Property FieldType() As String
            Get
                Return msFieldType
            End Get
            Set(ByVal Value As String)
                msFieldType = Value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal Value As Integer)
                miSortOrder = Value
            End Set
        End Property
        Public Property IncludeFieldInReport() As Boolean
            Get
                Return mbIncludeFieldInReport
            End Get
            Set(ByVal value As Boolean)
                mbIncludeFieldInReport = value
            End Set
        End Property

        Public Overrides Function toString() As String
            Return msName
        End Function

        Public Function GetRegistrationOptionValues() As ArrayList

            Dim oArrayList As New ArrayList
            Dim oRegistrationOptionValue As CRegistrationOptionValue
            For Each oRegistrationOptionValue In Lists.External.Events.RegistrationOptionValues.Values
                If oRegistrationOptionValue.RegistrationOptionName.ID = Me.ID Then
                    oArrayList.Add(oRegistrationOptionValue)
                End If
            Next
            oArrayList.Sort(New CSorterBySortOrder)
            Return oArrayList

        End Function

    End Class

End Namespace