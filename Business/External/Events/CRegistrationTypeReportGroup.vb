Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistrationTypeReportGroup

        Private miRegistrationTypeReportGroupID As Integer
        Private msName As String
        Private miConferenceID As Integer
        Private miSortOrder As Integer

        Public Sub New()

            miRegistrationTypeReportGroupID = 0
            msName = String.Empty
            miConferenceID = 0
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegistrationTypeReportGroupID = oDataRow("RegistrationTypeReportGroup_ID")
            msName = oDataRow("Name")
            miConferenceID = oDataRow("Conference_ID")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public Property ID() As Integer
            Get
                Return miRegistrationTypeReportGroupID
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeReportGroupID = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property
        Public Property ConferenceID() As Integer
            Get
                Return miConferenceID
            End Get
            Set(ByVal Value As Integer)
                miConferenceID = Value
            End Set
        End Property
        Public ReadOnly Property Conference() As CConference
            Get
                Return Lists.External.Events.GetConferenceByID(miConferenceID)
            End Get
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace