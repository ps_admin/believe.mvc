﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CContactOldPassword

    Private miOldPasswordID As Integer
    Private miContactID As Integer
    Private mdDateChanged As Date
    Private msPassword As String

    Public Sub New()

        miOldPasswordID = 0
        miContactID = 0
        mdDateChanged = Nothing
        msPassword = String.Empty

    End Sub

    Public Sub Save()

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
        oParameters.Add(New SqlClient.SqlParameter("@DateChanged", SQLDateTime(mdDateChanged)))
        oParameters.Add(New SqlClient.SqlParameter("@Password", msPassword))

        If Me.ID = 0 Then
            miOldPasswordID = DataAccess.GetValue("sp_Common_ContactOldPasswordInsert", oParameters)
        End If

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miOldPasswordID
        End Get
    End Property
    Public Property ContactID() As Integer
        Get
            Return miContactID
        End Get
        Set(ByVal value As Integer)
            miContactID = value
        End Set
    End Property
    Public Property DateChanged() As Date
        Get
            Return mdDateChanged
        End Get
        Set(ByVal value As Date)
            mdDateChanged = value
        End Set
    End Property
    Public Property Password() As String
        Get
            Return msPassword
        End Get
        Set(ByVal value As String)
            msPassword = value
        End Set
    End Property

End Class
