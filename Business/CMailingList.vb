
<Serializable()> _
<CLSCompliant(True)> _
Public Class CMailingList

    Private miMailingListID As Integer
    Private msName As String
    Private msDescription As String
    Private msDatabase As String
    Private mbTickedByDefault As Boolean
    Private miTickedForCampusID As Integer
    Private miSortOrder As Integer

    Public Sub New()

        miMailingListID = 0
        msName = String.Empty
        msDescription = String.Empty
        msDatabase = String.Empty
        mbTickedByDefault = False
        miTickedForCampusID = 0
        miSortOrder = 0

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miMailingListID = oDataRow("MailingList_ID")
        msName = oDataRow("Name")
        msDescription = oDataRow("Description")
        msDatabase = oDataRow("Database")
        mbTickedByDefault = oDataRow("TickedByDefault")
        miTickedForCampusID = IsNull(oDataRow("TickedForCampus_ID"), 0)
        miSortOrder = oDataRow("SortOrder")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miMailingListID
        End Get
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return msDescription
        End Get
        Set(ByVal Value As String)
            msDescription = Value
        End Set
    End Property

    Public Property Database() As String
        Get
            Return msDatabase
        End Get
        Set(ByVal value As String)
            msDatabase = value
        End Set
    End Property

    Public Property TickedByDefault() As String
        Get
            Return mbTickedByDefault
        End Get
        Set(ByVal value As String)
            mbTickedByDefault = value
        End Set
    End Property

    Public Property TickedForCampusID() As Integer
        Get
            Return miTickedForCampusID
        End Get
        Set(ByVal value As Integer)
            miTickedForCampusID = value
        End Set
    End Property

    Public Property SortOrder() As Integer
        Get
            Return miSortOrder
        End Get
        Set(ByVal value As Integer)
            miSortOrder = value
        End Set
    End Property

    Public Overrides Function toString() As String
        Return msName
    End Function
End Class