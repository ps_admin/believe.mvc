Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class C247PrayerTime

        Private miTimeID As Integer
        Private msName As String
        Private msStartTime As String
        Private msEndTime As String
        Private miSortOrder As Integer

        Public Sub New()

            miTimeID = 0
            msName = String.Empty
            msStartTime = String.Empty
            msEndTime = String.Empty
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miTimeID = oDataRow("Time_ID")
            msName = oDataRow("Name")
            msStartTime = oDataRow("StartTime")
            msEndTime = oDataRow("EndTime")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miTimeID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property StartTime() As String
            Get
                Return msStartTime
            End Get
            Set(ByVal value As String)
                msStartTime = value
            End Set
        End Property
        Public Property EndTime() As String
            Get
                Return msEndTime
            End Get
            Set(ByVal value As String)
                msEndTime = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace