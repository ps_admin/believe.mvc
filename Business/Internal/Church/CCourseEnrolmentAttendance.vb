﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCourseEnrolmentAttendance

        Private miCourseEnrolmentAttendanceID As Integer
        Private miContactID As Integer
        Private miCourseSessionID As Integer
        Private miMarkedOffByID As Integer
        Private mdMarkedOffDateTime As Date
        Private msGUID As String
        Private mcTopicsCovered As CArrayList(Of CCourseEnrolmentAttendanceTopicsCovered)

        Private mbChanged As Boolean

        Public Sub New()

            miCourseEnrolmentAttendanceID = 0
            miContactID = 0
            miCourseSessionID = 0
            miMarkedOffByID = 0
            mdMarkedOffDateTime = Nothing
            msGUID = System.Guid.NewGuid.ToString
            mcTopicsCovered = New CArrayList(Of CCourseEnrolmentAttendanceTopicsCovered)

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCourseEnrolmentAttendanceID = oDataRow("CourseEnrolmentAttendance_ID")
            miContactID = oDataRow("Contact_ID")
            miCourseSessionID = oDataRow("CourseSession_ID")
            miMarkedOffByID = oDataRow("MarkedOffBy_ID")
            mdMarkedOffDateTime = oDataRow("MarkedOffDateTime")

            For Each oTopicsCoveredRow As DataRow In oDataRow.GetChildRows("CourseEnrolmentAttendanceTopicsCovered")
                mcTopicsCovered.Add(New CCourseEnrolmentAttendanceTopicsCovered(oTopicsCoveredRow))
            Next
            mcTopicsCovered.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@CourseSession_ID", miCourseSessionID))
                oParameters.Add(New SqlClient.SqlParameter("@MarkedOffBy_ID", miMarkedOffByID))
                oParameters.Add(New SqlClient.SqlParameter("@MarkedOffDateTime", SQLDateTime(mdMarkedOffDateTime)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", SQLObject(oUser)))

                If Me.ID = 0 Then
                    miCourseEnrolmentAttendanceID = DataAccess.GetValue("sp_Church_CourseEnrolmentAttendanceInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@CourseEnrolmentAttendance_ID", miCourseEnrolmentAttendanceID))
                    DataAccess.ExecuteCommand("sp_Church_CourseEnrolmentAttendanceUpdate", oParameters)
                End If

                mbChanged = False

            End If

            If Me.ID > 0 Then
                SaveTopicsCovered()
                mcTopicsCovered.ResetChanges()
            End If

        End Sub

        Private Sub SaveTopicsCovered()

            Dim oParameters As ArrayList
            Dim oTopicCovered As CCourseEnrolmentAttendanceTopicsCovered

            'Remove the Deleted topics covered
            For Each oTopicCovered In mcTopicsCovered.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@CourseEnrolmentAttendanceTopicsCovered_ID", oTopicCovered.ID))
                DataAccess.ExecuteCommand("sp_Church_CourseEnrolmentAttendanceTopicsCoveredDelete", oParameters)
            Next

            For Each oTopicCovered In mcTopicsCovered
                oTopicCovered.CourseEnrolmentAttendanceID = miCourseEnrolmentAttendanceID
                oTopicCovered.Save()
            Next

        End Sub

        Public Function CheckChanges(ByVal oCourseEnrolmentAttendance As CCourseEnrolmentAttendance) As Boolean

            Return oCourseEnrolmentAttendance Is Nothing OrElse _
                (miContactID <> oCourseEnrolmentAttendance.ContactID Or _
                miCourseSessionID <> oCourseEnrolmentAttendance.CourseSessionID Or _
                miMarkedOffByID <> oCourseEnrolmentAttendance.MarkedOffByID Or _
                Format(mdMarkedOffDateTime, "dd MMM yyyy HH:mm:ss") <> Format(oCourseEnrolmentAttendance.MarkedOffDateTime, "dd MMM yyyy HH:mm:ss"))

        End Function

        Public ReadOnly Property TopicsCovered() As CArrayList(Of CCourseEnrolmentAttendanceTopicsCovered)
            Get
                Return mcTopicsCovered
            End Get
        End Property
        Public ReadOnly Property ID() As Integer
            Get
                Return miCourseEnrolmentAttendanceID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property CourseSessionID() As Integer
            Get
                Return miCourseSessionID
            End Get
            Set(ByVal value As Integer)
                If miCourseSessionID <> value Then mbChanged = True
                miCourseSessionID = value
            End Set
        End Property
        Public ReadOnly Property CourseSession() As CCourseSession
            Get
                Return Lists.Internal.Church.GetCourseSessionByID(miCourseSessionID)
            End Get
        End Property
        Public Property MarkedOffByID() As Integer
            Get
                Return miMarkedOffByID
            End Get
            Set(ByVal value As Integer)
                If miMarkedOffByID <> value Then mbChanged = True
                miMarkedOffByID = value
            End Set
        End Property
        Public Property MarkedOffDateTime() As Date
            Get
                Return mdMarkedOffDateTime
            End Get
            Set(ByVal value As Date)
                If mdMarkedOffDateTime <> value Then mbChanged = True
                mdMarkedOffDateTime = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace