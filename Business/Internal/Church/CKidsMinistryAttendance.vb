Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CKidsMinistryAttendance

        Private miKidsMinistryAttendanceID As Integer
        Private miContactID As Integer
        Private miPCRDateID As Integer
        Private mdServiceDate As Date
        Private miChurchServiceID As Integer
        Private mdCheckInTime As Date
        Private miCheckedInByParentID As Integer
        Private msCheckedInByParentName As String
        Private miCheckedInByLeaderID As Integer
        Private msCheckedInByLeaderName As String
        Private mdCheckOutTime As Date
        Private miCheckedOutByParentID As Integer
        Private msCheckedOutByParentName As String
        Private miCheckedOutByLeaderID As Integer
        Private msCheckedOutByLeaderName As String
        Private mbChanged As Boolean
        Private msGUID As String

        Public Sub New()

            miKidsMinistryAttendanceID = 0
            miContactID = 0
            miPCRDateID = 0
            mdServiceDate = Nothing
            miChurchServiceID = 0
            mdCheckInTime = Nothing
            miCheckedInByParentID = 0
            msCheckedInByParentName = String.Empty
            miCheckedInByLeaderID = 0
            msCheckedInByLeaderName = String.Empty
            mdCheckOutTime = Nothing
            miCheckedOutByParentID = 0
            msCheckedOutByParentName = String.Empty
            miCheckedOutByLeaderID = 0
            msCheckedOutByLeaderName = String.Empty
            mbChanged = False
            msGUID = System.Guid.NewGuid.ToString

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miKidsMinistryAttendanceID = oDataRow("KidsMinistryAttendance_ID")
            miContactID = oDataRow("Contact_ID")
            miPCRDateID = IsNull(oDataRow("PCRDate_ID"), 0)
            mdServiceDate = oDataRow("Date")
            miChurchServiceID = oDataRow("ChurchService_ID")
            mdCheckInTime = oDataRow("CheckInTime")
            miCheckedInByParentID = oDataRow("CheckedInByParent_ID")
            msCheckedInByParentName = oDataRow("CheckedInByParentName")
            miCheckedInByLeaderID = oDataRow("CheckedInByLeader_ID")
            msCheckedInByLeaderName = oDataRow("CheckedInByLeaderName")
            mdCheckOutTime = IsNull(oDataRow("CheckOutTime"), Nothing)
            miCheckedOutByParentID = IsNull(oDataRow("CheckedOutByParent_ID"), 0)
            msCheckedOutByParentName = oDataRow("CheckedOutByParentName")
            miCheckedOutByLeaderID = IsNull(oDataRow("CheckedOutByLeader_ID"), 0)
            msCheckedOutByLeaderName = oDataRow("CheckedOutByLeaderName")

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@PCRDate_ID", SQLNull(miPCRDateID)))
                oParameters.Add(New SqlClient.SqlParameter("@Date", SQLDate(mdServiceDate)))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", miChurchServiceID))
                oParameters.Add(New SqlClient.SqlParameter("@CheckInTime", SQLDateTime(mdCheckInTime)))
                oParameters.Add(New SqlClient.SqlParameter("@CheckedInByParent_ID", miCheckedInByParentID))
                oParameters.Add(New SqlClient.SqlParameter("@CheckedInByLeader_ID", miCheckedInByLeaderID))
                oParameters.Add(New SqlClient.SqlParameter("@CheckOutTime", SQLDateTime(mdCheckOutTime)))
                oParameters.Add(New SqlClient.SqlParameter("@CheckedOutByParent_ID", SQLNull(miCheckedOutByParentID)))
                oParameters.Add(New SqlClient.SqlParameter("@CheckedOutByLeader_ID", SQLNull(miCheckedOutByLeaderID)))

                If Me.ID = 0 Then
                    miKidsMinistryAttendanceID = DataAccess.GetValue("sp_Church_KidsMinistryAttendanceInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryAttendance_ID", miKidsMinistryAttendanceID))
                    DataAccess.ExecuteCommand("sp_Church_KidsMinistryAttendanceUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miKidsMinistryAttendanceID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If value <> miContactID Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Property PCRDateID() As Integer
            Get
                Return miPCRDateID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miPCRDateID Then mbChanged = True
                miPCRDateID = Value
            End Set
        End Property
        Public Property ServiceDate() As Date
            Get
                Return mdServiceDate
            End Get
            Set(ByVal value As Date)
                If value <> mdServiceDate Then mbChanged = True
                mdServiceDate = value
            End Set
        End Property
        Property ChurchServiceID() As Integer
            Get
                Return miChurchServiceID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miChurchServiceID Then mbChanged = True
                miChurchServiceID = Value
            End Set
        End Property
        Public Property CheckInTime() As Date
            Get
                Return mdCheckInTime
            End Get
            Set(ByVal value As Date)
                If value <> mdCheckInTime Then mbChanged = True
                mdCheckInTime = value
            End Set
        End Property
        Property CheckedInByParentID() As Integer
            Get
                Return miCheckedInByParentID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miCheckedInByParentID Then mbChanged = True
                miCheckedInByParentID = Value
            End Set
        End Property
        ReadOnly Property CheckedInByParentName() As String
            Get
                Return msCheckedInByParentName
            End Get
        End Property
        Property CheckedInByLeaderID() As Integer
            Get
                Return miCheckedInByLeaderID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miCheckedInByLeaderID Then mbChanged = True
                miCheckedInByLeaderID = Value
            End Set
        End Property
        ReadOnly Property CheckedInByLeaderName() As String
            Get
                Return msCheckedInByLeaderName
            End Get
        End Property
        Public Property CheckOutTime() As Date
            Get
                Return mdCheckOutTime
            End Get
            Set(ByVal value As Date)
                If value <> mdCheckOutTime Then mbChanged = True
                mdCheckOutTime = value
            End Set
        End Property
        Property CheckedOutByParentID() As Integer
            Get
                Return miCheckedOutByParentID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miCheckedOutByParentID Then mbChanged = True
                miCheckedOutByParentID = Value
            End Set
        End Property
        ReadOnly Property CheckedOutByParentName() As String
            Get
                Return msCheckedOutByParentName
            End Get
        End Property
        Property CheckedOutByLeaderID() As Integer
            Get
                Return miCheckedOutByLeaderID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miCheckedOutByLeaderID Then mbChanged = True
                miCheckedOutByLeaderID = Value
            End Set
        End Property
        ReadOnly Property CheckedOutByLeaderName() As String
            Get
                Return msCheckedOutByLeaderName
            End Get
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property


    End Class

End Namespace