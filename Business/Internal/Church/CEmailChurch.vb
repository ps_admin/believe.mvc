Imports System.text

Namespace Internal.Church

    Public Class CEmailChurch

        Private msEmailText As String
        Private msSubject As String

        Public Sub New()
            msEmailText = ""
            msSubject = ""
        End Sub

        Public Function SendEmail(ByVal sEmailAddress As String, ByVal sName As String, ByRef sError As String) As Boolean

            Return DataAccess.SendEmail(sEmailAddress, sName, msSubject, EmailHeader + msEmailText + EmailFooter, New ArrayList, sError)

        End Function


        Public Property EmailText() As String
            Get
                Return msEmailText
            End Get
            Set(ByVal value As String)
                msEmailText = value
            End Set
        End Property
        Public Property Subject() As String
            Get
                Return msSubject
            End Get
            Set(ByVal value As String)
                msSubject = value
            End Set
        End Property

        Private ReadOnly Property EmailHeader() As String
            Get

                Dim sHeader As New StringBuilder

                sHeader.AppendLine("<html>")
                sHeader.AppendLine("<head></head>")
                sHeader.AppendLine("<body style=""font-family: 'Trebuchet MS', Arial"">")
                sHeader.AppendLine("<table cellspacing=""0"" cellpadding=""3"" style=""width: 1024px"">")
                sHeader.AppendLine("<tr><td>")

                Return sHeader.ToString()

            End Get
        End Property

        Private ReadOnly Property EmailFooter() As String
            Get

                Dim sHeader As New StringBuilder

                sHeader.AppendLine("</td></tr>")
                sHeader.AppendLine("</table>")
                sHeader.AppendLine("</body>")
                sHeader.AppendLine("</html>")

                Return sHeader.ToString()

            End Get
        End Property

    End Class

End Namespace