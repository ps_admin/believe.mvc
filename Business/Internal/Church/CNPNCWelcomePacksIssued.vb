Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CNPNCWelcomePacksIssued

        Private miWelcomePacksIssuedID As Integer
        Private miCampusID As Integer
        Private mdDateIssued As Date
        Private miNumberIssued As Integer
        Private miNumberReturned As Integer
        Private miEnteredByID As Integer
        Private mdDateEntered As Date

        Private mbChanged As Boolean

        Public Sub New()

            miWelcomePacksIssuedID = 0
            miCampusID = 0
            mdDateIssued = Nothing
            miNumberIssued = 0
            miNumberReturned = 0
            miEnteredByID = 0
            mdDateEntered = Nothing

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miWelcomePacksIssuedID = oDataRow("WelcomePacksIssued_ID")
            miCampusID = oDataRow("Campus_ID")
            mdDateIssued = oDataRow("DateIssued")
            miNumberIssued = oDataRow("NumberIssued")
            miNumberReturned = oDataRow("NumberReturned")
            miEnteredByID = oDataRow("EnteredBy_ID")
            mdDateEntered = oDataRow("DateEntered")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", miCampusID))
                oParameters.Add(New SqlClient.SqlParameter("@DateIssued", SQLDate(mdDateIssued)))
                oParameters.Add(New SqlClient.SqlParameter("@NumberIssued", miNumberIssued))
                oParameters.Add(New SqlClient.SqlParameter("@NumberReturned", miNumberReturned))
                oParameters.Add(New SqlClient.SqlParameter("@EnteredBy_ID", SQLNull(miEnteredByID)))
                oParameters.Add(New SqlClient.SqlParameter("@DateEntered", SQLDateTime(mdDateEntered)))

                If Me.ID = 0 Then
                    miWelcomePacksIssuedID = DataAccess.GetValue("sp_Church_NPNCWelcomePacksIssuedInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@WelcomePacksIssued_ID", miWelcomePacksIssuedID))
                    DataAccess.ExecuteCommand("sp_Church_NPNCWelcomePacksIssuedUpdate", oParameters)
                End If

            End If

        End Sub

        Public Sub Delete()

            If miWelcomePacksIssuedID > 0 Then
                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@WelcomePacksIssued_ID", miWelcomePacksIssuedID))
                DataAccess.ExecuteCommand("sp_Church_NPNCWelcomePacksIssuedDelete", oParameters)
            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miWelcomePacksIssuedID
            End Get
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal value As Integer)
                If miCampusID <> value Then mbChanged = True
                miCampusID = value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public Property DateIssued() As Date
            Get
                Return mdDateIssued
            End Get
            Set(ByVal value As Date)
                If mdDateIssued <> value Then mbChanged = True
                mdDateIssued = value
            End Set
        End Property
        Public Property NumberIssued() As Integer
            Get
                Return miNumberIssued
            End Get
            Set(ByVal value As Integer)
                If miNumberIssued <> value Then mbChanged = True
                miNumberIssued = value
            End Set
        End Property
        Public Property NumberReturned() As Integer
            Get
                Return miNumberReturned
            End Get
            Set(ByVal value As Integer)
                If miNumberReturned <> value Then mbChanged = True
                miNumberReturned = value
            End Set
        End Property
        Public Property EnteredByID() As Integer
            Get
                Return miEnteredByID
            End Get
            Set(ByVal value As Integer)
                If miEnteredByID <> value Then mbChanged = True
                miEnteredByID = value
            End Set
        End Property
        Public ReadOnly Property EnteredBy() As CUser
            Get
                Return Lists.GetUserByID(miEnteredByID)
            End Get
        End Property
        Public Property DateEntered() As Date
            Get
                Return mdDateEntered
            End Get
            Set(ByVal value As Date)
                If mdDateEntered <> value Then mbChanged = True
                mdDateEntered = value
            End Set
        End Property

    End Class

End Namespace