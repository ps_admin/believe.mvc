Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CULGDateDay

        Private miULGDateDayID As Integer
        Private msName As String
        Private miSortOrder As Integer

        Public Sub New()

            miULGDateDayID = 0
            msName = String.Empty
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miULGDateDayID = oDataRow("ULGDateDay_ID")
            msName = oDataRow("Name")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miULGDateDayID
            End Get
        End Property

        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property

        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace