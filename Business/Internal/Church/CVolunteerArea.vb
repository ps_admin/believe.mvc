﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CVolunteerArea

        Private miVolunteerAreaID As Integer
        Private msName As String
        Private mbActive As Boolean

        Public Sub New()

            miVolunteerAreaID = 0
            msName = String.Empty
            mbActive = True

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miVolunteerAreaID = oDataRow("VolunteerArea_ID")
            msName = oDataRow("Name")
            mbActive = oDataRow("Active")

        End Sub

        ''' <summary>
        ''' The ID of the Volunteer Area
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ID() As Integer
            Get
                Return miVolunteerAreaID
            End Get
        End Property
        ''' <summary>
        ''' The Name of the Volunteer Area
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Name() As String
            Get
                Return msName
            End Get
        End Property
        ''' <summary>
        ''' Whether the Volunteer Area is active or not
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Active() As Boolean
            Get
                Return mbActive
            End Get
        End Property

    End Class

End Namespace