Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactComment

        Private miContactCommentID As Integer
        Private miContactID As Integer
        Private miCommentByID As Integer
        Private mdCommentDate As Date
        Private msComment As String
        Private miCommentTypeID As Integer
        Private miCommentSourceID As Integer
        Private miNPNCID As Integer
        Private miInReplyToCommentID As Integer     'This comment is a PCR Inbox reply to the ID of this comment
        Private msNPNCGUID As String        'Store the GUID of the NPNC record, so we know which NPNC ID to allocate (if necessary)
        Private mbActioned As Boolean
        Private miActionedByID As Integer
        Private mdActionedDate As Date
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miContactCommentID = 0
            miContactID = Nothing
            miCommentByID = 0
            mdCommentDate = Nothing
            msComment = String.Empty
            miCommentTypeID = 0
            miCommentSourceID = 0
            miNPNCID = 0
            miInReplyToCommentID = 0
            msNPNCGUID = String.Empty
            mbActioned = False
            miActionedByID = 0
            mdActionedDate = Nothing
            msGUID = System.Guid.NewGuid.ToString
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContactCommentID = oDataRow("Comment_ID")
            miContactID = oDataRow("Contact_ID")
            miCommentByID = oDataRow("CommentBy_ID")
            mdCommentDate = oDataRow("CommentDate")
            msComment = oDataRow("Comment")
            miCommentTypeID = oDataRow("CommentType_ID")
            miCommentSourceID = oDataRow("CommentSource_ID")
            miNPNCID = IsNull(oDataRow("NPNC_ID"), 0)
            miInReplyToCommentID = IsNull(oDataRow("InReplyToComment_ID"), 0)
            mbActioned = oDataRow("Actioned")
            miActionedByID = IsNull(oDataRow("ActionedBy_ID"), 0)
            mdActionedDate = IsNull(oDataRow("ActionedDate"), Nothing)
            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@CommentBy_ID", SQLNull(miCommentByID)))
                oParameters.Add(New SqlClient.SqlParameter("@CommentDate", SQLDateTime(mdCommentDate)))
                oParameters.Add(New SqlClient.SqlParameter("@Comment", msComment))
                oParameters.Add(New SqlClient.SqlParameter("@CommentType_ID", SQLNull(miCommentTypeID)))
                oParameters.Add(New SqlClient.SqlParameter("@CommentSource_ID", SQLNull(miCommentSourceID)))
                oParameters.Add(New SqlClient.SqlParameter("@NPNC_ID", SQLNull(miNPNCID)))
                oParameters.Add(New SqlClient.SqlParameter("@InReplyToComment_ID", SQLNull(miInReplyToCommentID)))
                oParameters.Add(New SqlClient.SqlParameter("@Actioned", mbActioned))
                oParameters.Add(New SqlClient.SqlParameter("@ActionedBy_ID", SQLNull(miActionedByID)))
                oParameters.Add(New SqlClient.SqlParameter("@ActionedDate", SQLDateTime(mdActionedDate)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miContactCommentID = DataAccess.GetValue("sp_Church_ContactCommentInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@Comment_ID", miContactCommentID))
                    DataAccess.ExecuteCommand("sp_Church_ContactCommentUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub
        Public ReadOnly Property ID() As Integer
            Get
                Return miContactCommentID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                miContactID = value
            End Set
        End Property
        Public Property CommentByID() As Integer
            Get
                Return miCommentByID
            End Get
            Set(ByVal value As Integer)
                If miCommentByID <> value Then mbChanged = True
                miCommentByID = value
            End Set
        End Property
        Public ReadOnly Property CommentBy() As CUser
            Get
                Return Lists.GetUserByID(miCommentByID)
            End Get
        End Property
        Public Property CommentDate() As Date
            Get
                Return mdCommentDate
            End Get
            Set(ByVal value As Date)
                If mdCommentDate <> value Then mbChanged = True
                mdCommentDate = value
            End Set
        End Property
        Public Property Comment() As String
            Get
                Return msComment
            End Get
            Set(ByVal value As String)
                If msComment <> value Then mbChanged = True
                msComment = value
            End Set
        End Property
        Public Property CommentTypeID() As Integer
            Get
                Return miCommentTypeID
            End Get
            Set(ByVal value As Integer)
                If miCommentTypeID <> value Then mbChanged = True
                miCommentTypeID = value
            End Set
        End Property
        Public ReadOnly Property CommentType() As CCommentType
            Get
                Return Lists.Internal.Church.GetCommentTypeByID(miCommentTypeID)
            End Get
        End Property
        Public Property CommentSourceID() As Integer
            Get
                Return miCommentSourceID
            End Get
            Set(ByVal value As Integer)
                If miCommentSourceID <> value Then mbChanged = True
                miCommentSourceID = value
            End Set
        End Property
        Public ReadOnly Property CommentSource() As CCommentSource
            Get
                Return Lists.Internal.Church.GetCommentSourceByID(miCommentSourceID)
            End Get
        End Property
        Public Property NPNCID() As String
            Get
                Return miNPNCID
            End Get
            Set(ByVal value As String)
                If miNPNCID <> value Then mbChanged = True
                miNPNCID = value
            End Set
        End Property
        Public Property NPNCGUID() As String
            Get
                Return msNPNCGUID
            End Get
            Set(ByVal value As String)
                If msNPNCGUID <> value Then mbChanged = True
                msNPNCGUID = value
            End Set
        End Property
        Public Property InReplyToCommentID() As Integer
            Get
                Return miInReplyToCommentID
            End Get
            Set(ByVal value As Integer)
                If miInReplyToCommentID <> value Then mbChanged = True
                miInReplyToCommentID = value
            End Set
        End Property
        Public Property Actioned() As Boolean
            Get
                Return mbActioned
            End Get
            Set(ByVal value As Boolean)
                If mbActioned <> value Then mbChanged = True
                mbActioned = value
            End Set
        End Property
        Public Property ActionedByID() As Integer
            Get
                Return miActionedByID
            End Get
            Set(ByVal value As Integer)
                If miActionedByID <> value Then mbChanged = True
                miActionedByID = value
            End Set
        End Property
        Public ReadOnly Property ActionedBy() As CUser
            Get
                Return Lists.GetUserByID(miActionedByID)
            End Get
        End Property
        Public Property ActionedDate() As Date
            Get
                Return mdActionedDate
            End Get
            Set(ByVal value As Date)
                If mdActionedDate <> value Then mbChanged = True
                mdActionedDate = value
            End Set
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub

    End Class

End Namespace