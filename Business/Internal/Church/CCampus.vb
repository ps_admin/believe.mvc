Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCampus

        Private miCampusID As Integer
        Private msName As String
        Private msShortName As String
        Private miSortOrder As Integer

        Public Sub New(Optional ByVal iCampus As Integer = 0, _
                       Optional ByVal sName As String = "", _
                       Optional ByVal sShortName As String = "", _
                       Optional ByVal iSortOrder As Integer = 0)

            miCampusID = iCampus
            msName = sName
            msShortName = sShortName
            miSortOrder = iSortOrder

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New(oDataRow("Campus_ID"), _
                   oDataRow("Name"), _
                   oDataRow("ShortName"), _
                   oDataRow("SortOrder"))

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miCampusID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property ShortName() As String
            Get
                Return msShortName
            End Get
            Set(ByVal value As String)
                msShortName = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

        Public Overrides Function ToString() As String
            Return Me.Name
        End Function
    End Class

End Namespace