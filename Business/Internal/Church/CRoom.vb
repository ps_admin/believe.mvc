Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRoom

        Private miRoomID As Integer
        Private msName As String
        Private mbRestrictedRoom As Boolean
        Private miSortOrder As Integer
        Private mbDeleted As Boolean

        Private moRoomBookings As CArrayList(Of CRoomBooking)
        Private mbChanged As Boolean

        Public Sub New()

            miRoomID = 0
            msName = String.Empty
            mbRestrictedRoom = False
            miSortOrder = 0
            mbDeleted = False

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRoomID = oDataRow("Room_ID")
            msName = oDataRow("Name")
            mbRestrictedRoom = oDataRow("RestrictedRoom")
            miSortOrder = oDataRow("SortOrder")
            mbDeleted = oDataRow("Deleted")
            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
                oParameters.Add(New SqlClient.SqlParameter("@RestrictedRoom", mbRestrictedRoom))
                oParameters.Add(New SqlClient.SqlParameter("@SortOrder", miSortOrder))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", mbDeleted))

                If Me.ID = 0 Then
                    miRoomID = DataAccess.GetValue("sp_Church_RoomInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@Room_ID", miRoomID))
                    DataAccess.ExecuteCommand("sp_Church_RoomUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property Bookings() As CArrayList(Of CRoomBooking)
            Get

                If moRoomBookings Is Nothing Then

                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_RoomBooking WHERE Room_ID = " & miRoomID.ToString & " AND Deleted = 0")
                    Dim oDataRow As DataRow
                    moRoomBookings = New CArrayList(Of CRoomBooking)

                    For Each oDataRow In oDataTable.Rows
                        moRoomBookings.Add(New CRoomBooking(oDataRow))
                    Next
                End If

                Return moRoomBookings
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miRoomID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                If msName <> value Then mbChanged = True
                msName = value
            End Set
        End Property
        Public Property RestrictedRoom() As Boolean
            Get
                Return mbRestrictedRoom
            End Get
            Set(ByVal value As Boolean)
                If mbRestrictedRoom <> value Then mbChanged = True
                mbRestrictedRoom = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                If miSortOrder <> value Then mbChanged = True
                miSortOrder = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                If mbDeleted <> value Then mbChanged = True
                mbDeleted = value
            End Set
        End Property

    End Class

End Namespace