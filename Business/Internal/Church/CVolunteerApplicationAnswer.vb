﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CVolunteerApplicationAnswer

        'Private Variables
        Private miAnswerID As Integer
        Private miApplicationID As Integer
        Private miQuestionID As Integer
        Private msAnswer As String

        'Initialise
        Public Sub New()

            miAnswerID = 0
            miApplicationID = 0
            miQuestionID = 0
            msAnswer = String.Empty

        End Sub
        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()

            miAnswerID = oDataRow("Answer_ID")
            miApplicationID = oDataRow("Application_ID")
            miQuestionID = oDataRow("Question_ID")
            msAnswer = oDataRow("Answer")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miAnswerID
            End Get
        End Property
        Public ReadOnly Property ApplicationID() As Integer
            Get
                Return miApplicationID
            End Get
        End Property
        Public ReadOnly Property Application As CVolunteerApplication
            Get
                Return Lists.Internal.Church.GetVolunteerApplicationByID(miApplicationID)
            End Get
        End Property
        Public ReadOnly Property QuestionID() As Integer
            Get
                Return miQuestionID
            End Get
        End Property
        Public ReadOnly Property Question() As CVolunteerApplicationQuestion
            Get
                Return Lists.Internal.Church.getVolunteerQuestionByID(miQuestionID)
            End Get
        End Property
        Public ReadOnly Property Answer() As String
            Get
                Return msAnswer
            End Get
        End Property
    End Class

End Namespace

