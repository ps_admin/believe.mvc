Namespace Internal.Church

    <Serializable()>
    <CLSCompliant(True)>
    Public Class CRoleCategory

        Private miRoleCategoryID As Integer
        Private msName As String
        Private mbChanged As Boolean

        Public Sub New()

            miRoleCategoryID = 0
            msName = String.Empty
        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRoleCategoryID = oDataRow("RoleCategory_ID")
            msName = oDataRow("Name")
        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miRoleCategoryID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                If msName <> value Then mbChanged = True
                msName = value
            End Set
        End Property

    End Class

End Namespace
