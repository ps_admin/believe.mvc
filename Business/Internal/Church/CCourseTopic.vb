Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCourseTopic
        Implements IComparable(Of CCourseTopic)

        Private miCourseTopicID As Integer
        Private miCourseID As Integer
        Private msName As String
        Private miPointWeighting As Integer
        Private mbDeleted As Boolean

        Private mbChanged As Boolean

        Public Sub New()

            miCourseTopicID = 0
            miCourseID = 0
            msName = String.Empty
            miPointWeighting = 0
            mbDeleted = False

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCourseTopicID = oDataRow("CourseTopic_ID")
            miCourseID = oDataRow("Course_ID")
            msName = oDataRow("Name")
            miPointWeighting = oDataRow("PointWeighting")
            mbDeleted = oDataRow("Deleted")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Course_ID", miCourseID))
                oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
                oParameters.Add(New SqlClient.SqlParameter("@PointWeighting", miPointWeighting))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", mbDeleted))

                If Me.ID = 0 Then
                    miCourseTopicID = DataAccess.GetValue("sp_Church_CourseTopicInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@CourseTopic_ID", miCourseTopicID))
                    DataAccess.ExecuteCommand("sp_Church_CourseTopicUpdate", oParameters)
                End If

                mbChanged = False

                'Update topic in the Global List
                Lists.Internal.Church.CourseTopic(Me.ID) = Me

            End If


        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miCourseTopicID
            End Get
        End Property
        Public Property CourseID() As Integer
            Get
                Return miCourseID
            End Get
            Set(ByVal value As Integer)
                If miCourseID <> value Then mbChanged = True
                miCourseID = value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                If msName <> value Then mbChanged = True
                msName = value
            End Set
        End Property
        Public Property PointWeighting() As Integer
            Get
                Return miPointWeighting
            End Get
            Set(ByVal value As Integer)
                If miPointWeighting <> value Then mbChanged = True
                miPointWeighting = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                If mbDeleted <> value Then mbChanged = True
                mbDeleted = value
            End Set
        End Property

        Public Function CompareTo(ByVal other As CCourseTopic) As Integer Implements System.IComparable(Of CCourseTopic).CompareTo

            Return String.Compare(Me.Name, other.Name)

        End Function

    End Class

End Namespace