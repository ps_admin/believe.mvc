Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CChurchStatusChangeRequest

        Private miStatusChangeRequestID As Integer
        Private miContactID As Integer
        Private moContact As CContact
        Private miChurchStatusID As Integer
        Private miDeleteReasonID As Integer
        Private miCurrentCampusID As Integer
        Private miCurrentMinistryID As Integer
        Private miCurrentRegionID As Integer
        Private miNPNCID As Integer
        Private msNPNCGUID As String
        Private miCampusID As Integer
        Private miMinistryID As Integer
        Private miRegionID As Integer
        Private miULGID As Integer
        Private msComments As String
        Private mdDateRequested As Date
        Private miRequestedByID As Integer
        Private mbActioned As Boolean
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miStatusChangeRequestID = 0
            miContactID = 0
            moContact = Nothing
            miChurchStatusID = 0
            miDeleteReasonID = 0
            miCurrentCampusID = 0
            miCurrentMinistryID = 0
            miCurrentRegionID = 0
            miNPNCID = 0
            msNPNCGUID = String.Empty
            miCampusID = 0
            miMinistryID = 0
            miRegionID = 0
            miULGID = 0
            msComments = String.Empty
            mdDateRequested = Nothing
            miRequestedByID = 0
            mbActioned = False
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miStatusChangeRequestID = oDataRow("StatusChangeRequest_ID")
            miContactID = oDataRow("Contact_ID")
            miChurchStatusID = oDataRow("ChurchStatus_ID")
            miDeleteReasonID = IsNull(oDataRow("DeleteReason_ID"), 0)
            miCurrentCampusID = IsNull(oDataRow("CurrentCampus_ID"), 0)
            miCurrentMinistryID = IsNull(oDataRow("CurrentMinistry_ID"), 0)
            miCurrentRegionID = IsNull(oDataRow("CurrentRegion_ID"), 0)
            miNPNCID = IsNull(oDataRow("NPNC_ID"), 0)
            miCampusID = IsNull(oDataRow("Campus_ID"), 0)
            miMinistryID = IsNull(oDataRow("Ministry_ID"), 0)
            miRegionID = IsNull(oDataRow("Region_ID"), 0)
            miULGID = IsNull(oDataRow("ULG_ID"), 0)
            msComments = oDataRow("Comments")
            mdDateRequested = oDataRow("DateRequested")
            miRequestedByID = oDataRow("RequestedBy_ID")
            mbActioned = oDataRow("Actioned")
            msGUID = oDataRow("GUID").ToString

            mbChanged = False

        End Sub

        Public Sub New(ByVal iStatusChangeRequest As Integer)

            Me.New(DataAccess.GetDataRow("sp_Church_ChurchStatusChangeRequestRead " & iStatusChangeRequest.ToString))

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchStatus_ID", SQLNull(miChurchStatusID)))
                oParameters.Add(New SqlClient.SqlParameter("@DeleteReason_ID", SQLNull(miDeleteReasonID)))
                oParameters.Add(New SqlClient.SqlParameter("@CurrentCampus_ID", SQLNull(miCurrentCampusID)))
                oParameters.Add(New SqlClient.SqlParameter("@CurrentMinistry_ID", SQLNull(miCurrentMinistryID)))
                oParameters.Add(New SqlClient.SqlParameter("@CurrentRegion_ID", SQLNull(miCurrentRegionID)))
                oParameters.Add(New SqlClient.SqlParameter("@NPNC_ID", SQLNull(miNPNCID)))
                oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLNull(miCampusID)))
                oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLNull(miMinistryID)))
                oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLNull(miRegionID)))
                oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLNull(miULGID)))
                oParameters.Add(New SqlClient.SqlParameter("@Comments", msComments))
                oParameters.Add(New SqlClient.SqlParameter("@DateRequested", SQLDateTime(mdDateRequested)))
                oParameters.Add(New SqlClient.SqlParameter("@RequestedBy_ID", SQLNull(miRequestedByID)))
                oParameters.Add(New SqlClient.SqlParameter("@Actioned", mbActioned))
                oParameters.Add(New SqlClient.SqlParameter("@GUID", msGUID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miStatusChangeRequestID = DataAccess.GetValue("sp_Church_ChurchStatusChangeRequestInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@StatusChangeRequest_ID", miStatusChangeRequestID))
                    DataAccess.ExecuteCommand("sp_Church_ChurchStatusChangeRequestUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miStatusChangeRequestID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public ReadOnly Property Contact() As CContact
            Get
                If moContact Is Nothing And miContactID > 0 Then
                    moContact = New CContact
                    moContact.LoadByID(miContactID)
                End If
                Return moContact
            End Get
        End Property
        Public Property ChurchStatusID() As Integer
            Get
                Return miChurchStatusID
            End Get
            Set(ByVal value As Integer)
                If miChurchStatusID <> value Then mbChanged = True
                miChurchStatusID = value
            End Set
        End Property
        Public ReadOnly Property ChurchStatus() As CChurchStatus
            Get
                Return Lists.Internal.Church.GetChurchStatusByID(miChurchStatusID)
            End Get
        End Property
        Public Property DeleteReasonID() As Integer
            Get
                Return miDeleteReasonID
            End Get
            Set(ByVal value As Integer)
                If miDeleteReasonID <> value Then mbChanged = True
                miDeleteReasonID = value
            End Set
        End Property
        Public ReadOnly Property DeleteReason() As CDeleteReason
            Get
                Return Lists.Internal.Church.GetDeleteReasonByID(miDeleteReasonID)
            End Get
        End Property
        Public Property CurrentCampusID() As Integer
            Get
                Return miCurrentCampusID
            End Get
            Set(ByVal value As Integer)
                If miCurrentCampusID <> value Then mbChanged = True
                miCurrentCampusID = value
            End Set
        End Property
        Public ReadOnly Property CurrentCampus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCurrentCampusID)
            End Get
        End Property
        Public Property CurrentMinistryID() As Integer
            Get
                Return miCurrentMinistryID
            End Get
            Set(ByVal value As Integer)
                If miCurrentMinistryID <> value Then mbChanged = True
                miCurrentMinistryID = value
            End Set
        End Property
        Public ReadOnly Property CurrentMinistry() As CMinistry
            Get
                Return Lists.Internal.Church.GetMinistryByID(miCurrentMinistryID)
            End Get
        End Property
        Public Property CurrentRegionID() As Integer
            Get
                Return miCurrentRegionID
            End Get
            Set(ByVal value As Integer)
                If miCurrentRegionID <> value Then mbChanged = True
                miCurrentRegionID = value
            End Set
        End Property
        Public ReadOnly Property CurrentRegion() As CRegion
            Get
                Return Lists.Internal.Church.GetRegionByID(miCurrentRegionID)
            End Get
        End Property
        Public Property NPNCID() As Integer
            Get
                Return miNPNCID
            End Get
            Set(ByVal value As Integer)
                If miNPNCID <> value Then mbChanged = True
                miNPNCID = value
            End Set
        End Property
        Public Property NPNCGUID() As String
            Get
                Return msNPNCGUID
            End Get
            Set(ByVal value As String)
                If msNPNCGUID <> value Then mbChanged = True
                msNPNCGUID = value
            End Set
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal value As Integer)
                If miCampusID <> value Then mbChanged = True
                miCampusID = value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public Property MinistryID() As Integer
            Get
                Return miMinistryID
            End Get
            Set(ByVal value As Integer)
                If miMinistryID <> value Then mbChanged = True
                miMinistryID = value
            End Set
        End Property
        Public ReadOnly Property Ministry() As CMinistry
            Get
                Return Lists.Internal.Church.GetMinistryByID(miMinistryID)
            End Get
        End Property
        Public Property RegionID() As Integer
            Get
                Return miRegionID
            End Get
            Set(ByVal value As Integer)
                If miRegionID <> value Then mbChanged = True
                miRegionID = value
            End Set
        End Property
        Public ReadOnly Property Region() As CRegion
            Get
                Return Lists.Internal.Church.GetRegionByID(miRegionID)
            End Get
        End Property
        Public Property ULGID() As Integer
            Get
                Return miULGID
            End Get
            Set(ByVal value As Integer)
                If miULGID <> value Then mbChanged = True
                miULGID = value
            End Set
        End Property
        Public ReadOnly Property ULG() As CULG
            Get
                Return Lists.Internal.Church.GetULGByID(miULGID)
            End Get
        End Property
        Public Property Comments() As String
            Get
                Return msComments
            End Get
            Set(ByVal value As String)
                msComments = value
            End Set
        End Property
        Public Property DateRequested() As Date
            Get
                Return mdDateRequested
            End Get
            Set(ByVal value As Date)
                If mdDateRequested <> value Then mbChanged = True
                mdDateRequested = value
            End Set
        End Property
        Public Property RequestedByID() As Integer
            Get
                Return miRequestedByID
            End Get
            Set(ByVal value As Integer)
                If miRequestedByID <> value Then mbChanged = True
                miRequestedByID = value
            End Set
        End Property
        Public ReadOnly Property RequestedBy() As CUser
            Get
                Return Lists.GetUserByID(miRequestedByID)
            End Get
        End Property
        Public Property Actioned() As Boolean
            Get
                Return mbActioned
            End Get
            Set(ByVal value As Boolean)
                If mbActioned <> value Then mbChanged = True
                mbActioned = value
            End Set
        End Property
        Public Property GUID() As String
            Get
                Return msGUID
            End Get
            Set(ByVal value As String)
                If msGUID <> value Then mbChanged = True
                msGUID = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property

    End Class

End Namespace