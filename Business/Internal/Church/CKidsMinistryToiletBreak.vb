Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CKidsMinistryToiletBreak

        Private miKidsMinistryToiletBreakID As Integer
        Private miContactID As Integer
        Private moContact As CContact
        Private miLeader1ID As Integer
        Private moLeader1 As CContact
        Private miLeader2ID As Integer
        Private moLeader2 As CContact
        Private miChurchServiceID As Integer
        Private mdTimeTaken As Date
        Private mdTimeReturned As Date
        Private msComments As String
        Private mbChanged As Boolean
        Private msGUID As String

        Public Sub New()

            miKidsMinistryToiletBreakID = 0
            miContactID = 0
            miLeader1ID = 0
            miLeader2ID = 0
            miChurchServiceID = 0
            mdTimeTaken = Nothing
            mdTimeReturned = Nothing
            msComments = String.Empty
            mbChanged = False
            msGUID = System.Guid.NewGuid.ToString

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()
            LoadDataRow(oDataRow)

        End Sub

        Public Sub LoadDataRow(ByRef oDataRow As DataRow)

            miKidsMinistryToiletBreakID = oDataRow("KidsMinistryToiletBreak_ID")
            miContactID = oDataRow("Contact_ID")
            miLeader1ID = oDataRow("Leader1_ID")
            miLeader2ID = oDataRow("Leader2_ID")
            miChurchServiceID = oDataRow("ChurchService_ID")
            mdTimeTaken = oDataRow("TimeTAken")
            mdTimeReturned = IsNull(oDataRow("TimeReturned"), Nothing)
            msComments = oDataRow("Comments")

        End Sub

        Public Sub LoadByID(ByVal iToiletBreakID As Integer)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryToiletBreak_ID", iToiletBreakID))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_KidsMinistryToiletBreakRead", oParameters)

            If oDataTable.Rows.Count > 0 Then
                LoadDataRow(oDataTable.Rows(0))
            End If

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Leader1_ID", miLeader1ID))
                oParameters.Add(New SqlClient.SqlParameter("@Leader2_ID", miLeader2ID))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", miChurchServiceID))
                oParameters.Add(New SqlClient.SqlParameter("@TimeTaken", SQLDateTime(mdTimeTaken)))
                oParameters.Add(New SqlClient.SqlParameter("@TimeReturned", SQLDateTime(mdTimeReturned)))
                oParameters.Add(New SqlClient.SqlParameter("@Comments", msComments))

                If Me.ID = 0 Then
                    miKidsMinistryToiletBreakID = DataAccess.GetValue("sp_Church_KidsMinistryToiletBreakInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryToiletBreak_ID", miKidsMinistryToiletBreakID))
                    DataAccess.ExecuteCommand("sp_Church_KidsMinistryToiletBreakUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miKidsMinistryToiletBreakID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If value <> miContactID Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public ReadOnly Property Contact() As CContact
            Get
                If moContact Is Nothing Then
                    moContact = New CContact
                    moContact.LoadByID(miContactID)
                End If
                Return moContact
            End Get
        End Property
        Public Property Leader1ID() As Integer
            Get
                Return miLeader1ID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miLeader1ID Then mbChanged = True
                miLeader1ID = Value
            End Set
        End Property
        Public ReadOnly Property Leader1() As CContact
            Get
                If moLeader1 Is Nothing Then
                    moLeader1 = New CContact
                    moLeader1.LoadByID(miLeader1ID)
                End If
                Return moLeader1
            End Get
        End Property
        Public Property Leader2ID() As Integer
            Get
                Return miLeader2ID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miLeader2ID Then mbChanged = True
                miLeader2ID = Value
            End Set
        End Property
        Public ReadOnly Property Leader2() As CContact
            Get
                If moLeader2 Is Nothing Then
                    moLeader2 = New CContact
                    moLeader2.LoadByID(miLeader2ID)
                End If
                Return moLeader2
            End Get
        End Property
        Public Property ChurchServiceID() As Integer
            Get
                Return miChurchServiceID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miChurchServiceID Then mbChanged = True
                miChurchServiceID = Value
            End Set
        End Property
        Public ReadOnly Property ChurchService() As CChurchService
            Get
                Return Lists.Internal.Church.GetChurchServiceByID(miChurchServiceID)
            End Get
        End Property
        Public Property TimeTaken() As Date
            Get
                Return mdTimeTaken
            End Get
            Set(ByVal Value As Date)
                If Value <> mdTimeTaken Then mbChanged = True
                mdTimeTaken = Value
            End Set
        End Property
        Public Property TimeReturned() As Date
            Get
                Return mdTimeReturned
            End Get
            Set(ByVal Value As Date)
                If Value <> mdTimeReturned Then mbChanged = True
                mdTimeReturned = Value
            End Set
        End Property
        Public Property Comments() As String
            Get
                Return msComments
            End Get
            Set(ByVal value As String)
                If value <> msComments Then mbChanged = True
                msComments = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace