Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class C247PrayerDuration

        Private miDurationID As Integer
        Private msName As String
        Private mdDuration As Decimal
        Private miSortOrder As Integer

        Public Sub New()

            miDurationID = 0
            msName = String.Empty
            mdDuration = 0
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miDurationID = oDataRow("Duration_ID")
            msName = oDataRow("Name")
            mdDuration = oDataRow("Duration")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miDurationID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property Duration() As Decimal
            Get
                Return mdDuration
            End Get
            Set(ByVal value As Decimal)
                mdDuration = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace