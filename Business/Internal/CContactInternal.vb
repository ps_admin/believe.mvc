Imports Planetshakers.Business.Internal.Church

Namespace Internal

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactInternal

        Private miContactID As Integer
        Private msBarcode As String

        Private miChurchStatusID As Integer
        Private moChurchStatusChangeRequest As CChurchStatusChangeRequest
        Private mbHadChurchStatusChangeRequest As Boolean
        Private miCampusID As Integer
        Private miMinistryID As Integer
        Private miRegionID As Integer
        Private msPostalAddress1 As String
        Private msPostalAddress2 As String
        Private msPostalSuburb As String
        Private msPostalPostcode As String
        Private miPostalStateID As Integer
        Private msPostalStateOther As String
        Private miPostalCountryID As Integer
        Private msPhoto As Byte()
        Private miEntryPointID As Integer
        Private mdCovenantFormDate As Date
        Private mdWaterBaptismDate As Date
        Private mdVolunteerFormDate As Date
        Private msPrimaryCarer As String
        Private miCountryOfOriginID As Integer
        Private msPrivacyNumber As String
        Private msPrimarySchool As String
        Private miSchoolGradeID As Integer
        Private msHighSchool As String
        Private msUniversity As String
        Private mbInformationIsConfidential As Boolean
        Private msFacebookAddress As String
        Private miEducationID As Integer
        Private miModeOfTransportID As Integer
        Private miCarParkUsedID As Integer
        Private miServiceAttendingID As Integer
        Private msOccupation As String
        Private msEmployer As String
        Private miEmploymentStatusID As Integer
        Private miEmploymentPositionID As Integer
        Private miEmploymentIndustryID As Integer
        Private msKidsMinistryComments As String
        Private msKidsMinistryGroup As String
        Private moDeleteReason As Object

        Private mcNPNC As CArrayList(Of CNPNC)
        Private mcULG As CArrayList(Of CULGContact)
        Private mcAdditionalDecision As CArrayList(Of CContactAdditionalDecision)
        Private mcContactRole As CArrayList(Of CContactRole)
        Private mcCourseInstance As CArrayList(Of CContactCourseInstance)
        Private mcCourseEnrolment As CArrayList(Of CContactCourseEnrolment)
        Private mcCourseEnrolmentAttendance As CArrayList(Of CCourseEnrolmentAttendance)
        Private mcComment As CArrayList(Of CContactComment)
        Private mc247Prayer As CArrayList(Of C247Prayer)
        Private mcKidsMinistryCode As CArrayList(Of CContactKidsMinistryCode)
        Private mcKidsMinistryClearance As CArrayList(Of CKidsMinistryClearance)

        Private mbChanged As Boolean
        Private mbPostalAddressChanged As Boolean

        Public Sub New()

            miContactID = 0
            msBarcode = String.Empty
            miChurchStatusID = 0
            moChurchStatusChangeRequest = Nothing
            mbHadChurchStatusChangeRequest = False
            miCampusID = 0
            miMinistryID = 0
            miRegionID = 0
            msPostalAddress1 = String.Empty
            msPostalAddress2 = String.Empty
            msPostalSuburb = String.Empty
            msPostalPostcode = String.Empty
            miPostalStateID = 0
            msPostalStateOther = String.Empty
            miPostalCountryID = 0
            msPhoto = Nothing
            miEntryPointID = 0
            mdCovenantFormDate = Nothing
            mdWaterBaptismDate = Nothing
            mdVolunteerFormDate = Nothing
            msPrimaryCarer = String.Empty
            miCountryOfOriginID = 0
            msPrivacyNumber = String.Empty
            msPrimarySchool = String.Empty
            miSchoolGradeID = Nothing
            msHighSchool = String.Empty
            msUniversity = String.Empty
            mbInformationIsConfidential = False
            msFacebookAddress = String.Empty
            miEducationID = 0
            miModeOfTransportID = 0
            miCarParkUsedID = 0
            miServiceAttendingID = 0
            msOccupation = String.Empty
            msEmployer = String.Empty
            miEmploymentStatusID = 0
            miEmploymentPositionID = 0
            miEmploymentIndustryID = 0
            msKidsMinistryComments = String.Empty
            msKidsMinistryGroup = String.Empty
            moDeleteReason = Nothing
            mcNPNC = New CArrayList(Of CNPNC)
            mcULG = New CArrayList(Of CULGContact)
            mcAdditionalDecision = New CArrayList(Of CContactAdditionalDecision)
            mcContactRole = New CArrayList(Of CContactRole)
            mcCourseInstance = New CArrayList(Of CContactCourseInstance)
            mcCourseEnrolment = New CArrayList(Of CContactCourseEnrolment)
            mcCourseEnrolmentAttendance = New CArrayList(Of CCourseEnrolmentAttendance)
            mcComment = New CArrayList(Of CContactComment)
            mc247Prayer = New CArrayList(Of C247Prayer)
            mcKidsMinistryCode = New CArrayList(Of CContactKidsMinistryCode)
            mcKidsMinistryClearance = New CArrayList(Of CKidsMinistryClearance)

            mbChanged = False
            mbPostalAddressChanged = False

        End Sub

        Public Function LoadID(ByVal iContactID As Integer, ByVal oContact As CContact) As Boolean

            Dim oDataSet As DataSet

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "ContactInternal"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "ChurchStatusChangeRequest"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table2", "NPNC"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table3", "ULGContact"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table4", "AdditionalDecision"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table5", "ContactRole"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table6", "RestrictedAccessRole"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table7", "RestrictedAccessULG"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table8", "CourseInstance"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table9", "CourseEnrolment"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table10", "CourseEnrolmentAttendance"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table11", "CourseEnrolmentAttendanceTopicsCovered"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table12", "ContactComment"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table13", "247Prayer"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table14", "ContactKidsMinistryCode"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table15", "KidsMinistryClearance"))

            oDataSet = DataAccess.GetDataSet("sp_Common_ContactInternalRead2 " & iContactID.ToString, oTableMappings)

            oDataSet.Relations.Add("ChurchStatusChangeRequest", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("ChurchStatusChangeRequest").Columns("Contact_ID"))

            oDataSet.Relations.Add("NPNC", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("NPNC").Columns("Contact_ID"))

            oDataSet.Relations.Add("ULGContact", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("ULGContact").Columns("Contact_ID"))

            oDataSet.Relations.Add("AdditionalDecision", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("AdditionalDecision").Columns("Contact_ID"))

            oDataSet.Relations.Add("Role", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("ContactRole").Columns("Contact_ID"))

            oDataSet.Relations.Add("RestrictedAccessRole", oDataSet.Tables("ContactRole").Columns("ContactRole_ID"), _
                                    oDataSet.Tables("RestrictedAccessRole").Columns("ContactRole_ID"))

            oDataSet.Relations.Add("RestrictedAccessULG", oDataSet.Tables("ContactRole").Columns("ContactRole_ID"), _
                                    oDataSet.Tables("RestrictedAccessULG").Columns("ContactRole_ID"))

            oDataSet.Relations.Add("CourseInstance", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("CourseInstance").Columns("Contact_ID"))

            oDataSet.Relations.Add("CourseEnrolment", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("CourseEnrolment").Columns("Contact_ID"))

            oDataSet.Relations.Add("CourseEnrolmentAttendance", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("CourseEnrolmentAttendance").Columns("Contact_ID"))

            oDataSet.Relations.Add("CourseEnrolmentAttendanceTopicsCovered", oDataSet.Tables("CourseEnrolmentAttendance").Columns("CourseEnrolmentAttendance_ID"), _
                                    oDataSet.Tables("CourseEnrolmentAttendanceTopicsCovered").Columns("CourseEnrolmentAttendance_ID"))

            oDataSet.Relations.Add("ContactComment", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("ContactComment").Columns("Contact_ID"))

            oDataSet.Relations.Add("247Prayer", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("247Prayer").Columns("Contact_ID"))

            oDataSet.Relations.Add("ContactKidsMinistryCode", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("ContactKidsMinistryCode").Columns("Contact_ID"))

            oDataSet.Relations.Add("KidsMinistryClearance", oDataSet.Tables("ContactInternal").Columns("Contact_ID"), _
                                    oDataSet.Tables("KidsMinistryClearance").Columns("Contact_ID"))

            miContactID = iContactID

            If oDataSet.Tables(0).Rows.Count = 0 Then
                Return False
            Else
                Return LoadDataRow(oDataSet.Tables(0).Rows(0))
            End If

        End Function

        Public Function LoadDataRow(ByRef oDataRow As DataRow) As Boolean

            miContactID = oDataRow("Contact_ID")
            msBarcode = IsNull(oDataRow("Barcode"), String.Empty)
            miChurchStatusID = oDataRow("ChurchStatus_ID")
            miCampusID = IsNull(oDataRow("Campus_ID"), 0)
            miMinistryID = IsNull(oDataRow("Ministry_ID"), 0)
            miRegionID = IsNull(oDataRow("Region_ID"), 0)
            msPostalAddress1 = oDataRow("PostalAddress1")
            msPostalAddress2 = oDataRow("PostalAddress2")
            msPostalSuburb = oDataRow("PostalSuburb")
            msPostalPostcode = oDataRow("PostalPostcode")
            miPostalStateID = IsNull(oDataRow("PostalState_ID"), 0)
            msPostalStateOther = oDataRow("PostalStateOther")
            miPostalCountryID = IsNull(oDataRow("PostalCountry_ID"), 0)
            msPhoto = IsNull(oDataRow("Photo"), Nothing)
            miEntryPointID = IsNull(oDataRow("EntryPoint_ID"), 0)
            mdCovenantFormDate = IsNull(oDataRow("CovenantFormDate"), Nothing)
            mdWaterBaptismDate = IsNull(oDataRow("WaterBaptismDate"), Nothing)
            mdVolunteerFormDate = IsNull(oDataRow("VolunteerFormDate"), Nothing)
            msPrimaryCarer = oDataRow("PrimaryCarer")
            miCountryOfOriginID = IsNull(oDataRow("CountryOfOrigin_ID"), 0)
            msPrivacyNumber = oDataRow("PrivacyNumber")
            msPrimarySchool = oDataRow("PrimarySchool")
            miSchoolGradeID = IsNull(oDataRow("SchoolGrade_ID"), 0)
            msHighSchool = oDataRow("HighSchool")
            msUniversity = oDataRow("University")
            mbInformationIsConfidential = oDataRow("InformationIsConfidential")
            msFacebookAddress = oDataRow("FacebookAddress")
            miEducationID = IsNull(oDataRow("Education_ID"), 0)
            miModeOfTransportID = IsNull(oDataRow("ModeOfTransport_ID"), 0)
            miCarParkUsedID = IsNull(oDataRow("CarParkUsed_ID"), 0)
            miServiceAttendingID = IsNull(oDataRow("ServiceAttending_ID"), 0)
            msOccupation = oDataRow("Occupation")
            msEmployer = oDataRow("Employer")
            miEmploymentStatusID = IsNull(oDataRow("EmploymentStatus_ID"), 0)
            miEmploymentPositionID = IsNull(oDataRow("EmploymentPosition_ID"), 0)
            miEmploymentIndustryID = IsNull(oDataRow("EmploymentIndustry_ID"), 0)
            msKidsMinistryComments = oDataRow("KidsMinistryComments")
            msKidsMinistryGroup = oDataRow("KidsMinistryGroup")
            moDeleteReason = Nothing
            mbChanged = False
            mbPostalAddressChanged = False

            If oDataRow.GetChildRows("ChurchStatusChangeRequest").Length > 0 Then
                moChurchStatusChangeRequest = New CChurchStatusChangeRequest(oDataRow.GetChildRows("ChurchStatusChangeRequest")(0))
                mbHadChurchStatusChangeRequest = True
            End If

            Dim oNPNCRow As DataRow
            For Each oNPNCRow In oDataRow.GetChildRows("NPNC")
                mcNPNC.Add(New CNPNC(oNPNCRow))
            Next

            Dim oULGRow As DataRow
            For Each oULGRow In oDataRow.GetChildRows("ULGContact")
                mcULG.Add(New CULGContact(oULGRow))
            Next

            Dim oAdditionalDecisionRow As DataRow
            For Each oAdditionalDecisionRow In oDataRow.GetChildRows("AdditionalDecision")
                mcAdditionalDecision.Add(New CContactAdditionalDecision(oAdditionalDecisionRow))
            Next

            Dim oRoleRow As DataRow
            For Each oRoleRow In oDataRow.GetChildRows("Role")
                mcContactRole.Add(New CContactRole(oRoleRow))
            Next

            For Each oCourseInstanceRow As DataRow In oDataRow.GetChildRows("CourseInstance")
                mcCourseInstance.Add(New CContactCourseInstance(oCourseInstanceRow))
            Next

            For Each oCourseEnrolmentRow As DataRow In oDataRow.GetChildRows("CourseEnrolment")
                mcCourseEnrolment.Add(New CContactCourseEnrolment(oCourseEnrolmentRow))
            Next

            For Each oCourseEnrolmentAttendanceRow As DataRow In oDataRow.GetChildRows("CourseEnrolmentAttendance")
                mcCourseEnrolmentAttendance.Add(New CCourseEnrolmentAttendance(oCourseEnrolmentAttendanceRow))
            Next

            For Each oCommentRow As DataRow In oDataRow.GetChildRows("ContactComment")
                mcComment.Add(New CContactComment(oCommentRow))
            Next

            For Each o247PrayerRow As DataRow In oDataRow.GetChildRows("247Prayer")
                mc247Prayer.Add(New C247Prayer(o247PrayerRow))
            Next

            For Each oKidsMinistryCodeRow As DataRow In oDataRow.GetChildRows("ContactKidsMinistryCode")
                mcKidsMinistryCode.Add(New CContactKidsMinistryCode(oKidsMinistryCodeRow))
            Next

            For Each oKidsMinistryClearanceRow As DataRow In oDataRow.GetChildRows("KidsMinistryClearance")
                mcKidsMinistryClearance.Add(New CKidsMinistryClearance(oKidsMinistryClearanceRow))
            Next

            mcNPNC.ResetChanges()
            mcULG.ResetChanges()
            mcAdditionalDecision.ResetChanges()
            mcContactRole.ResetChanges()
            mcCourseInstance.ResetChanges()
            mcCourseEnrolment.ResetChanges()
            mcCourseEnrolmentAttendance.ResetChanges()
            mcComment.ResetChanges()
            mc247Prayer.ResetChanges()
            mcKidsMinistryCode.ResetChanges()
            mcKidsMinistryClearance.ResetChanges()

            Return True

        End Function

        Public Sub Save(ByVal oContact As CContact, ByVal oUser As CUser)

            Dim oParameters As New ArrayList

            'If this contact is inactive, make sure their campus & ministry/region is removed
            If miChurchStatusID = EnumChurchStatus.Inactive Then
                CampusID = 0
                MinistryID = 0
                RegionID = 0
                mc247Prayer.Clear()  'Also remove their 24/7 prayer entries
            End If

            'If this contact is mailing list, make sure their ministry/region is removed
            If miChurchStatusID = EnumChurchStatus.MailingList Then
                MinistryID = 0
                RegionID = 0
            End If

            If mbChanged Then

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Barcode", msBarcode))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchStatus_ID", SQLNull(miChurchStatusID)))
                oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLNull(miCampusID)))
                oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLNull(miMinistryID)))
                oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLNull(miRegionID)))
                oParameters.Add(New SqlClient.SqlParameter("@PostalAddress1", msPostalAddress1))
                oParameters.Add(New SqlClient.SqlParameter("@PostalAddress2", msPostalAddress2))
                oParameters.Add(New SqlClient.SqlParameter("@PostalSuburb", msPostalSuburb))
                oParameters.Add(New SqlClient.SqlParameter("@PostalPostcode", msPostalPostcode))
                oParameters.Add(New SqlClient.SqlParameter("@PostalState_ID", SQLNull(miPostalStateID)))
                oParameters.Add(New SqlClient.SqlParameter("@PostalStateOther", msPostalStateOther))
                oParameters.Add(New SqlClient.SqlParameter("@PostalCountry_ID", SQLNull(miPostalCountryID)))
                oParameters.Add(New SqlClient.SqlParameter("@Photo", msPhoto))
                oParameters.Add(New SqlClient.SqlParameter("@EntryPoint_ID", SQLNull(miEntryPointID)))
                oParameters.Add(New SqlClient.SqlParameter("@CovenantFormDate", SQLDate(mdCovenantFormDate)))
                oParameters.Add(New SqlClient.SqlParameter("@WaterBaptismDate", SQLDate(mdWaterBaptismDate)))
                oParameters.Add(New SqlClient.SqlParameter("@VolunteerFormDate", SQLDate(mdVolunteerFormDate)))
                oParameters.Add(New SqlClient.SqlParameter("@PrimaryCarer", msPrimaryCarer))
                oParameters.Add(New SqlClient.SqlParameter("@CountryOfOrigin_ID", SQLNull(miCountryOfOriginID)))
                oParameters.Add(New SqlClient.SqlParameter("@PrivacyNumber", msPrivacyNumber))
                oParameters.Add(New SqlClient.SqlParameter("@PrimarySchool", msPrimarySchool))
                oParameters.Add(New SqlClient.SqlParameter("@SchoolGrade_ID", SQLNull(miSchoolGradeID)))
                oParameters.Add(New SqlClient.SqlParameter("@HighSchool", msHighSchool))
                oParameters.Add(New SqlClient.SqlParameter("@University", msUniversity))
                oParameters.Add(New SqlClient.SqlParameter("@InformationIsConfidential", mbInformationIsConfidential))
                oParameters.Add(New SqlClient.SqlParameter("@FacebookAddress", msFacebookAddress))
                oParameters.Add(New SqlClient.SqlParameter("@Education_ID", SQLNull(miEducationID)))
                oParameters.Add(New SqlClient.SqlParameter("@ModeOfTransport_ID", SQLNull(miModeOfTransportID)))
                oParameters.Add(New SqlClient.SqlParameter("@CarParkUsed_ID", SQLNull(miCarParkUsedID)))
                oParameters.Add(New SqlClient.SqlParameter("@ServiceAttending_ID", SQLNull(miServiceAttendingID)))
                oParameters.Add(New SqlClient.SqlParameter("@Occupation", msOccupation))
                oParameters.Add(New SqlClient.SqlParameter("@Employer", msEmployer))
                oParameters.Add(New SqlClient.SqlParameter("@EmploymentStatus_ID", SQLNull(miEmploymentStatusID)))
                oParameters.Add(New SqlClient.SqlParameter("@EmploymentPosition_ID", SQLNull(miEmploymentPositionID)))
                oParameters.Add(New SqlClient.SqlParameter("@EmploymentIndustry_ID", SQLNull(miEmploymentIndustryID)))
                oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryComments", msKidsMinistryComments))
                oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryGroup", msKidsMinistryGroup))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", False))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                DataAccess.ExecuteCommand("sp_Common_ContactInternalUpdate", oParameters)

                mbChanged = False
                mbPostalAddressChanged = False

            End If

            SaveNPNC(oContact, oUser)               'Save NP/NC first, because we might need to save the ID to a Change Request
            SaveChurchStatusChangeRequest(oUser)
            SaveULGContact(oUser)
            SaveAdditionalDecision(oUser)
            SaveContactRole(oUser)
            SaveCourseInstance(oUser)
            SaveCourseEnrolment(oUser)
            SaveCourseEnrolmentAttendance(oUser)
            SaveComment(oContact, oUser)
            Save247Prayer(oUser)
            SaveKidsMinistryCode(oUser)
            SaveKidsMinistryClearance(oUser)

            mcNPNC.ResetChanges()
            mcULG.ResetChanges()
            mcAdditionalDecision.ResetChanges()
            mcContactRole.ResetChanges()
            mcCourseInstance.ResetChanges()
            mcCourseEnrolment.ResetChanges()
            mcCourseEnrolmentAttendance.ResetChanges()
            mcComment.ResetChanges()
            mc247Prayer.ResetChanges()
            mcKidsMinistryCode.ResetChanges()
            mcKidsMinistryClearance.ResetChanges()

        End Sub

        Private Sub SaveChurchStatusChangeRequest(ByVal oUser As CUser)

            If moChurchStatusChangeRequest IsNot Nothing Then
                moChurchStatusChangeRequest.ContactID = miContactID
                If moChurchStatusChangeRequest.NPNCGUID <> String.Empty Then
                    If mcNPNC.GetItemByGUID(moChurchStatusChangeRequest.NPNCGUID) IsNot Nothing Then
                        moChurchStatusChangeRequest.NPNCID = mcNPNC.GetItemByGUID(moChurchStatusChangeRequest.NPNCGUID).ID
                        moChurchStatusChangeRequest.NPNCGUID = String.Empty
                    End If
                End If
                moChurchStatusChangeRequest.Save(oUser)
                mbHadChurchStatusChangeRequest = True
            ElseIf mbHadChurchStatusChangeRequest Then
                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_ChurchStatusChangeRequestDelete", oParameters)
            End If

        End Sub

        Private Sub SaveNPNC(ByVal oContact As CContact, ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oNPNC As CNPNC

            'Remove the Deleted NPNC's
            For Each oNPNC In mcNPNC.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@NPNC_ID", oNPNC.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_NPNCDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oNPNC In mcNPNC

                'Update the Ministry/Region on the NPNC record, if there is no outcome (or no Ministry/Region).
                'We don't update the Ministry/Region if there is an outcome. We want the Ministry/Region to stay what it was at the point of outcome.
                If oNPNC.OutcomeStatusID = 0 Or oNPNC.Outcomed Or oNPNC.MinistryID = 0 And miMinistryID > 0 Then oNPNC.MinistryID = miMinistryID
                If oNPNC.OutcomeStatusID = 0 Or oNPNC.Outcomed Or oNPNC.RegionID = 0 Then oNPNC.RegionID = miRegionID

                oNPNC.ContactID = Me.ID
                oNPNC.Save(oContact, oUser)
            Next

        End Sub

        Private Sub SaveULGContact(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oULGContact As CULGContact

            'Remove the Deleted ULG Contacts
            For Each oULGContact In mcULG.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ULGContact_ID", oULGContact.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_ULGContactDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oULGContact In mcULG
                oULGContact.ContactID = miContactID
                oULGContact.Save(oUser)
            Next

        End Sub

        Private Sub SaveAdditionalDecision(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oAdditionalDecision As CContactAdditionalDecision

            'Remove the Deleted Additional Decisions
            For Each oAdditionalDecision In mcAdditionalDecision.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@AdditionalDecision_ID", oAdditionalDecision.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_ContactAdditionalDecisionDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oAdditionalDecision In mcAdditionalDecision
                oAdditionalDecision.ContactID = miContactID
                oAdditionalDecision.Save(oUser)
            Next

        End Sub

        Private Sub SaveContactRole(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oContactRole As CContactRole

            'Remove the Deleted Contact Roles
            For Each oContactRole In mcContactRole.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContactRole_ID", oContactRole.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_ContactRoleDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oContactRole In mcContactRole
                oContactRole.ContactID = miContactID
                oContactRole.Save(oUser)
            Next

        End Sub

        Private Sub SaveCourseInstance(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oContactCourseInstance As CContactCourseInstance

            'Remove the Deleted Course Instances
            For Each oContactCourseInstance In mcCourseInstance.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContactCourseInstance_ID", oContactCourseInstance.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_ContactCourseInstanceDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oContactCourseInstance In mcCourseInstance
                oContactCourseInstance.ContactID = miContactID
                oContactCourseInstance.Save(oUser)
            Next

        End Sub

        Private Sub SaveCourseEnrolment(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oContactCourseEnrolment As CContactCourseEnrolment

            'Remove the Deleted Course Enrolments
            For Each oContactCourseEnrolment In mcCourseEnrolment.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContactCourseEnrolment_ID", oContactCourseEnrolment.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_ContactCourseEnrolmentDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oContactCourseEnrolment In mcCourseEnrolment
                oContactCourseEnrolment.ContactID = miContactID
                oContactCourseEnrolment.Save(oUser)
            Next

        End Sub

        Private Sub SaveCourseEnrolmentAttendance(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oCourseEnrolmentAttendance As CCourseEnrolmentAttendance

            'Remove the Deleted Course Enrolments
            For Each oCourseEnrolmentAttendance In mcCourseEnrolmentAttendance.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@CourseEnrolmentAttendance_ID", oCourseEnrolmentAttendance.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_CourseEnrolmentAttendanceDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oCourseEnrolmentAttendance In mcCourseEnrolmentAttendance
                oCourseEnrolmentAttendance.ContactID = miContactID
                oCourseEnrolmentAttendance.Save(oUser)
            Next

        End Sub

        Private Sub SaveComment(ByVal oContact As CContact, ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oContactComment As CContactComment

            'Remove the Deleted comments
            For Each oContactComment In mcComment.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Comment_ID", oContactComment.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_ContactCommentDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oContactComment In mcComment

                'Assign the NPNC ID to this record 
                If oContactComment.NPNCGUID <> String.Empty Then
                    Dim oNPNC As CNPNC
                    For Each oNPNC In oContact.Internal.NPNC
                        If oNPNC.GUID = oContactComment.NPNCGUID Then
                            oContactComment.NPNCID = oNPNC.ID
                        End If
                    Next
                End If

                oContactComment.ContactID = miContactID
                oContactComment.Save(oUser)
            Next

        End Sub

        Private Sub Save247Prayer(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim o247Prayer As C247Prayer

            'Remove the Deleted 24/7 Prayer slots
            For Each o247Prayer In mc247Prayer.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@247Prayer_ID", o247Prayer.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_247PrayerDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each o247Prayer In mc247Prayer
                o247Prayer.ContactID = miContactID
                o247Prayer.Save(oUser)
            Next

        End Sub

        Private Sub SaveKidsMinistryCode(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oContactKidsMinistryCode As CContactKidsMinistryCode

            'Remove the Deleted Kids Ministry Codes
            For Each oContactKidsMinistryCode In mcKidsMinistryCode.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContactKidsMinistryCode_ID", oContactKidsMinistryCode.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_ContactKidsMinistryCodeDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oContactKidsMinistryCode In mcKidsMinistryCode
                oContactKidsMinistryCode.ContactID = miContactID
                oContactKidsMinistryCode.Save(oUser)
            Next

        End Sub

        Private Sub SaveKidsMinistryClearance(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oKidsMinistryClearance As CKidsMinistryClearance

            'Remove the Deleted Kids Ministry Clearance
            For Each oKidsMinistryClearance In mcKidsMinistryClearance.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryClearance_ID", oKidsMinistryClearance.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_KidsMinistryClearanceDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oKidsMinistryClearance In mcKidsMinistryClearance
                oKidsMinistryClearance.ContactID = miContactID
                oKidsMinistryClearance.Save(oUser)
            Next

        End Sub

        Public Function ReadAttendance(ByVal dStartDate As Date, ByVal dEndDate As Date) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", dStartDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", dEndDate))

            Return DataAccess.GetDataTable("sp_Church_ContactAttendanceRead", oParameters)

        End Function

        Public Function GetCurrentNPNCRecord() As CNPNC

            Dim oNPNC As CNPNC
            For Each oNPNC In mcNPNC
                If oNPNC.OutcomeStatusID = 0 Then
                    Return oNPNC
                End If
            Next
            Return Nothing

        End Function
        Public ReadOnly Property UrbanLifeGroups() As CArrayList(Of CULGContact)
            Get
                Return mcULG
            End Get
        End Property
        Public ReadOnly Property UrbanLifeGroupsCurrent() As CArrayList(Of CULGContact)
            Get
                Dim oBuffer As New CArrayList(Of CULGContact)
                For Each oULGContact As CULGContact In mcULG
                    If oULGContact.Inactive = False Then oBuffer.Add(oULGContact)
                Next
                Return oBuffer
            End Get
        End Property
        Public ReadOnly Property NPNC() As CArrayList(Of CNPNC)
            Get
                Return mcNPNC
            End Get
        End Property
        Public ReadOnly Property AdditionalDecisions() As CArrayList(Of CContactAdditionalDecision)
            Get
                Return mcAdditionalDecision
            End Get
        End Property
        Public ReadOnly Property Roles() As CArrayList(Of CContactRole)
            Get
                mcContactRole.Sort()
                Return mcContactRole
            End Get
        End Property
        Public ReadOnly Property RolesCurrent() As CArrayList(Of CContactRole)
            Get
                Dim oArraylist As New CArrayList(Of CContactRole)
                Dim oRole As CContactRole
                For Each oRole In mcContactRole
                    'BIT added for object not set, as Role is nothing in some rows.
                    If oRole.Role IsNot Nothing Then
                        If Not oRole.Role.Deleted Then oArraylist.Add(oRole)
                    End If
                Next
                mcContactRole.Sort()
                Return oArraylist
            End Get
        End Property
        Public ReadOnly Property Courses() As CArrayList(Of CContactCourseInstance)
            Get
                mcCourseEnrolment.Sort()
                Return mcCourseInstance
            End Get
        End Property
        Public ReadOnly Property CoursesCurrent() As CArrayList(Of CContactCourseInstance)
            Get
                Dim oArraylist As New CArrayList(Of CContactCourseInstance)
                Dim oCourse As CContactCourseInstance
                For Each oCourse In Courses
                    If Not oCourse.CourseInstance.Course.Deleted And Not oCourse.CourseInstance.Deleted Then oArraylist.Add(oCourse)
                Next
                oArraylist.Sort()
                Return oArraylist
            End Get
        End Property
        Public ReadOnly Property CourseEnrolment() As CArrayList(Of CContactCourseEnrolment)
            Get
                mcCourseEnrolment.Sort()
                Return mcCourseEnrolment
            End Get
        End Property
        Public ReadOnly Property CourseEnrolmentAttendance() As CArrayList(Of CCourseEnrolmentAttendance)
            Get
                Return mcCourseEnrolmentAttendance
            End Get
        End Property
        Public ReadOnly Property Comments() As CArrayList(Of CContactComment)
            Get
                Return mcComment
            End Get
        End Property
        Public ReadOnly Property Prayer247() As CArrayList(Of C247Prayer)
            Get
                Return mc247Prayer
            End Get
        End Property
        Public ReadOnly Property KidsMinistryCodes() As CArrayList(Of CContactKidsMinistryCode)
            Get
                Return mcKidsMinistryCode
            End Get
        End Property
        Public ReadOnly Property KidsMinistryClearance() As CArrayList(Of CKidsMinistryClearance)
            Get
                Return mcKidsMinistryClearance
            End Get
        End Property

        Private ReadOnly Property ID() As Integer
            Get
                Return miContactID
            End Get
        End Property
        Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal Value As Integer)
                If miContactID <> Value Then mbChanged = True
                miContactID = Value
            End Set
        End Property
        Public Property Barcode() As String
            Get
                Return msBarcode
            End Get
            Set(ByVal Value As String)
                If msBarcode <> Value Then mbChanged = True
                msBarcode = Value
            End Set
        End Property
        Property ChurchStatusID() As Integer
            Get
                Return miChurchStatusID
            End Get
            Set(ByVal Value As Integer)
                If miChurchStatusID <> Value Then mbChanged = True
                miChurchStatusID = Value
            End Set
        End Property
        Public ReadOnly Property ChurchStatus() As CChurchStatus
            Get
                Return Lists.Internal.Church.GetChurchStatusByID(miChurchStatusID)
            End Get
        End Property
        Property ChurchStatusChangeRequest() As CChurchStatusChangeRequest
            Get
                Return moChurchStatusChangeRequest
            End Get
            Set(ByVal Value As CChurchStatusChangeRequest)
                If ObjectID(moChurchStatusChangeRequest) <> ObjectID(Value) Then mbChanged = True
                moChurchStatusChangeRequest = Value
            End Set
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal Value As Integer)
                If miCampusID <> Value Then mbChanged = True
                miCampusID = Value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public Property MinistryID() As Integer
            Get
                Return miMinistryID
            End Get
            Set(ByVal Value As Integer)
                If miMinistryID <> Value Then mbChanged = True
                miMinistryID = Value
            End Set
        End Property
        Public ReadOnly Property Ministry() As CMinistry
            Get
                Return Lists.Internal.Church.GetMinistryByID(miMinistryID)
            End Get
        End Property
        Public Property RegionID() As Integer
            Get
                Return miRegionID
            End Get
            Set(ByVal Value As Integer)
                If miRegionID <> Value Then mbChanged = True
                miRegionID = Value
            End Set
        End Property
        Public ReadOnly Property Region() As CRegion
            Get
                Return Lists.Internal.Church.GetRegionByID(miRegionID)
            End Get
        End Property
        Property PostalAddress1() As String
            Get
                Return msPostalAddress1
            End Get
            Set(ByVal Value As String)
                If msPostalAddress1 <> Value Then mbChanged = True : mbPostalAddressChanged = True
                msPostalAddress1 = Value
            End Set
        End Property
        Property PostalAddress2() As String
            Get
                Return msPostalAddress2
            End Get
            Set(ByVal Value As String)
                If msPostalAddress2 <> Value Then mbChanged = True : mbPostalAddressChanged = True
                msPostalAddress2 = Value
            End Set
        End Property
        Property PostalSuburb() As String
            Get
                Return msPostalSuburb
            End Get
            Set(ByVal Value As String)
                If msPostalSuburb <> Value Then mbChanged = True : mbPostalAddressChanged = True
                msPostalSuburb = Value
            End Set
        End Property
        Property PostalPostcode() As String
            Get
                Return msPostalPostcode
            End Get
            Set(ByVal Value As String)
                If msPostalPostcode <> Value Then mbChanged = True : mbPostalAddressChanged = True
                msPostalPostcode = Value
            End Set
        End Property
        Public Property PostalStateID() As Integer
            Get
                Return miPostalStateID
            End Get
            Set(ByVal Value As Integer)
                If miPostalStateID <> Value Then mbChanged = True : mbPostalAddressChanged = True
                miPostalStateID = Value
            End Set
        End Property
        Public ReadOnly Property PostalState() As CState
            Get
                Return Lists.GetStateByID(miPostalStateID)
            End Get
        End Property
        Property PostalStateOther() As String
            Get
                Return msPostalStateOther
            End Get
            Set(ByVal Value As String)
                If msPostalStateOther <> Value Then mbChanged = True : mbPostalAddressChanged = True
                msPostalStateOther = Value
            End Set
        End Property
        Public Property PostalCountryID() As Integer
            Get
                Return miPostalCountryID
            End Get
            Set(ByVal Value As Integer)
                If miPostalCountryID <> Value Then mbChanged = True : mbPostalAddressChanged = True
                miPostalCountryID = Value
            End Set
        End Property
        Public ReadOnly Property PostalCountry() As CCountry
            Get
                Return Lists.GetCountryByID(miPostalCountryID)
            End Get
        End Property
        Property Photo() As Byte()
            Get
                Return msPhoto
            End Get
            Set(ByVal Value As Byte())

                Dim sOriginal As String = String.Empty
                Dim sNew As String = String.Empty

                If Value IsNot Nothing Then sOriginal = BitConverter.ToString(Value)
                If msPhoto IsNot Nothing Then sNew = BitConverter.ToString(msPhoto)
                If sOriginal <> sNew Then mbChanged = True

                msPhoto = Value

            End Set
        End Property
        Property EntryPointID() As Integer
            Get
                Return miEntryPointID
            End Get
            Set(ByVal Value As Integer)
                If miEntryPointID <> Value Then mbChanged = True
                miEntryPointID = Value
            End Set
        End Property
        ReadOnly Property EntryPoint() As CEntryPoint
            Get
                Return Lists.Internal.Church.GetEntryPointByID(miEntryPointID)
            End Get
        End Property
        Property CovenantFormDate() As Date
            Get
                Return mdCovenantFormDate
            End Get
            Set(ByVal Value As Date)
                If mdCovenantFormDate <> Value Then mbChanged = True
                mdCovenantFormDate = Value
            End Set
        End Property
        Property WaterBaptismDate() As Date
            Get
                Return mdWaterBaptismDate
            End Get
            Set(ByVal Value As Date)
                If mdWaterBaptismDate <> Value Then mbChanged = True
                mdWaterBaptismDate = Value
            End Set
        End Property
        Property VolunteerFormDate() As Date
            Get
                Return mdVolunteerFormDate
            End Get
            Set(ByVal Value As Date)
                If mdVolunteerFormDate <> Value Then mbChanged = True
                mdVolunteerFormDate = Value
            End Set
        End Property
        Property PrimaryCarer() As String
            Get
                Return msPrimaryCarer
            End Get
            Set(ByVal Value As String)
                If msPrimaryCarer <> Value Then mbChanged = True
                msPrimaryCarer = Value
            End Set
        End Property
        Property CountryOfOriginID() As Integer
            Get
                Return miCountryOfOriginID
            End Get
            Set(ByVal Value As Integer)
                If miCountryOfOriginID <> Value Then mbChanged = True
                miCountryOfOriginID = Value
            End Set
        End Property
        Public Property PrivacyNumber() As String
            Get
                Return msPrivacyNumber
            End Get
            Set(ByVal value As String)
                If msPrivacyNumber <> value Then mbChanged = True
                msPrivacyNumber = value
            End Set
        End Property
        ReadOnly Property CountryOfOrigin() As CCountry
            Get
                Return Lists.GetCountryByID(miCountryOfOriginID)
            End Get
        End Property
        Property PrimarySchool() As String
            Get
                Return msPrimarySchool
            End Get
            Set(ByVal Value As String)
                If msPrimarySchool <> Value Then mbChanged = True
                msPrimarySchool = Value
            End Set
        End Property
        Property SchoolGradeID() As Integer
            Get
                Return miSchoolGradeID
            End Get
            Set(ByVal Value As Integer)
                If miSchoolGradeID <> Value Then mbChanged = True
                miSchoolGradeID = Value
            End Set
        End Property
        ReadOnly Property SchoolGrade() As CSchoolGrade
            Get
                Return Lists.GetSchoolGradeByID(miSchoolGradeID)
            End Get
        End Property
        Property HighSchool() As String
            Get
                Return msHighSchool
            End Get
            Set(ByVal Value As String)
                If msHighSchool <> Value Then mbChanged = True
                msHighSchool = Value
            End Set
        End Property
        Property University() As String
            Get
                Return msUniversity
            End Get
            Set(ByVal Value As String)
                If msUniversity <> Value Then mbChanged = True
                msUniversity = Value
            End Set
        End Property
        Property InformationIsConfidential() As Boolean
            Get
                Return mbInformationIsConfidential
            End Get
            Set(ByVal Value As Boolean)
                If mbInformationIsConfidential <> Value Then mbChanged = True
                mbInformationIsConfidential = Value
            End Set
        End Property
        Property FacebookAddress() As String
            Get
                Return msFacebookAddress
            End Get
            Set(ByVal value As String)
                If msFacebookAddress <> value Then mbChanged = True
                msFacebookAddress = value
            End Set
        End Property
        Property EducationID() As Integer
            Get
                Return miEducationID
            End Get
            Set(ByVal Value As Integer)
                If miEducationID <> Value Then mbChanged = True
                miEducationID = Value
            End Set
        End Property
        ReadOnly Property Education() As CEducation
            Get
                Return Lists.Internal.Church.GetEducationByID(miEducationID)
            End Get
        End Property
        Property ModeOfTransportID() As Integer
            Get
                Return miModeOfTransportID
            End Get
            Set(ByVal Value As Integer)
                If miModeOfTransportID <> Value Then mbChanged = True
                miModeOfTransportID = Value
            End Set
        End Property
        ReadOnly Property ModeOfTransport() As CModeOfTransport
            Get
                Return Lists.Internal.Church.GetModeOfTransportByID(miModeOfTransportID)
            End Get
        End Property
        Property CarParkUsedID() As Integer
            Get
                Return miCarParkUsedID
            End Get
            Set(ByVal Value As Integer)
                If miCarParkUsedID <> Value Then mbChanged = True
                miCarParkUsedID = Value
            End Set
        End Property
        ReadOnly Property CarParkUsed() As CCarParkUsed
            Get
                Return Lists.Internal.Church.GetCarParkUsedByID(miCarParkUsedID)
            End Get
        End Property
        Property ServiceAttendingID() As Integer
            Get
                Return miServiceAttendingID
            End Get
            Set(ByVal Value As Integer)
                If miServiceAttendingID <> Value Then mbChanged = True
                miServiceAttendingID = Value
            End Set
        End Property
        ReadOnly Property ServiceAttending() As CChurchService
            Get
                Return Lists.Internal.Church.GetChurchServiceByID(miServiceAttendingID)
            End Get
        End Property
        Property Occupation() As String
            Get
                Return msOccupation
            End Get
            Set(ByVal Value As String)
                If msOccupation <> Value Then mbChanged = True
                msOccupation = Value
            End Set
        End Property
        Property Employer() As String
            Get
                Return msEmployer
            End Get
            Set(ByVal Value As String)
                If msEmployer <> Value Then mbChanged = True
                msEmployer = Value
            End Set
        End Property
        Property EmploymentStatusID() As Integer
            Get
                Return miEmploymentStatusID
            End Get
            Set(ByVal Value As Integer)
                If miEmploymentStatusID <> Value Then mbChanged = True
                miEmploymentStatusID = Value
            End Set
        End Property
        ReadOnly Property EmploymentStatus() As CEmploymentStatus
            Get
                Return Lists.Internal.Church.GetEmploymentStatusByID(miEmploymentStatusID)
            End Get
        End Property
        Property EmploymentPositionID() As Integer
            Get
                Return miEmploymentPositionID
            End Get
            Set(ByVal Value As Integer)
                If miEmploymentPositionID <> Value Then mbChanged = True
                miEmploymentPositionID = Value
            End Set
        End Property
        ReadOnly Property EmploymentPosition() As CEmploymentPosition
            Get
                Return Lists.Internal.Church.GetEmploymentPositionByID(miEmploymentPositionID)
            End Get
        End Property
        Property EmploymentIndustryID() As Integer
            Get
                Return miEmploymentIndustryID
            End Get
            Set(ByVal Value As Integer)
                If miEmploymentIndustryID <> Value Then mbChanged = True
                miEmploymentIndustryID = Value
            End Set
        End Property
        ReadOnly Property EmploymentIndustry() As CEmploymentIndustry
            Get
                Return Lists.Internal.Church.GetEmploymentIndustryByID(miEmploymentIndustryID)
            End Get
        End Property
        Property KidsMinistryComments() As String
            Get
                Return msKidsMinistryComments
            End Get
            Set(ByVal Value As String)
                If msKidsMinistryComments <> Value Then mbChanged = True
                msKidsMinistryComments = Value
            End Set
        End Property
        Property KidsMinistryGroup() As String
            Get
                Return msKidsMinistryGroup
            End Get
            Set(ByVal Value As String)
                If msKidsMinistryGroup <> Value Then mbChanged = True
                msKidsMinistryGroup = Value
            End Set
        End Property

        Property DeletedReason() As Object
            Get
                Return moDeleteReason
            End Get
            Set(ByVal value As Object)
                moDeleteReason = value
            End Set
        End Property

        Public ReadOnly Property ContactPostalAddress() As String
            Get
                Dim oString As New Text.StringBuilder
                oString.Append(IIf(Me.PostalAddress1 <> "", Me.PostalAddress1 & vbCrLf, "") & _
                               IIf(Me.PostalAddress2 <> "", Me.PostalAddress2 & vbCrLf, "") & _
                               IIf(Me.PostalSuburb <> "", Me.PostalSuburb & " ", ""))
                If Me.PostalState IsNot Nothing Then
                    oString.Append(IIf(Me.PostalState.Name.Contains("Other"), IIf(Me.PostalStateOther <> "", Me.PostalStateOther & " ", ""), Me.PostalState.Name & " "))
                End If
                oString.Append(IIf(Me.PostalPostcode <> "", Me.PostalPostcode, "") & vbCrLf)
                If Me.PostalCountry IsNot Nothing Then
                    oString.Append(IIf(Me.PostalCountry.Name <> "", Me.PostalCountry.Name, ""))
                End If
                Return oString.ToString
            End Get
        End Property

        Public ReadOnly Property Changed() As Boolean
            Get
                If mbChanged Then Return True

                If mcNPNC.Changed Then Return True
                If mcULG.Changed Then Return True
                If mcContactRole.Changed Then Return True
                If mcCourseInstance.Changed Then Return True
                If mcComment.Changed Then Return True
                If mc247Prayer.Changed Then Return True
                If mcKidsMinistryCode.Changed Then Return True
                If mcKidsMinistryClearance.Changed Then Return True

                Dim oNPNC As CNPNC
                For Each oNPNC In mcNPNC
                    If oNPNC.Changed Then Return True
                Next
                Dim oULGContact As CULGContact
                For Each oULGContact In mcULG
                    If oULGContact.Changed Then Return True
                Next
                Dim oContactRole As CContactRole
                For Each oContactRole In mcContactRole
                    If oContactRole.Changed Then Return True
                Next
                Dim oContactCourseInstance As CContactCourseInstance
                For Each oContactCourseInstance In mcCourseInstance
                    If oContactCourseInstance.Changed Then Return True
                Next
                Dim oContactComment As CContactComment
                For Each oContactComment In mcComment
                    If oContactComment.Changed Then Return True
                Next
                Dim o247Prayer As CContactComment
                For Each o247Prayer In mcComment
                    If o247Prayer.Changed Then Return True
                Next
                Dim oKidsMinistryCode As CContactKidsMinistryCode
                For Each oKidsMinistryCode In mcKidsMinistryCode
                    If oKidsMinistryCode.Changed Then Return True
                Next
                Dim oKidsMinistryClearance As CKidsMinistryClearance
                For Each oKidsMinistryClearance In mcKidsMinistryClearance
                    If oKidsMinistryClearance.Changed Then Return True
                Next

                Return False

            End Get
        End Property

        Public Sub ResetChanges()

            mbChanged = False

            mcNPNC.ResetChanges()
            mcULG.ResetChanges()
            mcContactRole.ResetChanges()
            mcCourseInstance.ResetChanges()
            mcComment.ResetChanges()
            mc247Prayer.ResetChanges()
            mcKidsMinistryCode.ResetChanges()
            mcKidsMinistryClearance.ResetChanges()

            Dim oNPNC As CNPNC
            For Each oNPNC In mcNPNC
                oNPNC.ResetChanges()
            Next
            Dim oULGContact As CULGContact
            For Each oULGContact In mcULG
                oULGContact.ResetChanges()
            Next
            Dim oContactRole As CContactRole
            For Each oContactRole In mcContactRole
                oContactRole.ResetChanges()
            Next
            Dim oContactCourseInstance As CContactCourseInstance
            For Each oContactCourseInstance In mcCourseInstance
                oContactCourseInstance.ResetChanges()
            Next
            Dim oContactComment As CContactComment
            For Each oContactComment In mcComment
                oContactComment.ResetChanges()
            Next
            Dim o247Prayer As CContactComment
            For Each o247Prayer In mcComment
                o247Prayer.ResetChanges()
            Next
            Dim oKidsMinistryCode As CContactKidsMinistryCode
            For Each oKidsMinistryCode In mcKidsMinistryCode
                oKidsMinistryCode.ResetChanges()
            Next
            Dim oKidsMinistryClearance As CKidsMinistryClearance
            For Each oKidsMinistryClearance In mcKidsMinistryClearance
                oKidsMinistryClearance.ResetChanges()
            Next

        End Sub

        Public ReadOnly Property PostalAddressChanged() As Boolean
            Get
                Return mbPostalAddressChanged
            End Get
        End Property

        Public ReadOnly Property UrbanLifeGroupCurrentCount() As Integer
            Get
                Dim iCount As Integer = 0
                Dim oULGContact As CULGContact
                For Each oULGContact In mcULG
                    If Not oULGContact.Inactive Then iCount += 1
                Next
                Return iCount
            End Get
        End Property

        Public ReadOnly Property ActiveNPNC() As CArrayList(Of CNPNC)
            Get
                Dim oArrayList As New CArrayList(Of CNPNC)

                For Each oNPNC As CNPNC In NPNC
                    If Not oNPNC.Outcomed Then
                        oArrayList.Add(oNPNC)
                    End If
                Next

                Return oArrayList
            End Get
        End Property
    End Class

End Namespace