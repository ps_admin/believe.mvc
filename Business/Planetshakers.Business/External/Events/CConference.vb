Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CConference

        Private miConferenceID As Integer
        Private msName As String
        Private msConferenceNameShort As String
        Private msConferenceDate As String
        Private msConferenceLocation As String
        Private msConferenceTagline As String
        Private miConferenceType As EnumConferenceType
        Private miCountryID As Integer
        Private mbAllowOnlineRegistrationSingle As Boolean
        Private mbAllowOnlineRegistrationGroup As Boolean
        Private mbShowConferenceOnWeb As Boolean
        Private mbAllowGroupCreditApproval As Boolean
        Private mbAllowGroupOnlineBulkRegistrationPurchase As Boolean
        Private mbRequireEmergencyContactInfo As Boolean
        Private mbRequireMedicalInfo As Boolean
        Private mbIncludeULGOption As Boolean
        Private mbCrecheAvailable As Boolean
        Private mbPreOrderAvailable As Boolean
        Private mbElectivesAvailable As Boolean
        Private mbAccommodationAvailable As Boolean
        Private mbCateringAvailable As Boolean
        Private mbLeadershipBreakfastAvailable As Boolean
        Private mdAccommodationCost As Decimal
        Private mdCateringCost As Decimal
        Private mdLeadershipBreakfastCost As Decimal
        Private miBankAccountID As Integer
        Private miSortOrder As Integer
        Private miVolunteersManagerID As Integer
        Private moVolunteersManagementCutOff As DateTime
        Private moVolunteersManagerCutOff As DateTime
        Private msLetterHead As String
        Private moDateLastGroupRegistrationsAllocation As DateTime

        Public Sub New()

            miConferenceID = 0
            msName = String.Empty
            msConferenceNameShort = String.Empty
            msConferenceDate = String.Empty
            msConferenceLocation = String.Empty
            msConferenceTagline = String.Empty
            miConferenceType = 0
            miCountryID = 0
            mbAllowOnlineRegistrationSingle = False
            mbAllowOnlineRegistrationGroup = False
            mbShowConferenceOnWeb = False
            mbAllowGroupCreditApproval = False
            mbAllowGroupOnlineBulkRegistrationPurchase = False
            mbRequireEmergencyContactInfo = False
            mbRequireMedicalInfo = False
            mbIncludeULGOption = False
            mbCrecheAvailable = False
            mbPreOrderAvailable = False
            mbElectivesAvailable = False
            mbAccommodationAvailable = False
            mbCateringAvailable = False
            mbLeadershipBreakfastAvailable = False
            mdAccommodationCost = 0
            mdCateringCost = 0
            mdLeadershipBreakfastCost = 0
            miBankAccountID = Nothing
            miSortOrder = 0
            miVolunteersManagerID = 0
            moVolunteersManagementCutOff = Nothing
            moVolunteersManagerCutOff = Nothing
            msLetterHead = String.Empty
            moDateLastGroupRegistrationsAllocation = Nothing

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miConferenceID = oDataRow("Conference_ID")
            msName = oDataRow("ConferenceName")
            msConferenceNameShort = oDataRow("ConferenceNameShort")
            msConferenceDate = oDataRow("ConferenceDate")
            msConferenceLocation = oDataRow("ConferenceLocation")
            msConferenceTagline = oDataRow("ConferenceTagline")
            miConferenceType = oDataRow("ConferenceType")
            miCountryID = oDataRow("Country_ID")
            mbAllowOnlineRegistrationSingle = oDataRow("AllowOnlineRegistrationSingle")
            mbAllowOnlineRegistrationGroup = oDataRow("AllowOnlineRegistrationGroup")
            mbShowConferenceOnWeb = oDataRow("ShowConferenceOnWeb")
            mbAllowGroupCreditApproval = oDataRow("AllowGroupCreditApproval")
            mbAllowGroupOnlineBulkRegistrationPurchase = oDataRow("AllowGroupOnlineBulkRegistrationPurchase")
            mbRequireEmergencyContactInfo = oDataRow("RequireEmergencyContactInfo")
            mbRequireMedicalInfo = oDataRow("RequireMedicalInfo")
            mbIncludeULGOption = oDataRow("IncludeULGOption")
            mbCrecheAvailable = oDataRow("CrecheAvailable")
            mbPreOrderAvailable = oDataRow("PreOrderAvailable")
            mbElectivesAvailable = oDataRow("ElectivesAvailable")
            mbAccommodationAvailable = oDataRow("AccommodationAvailable")
            mbCateringAvailable = oDataRow("CateringAvailable")
            mbLeadershipBreakfastAvailable = oDataRow("LeadershipBreakfastAvailable")
            mdAccommodationCost = oDataRow("AccommodationCost")
            mdCateringCost = oDataRow("CateringCost")
            mdLeadershipBreakfastCost = oDataRow("LeadershipBreakfastCost")
            miBankAccountID = oDataRow("BankAccount_ID")
            miSortOrder = oDataRow("SortOrder")

            If Not IsDBNull(oDataRow("LetterHead")) Then msLetterHead = oDataRow("LetterHead")

            Try
                If Not IsDBNull(oDataRow("VolunteersManager_ID")) Then
                    miVolunteersManagerID = CType(oDataRow("VolunteersManager_ID"), Integer)
                Else
                    miVolunteersManagerID = 0
                End If
                'miVolunteersManagerID = CType(oDataRow("VolunteersManager_ID"), Integer)
            Catch ex As Exception
                miVolunteersManagerID = 0
            End Try

            Try
                'moVolunteersManagementCutOff = CType(oDataRow("VolunteersManagementCutOffDate"), DateTime)
                If Not IsDBNull(oDataRow("VolunteersManagementCutOffDate")) Then
                    moVolunteersManagementCutOff = CType(oDataRow("VolunteersManagementCutOffDate"), DateTime)
                Else
                    moVolunteersManagementCutOff = Nothing
                End If
            Catch ex As Exception
                moVolunteersManagementCutOff = Nothing
            End Try

            Try
                'moVolunteersManagerCutOff = CType(oDataRow("VolunteersManagerCutOffDate"), DateTime)
                If Not IsDBNull(oDataRow("VolunteersManagerCutOffDate")) Then
                    moVolunteersManagerCutOff = CType(oDataRow("VolunteersManagerCutOffDate"), DateTime)
                Else
                    moVolunteersManagerCutOff = Nothing
                End If
            Catch ex As Exception
                moVolunteersManagerCutOff = Nothing
            End Try

            Try
                'moDateLastGroupRegistrationsAllocation = CType(oDataRow("LastDateAllocateGroupRegistrations"), DateTime)
                If Not IsDBNull(oDataRow("LastDateAllocateGroupRegistrations")) Then
                    moDateLastGroupRegistrationsAllocation = CType(oDataRow("LastDateAllocateGroupRegistrations"), DateTime)
                Else
                    moDateLastGroupRegistrationsAllocation = Nothing
                End If
            Catch ex As Exception
                moDateLastGroupRegistrationsAllocation = Nothing
            End Try

        End Sub

        Public Function Save() As Boolean

            If ID > 0 Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", miConferenceID))
                oParameters.Add(New SqlClient.SqlParameter("@ConferenceName", msName))
                oParameters.Add(New SqlClient.SqlParameter("@ConferenceNameShort", msConferenceNameShort))
                oParameters.Add(New SqlClient.SqlParameter("@ConferenceDate", msConferenceDate))
                oParameters.Add(New SqlClient.SqlParameter("@ConferenceLocation", msConferenceLocation))
                oParameters.Add(New SqlClient.SqlParameter("@ConferenceTagline", msConferenceTagline))
                oParameters.Add(New SqlClient.SqlParameter("@ConferenceType", miConferenceType))
                oParameters.Add(New SqlClient.SqlParameter("@Country_ID", miCountryID))
                oParameters.Add(New SqlClient.SqlParameter("@AllowOnlineRegistrationSingle", mbAllowOnlineRegistrationSingle))
                oParameters.Add(New SqlClient.SqlParameter("@AllowOnlineRegistrationGroup", mbAllowOnlineRegistrationGroup))
                oParameters.Add(New SqlClient.SqlParameter("@ShowConferenceOnWeb", mbShowConferenceOnWeb))
                oParameters.Add(New SqlClient.SqlParameter("@AllowGroupCreditApproval", mbAllowGroupCreditApproval))
                oParameters.Add(New SqlClient.SqlParameter("@AllowGroupOnlineBulkRegistrationPurchase", mbAllowGroupOnlineBulkRegistrationPurchase))
                oParameters.Add(New SqlClient.SqlParameter("@IncludeULGOption", mbIncludeULGOption))
                oParameters.Add(New SqlClient.SqlParameter("@CrecheAvailable", mbCrecheAvailable))
                oParameters.Add(New SqlClient.SqlParameter("@RequireEmergencyContactInfo", mbRequireEmergencyContactInfo))
                oParameters.Add(New SqlClient.SqlParameter("@RequireMedicalInfo", mbRequireMedicalInfo))
                oParameters.Add(New SqlClient.SqlParameter("@PreOrderAvailable", mbPreOrderAvailable))
                oParameters.Add(New SqlClient.SqlParameter("@ElectivesAvailable", mbElectivesAvailable))
                oParameters.Add(New SqlClient.SqlParameter("@AccommodationAvailable", mbAccommodationAvailable))
                oParameters.Add(New SqlClient.SqlParameter("@CateringAvailable", mbCateringAvailable))
                oParameters.Add(New SqlClient.SqlParameter("@LeadershipBreakfastAvailable", mbLeadershipBreakfastAvailable))
                oParameters.Add(New SqlClient.SqlParameter("@AccommodationCost", mdAccommodationCost))
                oParameters.Add(New SqlClient.SqlParameter("@CateringCost", mdCateringCost))
                oParameters.Add(New SqlClient.SqlParameter("@LeadershipBreakfastCost", mdLeadershipBreakfastCost))
                oParameters.Add(New SqlClient.SqlParameter("@BankAccount_ID", miBankAccountID))
                oParameters.Add(New SqlClient.SqlParameter("@SortOrder", miSortOrder))

                DataAccess.ExecuteCommand("sp_ConferenceUpdate", oParameters)

                Return True

            Else
                Return False
            End If

        End Function

        Public ReadOnly Property ID() As Integer
            Get
                Return miConferenceID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property
        Public Property ConferenceNameShort() As String
            Get
                Return msConferenceNameShort
            End Get
            Set(ByVal Value As String)
                msConferenceNameShort = Value
            End Set
        End Property
        Public Property ConferenceDate() As String
            Get
                Return msConferenceDate
            End Get
            Set(ByVal Value As String)
                msConferenceDate = Value
            End Set
        End Property
        Public Property ConferenceLocation() As String
            Get
                Return msConferenceLocation
            End Get
            Set(ByVal Value As String)
                msConferenceLocation = Value
            End Set
        End Property
        Public Property ConferenceTagline() As String
            Get
                Return msConferenceTagline
            End Get
            Set(ByVal Value As String)
                msConferenceTagline = Value
            End Set
        End Property
        Public Property ConferenceType() As EnumConferenceType
            Get
                Return miConferenceType
            End Get
            Set(ByVal Value As EnumConferenceType)
                miConferenceType = Value
            End Set
        End Property
        Public Property CountryID() As Integer
            Get
                Return miCountryID
            End Get
            Set(ByVal Value As Integer)
                miCountryID = Value
            End Set
        End Property
        Public ReadOnly Property Country() As CCountry
            Get
                Return Lists.GetCountryByID(miCountryID)
            End Get
        End Property
        Public Property AllowOnlineRegistrationSingle() As Boolean
            Get
                Return mbAllowOnlineRegistrationSingle
            End Get
            Set(ByVal Value As Boolean)
                mbAllowOnlineRegistrationSingle = Value
            End Set
        End Property
        Public Property AllowOnlineRegistrationGroup() As Boolean
            Get
                Return mbAllowOnlineRegistrationGroup
            End Get
            Set(ByVal Value As Boolean)
                mbAllowOnlineRegistrationGroup = Value
            End Set
        End Property
        Public Property ShowConferenceOnWeb() As Boolean
            Get
                Return mbShowConferenceOnWeb
            End Get
            Set(ByVal Value As Boolean)
                mbShowConferenceOnWeb = Value
            End Set
        End Property
        Public Property AllowGroupCreditApproval() As Boolean
            Get
                Return mbAllowGroupCreditApproval
            End Get
            Set(ByVal Value As Boolean)
                mbAllowGroupCreditApproval = Value
            End Set
        End Property
        Public Property AllowGroupOnlineBulkRegistrationPurchase() As Boolean
            Get
                Return mbAllowGroupOnlineBulkRegistrationPurchase
            End Get
            Set(ByVal Value As Boolean)
                mbAllowGroupOnlineBulkRegistrationPurchase = Value
            End Set
        End Property
        Public Property RequireEmergencyContactInfo() As Boolean
            Get
                Return mbRequireEmergencyContactInfo
            End Get
            Set(ByVal value As Boolean)
                mbRequireEmergencyContactInfo = value
            End Set
        End Property
        Public Property RequireMedicalInfo() As Boolean
            Get
                Return mbRequireMedicalInfo
            End Get
            Set(ByVal value As Boolean)
                mbRequireMedicalInfo = value
            End Set
        End Property
        Public Property IncludeULGOption() As Boolean
            Get
                Return mbIncludeULGOption
            End Get
            Set(ByVal value As Boolean)
                mbIncludeULGOption = value
            End Set
        End Property
        Public Property CrecheAvailable() As Boolean
            Get
                Return mbCrecheAvailable
            End Get
            Set(ByVal Value As Boolean)
                mbCrecheAvailable = Value
            End Set
        End Property
        Public Property PreOrderAvailable() As Boolean
            Get
                Return mbPreOrderAvailable
            End Get
            Set(ByVal Value As Boolean)
                mbPreOrderAvailable = Value
            End Set
        End Property
        Public Property ElectivesAvailable() As Boolean
            Get
                Return mbElectivesAvailable
            End Get
            Set(ByVal Value As Boolean)
                mbElectivesAvailable = Value
            End Set
        End Property
        Public Property AccommodationAvailable() As Boolean
            Get
                Return mbAccommodationAvailable
            End Get
            Set(ByVal Value As Boolean)
                mbAccommodationAvailable = Value
            End Set
        End Property
        Public Property CateringAvailable() As Boolean
            Get
                Return mbCateringAvailable
            End Get
            Set(ByVal Value As Boolean)
                mbCateringAvailable = Value
            End Set
        End Property
        Public Property LeadershipBreakfastAvailable() As Boolean
            Get
                Return mbLeadershipBreakfastAvailable
            End Get
            Set(ByVal Value As Boolean)
                mbLeadershipBreakfastAvailable = Value
            End Set
        End Property
        Public Property AccommodationCost() As Decimal
            Get
                Return mdAccommodationCost
            End Get
            Set(ByVal Value As Decimal)
                mdAccommodationCost = Value
            End Set
        End Property
        Public Property CateringCost() As Decimal
            Get
                Return mdCateringCost
            End Get
            Set(ByVal Value As Decimal)
                mdCateringCost = Value
            End Set
        End Property
        Public Property LeadershipBreakfastCost() As Decimal
            Get
                Return mdLeadershipBreakfastCost
            End Get
            Set(ByVal Value As Decimal)
                mdLeadershipBreakfastCost = Value
            End Set
        End Property
        Public Property BankAccountID() As Integer
            Get
                Return miBankAccountID
            End Get
            Set(ByVal value As Integer)
                miBankAccountID = value
            End Set
        End Property
        Public ReadOnly Property BankAccount() As CBankAccount
            Get
                Return Lists.GetBankAccountByID(miBankAccountID)
            End Get
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal Value As Integer)
                miSortOrder = Value
            End Set
        End Property
        Public ReadOnly Property LetterHead() As String
            Get
                Return msLetterHead
            End Get
        End Property

        Public ReadOnly Property ConferenceFirstStartDate() As Date
            Get
                Dim oVenue As CVenue
                Dim dStartDate As Date = Nothing
                For Each oVenue In Lists.External.Events.ConferenceVenues.Values
                    If oVenue.Conference.ID = ID Then
                        If dStartDate = Nothing OrElse oVenue.ConferenceStartDate < dStartDate Then
                            dStartDate = oVenue.ConferenceStartDate
                        End If
                    End If
                Next
                Return dStartDate
            End Get
        End Property

        Public ReadOnly Property ConferenceLastEndDate() As Date
            Get
                Dim oVenue As CVenue
                Dim dEndDate As Date = Nothing
                For Each oVenue In Lists.External.Events.ConferenceVenues.Values
                    If oVenue.Conference.ID = ID Then
                        If dEndDate = Nothing OrElse oVenue.ConferenceEndDate > dEndDate Then
                            dEndDate = oVenue.ConferenceEndDate
                        End If
                    End If
                Next
                Return dEndDate
            End Get
        End Property

        Public ReadOnly Property ConferenceActive() As Boolean
            Get
                If ConferenceLastEndDate <> Nothing Then
                    Return CDate(Format(ConferenceLastEndDate, "dd MMM yyyy")) >= CDate(Format(GetDateTime, "dd MMM yyyy"))
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property VolunteersManager() As CContact
            Get
                Dim oContact As New CContact

                If miVolunteersManagerID > 0 Then
                    oContact.LoadByID(miVolunteersManagerID)
                    Return oContact
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property VolunteersManagementCutOffDate() As DateTime
            Get
                Return moVolunteersManagementCutOff
            End Get
        End Property

        Public ReadOnly Property VolunteersManagerCutOffDate() As DateTime
            Get
                Return moVolunteersManagerCutOff
            End Get
        End Property

        Public ReadOnly Property DateLastGroupRegistrationsAllocation() As DateTime
            Get
                Return moDateLastGroupRegistrationsAllocation
            End Get
        End Property

        Public Function GetRegistrationOptionNames() As ArrayList

            Dim oArrayList As New ArrayList
            Dim oRegistrationOptionName As CRegistrationOptionName
            For Each oRegistrationOptionName In Lists.External.Events.RegistrationOptionNames.Values
                If oRegistrationOptionName.Conference.ID = Me.ID Then
                    oArrayList.Add(oRegistrationOptionName)
                End If
            Next
            oArrayList.Sort(New CSorterBySortOrder)
            Return oArrayList

        End Function
        Public Function GetRegistrationOptionName(ByVal sName As String) As CRegistrationOptionName

            Dim oArrayList As New ArrayList
            Dim oRegistrationOptionName As CRegistrationOptionName
            For Each oRegistrationOptionName In GetRegistrationOptionNames()
                If oRegistrationOptionName.Name = sName Then Return oRegistrationOptionName
            Next
            Return Nothing

        End Function

        Public Function HasRegistrationOption(ByVal sName As String) As Boolean

            Dim oRegistrationOptionName As CRegistrationOptionName
            For Each oRegistrationOptionName In GetRegistrationOptionNames()
                If oRegistrationOptionName.Name = sName Then Return True
            Next
            Return False

        End Function

        Public Function VenueCount() As Integer

            Dim iCount As Integer = 0
            For Each oVenue As CVenue In Lists.External.Events.ConferenceVenues.Values
                If oVenue.ConferenceID = ID Then iCount += 1
            Next
            Return iCount

        End Function

        Public Overrides Function toString() As String
            Return msName
        End Function

    End Class

End Namespace