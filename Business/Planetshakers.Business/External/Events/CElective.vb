Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CElective

        Private miElectiveID As Integer
        Private msName As String
        Private miSortOrder As Integer

        Private moRegistrationType As CArrayList(Of CRegistrationType)

        Public Sub New()

            miElectiveID = 0
            msName = String.Empty
            miSortOrder = 0
            moRegistrationType = New CArrayList(Of CRegistrationType)

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miElectiveID = oDataRow("Elective_ID")
            msName = oDataRow("Name")
            miSortOrder = oDataRow("SortOrder")

            Dim oRegistrationTypeRow As DataRow
            For Each oRegistrationTypeRow In oDataRow.GetChildRows("RegistrationType")
                moRegistrationType.Add(Lists.External.Events.GetRegistrationTypeByID(oRegistrationTypeRow("RegistrationType_ID")))
            Next

        End Sub

        ReadOnly Property RegistrationType() As CArrayList(Of CRegistrationType)
            Get
                Return moRegistrationType
            End Get
        End Property

        Public Property ID() As Integer
            Get
                Return miElectiveID
            End Get
            Set(ByVal Value As Integer)
                miElectiveID = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = Value
            End Set
        End Property

    End Class

End Namespace
