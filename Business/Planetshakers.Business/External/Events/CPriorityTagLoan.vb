﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CPriorityTagLoan
    Private msPriorityTagID As String
    Private miConferenceID As Integer
    Private miVolunteerDepartmentID As Integer
    Private miAccessLevelID As Integer
    Private moContactID As Integer
    Private moCheckOut As DateTime
    Private moCheckIn As DateTime

    Public Sub New()
        msPriorityTagID = ""
        miConferenceID = 0
        miVolunteerDepartmentID = 0
        miAccessLevelID = 0
        moContactID = 0
        moCheckOut = Nothing
        moCheckIn = Nothing
    End Sub

    Public Sub New(ByRef oDataRow As DataRow)
        Me.New()
        LoadDataRow(oDataRow)
    End Sub

    Public Sub New(ByVal iVolunteerTagID As String, ByVal oConference As External.Events.CConference)
        Me.New()
        Dim oParams As New ArrayList
        oParams.Add(New SqlClient.SqlParameter("@PriorityTag_ID", iVolunteerTagID))
        oParams.Add(New SqlClient.SqlParameter("@Conference_ID", oConference.ID))

        Dim oDataTable As DataTable = Business.DataAccess.GetDataTable("sp_Events_PriorityTagLoanRead", oParams)

        If oDataTable.Rows.Count = 1 Then
            LoadDataRow(oDataTable.Rows(0))
        End If
    End Sub

    Private Sub LoadDataRow(ByRef oDataRow As DataRow)
        msPriorityTagID = oDataRow("PriorityTag_ID")
        moContactID = CType(oDataRow("Contact_ID"), Integer)
        miConferenceID = CType(oDataRow("Conference_ID"), Integer)
        miVolunteerDepartmentID = CType(oDataRow("VolunteerDepartment_ID"), Integer)
        miAccessLevelID = CType(oDataRow("AccessLevel_ID"), Integer)
        moCheckOut = CType(IIf(Convert.IsDBNull(oDataRow("PriorityTagLoan_CheckOut")), Nothing, oDataRow("PriorityTagLoan_CheckOut")), DateTime)
        moCheckIn = CType(IIf(Convert.IsDBNull(oDataRow("PriorityTagLoan_CheckIn")), Nothing, oDataRow("PriorityTagLoan_CheckIn")), DateTime)
    End Sub

    Public ReadOnly Property ID() As String
        Get
            Return msPriorityTagID
        End Get
    End Property

    Public ReadOnly Property ContactID() As Integer
        Get
            Return moContactID
        End Get
    End Property

    Public ReadOnly Property Contact() As CContact
        Get
            Dim oContact As New CContact
            oContact.LoadByID(ContactID)
            Return oContact
        End Get
    End Property

    Public ReadOnly Property ConferenceID() As Integer
        Get
            Return miConferenceID
        End Get
    End Property

    Public ReadOnly Property Conference() As External.Events.CConference
        Get
            Return Lists.External.Events.GetConferenceByID(ConferenceID)
        End Get
    End Property

    Public ReadOnly Property VolunteerDepartmentID() As Integer
        Get
            Return miVolunteerDepartmentID
        End Get
    End Property

    Public ReadOnly Property VolunteerDepartment() As External.Events.CVolunteerDepartment
        Get
            Return Lists.External.Events.VolunteerDepartments(VolunteerDepartmentID)
        End Get
    End Property

    Public ReadOnly Property AccessLevelID() As Integer
        Get
            Return miAccessLevelID
        End Get
    End Property

    Public ReadOnly Property AccessLevel() As CAccessLevel
        Get
            Return Lists.External.Events.AccessLevels()(AccessLevelID)
        End Get
    End Property

    Public ReadOnly Property CheckOutDate() As DateTime
        Get
            Return moCheckOut
        End Get
    End Property

    Public ReadOnly Property CheckInDate() As DateTime
        Get
            Return moCheckIn
        End Get
    End Property
End Class
