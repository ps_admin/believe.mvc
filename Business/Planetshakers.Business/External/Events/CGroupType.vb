Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CGroupType

        Private miGroupTypeID As Integer
        Private msName As String

        Public Sub New()

            miGroupTypeID = 0
            msName = String.Empty

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miGroupTypeID = oDataRow("GroupType_ID")
            msName = oDataRow("Name")

        End Sub
        Public Property ID() As Integer
            Get
                Return miGroupTypeID
            End Get
            Set(ByVal Value As Integer)
                miGroupTypeID = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property

        Public Overrides Function ToString() As String
            Return Me.Name
        End Function

    End Class

End Namespace