Namespace External.Events

    Public Module MEvents

        Public Function SummaryReportAttendance(ByVal oConference As CConference, ByVal oState As CState) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))

            Return DataAccess.GetDataTable("sp_Events_ReportSummaryAttendance", oParameters)

        End Function

        Public Function SummaryReportRegistrationTypes(ByVal oConference As CConference, ByVal oState As CState) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))

            Return DataAccess.GetDataTable("sp_Events_ReportSummaryRegistrationTypes ", oParameters)

        End Function

        Public Function SummaryReportState(ByVal oConference As CConference, ByVal oState As CState) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))

            Return DataAccess.GetDataTable("sp_Events_ReportSummaryState", oParameters)

        End Function

        Public Function SummaryReportAge(ByVal oConference As CConference, ByVal oState As CState) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))

            Return DataAccess.GetDataTable("sp_Events_ReportSummaryAge", oParameters)

        End Function

        Public Function SummaryReportReferredBy(ByVal oConference As CConference, ByVal oState As CState) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))

            Return DataAccess.GetDataTable("sp_Events_ReportSummaryReferredBy", oParameters)

        End Function

        Public Function SummaryReportElectives(ByVal oConference As CConference, ByVal oState As CState) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))

            Return DataAccess.GetDataTable("sp_Events_ReportSummaryElectives", oParameters)

        End Function

        Public Function SummaryReportVIP(ByVal oConference As CConference, ByVal oState As CState) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))

            Return DataAccess.GetDataTable("sp_Events_ReportSummaryVIP", oParameters)

        End Function

        Public Function SummaryReportPreOrders(ByVal oConference As CConference, ByVal oState As CState) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))

            Return DataAccess.GetDataTable("sp_Events_ReportSummaryPreOrders", oParameters)

        End Function

        Public Function SummaryReportPlanetKids(ByVal oConference As CConference, ByVal oState As CState) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))

            Return DataAccess.GetDataTable("sp_Events_ReportSummaryPlanetKids", oParameters)

        End Function

        Public Function GetOnlineRegistrationTypes(ByVal oVenue As CVenue, ByVal oContact As CContact, ByVal oGroup As CGroup, ByVal dDate As Date) As CArrayList(Of CRegistrationType)

            Dim oRegistrationTypes As New CArrayList(Of CRegistrationType)
            Dim oRegistrationType As CRegistrationType
            Dim oSelectedRegistrationType As CRegistrationType = Nothing

            For Each oRegistrationTypeSaleGroup As CRegistrationTypeSaleGroup In Lists.External.Events.RegistrationTypeSaleGroups.Values
                If oRegistrationTypeSaleGroup.Conference.ID = oVenue.Conference.ID Then

                    Dim oRegistrationTypeList As ArrayList = Lists.External.Events.RegistrationTypesByVenue(oVenue)

                    'Remove Mighty Men Father/Son Registration Types
                    For Each oRT As CRegistrationType In oRegistrationTypeList.Clone
                        If oRT.ID >= 768 And oRT.ID <= 775 Then oRegistrationTypeList.Remove(oRT)
                    Next
                    If Not oContact Is Nothing Then
                        For Each oRT As CRegistrationType In oRegistrationTypeList.Clone
                            If oRT.Name.Contains("Family") Then oRegistrationTypeList.Remove(oRT)
                        Next
                    End If
                    For Each oRegistrationType In oRegistrationTypeList
                        If oRegistrationType.RegistrationTypeSaleGroupID = oRegistrationTypeSaleGroup.ID Then
                            If CDate(Format(oRegistrationType.StartDate, "dd MMM yyyy HH:MM").ToString).CompareTo(CDate(Format(dDate, "dd MMM yyyy HH:MM").ToString)) <= 0 Then
                                If CDate(Format(oRegistrationType.EndDate, "dd MMM yyyy HH:MM").ToString).CompareTo(CDate(Format(dDate, "dd MMM yyyy HH:MM").ToString)) >= 0 Then
                                    If (oRegistrationType.AvailableOnlineSingle And oGroup Is Nothing) Or _
                                       (oRegistrationType.AvailableOnlineGroup And oGroup IsNot Nothing) Then
                                        'we only want to add the cheapest registration type from each registration sale group.
                                        If oSelectedRegistrationType Is Nothing OrElse oSelectedRegistrationType.RegistrationCost > oRegistrationType.RegistrationCost Then
                                            oSelectedRegistrationType = oRegistrationType
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next
                    If oSelectedRegistrationType IsNot Nothing Then
                        oRegistrationTypes.Add(oSelectedRegistrationType)
                        oSelectedRegistrationType = Nothing
                    End If
                End If
            Next

            'Remove registration types that fall outside the user's date of birth.
            If oContact IsNot Nothing AndAlso oContact.DateOfBirth <> Nothing Then
                For Each oRegistrationType In oRegistrationTypes.Clone
                    If oGroup Is Nothing Then
                        If oRegistrationType.RegistrationTypeSaleGroup.MaximumAge < GetAgeDecimal(oContact.DateOfBirth, oVenue.ConferenceStartDate) Or _
                           oRegistrationType.RegistrationTypeSaleGroup.MinimumAge > GetAgeDecimal(oContact.DateOfBirth, oVenue.ConferenceStartDate) Then
                            oRegistrationTypes.Remove(oRegistrationType)
                        End If
                    End If
                Next
            End If

            'Remove Planetkids if it's closed
            For Each oRegistrationType In oRegistrationTypes.Clone
                If oRegistrationType.IsChildRegistrationType Then
                    If oVenue.PlanetkidsEarlyChildhoodClosed Then
                        If oRegistrationType.Name.Contains("Early Childhood") Then oRegistrationTypes.Remove(oRegistrationType)
                    End If
                    If oVenue.PlanetkidsPrimaryClosed Then
                        If oRegistrationType.Name.Contains("Primary") Then oRegistrationTypes.Remove(oRegistrationType)
                    End If
                End If
            Next


            oRegistrationTypes.Sort()
            Return oRegistrationTypes

        End Function

        Public Function GetOnlineRegistrationTypes(ByVal oRegistrationTypeCombo As CRegistrationTypeCombo, ByVal oContact As CContact, ByVal oGroup As CGroup, ByVal dDate As Date) As CArrayList(Of CRegistrationTypeComboDetail)

            Dim oComboDetails As New CArrayList(Of CRegistrationTypeComboDetail)

            For Each oComboDetail As CRegistrationTypeComboDetail In Lists.External.Events.RegistrationTypeComboDetail.Values
                If oComboDetail.RegistrationTypeComboID = oRegistrationTypeCombo.ID Then

                    Dim oRegistationType1 As CRegistrationType = Lists.External.Events.GetRegistrationTypeByID(oComboDetail.RegistrationTypeID1)
                    Dim oRegistationType2 As CRegistrationType = Lists.External.Events.GetRegistrationTypeByID(oComboDetail.RegistrationTypeID2)

                    'Check that both RegistrationTypes fall within the right parameters.

                    If CDate(Format(oRegistationType1.StartDate, "dd MMM yyyy HH:MM").ToString).CompareTo(CDate(Format(dDate, "dd MMM yyyy HH:MM").ToString)) <= 0 Then
                        If CDate(Format(oRegistationType1.EndDate, "dd MMM yyyy HH:MM").ToString).CompareTo(CDate(Format(dDate, "dd MMM yyyy HH:MM").ToString)) >= 0 Then
                            If (oRegistationType1.AvailableOnlineSingle And oGroup Is Nothing) Or _
                               (oRegistationType1.AvailableOnlineGroup And oGroup IsNot Nothing) Then
                                If oContact Is Nothing OrElse _
                                   (oRegistationType1.RegistrationTypeSaleGroup.MaximumAge > GetAgeDecimal(oContact.DateOfBirth, oRegistrationTypeCombo.Venue1.ConferenceStartDate) Or _
                                   oRegistationType1.RegistrationTypeSaleGroup.MinimumAge < GetAgeDecimal(oContact.DateOfBirth, oRegistrationTypeCombo.Venue1.ConferenceStartDate)) Then

                                    If CDate(Format(oRegistationType2.StartDate, "dd MMM yyyy HH:MM").ToString).CompareTo(CDate(Format(dDate, "dd MMM yyyy HH:MM").ToString)) <= 0 Then
                                        If CDate(Format(oRegistationType2.EndDate, "dd MMM yyyy HH:MM").ToString).CompareTo(CDate(Format(dDate, "dd MMM yyyy HH:MM").ToString)) >= 0 Then
                                            If (oRegistationType2.AvailableOnlineSingle And oGroup Is Nothing) Or _
                                               (oRegistationType2.AvailableOnlineGroup And oGroup IsNot Nothing) Then
                                                If oContact Is Nothing OrElse _
                                                   (oRegistationType2.RegistrationTypeSaleGroup.MaximumAge > GetAgeDecimal(oContact.DateOfBirth, oRegistrationTypeCombo.Venue2.ConferenceStartDate) Or _
                                                    oRegistationType2.RegistrationTypeSaleGroup.MinimumAge < GetAgeDecimal(oContact.DateOfBirth, oRegistrationTypeCombo.Venue2.ConferenceStartDate)) Then

                                                    oComboDetails.Add(oComboDetail)

                                                End If
                                            End If
                                        End If

                                    End If
                                End If
                            End If
                        End If

                    End If
                End If
            Next

            Return oComboDetails

        End Function

#Region "Group Functions"

        Public Function GroupRegistrationsPurchasedTable(ByVal oGroup As CGroup, ByVal oConference As CConference) As DataTable

            Dim oRegistrationsPurchasedTable As New DataTable
            oRegistrationsPurchasedTable.Columns.Add("Item", Type.GetType("System.String"))
            oRegistrationsPurchasedTable.Columns.Add("Purchased", Type.GetType("System.Int32"))
            oRegistrationsPurchasedTable.Columns.Add("Allocated", Type.GetType("System.Int32"))
            oRegistrationsPurchasedTable.Columns.Add("Remaining", Type.GetType("System.Int32"))


            'Put together a list of purchased registrations, so we can work out which one are the combo ones, then account for and remove them.
            Dim cRegistrationPurchasedList As New ArrayList
            For Each oBulkRegistration As CGroupBulkRegistration In oGroup.BulkRegistrations
                cRegistrationPurchasedList.Add(New Object() {oBulkRegistration, oBulkRegistration.Quantity})
            Next

            'Put together a list of allocated registrations, so we can work out which one are the combo ones, then account for and remove them.
            Dim cRegistrationAllocatedList As New CArrayList(Of CRegistration)
            For Each oContact As CContact In oGroup.Contacts
                cRegistrationAllocatedList.AddRange(oContact.External.Events.Registrations(Nothing, oGroup.ID))
            Next

            'Sort out the Combo Registrations before all others
            For Each oComboDetail As CRegistrationTypeComboDetail In Lists.External.Events.RegistrationTypeComboDetail.Values
                If oComboDetail.RegistrationTypeCombo.ConferenceID = oConference.ID Then

                    Dim oDataRow As DataRow = oRegistrationsPurchasedTable.NewRow
                    oDataRow("Item") = "Registration: " & oComboDetail.RegistrationTypeCombo.Name & " - " & oComboDetail.Name

                    oDataRow("Purchased") = CheckPurchasedForCombo(cRegistrationPurchasedList, oComboDetail)
                    oDataRow("Allocated") = CheckAllocatedForCombo(cRegistrationAllocatedList, oComboDetail).Count

                    If oDataRow("Purchased") > 0 Or oDataRow("Allocated") > 0 Then
                        oDataRow("Remaining") = oDataRow("Purchased") - oDataRow("Allocated")
                        oRegistrationsPurchasedTable.Rows.Add(oDataRow)
                    End If

                End If
            Next


            For Each oVenue As CVenue In Lists.External.Events.ConferenceVenues(oConference).Values
                For Each oRegistrationType As CRegistrationType In Lists.External.Events.RegistrationTypesByVenue(oVenue)
                    If oRegistrationType.ConferenceID = oConference.ID Then

                        Dim oDataRow As DataRow = oRegistrationsPurchasedTable.NewRow
                        oDataRow("Item") = "Registration: " & oVenue.Name & " - " & oRegistrationType.Name
                        oDataRow("Purchased") = 0
                        oDataRow("Allocated") = 0

                        For Each oObj As Object In cRegistrationPurchasedList
                            If oObj(0).Venue.ID = oVenue.ID And oObj(0).RegistrationTypeID = oRegistrationType.ID Then
                                oDataRow("Purchased") += oObj(1)
                            End If
                        Next

                        For Each oRegistration As CRegistration In cRegistrationAllocatedList
                            If oRegistration.Venue.ID = oVenue.ID And oRegistration.RegistrationType.ID = oRegistrationType.ID Then
                                oDataRow("Allocated") += 1
                            End If
                        Next

                        If oDataRow("Purchased") > 0 Or oDataRow("Allocated") > 0 Then
                            oDataRow("Remaining") = oDataRow("Purchased") - oDataRow("Allocated")
                            oRegistrationsPurchasedTable.Rows.Add(oDataRow)
                        End If

                    End If
                Next
            Next

            For Each oVenue As CVenue In Lists.External.Events.ConferenceVenues(oConference).Values

                'Accommodation
                Dim oAccommodationDataRow As DataRow = oRegistrationsPurchasedTable.NewRow
                oAccommodationDataRow("Purchased") = 0
                oAccommodationDataRow("Allocated") = 0
                oAccommodationDataRow("Item") = oVenue.Name & " - " & "Budget Accommodation"

                'Catering
                Dim oCateringDataRow As DataRow = oRegistrationsPurchasedTable.NewRow
                oCateringDataRow("Purchased") = 0
                oCateringDataRow("Allocated") = 0
                oCateringDataRow("Item") = oVenue.Name & " - " & "Budget Catering"

                'Leadership Breakfast
                Dim oLeadershipBreakfastRow As DataRow = oRegistrationsPurchasedTable.NewRow
                oLeadershipBreakfastRow("Purchased") = 0
                oLeadershipBreakfastRow("Allocated") = 0
                oLeadershipBreakfastRow("Item") = oVenue.Name & " - " & "Leadership Breakfast"

                For Each oBulkPurchase As CGroupBulkPurchase In oGroup.BulkPurchase
                    If oBulkPurchase.Venue.ID = oVenue.ID Then
                        oAccommodationDataRow("Purchased") += oBulkPurchase.AccommodationQuantity
                        oCateringDataRow("Purchased") += oBulkPurchase.CateringQuantity
                        oLeadershipBreakfastRow("Purchased") += oBulkPurchase.LeadershipBreakfastQuantity
                    End If
                Next

                For Each oContact As CContact In oGroup.Contacts
                    For Each oRegistration As CRegistration In oContact.External.Events.Registrations(oConference, oGroup.ID)
                        If oRegistration.Venue.ID = oVenue.ID Then
                            If oRegistration.GroupLeaderID = oGroup.ID Then
                                If oRegistration.Accommodation Then oAccommodationDataRow("Allocated") += 1
                                If oRegistration.Catering Then oCateringDataRow("Allocated") += 1
                                If oRegistration.LeadershipBreakfast Then oLeadershipBreakfastRow("Allocated") += 1
                            End If
                        End If
                    Next
                Next

                oAccommodationDataRow("Remaining") = oAccommodationDataRow("Purchased") - oAccommodationDataRow("Allocated")
                oCateringDataRow("Remaining") = oCateringDataRow("Purchased") - oCateringDataRow("Allocated")
                oLeadershipBreakfastRow("Remaining") = oLeadershipBreakfastRow("Purchased") - oLeadershipBreakfastRow("Allocated")

                If oConference.AccommodationAvailable And (oAccommodationDataRow("Purchased") > 0 Or oAccommodationDataRow("Allocated") > 0) Then oRegistrationsPurchasedTable.Rows.Add(oAccommodationDataRow)
                If oConference.CateringAvailable And (oCateringDataRow("Purchased") > 0 Or oCateringDataRow("Allocated") > 0) Then oRegistrationsPurchasedTable.Rows.Add(oCateringDataRow)

                If oConference.LeadershipBreakfastAvailable And (oLeadershipBreakfastRow("Purchased") > 0 Or oLeadershipBreakfastRow("Allocated") > 0) Then oRegistrationsPurchasedTable.Rows.Add(oLeadershipBreakfastRow)

            Next

            Return oRegistrationsPurchasedTable

        End Function

        Public Function GroupCurrentRegistrantTable(ByVal oGroup As CGroup, ByVal oConference As CConference, Optional ByVal sSortExpression As String = "", Optional ByVal sSortDirection As String = "") As DataView

            Dim oCurrentRegistrantsTable As New DataTable
            oCurrentRegistrantsTable.Columns.Add("GUID", Type.GetType("System.String"))
            oCurrentRegistrantsTable.Columns.Add("Contact_ID", Type.GetType("System.Int32"))
            oCurrentRegistrantsTable.Columns.Add("Registration_ID", Type.GetType("System.Int32"))
            oCurrentRegistrantsTable.Columns.Add("SecondaryRegistration_ID", Type.GetType("System.Int32"))
            oCurrentRegistrantsTable.Columns.Add("ComboDetail_ID", Type.GetType("System.Int32"))
            oCurrentRegistrantsTable.Columns.Add("Name", Type.GetType("System.String"))
            oCurrentRegistrantsTable.Columns.Add("FirstName", Type.GetType("System.String"))
            oCurrentRegistrantsTable.Columns.Add("LastName", Type.GetType("System.String"))
            oCurrentRegistrantsTable.Columns.Add("Registration", Type.GetType("System.String"))
            oCurrentRegistrantsTable.Columns.Add("Catering", Type.GetType("System.String"))
            oCurrentRegistrantsTable.Columns.Add("Accommodation", Type.GetType("System.String"))
            oCurrentRegistrantsTable.Columns.Add("PlanetKids", Type.GetType("System.String"))
            oCurrentRegistrantsTable.Columns.Add("Other", Type.GetType("System.String"))
            oCurrentRegistrantsTable.Columns.Add("TotalPrice", Type.GetType("System.Decimal"))


            'Put together a list of purchased registrations, so we can work out which one are the combo ones, then account for and remove them.
            Dim cRegistrationPurchasedList As New ArrayList
            For Each oBulkRegistration As CGroupBulkRegistration In oGroup.BulkRegistrations
                If oBulkRegistration.Venue.ConferenceID = oConference.ID Then
                    cRegistrationPurchasedList.Add(New Object() {oBulkRegistration, oBulkRegistration.Quantity})
                End If
            Next

            'Put together a list of allocated registrations, so we can work out which one are the combo ones, then account for and remove them.
            Dim cRegistrationAllocatedList As New CArrayList(Of CRegistration)
            For Each oContact As CContact In oGroup.Contacts
                cRegistrationAllocatedList.AddRange(oContact.External.Events.Registrations(Nothing, oGroup.ID))
            Next


            'Sort out the Combo Registrations before all others
            For Each oComboDetail As CRegistrationTypeComboDetail In Lists.External.Events.RegistrationTypeComboDetail.Values
                If oComboDetail.RegistrationTypeCombo.ConferenceID = oConference.ID Then

                    Dim oArraylist As ArrayList = CheckAllocatedForCombo(cRegistrationAllocatedList, oComboDetail)

                    For Each oObj As Object() In oArraylist

                        Dim oContact As CContact = oGroup.Contacts.GetItemByID(oObj(0))
                        Dim oPrimaryRegistration As CRegistration = oObj(1)
                        Dim oSecondaryRegistration As CRegistration = oObj(2)

                        Dim oDataRow As DataRow = oCurrentRegistrantsTable.NewRow
                        oDataRow("GUID") = oContact.GUID
                        oDataRow("Contact_ID") = oPrimaryRegistration.ContactID
                        oDataRow("Registration_ID") = oPrimaryRegistration.ID
                        oDataRow("SecondaryRegistration_ID") = oSecondaryRegistration.ID
                        oDataRow("ComboDetail_ID") = oComboDetail.ID
                        oDataRow("Name") = oContact.Name
                        oDataRow("FirstName") = oContact.FirstName
                        oDataRow("LastName") = oContact.LastName
                        oDataRow("Registration") = oComboDetail.RegistrationTypeCombo.Name & " - " & oComboDetail.Name & " - " & Format(oPrimaryRegistration.RegistrationType.RegistrationCost + oSecondaryRegistration.RegistrationType.RegistrationCost, "$#0.00")
                        oDataRow("Accommodation") = IIf(oPrimaryRegistration.Accommodation, "Yes - " & Format(oConference.AccommodationCost, "$#0"), "No")
                        oDataRow("Catering") = IIf(oPrimaryRegistration.Catering, "Yes - " & Format(oConference.CateringCost, "$#0"), "No")
                        oDataRow("Other") = IIf(oPrimaryRegistration.LeadershipBreakfast, "Leadership Breakfast - " & Format(oConference.LeadershipBreakfastCost, "$#0"), "None")
                        oDataRow("TotalPrice") = oPrimaryRegistration.TotalCost + oSecondaryRegistration.TotalCost
                        oCurrentRegistrantsTable.Rows.Add(oDataRow)
                    Next

                End If
            Next

            'Now add in the rest of the registrations
            For Each oRegistration As CRegistration In cRegistrationAllocatedList
                If oRegistration.Venue.ConferenceID = oConference.ID Then

                    Dim oContact As CContact = oGroup.Contacts.GetItemByID(oRegistration.ContactID)

                    Dim oDataRow As DataRow = oCurrentRegistrantsTable.NewRow
                    oDataRow("GUID") = oContact.GUID
                    oDataRow("Contact_ID") = oRegistration.ContactID
                    oDataRow("Registration_ID") = oRegistration.ID
                    oDataRow("Name") = oContact.Name
                    oDataRow("FirstName") = oContact.FirstName
                    oDataRow("LastName") = oContact.LastName
                    oDataRow("Registration") = oRegistration.Venue.Name & " - " & oRegistration.RegistrationType.Name & " - " & Format(oRegistration.RegistrationType.RegistrationCost, "$#0.00")
                    oDataRow("Accommodation") = IIf(oRegistration.Accommodation, "Yes - " & Format(oConference.AccommodationCost, "$#0"), "No")
                    oDataRow("Catering") = IIf(oRegistration.Catering, "Yes - " & Format(oConference.CateringCost, "$#0"), "No")
                    oDataRow("Other") = IIf(oRegistration.LeadershipBreakfast, "Leadership Breakfast - " & Format(oConference.LeadershipBreakfastCost, "$#0"), "None")
                    oDataRow("TotalPrice") = oRegistration.TotalCost
                    oCurrentRegistrantsTable.Rows.Add(oDataRow)

                End If
            Next

            Dim oDataView As DataView = oCurrentRegistrantsTable.DefaultView
            If sSortExpression <> String.Empty Then oDataView.Sort = sSortExpression & sSortDirection

            Return oDataView

        End Function

        Public Function GroupPreviousRegistrantTable(ByVal oGroup As CGroup, ByVal oConference As CConference) As DataTable

            Dim oPreviousRegistrantsTable As New DataTable
            oPreviousRegistrantsTable.Columns.Add("GUID", Type.GetType("System.String"))
            oPreviousRegistrantsTable.Columns.Add("Name", Type.GetType("System.String"))
            oPreviousRegistrantsTable.Columns.Add("FirstName", Type.GetType("System.String"))
            oPreviousRegistrantsTable.Columns.Add("LastName", Type.GetType("System.String"))
            oPreviousRegistrantsTable.Columns.Add("Gender", Type.GetType("System.String"))
            oPreviousRegistrantsTable.Columns.Add("LastConferenceRegistered", Type.GetType("System.String"))

            Dim oContact As CContact

            For Each oContact In oGroup.Contacts
                If Not oContact.External.Events.HasCurrentRegistration(oGroup.ID, oConference) Then
                    Dim oDataRow As DataRow = oPreviousRegistrantsTable.NewRow
                    oDataRow("Name") = oContact.Name
                    oDataRow("FirstName") = oContact.FirstName
                    oDataRow("LastName") = oContact.LastName
                    oDataRow("Gender") = oContact.Gender
                    If oContact.External.Events.GetLatestRegistration IsNot Nothing Then
                        oDataRow("LastConferenceRegistered") = oContact.External.Events.GetLatestRegistration.Venue.Conference.Name
                    Else
                        oDataRow("LastConferenceRegistered") = String.Empty
                    End If
                    oDataRow("GUID") = oContact.GUID
                    oPreviousRegistrantsTable.Rows.Add(oDataRow)
                End If
            Next

            Return oPreviousRegistrantsTable

        End Function

        Public Function GroupRegistrationsPurchasedDetailTable(ByVal oGroup As CGroup, ByVal oConference As CConference) As DataTable

            Dim oRegistrationsPurchasedTable As New DataTable
            oRegistrationsPurchasedTable.Columns.Add("SortOrder", Type.GetType("System.Int32"))
            oRegistrationsPurchasedTable.Columns.Add("Date", Type.GetType("System.DateTime"))
            oRegistrationsPurchasedTable.Columns.Add("Item", Type.GetType("System.String"))
            oRegistrationsPurchasedTable.Columns.Add("Quantity", Type.GetType("System.String"))
            oRegistrationsPurchasedTable.Columns.Add("Price", Type.GetType("System.Decimal"))
            oRegistrationsPurchasedTable.Columns.Add("GUID", Type.GetType("System.String"))

            Dim oDataRow As DataRow
            Dim oGroupBulkRegistration As CGroupBulkRegistration
            Dim oGroupBulkPurchase As CGroupBulkPurchase

            For Each oGroupBulkRegistration In oGroup.BulkRegistrations
                If oGroupBulkRegistration.Venue.Conference.ID = oConference.ID Then
                    oDataRow = oRegistrationsPurchasedTable.NewRow
                    oDataRow("SortOrder") = 1
                    oDataRow("Date") = CDate(oGroupBulkRegistration.DateAdded)
                    oDataRow("Item") = "Registration - " & oGroupBulkRegistration.Venue.Name & " - " & oGroupBulkRegistration.RegistrationType.Name
                    oDataRow("Quantity") = oGroupBulkRegistration.Quantity
                    oDataRow("Price") = oGroupBulkRegistration.TotalCost
                    oDataRow("GUID") = oGroupBulkRegistration.GUID
                    oRegistrationsPurchasedTable.Rows.Add(oDataRow)
                End If
            Next

            For Each oGroupBulkPurchase In oGroup.BulkPurchase
                If oGroupBulkPurchase.Venue.Conference.ID = oConference.ID Then
                    If oGroupBulkPurchase.AccommodationQuantity > 0 Then
                        oDataRow = oRegistrationsPurchasedTable.NewRow
                        oDataRow("SortOrder") = 1
                        oDataRow("Date") = CDate(oGroupBulkPurchase.DateAdded)
                        oDataRow("Item") = "Budget Accommodation"
                        oDataRow("Quantity") = oGroupBulkPurchase.AccommodationQuantity
                        oDataRow("Price") = oGroupBulkPurchase.AccommodationCost
                        oDataRow("GUID") = oGroupBulkPurchase.GUID
                        oRegistrationsPurchasedTable.Rows.Add(oDataRow)
                    End If
                    If oGroupBulkPurchase.CateringQuantity > 0 Then
                        oDataRow = oRegistrationsPurchasedTable.NewRow
                        oDataRow("SortOrder") = 1
                        oDataRow("Date") = CDate(oGroupBulkPurchase.DateAdded)
                        oDataRow("Item") = "Budget Catering"
                        oDataRow("Quantity") = oGroupBulkPurchase.CateringQuantity
                        oDataRow("Price") = oGroupBulkPurchase.CateringCost
                        oDataRow("GUID") = oGroupBulkPurchase.GUID
                        oRegistrationsPurchasedTable.Rows.Add(oDataRow)
                    End If
                    If oGroupBulkPurchase.LeadershipBreakfastQuantity > 0 Then
                        oDataRow = oRegistrationsPurchasedTable.NewRow
                        oDataRow("SortOrder") = 1
                        oDataRow("Date") = CDate(oGroupBulkPurchase.DateAdded)
                        oDataRow("Item") = "Leadership Breakfast"
                        oDataRow("Quantity") = oGroupBulkPurchase.LeadershipBreakfastQuantity
                        oDataRow("Price") = oGroupBulkPurchase.LeadershipBreakfastCost
                        oDataRow("GUID") = oGroupBulkPurchase.GUID
                        oRegistrationsPurchasedTable.Rows.Add(oDataRow)
                    End If
                End If
            Next

            Return oRegistrationsPurchasedTable

        End Function

        Public Function GroupBuyRegistrationsTable(ByVal oGroup As CGroup, ByVal oConference As CConference, ByVal bOnline As Boolean) As DataTable

            Dim oDataTable As New DataTable
            oDataTable.Columns.Add("RegistrationType_ID", Type.GetType("System.Int32"))
            oDataTable.Columns.Add("Venue_ID", Type.GetType("System.Int32"))
            oDataTable.Columns.Add("ComboDetail_ID", Type.GetType("System.Int32"))
            oDataTable.Columns.Add("BulkPurchaseType", Type.GetType("System.Int32"))
            oDataTable.Columns.Add("Item", Type.GetType("System.String"))
            oDataTable.Columns.Add("ItemPrice", Type.GetType("System.Decimal"))
            oDataTable.Columns.Add("Quantity", Type.GetType("System.Int32"))
            oDataTable.Columns.Add("TotalPrice", Type.GetType("System.Decimal"))

            Dim oVenue As CVenue
            Dim oRegistrationType As CRegistrationType
            If bOnline Then
                For Each oVenue In Lists.External.Events.ConferenceVenuesCurrent(oConference).Values
                    If Not oVenue.IsClosed Then
                        For Each oRegistrationType In GetOnlineRegistrationTypes(oVenue, Nothing, oGroup, GetDateTime)
                            If Not oRegistrationType.IsComboRegistration Then
                                'Exclude 'Youth Leader' registration types for non-youth groups
                                If Not (oRegistrationType.Name.Contains("Youth Camp") Or oRegistrationType.Name.Contains("Catering")) Or oGroup.GroupTypeID = EnumGroupType.YouthGroup Then
                                    Dim oRow As DataRow = oDataTable.NewRow
                                    oRow("RegistrationType_ID") = oRegistrationType.ID
                                    oRow("Venue_ID") = oVenue.ID
                                    oRow("BulkPurchaseType") = 0
                                    oRow("Item") = oVenue.Name & " - Registration - " & oRegistrationType.Name
                                    If oRegistrationType.GroupMinimumQuantity > 1 Then
                                        oRow("Item") += " (" & oRegistrationType.GroupMinimumQuantity & "+)"
                                    End If
                                    oRow("ItemPrice") = oRegistrationType.RegistrationCost
                                    oRow("Quantity") = 0
                                    oRow("TotalPrice") = 0
                                    oDataTable.Rows.Add(oRow)
                                End If
                            End If
                        Next
                    End If
                Next

                For Each oRegistrationCombo As CRegistrationTypeCombo In Lists.External.Events.RegistrationTypeCombo.Values
                    If oRegistrationCombo.ConferenceID = oConference.ID Then
                        For Each oComboDetail As CRegistrationTypeComboDetail In GetOnlineRegistrationTypes(oRegistrationCombo, Nothing, oGroup, GetDateTime)

                            If oComboDetail.RegistrationType1.AvailableOnlineGroup And oComboDetail.RegistrationType2.AvailableOnlineGroup Then
                                Dim oRow As DataRow = oDataTable.NewRow
                                oRow("ComboDetail_ID") = oComboDetail.ID
                                oRow("BulkPurchaseType") = 0
                                oRow("Item") = oComboDetail.RegistrationTypeCombo.Name & " - Registration - " & oComboDetail.Name
                                If oComboDetail.RegistrationType1.GroupMinimumQuantity > 1 Or oComboDetail.RegistrationType2.GroupMinimumQuantity > 1 Then
                                    oRow("Item") += " (" & Math.Max(oComboDetail.RegistrationType1.GroupMinimumQuantity, oComboDetail.RegistrationType2.GroupMinimumQuantity) & "+)"
                                End If
                                oRow("ItemPrice") = oComboDetail.RegistrationType1.RegistrationCost + oComboDetail.RegistrationType2.RegistrationCost
                                oRow("Quantity") = 0
                                oRow("TotalPrice") = 0
                                oDataTable.Rows.Add(oRow)
                            End If
                        Next
                    End If
                Next
            Else
                For Each oVenue In Lists.External.Events.ConferenceVenues(oConference).Values
                    For Each oRegistrationType In Lists.External.Events.RegistrationTypesByVenue(oVenue)
                        Dim oRow As DataRow = oDataTable.NewRow
                        oRow("RegistrationType_ID") = oRegistrationType.ID
                        oRow("Venue_ID") = oVenue.ID
                        oRow("BulkPurchaseType") = 0
                        oRow("Item") = oVenue.Name & " - Registration - " & oRegistrationType.Name
                        oRow("ItemPrice") = oRegistrationType.RegistrationCost
                        oRow("Quantity") = 0
                        oRow("TotalPrice") = 0
                        oDataTable.Rows.Add(oRow)
                    Next
                Next
            End If

            For Each oVenue In Lists.External.Events.ConferenceVenues(oConference).Values

                'Accommodation
                If oConference.AccommodationAvailable Then
                    'Exclude Accommodation if it has been closed (only online)....
                    If Not bOnline Or (Not oVenue.AccommodationClosed And oGroup.GroupTypeID = EnumGroupType.YouthGroup) Then
                        Dim oRow As DataRow = oDataTable.NewRow
                        oRow("Venue_ID") = oVenue.ID
                        oRow("BulkPurchaseType") = EnumBulkPurchaseType.Accommodation
                        oRow("Item") = oVenue.Name & " - Budget Accommodation"
                        oRow("ItemPrice") = oConference.AccommodationCost
                        oRow("Quantity") = 0
                        oRow("TotalPrice") = 0
                        oDataTable.Rows.Add(oRow)
                    End If
                End If

                'Catering
                If oConference.CateringAvailable Then
                    'Exclude Catering if it has been closed (only online)....
                    If Not bOnline Or (Not oVenue.AccommodationClosed And oGroup.GroupTypeID = EnumGroupType.YouthGroup) Then
                        Dim oRow As DataRow = oDataTable.NewRow
                        oRow("Venue_ID") = oVenue.ID
                        oRow("BulkPurchaseType") = EnumBulkPurchaseType.Catering
                        oRow("Item") = oVenue.Name & " - Budget Catering"
                        oRow("ItemPrice") = oConference.CateringCost
                        oRow("Quantity") = 0
                        oRow("TotalPrice") = 0
                        oDataTable.Rows.Add(oRow)
                    End If
                End If

                'Leadership Breakfast
                If oConference.LeadershipBreakfastAvailable Then
                    Dim oRow As DataRow = oDataTable.NewRow
                    oRow("Venue_ID") = oVenue.ID
                    oRow("BulkPurchaseType") = EnumBulkPurchaseType.LeadershipBreakfast
                    oRow("Item") = oVenue.Name & " - Leadership Breakfast"
                    oRow("ItemPrice") = oConference.LeadershipBreakfastCost
                    oRow("Quantity") = 0
                    oRow("TotalPrice") = 0
                    oDataTable.Rows.Add(oRow)
                End If
            Next

            Return oDataTable

        End Function

        Public Function CheckPurchasedForCombo(ByRef cRegistrationPurchasedList As ArrayList, ByVal oCombo As CRegistrationTypeComboDetail) As Integer

            Dim iCount As Integer = 0
            Dim bSearchAgain As Boolean = True

            While bSearchAgain
                bSearchAgain = False
                For Each oObj As Object In cRegistrationPurchasedList
                    If oObj(0).RegistrationTypeID = oCombo.RegistrationTypeID1 Then
                        For Each oSearchObj As Object In cRegistrationPurchasedList
                            If oSearchObj(0).RegistrationTypeID = oCombo.RegistrationTypeID2 Then
                                oObj(1) -= 1
                                oSearchObj(1) -= 1
                                If oObj(1) = 0 Then cRegistrationPurchasedList.Remove(oObj)
                                If oSearchObj(1) = 0 Then cRegistrationPurchasedList.Remove(oSearchObj)
                                iCount += 1
                                bSearchAgain = True
                                Exit For
                            End If
                        Next
                    End If
                    If bSearchAgain Then Exit For
                Next
            End While

            Return iCount

        End Function

        Public Function CheckAllocatedForCombo(ByRef cRegistrationAllocatedList As CArrayList(Of CRegistration), ByVal oCombo As CRegistrationTypeComboDetail) As ArrayList

            Dim oArrayList As New ArrayList
            Dim bSearchAgain As Boolean = True

            While bSearchAgain
                bSearchAgain = False
                For Each oRegistration As CRegistration In cRegistrationAllocatedList
                    If oRegistration.RegistrationTypeID = oCombo.RegistrationTypeID1 Then
                        For Each oSearchRegistration As CRegistration In cRegistrationAllocatedList
                            If oSearchRegistration.RegistrationTypeID = oCombo.RegistrationTypeID2 Then
                                oArrayList.Add(New Object() {oRegistration.ContactID, oRegistration, oSearchRegistration})
                                cRegistrationAllocatedList.Remove(oRegistration)
                                cRegistrationAllocatedList.Remove(oSearchRegistration)
                                bSearchAgain = True
                                Exit For
                            End If
                        Next
                    End If
                    If bSearchAgain Then Exit For
                Next
            End While

            Return oArrayList

        End Function

        Public Function IsGroupLeader(ByVal iGroupID As Integer) As Boolean

            Return DataAccess.GetValue("SELECT COUNT(*) FROM Events_Group WHERE GroupLeader_ID = " & iGroupID.ToString) > 0

        End Function

        Public Sub ChangeGroupLeader(ByVal iGroupID As Integer, ByVal iNewLeaderID As Integer)

            DataAccess.ExecuteCommand("UPDATE Events_Group SET GroupLeader_ID = " & iNewLeaderID.ToString & " WHERE GroupLeader_ID = " & iGroupID.ToString)

        End Sub

        Public Sub UpdateConsentFormSent(ByVal iRegistrationID As Integer)

            DataAccess.ExecuteCommand("UPDATE Events_RegistrationChildInformation SET ConsentFormSent = 1, ConsentFormSentDate = dbo.GetRelativeDate() WHERE Registration_ID = " & iRegistrationID.ToString)

        End Sub

#End Region

    End Module

End Namespace
