﻿
Imports PdfSharp
Imports PdfSharp.Drawing
Imports PdfSharp.Drawing.Layout
Imports PdfSharp.Pdf
Imports System.IO
Imports System.Net
Imports System.Text
Imports Planetshakers.Business.Internal.Church

Namespace External.Events

    Public Module MPDF

        Public Sub FillRegistrationFormPS11(ByRef oContact As CContact, ByRef oRegistration As CRegistration, ByRef sPDFOutput As String)

            Dim sPDFInput As String = System.IO.Path.GetTempPath & "PS11RegoForm.pdf"
            sPDFOutput = System.IO.Path.GetTempFileName & ".pdf"

            If Not System.IO.File.Exists(sPDFInput) Then
                Dim wc As New System.Net.WebClient
                wc.DownloadFile("http://accounts.planetshakers.com/events/pdf/PS11.pdf", sPDFInput)
            End If

            Dim document As PdfDocument = IO.PdfReader.Open(sPDFInput)


            ' Create an empty page
            Dim page As PdfPage = document.Pages(0)
            Dim gfx As XGraphics = XGraphics.FromPdfPage(page)

            ' Create a font
            Dim font As XFont = New XFont("Trebuchet MS", 7, XFontStyle.Bold)
            Dim fontTick As XFont = New XFont("Wingdings", 10, XFontStyle.Bold)
            Dim fontLarge As XFont = New XFont("Trebuchet MS", 25, XFontStyle.Bold)




            'Section 1
            If oRegistration.GroupLeaderID > 0 Then
                Dim oGroup As New CGroup
                oGroup.Load(oRegistration.GroupLeaderID)
                gfx.DrawString(oGroup.GroupLeader.FirstName, font, XBrushes.Black, New System.Drawing.PointF(72, 154))
                gfx.DrawString(oGroup.GroupLeader.LastName, font, XBrushes.Black, New System.Drawing.PointF(205, 154))
                gfx.DrawString(oGroup.GroupName, font, XBrushes.Black, New System.Drawing.PointF(50, 165))
            End If


            'Section 2
            Select Case CType(oContact.SalutationID, EnumSalutation)
                Case EnumSalutation.Mr
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(37, 210))
                Case EnumSalutation.Mrs
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(62, 210))
                Case EnumSalutation.Miss
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(90, 210))
                Case EnumSalutation.Ms
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(123, 210))
                Case EnumSalutation.Pastor
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(150, 210))
                Case EnumSalutation.Dr
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(184, 210))
                Case Else
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(211, 210))
            End Select
            gfx.DrawString(oContact.FirstName, font, XBrushes.Black, New System.Drawing.PointF(50, 220))
            gfx.DrawString(oContact.LastName, font, XBrushes.Black, New System.Drawing.PointF(180, 220))
            gfx.DrawString(oContact.Address1, font, XBrushes.Black, New System.Drawing.PointF(50, 230))
            gfx.DrawString(oContact.Suburb, font, XBrushes.Black, New System.Drawing.PointF(50, 240))
            gfx.DrawString(oContact.StateText, font, XBrushes.Black, New System.Drawing.PointF(180, 240))
            gfx.DrawString(oContact.CountryText, font, XBrushes.Black, New System.Drawing.PointF(50, 250))
            gfx.DrawString(oContact.Postcode, font, XBrushes.Black, New System.Drawing.PointF(200, 250))
            gfx.DrawString(oContact.PhoneHome, font, XBrushes.Black, New System.Drawing.PointF(55, 260))
            gfx.DrawString(oContact.PhoneWork, font, XBrushes.Black, New System.Drawing.PointF(140, 260))
            gfx.DrawString(oContact.PhoneMobile, font, XBrushes.Black, New System.Drawing.PointF(230, 260))
            gfx.DrawString(Format(oContact.DateOfBirth, "dd MMM yyyy"), font, XBrushes.Black, New System.Drawing.PointF(55, 270))
            Select Case oContact.Gender.ToLower
                Case "male"
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(187, 270))
                Case "female"
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(230, 270))
            End Select
            gfx.DrawString(IIf(oContact.Email = String.Empty, oContact.Email2, oContact.Email), font, XBrushes.Black, New System.Drawing.PointF(40, 280))
            gfx.DrawString(oContact.External.Events.EmergencyContactName, font, XBrushes.Black, New System.Drawing.PointF(40, 300))
            gfx.DrawString(oContact.External.Events.EmergencyContactRelationship, font, XBrushes.Black, New System.Drawing.PointF(190, 300))
            gfx.DrawString(oContact.External.Events.EmergencyContactPhone, font, XBrushes.Black, New System.Drawing.PointF(60, 310))


            'Section 3
            If oContact.External.ChurchName <> String.Empty Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(83, 335))
                gfx.DrawString(oContact.External.ChurchName, font, XBrushes.Black, New System.Drawing.PointF(185, 334))
            Else
                'gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(113, 335))
            End If
            gfx.DrawString(oContact.External.SeniorPastorName, font, XBrushes.Black, New System.Drawing.PointF(100, 344))
            gfx.DrawString(oContact.External.ChurchAddress1, font, XBrushes.Black, New System.Drawing.PointF(80, 354))
            gfx.DrawString(oContact.External.ChurchSuburb, font, XBrushes.Black, New System.Drawing.PointF(50, 364))
            gfx.DrawString(oContact.External.ChurchStateText, font, XBrushes.Black, New System.Drawing.PointF(200, 364))
            gfx.DrawString(oContact.External.ChurchCountryText, font, XBrushes.Black, New System.Drawing.PointF(50, 374))
            gfx.DrawString(oContact.External.ChurchPostcode, font, XBrushes.Black, New System.Drawing.PointF(200, 374))

            Select Case CType(oContact.External.DenominationID, EnumDenomination)
                Case EnumDenomination.ACCAOG
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(83, 384))
                Case EnumDenomination.Anglican
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(121, 384))
                Case EnumDenomination.Bretheran
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(158, 384))
                Case EnumDenomination.Catholic
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(194, 384))
                Case EnumDenomination.Charismatic
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(229, 384))
                Case EnumDenomination.ChristianCityChurch
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 392))
                Case EnumDenomination.ChristianOutreachCentre
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(81, 392))
                Case EnumDenomination.ChurchOfChrist
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(160, 392))
                Case EnumDenomination.FoursquareElim
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(214, 392))
                Case EnumDenomination.Independant
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 401))
                Case EnumDenomination.Lutheran
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(60, 401))
                Case EnumDenomination.Methodist
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(95, 401))
                Case EnumDenomination.Pentecostal
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(135, 401))
                Case EnumDenomination.Presbyterian
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(179, 401))
                Case EnumDenomination.SalvationArmy
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(224, 401))
                Case EnumDenomination.SeventhDayAdventist
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 409))
                Case EnumDenomination.UnitingChurch
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(86, 409))
                Case EnumDenomination.NA, 0
                    'Nothing
                Case Else
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(118, 409))
                    gfx.DrawString(oContact.External.Denomination.Name, font, XBrushes.Black, New System.Drawing.PointF(145, 408))
            End Select

            Dim oContactChurchInvolvement As CContactChurchInvolvement
            For Each oContactChurchInvolvement In oContact.External.ChurchInvolvement
                Select Case CType(oContactChurchInvolvement.ChurchInvolvementID, EnumChurchInvolvement)
                    Case EnumChurchInvolvement.SeniorPastorMinister
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 428))
                    Case EnumChurchInvolvement.YouthPastorLeader
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(86, 428))
                    Case EnumChurchInvolvement.TeamPastorLeader
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(151, 428))
                    Case EnumChurchInvolvement.WorshipPastorLeader
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(214, 428))
                    Case EnumChurchInvolvement.YouthMinistry
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 436))
                    Case EnumChurchInvolvement.ChildrensMinistry
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(66, 436))
                    Case EnumChurchInvolvement.UniversityMinistry
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(127, 436))
                    Case EnumChurchInvolvement.MusicianVocalist
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(187, 436))
                    Case EnumChurchInvolvement.ChurchStaff
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(247, 436))
                    Case EnumChurchInvolvement.VolunteerAtChurch
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 444))
                    Case EnumChurchInvolvement.BehindTheScenes
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(77, 444))
                    Case EnumChurchInvolvement.SocialJusticeCommunity
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(207, 444))
                    Case EnumChurchInvolvement.CongregationMember
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 452))
                End Select
            Next


            'Section 4A
            Select Case oRegistration.RegistrationType.ID
                Case 244, 270
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(96, 490))
                Case 245, 271
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(151, 490))
                Case 246, 272
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(203, 490))
                Case 248, 273
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(250, 490))
                Case 249
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(96, 500))
                Case 250
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(151, 500))
                Case 251
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(203, 500))
                Case 252
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(250, 500))
                Case 253, 275
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(96, 510))
                Case 254, 276
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(151, 510))
                Case 255, 277
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(203, 510))
                Case 256, 278
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(250, 510))
                Case 257
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(96, 521))
                Case 258
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(151, 521))
                Case 259
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(203, 521))
                Case 260
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(250, 521))
                Case 261, 279
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(96, 537))
                Case 262, 280
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(151, 537))
                Case 263, 281
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(203, 537))
                Case 264, 282
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(250, 537))
                Case 265
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(96, 552))
                Case 266
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(151, 552))
                Case 267
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(203, 552))
            End Select


            'Section 4B
            If oRegistration.Accommodation Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(125, 591))
            Else
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(211, 591))
            End If
            If oRegistration.Catering Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(125, 602))
            Else
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(211, 602))
            End If
            gfx.DrawString(oRegistration.CateringNeeds, font, XBrushes.Black, New System.Drawing.PointF(15, 622))



            'Section 4C
            Dim tf As XTextFormatter = New XTextFormatter(gfx)
            tf.Alignment = XParagraphAlignment.Right

            Dim rect As New XRect(187.5, 637.5, 32, 8)
            gfx.DrawRectangle(XBrushes.White, rect)
            tf.DrawString(Format(oRegistration.RegistrationPrice, "#0"), font, XBrushes.Black, rect, XStringFormats.TopLeft)
            gfx.DrawString(Strings.Right(Format(oRegistration.RegistrationPrice, ".00"), 2), font, XBrushes.Black, New System.Drawing.PointF(225, 644.5))

            rect = New XRect(187, 648, 32, 8)
            gfx.DrawRectangle(XBrushes.White, rect)
            tf.DrawString(Format(IIf(oRegistration.Accommodation, oRegistration.Venue.Conference.AccommodationCost, 0), "#0"), font, XBrushes.Black, rect, XStringFormats.TopLeft)
            gfx.DrawString(Strings.Right(Format(IIf(oRegistration.Accommodation, oRegistration.Venue.Conference.AccommodationCost, 0), ".00"), 2), font, XBrushes.Black, New System.Drawing.PointF(225, 654.5))

            rect = New XRect(187, 658, 32, 8)
            gfx.DrawRectangle(XBrushes.White, rect)
            tf.DrawString(Format(IIf(oRegistration.Catering, oRegistration.Venue.Conference.CateringCost, 0), "#0"), font, XBrushes.Black, rect, XStringFormats.TopLeft)
            gfx.DrawString(Strings.Right(Format(IIf(oRegistration.Catering, oRegistration.Venue.Conference.CateringCost, 0), ".00"), 2), font, XBrushes.Black, New System.Drawing.PointF(225, 664.5))

            rect = New XRect(187, 668, 32, 8)
            gfx.DrawRectangle(XBrushes.White, rect)
            tf.DrawString(Format(oRegistration.TotalCost, "#0"), font, XBrushes.Black, rect, XStringFormats.TopLeft)
            gfx.DrawString(Strings.Right(Format(oRegistration.TotalCost, ".00"), 2), font, XBrushes.Black, New System.Drawing.PointF(225, 674.5))


            gfx.DrawRectangle(XBrushes.Black, 15, 680, 280, 60)
            gfx.DrawRectangle(XBrushes.White, 17, 682, 276, 56)
            gfx.DrawString("Paid Online", fontLarge, XBrushes.Black, New System.Drawing.PointF(85, 720))

            'gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 689))
            'gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(44, 689))
            'gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(82, 689))
            'gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(117, 689))
            'gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(146, 689))
            'gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(193, 689))
            'gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(228, 689))


            'Section 5
            If oRegistration.RegistrationType.IsYouthRegistrationType Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(307, 166))
            End If
            If oRegistration.RegistrationType.Name.ToLower.Contains("youth leader") Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(307, 183))
            End If


            'Section 6
            Select Case CType(oRegistration.ChildInformation.SchoolGradeID, EnumSchoolGrade)
                Case EnumSchoolGrade.NA, EnumSchoolGrade.Prep
                    Select Case CInt(GetAge(oContact.DateOfBirth, oRegistration.Venue.ConferenceStartDate))
                        Case 2
                            gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(307, 271))
                        Case 3
                            gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(307, 281))
                        Case 4
                            gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(307, 291))
                        Case 5
                            gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(307, 301))
                    End Select
                Case EnumSchoolGrade.Prep
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(477, 271))
                Case EnumSchoolGrade.Grade1
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(477, 281))
                Case EnumSchoolGrade.Grade2
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(477, 291))
                Case EnumSchoolGrade.Grade3
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(477, 301))
                Case EnumSchoolGrade.Grade4
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(523, 271))
                Case EnumSchoolGrade.Grade5
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(523, 281))
                Case EnumSchoolGrade.Grade6
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(523, 291))
            End Select



            gfx.DrawString(oRegistration.ChildInformation.ChildsFriend, font, XBrushes.Black, New System.Drawing.PointF(380, 334))

            If oContact.External.Events.MedicalAllergies.Trim = String.Empty Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(401, 345))
            Else
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(435, 345))
                gfx.DrawString(oContact.External.Events.MedicalAllergies, font, XBrushes.Black, New System.Drawing.PointF(458, 344))
            End If
            gfx.DrawString(oContact.External.Events.MedicalInformation, font, XBrushes.Black, New System.Drawing.PointF(311, 362))

            gfx.DrawString(oRegistration.ChildInformation.DropOffPickUpName, font, XBrushes.Black, New System.Drawing.PointF(328, 389))
            gfx.DrawString(oRegistration.ChildInformation.DropOffPickUpRelationship, font, XBrushes.Black, New System.Drawing.PointF(482, 389))
            gfx.DrawString(oRegistration.ChildInformation.DropOffPickUpContactNumber, font, XBrushes.Black, New System.Drawing.PointF(352, 399))
            gfx.DrawString(oRegistration.ChildInformation.ChildrensChurchPastor, font, XBrushes.Black, New System.Drawing.PointF(394, 416))


            ' Save the document...
            document.Save(sPDFOutput)


        End Sub

        Public Sub FillRegistrationFormPS12(ByRef oContact As CContact, ByRef oRegistration As CRegistration, ByRef sPDFOutput As String)

            Dim sPDFInput As String = System.IO.Path.GetTempPath & "PS12RegoForm.pdf"
            sPDFOutput = System.IO.Path.GetTempFileName & ".pdf"

            If Not System.IO.File.Exists(sPDFInput) Then
                Dim wc As New System.Net.WebClient
                wc.DownloadFile("http://accounts.planetshakers.com/events/pdf/PS12.pdf", sPDFInput)
            End If

            Dim document As PdfDocument = IO.PdfReader.Open(sPDFInput)


            ' Create an empty page
            Dim page As PdfPage = document.Pages(0)
            Dim gfx As XGraphics = XGraphics.FromPdfPage(page)

            ' Create a font
            Dim font As XFont = New XFont("Trebuchet MS", 7, XFontStyle.Bold)
            Dim fontTick As XFont = New XFont("Wingdings", 10, XFontStyle.Bold)
            Dim fontLarge As XFont = New XFont("Trebuchet MS", 25, XFontStyle.Bold)




            'Section 1
            If oRegistration.GroupLeaderID > 0 Then
                Dim oGroup As New CGroup
                oGroup.Load(oRegistration.GroupLeaderID)
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(241, 82))

                gfx.DrawString(oGroup.GroupLeader.FirstName, font, XBrushes.Black, New System.Drawing.PointF(75, 103))
                gfx.DrawString(oGroup.GroupLeader.LastName, font, XBrushes.Black, New System.Drawing.PointF(75, 114))
                gfx.DrawString(oGroup.GroupName, font, XBrushes.Black, New System.Drawing.PointF(75, 125))
            Else
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(261, 82))
            End If



            'Section 2
            Select Case CType(oContact.SalutationID, EnumSalutation)
                Case EnumSalutation.Mr
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(29, 176))
                Case EnumSalutation.Mrs
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(47, 176))
                Case EnumSalutation.Miss
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(68, 176))
                Case EnumSalutation.Ms
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(91, 176))
                Case EnumSalutation.Pastor
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(111, 176))
                Case EnumSalutation.Dr
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(137, 176))
                Case Else
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(155, 176))
            End Select

            gfx.DrawString(oContact.FirstName, font, XBrushes.Black, New System.Drawing.PointF(40, 188))
            gfx.DrawString(oContact.LastName, font, XBrushes.Black, New System.Drawing.PointF(190, 188))
            gfx.DrawString(oContact.Address1, font, XBrushes.Black, New System.Drawing.PointF(40, 199))
            gfx.DrawString(oContact.Suburb, font, XBrushes.Black, New System.Drawing.PointF(40, 210))
            gfx.DrawString(oContact.StateText, font, XBrushes.Black, New System.Drawing.PointF(190, 210))
            gfx.DrawString(oContact.CountryText, font, XBrushes.Black, New System.Drawing.PointF(40, 222))
            gfx.DrawString(oContact.Postcode, font, XBrushes.Black, New System.Drawing.PointF(190, 222))
            gfx.DrawString(oContact.PhoneWork, font, XBrushes.Black, New System.Drawing.PointF(40, 233))
            gfx.DrawString(oContact.PhoneMobile, font, XBrushes.Black, New System.Drawing.PointF(190, 233))
            gfx.DrawString(oContact.PhoneHome, font, XBrushes.Black, New System.Drawing.PointF(40, 244))
            gfx.DrawString(oContact.Fax, font, XBrushes.Black, New System.Drawing.PointF(190, 244))
            gfx.DrawString(IIf(oContact.Email = String.Empty, oContact.Email2, oContact.Email), font, XBrushes.Black, New System.Drawing.PointF(40, 255))

            gfx.DrawString(oContact.External.Events.EmergencyContactName, font, XBrushes.Black, New System.Drawing.PointF(40, 281))
            gfx.DrawString(oContact.External.Events.EmergencyContactRelationship, font, XBrushes.Black, New System.Drawing.PointF(190, 281))
            gfx.DrawString(oContact.External.Events.EmergencyContactPhone, font, XBrushes.Black, New System.Drawing.PointF(50, 292))


            'Section 3
            If oContact.External.ChurchName <> String.Empty Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(96, 323))
                gfx.DrawString(oContact.External.ChurchName, font, XBrushes.Black, New System.Drawing.PointF(65, 334))
            End If
            gfx.DrawString(oContact.External.SeniorPastorName, font, XBrushes.Black, New System.Drawing.PointF(90, 345))
            gfx.DrawString(oContact.External.ChurchAddress1, font, XBrushes.Black, New System.Drawing.PointF(70, 356))
            gfx.DrawString(oContact.External.ChurchSuburb, font, XBrushes.Black, New System.Drawing.PointF(45, 368))
            gfx.DrawString(oContact.External.ChurchStateText, font, XBrushes.Black, New System.Drawing.PointF(190, 368))
            gfx.DrawString(oContact.External.ChurchCountryText, font, XBrushes.Black, New System.Drawing.PointF(45, 379))
            gfx.DrawString(oContact.External.ChurchPostcode, font, XBrushes.Black, New System.Drawing.PointF(190, 379))

            If oContact.HasInternal Then
                Select Case CType(oContact.Internal.CampusID, EnumChurchCampus)
                    Case EnumChurchCampus.MelbourneCity
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 399))
                    Case EnumChurchCampus.Geelong
                        gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(66, 399))
                End Select
            End If


            'Section 4A
            Select oRegistration.RegistrationType.ID
                Case 318
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(378, 51))
                Case 322
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(378, 62))
                Case 326
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(378, 73))
                Case 330, 334
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(378, 85))
                Case 339
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(534, 51))
                Case 340
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(534, 62))
                Case 341
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(534, 73))
                Case 342
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(482, 96))
                Case 343
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(482, 107))
                Case 344
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(482, 119))
                Case 345
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(506, 119))
                Case 346, 347
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(530, 119))
                Case 319
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(404, 160))
                Case 323
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(404, 171))
                Case 327
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(404, 182))
                Case 331, 335
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(404, 194))
                Case 320
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(474, 160))
                Case 324
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(474, 171))
                Case 328
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(474, 182))
                Case 332, 336
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(474, 194))
                Case 321
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(543, 160))
                Case 325
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(543, 171))
                Case 329
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(543, 182))
                Case 333, 337
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(543, 194))
            End Select


            'Section 4B
            If oRegistration.Accommodation Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(398, 231))
            Else
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(454, 231))
            End If
            If oRegistration.Catering Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(398, 243))
            Else
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(454, 243))
            End If
            gfx.DrawString(oRegistration.CateringNeeds, font, XBrushes.Black, New System.Drawing.PointF(310, 263))



            'Section 4C
            Dim tf As XTextFormatter = New XTextFormatter(gfx)
            tf.Alignment = XParagraphAlignment.Right

            Dim rect As New XRect(443, 294, 32, 8)
            gfx.DrawRectangle(XBrushes.White, rect)
            tf.DrawString(Format(oRegistration.RegistrationPrice, "#0"), font, XBrushes.Black, rect, XStringFormats.TopLeft)
            gfx.DrawString(Strings.Right(Format(oRegistration.RegistrationPrice, ".00"), 2), font, XBrushes.Black, New System.Drawing.PointF(487, 301))

            rect = New XRect(443, 305, 32, 8)
            gfx.DrawRectangle(XBrushes.White, rect)
            tf.DrawString(Format(IIf(oRegistration.Accommodation, oRegistration.Venue.Conference.AccommodationCost, 0), "#0"), font, XBrushes.Black, rect, XStringFormats.TopLeft)
            gfx.DrawString(Strings.Right(Format(IIf(oRegistration.Accommodation, oRegistration.Venue.Conference.AccommodationCost, 0), ".00"), 2), font, XBrushes.Black, New System.Drawing.PointF(487, 312))

            rect = New XRect(443, 316, 32, 8)
            gfx.DrawRectangle(XBrushes.White, rect)
            tf.DrawString(Format(IIf(oRegistration.Catering, oRegistration.Venue.Conference.CateringCost, 0), "#0"), font, XBrushes.Black, rect, XStringFormats.TopLeft)
            gfx.DrawString(Strings.Right(Format(IIf(oRegistration.Catering, oRegistration.Venue.Conference.CateringCost, 0), ".00"), 2), font, XBrushes.Black, New System.Drawing.PointF(487, 323))

            rect = New XRect(443, 328, 32, 8)
            gfx.DrawRectangle(XBrushes.White, rect)
            tf.DrawString(Format(oRegistration.TotalCost, "#0"), font, XBrushes.Black, rect, XStringFormats.TopLeft)
            gfx.DrawString(Strings.Right(Format(oRegistration.TotalCost, ".00"), 2), font, XBrushes.Black, New System.Drawing.PointF(487, 335))


            gfx.DrawRectangle(XBrushes.White, 304, 338, 272, 10)
            gfx.DrawRectangle(XBrushes.Black, 304, 348, 280, 55)
            gfx.DrawRectangle(XBrushes.White, 308, 352, 272, 47)
            gfx.DrawString("Paid Online", fontLarge, XBrushes.Black, New System.Drawing.PointF(377, 383))


            'Section 5
            If oRegistration.RegistrationType.Name.ToLower.Contains("youth leader") Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 476))
            End If

            If oRegistration.RegistrationType.IsYouthRegistrationType Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 460))

                If oContact.DateOfBirth <> Nothing Then
                    gfx.DrawString(GetAge(oContact.DateOfBirth, CDate("1 Apr 2012")), font, XBrushes.Black, New System.Drawing.PointF(55, 500))
                    gfx.DrawString(Format(oContact.DateOfBirth, "dd MMM yyyy"), font, XBrushes.Black, New System.Drawing.PointF(55, 512))
                End If
                If oContact.HasInternal Then
                    gfx.DrawString(oContact.Internal.HighSchool, font, XBrushes.Black, New System.Drawing.PointF(55, 523))
                End If
                gfx.DrawString(oContact.External.LeaderName, font, XBrushes.Black, New System.Drawing.PointF(85, 534))

            End If


            'Section 6
            Select Case CType(oRegistration.ChildInformation.SchoolGradeID, EnumSchoolGrade)
                Case EnumSchoolGrade.NA, EnumSchoolGrade.Prep
                    Select Case CInt(GetAge(oContact.DateOfBirth, oRegistration.Venue.ConferenceStartDate))
                        Case 2
                            gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 573))
                        Case 3
                            gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 584))
                        Case 4
                            gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(15, 595))
                        Case 5
                            gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(90, 573))
                    End Select
                Case EnumSchoolGrade.Prep
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(90, 584))
                Case EnumSchoolGrade.Grade1
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(90, 595))
                Case EnumSchoolGrade.Grade2
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(154, 573))
                Case EnumSchoolGrade.Grade3
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(154, 584))
                Case EnumSchoolGrade.Grade4
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(154, 595))
                Case EnumSchoolGrade.Grade5
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(224, 573))
                Case EnumSchoolGrade.Grade6
                    gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(224, 584))
            End Select


            If oContact.DateOfBirth <> Nothing Then
                gfx.DrawString(GetAge(oContact.DateOfBirth, CDate("1 Apr 2012")), font, XBrushes.Black, New System.Drawing.PointF(75, 606))
                gfx.DrawString(Format(oContact.DateOfBirth, "dd MMM yyyy"), font, XBrushes.Black, New System.Drawing.PointF(205, 606))
            End If

            gfx.DrawString(oRegistration.ChildInformation.ChildsFriend, font, XBrushes.Black, New System.Drawing.PointF(20, 630))

            If oContact.External.Events.MedicalAllergies.Trim = String.Empty Then
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(101, 641))
            Else
                gfx.DrawString(Chr(252), fontTick, XBrushes.Black, New System.Drawing.PointF(120, 641))
                gfx.DrawString(oContact.External.Events.MedicalAllergies, font, XBrushes.Black, New System.Drawing.PointF(153, 641))
            End If
            gfx.DrawString(oContact.External.Events.MedicalInformation, font, XBrushes.Black, New System.Drawing.PointF(15, 656))

            gfx.DrawString(oRegistration.ChildInformation.DropOffPickUpName, font, XBrushes.Black, New System.Drawing.PointF(35, 687))
            gfx.DrawString(oRegistration.ChildInformation.DropOffPickUpRelationship, font, XBrushes.Black, New System.Drawing.PointF(190, 687))
            gfx.DrawString(oRegistration.ChildInformation.DropOffPickUpContactNumber, font, XBrushes.Black, New System.Drawing.PointF(58, 698))
            gfx.DrawString(oRegistration.ChildInformation.ChildrensChurchPastor, font, XBrushes.Black, New System.Drawing.PointF(92, 710))


            ' Save the document...
            document.Save(sPDFOutput)


        End Sub

    End Module

End Namespace
