Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistrationTypeCombo

        Private miRegistrationTypeComboID As Integer
        Private msName As String
        Private miConferenceID As Integer
        Private miVenueID1 As Integer
        Private miVenueID2 As Integer

        Public Sub New()

            miRegistrationTypeComboID = 0
            msName = String.Empty
            miConferenceID = 0
            miVenueID1 = 0
            miVenueID2 = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegistrationTypeComboID = oDataRow("RegistrationTypeCombo_ID")
            msName = oDataRow("Name")
            miConferenceID = oDataRow("Conference_ID")
            miVenueID1 = oDataRow("Venue_ID1")
            miVenueID2 = oDataRow("Venue_ID2")

        End Sub

        Public Property ID() As Integer
            Get
                Return miRegistrationTypeComboID
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeComboID = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property

        Public Property ConferenceID() As Integer
            Get
                Return miConferenceID
            End Get
            Set(ByVal Value As Integer)
                miConferenceID = Value
            End Set
        End Property
        Public ReadOnly Property Conference() As CConference
            Get
                Return Lists.External.Events.GetConferenceByID(miConferenceID)
            End Get
        End Property

        Public Property VenueID1() As Integer
            Get
                Return miVenueID1
            End Get
            Set(ByVal Value As Integer)
                miVenueID1 = Value
            End Set
        End Property
        Public ReadOnly Property Venue1() As CVenue
            Get
                Return Lists.External.Events.GetVenueByID(miVenueID1)
            End Get
        End Property

        Public Property VenueID2() As Integer
            Get
                Return miVenueID2
            End Get
            Set(ByVal Value As Integer)
                miVenueID2 = Value
            End Set
        End Property
        Public ReadOnly Property Venue2() As CVenue
            Get
                Return Lists.External.Events.GetVenueByID(miVenueID2)
            End Get
        End Property

    End Class

End Namespace