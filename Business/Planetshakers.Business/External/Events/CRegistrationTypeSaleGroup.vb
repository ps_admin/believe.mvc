Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistrationTypeSaleGroup

        Private miRegistrationTypeSaleGroupID As Integer
        Private msName As String
        Private mdMinimumAge As Decimal
        Private mdMaximumAge As Decimal
        Private miConferenceID As Integer
        Private miSortOrder As Integer

        Public Sub New()

            miRegistrationTypeSaleGroupID = 0
            msName = String.Empty
            mdMinimumAge = 0
            mdMaximumAge = 0
            miConferenceID = 0
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegistrationTypeSaleGroupID = oDataRow("RegistrationTypeSaleGroup_ID")
            msName = oDataRow("Name")
            mdMinimumAge = oDataRow("MinimumAge")
            mdMaximumAge = oDataRow("MaximumAge")
            miConferenceID = oDataRow("Conference_ID")
            miSortOrder = oDataRow("SortOrder")

        End Sub
        Public Property ID() As Integer
            Get
                Return miRegistrationTypeSaleGroupID
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeSaleGroupID = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property
        Public Property ConferenceID() As Integer
            Get
                Return miConferenceID
            End Get
            Set(ByVal Value As Integer)
                miConferenceID = Value
            End Set
        End Property
        Public ReadOnly Property Conference() As CConference
            Get
                Return Lists.External.Events.GetConferenceByID(miConferenceID)
            End Get
        End Property
        Public Property MinimumAge() As Decimal
            Get
                Return mdMinimumAge
            End Get
            Set(ByVal value As Decimal)
                mdMinimumAge = value
            End Set
        End Property
        Public Property MaximumAge() As Decimal
            Get
                Return mdMaximumAge
            End Get
            Set(ByVal value As Decimal)
                mdMaximumAge = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace