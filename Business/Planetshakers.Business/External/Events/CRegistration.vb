Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistration

        Private miRegistrationID As Integer
        Private miContactID As Integer
        Private miGroupLeaderID As Integer
        Private mbFamilyRegistration As Boolean
        Private miRegisteredByFriendID As Integer
        Private miVenueID As Integer
        Private miConferenceID As Integer         'Temporary - not written to database. Used to tell which 'registration options' are applicable to this registration
        Private miRegistrationTypeID As Integer
        Private mdRegistrationDiscount As Decimal
        Private miElectiveID As Integer
        Private mbAccommodation As Boolean
        Private miAccommodationVenueID As Integer
        Private mbCatering As Boolean
        Private msCateringNeeds As String
        Private mbLeadershipBreakfast As Boolean
        Private mbLeadershipSummit As Boolean
        Private miULGID As Integer
        Private msParentGuardian As String
        Private msParentGuardianPhone As String
        Private mbAcceptTermsRego As Boolean
        Private mbAcceptTermsParent As Boolean
        Private mbAcceptTermsCreche As Boolean
        Private mdRegistrationDate As Date
        Private miRegisteredByID As Integer
        Private mbAttended As Boolean
        Private mbVolunteer As Boolean
        Private miVolunteerDepartmentID As Integer
        Private miComboRegistrationID As Integer

        Private mcPreOrder As CArrayList(Of CPreOrder)
        Private mcRegistrationOption As CArrayList(Of CRegistrationOption)

        Private moChildInformation As External.Events.CRegistrationChildInformation

        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miRegistrationID = 0
            miContactID = 0
            miGroupLeaderID = 0
            mbFamilyRegistration = False
            miRegisteredByFriendID = 0
            miVenueID = 0
            miConferenceID = 0
            miRegistrationTypeID = 0
            mdRegistrationDiscount = 0
            miElectiveID = 0
            mbAccommodation = False
            miAccommodationVenueID = 0
            mbCatering = False
            msCateringNeeds = String.Empty
            mbLeadershipBreakfast = False
            mbLeadershipSummit = False
            miULGID = 0
            msParentGuardian = String.Empty
            msParentGuardianPhone = String.Empty
            mbAcceptTermsRego = False
            mbAcceptTermsParent = False
            mbAcceptTermsCreche = False
            mdRegistrationDate = Nothing
            miRegisteredByID = 0
            mbAttended = False
            mbVolunteer = False
            miVolunteerDepartmentID = 0
            miComboRegistrationID = 0

            mcPreOrder = New CArrayList(Of CPreOrder)
            mcRegistrationOption = New CArrayList(Of CRegistrationOption)
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegistrationID = oDataRow("Registration_ID")
            miContactID = oDataRow("Contact_ID")
            miGroupLeaderID = IsNull(oDataRow("GroupLeader_ID"), 0)
            mbFamilyRegistration = oDataRow("FamilyRegistration")
            miRegisteredByFriendID = IsNull(oDataRow("RegisteredByFriend_ID"), 0)
            miVenueID = IsNull(oDataRow("Venue_ID"), 0)
            miRegistrationTypeID = oDataRow("RegistrationType_ID")
            mdRegistrationDiscount = oDataRow("RegistrationDiscount")
            miElectiveID = IsNull(oDataRow("Elective_ID"), 0)
            mbAccommodation = oDataRow("Accommodation")
            miAccommodationVenueID = IsNull(oDataRow("Accommodation_ID"), 0)
            mbCatering = oDataRow("Catering")
            msCateringNeeds = oDataRow("CateringNeeds")
            mbLeadershipBreakfast = oDataRow("LeadershipBreakfast")
            mbLeadershipSummit = oDataRow("LeadershipSummit")
            miULGID = IsNull(oDataRow("ULG_ID"), 0)
            msParentGuardian = oDataRow("ParentGuardian")
            msParentGuardianPhone = oDataRow("ParentGuardianPhone")
            mbAcceptTermsRego = oDataRow("AcceptTermsRego")
            mbAcceptTermsParent = oDataRow("AcceptTermsParent")
            mbAcceptTermsCreche = oDataRow("AcceptTermsCreche")
            miRegisteredByID = IsNull(oDataRow("RegisteredBy_ID"), 0)
            mdRegistrationDate = IsNull(oDataRow("RegistrationDate"), Nothing)
            mbAttended = oDataRow("Attended")
            mbVolunteer = oDataRow("Volunteer")
            miVolunteerDepartmentID = IsNull(oDataRow("VolunteerDepartment_ID"), 0)
            miComboRegistrationID = IsNull(oDataRow("ComboRegistration_ID"), 0)

            mcPreOrder = New CArrayList(Of CPreOrder)
            For Each oChildRow As DataRow In oDataRow.GetChildRows("PreOrder")
                mcPreOrder.Add(New CPreOrder(oChildRow))
            Next

            mcRegistrationOption = New CArrayList(Of CRegistrationOption)
            For Each oChildRow As DataRow In oDataRow.GetChildRows("RegistrationOption")
                mcRegistrationOption.Add(New CRegistrationOption(oChildRow))
            Next

            mcPreOrder.ResetChanges()
            mcRegistrationOption.ResetChanges()

            If oDataRow.GetChildRows("RegistrationChildInformation").Length > 0 Then
                moChildInformation = New CRegistrationChildInformation(oDataRow.GetChildRows("RegistrationChildInformation")(0))
            End If

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@GroupLeader_ID", SQLNull(miGroupLeaderID)))
                oParameters.Add(New SqlClient.SqlParameter("@FamilyRegistration", mbFamilyRegistration))
                oParameters.Add(New SqlClient.SqlParameter("@RegisteredByFriend_ID", SQLNull(miRegisteredByFriendID)))
                oParameters.Add(New SqlClient.SqlParameter("@Venue_ID", SQLNull(miVenueID)))
                oParameters.Add(New SqlClient.SqlParameter("@RegistrationType_ID", miRegistrationTypeID))
                oParameters.Add(New SqlClient.SqlParameter("@RegistrationDiscount", mdRegistrationDiscount))
                oParameters.Add(New SqlClient.SqlParameter("@Elective_ID", SQLNull(miElectiveID)))
                oParameters.Add(New SqlClient.SqlParameter("@Accommodation", mbAccommodation))
                oParameters.Add(New SqlClient.SqlParameter("@Accommodation_ID", SQLNull(miAccommodationVenueID)))
                oParameters.Add(New SqlClient.SqlParameter("@Catering", mbCatering))
                oParameters.Add(New SqlClient.SqlParameter("@CateringNeeds", msCateringNeeds))
                oParameters.Add(New SqlClient.SqlParameter("@LeadershipBreakfast", mbLeadershipBreakfast))
                oParameters.Add(New SqlClient.SqlParameter("@LeadershipSummit", mbLeadershipSummit))
                oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLNull(miULGID)))
                oParameters.Add(New SqlClient.SqlParameter("@ParentGuardian", msParentGuardian))
                oParameters.Add(New SqlClient.SqlParameter("@ParentGuardianPhone", msParentGuardianPhone))
                oParameters.Add(New SqlClient.SqlParameter("@AcceptTermsRego", mbAcceptTermsRego))
                oParameters.Add(New SqlClient.SqlParameter("@AcceptTermsParent", mbAcceptTermsParent))
                oParameters.Add(New SqlClient.SqlParameter("@AcceptTermsCreche", mbAcceptTermsCreche))
                oParameters.Add(New SqlClient.SqlParameter("@RegisteredBy_ID", miRegisteredByID))
                oParameters.Add(New SqlClient.SqlParameter("@RegistrationDate", SQLDateTime(mdRegistrationDate)))
                oParameters.Add(New SqlClient.SqlParameter("@Attended", mbAttended))
                oParameters.Add(New SqlClient.SqlParameter("@Volunteer", mbVolunteer))
                oParameters.Add(New SqlClient.SqlParameter("@VolunteerDepartment_ID", SQLNull(miVolunteerDepartmentID)))
                oParameters.Add(New SqlClient.SqlParameter("@ComboRegistration_ID", SQLNull(miComboRegistrationID)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miRegistrationID = DataAccess.GetValue("sp_Events_RegistrationInsert", oParameters)
                    CheckSendTravelEmail()
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@Registration_ID", ID))
                    DataAccess.ExecuteCommand("sp_Events_RegistrationUpdate", oParameters)
                End If

                mbChanged = False

            End If

            SavePreOrder(oUser)
            SaveRegistrationOption(oUser)

            mcPreOrder.ResetChanges()
            mcRegistrationOption.ResetChanges()

            If moChildInformation IsNot Nothing Then
                moChildInformation.ID = Me.ID
                moChildInformation.Save(oUser)
            End If

        End Sub

        Private Sub SavePreOrder(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oPreOrder As CPreOrder

            'Remove the Deleted preorders
            For Each oPreOrder In mcPreOrder.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@PreOrder_ID", oPreOrder.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Events_PreOrderDelete", oParameters)
            Next

            For Each oPreOrder In mcPreOrder
                oPreOrder.RegistrationID = miRegistrationID
                oPreOrder.Save(oUser)
            Next

        End Sub

        Private Sub SaveRegistrationOption(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oRegistrationOption As CRegistrationOption

            'Remove the Deleted preorders
            For Each oRegistrationOption In mcRegistrationOption.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@RegistrationOption_ID", oRegistrationOption.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Events_RegistrationOptionDelete", oParameters)
            Next

            For Each oRegistrationOption In mcRegistrationOption
                oRegistrationOption.RegistrationID = miRegistrationID
                oRegistrationOption.Save(ouser)
            Next

        End Sub

        Private Sub CheckSendTravelEmail()

            'This procedure checks the registration to see if we should send travel information
            Dim oRegistrationOption As CRegistrationOption
            For Each oRegistrationOption In Me.RegistrationOption
                If oRegistrationOption.RegistrationOptionName.Name = "ACCOMMODATIONINFO" Or _
                   oRegistrationOption.RegistrationOptionName.Name = "CONTACTTRAVEL" Then
                    If oRegistrationOption.RegistrationOptionText <> String.Empty Then
                        If CBool(oRegistrationOption.RegistrationOptionText) Then
                            Dim oContact As New CContact(miContactID)
                            MEmailEvents.SingleTravelInformation(oContact, Me.Venue.Conference, String.Empty)
                        End If
                    End If
                End If
            Next

        End Sub
        Public ReadOnly Property ChildInformation() As CRegistrationChildInformation
            Get
                If moChildInformation Is Nothing Then
                    moChildInformation = New CRegistrationChildInformation
                End If
                Return moChildInformation
            End Get
        End Property

        Public ReadOnly Property PreOrder() As CArrayList(Of CPreOrder)
            Get
                Return mcPreOrder
            End Get
        End Property
        Public ReadOnly Property RegistrationOption() As CArrayList(Of CRegistrationOption)
            Get
                If Me.Conference IsNot Nothing Or Me.Venue IsNot Nothing Then
                    Dim oRegistrationOption As CRegistrationOption
                    Dim oRegistrationOptionName As CRegistrationOptionName

                    For Each oRegistrationOptionName In Lists.External.Events.RegistrationOptionNames.Values
                        If (Conference IsNot Nothing AndAlso oRegistrationOptionName.Conference.ID = Conference.ID) Or _
                            (miVenueID > 0 AndAlso oRegistrationOptionName.Conference.ID = Venue.Conference.ID) Then
                            Dim bFound As Boolean = False
                            For Each oRegistrationOption In mcRegistrationOption
                                If oRegistrationOption.RegistrationOptionName.ID = oRegistrationOptionName.ID Then
                                    bFound = True
                                End If
                            Next
                            If Not bFound Then
                                Dim oNewRegistrationOption As New CRegistrationOption
                                oNewRegistrationOption.RegistrationOptionNameID = oRegistrationOptionName.ID
                                mcRegistrationOption.Add(oNewRegistrationOption)
                                If oNewRegistrationOption.RegistrationOptionName.HasValues Then
                                    Dim oRegistrationOptionValue As New CRegistrationOptionValue
                                    For Each oRegistrationOptionValue In oNewRegistrationOption.RegistrationOptionName.GetRegistrationOptionValues
                                        If oRegistrationOptionValue.IsDefault Then
                                            oNewRegistrationOption.RegistrationOptionValueID = oRegistrationOptionValue.ID
                                        End If
                                    Next
                                End If
                                oNewRegistrationOption.Changed = False
                            End If
                        End If
                    Next
                End If

                Return mcRegistrationOption
            End Get
        End Property

        Public Property ID() As Integer
            Get
                Return miRegistrationID
            End Get
            Set(ByVal value As Integer)
                miRegistrationID = value
            End Set
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property GroupLeaderID() As Integer
            Get
                Return miGroupLeaderID
            End Get
            Set(ByVal Value As Integer)
                If miGroupLeaderID <> Value Then mbChanged = True
                miGroupLeaderID = Value
            End Set
        End Property
        Public Property FamilyRegistration() As Boolean
            Get
                Return mbFamilyRegistration
            End Get
            Set(ByVal Value As Boolean)
                If mbFamilyRegistration <> Value Then mbChanged = True
                mbFamilyRegistration = Value
            End Set
        End Property
        Public Property RegisteredByFriendID() As Integer
            Get
                Return miRegisteredByFriendID
            End Get
            Set(ByVal Value As Integer)
                If miRegisteredByFriendID <> Value Then mbChanged = True
                miRegisteredByFriendID = Value
            End Set
        End Property
        Public Property VenueID() As Integer
            Get
                Return miVenueID
            End Get
            Set(ByVal Value As Integer)
                If miVenueID <> Value Then mbChanged = True
                miVenueID = Value
            End Set
        End Property
        Public ReadOnly Property Venue() As CVenue
            Get
                Return Lists.External.Events.GetVenueByID(miVenueID)
            End Get
        End Property
        Public Property ConferenceID() As Integer
            Get
                Return miConferenceID
            End Get
            Set(ByVal Value As Integer)
                If miConferenceID <> Value Then mbChanged = True
                miConferenceID = Value
            End Set
        End Property
        Public ReadOnly Property Conference() As CConference
            Get
                Return Lists.External.Events.GetConferenceByID(miConferenceID)
            End Get
        End Property
        Public Property RegistrationTypeID() As Integer
            Get
                Return miRegistrationTypeID
            End Get
            Set(ByVal Value As Integer)
                If miRegistrationTypeID <> Value Then mbChanged = True
                miRegistrationTypeID = Value
            End Set
        End Property
        Public ReadOnly Property RegistrationType() As CRegistrationType
            Get
                Return Lists.External.Events.GetRegistrationTypeByID(miRegistrationTypeID)
            End Get
        End Property
        Public Property RegistrationDiscount() As Decimal
            Get
                Return mdRegistrationDiscount
            End Get
            Set(ByVal Value As Decimal)
                If mdRegistrationDiscount <> Value Then mbChanged = True
                mdRegistrationDiscount = Value
            End Set
        End Property
        Public Property ElectiveID() As Integer
            Get
                Return miElectiveID
            End Get
            Set(ByVal Value As Integer)
                If miElectiveID <> Value Then mbChanged = True
                miElectiveID = Value
            End Set
        End Property
        Public ReadOnly Property Elective() As CElective
            Get
                Return Lists.External.Events.GetElectiveByID(miElectiveID)
            End Get
        End Property
        Public ReadOnly Property RegistrationPrice() As Decimal
            Get
                Return Math.Round(RegistrationType.RegistrationCost * ((100 - RegistrationDiscount) / 100), 2)
            End Get
        End Property
        Public Property Accommodation() As Boolean
            Get
                Return mbAccommodation
            End Get
            Set(ByVal value As Boolean)
                If mbAccommodation <> value Then mbChanged = True
                mbAccommodation = value
            End Set
        End Property
        Public Property AccommodationVenueID() As Integer
            Get
                Return miAccommodationVenueID
            End Get
            Set(ByVal value As Integer)
                If miAccommodationVenueID <> value Then mbChanged = True
                miAccommodationVenueID = value
            End Set
        End Property
        Public ReadOnly Property AccommodationVenue() As CAccommodationVenue
            Get
                Return Lists.External.Events.GetAccommodationVenueByID(miAccommodationVenueID)
            End Get
        End Property
        Public Property Catering() As Boolean
            Get
                Return mbCatering
            End Get
            Set(ByVal value As Boolean)
                If mbCatering <> value Then mbChanged = True
                mbCatering = value
            End Set
        End Property
        Public Property CateringNeeds() As String
            Get
                Return msCateringNeeds
            End Get
            Set(ByVal Value As String)
                If msCateringNeeds <> Value Then mbChanged = True
                msCateringNeeds = Value
            End Set
        End Property
        Public Property LeadershipBreakfast() As Boolean
            Get
                Return mbLeadershipBreakfast
            End Get
            Set(ByVal value As Boolean)
                If mbLeadershipBreakfast <> value Then mbChanged = True
                mbLeadershipBreakfast = value
            End Set
        End Property
        Public Property LeadershipSummit() As Boolean
            Get
                Return mbLeadershipSummit
            End Get
            Set(ByVal value As Boolean)
                If mbLeadershipSummit <> value Then mbChanged = True
                mbLeadershipSummit = value
            End Set
        End Property
        Public Property ULGID() As Integer
            Get
                Return miULGID
            End Get
            Set(ByVal value As Integer)
                If miULGID <> value Then mbChanged = True
                miULGID = value
            End Set
        End Property
        Public ReadOnly Property ULG() As CRegistrationULG
            Get
                Return Lists.External.Events.GetRegistrationULGByID(miULGID)
            End Get
        End Property
        Property ParentGuardian() As String
            Get
                Return msParentGuardian
            End Get
            Set(ByVal Value As String)
                If msParentGuardian <> Value Then mbChanged = True
                msParentGuardian = Value
            End Set
        End Property
        Public Property ParentGuardianPhone() As String
            Get
                Return msParentGuardianPhone
            End Get
            Set(ByVal Value As String)
                If msParentGuardianPhone <> Value Then mbChanged = True
                msParentGuardianPhone = Value
            End Set
        End Property
        Public Property AcceptTermsRego() As Boolean
            Get
                Return mbAcceptTermsRego
            End Get
            Set(ByVal Value As Boolean)
                If mbAcceptTermsRego <> Value Then mbChanged = True
                mbAcceptTermsRego = Value
            End Set
        End Property
        Property AcceptTermsParent() As Boolean
            Get
                Return mbAcceptTermsParent
            End Get
            Set(ByVal Value As Boolean)
                If mbAcceptTermsParent <> Value Then mbChanged = True
                mbAcceptTermsParent = Value
            End Set
        End Property
        Public Property AcceptTermsCreche() As Boolean
            Get
                Return mbAcceptTermsCreche
            End Get
            Set(ByVal Value As Boolean)
                If mbAcceptTermsCreche <> Value Then mbChanged = True
                mbAcceptTermsCreche = Value
            End Set
        End Property
        Property RegistrationDate() As Date
            Get
                Return mdRegistrationDate
            End Get
            Set(ByVal Value As Date)
                If mdRegistrationDate <> Value Then mbChanged = True
                mdRegistrationDate = Value
            End Set
        End Property
        Public Property RegisteredByID() As Integer
            Get
                Return miRegisteredByID
            End Get
            Set(ByVal Value As Integer)
                If miRegisteredByID <> Value Then mbChanged = True
                miRegisteredByID = Value
            End Set
        End Property
        ReadOnly Property RegisteredBy() As CUser
            Get
                Return Lists.GetUserByID(miRegisteredByID)
            End Get
        End Property
        Property Attended() As Boolean
            Get
                Return mbAttended
            End Get
            Set(ByVal Value As Boolean)
                If mbAttended <> Value Then mbChanged = True
                mbAttended = Value
            End Set
        End Property
        Public Property Volunteer() As Boolean
            Get
                Return mbVolunteer
            End Get
            Set(ByVal Value As Boolean)
                If mbVolunteer <> Value Then mbChanged = True
                mbVolunteer = Value
            End Set
        End Property
        Property VolunteerDepartmentID() As Integer
            Get
                Return miVolunteerDepartmentID
            End Get
            Set(ByVal value As Integer)
                If miVolunteerDepartmentID <> value Then mbChanged = True
                miVolunteerDepartmentID = value
            End Set
        End Property
        Public ReadOnly Property VolunteerDepartment() As CVolunteerDepartment
            Get
                Return Lists.External.Events.GetVolunteerDepartmentByID(miVolunteerDepartmentID)
            End Get
        End Property
        Public Property ComboRegistrationID() As Integer
            Get
                Return miComboRegistrationID
            End Get
            Set(ByVal value As Integer)
                If miComboRegistrationID <> value Then mbChanged = True
                miComboRegistrationID = value
            End Set
        End Property
        Public Property GUID() As String
            Get
                Return msGUID
            End Get
            Set(ByVal Value As String)
                msGUID = Value
            End Set
        End Property

        Public ReadOnly Property TotalCost() As Decimal
            Get
                Dim dTotal As Decimal
                Dim oPreOrder As CPreOrder

                For Each oPreOrder In mcPreOrder
                    dTotal += oPreOrder.Price(Venue.Conference.Country)
                Next

                dTotal += Math.Round(Venue.Conference.AccommodationCost * IIf(Me.Accommodation, 1, 0) * ((100 - RegistrationDiscount) / 100), 2)
                dTotal += Math.Round(Venue.Conference.CateringCost * IIf(Me.Catering, 1, 0) * ((100 - RegistrationDiscount) / 100), 2)
                dTotal += Math.Round(Venue.Conference.LeadershipBreakfastCost * IIf(Me.LeadershipBreakfast, 1, 0) * ((100 - RegistrationDiscount) / 100), 2)

                dTotal += RegistrationPrice

                Return dTotal

            End Get
        End Property

        Public Function GetRegistrationOption(ByVal sName As String) As String

            Dim oRegistrationOption As CRegistrationOption = GetRegistrationOptionObject(sName)

            If oRegistrationOption IsNot Nothing Then
                If oRegistrationOption.RegistrationOptionValue IsNot Nothing Then
                    Return oRegistrationOption.RegistrationOptionValue.Value
                Else
                    Return oRegistrationOption.RegistrationOptionText
                End If
            Else
                Return String.Empty
            End If

        End Function

        Public Sub SetRegistrationOption(ByVal sName As String, ByVal sValue As String)

            Dim oRegistrationOption As CRegistrationOption = GetRegistrationOptionObject(sName)
            Dim oRegistrationOptionValue As CRegistrationOptionValue
            Dim bHasValues As Boolean = False

            If oRegistrationOption.RegistrationOptionName.HasValues Then
                For Each oRegistrationOptionValue In Lists.External.Events.RegistrationOptionValues.Values
                    If oRegistrationOptionValue.RegistrationOptionName.ID = oRegistrationOption.RegistrationOptionName.ID Then
                        If oRegistrationOptionValue.Value = sValue Then
                            oRegistrationOption.RegistrationOptionValueID = oRegistrationOptionValue.ID
                        End If
                    End If
                Next
            Else
                oRegistrationOption.RegistrationOptionText = sValue
            End If

        End Sub

        Public Function GetRegistrationOptionObject(ByVal sName As String) As CRegistrationOption

            'If venue has not been set yet, return an empty RegistrationObject (because without the conference, we don't know which set of registrationoptions to use)
            If miVenueID = 0 And Conference Is Nothing Then Return Nothing

            Dim oRegistrationOption As CRegistrationOption
            For Each oRegistrationOption In Me.RegistrationOption
                If oRegistrationOption.RegistrationOptionName.Name = sName Then
                    Return oRegistrationOption
                End If
            Next

            Dim oRegistrationOptionName As CRegistrationOptionName
            For Each oRegistrationOptionName In Lists.External.Events.RegistrationOptionNames.Values
                If (Conference IsNot Nothing AndAlso oRegistrationOptionName.Conference.ID = Conference.ID) Or _
                   (miVenueID > 0 AndAlso oRegistrationOptionName.Conference.ID = Venue.Conference.ID) Then
                    If oRegistrationOptionName.Name = sName Then
                        oRegistrationOption = New CRegistrationOption
                        oRegistrationOption.RegistrationOptionNameID = oRegistrationOptionName.ID
                        Me.RegistrationOption.Add(oRegistrationOption)
                        Return oRegistrationOption
                    End If
                End If
            Next

            Return Nothing

        End Function

        Public Property Changed() As Boolean
            Get
                If mbChanged Then Return True

                If mcPreOrder.Changed Then Return True
                If mcRegistrationOption.Changed Then Return True

                If moChildInformation IsNot Nothing Then
                    If moChildInformation.Changed Then Return True
                End If

                Dim oPreOrder As CPreOrder
                For Each oPreOrder In mcPreOrder
                    If oPreOrder.Changed Then Return True
                Next

                Dim oRegistrationOption As CRegistrationOption
                For Each oRegistrationOption In mcRegistrationOption
                    If oRegistrationOption.Changed Then Return True
                Next

                Return False

            End Get
            Set(ByVal value As Boolean)
                mbChanged = value
            End Set
        End Property

        Public Sub ResetChanges()

            mbChanged = False

            mcPreOrder.ResetChanges()
            mcRegistrationOption.ResetChanges()

            If moChildInformation IsNot Nothing Then
                moChildInformation.ResetChanges()
            End If

            Dim oPreOrder As CPreOrder
            For Each oPreOrder In mcPreOrder
                oPreOrder.ResetChanges()
            Next

            Dim oRegistrationOption As CRegistrationOption
            For Each oRegistrationOption In mcRegistrationOption
                oRegistrationOption.ResetChanges()
            Next

        End Sub

    End Class

End Namespace