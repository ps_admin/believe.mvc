Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContentSubCategory

        Private miContentSubCategory As Integer
        Private miContentCategory As Integer
        Private msName As String
        Private miSortOrder As Integer

        Public Sub New()

            miContentSubCategory = 0
            miContentCategory = 0
            msName = String.Empty
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContentSubCategory = oDataRow("ContentSubCategory_ID")
            miContentCategory = oDataRow("ContentCategory_ID")
            msName = oDataRow("Name")
            miSortOrder = oDataRow("SortOrder")

        End Sub
        Public ReadOnly Property ID() As Integer
            Get
                Return miContentSubCategory
            End Get
        End Property
        Public ReadOnly Property ContentCategory() As CContentCategory
            Get
                Return Lists.External.Revolution.GetContentCategoryByID(miContentCategory)
            End Get
        End Property
        Public Property ContentCategoryID() As Integer
            Get
                Return miContentCategory
            End Get
            Set(ByVal value As Integer)
                miContentCategory = value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property
        Public ReadOnly Property FullName() As String
            Get
                Return ContentCategory.Name & " - " & Me.Name
            End Get
        End Property

    End Class

End Namespace