Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CSubscription

        Private miSubscriptionID As Integer
        Private msName As String
        Private mdCost As Decimal
        Private miSubscriptionLength As Integer
        Private miSubscriptionActive As Integer
        Private miBankAccountID As Integer
        Private mbOneTimeSubscriptionOnly As Boolean

        Public Sub New()

            miSubscriptionID = 0
            msName = String.Empty
            mdCost = 0
            miSubscriptionLength = 0
            miSubscriptionActive = 0
            miBankAccountID = 0
            mbOneTimeSubscriptionOnly = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miSubscriptionID = oDataRow("Subscription_ID")
            msName = oDataRow("Name")
            mdCost = oDataRow("Cost")
            miSubscriptionLength = oDataRow("SubscriptionLength")
            miSubscriptionActive = oDataRow("SubscriptionActive")
            miBankAccountID = oDataRow("BankAccount_ID")
            mbOneTimeSubscriptionOnly = oDataRow("OneTimeSubscriptionOnly")

        End Sub

        Public ReadOnly Property ContentPacks() As CArrayList(Of CContentPack)
            Get
                Dim oContentPacks As New CArrayList(Of CContentPack)
                Dim oContentPack As CContentPack
                Dim oSubscription As CSubscription
                For Each oContentPack In Lists.External.Revolution.ContentPack.Values
                    For Each oSubscription In oContentPack.Subscription
                        If oSubscription.ID = miSubscriptionID Then
                            oContentPacks.Add(oContentPack)
                        End If
                    Next
                Next
                Return oContentPacks
            End Get
        End Property
        Public ReadOnly Property ID() As Integer
            Get
                Return miSubscriptionID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property Cost() As Decimal
            Get
                Return mdCost
            End Get
            Set(ByVal value As Decimal)
                mdCost = value
            End Set
        End Property
        Public Property SubscriptionLength() As Integer
            Get
                Return miSubscriptionLength
            End Get
            Set(ByVal value As Integer)
                miSubscriptionLength = value
            End Set
        End Property
        Public Property SubscriptionActive() As Integer
            Get
                Return miSubscriptionActive
            End Get
            Set(ByVal value As Integer)
                miSubscriptionActive = value
            End Set
        End Property
        Public Property BankAccountID() As Integer
            Get
                Return miBankAccountID
            End Get
            Set(ByVal value As Integer)
                miBankAccountID = value
            End Set
        End Property
        Public ReadOnly Property BankAccount() As CBankAccount
            Get
                Return Lists.BankAccounts(miBankAccountID)
            End Get
        End Property
        Public Property OneTimeSubscriptionOnly() As Boolean
            Get
                Return mbOneTimeSubscriptionOnly
            End Get
            Set(ByVal value As Boolean)
                mbOneTimeSubscriptionOnly = value
            End Set
        End Property

    End Class

End Namespace