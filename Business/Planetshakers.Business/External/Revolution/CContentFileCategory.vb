Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContentFileCategory

        Private miContentFileCategoryID As Integer
        Private msName As String

        Public Sub New()

            miContentFileCategoryID = 0
            msName = String.Empty

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContentFileCategoryID = oDataRow("ContentFileCategory_ID")
            msName = oDataRow("Name")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miContentFileCategoryID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property

    End Class

End Namespace
