Namespace External

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactChurchInvolvement

        Private miContactChurchInvolvementID As Integer
        Private miContactID As Integer
        Private miChurchInvolvementID As Integer
        Private mdDateAdded As Date
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miContactChurchInvolvementID = 0
            miContactID = 0
            miChurchInvolvementID = 0
            mdDateAdded = Nothing
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContactChurchInvolvementID = oDataRow("ContactChurchInvolvement_ID")
            miContactID = oDataRow("Contact_ID")
            miChurchInvolvementID = oDataRow("ChurchInvolvement_ID")
            mdDateAdded = oDataRow("DateAdded")

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchInvolvement_ID", miChurchInvolvementID))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miContactChurchInvolvementID = DataAccess.GetValue("sp_External_ContactChurchInvolvementInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContactChurchInvolvement_ID", miContactChurchInvolvementID))
                    DataAccess.ExecuteCommand("sp_External_ContactChurchInvolvementUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miContactChurchInvolvementID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property ChurchInvolvementID() As Integer
            Get
                Return miChurchInvolvementID
            End Get
            Set(ByVal value As Integer)
                If miChurchInvolvementID <> value Then mbChanged = True
                miChurchInvolvementID = value
            End Set
        End Property
        Public ReadOnly Property ChurchInvolvement() As CChurchInvolvement
            Get
                Return Lists.External.GetChurchInvolvementByID(miChurchInvolvementID)
            End Get
        End Property
        Public Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal value As Date)
                If mdDateAdded <> value Then mbChanged = True
                mdDateAdded = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace