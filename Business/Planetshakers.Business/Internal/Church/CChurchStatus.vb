Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CChurchStatus

        Private miChurchStatusID As Integer
        Private msName As String
        Private mbRequestReasonWhenChangingTo As Boolean
        Private miSortOrder As Integer
        Private mbDeleted As Boolean

        Private mcAllowedChangesApproval As ArrayList
        Private mcAllowedChangesNonApproval As ArrayList

        Public Sub New()

            miChurchStatusID = 0
            msName = String.Empty
            mbRequestReasonWhenChangingTo = False
            miSortOrder = 0
            mbDeleted = False
            mcAllowedChangesApproval = New ArrayList
            mcAllowedChangesNonApproval = New ArrayList

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miChurchStatusID = oDataRow("ChurchStatus_ID")
            msName = oDataRow("Name")
            mbRequestReasonWhenChangingTo = oDataRow("RequestReasonWhenChangingTo")
            miSortOrder = oDataRow("SortOrder")
            mbDeleted = oDataRow("Deleted")

        End Sub

        Public Sub LoadChangeRow(ByRef oDataRow As DataRow)

            Dim oChangeRow As DataRow
            For Each oChangeRow In oDataRow.GetChildRows("ChurchStatusChange")
                Dim oChurchStatus As CChurchStatus = Lists.Internal.Church.GetChurchStatusByID(oChangeRow("ChurchStatusTo_ID"))
                If Not oChurchStatus.Deleted Then
                    If oChangeRow("ChangeMustBeApproved") Then
                        mcAllowedChangesApproval.Add(oChurchStatus)
                    Else
                        mcAllowedChangesNonApproval.Add(oChurchStatus)
                    End If
                End If
            Next

        End Sub

        Public Function IsChangeRequestRequired(ByVal oNewStatus As CChurchStatus) As Boolean
            Dim oCheckStatus As CChurchStatus
            For Each oCheckStatus In mcAllowedChangesApproval
                If oCheckStatus.ID = oNewStatus.ID Then Return True
            Next
            Return False
        End Function

        Public ReadOnly Property AllowedChangedApproval() As ArrayList
            Get
                Return mcAllowedChangesApproval
            End Get
        End Property
        Public ReadOnly Property AllowedChangedNonApproval() As ArrayList
            Get
                Return mcAllowedChangesNonApproval
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miChurchStatusID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property RequestReasonWhenChangingTo() As Boolean
            Get
                Return mbRequestReasonWhenChangingTo
            End Get
            Set(ByVal value As Boolean)
                mbRequestReasonWhenChangingTo = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                mbDeleted = value
            End Set
        End Property
        Public Overrides Function ToString() As String
            Return Me.Name
        End Function
    End Class

End Namespace