Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactCourseEnrolment
        Implements IComparable(Of CContactCourseEnrolment)

        Private miContactCourseEnrolmentID As Integer
        Private miContactID As Integer
        Private miCourseInstanceID As Integer
        Private mdEnrolmentDate As Date
        Private msComments As String
        Private mdIntroductoryEmailSentDate As Date
        Private mbCompleted As Boolean
        Private msGUID As String
        Private mcCourseEnrolmentAttendance As CArrayList(Of CCourseEnrolmentAttendance)

        Private mbChanged As Boolean

        Public Sub New()

            miContactCourseEnrolmentID = 0
            miContactID = 0
            miCourseInstanceID = 0
            mdEnrolmentDate = Nothing
            msComments = String.Empty
            mdIntroductoryEmailSentDate = Nothing
            mbCompleted = False
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContactCourseEnrolmentID = oDataRow("ContactCourseEnrolment_ID")
            miContactID = oDataRow("Contact_ID")
            miCourseInstanceID = oDataRow("CourseInstance_ID")
            mdEnrolmentDate = oDataRow("EnrolmentDate")
            msComments = oDataRow("Comments")
            mdIntroductoryEmailSentDate = IsNull(oDataRow("IntroductoryEmailSentDate"), Nothing)
            mbCompleted = oDataRow("Completed")

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", miCourseInstanceID))
                oParameters.Add(New SqlClient.SqlParameter("@EnrolmentDate", SQLDateTime(mdEnrolmentDate)))
                oParameters.Add(New SqlClient.SqlParameter("@Comments", msComments))
                oParameters.Add(New SqlClient.SqlParameter("@IntroductoryEmailSentDate", SQLDate(mdIntroductoryEmailSentDate)))
                oParameters.Add(New SqlClient.SqlParameter("@Completed", mbCompleted))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miContactCourseEnrolmentID = DataAccess.GetValue("sp_Church_ContactCourseEnrolmentInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContactCourseEnrolment_ID", miContactCourseEnrolmentID))
                    DataAccess.ExecuteCommand("sp_Church_ContactCourseEnrolmentUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miContactCourseEnrolmentID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property CourseInstanceID() As Integer
            Get
                Return miCourseInstanceID
            End Get
            Set(ByVal value As Integer)
                If miCourseInstanceID <> value Then mbChanged = True
                miCourseInstanceID = value
            End Set
        End Property
        Public ReadOnly Property CourseInstance() As CCourseInstance
            Get
                Return Lists.Internal.Church.GetCourseInstanceByID(miCourseInstanceID)
            End Get
        End Property
        Public Property EnrolmentDate() As Date
            Get
                Return mdEnrolmentDate
            End Get
            Set(ByVal value As Date)
                If mdEnrolmentDate <> value Then mbChanged = True
                mdEnrolmentDate = value
            End Set
        End Property
        Public Property Comments() As String
            Get
                Return msComments
            End Get
            Set(ByVal value As String)
                If msComments <> value Then mbChanged = True
                msComments = value
            End Set
        End Property
        Public Property IntroductoryEmailSentDate() As Date
            Get
                Return mdIntroductoryEmailSentDate
            End Get
            Set(ByVal value As Date)
                If mdIntroductoryEmailSentDate <> value Then mbChanged = True
                mdIntroductoryEmailSentDate = value
            End Set
        End Property
        Public Property Completed() As Boolean
            Get
                Return mbCompleted
            End Get
            Set(ByVal value As Boolean)
                If mbCompleted <> value Then mbChanged = True
                mbCompleted = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

        Public Function CompareTo(ByVal other As CContactCourseEnrolment) As Integer Implements System.IComparable(Of CContactCourseEnrolment).CompareTo
            If Me.CourseInstance.StartDate = other.CourseInstance.StartDate Then
                Return Date.Compare(other.EnrolmentDate, Me.EnrolmentDate)
            Else
                Return Date.Compare(other.CourseInstance.StartDate, Me.CourseInstance.StartDate)
            End If
        End Function

    End Class

End Namespace