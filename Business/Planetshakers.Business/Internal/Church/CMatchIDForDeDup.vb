Namespace Internal.Church

    <Serializable()>
    <CLSCompliant(True)>
    Public Class CDatabaseDedupeMatch

        Private miMatchID As Integer
        Private mbChanged As Boolean

        Public Sub New()
            miMatchID = 0
        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miMatchID = oDataRow("Match_ID")
        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miMatchID
            End Get
        End Property

    End Class

End Namespace