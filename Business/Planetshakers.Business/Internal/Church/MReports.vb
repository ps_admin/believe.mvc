Namespace Internal.Church.Reports

    Public Module MReports

        Public Function ReportPartnershipComparison(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                    ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                    ByVal oCampus As CCampus, ByVal oMinistry As CMinistry) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

            Return DataAccess.GetDataSet("sp_Church_ReportPC01PartnershipComparison", New ArrayList, oParameters)

        End Function

        Public Function ReportPartnershipComparisonChurchStatus(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                                  ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                                  ByVal oCampus As CCampus, ByVal oMinistry As CMinistry, ByVal oRegion As CRegion) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))

            Return DataAccess.GetDataSet("sp_Church_ReportPC12PartnershipComparisonChurchStatus", New ArrayList, oParameters)

        End Function

        Public Function PartnershipStatusBreakdown(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                    ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                    ByVal oCampus As CCampus) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataSet("sp_Church_ReportPC02PartnershipStatusBreakdown", New ArrayList, oParameters)

        End Function

        Public Function PastoralCareSummary(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                    ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                    ByVal oCampus As CCampus, ByVal oMinistry As CMinistry) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

            Return DataAccess.GetDataSet("sp_Church_ReportPC03PastoralCareSummary", New ArrayList, oParameters)

        End Function

        Public Function PastoralContactsByMinistryOrRegion(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                    ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                    ByVal oCampus As CCampus, ByVal oMinistry As CMinistry, _
                                                    ByVal bByRegion As Boolean) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@ByRegion", bByRegion))

            Return DataAccess.GetDataSet("sp_Church_ReportPC04PastoralContactsByMinistryOrRegion", New ArrayList, oParameters)

        End Function

        Public Function SundayAttendanceByMinistryOrRegion(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                    ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                    ByVal oCampus As CCampus, ByVal oMinistry As CMinistry, _
                                                    ByVal bByRegion As Boolean) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@ByRegion", bByRegion))

            Return DataAccess.GetDataSet("sp_Church_ReportPC05SundayAttendanceByMinistryOrRegion", New ArrayList, oParameters)

        End Function

        Public Function GoneMissingByMinistryOrRegion(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                    ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                    ByVal oCampus As CCampus, ByVal oMinistry As CMinistry, _
                                                    ByVal bByRegion As Boolean) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@ByRegion", bByRegion))

            Return DataAccess.GetDataSet("sp_Church_ReportPC06GoneMissingByMinistryOrRegion", New ArrayList, oParameters)

        End Function

        Public Function GoneMissingContactedByMinistry(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                    ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                    ByVal oCampus As CCampus, ByVal oMinistry As CMinistry) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

            Return DataAccess.GetDataTable("sp_Church_ReportPC07GoneMissingContactedByMinistry", oParameters)

        End Function

        Public Function GoneMissingNamesListByMinistryOrRegion(ByVal iMonth As Integer, ByVal iYear As Integer, _
                                                    ByVal oCampus As CCampus, ByVal oMinistry As CMinistry, _
                                                    ByVal oRegion As CRegion) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Month", iMonth))
            oParameters.Add(New SqlClient.SqlParameter("@Year", iYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))

            Return DataAccess.GetDataTable("sp_Church_ReportPC08GoneMissingNamesList", oParameters)

        End Function

        Public Function ULGOfferingsByMinistry(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                    ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                    ByVal oCampus As CCampus, ByVal oMinistry As CMinistry) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

            Return DataAccess.GetDataSet("sp_Church_ReportPC09ULGOfferingsByMinistry", New ArrayList, oParameters)

        End Function

        Public Function ULGAttendanceMinistryOrRegion(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                    ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                    ByVal oCampus As CCampus, ByVal oMinistry As CMinistry, _
                                                    ByVal bByRegion As Boolean) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@ByRegion", bByRegion))

            Return DataAccess.GetDataSet("sp_Church_ReportPC10ULGAttendanceByMinistryOrRegion", New ArrayList, oParameters)

        End Function

        Public Function AdditionsAndDeletionsByMinistry(ByVal dStartDate As Date, ByVal dEndDate As Date, _
                                                        ByVal oCampus As CCampus, ByVal oMinistry As CMinistry) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", dStartDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", dEndDate))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

            Return DataAccess.GetDataSet("sp_Church_ReportPC11AdditionsAndDeletionsByMinistry", New ArrayList, oParameters)

        End Function
        Public Function MovementTable(ByVal dStartDate As Date,
                                      ByVal dEndDate As Date, _
                                      ByVal oCampus As CCampus, _
                                      ByVal oMinistry As CMinistry, _
                                      ByVal oRegion As CRegion) As DataSet

            'Dim StartDate As String = iStartYear.ToString() + "-" + iStartMonth.ToString() + "-01 00:00:00.000"
            'Dim EndDate As String = iEndYear.ToString() + "-" + iEndMonth.ToString() + "-31 23:59:59.000"
            Dim oParameters As New ArrayList
            'oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            'oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            'oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            'oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", dStartDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", dEndDate))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))

            Return DataAccess.GetDataSet("sp_Church_ReportPC13MovementTable", New ArrayList, oParameters)

        End Function

        Public Function MasterStatsOverview(ByVal iMonth As Integer, ByVal iYear As Integer, _
                                            ByVal oCampus As CCampus) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Month", iMonth))
            oParameters.Add(New SqlClient.SqlParameter("@Year", iYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataTable("sp_Church_ReportMS01MasterStats", oParameters)

        End Function

        Public Function MasterStatsChurchPartnership(ByVal iMonth1 As Integer, ByVal iYear1 As Integer, _
                                                     ByVal iMonth2 As Integer, ByVal iYear2 As Integer, _
                                                     ByVal oCampus As CCampus) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Month1", iMonth1))
            oParameters.Add(New SqlClient.SqlParameter("@Year1", iYear1))
            oParameters.Add(New SqlClient.SqlParameter("@Month2", iMonth2))
            oParameters.Add(New SqlClient.SqlParameter("@Year2", iYear2))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataTable("sp_Church_ReportMS02ChurchPartnership", oParameters)

        End Function

        Public Function NPNCEntryPoints(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                        ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                        ByRef oNPNCType As CNPNCType, ByVal oCampus As CCampus, _
                                        ByVal oMinistry As CMinistry) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", oNPNCType.ID))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

            Return DataAccess.GetDataTable("sp_Church_ReportNPNC01NPEntryPoints", oParameters)

        End Function

        Public Function NPNCMinistryAllocation(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                 ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                 ByVal oNPNCType As CNPNCType, ByVal oCampus As CCampus) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", SQLObject(oNPNCType)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataTable("sp_Church_ReportNPNC02MinistryAllocation", oParameters)

        End Function

        Public Function NPNCNewPeopleInitialStatus(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                 ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                 ByVal oCampus As CCampus) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataSet("sp_Church_ReportNPNC03InitialStatus", New ArrayList, oParameters)

        End Function

        Public Function NPNCNewChristianDecisionType(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                     ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                     ByVal oCampus As CCampus) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataSet("sp_Church_ReportNPNC04NCDecisionType", New ArrayList, oParameters)

        End Function

        Public Function NPNCNewPeopleWelcomePacksMonthly(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                         ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                         ByVal oCampus As CCampus) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataSet("sp_Church_ReportNPNC05NPWelcomePacksMonthly", New ArrayList, oParameters)

        End Function

        Public Function NPNCInFollowup(ByVal iStartYear As Integer, ByVal iEndYear As Integer, _
                                       ByVal oNPNCType As CNPNCType, ByVal oCampus As CCampus) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", SQLObject(oNPNCType)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataTable("sp_Church_ReportNPNC06InFollowup", oParameters)

        End Function

        Public Function NPNCNewPeopleWelcomePacksYearly(ByVal iStartYear As Integer, ByVal iEndYear As Integer, _
                                                        ByVal oCampus As CCampus) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataTable("sp_Church_ReportNPNC07NPWelcomePacksYearly", oParameters)

        End Function

        Public Function NPNCOutcome(ByVal dStartDate As Date, ByVal dEndDate As Date, _
                                    ByVal oNPNCType As CNPNCType, ByVal oCampus As CCampus, _
                                    ByVal oMinistry As CMinistry, ByVal oRegion As CRegion, _
                                    ByVal iDateType As Integer) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", dStartDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", dEndDate))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", SQLObject(oNPNCType)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@DateType", iDateType))

            Return DataAccess.GetDataSet("sp_Church_ReportNPNC08Outcome", New ArrayList, oParameters)

        End Function

        Public Function NPNCAgeDemographic(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                           ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                           ByVal oNPNCType As CNPNCType, ByVal oCampus As CCampus, _
                                           ByVal oMinistry As CMinistry) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", SQLObject(oNPNCType)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

            Return DataAccess.GetDataSet("sp_Church_ReportNPNC09AgeDemographic", New ArrayList, oParameters)

        End Function

        

        Public Function NPNCFirstContact(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                         ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                         ByVal oNPNCType As CNPNCType, ByVal oCampus As CCampus, _
                                         ByVal oMinistry As CMinistry) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", SQLObject(oNPNCType)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

            Return DataAccess.GetDataSet("sp_Church_ReportNPNC10FirstContact", New ArrayList, oParameters)

        End Function

        Public Function NPNCSummaryStats(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                                ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                                ByVal iNPNCTypeID As Integer, ByVal oCampus As CCampus) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", iNPNCTypeID))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataSet("sp_Church_ReportNPNC11SummaryStats", New ArrayList, oParameters)

        End Function

        Public Function ULGMonthlySumary(ByVal iStartMonth As Integer, ByVal iStartYear As Integer, _
                                         ByVal iEndMonth As Integer, ByVal iEndYear As Integer, _
                                         ByVal oULG As CULG, ByVal oCampus As CCampus, _
                                         ByVal oMinistry As CMinistry, ByVal oRegion As CRegion) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLObject(oULG)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))

            Return DataAccess.GetDataSet("sp_Church_ReportUL01MonthlySummary", New ArrayList, oParameters)

        End Function
        
        Public Function UniqueULGAttendanceSumary(ByVal iStartMonth As Integer, ByVal iStartYear As Integer,
                                         ByVal iEndMonth As Integer, ByVal iEndYear As Integer,
                                         ByVal oULG As CULG, ByVal oCampus As CCampus,
                                         ByVal oMinistry As CMinistry, ByVal oRegion As CRegion) As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartMonth", iStartMonth))
            oParameters.Add(New SqlClient.SqlParameter("@StartYear", iStartYear))
            oParameters.Add(New SqlClient.SqlParameter("@EndMonth", iEndMonth))
            oParameters.Add(New SqlClient.SqlParameter("@EndYear", iEndYear))
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLObject(oULG)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))

            Return DataAccess.GetDataSet("sp_Church_ReportUniqueULMonthlySummary", New ArrayList, oParameters)

        End Function

        Public Function ReportMissingVolunteerTags(ByVal iConferenceID As Integer)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", iConferenceID))
            Return DataAccess.GetDataSet("sp_Events_ReportMissingVolunteerTags", New ArrayList, oParameters)

        End Function

        Public Function ReportMissingPriorityTags(ByVal iConferenceID As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", iConferenceID))
            Return DataAccess.GetDataTable("[dbo].[sp_Events_ReportMissingPriorityTags]", oParameters)

        End Function

    End Module

End Namespace
