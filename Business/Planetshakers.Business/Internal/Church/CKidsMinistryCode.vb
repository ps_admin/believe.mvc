Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CKidsMinistryCode

        Private miKidsMinistryCodeID As Integer
        Private msName As String
        Private msImage As Byte()
        Private msColor As String
        Private miSortOrder As Integer

        Public Sub New()

            miKidsMinistryCodeID = 0
            msName = String.Empty
            msImage = Nothing
            msColor = String.Empty
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miKidsMinistryCodeID = oDataRow("KidsMinistryCode_ID")
            msName = oDataRow("Name")
            msImage = IsNull(oDataRow("Image"), Nothing)
            msColor = oDataRow("Color")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public Sub Save()

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
            If msImage IsNot Nothing Then oParameters.Add(New SqlClient.SqlParameter("@Image", msImage))
            oParameters.Add(New SqlClient.SqlParameter("@Color", msColor))
            oParameters.Add(New SqlClient.SqlParameter("@SortOrder", miSortOrder))

            If Me.ID = 0 Then
                miKidsMinistryCodeID = DataAccess.GetValue("sp_Church_KidsMinistryCodeInsert", oParameters)
            Else
                oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryCode_ID", miKidsMinistryCodeID))
                DataAccess.ExecuteCommand("sp_Church_KidsMinistryCodeUpdate", oParameters)
            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miKidsMinistryCodeID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Property Image() As Byte()
            Get
                Return msImage
            End Get
            Set(ByVal Value As Byte())
                msImage = Value
            End Set
        End Property
        Public Property Color() As String
            Get
                Return msColor
            End Get
            Set(ByVal value As String)
                msColor = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace