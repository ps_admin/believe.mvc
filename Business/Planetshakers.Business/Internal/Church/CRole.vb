Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRole

        Private miRoleID As Integer
        Private msName As String
        Private miRoleTypeID As Integer
        Private miRoleTypeULID As Integer
        Private mbDeleted As Boolean

        Private mbChanged As Boolean

        Public Sub New()

            miRoleID = 0
            msName = String.Empty
            miRoleTypeID = 0
            miRoleTypeULID = 0
            mbDeleted = False

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRoleID = oDataRow("Role_ID")
            msName = oDataRow("Name")
            miRoleTypeID = IsNull(oDataRow("RoleType_ID"), 0)
            miRoleTypeULID = IsNull(oDataRow("RoleTypeUL_ID"), 0)
            mbDeleted = oDataRow("Deleted")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
                oParameters.Add(New SqlClient.SqlParameter("@RoleType_ID", miRoleTypeID))
                oParameters.Add(New SqlClient.SqlParameter("@RoleTypeUL_ID", SQLNull(miRoleTypeULID)))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", mbDeleted))

                If Me.ID = 0 Then
                    miRoleID = DataAccess.GetValue("sp_Church_RoleInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@Role_ID", miRoleID))
                    DataAccess.ExecuteCommand("sp_Church_RoleUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miRoleID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                If msName <> value Then mbChanged = True
                msName = value
            End Set
        End Property
        Public Property RoleTypeID() As Integer
            Get
                Return miRoleTypeID
            End Get
            Set(ByVal value As Integer)
                If miRoleTypeID <> value Then mbChanged = True
                miRoleTypeID = value
            End Set
        End Property
        Public ReadOnly Property RoleType() As CRoleType
            Get
                Return Lists.Internal.Church.GetRoleTypeByID(miRoleTypeID)
            End Get
        End Property
        Public Property RoleTypeULID() As Integer
            Get
                Return miRoleTypeULID
            End Get
            Set(ByVal value As Integer)
                If miRoleTypeULID <> value Then mbChanged = True
                miRoleTypeULID = value
            End Set
        End Property
        Public ReadOnly Property RoleTypeUL() As CRoleTypeUL
            Get
                Return Lists.Internal.Church.GetRoleTypeULByID(miRoleTypeULID)
            End Get
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                If mbDeleted <> value Then mbChanged = True
                mbDeleted = value
            End Set
        End Property

    End Class

End Namespace