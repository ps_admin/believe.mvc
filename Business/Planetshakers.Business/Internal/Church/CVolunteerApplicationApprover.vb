﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CVolunteerApplicationApprover

        Private miApproverID As Integer
        Private miContactID As Integer
        Private moContact As CContact
        Private miMinistryID As Integer
        Private miRegionID As Integer
        Private miCampusID As Integer
        Private miApprovalTypeID As Integer

        Public Sub New()

            miApproverID = 0
            miContactID = 0
            moContact = Nothing
            miMinistryID = 0
            miRegionID = 0
            miCampusID = 0
            miApprovalTypeID = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miApproverID = oDataRow("Approver_ID")
            miContactID = oDataRow("Contact_ID")
            miMinistryID = oDataRow("Ministry_ID")
            miRegionID = oDataRow("Region_ID")
            miCampusID = oDataRow("Campus_ID")
            miApprovalTypeID = oDataRow("ApprovalType_ID")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miApproverID
            End Get
        End Property
        Public ReadOnly Property ContactID() As Integer
            Get
                Return miContactID
            End Get
        End Property
        Public ReadOnly Property Contact() As CContact
            Get
                If moContact Is Nothing And miContactID > 0 Then
                    moContact = New CContact
                    moContact.LoadByID(miContactID)
                End If
                Return moContact
            End Get
        End Property
        Public ReadOnly Property MinistryID() As Integer
            Get
                Return miMinistryID
            End Get
        End Property
        Public ReadOnly Property Ministry() As CMinistry
            Get
                Return Lists.Internal.Church.GetMinistryByID(miMinistryID)
            End Get
        End Property
        Public ReadOnly Property RegionID() As Integer
            Get
                Return miRegionID
            End Get
        End Property
        Public ReadOnly Property Region() As CRegion
            Get
                Return Lists.Internal.Church.GetRegionByID(miRegionID)
            End Get
        End Property
        Public ReadOnly Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public ReadOnly Property ApprovalTypeID()
            Get
                Return miApprovalTypeID
            End Get
        End Property
        Public ReadOnly Property ApprovalType()
            Get
                Return Lists.Internal.Church.GetVolunteerApprovalTypeByID(miApprovalTypeID)
            End Get
        End Property
    End Class

End Namespace
