Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CULGContact

        Private miULGContactID As Integer
        Private miULGID As Integer
        Private miContactID As Integer
        Private moContact As CContact
        Private mdDateJoined As Date
        Private mbInactive As Boolean
        Private miSortOrder As Integer
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miULGContactID = 0
            miULGID = 0
            miContactID = 0
            moContact = Nothing
            mdDateJoined = Nothing
            mbInactive = False
            msGUID = System.Guid.NewGuid.ToString
            miSortOrder = 0

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miULGContactID = oDataRow("ULGContact_ID")
            miULGID = oDataRow("ULG_ID")
            miContactID = oDataRow("Contact_ID")
            mdDateJoined = oDataRow("DateJoined")
            mbInactive = oDataRow("Inactive")
            miSortOrder = oDataRow("SortOrder")
            msGUID = oDataRow("GUID").ToString

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", miULGID))
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@DateJoined", SQLDateTime(mdDateJoined)))
                oParameters.Add(New SqlClient.SqlParameter("@Inactive", mbInactive))
                oParameters.Add(New SqlClient.SqlParameter("@SortOrder", miSortOrder))
                oParameters.Add(New SqlClient.SqlParameter("@GUID", msGUID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miULGContactID = DataAccess.GetValue("sp_Church_ULGContactInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ULGContact_ID", miULGContactID))
                    DataAccess.ExecuteCommand("sp_Church_ULGContactUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miULGContactID
            End Get
        End Property
        Public Property ULGID() As Integer
            Get
                Return miULGID
            End Get
            Set(ByVal value As Integer)
                If miULGID <> value Then mbChanged = True
                miULGID = value
            End Set
        End Property
        Public ReadOnly Property ULG() As CULG
            Get
                If miULGID > 0 Then
                    Return Lists.Internal.Church.GetULGByID(miULGID)
                Else
                    Return Nothing
                End If
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public ReadOnly Property Contact() As CContact
            Get
                If moContact Is Nothing And miContactID > 0 Then
                    moContact = New CContact
                    moContact.LoadByID(miContactID)
                End If
                Return moContact
            End Get
        End Property
        Public Property DateJoined() As Date
            Get
                Return mdDateJoined
            End Get
            Set(ByVal value As Date)
                If mdDateJoined <> value Then mbChanged = True
                mdDateJoined = value
            End Set
        End Property
        Public Property Inactive() As Boolean
            Get
                Return mbInactive
            End Get
            Set(ByVal value As Boolean)
                If mbInactive <> value Then mbChanged = True
                mbInactive = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace