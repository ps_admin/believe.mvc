Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRestrictedAccessRole

        Private miRestrictedAccessRoleID As Integer
        Private miRoleID As Integer
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miRestrictedAccessRoleID = 0
            miRoleID = 0
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRestrictedAccessRoleID = oDataRow("RestrictedAccessRole_ID")
            miRoleID = oDataRow("Role_ID")

            mbChanged = False

        End Sub

        Public Sub Save(ByVal iContactRoleID As Integer, ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContactRole_ID", iContactRoleID))
                oParameters.Add(New SqlClient.SqlParameter("@Role_ID", SQLNull(miRoleID)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miRestrictedAccessRoleID = DataAccess.GetValue("sp_Church_RestrictedAccessRoleInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@RestrictedAccessRole_ID", miRestrictedAccessRoleID))
                    DataAccess.ExecuteCommand("sp_Church_RestrictedAccessRoleUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miRestrictedAccessRoleID
            End Get
        End Property
        Public Property RoleID() As Integer
            Get
                Return miRoleID
            End Get
            Set(ByVal value As Integer)
                If miRoleID <> value Then mbChanged = True
                miRoleID = value
            End Set
        End Property
        Public ReadOnly Property Role() As CRole
            Get
                Return Lists.Internal.Church.GetRoleByID(miRoleID)
            End Get
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace