Imports System.text

Namespace Internal.Church

    Public Module MEmailChurch

        Public Function SendEmailForgetPassword(ByVal oContact As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Dear " & oContact.FirstName & ", <br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("This email has been sent automatically by the Planetshakers Church Database Webpage in response")
            sEmail.AppendLine("to your request to reset your password.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("To reset your password and access the church database, please click the link below.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("If nothing happens, please copy and paste the link into the address bar of your internet browser.<br />")
            sEmail.AppendLine("This link will take you to the change password page.<br />")
            sEmail.AppendLine("<br />")
            Select Case DatabaseOptions.DatabaseInstance
                Case EnumDatabaseInstance.AU
                    sEmail.AppendLine("<a href=""https://church.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & """>https://church.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & "</a><br />")
                Case EnumDatabaseInstance.SA
                    sEmail.AppendLine("<a href=""http://churchsa.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & """>http://churchsa.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & "</a><br />")
            End Select
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("God Bless,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("The Planetshakers Church Database Team<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Ministries International Inc<br />")
            sEmail.AppendLine("ABN: 79 480 989 066<br />")
            sEmail.AppendLine("Post: PO Box 5171, South Melbourne Vic 3205<br />")
            sEmail.AppendLine("P: 1300 88 33 21 / +61 3 9896 7999<br />")
            sEmail.AppendLine("F: +61 3 9830 7683<br />")
            sEmail.AppendLine("E: church@planetshakers.com<br />")
            sEmail.AppendLine("W: www.planetshakers.com<br />")

            Dim oEmail As New CEmailChurch
            oEmail.EmailText = sEmail.ToString
            oEmail.Subject = "Planetshakers Church Database Password Reset"
            Return oEmail.SendEmail(oContact.Email, oContact.Name, sError)

        End Function

        Public Function SendEmailRegionalPastorNewAssignmentConfirmation(ByVal oRegionalPastors As CArrayList(Of CRegionalPastor), ByVal oNPNC As CNPNC, ByVal oPerson As CContact, ByVal oULGCoaches As CArrayList(Of CContact), ByVal oCarer As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            Dim sULGCoach As String = String.Empty
            Dim sCarer As String = "No Carer"

            Dim oCoach As CContact
            For Each oCoach In oULGCoaches
                sULGCoach += oCoach.Name & ", "
            Next
            If sULGCoach.Length = 0 Then
                sULGCoach = "No ULG Coach"
            Else
                sULGCoach = Left(sULGCoach, Len(sULGCoach) - 2)
            End If

            If oCarer IsNot Nothing Then sCarer = oCarer.Name

            Dim oRegionalPastor As CRegionalPastor
            For Each oRegionalPastor In oRegionalPastors
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Hi " & oRegionalPastor.Contact.FirstName & ",<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Confirming an email has been sent to the UL coach (" & sULGCoach & ") & carer (" & sCarer & ") to advise of the " & oNPNC.NPNCType.Name & " (" & oPerson.Name & ") recently added to their group.<br />")
                If oNPNC.ULG IsNot Nothing Then sEmail.AppendLine("<strong>Urban Life:</strong> " & oNPNC.ULG.CodeAndName & "<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Comments:<br />")
                Dim oComment As CContactComment
                For Each oComment In oPerson.Internal.Comments
                    If oComment.CommentSourceID = EnumCommentSource.NPNCInitialComment Then
                        If oComment.NPNCID = oNPNC.ID Or oComment.NPNCGUID = oNPNC.GUID Then
                            sEmail.AppendLine(oComment.Comment & "<br />")
                        End If
                    End If
                Next
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("PLEASE NOTE: This is an automatically generated email, for information purposes only. Please do not reply!<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Regards,<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Planetshakers Pastoral Team")

                Dim sEmailAddress As String
                If oRegionalPastor.Contact.Email <> String.Empty And Not oRegionalPastor.Contact.DoNotIncludeEmail1InMailingList Then
                    sEmailAddress = oRegionalPastor.Contact.Email
                ElseIf oRegionalPastor.Contact.Email2 <> String.Empty And Not oRegionalPastor.Contact.DoNotIncludeEmail2InMailingList Then
                    sEmailAddress = oRegionalPastor.Contact.Email2
                Else
                    sEmailAddress = String.Empty
                End If

                If sEmailAddress <> String.Empty Then
                    Dim oEmail As New CEmailChurch
                    oEmail.EmailText = sEmail.ToString
                    oEmail.Subject = oNPNC.NPNCType.Name & " Notification"
                    If Not oEmail.SendEmail(sEmailAddress, oRegionalPastor.Contact.Name, sError) Then
                        oEmail.Subject += " - Error occured while sending."
                        oEmail.EmailText += "<br /><br /><strong>Error occurred while sending:</strong><br /> " + sError
                        oEmail.SendEmail("darylw@planetshakers.com", oRegionalPastor.Contact.Name, sError)
                    End If
                End If

            Next

        End Function
        Public Function SendEmailRegionalPastorNewAssignmentConfirmation2(ByVal oRegionalPastors As CArrayList(Of CRegionalPastor), ByVal oNPNC As CNPNC, ByVal oPerson As CContact, ByVal oULGCoaches As CArrayList(Of CContact), ByVal oCarer As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            Dim sULGCoach As String = String.Empty
            Dim sCarer As String = "No Carer"

            Dim oCoach As CContact
            For Each oCoach In oULGCoaches
                sULGCoach += oCoach.Name & ", "
            Next
            If sULGCoach.Length = 0 Then
                sULGCoach = "No ULG Coach"
            Else
                sULGCoach = Left(sULGCoach, Len(sULGCoach) - 2)
            End If

            If oCarer IsNot Nothing Then sCarer = oCarer.FirstName + " " + oCarer.LastName

            Dim oRegionalPastor As CRegionalPastor
            For Each oRegionalPastor In oRegionalPastors
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Hi " & oRegionalPastor.Contact.FirstName & ",<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Confirming an email has been sent to the UL coach (" & sULGCoach & ") & carer (" & sCarer & ") to advise of the " & oNPNC.NPNCType.Name & " (" & oPerson.Name & ") recently added to their group.<br />")
                If oNPNC.ULG IsNot Nothing Then sEmail.AppendLine("<strong>Urban Life:</strong> " & oNPNC.ULG.CodeAndName & "<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Comments:<br />")
                Dim oComment As CContactComment
                For Each oComment In oPerson.Internal.Comments
                    If oComment.CommentSourceID = EnumCommentSource.NPNCInitialComment Then
                        If oComment.NPNCID = oNPNC.ID Or oComment.NPNCGUID = oNPNC.GUID Then
                            sEmail.AppendLine(oComment.Comment & "<br />")
                        End If
                    End If
                Next
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("PLEASE NOTE: This is an automatically generated email, for information purposes only. Please do not reply!<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Regards,<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("Planetshakers Pastoral Team")

                Dim sEmailAddress As String
                If oRegionalPastor.Contact.Email <> String.Empty And Not oRegionalPastor.Contact.DoNotIncludeEmail1InMailingList Then
                    sEmailAddress = oRegionalPastor.Contact.Email
                ElseIf oRegionalPastor.Contact.Email2 <> String.Empty And Not oRegionalPastor.Contact.DoNotIncludeEmail2InMailingList Then
                    sEmailAddress = oRegionalPastor.Contact.Email2
                Else
                    sEmailAddress = String.Empty
                End If

                If sEmailAddress <> String.Empty Then
                    Dim oEmail As New CEmailChurch
                    oEmail.EmailText = sEmail.ToString
                    oEmail.Subject = oNPNC.NPNCType.Name & " Notification"
                    If Not oEmail.SendEmail(sEmailAddress, oRegionalPastor.Contact.Name, sError) Then
                        oEmail.Subject += " - Error occured while sending."
                        oEmail.EmailText += "<br /><br /><strong>Error occurred while sending:</strong><br /> " + sError
                        oEmail.SendEmail("darylw@planetshakers.com", oRegionalPastor.Contact.Name, sError)
                    End If
                End If

            Next

        End Function

        Public Function SendEmailULCoachCarerNewAssignment(ByVal oContact As CContact, ByVal oNPNC As CNPNC, ByVal oULGCoach As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Hi Urban Life Coach,<br />")
            sEmail.AppendLine("<br />")
            sEmail.Append("Please find below the details of a " & oNPNC.NPNCType.Name & " added to your group")
            If oNPNC.Carer IsNot Nothing Then sEmail.Append(", under the care of " & oNPNC.Carer.Name)
            sEmail.AppendLine(". Please ensure that your " & oNPNC.NPNCType.Name & " has been contacted by the carer within 24hrs and their record on the church database has been updated.<br />")
            sEmail.AppendLine("<br />")
            If oNPNC.ULG IsNot Nothing Then sEmail.AppendLine("<strong>Urban Life:</strong> " & oNPNC.ULG.CodeAndName & "<br />")
            sEmail.AppendLine("<strong>Type:</strong> " & oNPNC.NPNCType.Name & "<br />")
            sEmail.AppendLine("<strong>Name:</strong> " & oContact.FullName & "<br />")
            sEmail.AppendLine("<strong>Gender:</strong> " & oContact.Gender & "<br />")
            sEmail.AppendLine("<strong>Home Phone:</strong> " & oContact.PhoneHome & "<br />")
            sEmail.AppendLine("<strong>Mobile Phone:</strong> " & oContact.PhoneMobile & "<br />")
            sEmail.AppendLine("<strong>Email:</strong> " & oContact.Email & "<br />")
            sEmail.AppendLine("<strong>DOB:</strong> " & IIf(oContact.DateOfBirth <> Nothing, FormatDate(oContact.DateOfBirth), "") & "<br />")
            sEmail.AppendLine("<strong>Initial Status:</strong> " & oNPNC.InitialStatus.Name & "<br />")
            sEmail.AppendLine("<strong>Initial Comments:</strong> ")
            Dim oComment As CContactComment
            For Each oComment In oContact.Internal.Comments
                If oComment.CommentSourceID = EnumCommentSource.NPNCInitialComment Then
                    If oComment.NPNCID = oNPNC.ID Or oComment.NPNCGUID = oNPNC.GUID Then
                        sEmail.AppendLine(oComment.Comment & "<br />")
                    End If
                End If
            Next
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")

            Dim oDate As Object = DataAccess.GetValue("sp_Church_ULGGetNextReportDueDate 0")
            Dim sDate As String = ""

            If oDate IsNot DBNull.Value Then sDate = CType(oDate, String)

            If sDate IsNot Nothing And IsDate(sDate) Then
                sEmail.AppendLine("The next Urban Life report is due on " & Format(CDate(sDate), "dd MMM") & "<br />")
            End If

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("PLEASE NOTE: This is an automatically generated email, for information purposes only. Please do not reply!<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Regards,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Pastoral Team")

            Dim sEmailAddress As String
            If oULGCoach.Email <> String.Empty And Not oULGCoach.DoNotIncludeEmail1InMailingList Then
                sEmailAddress = oULGCoach.Email
            ElseIf oULGCoach.Email2 <> String.Empty And Not oULGCoach.DoNotIncludeEmail2InMailingList Then
                sEmailAddress = oULGCoach.Email2
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = oNPNC.NPNCType.Name & " Allocation"
                If Not oEmail.SendEmail(sEmailAddress, oULGCoach.Name, sError) Then
                    oEmail.Subject += " - Error occured while sending."
                    oEmail.EmailText += "<br /><br /><strong>Error occurred while sending:</strong><br /> " + sError
                    oEmail.SendEmail("darylw@planetshakers.com", oULGCoach.Name, sError)
                End If
            End If

        End Function
        Public Function SendEmailULCoachCarerNewAssignment2(ByVal oContact As CContact, ByVal oNPNC As CNPNC, ByVal oULGCoach As CContact, ByVal oCarer As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Hi Urban Life Coach,<br />")
            sEmail.AppendLine("<br />")
            sEmail.Append("Please find below the details of a " & oNPNC.NPNCType.Name & " added to your group")
            If oCarer IsNot Nothing Then sEmail.Append(", under the care of " & oCarer.FirstName & " " & oCarer.LastName)
            sEmail.AppendLine(". Please ensure that your " & oNPNC.NPNCType.Name & " has been contacted by the carer within 24hrs and their record on the church database has been updated.<br />")
            sEmail.AppendLine("<br />")
            If oNPNC.ULG IsNot Nothing Then sEmail.AppendLine("<strong>Urban Life:</strong> " & oNPNC.ULG.CodeAndName & "<br />")
            sEmail.AppendLine("<strong>Type:</strong> " & oNPNC.NPNCType.Name & "<br />")
            sEmail.AppendLine("<strong>Name:</strong> " & oContact.FullName & "<br />")
            sEmail.AppendLine("<strong>Gender:</strong> " & oContact.Gender & "<br />")
            sEmail.AppendLine("<strong>Home Phone:</strong> " & oContact.PhoneHome & "<br />")
            sEmail.AppendLine("<strong>Mobile Phone:</strong> " & oContact.PhoneMobile & "<br />")
            sEmail.AppendLine("<strong>Email:</strong> " & oContact.Email & "<br />")
            sEmail.AppendLine("<strong>DOB:</strong> " & IIf(oContact.DateOfBirth <> Nothing, FormatDate(oContact.DateOfBirth), "") & "<br />")
            sEmail.AppendLine("<strong>Initial Status:</strong> " & oNPNC.InitialStatus.Name & "<br />")
            sEmail.AppendLine("<strong>Initial Comments:</strong> ")
            Dim oComment As CContactComment
            For Each oComment In oContact.Internal.Comments
                If oComment.CommentSourceID = EnumCommentSource.NPNCInitialComment Then
                    If oComment.NPNCID = oNPNC.ID Or oComment.NPNCGUID = oNPNC.GUID Then
                        sEmail.AppendLine(oComment.Comment & "<br />")
                    End If
                End If
            Next
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")

            Dim oDate As Object = DataAccess.GetValue("sp_Church_ULGGetNextReportDueDate 0")
            Dim sDate As String = ""

            If oDate IsNot DBNull.Value Then sDate = CType(oDate, String)

            If sDate IsNot Nothing And IsDate(sDate) Then
                sEmail.AppendLine("The next Urban Life report is due on " & Format(CDate(sDate), "dd MMM") & "<br />")
            End If

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("PLEASE NOTE: This is an automatically generated email, for information purposes only. Please do not reply!<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Regards,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Pastoral Team")

            Dim sEmailAddress As String
            If oULGCoach.Email <> String.Empty And Not oULGCoach.DoNotIncludeEmail1InMailingList Then
                sEmailAddress = oULGCoach.Email
            ElseIf oULGCoach.Email2 <> String.Empty And Not oULGCoach.DoNotIncludeEmail2InMailingList Then
                sEmailAddress = oULGCoach.Email2
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = oNPNC.NPNCType.Name & " Allocation"
                If Not oEmail.SendEmail(sEmailAddress, oULGCoach.Name, sError) Then
                    oEmail.Subject += " - Error occured while sending."
                    oEmail.EmailText += "<br /><br /><strong>Error occurred while sending:</strong><br /> " + sError
                    oEmail.SendEmail("darylw@planetshakers.com", oULGCoach.Name, sError)
                End If
            End If

        End Function

        Public Function SendEmailULCarerNewAssignment(ByVal oContact As CContact, ByVal oNPNC As CNPNC, ByVal oCarer As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Hi Planetshaker Carer,<br />")
            sEmail.AppendLine("<br />")
            If (Not oNPNC Is Nothing) Then
                sEmail.AppendLine("Please find below the details of a " & oNPNC.NPNCType.Name & " you have been assigned to care for. Please contact your " & oNPNC.NPNCType.Name & " within 24hrs and update their record on the church database.<br />")
                sEmail.AppendLine("<br />")
                sEmail.AppendLine("<strong>Urban Life:</strong> " & oNPNC.ULG.CodeAndName & "<br />")
                sEmail.AppendLine("<strong>Type:</strong> " & oNPNC.NPNCType.Name & "<br />")
            Else
                sEmail.AppendLine("Please find below the details of a person you have been assigned to care for. Please contact within 24hrs and update their record on the church database.<br />")
                sEmail.AppendLine("<br />")
            End If
            sEmail.AppendLine("<strong>Name:</strong> " & oContact.FullName & "<br />")
            sEmail.AppendLine("<strong>Gender:</strong> " & oContact.Gender & "<br />")
            sEmail.AppendLine("<strong>Home Phone:</strong> " & oContact.PhoneHome & "<br />")
            sEmail.AppendLine("<strong>Mobile Phone:</strong> " & oContact.PhoneMobile & "<br />")
            sEmail.AppendLine("<strong>Email:</strong> " & oContact.Email & "<br />")
            sEmail.AppendLine("<strong>DOB:</strong> " & IIf(oContact.DateOfBirth <> Nothing, FormatDate(oContact.DateOfBirth), "") & "<br />")
            If (Not oNPNC Is Nothing) Then
                sEmail.AppendLine("<strong>Initial Status:</strong> " & oNPNC.InitialStatus.Name & "<br />")
                sEmail.AppendLine("<strong>Initial Comments:</strong> ")
                Dim oComment As CContactComment
                For Each oComment In oContact.Internal.Comments
                    If oComment.CommentSourceID = EnumCommentSource.NPNCInitialComment Then
                        If oComment.NPNCID = oNPNC.ID Or oComment.NPNCGUID = oNPNC.GUID Then
                            sEmail.AppendLine(oComment.Comment & "<br />")
                        End If
                    End If
                Next
            End If
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")

            Dim oDate As Object = DataAccess.GetValue("sp_Church_ULGGetNextReportDueDate 0")
            Dim sDate As String = ""

            If oDate IsNot DBNull.Value Then sDate = CType(oDate, String)

            If sDate IsNot Nothing And IsDate(sDate) Then
                sEmail.AppendLine("The next Urban Life report is due on " & Format(CDate(sDate), "dd MMM") & "<br />")
            End If

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("PLEASE NOTE: This is an automatically generated email, for information purposes only. Please do not reply!<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Regards,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Pastoral Team")

            Dim sEmailAddress As String
            If oCarer.Email <> String.Empty And Not oCarer.DoNotIncludeEmail1InMailingList Then
                sEmailAddress = oCarer.Email
            ElseIf oCarer.Email2 <> String.Empty And Not oCarer.DoNotIncludeEmail2InMailingList Then
                sEmailAddress = oCarer.Email2
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = "Urban Life Member Allocation"
                If Not oEmail.SendEmail(sEmailAddress, oCarer.FirstName + " " + oCarer.LastName, sError) Then
                    oEmail.Subject += " - Error occured while sending."
                    oEmail.EmailText += "<br /><br /><strong>Error occurred while sending:</strong><br /> " + sError
                    oEmail.SendEmail("darylw@planetshakers.com", oCarer.Name, sError)
                End If
            End If

        End Function

        Public Function SendEmailCarerNewAssignment2(ByVal oContact As CContact, ByVal oNPNC As CNPNC, ByVal oCarer As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Hi Planetshaker Carer,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Please find below the details of a " & oNPNC.NPNCType.Name & " you have been assigned to care for. Please contact your " & oNPNC.NPNCType.Name & " within 24hrs and update their record on the church database.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>Urban Life:</strong> " & oNPNC.ULG.CodeAndName & "<br />")
            sEmail.AppendLine("<strong>Type:</strong> " & oNPNC.NPNCType.Name & "<br />")
            sEmail.AppendLine("<strong>Name:</strong> " & oContact.FullName & "<br />")
            sEmail.AppendLine("<strong>Gender:</strong> " & oContact.Gender & "<br />")
            sEmail.AppendLine("<strong>Home Phone:</strong> " & oContact.PhoneHome & "<br />")
            sEmail.AppendLine("<strong>Mobile Phone:</strong> " & oContact.PhoneMobile & "<br />")
            sEmail.AppendLine("<strong>Email:</strong> " & oContact.Email & "<br />")
            sEmail.AppendLine("<strong>DOB:</strong> " & IIf(oContact.DateOfBirth <> Nothing, FormatDate(oContact.DateOfBirth), "") & "<br />")
            sEmail.AppendLine("<strong>Initial Status:</strong> " & oNPNC.InitialStatus.Name & "<br />")
            sEmail.AppendLine("<strong>Initial Comments:</strong> ")
            Dim oComment As CContactComment
            For Each oComment In oContact.Internal.Comments
                If oComment.CommentSourceID = EnumCommentSource.NPNCInitialComment Then
                    If oComment.NPNCID = oNPNC.ID Or oComment.NPNCGUID = oNPNC.GUID Then
                        sEmail.AppendLine(oComment.Comment & "<br />")
                    End If
                End If
            Next
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")

            Dim oDate As Object = DataAccess.GetValue("sp_Church_ULGGetNextReportDueDate 0")
            Dim sDate As String = ""

            If oDate IsNot DBNull.Value Then sDate = CType(oDate, String)

            If sDate IsNot Nothing And IsDate(sDate) Then
                sEmail.AppendLine("The next Urban Life report is due on " & Format(CDate(sDate), "dd MMM") & "<br />")
            End If

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("PLEASE NOTE: This is an automatically generated email, for information purposes only. Please do not reply!<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Regards,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Pastoral Team")

            Dim sEmailAddress As String
            If oCarer.Email <> String.Empty And Not oCarer.DoNotIncludeEmail1InMailingList Then
                sEmailAddress = oCarer.Email
            ElseIf oCarer.Email2 <> String.Empty And Not oCarer.DoNotIncludeEmail2InMailingList Then
                sEmailAddress = oCarer.Email2
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = oNPNC.NPNCType.Name & " Allocation"
                If Not oEmail.SendEmail(sEmailAddress, oCarer.FirstName + " " + oCarer.LastName, sError) Then
                    If Not oNPNC.Carer Is Nothing Then
                        oEmail.Subject += " - Error occured while sending."
                        oEmail.EmailText += "<br /><br /><strong>Error occurred while sending:</strong><br /> " + sError

                        oEmail.SendEmail("darylw@planetshakers.com", oNPNC.Carer.Name, sError)
                    End If
                End If
            End If

        End Function

        Public Function SendEmailCarerNewAssignment(ByVal oContact As CContact, ByVal oNPNC As CNPNC) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Hi Planetshaker Carer,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Please find below the details of a " & oNPNC.NPNCType.Name & " you have been assigned to care for. Please contact your " & oNPNC.NPNCType.Name & " within 24hrs and update their record on the church database.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>Urban Life:</strong> " & oNPNC.ULG.CodeAndName & "<br />")
            sEmail.AppendLine("<strong>Type:</strong> " & oNPNC.NPNCType.Name & "<br />")
            sEmail.AppendLine("<strong>Name:</strong> " & oContact.FullName & "<br />")
            sEmail.AppendLine("<strong>Gender:</strong> " & oContact.Gender & "<br />")
            sEmail.AppendLine("<strong>Home Phone:</strong> " & oContact.PhoneHome & "<br />")
            sEmail.AppendLine("<strong>Mobile Phone:</strong> " & oContact.PhoneMobile & "<br />")
            sEmail.AppendLine("<strong>Email:</strong> " & oContact.Email & "<br />")
            sEmail.AppendLine("<strong>DOB:</strong> " & IIf(oContact.DateOfBirth <> Nothing, FormatDate(oContact.DateOfBirth), "") & "<br />")
            sEmail.AppendLine("<strong>Initial Status:</strong> " & oNPNC.InitialStatus.Name & "<br />")
            sEmail.AppendLine("<strong>Initial Comments:</strong> ")
            Dim oComment As CContactComment
            For Each oComment In oContact.Internal.Comments
                If oComment.CommentSourceID = EnumCommentSource.NPNCInitialComment Then
                    If oComment.NPNCID = oNPNC.ID Or oComment.NPNCGUID = oNPNC.GUID Then
                        sEmail.AppendLine(oComment.Comment & "<br />")
                    End If
                End If
            Next
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")

            Dim oDate As Object = DataAccess.GetValue("sp_Church_ULGGetNextReportDueDate 0")
            Dim sDate As String = ""

            If oDate IsNot DBNull.Value Then sDate = CType(oDate, String)

            If sDate IsNot Nothing And IsDate(sDate) Then
                sEmail.AppendLine("The next Urban Life report is due on " & Format(CDate(sDate), "dd MMM") & "<br />")
            End If

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("PLEASE NOTE: This is an automatically generated email, for information purposes only. Please do not reply!<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Regards,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Pastoral Team")

            Dim sEmailAddress As String
            If oNPNC.Carer.Email <> String.Empty And Not oNPNC.Carer.DoNotIncludeEmail1InMailingList Then
                sEmailAddress = oNPNC.Carer.Email
            ElseIf oNPNC.Carer.Email2 <> String.Empty And Not oNPNC.Carer.DoNotIncludeEmail2InMailingList Then
                sEmailAddress = oNPNC.Carer.Email2
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = oNPNC.NPNCType.Name & " Allocation"
                If Not oEmail.SendEmail(sEmailAddress, oNPNC.Carer.Name, sError) Then
                    oEmail.Subject += " - Error occured while sending."
                    oEmail.EmailText += "<br /><br /><strong>Error occurred while sending:</strong><br /> " + sError
                    oEmail.SendEmail("darylw@planetshakers.com", oNPNC.Carer.Name, sError)
                End If
            End If

        End Function

        Public Function SendEmailPCRNotCompleted(ByVal oContact As CContact, ByVal oULG As CULG, ByVal oPCRDate As CPCRDate) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("Hi " & oContact.FirstName & ",<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Just a reminder that we haven't received your completed PCR for the week ending <strong>" & Format(oPCRDate.Date, "dddd, MMMM dd") & "</strong><br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("This PCR is for Urban Life Group: <strong>" & oULG.Code & " - " & oULG.Name & "</strong><br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Can you please complete this asap, to assist our Pastoral Team. If you have completed your PCR and received this email in error please contact your Regional ")
            sEmail.AppendLine("Pastor or Department Leader.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("PLEASE NOTE: This is an automatically generated email, for information purposes only. Please do not reply to this email address!<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Regards,<br />")
            sEmail.AppendLine("Pastoral Team.<br />")
            sEmail.AppendLine("<br />")

            Dim sEmailAddress As String
            If oContact.Email <> String.Empty And Not oContact.DoNotIncludeEmail1InMailingList Then
                sEmailAddress = oContact.Email
            ElseIf oContact.Email2 <> String.Empty And Not oContact.DoNotIncludeEmail2InMailingList Then
                sEmailAddress = oContact.Email2
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = "Planetshakers City Church - PCR Report"
                oEmail.SendEmail(sEmailAddress, oContact.Name, sError)
            End If

        End Function

        'This prcedure is used to send 24/7 Prayer emails to individual contacts
        Public Function SendEmail247Prayer(ByVal oContact As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("Dear " & oContact.FirstName & ", <br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Thank you for joining and partnering with us in prayer. Whether you have just signed up for the first time or are receiving this email as a reminder of your previous commitment, we greatly value your dedication to be part of our 24/7 Prayer Team. There is tremendous Power available to us as we join together, agree and declare ""Let Heaven Come"" over Planetshakers! <br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Your allocated 24/7 Prayer time is:<br />")
            sEmail.AppendLine("<br />")

            Dim o247Prayer As C247Prayer
            For Each o247Prayer In oContact.Internal.Prayer247
                sEmail.AppendLine("<strong>" & o247Prayer.Day.Name & ", " & o247Prayer.Time.Name & " until " & Format(DateAdd(DateInterval.Minute, o247Prayer.Duration.Duration * 60, CDate(o247Prayer.Time.Name)), "h:mmtt").ToLower & "</strong><br />")
            Next
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Please find our annual, standard guideline on ""How To Pray"" below. You will soon begin receiving our fortnightly 24/7 Prayer letters, which will include prayer points from each of our Department Pastors.<br />")

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<h3><strong>How To Pray</strong></h3>")
            sEmail.AppendLine("As you prepare to pray, please keep in mind the following principles of effective prayer �")
            sEmail.AppendLine("<br />")

            sEmail.AppendLine("<ol><br />")

            sEmail.AppendLine("<li><strong>Be Still</strong><br />")
            sEmail.AppendLine("Psalm 46:10 says, <i>""Be still and know that I am God.""</i> Find a quiet place, eliminate distractions and centre on Him.</li><br />")

            sEmail.AppendLine("<li><strong>Worship, praise and adore Him</strong><br />")
            sEmail.AppendLine("<i>""Enter His gates with thanksgiving and into His courts with praise. Be thankful to Him and bless His name for the Lord is good, His mercy is everlasting and his truth endures to all generations.""</i> (Psalm 10:4-5)<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("It�s a great idea to put on a worship CD and begin to sing along. Focus on God�s goodness, thanking, praising and expressing your love to Him. This process builds our faith and opens us up to His presence.</li><br />")

            sEmail.AppendLine("<li><strong>Confess anything the displeases Him and receive forgiveness</strong><br />")
            sEmail.AppendLine("<i>""Who may ascend in to the hill of the Lord and who may stand in His holy place. He who has clean hands and a pure heart.""</i>(Psalm 24: 3-4.) He will always deal with us first. </li><br />")

            sEmail.AppendLine("<li><strong>Specify your petitions and requests and wait on the Lord</strong><br />")
            sEmail.AppendLine("Bring each prayer point before the Lord and allow the Holy Spirit to lead you. Pray as He directs you, for example you might be prompted to stay with one point rather than pray through the entire list.</li><br />")

            sEmail.AppendLine("<li><strong>Use the Weapons God has given you:</strong><br />")
            sEmail.AppendLine("<ul>")
            sEmail.AppendLine("<li><i>Pray the Word</i><br />Write down specific Scriptures to use when you pray, for example 1 John 5: 14-15, Ephesians 1:18-19 or Colossians 1:9-12</li><br />")
            sEmail.AppendLine("<li><i>Use Jesus� Blood</i><br />Cover yourself and whoever you are praying for with the blood of Jesus � it provides protection, cleansing and life (Exodus 12:13)</li><br />")
            sEmail.AppendLine("<li><i>Use Jesus� Name</i><br />For all the power and authority has been given to us in the name of Jesus (Philippians 2:10). Declare the Lordship of Jesus over our Church, specific Schools, Universities, Cape Town, North East, Geelong, etc.</li><br />")
            sEmail.AppendLine("<li><i>Pray in the Spirit</i><br />Romans 8:26-27 says, <i>""In the same way the spirit always helps our weaknesses for we do not know how to pray as we should, but the Spirit himself intercedes for us with groanings too deep for words and He who searches the hearts knows what the mind of the Spirit is because He intercedes for the Saints according to the will of God.""</i><br /><br />It is important to use your heavenly language (tongues) to pray, especially if you have run out of words to say, get stuck or feel the Holy Spirit prompting you.</li> <br /")
            sEmail.AppendLine("<li><i>Pray in Faith</i><br />Believe that you have received what you�ve asked for. Mark 11:22-24 says, <i>""Have faith in God."" Jesus answered, ""I tell you the truth, if anyone says to the mountain, go throw yourself into the sea and does not doubt in his heart but believes that what he says will happen, it will be done for him. Therefore I tell you, whatever you ask for in prayer, believe.""</i></li></ul></li><br />")
            sEmail.AppendLine("<li><strong>Give thanks in faith for what He has done</strong><br />")
            sEmail.AppendLine("Psalm 9:1 says, <i>""I will give thanks to you, LORD, with all my heart; I will tell of all your wonderful deeds.""</i> Thank God that we will see the work in schools explode, we will see breakthrough in universities, etc.</li>")

            sEmail.AppendLine("</ol><br />")
            sEmail.AppendLine("We trust you will find your participation on the 24/7 Prayer Team purposeful and rewarding, and our team continue to cover you and your families in prayer. You are such a blessing to our Church family as you commit to pray.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("God Bless You,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>Planetshakers 24/7 Prayer Team</strong><br/>")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<i>Please note this is an automated email.</i>")

            Dim sEmailAddress As String
            If oContact.Email <> String.Empty Then
                sEmailAddress = oContact.Email
            ElseIf oContact.Email2 <> String.Empty Then
                sEmailAddress = oContact.Email2
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = "Thank You for Signing Up for 24/7 Prayer"
                Return oEmail.SendEmail(sEmailAddress, oContact.Name, sError)
            End If

        End Function

        'This procedure is used to send 24/7 Prayer emails to the bulk list (Uses datarows - much quicker than loading each contact)
        Public Function SendEmail247Prayer(ByRef oDataRow As DataRow) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("Dear " & oDataRow("FirstName") & ", <br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Thank you for joining and partnering with us in prayer. Whether you have just signed up for the first time or are receiving this email as a reminder of your previous commitment, we greatly value your dedication to be part of our 24/7 Prayer Team. There is tremendous Power available to us as we join together, agree and declare ""Let Heaven Come"" over Planetshakers! <br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Your allocated 24/7 Prayer time is:<br />")
            sEmail.AppendLine("<br />")

            Dim o247PrayerRow As DataRow
            For Each o247PrayerRow In oDataRow.GetChildRows("247Prayer")
                sEmail.AppendLine("<strong>" & o247PrayerRow("Day") & ", " & o247PrayerRow("Time") & " until " & Format(DateAdd(DateInterval.Minute, o247PrayerRow("Duration") * 60, CDate(o247PrayerRow("Time"))), "hh:mmtt").ToLower & "</strong><br />")
            Next
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Please find our annual, standard guideline on ""How To Pray"" below. You will soon begin receiving our fortnightly 24/7 Prayer letters, which will include prayer points from each of our Department Pastors.<br />")

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<h3><strong>How To Pray</strong></h3>")
            sEmail.AppendLine("As you prepare to pray, please keep in mind the following principles of effective prayer �")
            sEmail.AppendLine("<br />")

            sEmail.AppendLine("<ol><br />")

            sEmail.AppendLine("<li><strong>Be Still</strong><br />")
            sEmail.AppendLine("Psalm 46:10 says, <i>""Be still and know that I am God.""</i> Find a quiet place, eliminate distractions and centre on Him.</li><br />")

            sEmail.AppendLine("<li><strong>Worship, praise and adore Him</strong><br />")
            sEmail.AppendLine("<i>""Enter His gates with thanksgiving and into His courts with praise. Be thankful to Him and bless His name for the Lord is good, His mercy is everlasting and his truth endures to all generations.""</i> (Psalm 10:4-5)<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("It�s a great idea to put on a worship CD and begin to sing along. Focus on God�s goodness, thanking, praising and expressing your love to Him. This process builds our faith and opens us up to His presence.</li><br />")

            sEmail.AppendLine("<li><strong>Confess anything the displeases Him and receive forgiveness</strong><br />")
            sEmail.AppendLine("<i>""Who may ascend in to the hill of the Lord and who may stand in His holy place. He who has clean hands and a pure heart.""</i>(Psalm 24: 3-4.) He will always deal with us first. </li><br />")

            sEmail.AppendLine("<li><strong>Specify your petitions and requests and wait on the Lord</strong><br />")
            sEmail.AppendLine("Bring each prayer point before the Lord and allow the Holy Spirit to lead you. Pray as He directs you, for example you might be prompted to stay with one point rather than pray through the entire list.</li><br />")

            sEmail.AppendLine("<li><strong>Use the Weapons God has given you:</strong><br />")
            sEmail.AppendLine("<ul>")
            sEmail.AppendLine("<li><i>Pray the Word</i><br />Write down specific Scriptures to use when you pray, for example 1 John 5: 14-15, Ephesians 1:18-19 or Colossians 1:9-12</li><br />")
            sEmail.AppendLine("<li><i>Use Jesus� Blood</i><br />Cover yourself and whoever you are praying for with the blood of Jesus � it provides protection, cleansing and life (Exodus 12:13)</li><br />")
            sEmail.AppendLine("<li><i>Use Jesus� Name</i><br />For all the power and authority has been given to us in the name of Jesus (Philippians 2:10). Declare the Lordship of Jesus over our Church, specific Schools, Universities, Cape Town, North East, Geelong, etc.</li><br />")
            sEmail.AppendLine("<li><i>Pray in the Spirit</i><br />Romans 8:26-27 says, <i>""In the same way the spirit always helps our weaknesses for we do not know how to pray as we should, but the Spirit himself intercedes for us with groanings too deep for words and He who searches the hearts knows what the mind of the Spirit is because He intercedes for the Saints according to the will of God.""</i><br /><br />It is important to use your heavenly language (tongues) to pray, especially if you have run out of words to say, get stuck or feel the Holy Spirit prompting you.</li> <br /")
            sEmail.AppendLine("<li><i>Pray in Faith</i><br />Believe that you have received what you�ve asked for. Mark 11:22-24 says, <i>""Have faith in God."" Jesus answered, ""I tell you the truth, if anyone says to the mountain, go throw yourself into the sea and does not doubt in his heart but believes that what he says will happen, it will be done for him. Therefore I tell you, whatever you ask for in prayer, believe.""</i></li></ul></li><br />")
            sEmail.AppendLine("<li><strong>Give thanks in faith for what He has done</strong><br />")
            sEmail.AppendLine("Psalm 9:1 says, <i>""I will give thanks to you, LORD, with all my heart; I will tell of all your wonderful deeds.""</i> Thank God that we will see the work in schools explode, we will see breakthrough in universities, etc.</li>")

            sEmail.AppendLine("</ol><br />")
            sEmail.AppendLine("We trust you will find your participation on the 24/7 Prayer Team purposeful and rewarding, and our team continue to cover you and your families in prayer. You are such a blessing to our Church family as you commit to pray.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("God Bless You,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>Planetshakers 24/7 Prayer Team</strong><br/>")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<i>Please note this is an automated email.</i>")

            Dim oEmail As New CEmailChurch
            oEmail.EmailText = sEmail.ToString
            oEmail.Subject = "Thank You for Signing Up for 24/7 Prayer"
            Return oEmail.SendEmail(oDataRow("Email"), oDataRow("FullName"), sError)

        End Function

        Public Function SendEmailPCRInboxComment(ByVal oCommentAbout As CContact, ByVal oCommentCreator As CContact, ByVal oNewComment As CContactComment, ByVal oOriginalComment As CContactComment, ByVal oULGList As CArrayList(Of CULG)) As Boolean

            Dim sError As String = String.Empty
            Dim cEmailAddresses As New ArrayList
            Dim sEmail As New StringBuilder
            Dim bNPNC As Boolean

            bNPNC = (oCommentAbout.Internal.ChurchStatusID = EnumChurchStatus.NewChristian Or oCommentAbout.Internal.ChurchStatusID = EnumChurchStatus.NewPeople)

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Hi ULG Coach" & IIf(bNPNC, " / Carer", "") & ",<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine(oCommentCreator.Name & " has made a comment regarding the following person: " & oCommentAbout.Name & ".<br />This will require action from yourself.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>Comment:</strong><br /> " & oNewComment.Comment & "<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>This was in reply to your comment:</strong><br /><i>" & oOriginalComment.Comment & "</i><br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Please log into the Planetshakers Database and respond accordingly via a PCR Comment.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("PLEASE NOTE: This is an automatically generated email, for information purposes only. Please do not reply!<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Regards,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Pastoral Team")

            If bNPNC Then
                For Each oNPNC As CNPNC In oCommentAbout.Internal.NPNC
                    If Not oNPNC.Outcomed And oNPNC.Carer IsNot Nothing Then
                        If oNPNC.Carer.Email <> String.Empty And Not oNPNC.Carer.DoNotIncludeEmail1InMailingList Then
                            cEmailAddresses.Add(New String() {oNPNC.Carer.Email, oNPNC.Carer.Name})
                        ElseIf oNPNC.Carer.Email2 <> String.Empty And Not oNPNC.Carer.DoNotIncludeEmail2InMailingList Then
                            cEmailAddresses.Add(New String() {oNPNC.Carer.Email2, oNPNC.Carer.Name})
                        End If
                        Exit For 'Only send to the first contact
                    End If
                Next
            End If

            For Each oULG As CULG In oULGList
                For Each oCoach As CContact In oULG.GetCoachList
                    If oCoach.Email <> String.Empty And Not oCoach.DoNotIncludeEmail1InMailingList Then
                        cEmailAddresses.Add(New String() {oCoach.Email, oCoach.Name})
                    ElseIf oCoach.Email2 <> String.Empty And Not oCoach.DoNotIncludeEmail2InMailingList Then
                        cEmailAddresses.Add(New String() {oCoach.Email2, oCoach.Name})
                    End If
                Next
            Next

            If cEmailAddresses.Count > 0 Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = "PCR Inbox Comment has been made"
                For Each sEmailAddress As String() In cEmailAddresses
                    If Not oEmail.SendEmail(sEmailAddress(0), sEmailAddress(1), sError) Then
                        'REMOVED on 6/8/2014 as requested by Andre
                        'When emails fail to send, send to daryl
                        'oEmail.Subject += " - Error occured while sending."
                        'oEmail.EmailText += "<br /><br /><strong>Error occurred while sending:</strong><br /> " + sError
                        'oEmail.SendEmail("darylw@planetshakers.com", sEmailAddress(1), sError)
                    End If
                Next
            End If

        End Function

        Public Function SendEmailUlCarerRoleAssignment(ByVal oContact As CContact)

            Dim sEmail As New StringBuilder
            Dim sError As String = String.Empty
          
            
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Hi " & oContact.FirstName & " " & oContact.LastName & ",<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<p>Thanks so much for serving in this very important function as an Urban Life Carer, we know that as you accept this appointment Gods anointing will come upon you to supernaturally care for people.</p>")
            sEmail.AppendLine("<p><b>If you already have access to the Church Database please keep on using your same details to log in and ignore the rest of this email.</b></p>")
            sEmail.AppendLine("<p>If this is your first time accessing the Church database please see the below details to assist you in logging on to the church database:</p>")
            sEmail.AppendLine("<p>Email Address : " + oContact.Email + "</p>")
            sEmail.AppendLine("<p>Password : planet1 </p><br />")
            sEmail.AppendLine("<p><b>PLEASE NOTE</b>:</p><p>This is a temporary password and you will be prompted to change it the first time you log in.</p>")
            sEmail.AppendLine("<p>This is an automatically generated email for information purposes only. Do not reply to this email address. If you have any questions in regards to this email please contact our offices at (03) 9896 7999.</p>")
            sEmail.AppendLine("<p>Regards,</p>")
            sEmail.AppendLine("<p>Planetshakers Pastoral Team</p>")

            Dim sEmailAddress As String
            If oContact.Email <> String.Empty Then
                sEmailAddress = oContact.Email
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = "Urban Life Carer Allocation"
                Return oEmail.SendEmail(sEmailAddress, oContact.Name, sError)
            End If

        End Function

        Public Function SendEmailNewVolunteerNotificationToMinistry(ByVal oContact As CContact, _
                                                          ByVal oVolunteer As CContact, _
                                                          ByVal sVolArea As String)

            Dim sEmail As New StringBuilder
            Dim sError As String = String.Empty

            sEmail.AppendLine("<br/>")
            sEmail.AppendLine("Hi " & oContact.FullName & ",<br />")
            sEmail.AppendLine("<br/>")
            sEmail.AppendLine("<p>" & oVolunteer.FullName & " has been approved to serve in your area ( " & sVolArea & " )!</p>")
            sEmail.AppendLine("<p>Please ensure that you contact them to get them activated!</p>")
            sEmail.AppendLine("<p>Name: " & oVolunteer.FullName & "<br/>")
            sEmail.AppendLine("Email: " & oVolunteer.Email & "<br/>")
            sEmail.AppendLine("Email2: " & oVolunteer.Email2 & "<br/>")
            sEmail.AppendLine("Mobile: " & oVolunteer.PhoneMobile & "</p>")

            sEmail.AppendLine("<p>This is an automatically generated email for information purposes only. Do not reply to this email address. If you have any questions in regards to this email please contact our offices at (03) 9896 7999.</p>")
            sEmail.AppendLine("<p>Regards,<br/>")
            sEmail.AppendLine("Planetshakers Pastoral Team</p>")

            Dim sEmailAddress As String
            If oContact.Email <> String.Empty Then
                sEmailAddress = oContact.Email
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = "[" & sVolArea & "] " & "New Volunteer - " & oVolunteer.FullName
                Return oEmail.SendEmail(sEmailAddress, oContact.Name, sError)
            End If
        End Function

        Public Function SendEmailNewVolunteerNotificationToCoach(ByVal oContact As CContact, _
                                                                 ByVal oVolunteer As CContact, _
                                                                 ByVal sVolArea As String)

            Dim sEmail As New StringBuilder
            Dim sError As String = String.Empty

            sEmail.AppendLine("<br/>")
            sEmail.AppendLine("Hi " & oContact.FullName & ",<br />")
            sEmail.AppendLine("<br/>")
            sEmail.AppendLine("<p>" & oVolunteer.FullName & ", your urban life member has been approved to serve in " & sVolArea & "!</p>")
            sEmail.AppendLine("<p>This is an automatically generated email for information purposes only. Do not reply to this email address. If you have any questions in regards to this email please contact our offices at (03) 9896 7999.</p>")
            sEmail.AppendLine("<p>Regards,<br/>")
            sEmail.AppendLine("Planetshakers Pastoral Team</p>")

            Dim sEmailAddress As String
            If oContact.Email <> String.Empty Then
                sEmailAddress = oContact.Email
            Else
                sEmailAddress = String.Empty
            End If

            If sEmailAddress <> String.Empty Then
                Dim oEmail As New CEmailChurch
                oEmail.EmailText = sEmail.ToString
                oEmail.Subject = "[" & sVolArea & "] " & "New Volunteer - " & oVolunteer.FullName
                Return oEmail.SendEmail(sEmailAddress, oContact.Name, sError)
            End If
        End Function

    End Module

End Namespace