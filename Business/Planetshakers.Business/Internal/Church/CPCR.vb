Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CPCR

        Private miPCRID As Integer
        Private moPCRDate As CPCRDate
        Private moULG As CULG
        Private miULGChildren As Integer
        Private miULGVisitors As Integer
        Private mdULGOffering As Decimal
        Private msGeneralComments As String
        Private mbReportCompleted As Boolean
        Private mdReportCompletedDate As Date
        Private mcPCRContact As CArrayList(Of CPCRContact)
        Private mcPCRVisitor As CArrayList(Of CPCRVisitor)
        Private mbOfferingReceived As Boolean

        Private mbChanged As Boolean

        Public Sub New()

            miPCRID = 0
            moPCRDate = Nothing
            moULG = Nothing
            miULGChildren = 0
            miULGVisitors = 0
            mdULGOffering = 0
            msGeneralComments = String.Empty
            mbReportCompleted = False
            mdReportCompletedDate = Nothing
            mcPCRContact = New CArrayList(Of CPCRContact)
            mcPCRVisitor = New CArrayList(Of CPCRVisitor)
            mbOfferingReceived = False
            mbChanged = False

        End Sub
        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miPCRID = oDataRow("PCR_ID")
            moPCRDate = Lists.Internal.Church.GetPCRDateByID(oDataRow("PCRDate_ID"))
            moULG = Lists.Internal.Church.GetULGByID(oDataRow("ULG_ID"))
            miULGChildren = oDataRow("ULGChildren")
            miULGVisitors = oDataRow("ULGVisitors")
            mdULGOffering = oDataRow("ULGOffering")
            mbReportCompleted = oDataRow("ReportCompleted")
            mdReportCompletedDate = IsNull(oDataRow("ReportCompletedDate"), Nothing)
            mbOfferingReceived = oDataRow("OfferingReceived")

            mbChanged = False

        End Sub


        Public Function LoadByDateAndULG2(ByVal Access As Boolean, ByVal iContactID As Integer, ByVal iPCRDateID As Integer, ByVal iULGID As Integer) As Boolean

            Dim oDataSet As DataSet

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "PCR"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "PCRContact"))
            oTableMappings.Add(New Common.DataTableMapping("Table2", "Comment"))
            oTableMappings.Add(New Common.DataTableMapping("Table3", "PCRVisitor"))

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@PCRDate_ID", iPCRDateID))
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", iULGID))
            If (Access) Then
                'NightlyPCRIncompleteEmail()
                oDataSet = DataAccess.GetDataSet("sp_Church_PCRRead", oTableMappings, oParameters)
            Else
                '----------------------------------------------------------------------
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))
                '-----------------------------------------------------------------------
                'NightlyPCRIncompleteEmail()
                oDataSet = DataAccess.GetDataSet("sp_Church_PCRRead2", oTableMappings, oParameters)
            End If
            oDataSet.Relations.Add("PCRVisitor", oDataSet.Tables("PCR").Columns("PCR_ID"), _
                                                  oDataSet.Tables("PCRVisitor").Columns("PCR_ID"), False)

            oDataSet.Relations.Add("PCRContact", oDataSet.Tables("PCR").Columns("PCR_ID"), _
                                                  oDataSet.Tables("PCRContact").Columns("PCR_ID"))

            oDataSet.Relations.Add("Comment", oDataSet.Tables("PCRContact").Columns("Contact_ID"), _
                                                  oDataSet.Tables("Comment").Columns("Contact_ID"))

            If oDataSet.Tables("PCR").Rows.Count = 1 Then
                Return LoadDataRow(oDataSet.Tables("PCR").Rows(0))
            Else
                Return False
            End If

        End Function
        Public Function LoadByDateAndULG(ByVal iPCRDateID As Integer, ByVal iULGID As Integer) As Boolean

            Dim oDataSet As DataSet

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "PCR"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "PCRContact"))
            oTableMappings.Add(New Common.DataTableMapping("Table2", "Comment"))
            oTableMappings.Add(New Common.DataTableMapping("Table3", "PCRVisitor"))

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@PCRDate_ID", iPCRDateID))
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", iULGID))

            oDataSet = DataAccess.GetDataSet("sp_Church_PCRRead", oTableMappings, oParameters)

            oDataSet.Relations.Add("PCRVisitor", oDataSet.Tables("PCR").Columns("PCR_ID"), _
                                                  oDataSet.Tables("PCRVisitor").Columns("PCR_ID"), False)

            oDataSet.Relations.Add("PCRContact", oDataSet.Tables("PCR").Columns("PCR_ID"), _
                                                  oDataSet.Tables("PCRContact").Columns("PCR_ID"))

            oDataSet.Relations.Add("Comment", oDataSet.Tables("PCRContact").Columns("Contact_ID"), _
                                                  oDataSet.Tables("Comment").Columns("Contact_ID"))

            If oDataSet.Tables("PCR").Rows.Count = 1 Then
                Return LoadDataRow(oDataSet.Tables("PCR").Rows(0))
            Else
                Return False
            End If

        End Function

        Public Function LoadDataRow(ByRef oDataRow As DataRow) As Boolean

            miPCRID = oDataRow("PCR_ID")
            moPCRDate = Lists.Internal.Church.GetPCRDateByID(oDataRow("PCRDate_ID"))
            moULG = Lists.Internal.Church.GetULGByID(oDataRow("ULG_ID"))
            miULGChildren = oDataRow("ULGChildren")
            miULGVisitors = oDataRow("ULGVisitors")
            mdULGOffering = oDataRow("ULGOffering")
            msGeneralComments = oDataRow("GeneralComments")
            mbReportCompleted = oDataRow("ReportCompleted")
            mdReportCompletedDate = IsNull(oDataRow("ReportCompletedDate"), Nothing)
            mbOfferingReceived = IsNull(oDataRow("OfferingReceived"), False)

            Dim oContactRow As DataRow
            For Each oContactRow In oDataRow.GetChildRows("PCRContact")
                mcPCRContact.Add(New CPCRContact(oContactRow))
            Next

            For Each oRow As DataRow In oDataRow.GetChildRows("PCRVisitor")
                Dim oVisitor As New CPCRVisitor()
                oVisitor.Load(oRow)
                mcPCRVisitor.Add(oVisitor)
            Next

            mbChanged = False

            Return True

        End Function

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@PCRDate_ID", SQLObject(moPCRDate)))
                oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLObject(moULG)))
                oParameters.Add(New SqlClient.SqlParameter("@ULGChildren", miULGChildren))
                oParameters.Add(New SqlClient.SqlParameter("@ULGVisitors", miULGVisitors))
                oParameters.Add(New SqlClient.SqlParameter("@ULGOffering", mdULGOffering))
                oParameters.Add(New SqlClient.SqlParameter("@GeneralComments", msGeneralComments))
                oParameters.Add(New SqlClient.SqlParameter("@ReportCompleted", mbReportCompleted))
                oParameters.Add(New SqlClient.SqlParameter("@ReportCompletedDate", SQLDateTime(mdReportCompletedDate)))
                oParameters.Add(New SqlClient.SqlParameter("@OfferingReceived", mbOfferingReceived))

                If Me.ID = 0 Then
                    miPCRID = DataAccess.GetValue("sp_Church_PCRInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@PCR_ID", miPCRID))
                    DataAccess.ExecuteCommand("sp_Church_PCRUpdate", oParameters)
                End If

                mbChanged = False

            End If

            SavePCRContacts(oUser)
            SavePCRVisitors(oUser)
            mcPCRContact.ResetChanges()

        End Sub

        Private Sub SavePCRContacts(ByVal oUser As CUser)

            Dim oParameters As ArrayList

            'Remove the Deleted payments
            For Each oPCRContact As CPCRContact In mcPCRContact.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@PCRContact_ID", oPCRContact.ID))
                DataAccess.ExecuteCommand("sp_Church_PCRContactDelete", oParameters)
            Next

            For Each oPCRContact As CPCRContact In mcPCRContact
                oPCRContact.PCRID = miPCRID
                oPCRContact.Save(oUser)
            Next

        End Sub

        Private Sub SavePCRVisitors(ByVal oUser As CUser)

            Dim oParameters As ArrayList

            'Remove the Deleted payments
            For Each oPCRVisitor As CPCRVisitor In mcPCRVisitor.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@PCRVisitor_ID", oPCRVisitor.ID))
                DataAccess.ExecuteCommand("sp_Church_PCRVisitorDelete", oParameters)
            Next

            For Each oPCRVisitor As CPCRVisitor In mcPCRVisitor
                oPCRVisitor.PCRID = miPCRID
                oPCRVisitor.Save(oUser)
            Next

        End Sub

        Public ReadOnly Property PCRContact() As CArrayList(Of CPCRContact)
            Get
                Return mcPCRContact
            End Get
        End Property
        Public ReadOnly Property ID() As Integer
            Get
                Return miPCRID
            End Get
        End Property
        Public Property PCRDate() As CPCRDate
            Get
                Return moPCRDate
            End Get
            Set(ByVal value As CPCRDate)
                moPCRDate = value
            End Set
        End Property
        Public Property ULG() As CULG
            Get
                Return moULG
            End Get
            Set(ByVal value As CULG)
                moULG = value
            End Set
        End Property
        Public Property ULGChildren() As Integer
            Get
                Return miULGChildren
            End Get
            Set(ByVal value As Integer)
                If miULGChildren <> value Then mbChanged = True
                miULGChildren = value
            End Set
        End Property
        Public ReadOnly Property ULGVisitors() As CArrayList(Of CPCRVisitor)
            Get
                Return mcPCRVisitor
            End Get
        End Property
        Public Property ULGOffering() As Decimal
            Get
                Return mdULGOffering
            End Get
            Set(ByVal value As Decimal)
                If mdULGOffering <> value Then mbChanged = True
                mdULGOffering = value
            End Set
        End Property
        Public Property GeneralComments() As String
            Get
                Return msGeneralComments
            End Get
            Set(ByVal value As String)
                If msGeneralComments <> value Then mbChanged = True
                msGeneralComments = value
            End Set
        End Property
        Public Property ReportCompleted() As Boolean
            Get
                Return mbReportCompleted
            End Get
            Set(ByVal value As Boolean)
                If mbReportCompleted <> value Then mbChanged = True
                mbReportCompleted = value
            End Set
        End Property
        Public Property ReportCompletedDate() As Date
            Get
                Return mdReportCompletedDate
            End Get
            Set(ByVal value As Date)
                If mdReportCompletedDate <> value Then mbChanged = True
                mdReportCompletedDate = value
            End Set
        End Property
        Public Property OfferingReceived() As Boolean
            Get
                Return mbOfferingReceived
            End Get
            Set(ByVal value As Boolean)
                If mbOfferingReceived <> value Then mbChanged = True
                mbOfferingReceived = value
            End Set
        End Property
    End Class

End Namespace