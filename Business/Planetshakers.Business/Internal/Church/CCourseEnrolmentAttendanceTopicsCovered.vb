Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCourseEnrolmentAttendanceTopicsCovered

        Private miCourseEnrolmentAttendanceTopicsCoveredID As Integer
        Private miCourseEnrolmentAttendanceID As Integer
        Private miCourseTopicID As String

        Private mbChanged As Boolean

        Public Sub New()

            miCourseEnrolmentAttendanceTopicsCoveredID = 0
            miCourseEnrolmentAttendanceID = 0
            miCourseTopicID = 0

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCourseEnrolmentAttendanceTopicsCoveredID = oDataRow("CourseEnrolmentAttendanceTopicsCovered_ID")
            miCourseEnrolmentAttendanceID = oDataRow("CourseEnrolmentAttendance_ID")
            miCourseTopicID = oDataRow("CourseTopic_ID")
            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@CourseEnrolmentAttendance_ID", miCourseEnrolmentAttendanceID))
                oParameters.Add(New SqlClient.SqlParameter("@CourseTopic_ID", miCourseTopicID))

                If Me.ID = 0 Then
                    miCourseEnrolmentAttendanceTopicsCoveredID = DataAccess.GetValue("sp_Church_CourseEnrolmentAttendanceTopicsCoveredInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@CourseEnrolmentAttendanceTopicsCovered_ID", miCourseEnrolmentAttendanceTopicsCoveredID))
                    DataAccess.ExecuteCommand("sp_Church_CourseEnrolmentAttendanceTopicsCoveredUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miCourseEnrolmentAttendanceTopicsCoveredID
            End Get
        End Property
        Public Property CourseEnrolmentAttendanceID() As Integer
            Get
                Return miCourseEnrolmentAttendanceID
            End Get
            Set(ByVal value As Integer)
                If miCourseEnrolmentAttendanceID <> value Then mbChanged = True
                miCourseEnrolmentAttendanceID = value
            End Set
        End Property
        Public Property CourseTopicID() As String
            Get
                Return miCourseTopicID
            End Get
            Set(ByVal value As String)
                If miCourseTopicID <> value Then mbChanged = True
                miCourseTopicID = value
            End Set
        End Property
        Public ReadOnly Property CourseTopic() As CCourseTopic
            Get
                Return Lists.Internal.Church.GetCourseTopicByID(miCourseTopicID)
            End Get
        End Property

    End Class

End Namespace