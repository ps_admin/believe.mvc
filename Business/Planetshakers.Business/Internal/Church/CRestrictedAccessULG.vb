Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRestrictedAccessULG

        Private miRestrictedAccessULGID As Integer
        Private miULGID As Integer
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miRestrictedAccessULGID = 0
            miULGID = 0
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRestrictedAccessULGID = oDataRow("RestrictedAccessULG_ID")
            miULGID = oDataRow("ULG_ID")

            mbChanged = False

        End Sub

        Public Sub Save(ByVal iContactRoleID As Integer, ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContactRole_ID", iContactRoleID))
                oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLNull(miULGID)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miRestrictedAccessULGID = DataAccess.GetValue("sp_Church_RestrictedAccessULGInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@RestrictedAccessULG_ID", miRestrictedAccessULGID))
                    DataAccess.ExecuteCommand("sp_Church_RestrictedAccessULGUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miRestrictedAccessULGID
            End Get
        End Property
        Public Property ULGID() As Integer
            Get
                Return miULGID
            End Get
            Set(ByVal value As Integer)
                If miULGID <> value Then mbChanged = True
                miULGID = value
            End Set
        End Property
        Public ReadOnly Property ULG() As CULG
            Get
                Return Lists.Internal.Church.GetULGByID(miULGID)
            End Get
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub

        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace