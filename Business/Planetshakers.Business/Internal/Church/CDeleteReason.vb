Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CDeleteReason

        Private miDeleteReasonID As Integer
        Private msName As String
        Private mbCommentsRequired As Boolean
        Private miSortOrder As Integer

        Public Sub New()

            miDeleteReasonID = 0
            msName = String.Empty
            mbCommentsRequired = False
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miDeleteReasonID = oDataRow("DeleteReason_ID")
            msName = oDataRow("Name")
            mbCommentsRequired = oDataRow("CommentsRequired")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miDeleteReasonID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property CommentsRequired() As Boolean
            Get
                Return mbCommentsRequired
            End Get
            Set(ByVal value As Boolean)
                mbCommentsRequired = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace