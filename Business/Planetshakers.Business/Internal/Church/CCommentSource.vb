Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCommentSource

        Private miCommentSourceID As Integer
        Private msName As String
        Private mbShowInNPNC As Boolean

        Public Sub New()

            miCommentSourceID = 0
            msName = String.Empty
            mbShowInNPNC = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCommentSourceID = oDataRow("CommentSource_ID")
            msName = oDataRow("Name")
            mbShowInNPNC = oDataRow("ShowInNPNC")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miCommentSourceID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property ShowInNPNC() As Boolean
            Get
                Return mbShowInNPNC
            End Get
            Set(ByVal value As Boolean)
                mbShowInNPNC = value
            End Set
        End Property

    End Class

End Namespace