﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CRoomBookingOptions

    Private mdLimitedBookingsStart As Date
    Private mdLimitedBookingsEnd As Date

    Private mbChanged As Boolean

    Public Sub New()

        Dim oDatarow As DataRow = DataAccess.GetDataRow("SELECT * FROM Church_RoomBookingOptions")

        mdLimitedBookingsStart = oDatarow("LimitedBookingsStart")
        mdLimitedBookingsEnd = oDatarow("LimitedBookingsEnd")

        mbChanged = False

    End Sub

    Public Sub Save()

        If mbChanged Then

            DataAccess.ExecuteCommand("UPDATE Church_RoomBookingOptions SET LimitedBookingsStart = '" & SQLDate(mdLimitedBookingsStart) & "', LimitedBookingsEnd = '" & SQLDate(mdLimitedBookingsEnd) & "'")

        End If

    End Sub

    Public Property LimitedBookingsStart() As Date
        Get
            Return mdLimitedBookingsStart
        End Get
        Set(ByVal value As Date)
            If mdLimitedBookingsStart <> value Then mbChanged = True
            mdLimitedBookingsStart = value
        End Set
    End Property

    Public Property LimitedBookingsEnd() As Date
        Get
            Return mdLimitedBookingsEnd
        End Get
        Set(ByVal value As Date)
            If mdLimitedBookingsEnd <> value Then mbChanged = True
            mdLimitedBookingsEnd = value
        End Set
    End Property

End Class
