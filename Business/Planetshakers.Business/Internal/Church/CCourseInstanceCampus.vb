Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCourseInstanceCampus
        Implements IComparable(Of CCourseInstanceCampus)

        Private miCourseInstanceCampusID As Integer
        Private miCourseInstanceID As Integer
        Private miCampusID As String

        Private mbChanged As Boolean

        Public Sub New()

            miCourseInstanceCampusID = 0
            miCourseInstanceID = 0
            miCampusID = 0

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCourseInstanceCampusID = oDataRow("CourseInstanceCampus_ID")
            miCourseInstanceID = oDataRow("CourseInstance_ID")
            miCampusID = oDataRow("Campus_ID")
            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", miCourseInstanceID))
                oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", miCampusID))

                If Me.ID = 0 Then
                    miCourseInstanceCampusID = DataAccess.GetValue("sp_Church_CourseInstanceCampusInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@CourseInstanceCampus_ID", miCourseInstanceCampusID))
                    DataAccess.ExecuteCommand("sp_Church_CourseInstanceCampusUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miCourseInstanceCampusID
            End Get
        End Property
        Public Property CourseInstanceID() As Integer
            Get
                Return miCourseInstanceID
            End Get
            Set(ByVal value As Integer)
                If miCourseInstanceID <> value Then mbChanged = True
                miCourseInstanceID = value
            End Set
        End Property
        Public Property CampusID() As String
            Get
                Return miCampusID
            End Get
            Set(ByVal value As String)
                If miCampusID <> value Then mbChanged = True
                miCampusID = value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property

        Public Function CompareTo(ByVal other As CCourseInstanceCampus) As Integer Implements System.IComparable(Of CCourseInstanceCampus).CompareTo

            Return String.Compare(Me.Campus.Name, other.Campus.Name)

        End Function

    End Class

End Namespace