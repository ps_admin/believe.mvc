
<Serializable()> _
<CLSCompliant(True)> _
Public Class CPaymentType

    Private miPaymentTypeID As Integer
    Private msName As String

    Public Sub New()

        miPaymentTypeID = 0
        msName = String.Empty

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miPaymentTypeID = oDataRow("PaymentType_ID")
        msName = oDataRow("PaymentType")

    End Sub

    Public Property ID() As Integer
        Get
            Return miPaymentTypeID
        End Get
        Set(ByVal Value As Integer)
            miPaymentTypeID = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property

    Public Overrides Function toString() As String
        Return msName
    End Function

End Class