
<Serializable()> _
<CLSCompliant(True)> _
Public Class CCreditCardType

    Private miCreditCardTypeID As Integer
    Private msCreditCardType As String

    Public Sub New()

        miCreditCardTypeID = 0
        msCreditCardType = String.Empty

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miCreditCardTypeID = oDataRow("CreditCardType_ID")
        msCreditCardType = oDataRow("CreditCardType")

    End Sub

    Public Property ID() As Integer
        Get
            Return miCreditCardTypeID
        End Get
        Set(ByVal Value As Integer)
            miCreditCardTypeID = Value
        End Set
    End Property
    Public Property CreditCardType() As String
        Get
            Return msCreditCardType
        End Get
        Set(ByVal Value As String)
            msCreditCardType = Value
        End Set
    End Property

    Public Overrides Function toString() As String
        Return msCreditCardType
    End Function

End Class