
<CLSCompliant(True)> _
Public Class CEmail

    Private msToAddress As String
    Private msToName As String
    Private msEmailText As String
    Private msSubject As String
    Private msEmailHeading As String

    Public Sub New()
        msToAddress = ""
        msToName = ""
        msEmailText = ""
        msSubject = ""
    End Sub

    Public Function SendEmail(ByRef sError As String) As Boolean

        Return DataAccess.SendEmail(msToAddress, msToName, msSubject, msEmailText, New ArrayList, sError)

    End Function

    Public Property EmailText() As String
        Get
            Return msEmailText
        End Get
        Set(ByVal value As String)
            msEmailText = value
        End Set
    End Property
    Public Property Subject() As String
        Get
            Return msSubject
        End Get
        Set(ByVal value As String)
            msSubject = value
        End Set
    End Property
    Public Property ToAddress() As String
        Get
            Return msToAddress
        End Get
        Set(ByVal value As String)
            msToAddress = value
        End Set
    End Property
    Public Property ToName() As String
        Get
            Return msToName
        End Get
        Set(ByVal value As String)
            msToName = value
        End Set
    End Property

End Class