
<Serializable()> _
<CLSCompliant(True)> _
Public Class CContactDatabaseRole
    Implements IComparable(Of CContactDatabaseRole)

    Private miContactDatabaseRoleID As Integer
    Private miDatabaseRoleID As Integer
    Private mdDateAdded As Date
    Private msGUID As String

    Private mbChanged As Boolean

    Public Sub New()

        miContactDatabaseRoleID = 0
        miDatabaseRoleID = 0
        mdDateAdded = Nothing
        msGUID = System.Guid.NewGuid.ToString

        mbChanged = False
    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miContactDatabaseRoleID = oDataRow("ContactDatabaseRole_ID")
        miDatabaseRoleID = oDataRow("DatabaseRole_ID")
        mdDateAdded = oDataRow("DateAdded")

    End Sub

    Public Sub Save(ByVal iContactID As Integer, ByVal oUser As CUser)

        If mbChanged Then

            Dim oParameters As New ArrayList

            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))
            oParameters.Add(New SqlClient.SqlParameter("@DatabaseRole_ID", miDatabaseRoleID))
            oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

            If Me.ID = 0 Then
                miContactDatabaseRoleID = DataAccess.GetValue("sp_Common_ContactDatabaseRoleInsert", oParameters)
            Else
                oParameters.Add(New SqlClient.SqlParameter("@ContactDatabaseRole_ID", ID))
                DataAccess.ExecuteCommand("sp_Common_ContactDatabaseRoleUpdate", oParameters)
            End If

        End If

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miContactDatabaseRoleID
        End Get
    End Property
    Public Property DatabaseRoleID() As Integer
        Get
            Return miDatabaseRoleID
        End Get
        Set(ByVal value As Integer)
            If miDatabaseRoleID <> value Then mbChanged = True
            miDatabaseRoleID = value
        End Set
    End Property
    Public ReadOnly Property DatabaseRole() As CDatabaseRole
        Get
            If miDatabaseRoleID > 0 Then
                Return Lists.GetDatabaseRoleByID(miDatabaseRoleID)
            Else
                Return Nothing
            End If
        End Get
    End Property
    Public Property DateAdded() As Date
        Get
            Return mdDateAdded
        End Get
        Set(ByVal Value As Date)
            If mdDateAdded <> Value Then mbChanged = True
            mdDateAdded = Value
        End Set
    End Property
    Public ReadOnly Property GUID() As String
        Get
            Return msGUID
        End Get
    End Property
    Public ReadOnly Property Changed() As Boolean
        Get
            Return mbChanged
        End Get
    End Property

    Public Function CompareTo(ByVal other As CContactDatabaseRole) As Integer Implements System.IComparable(Of CContactDatabaseRole).CompareTo
        Return String.Compare(Me.DatabaseRole.Name, other.DatabaseRole.Name)
    End Function
End Class