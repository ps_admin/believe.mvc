
<Serializable()> _
<CLSCompliant(True)> _
Public Class CDatabaseRolePermission
    Inherits CPermission

    Private miDatabaseRolePermissionID As Integer
    Private mbViewState As Boolean
    Private mbViewCountry As Boolean

    Public Sub New()
        MyBase.New()

        miDatabaseRolePermissionID = 0
        mbViewState = False
        mbViewCountry = False

        mbChanged = False

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miDatabaseRolePermissionID = oDataRow("DatabaseRolePermission_ID")
        mbViewState = oDataRow("ViewState")
        mbViewCountry = oDataRow("ViewCountry")

        MyBase.miDatabaseObjectID = oDataRow("DatabaseObject_ID")
        MyBase.mbRead = oDataRow("Read")
        MyBase.mbWrite = oDataRow("Write")
        MyBase.mbChanged = False

    End Sub

    Public Sub Save(ByVal iDatabaseRole As Integer)

        If mbChanged Then

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@DatabaseRole_ID", iDatabaseRole))
            oParameters.Add(New SqlClient.SqlParameter("@DatabaseObject_ID", MyBase.miDatabaseObjectID))
            oParameters.Add(New SqlClient.SqlParameter("@Read", MyBase.mbRead))
            oParameters.Add(New SqlClient.SqlParameter("@Write", MyBase.mbWrite))
            oParameters.Add(New SqlClient.SqlParameter("@ViewState", mbViewState))
            oParameters.Add(New SqlClient.SqlParameter("@ViewCountry", mbViewCountry))

            If ID = 0 Then
                miDatabaseRolePermissionID = DataAccess.GetValue("sp_Common_DatabaseRolePermissionInsert", oParameters)
            Else
                oParameters.Add(New SqlClient.SqlParameter("@DatabaseRolePermission_ID", ID))
                DataAccess.ExecuteCommand("sp_Common_DatabaseRolePermissionUpdate", oParameters)
            End If

            MyBase.mbChanged = False

        End If

    End Sub


    Public ReadOnly Property ID() As Integer
        Get
            Return miDatabaseRolePermissionID
        End Get
    End Property
    Public Property ViewState() As Boolean
        Get
            Return mbViewState
        End Get
        Set(ByVal Value As Boolean)
            If mbViewState <> Value Then MyBase.mbChanged = True
            mbViewState = Value
        End Set
    End Property
    Public Property ViewCountry() As Boolean
        Get
            Return mbViewCountry
        End Get
        Set(ByVal Value As Boolean)
            If mbViewCountry <> Value Then MyBase.mbChanged = True
            mbViewCountry = Value
        End Set
    End Property
    Public ReadOnly Property SortOrder() As Integer
        Get
            If DatabaseObject IsNot Nothing Then
                Return DatabaseObject.SortOrder
            Else
                Return 0
            End If
        End Get
    End Property
End Class