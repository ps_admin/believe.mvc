
<Serializable()> _
<CLSCompliant(True)> _
Public Class CDenomination

    Private miDenominationID As Integer
    Private msName As String

    Public Sub New()

        miDenominationID = 0
        msName = String.Empty

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miDenominationID = odatarow("Denomination_ID")
        msName = odatarow("Denomination")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miDenominationID
        End Get
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property

    Public Overrides Function toString() As String
        Return msName
    End Function

End Class