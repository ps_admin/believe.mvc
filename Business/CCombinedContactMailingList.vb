﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CCombinedContactMailingList
#Region "Private Fields"
        Private miID As Integer
        Private miContact_ID As Integer
        Private miMailingList_ID As Integer
        Private miMailingListType_ID As Integer
        Private mbSubscriptionActive As Boolean
        Private mbChanged As Boolean
#End Region
#Region "Constructors"
        Public Sub New()
            miID = 0
            miContact_ID = 0
            miMailingList_ID = 0
            miMailingListType_ID = 0
            mbSubscriptionActive = False
            mbChanged = False
        End Sub

        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()

            miID = oDataRow("ID")
            miContact_ID = oDataRow("Contact_ID")
            miMailingList_ID = oDataRow("MailingList_ID")
            miMailingListType_ID = oDataRow("MailingListType_ID")
            mbSubscriptionActive = oDataRow("SubscriptionActive")
            mbChanged = False

        End Sub
#End Region
#Region "Other Methods"
        Public Sub Save(ByRef oUser As CUser)
            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContact_ID))
                oParameters.Add(New SqlClient.SqlParameter("@MailingList_ID", miMailingList_ID))
                oParameters.Add(New SqlClient.SqlParameter("@MailingListType_ID", miMailingListType_ID))
                oParameters.Add(New SqlClient.SqlParameter("@SubscriptionActive", mbSubscriptionActive))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miID = DataAccess.GetValue("sp_Common_CombinedContactMailingListInsert", oParameters)
                Else
                oParameters.Add(New SqlClient.SqlParameter("@ID", miID))
                    DataAccess.ExecuteCommand("sp_Common_CombinedContactMailingListUpdate", oParameters)
                End If

                mbChanged = False

            End If
        End Sub
#End Region
#Region "Public Properties"
        Public ReadOnly Property ID() As Integer
            Get
                Return miID
            End Get
        End Property
        Public Property Contact_ID() As Integer
            Get
                Return miContact_ID
            End Get
            Set(ByVal value As Integer)
                If miContact_ID <> value Then mbChanged = True
                miContact_ID = value
            End Set
        End Property
        Public Property MailingList_ID() As Integer
            Get
                Return miMailingList_ID
            End Get
            Set(ByVal value As Integer)
                If miMailingList_ID <> value Then mbChanged = True
                miMailingList_ID = value
            End Set
        End Property
        Public Property MailingListType_ID() As Integer
            Get
                Return miMailingListType_ID
            End Get
            Set(ByVal value As Integer)
                If miMailingListType_ID <> value Then mbChanged = True
                miMailingListType_ID = value
            End Set
        End Property
        Public Property SubscriptionActive() As Boolean
            Get
                Return mbSubscriptionActive
            End Get
            Set(ByVal value As Boolean)
                If mbSubscriptionActive <> value Then mbChanged = True
                mbSubscriptionActive = value
            End Set
        End Property
#End Region
    End Class