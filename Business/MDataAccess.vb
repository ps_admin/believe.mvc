Public Module MDataAccess

    Dim moDataAccess As CDataAccess

    Public Property ConnectionString() As String
        Get
            Return DataAccess.ConnectionString
        End Get
        Set(ByVal sConnectionString As String)
            DataAccess.ConnectionString = sConnectionString
        End Set
    End Property

    Public ReadOnly Property DataAccess() As CDataAccess
        Get

            If moDataAccess Is Nothing Then
                If System.Web.HttpContext.Current IsNot Nothing Or My.Computer.Name.ToLower = "ps-rs01" Or My.Computer.Name.ToLower = "PS-WDS-XXX" Or My.Computer.Name.ToLower = "ps-harriss" _
                                        Or (My.Application.Info.AssemblyName = "AutoSendEmails") Then '  Added this code to set connection when called from AutoSendMails Then '  Then
                    moDataAccess = New CDataAccessDirect
                Else
                    moDataAccess = New CDataAccessWeb
                End If
            End If

            Return moDataAccess

        End Get
    End Property

End Module
