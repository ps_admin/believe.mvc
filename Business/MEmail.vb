Imports System.Text

Public Module MEmail

    Public Function PlanetshakersAccountCreation(ByRef oContact As CContact, ByRef sError As String) As Boolean

        Dim sEmail As New StringBuilder
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("Dear " & oContact.FirstName & ", <br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("Thank you so much for registering with Planetshakers.<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("You can now sign up for any of these exciting opportunities!<br />")
        sEmail.AppendLine("<ul><li>Planetshakers Conference 2010<br />")
        sEmail.AppendLine("<li>Planetshakers Revolution<br />")
        sEmail.AppendLine("<li>Beautiful Woman 2009 and 2010<br />")
        sEmail.AppendLine("<li>Planetshakers Internship February 2010<br />")
        sEmail.AppendLine("</ul><br />")
        sEmail.AppendLine("Also, if you are a member of Planetshakers City Church, in the future you�ll be able to sign up online for these incredible events!<br />")
        sEmail.AppendLine("<ul><li>DNA<br />")
        sEmail.AppendLine("<li>Inner Champion<br />")
        sEmail.AppendLine("<li>Kingdom Purpose<br />")
        sEmail.AppendLine("<li>Kingdom Life<br />")
        sEmail.AppendLine("<li>PlanetUNI Events<br />")
        sEmail.AppendLine("<li>Mission Trips<br />")
        sEmail.AppendLine("</ul><br />")
        sEmail.AppendLine("Remember, your sign-in details to access all of this at planetshakers.com is:<br />")
        sEmail.AppendLine("<strong>Email:</strong> " & oContact.Email & "<br />")
        sEmail.AppendLine("<strong>Password:</strong> " & oContact.Password & "<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("God Bless,<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("The Planetshakers Team<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("Planetshakers Ministries International Inc<br />")
        sEmail.AppendLine("ABN: 79 480 989 066<br />")
        sEmail.AppendLine("Post: PO Box 5171, South Melbourne Vic 3205<br />")
        sEmail.AppendLine("P: 1300 88 33 21 / +61 3 9896 7999<br />")
        sEmail.AppendLine("F: +61 3 9830 7683<br />")
        sEmail.AppendLine("W: www.planetshakers.com<br />")

        Dim oEmail As New CEmail
        oEmail.EmailText = EmailHeader + sEmail.ToString + EmailFooter
        oEmail.Subject = "Your new Planetshakers Account"
        oEmail.ToAddress = oContact.Email
        oEmail.ToName = oContact.Name

        Return oEmail.SendEmail(sError)

    End Function

    Public Function ForgetPassword(ByVal oContact As CContact) As Boolean

        Dim sError As String = String.Empty
        Dim sEmail As New StringBuilder

        sEmail.AppendLine("<br />")
        sEmail.AppendLine("Dear " & oContact.FirstName & ", <br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("This email has been sent automatically by the Planetshakers Webpage in response")
        sEmail.AppendLine("to your request to reset your password.<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("To reset your password and access your Planetshakers Account, please click the link below.<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("If nothing happens, please copy and paste the link into the address bar of your internet browser.<br />")
        sEmail.AppendLine("This link will take you to the change password page.<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("<a href=""https://accounts.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & """>https://accounts.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & "</a><br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("God Bless,<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("The Planetshakers Team<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("Planetshakers Ministries International Inc<br />")
        sEmail.AppendLine("ABN: 79 480 989 066<br />")
        sEmail.AppendLine("Post: PO Box 5171, South Melbourne Vic 3205<br />")
        sEmail.AppendLine("P: 1300 88 33 21 / +61 3 9896 7999<br />")
        sEmail.AppendLine("F: +61 3 9830 7683<br />")
        sEmail.AppendLine("W: www.planetshakers.com<br />")

        Dim oEmail As New CEmail
        oEmail.EmailText = EmailHeader + sEmail.ToString + EmailFooter
        oEmail.Subject = "Planetshakers Account Password Reset"
        oEmail.ToAddress = oContact.Email
        oEmail.ToName = oContact.Name

        Return oEmail.SendEmail(sError)

    End Function

    Public Function SetAccountPassword(ByVal oContact As CContact) As Boolean

        Dim sError As String = String.Empty
        Dim sEmail As New StringBuilder

        sEmail.AppendLine("<br />")
        sEmail.AppendLine("Dear " & oContact.FirstName & ", <br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("This email has been sent automatically by the Planetshakers Webpage in response")
        sEmail.AppendLine("to your request to set your account password.<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("To set your password and access your Planetshakers Account, please click the link below.<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("If nothing happens, please copy and paste the link into the address bar of your internet browser.<br />")
        sEmail.AppendLine("This link will take you to the change password page.<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("<a href=""https://accounts.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & """>https://accounts.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & "</a><br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("God Bless,<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("The Planetshakers Team<br />")
        sEmail.AppendLine("<br />")
        sEmail.AppendLine("Planetshakers Ministries International Inc<br />")
        sEmail.AppendLine("ABN: 79 480 989 066<br />")
        sEmail.AppendLine("Post: PO Box 5171, South Melbourne Vic 3205<br />")
        sEmail.AppendLine("P: 1300 88 33 21 / +61 3 9896 7999<br />")
        sEmail.AppendLine("F: +61 3 9830 7683<br />")
        sEmail.AppendLine("W: www.planetshakers.com<br />")

        Dim oEmail As New CEmail
        oEmail.EmailText = EmailHeader + sEmail.ToString + EmailFooter
        oEmail.Subject = "Planetshakers Account Password"
        oEmail.ToAddress = oContact.Email
        oEmail.ToName = oContact.Name

        Return oEmail.SendEmail(sError)

    End Function

    Private ReadOnly Property EmailHeader() As String
        Get

            Dim sHeader As New StringBuilder

            sHeader.AppendLine("<html>")
            sHeader.AppendLine("<head></head>")
            sHeader.AppendLine("<body style=""font-family: 'Trebuchet MS', Arial"">")
            sHeader.AppendLine("<table cellspacing=""0"" cellpadding=""3"" style=""width: 600px"">")
            sHeader.AppendLine("<tr><td>")

            Return sHeader.ToString()

        End Get
    End Property

    Private ReadOnly Property EmailFooter() As String
        Get

            Dim sHeader As New StringBuilder

            sHeader.AppendLine("</td></tr>")
            sHeader.AppendLine("</table>")
            sHeader.AppendLine("</body>")
            sHeader.AppendLine("</html>")

            Return sHeader.ToString()

        End Get
    End Property

End Module