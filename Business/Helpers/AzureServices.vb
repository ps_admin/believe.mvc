﻿Imports Azure.Identity
Imports Azure.Security.KeyVault.Secrets
Imports System.Threading.Tasks

Namespace Planetshakers.Business.Helpers
    Public Module AzureServices
        Private ReadOnly _tenantId As String = "944da9c5-9dc7-4d02-9c8e-a630c656fc2d"
        Private ReadOnly _applicationId As String = "bd51ad0e-1c94-49f4-9f5e-864e3e9eeedd"
        Private ReadOnly _clientSecret As String = "pOc8Q~auLfB8bgoWvg3pxra~LAxz7dEeSoQWIc3N"

        Public Async Function KeyVault_RetreiveSecretAsync(keyVaultName As String, secretName As String) As Task(Of String)
            Dim credentials As New ClientSecretCredential(_tenantId, _applicationId, _clientSecret)
            Dim secretClient As New SecretClient(New Uri($"https://{keyVaultName}.vault.azure.net/"), credentials)

            Try
                Dim secret = Await secretClient.GetSecretAsync(secretName)
                Return secret.Value.Value.ToString()
            Catch ex As Exception
                Console.WriteLine($"Failed to retrieve secret: {ex.Message}")
                Return ex.Message.ToString()
            End Try
        End Function

        Public Function KeyVault_RetreiveSecret(keyVaultName As String, secretName As String) As String
            Dim credentials As New ClientSecretCredential(_tenantId, _applicationId, _clientSecret)
            Dim secretClient As New SecretClient(New Uri($"https://{keyVaultName}.vault.azure.net/"), credentials)

            Try
                Dim secret = secretClient.GetSecret(secretName)
                Return secret.Value.Value.ToString()
            Catch ex As Exception
                Console.WriteLine($"Failed to retrieve secret: {ex.Message}")
                Return ex.Message.ToString()
            End Try
        End Function
    End Module
End Namespace
