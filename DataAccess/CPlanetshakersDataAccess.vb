Imports System.Data.SqlClient

Public Class CPlanetshakersDataAccess
    Implements IDisposable

    Private mbInTransaction As Boolean                   'Are we currently in a transaction?
    Private moSQLConnectionTransaction As SqlConnection  'Connection we will hold for transaction object
    Private moSQLTransaction As SqlTransaction           'Transaction Object

    Private mbDebug As Boolean = False
    Private miTimeOut As Integer = 0
    Private miTimeOutDefault As Integer = 0

    Private msConnectionString As String
    Private miUserID As Integer

    Public Function GetDataSet(ByVal sSQL As String, ByVal oTableMappings As ArrayList) As DataSet

        Dim oSQLAdapter As New SqlClient.SqlDataAdapter
        Dim oSQLCommand As New SqlClient.SqlCommand
        Dim oDataSet As New DataSet
        Dim oTableMapping As Common.DataTableMapping

        Try
            Dim lStart As Long = Now.Ticks

            If mbInTransaction Then
                oSQLCommand.Connection = moSQLConnectionTransaction
                oSQLCommand.Transaction = moSQLTransaction
            Else
                oSQLCommand.Connection = GetConnection()
            End If

            oSQLCommand.CommandTimeout = miTimeOut : miTimeOut = miTimeOutDefault
            oSQLCommand.CommandText = sSQL
            oSQLAdapter.SelectCommand = oSQLCommand
            For Each oTableMapping In oTableMappings
                oSQLAdapter.TableMappings.Add(oTableMapping)
            Next
            oSQLAdapter.Fill(oDataSet)


            If mbDebug Then Console.WriteLine(sSQL & " - Duration: " & Format((Now.Ticks - lStart) / 10000000, "#0.000"))

            Return oDataSet

        Catch ex As Threading.ThreadAbortException
            'If thread is aborted, just ignore it
        Catch ex As Exception
            Throw New Exception(ex.Message & vbCrLf & "SQL Query: " & sSQL, ex.InnerException)
        Finally
            If Not mbInTransaction Then
                CloseConnection(oSQLCommand)
            End If

        End Try

        Return Nothing

    End Function

    Public Function GetDataSet(ByVal sSQL As String, ByVal oTableMappings As ArrayList, ByVal oParameters As ArrayList) As DataSet

        Dim oSQLAdapter As New SqlClient.SqlDataAdapter
        Dim oSQLCommand As New SqlClient.SqlCommand
        Dim oDataSet As New DataSet
        Dim oTableMapping As Common.DataTableMapping

        Try
            Dim lStart As Long = Now.Ticks

            If mbInTransaction Then
                oSQLCommand.Connection = moSQLConnectionTransaction
                oSQLCommand.Transaction = moSQLTransaction
            Else
                oSQLCommand.Connection = GetConnection()
            End If

            Dim oSQLParameter As SqlParameter
            For Each oSQLParameter In oParameters
                oSQLCommand.Parameters.Add(oSQLParameter)
            Next
            For Each oTableMapping In oTableMappings
                oSQLAdapter.TableMappings.Add(oTableMapping)
            Next
            oSQLCommand.CommandTimeout = miTimeOut : miTimeOut = miTimeOutDefault
            oSQLCommand.CommandText = sSQL
            oSQLCommand.CommandType = CommandType.StoredProcedure
            oSQLAdapter.SelectCommand = oSQLCommand

            oSQLAdapter.Fill(oDataSet)


            If mbDebug Then Console.WriteLine(sSQL & " - Duration: " & Format((Now.Ticks - lStart) / 10000000, "#0.000"))

            Return oDataSet

        Catch ex As Threading.ThreadAbortException
            'If thread is aborted, just ignore it
        Catch ex As Exception
            Throw New Exception(ex.Message & vbCrLf & "SQL Query: " & sSQL & vbCrLf & " Parameters: " & GetParameters(oParameters), ex.InnerException)
        Finally
            If Not mbInTransaction Then
                CloseConnection(oSQLCommand)
            End If
        End Try

        Return Nothing

    End Function

    Public Function GetDataTable(ByVal sSQL As String) As DataTable

        Dim oDataTable As New DataTable
        Dim oSQLAdapter As New SqlClient.SqlDataAdapter
        Dim oSQLCommand As New SqlClient.SqlCommand

        Try
            Dim lStart As Long = Now.Ticks

            If mbInTransaction Then
                oSQLCommand.Connection = moSQLConnectionTransaction
                oSQLCommand.Transaction = moSQLTransaction
            Else
                oSQLCommand.Connection = GetConnection()
            End If

            oSQLCommand.CommandTimeout = miTimeOut : miTimeOut = miTimeOutDefault
            oSQLCommand.CommandText = sSQL
            oSQLAdapter.SelectCommand = oSQLCommand
            oSQLAdapter.Fill(oDataTable)

            If mbDebug Then Console.WriteLine(sSQL & " - Duration: " & Format((Now.Ticks - lStart) / 10000000, "#0.000"))

            Return oDataTable

        Catch ex As Threading.ThreadAbortException
            'If thread is aborted, just ignore it
        Catch ex As Exception
            Throw New Exception(ex.Message & vbCrLf & "SQL Query: " & sSQL & vbCrLf, ex.InnerException)
        Finally
            If Not mbInTransaction Then
                CloseConnection(oSQLCommand)
            End If
        End Try

        Return Nothing

    End Function

    Public Function GetDataTable(ByVal sSQL As String, ByRef oParameters As ArrayList) As DataTable

        Dim oDataTable As New DataTable
        Dim oSQLAdapter As New SqlClient.SqlDataAdapter
        Dim oSQLCommand As New SqlClient.SqlCommand

        Try
            Dim lStart As Long = Now.Ticks

            If mbInTransaction Then
                oSQLCommand.Connection = moSQLConnectionTransaction
                oSQLCommand.Transaction = moSQLTransaction
            Else
                oSQLCommand.Connection = GetConnection()
            End If

            Dim oSQLParameter As SqlParameter
            For Each oSQLParameter In oParameters
                oSQLCommand.Parameters.Add(oSQLParameter)
            Next
            oSQLCommand.CommandTimeout = miTimeOut : miTimeOut = miTimeOutDefault
            oSQLCommand.CommandText = sSQL
            oSQLCommand.CommandType = CommandType.StoredProcedure
            oSQLAdapter.SelectCommand = oSQLCommand

            oSQLAdapter.Fill(oDataTable)

            If mbDebug Then Console.WriteLine(sSQL & " - Duration: " & Format((Now.Ticks - lStart) / 10000000, "#0.000"))

            Return oDataTable

        Catch ex As Threading.ThreadAbortException
            'If thread is aborted, just ignore it
        Catch ex As Exception
            Throw New Exception(ex.Message & vbCrLf & "SQL Query: " & sSQL & vbCrLf & " Parameters: " & GetParameters(oParameters) & vbCrLf, ex.InnerException)
        Finally
            If Not mbInTransaction Then
                CloseConnection(oSQLCommand)
            End If
        End Try

        Return Nothing

    End Function

    Public Function GetDataRow(ByVal sSQL As String) As DataRow
        Dim oDataTable As DataTable = GetDataTable(sSQL)
        If oDataTable.Rows.Count > 0 Then
            Return oDataTable.Rows(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function GetValue(ByVal sSQL As String) As Object

        Dim oSQLCommand As New SqlClient.SqlCommand

        Try
            Dim lStart As Long = Now.Ticks

            If mbInTransaction Then
                oSQLCommand.Connection = moSQLConnectionTransaction
                oSQLCommand.Transaction = moSQLTransaction
            Else
                oSQLCommand.Connection = GetConnection()
            End If

            oSQLCommand.CommandTimeout = miTimeOut : miTimeOut = miTimeOutDefault
            oSQLCommand.CommandText = sSQL

            Return oSQLCommand.ExecuteScalar

            If mbDebug Then Console.WriteLine(sSQL & " - Duration: " & Format((Now.Ticks - lStart) / 10000000, "#0.000"))

        Catch ex As Threading.ThreadAbortException
            'If thread is aborted, just ignore it
        Catch ex As Exception
            Throw New Exception(ex.Message & vbCrLf & "SQL Query: " & sSQL, ex.InnerException)
        Finally
            If Not mbInTransaction Then
                CloseConnection(oSQLCommand)
            End If
        End Try

        Return Nothing

    End Function

    Public Function GetValue(ByVal sSQL As String, ByVal oParameters As ArrayList) As Object

        Dim oSQLCommand As New SqlClient.SqlCommand

        Try
            Dim lStart As Long = Now.Ticks

            If mbInTransaction Then
                oSQLCommand.Connection = moSQLConnectionTransaction
                oSQLCommand.Transaction = moSQLTransaction
            Else
                oSQLCommand.Connection = GetConnection()
            End If

            Dim oSQLParameter As SqlParameter
            For Each oSQLParameter In oParameters
                oSQLCommand.Parameters.Add(oSQLParameter)
            Next
            oSQLCommand.CommandTimeout = miTimeOut : miTimeOut = miTimeOutDefault
            oSQLCommand.CommandText = sSQL
            oSQLCommand.CommandType = CommandType.StoredProcedure

            Return oSQLCommand.ExecuteScalar

            If mbDebug Then Console.WriteLine(sSQL & " - Duration: " & Format((Now.Ticks - lStart) / 10000000, "#0.000"))

        Catch ex As Threading.ThreadAbortException
            'If thread is aborted, just ignore it
        Catch ex As Exception
            Throw New Exception(ex.Message & vbCrLf & "SQL Query: " & sSQL & vbCrLf & " Parameters: " & GetParameters(oParameters), ex.InnerException)
        Finally
            If Not mbInTransaction Then
                CloseConnection(oSQLCommand)
            End If
        End Try

        Return Nothing

    End Function

    Public Sub ExecuteCommand(ByVal sSQL As String)

        Dim oSQLCommand As New SqlClient.SqlCommand

        Try
            Dim lStart As Long = Now.Ticks

            If mbInTransaction Then
                oSQLCommand.Connection = moSQLConnectionTransaction
                oSQLCommand.Transaction = moSQLTransaction
            Else
                oSQLCommand.Connection = GetConnection()
            End If

            oSQLCommand.CommandTimeout = miTimeOut : miTimeOut = miTimeOutDefault
            oSQLCommand.CommandText = sSQL
            oSQLCommand.ExecuteNonQuery()

            If mbDebug Then Console.WriteLine(sSQL & " - Duration: " & Format((Now.Ticks - lStart) / 10000000, "#0.000"))

        Catch ex As Threading.ThreadAbortException
            'If thread is aborted, just ignore it
        Catch ex As Exception
            Throw New Exception(ex.Message & vbCrLf & "SQL Query: " & sSQL, ex.InnerException)
        Finally
            If Not mbInTransaction Then
                CloseConnection(oSQLCommand)
            End If
        End Try

    End Sub

    Public Sub ExecuteCommand(ByVal sSQL As String, ByRef oParameters As ArrayList)

        Dim oSQLCommand As New SqlClient.SqlCommand

        Try
            Dim lStart As Long = Now.Ticks

            If mbInTransaction Then
                oSQLCommand.Connection = moSQLConnectionTransaction
                oSQLCommand.Transaction = moSQLTransaction
            Else
                oSQLCommand.Connection = GetConnection()
            End If

            Dim oSQLParameter As SqlParameter
            For Each oSQLParameter In oParameters
                oSQLCommand.Parameters.Add(oSQLParameter)
            Next
            oSQLCommand.CommandTimeout = miTimeOut : miTimeOut = miTimeOutDefault
            oSQLCommand.CommandText = sSQL
            oSQLCommand.CommandType = CommandType.StoredProcedure
            oSQLCommand.ExecuteNonQuery()

            If mbDebug Then Console.WriteLine(sSQL & " - Duration: " & Format((Now.Ticks - lStart) / 10000000, "#0.000"))

        Catch ex As Threading.ThreadAbortException
            'If thread is aborted, just ignore it
        Catch ex As Exception
            Throw New Exception(ex.Message & vbCrLf & "SQL Query: " & sSQL & vbCrLf & " Parameters: " & GetParameters(oParameters), ex.InnerException)
        Finally
            If Not mbInTransaction Then
                CloseConnection(oSQLCommand)
            End If
        End Try

    End Sub

    Public Sub BeginTransaction()
        moSQLConnectionTransaction = GetConnection()
        moSQLTransaction = moSQLConnectionTransaction.BeginTransaction()
        mbInTransaction = True
    End Sub
    Public Sub CommitTransaction()
        If mbInTransaction Then
            moSQLTransaction.Commit()
            moSQLConnectionTransaction.Close()
            moSQLTransaction = Nothing
            mbInTransaction = False
        End If
    End Sub
    Public Sub RollbackTransaction()
        If mbInTransaction Then
            moSQLTransaction.Rollback()
            moSQLConnectionTransaction.Close()
            moSQLTransaction = Nothing
            mbInTransaction = False
        End If
    End Sub
    Public ReadOnly Property InTransaction() As Boolean
        Get
            Return mbInTransaction
        End Get
    End Property

    Friend Function GetConnection() As SqlConnection

        If msConnectionString = "" Then
            Throw New Exception("Connection String not set!")
        End If

        Dim oSQLConnection As SqlConnection = New SqlConnection
        oSQLConnection.ConnectionString = msConnectionString
        oSQLConnection.Open()

        Return oSQLConnection

    End Function

    Private Sub CloseConnection(ByRef oSQLCommand As SqlClient.SqlCommand)

        If Not oSQLCommand Is Nothing Then
            If Not oSQLCommand.Connection Is Nothing Then
                oSQLCommand.Connection.Close()
                oSQLCommand.Connection = Nothing
            End If
            oSQLCommand = Nothing
        End If

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        RollbackTransaction()
    End Sub

    Public Property TimeOut() As Integer
        Get
            Return miTimeOut
        End Get
        Set(ByVal value As Integer)
            miTimeOut = value
        End Set
    End Property
    Public Property UserID() As Integer
        Get
            Return miUserID
        End Get
        Set(ByVal value As Integer)
            miUserID = value
        End Set
    End Property

    Public Property ConnectionString() As String
        Get
            Return msConnectionString
        End Get
        Set(ByVal value As String)
            msConnectionString = value
        End Set
    End Property

    Public Function GetParameters(ByVal oParameters As ArrayList) As String

        Dim oParameter As SqlParameter
        Dim sOutput As String = String.Empty

        For Each oParameter In oParameters
            If oParameter.DbType = DbType.Binary Then
                sOutput += oParameter.ParameterName & ": (binary); "
            Else
                sOutput += oParameter.ParameterName & ": " & oParameter.Value.ToString() & "; "
            End If
        Next

        Return sOutput

    End Function

    Sub Dispose() Implements IDisposable.Dispose
        Try
            moSQLConnectionTransaction.Dispose()
            moSQLTransaction.Dispose()
        Catch ex As Exception
        End Try
    End Sub
End Class