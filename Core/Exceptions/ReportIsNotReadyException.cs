﻿using System;

namespace Planetshakers.Core.Exceptions
{
    public class ReportIsNotReadyException : Exception
    {
        public ReportIsNotReadyException(String message, Exception ex) : base(message, ex)
        {
        }
    }
}
