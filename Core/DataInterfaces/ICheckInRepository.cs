﻿
using System.Collections.Generic;
using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface ICheckInRepository
    {
        Registration FindById(int id, int venue_id, int conference_id);
        void MarkById(int id);
        void InsertRegistration(int id, int venue_id, int registrationtype_id);

        void InsertAttendance(Attendance attendance);
        void InsertTopicCovered(TopicCovered t);

        List<Enrolment> CourseEnrolmentSearch(string query);
    }
}
