﻿using System;
using System.Collections.Generic;
using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface IAdministrationRepository
    {
        void insertWelcome(WelcomeCard welcomeCardEntry);
    }
}
