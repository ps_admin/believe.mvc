﻿namespace Planetshakers.Core.Models
{
    public class RoleItem 
    {
        public int Id { get; set; }
        public string Module { get; set; }
        public string Name { get; set; }     
    }
}