﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Core.Models
{
    public class Registration
    {
        public int ID { get; set; }
        public int ContactID { get; set; }
        public bool Attended { get; set; }
        public string GroupName { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public bool IsVIP { get; set; }
        public bool HasPBCB { get; set; }
        public bool HasPriority { get; set; }

        public string RegoType { get; set; }
        public int RegoOptionNameID { get; set; }
        public string RegoOptionText { get; set; }
        public string RegoOptionNameDesc { get; set; }

        public int VenueId { get; set; }
        public int ConferenceId { get; set; }
        public string ConferenceName { get; set; }

        public DateTime RegistrationDate { get; set; }
        public string CustomTag { get; set; }

        public int unallocated { get; set; }
        public double OutstandingBalance { get; set; }
    }

    public class OutstandingBalance
    {
        public int ContactID { get; set; }
        public string Type { get; set; }
        public string GroupName { get; set; }
        public double Balance { get; set; }
    }      

    public class EventModel
    {
        public int VenueID { get; set; }
        public string ConferenceName { get; set; }
    }
    
    public class UnallocatedRegistration
    {
        public int GroupLeader_ID { get; set; }
        public string GroupLeader { get; set; }

        public int Venue_ID { get; set; }
        public int Conference_ID { get; set; }

        public int RegistrationType_ID { get; set; }
        public string RegistrationType { get; set; }

        public int Unallocated { get; set; }
        public int Allocated { get; set; }
    }

    public class Enrolment
    {
        public int ContactID { get; set; }
        public string Barcode { get; set; }
        public string Course { get; set; }

        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public int CourseInstance_ID { get; set; }
        public int CourseSession_ID { get; set; }

        public DateTime Date { get; set; }
        public Boolean Available { get; set; }
    }

    public class EnrolmentViewModel
    {
        public List<Enrolment> Enrolments { get; set; }
        public Enrolment SelectedEnrolment { get; set; }
        public string DisplayMode { get; set; }

        public List<Attendance> Attended { get; set; }
        public int SelectedCourseInstance_ID { get; set; }

        public string SelectedBarcode { get; set; }
    }

    public class Attendance
    {
        public int Contact_ID { get; set; }
        public int Session_ID { get; set; }
        public int MarkedOffBy_ID { get; set; }
        public DateTime MarkedOffDateTime { get; set; }
    }

    public class TopicCovered
    {
        public int Attendance_ID { get; set; }
        public int Topic_ID { get; set; }
    
    }

    public class TopicCoveredViewModel
    {
        public List<TopicCovered> topics { get; set; }
    }
}
