﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class EventRegistered
    {
        public int Venue_ID { get; set; }
        public int Conference_ID { get; set; }

        //public string EventName { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
