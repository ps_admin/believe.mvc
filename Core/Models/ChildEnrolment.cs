﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class ChildEnrolment
    {
        public int ChildEnrolment_ID { get; set; }
        public int Registration_ID { get; set; }
        public int Contact_ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get { return FirstName + " " + LastName; } }

        public DateTime DateOfBirth { get; set; }

        public int Age 
        { 
            get
            {
                int age = 0;
                age = DateTime.Now.Year - DateOfBirth.Year;

                if (DateTime.Now.DayOfYear < DateOfBirth.DayOfYear)
                    age -= 1;

                return age;
            } 
        }

        public int ChildSchoolGrade_ID { get; set; }

        public string SchoolGrade { get; set; }

        public string ParentName { get; set; }
        public string ParentEmail { get; set; }
        public string ParentMobile { get; set; }

        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }
        public string EmergencyContactRelationship { get; set; }

        public int SignedBy { get; set; }
        public DateTime DateSigned { get; set; }
    }
}
