﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models {
    public class ULMember 
    {
        public int ID { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Status { get; set; }
        public string Carers { get; set; }
        public DateTime LastCommentMade { get; set; }

    }
}
