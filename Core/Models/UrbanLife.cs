﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class UrbanLife
    {
        public int Id { get; set; }
        public string Code { get; set; }

        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Postcode { get; set; }
        public string Suburb { get; set; }

        public string Region { get; set; }
        public string Ministry { get; set; }
        public string Campus { get; set; }

        public string MeetingDay { get; set; }
        public string MeetingTime { get; set; }
    }
}
