﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    /// <summary>
    /// This is a model to capture the information whether a user 
    /// has access to any particular of urban life group.
    /// </summary>
    public class RestrictedAccessUrbanLife
    {
        public int RestrictedAccessULGId { get; set; }
        public int ContactRoleId { get; set; }
        public int ULGId { get; set; }
        public int ContactId { get; set; }
        public int RoleID { get; set; }
        public int CampusId { get; set; }
        public string DateAdded { get; set; }
    }
}
