﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Planetshakers.Core.Models
{
    public class BoomContactEntry
    {
        public int BoomContactEntry_ID { get; set; }
        public int Contact_ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int CheckInDateTransportOption_ID { get; set; }
        public DateTime CheckInTime { get; set; }
        public int CheckInBy { get; set; }

        public int CheckOutDateTransportOption_ID { get; set; }
        public DateTime CheckOutTime { get; set; }
        public int CheckOutBy { get; set; }

        public int ULG_ID { get; set; }

        public bool isCheckedIn { get; set; }
        public bool isCheckedOut { get; set; }

    }

    public class BoomNewPeopleForm
    {
        [Display(Name = "First Name *")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name *")]
        public string LastName { get; set; }

        [Display(Name = "Address *")]
        public string Address { get; set; }

        [Display(Name = "Suburb *")]
        public string Suburb { get; set; }

        [Display(Name = "Postcode")]
        public string Postcode { get; set; }

        [Display(Name = "Gender *")]
        public string Gender { get; set; }

        [Display(Name = "Mobile *")]
        public string Mobile { get; set; }

        [Display(Name = "Home Phone")]
        public string Phone { get; set; }
        
        [Display(Name = "Email *")]
        public string Email { get; set; }

        [Display(Name = "Facebook")]
        public string Facebook { get; set; }

        [Display(Name = "Date of Birth *")]
        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "School *")]
        public string School { get; set; }

        [Display(Name = "Tonight I came with... *")]
        public string CameWith { get; set; }

        [Display(Name = "They are from: *")]
        public string CameWithUL { get; set; }

        [Display(Name = "Campus")]
        public int Campus_ID { get; set; }

        public List<SelectListItem> Campus_List { get; set; }

        [Display(Name = "Came with bus?")]
        public bool CameOnBus { get; set; }

        [Display(Name = "New Christian?")]
        public bool IsNewChristian { get; set; }

        [Display (Name = "Name *")]
        public string EmergencyContact { get; set; }

        [Display (Name = "Contact Number *")]
        public string EmergencyContactNumber { get; set; }

        [Display(Name = "Comments")]
        public string Comments { get; set; }
    }


}
