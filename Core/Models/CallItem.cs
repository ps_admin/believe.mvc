﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class CallItem
    {
        public int Contact_ID;
        public string FirstName;
        public string LastName;
        public string Mobile;

        public int? CallStatus_ID;
        public string CurrentStatus;
        public string Comment;        
    }
}
