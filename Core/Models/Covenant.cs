﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class Covenant
    {
        public int Contact_Id { get; set; }

        public string Role { get; set; }

        public string FullName { get; set; }

        public Boolean CodeOfConduct { get; set; }

        public Boolean OHSPolicy { get; set; }

        public DateTime SignedDate { get; set; }
    }
}
