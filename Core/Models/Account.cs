﻿namespace Planetshakers.Core.Models
{
    public class Account
    {
        public int ContactId { get; set; }
        public string Barcode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool PasswordResetRequired { get; set; }
        public int PastorOfRegionId { get; set; }
    }
}
