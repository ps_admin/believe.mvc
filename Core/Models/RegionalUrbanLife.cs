﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    /// <summary>
    /// This is a model to capture the information whether a user 
    /// has access to any particular of urban life group which is belong
    /// to the region.
    /// </summary>
    public class RegionalUrbanLife
    {
        public int ContactId { get; set; }
        public int RegionId { get; set; }
        public int RoleId { get; set; }
    }
}
