﻿using System;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Planetshakers.Events.Mvc
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.Add(typeof(DateTime), new MyDateTimeModelBinder());

            Application.Set("RequireHttps", WebConfigurationManager.AppSettings.Get("RequireHttps"));

            //Added because business is not connecting to database.
            System.Configuration.ConnectionStringSettings connString;
            connString = WebConfigurationManager.ConnectionStrings["DefaultConnection"];
            Planetshakers.Business.MDataAccess.ConnectionString = connString.ToString();

        }

        protected void Application_BeginRequest()
        {
            if (((string)Application.Get("RequireHttps")).Equals("True"))
            {
                if (!Context.Request.IsSecureConnection)
                {
                    // This is an insecure connection, so redirect to the secure version
                    UriBuilder uri = new UriBuilder(Context.Request.Url);
                    uri.Scheme = "https";
                    uri.Port = 443;

                    Response.Redirect(uri.ToString());
                }
            }
        }
    }
}