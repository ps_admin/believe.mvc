﻿using System.Web;
using System.Web.Optimization;

namespace Planetshakers.Events.Mvc
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/planetshakers/events/register").Include(
                        "~/Content/themes/planetshakers/js/events.register.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/planetshakers/events/registration").Include(
                        "~/Content/themes/planetshakers/js/events.registrations.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/planetshakers/events/payment").Include(
                        "~/Content/themes/planetshakers/js/events.payment.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/planetshakers/events/confirmation").Include(
                        "~/Content/themes/planetshakers/js/events.confirmation.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/planetshakers/events/childenrolment").Include(
                        "~/Content/themes/planetshakers/js/events.childenrolment.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/planetshakers/accounts").Include(
                        "~/Content/themes/planetshakers/js/account.register.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/planetshakers/invitation/register").Include(
                        "~/Content/themes/planetshakers/js/invitation.register.min.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/development").Include(
                        "~/Scripts/holder.js"));

            //bundles.Add(new StyleBundle("~/bundles/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/bundles/Content/themes/planetshakers").Include(
                        "~/Content/themes/planetshakers/css/navigation-bar.css",
                        "~/Content/themes/planetshakers/css/planetshakers.min.css"));

            bundles.Add(new StyleBundle("~/bundles/Content/css/bootstrap").Include(
                        "~/Content/bootstrap/css/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/bundles/Content/css/fontawesome").Include(
                        "~/Content/bootstrap/vendor/fontawesome-free/css/all.min.css"));

            bundles.Add(new StyleBundle("~/bundles/Content/footable").Include(
                        "~/Content/footable/css/footable.core.min.css"));

            bundles.Add(new StyleBundle("~/bundles/Content/sb-admin-2").Include(
                       "~/Content/bootstrap/sb-admin-2.min.css"));

            bundles.Add(new StyleBundle("~/bundles/Content/datetimepicker").Include(
                        "~/Content/themes/datetimepicker/jquery.datetimepicker.css"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                        "~/Content/themes/datetimepicker/jquery.datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/planetshakers/events/Resetpassword").Include(
                        "~/Scripts/ResetPassword.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapJS").Include(
                        "~/Content/bootstrap/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/footableJS").Include(
                        "~/Content/footable/js/footable.js"));

            bundles.Add(new ScriptBundle("~/bundles/sb-admin-2JS").Include(
                        "~/Content/bootstrap/sb-admin-2.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/PopperJS").Include(
                        "~/Content/bootstrap/js/Popper.min.js"));
        }
    }
}