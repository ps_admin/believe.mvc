﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class SponsorshipConfirmModel
    {
        public string EventName { get; set; }
        public int Event_ID { get; set; }
        public System.Guid GUID { get; set; }
    }

}