﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class GoogleProfile
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public GImage Image { get; set; }
        public List<GEmail> Emails { get; set; }
        public string Gender { get; set; }
        public string ObjectType { get; set; }
        public string BirthDay { get; set; }
        public gFN Name { get; set; }
        // public string givenName { get; set; }
        public string sAddress1 { get; set; }
        public string sAddress2 { get; set; }
        public string sSuburb { get; set; }
        public string sPostCode { get; set; }
        public string sState { get; set; }
        public string sStateOther { get; set; }
        public string sCountry { get; set; }
        public string sMobile { get; set; }
        public string IsPlusUserCode { get; set; }
        public string accessCode { get; set; }
        public string verified { get; set; }
        public string Language { get; set; }
    }

    public class GEmail
    {
        public string Value { get; set; }
        public string Type { get; set; }
    }

    public class GImage
    {
        public string Url { get; set; }
    }

    public class gFN
    {
        public string familyName { get; set; }
        public string givenName { get; set; }

    }
}