﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public enum GroupSizeID
    {
        zero_to_ten = 1,
        eleven_to
    }

    public enum GroupCreditStatus {
        NonCredit = 1,
        AwaitingCreditApproval = 2,
        CreditApproved = 3,
        CreditDeclined = 4
    }

    public enum GroupType
    {
        MissionGroup = 1
    }

    public enum PaymentType
    {
        Cash = 1,
        Cheque = 2,
        CreditCard = 3,
        EFTPOS = 4,
        SPONSORHIP = 7
    }

    public enum PaymentSource
    {
        Deposit = 1,
        Applicant = 2,
        Sponsor = 3,
        CollegeFund = 4,
        GroupInvoice = 5
    }
}