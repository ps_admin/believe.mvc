//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Mvc.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Users
    {
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Nullable<int> State_ID { get; set; }
        public Nullable<int> Country_ID { get; set; }
        public string Password { get; set; }
        public Nullable<System.DateTime> LastPasswordChangeDate { get; set; }
        public int InvalidPasswordAttempts { get; set; }
        public bool PasswordResetRequired { get; set; }
        public bool Inactive { get; set; }
        public Nullable<int> Campus_ID { get; set; }
    }
}
