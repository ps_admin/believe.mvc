﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class ChildEnrolment
    {
        public int ChildEnrolment_ID { get; set; }
        public int Registration_ID { get; set; }
        public int Contact_ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get { return FirstName + " " + LastName; } }

        public string RoomAllocation { get; set; }
        
        public DateTime DateOfBirth { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd mmm yyyy}")]
        public String sDateOfBirth
        { 
            get 
            {
                return DateOfBirth.ToString();
            } 
        }

        public int Age
        {
            get
            {
                int age = 0;

                if (DateOfBirth != null)
                {
                    //var dob = (DateTime)DateOfBirth;
                    age = DateTime.Now.Year - DateOfBirth.Year;

                    if (DateTime.Now.DayOfYear < DateOfBirth.DayOfYear)
                        age -= 1;
                }

                return age;
            }
        }

        public int ChildSchoolGrade_ID { get; set; }
        public string SchoolGrade { get; set; }

        public string AdditionalInformation { get; set; }

        public string ParentName { get; set; }
        public string ParentEmail { get; set; }
        public string ParentMobile { get; set; }

        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }
        public string EmergencyContactRelationship { get; set; }

        public string EmergencyContactName2 { get; set; }
        public string EmergencyContactNumber2 { get; set; }
        public string EmergencyContactRelationship2 { get; set; }

        public string Comments { get; set; }

        public bool Verified { get; set; }
        public int? SignedBy { get; set; }
        public DateTime? DateSigned { get; set; }
    }

}