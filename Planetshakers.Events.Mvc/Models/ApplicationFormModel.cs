﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Mvc.Models
{
    public class ApplicationFormModel
    {
        public int Contact_ID { get; set; }
        public ContactQuestion ContactQuestion { get; set; }
        public List<Question> Questions { get; set; }
        public int Application_ID { get; set; }
        public int RegistrationType_ID { get; set; }
        public int Venue_ID { get; set; }
        public string ErrorMessage { get; set; }
        public string SuccessMessage { get; set; }
        public List<ReferralQuestion> Referrals { get; set; }
        public List<EmergencyContact> EmergencyContacts { get; set; }
        public bool saveAsDraft { get; set; }

        public List<SelectListItem> CampusList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Church_Campus
                                select new SelectListItem
                                {
                                    Text = c.Name,
                                    //Value = SqlFunctions.StringConvert((double)c.Campus_ID).Trim()
                                    Value = c.Name
                                }).ToList();

                    return list;
                }
            }
        }
        //public List<SelectListItem> ReferralType
        //{
        //    get
        //    {
        //        using (PlanetshakersEntities context = new PlanetshakersEntities())
        //        {
        //            var list = (from c in context.Volunteer_ApplicationReferralType
        //                        select new SelectListItem
        //                        {
        //                            Text = c.Name,
        //                            //Value = SqlFunctions.StringConvert((double)c.Campus_ID).Trim()
        //                            Value = c.ReferralType_ID.ToString()
        //                        }).ToList();

        //            return list;
        //        }
        //    }
        //}
        public List<SelectListItem> CountryList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Common_GeneralCountry
                                select new SelectListItem
                                {
                                    Text = c.Country,
                                    Value = c.Country_ID.ToString()
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> StateList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Common_GeneralState
                                where c.Country_ID == 11
                                select new SelectListItem
                                {
                                    Text = c.State,
                                    Value = c.State_ID.ToString()
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> GenderList
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem { Text = "Male", Value = "Male" });
                list.Add(new SelectListItem { Text = "Female", Value = "Female" });
                return list;
            }
        }
        
    }

    public class ReferralQuestion
    {
        public int ApplicationReferral_ID { get; set; }
        public string FullName { get; set; }
        public string ReferralType { get; set;}
        public string Relationship { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

    }

    public class ContactQuestion
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime DOB { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
        public int? Country_ID { get; set; }
        public int? State_ID { get; set; }
        public string Gender { get; set; }
    }

    public class EmergencyContact
    {
        //public int Number { get; set; }
        public int EmergencyContact_ID { get; set; }
        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string Relationship { get; set; }
        public string Comment { get; set; }
    }
}