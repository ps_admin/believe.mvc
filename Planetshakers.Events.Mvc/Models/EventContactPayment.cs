﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class EventContactPayment
    { 
        #region Constructors

        /// <summary>
        /// Constructor with default values
        /// </summary>
        public EventContactPayment()
        {
            contactPaymentID = null;
            contactID = null;
            conferenceID = null;
            paymentType = null;
            paymentAmount = null;
            ccNumber = null;
            ccExpiry = null;
            ccName = null;
            ccPhone = null;
            ccManual = null;
            ccTransactionRef = null;
            ccRefund = null;
            chequeDrawer = null;
            chequeBank = null;
            chequeBranch = null;
            paypalTransactionRef = null;
            comment = null;
            paymentDate = null;
            paymentByID = null;
            bankAccID = null;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="obj"></param>
        public EventContactPayment(Events_ContactPayment obj)
        {
            ParseObject(obj);
        }

        /// <summary>
        /// Initialise Group Payment Object
        /// </summary>
        /// <param name="group_payment_id">The primary identifier</param>
        public EventContactPayment(int contact_payment_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var temp = (from gp in context.Events_ContactPayment
                            where gp.ContactPayment_ID == contact_payment_id
                            select gp).FirstOrDefault();

                // Should NEVER be null
                if (temp != null)
                {
                    ParseObject(temp);
                }
                else
                {
                    throw new Exception("Group Payment with id = " + contact_payment_id + " not found");
                }
            }
        }

        #endregion
        #region Public Methods

        /// <summary>
        /// Create a new Credit Card Payment
        /// </summary>
        /// <param name="group_leader_id">Group Leader ID</param>
        /// <param name="conference_id">Conference ID</param>
        /// <param name="payment_amount">Total Amount</param>
        /// <param name="cc_number">Credit Card Number</param>
        /// <param name="cc_expiry">Credit Card Expiry</param>
        /// <param name="cc_name">Cardholder's Name</param>
        /// <param name="cc_phone">Cardholder's Phone Number</param>
        /// <param name="cc_transactionRef">Credit Card Transaction Reference</param>
        /// <param name="payment_by">Contact ID of the one making the payment</param>
        public void NewCCPayment(int contact_id, int conference_id,
                                 decimal payment_amount, string cc_number,
                                 string cc_expiry, string cc_name, string cc_phone,
                                 string cc_transactionRef, int payment_by,
                                 int bank_acc_id)
        {
            contactID = contact_id;
            conferenceID = conference_id;
            paymentType = PaymentType.CreditCard;
            paymentAmount = payment_amount;
            ccNumber = cc_number;
            ccExpiry = cc_expiry;
            ccName = cc_name;
            ccPhone = cc_phone;
            ccTransactionRef = cc_transactionRef;
            paymentDate = DateTime.Now;
            paymentByID = payment_by;
            bankAccID = bank_acc_id;
        }

        /// <summary>
        /// Create a new Cheque Payment
        /// </summary>
        /// <param name="group_leader_id">Group Leader ID</param>
        /// <param name="conference_id">Conference ID</param>
        /// <param name="payment_amount">Payment Amount</param>
        /// <param name="cheque_drawer">Cheque Drawer</param>
        /// <param name="cheque_branch">Cheque Branch</param>
        /// <param name="cheque_bank">Cheque Bank</param>
        /// <param name="payment_by">Contact ID of the one making the payment</param>
        public void NewChequePayment(int contact_id, int conference_id,
                                     decimal payment_amount, string cheque_drawer,
                                     string cheque_branch, string cheque_bank,
                                     int payment_by, int bank_acc_id)
        {
            contactID = contact_id;
            conferenceID = conference_id;
            paymentType = PaymentType.Cheque;
            paymentAmount = payment_amount;
            chequeDrawer = cheque_drawer;
            chequeBranch = cheque_branch;
            chequeBank = cheque_bank;
            paymentDate = DateTime.Now;
            paymentByID = payment_by;
            bankAccID = bank_acc_id;
        }

        /// <summary>
        /// Save changes
        /// </summary>
        public void Save()
        {
            try
            {
                Validate();
                if (_contactPaymentID <= 0) DbCreate();
                else DbUpdate();
            }
            catch (Exception ex)
            {
                throw ex;
                // Handle the exception, or throw it again
            }
        }

        #endregion
        #region Helpers

        /// <summary>
        /// Used to validate data before pushing it up to the database
        /// </summary>
        private void Validate()
        {
            if (_contactID == 0) throw new Exception("Contact ID is undefined");
            if (_conferenceID == 0) throw new Exception("Conference ID is undefined");
            if (_paymentTypeID == 0) throw new Exception("Payment Type is undefined");
            // TODO: more validation (if needed)
        }

        /// <summary>
        /// Parses data from an Events_GroupPayment object
        /// </summary>
        /// <param name="obj"></param>
        private void ParseObject(Events_ContactPayment obj)
        {
            // Initialise variables
            _contactPaymentID = obj.ContactPayment_ID;
            _contactID = obj.Contact_ID;
            _conferenceID = obj.Conference_ID;
            _paymentTypeID = obj.PaymentType_ID;
            _paymentAmount = obj.PaymentAmount;
            _ccNumber = obj.CCNumber;
            _ccExpiry = obj.CCExpiry;
            _ccName = obj.CCName;
            _ccPhone = obj.CCPhone;
            _ccManual = obj.CCManual;
            _ccTransactionRef = obj.CCTransactionRef;
            _ccRefund = obj.CCRefund;
            _chequeDrawer = obj.ChequeDrawer;
            _chequeBank = obj.ChequeBank;
            _chequeBranch = obj.ChequeBranch;
            _paypalTransactionRef = obj.PaypalTransactionRef;
            _comment = obj.Comment;
            _paymentDate = obj.PaymentDate;
            _paymentByID = obj.PaymentBy_ID;
            _bankAccID = obj.BankAccount_ID;
        }

        #endregion
        #region Database related

        /// <summary>
        /// Creates a new record on the database
        /// </summary>
        private void DbCreate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var new_payment = new Events_ContactPayment();

                // Initialise variables
                new_payment.Contact_ID = _contactID;
                new_payment.Conference_ID = _conferenceID;
                new_payment.PaymentType_ID = _paymentTypeID;
                new_payment.PaymentAmount = _paymentAmount;
                new_payment.CCNumber = _ccNumber;
                new_payment.CCExpiry = _ccExpiry;
                new_payment.CCName = _ccName;
                new_payment.CCPhone = _ccPhone;
                new_payment.CCManual = _ccManual;
                new_payment.CCTransactionRef = _ccTransactionRef;
                new_payment.CCRefund = _ccRefund;
                new_payment.ChequeDrawer = _chequeDrawer;
                new_payment.ChequeBank = _chequeBank;
                new_payment.ChequeBranch = _chequeBranch;
                new_payment.PaypalTransactionRef = _paypalTransactionRef;
                new_payment.Comment = _comment;
                new_payment.PaymentDate = _paymentDate;
                new_payment.PaymentBy_ID = _paymentByID;
                new_payment.BankAccount_ID = _bankAccID;

                context.Events_ContactPayment.Add(new_payment);
                context.SaveChanges();

                // Update id in the class.
                _contactPaymentID = new_payment.ContactPayment_ID;
            }
        }

        /// <summary>
        /// Updates an existing record on the database
        /// </summary>
        private void DbUpdate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var existing = (from gp in context.Events_ContactPayment
                                where gp.ContactPayment_ID == _contactPaymentID
                                select gp).FirstOrDefault();

                // Should NEVER be null
                if (existing != null)
                {
                    // Update Columns
                    existing.Contact_ID = _contactID;
                    existing.Conference_ID = _conferenceID;
                    existing.PaymentType_ID = _paymentTypeID;
                    existing.PaymentAmount = _paymentAmount;
                    existing.CCNumber = _ccNumber;
                    existing.CCExpiry = _ccExpiry;
                    existing.CCName = _ccName;
                    existing.CCPhone = _ccPhone;
                    existing.CCManual = _ccManual;
                    existing.CCTransactionRef = _ccTransactionRef;
                    existing.CCRefund = _ccRefund;
                    existing.ChequeDrawer = _chequeDrawer;
                    existing.ChequeBank = _chequeBank;
                    existing.ChequeBranch = _chequeBranch;
                    existing.PaypalTransactionRef = _paypalTransactionRef;
                    existing.Comment = _comment;
                    existing.PaymentDate = _paymentDate;
                    existing.PaymentBy_ID = _paymentByID;
                    existing.BankAccount_ID = _bankAccID;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Failed to update group payment with id = " + _contactPaymentID);
                }
            }
        }

        #endregion
        #region Variables and Get/Set Methods

        private int _contactPaymentID;
        public int? contactPaymentID
        {
            get
            {
                if (_contactPaymentID == 0) return null;
                else return _contactPaymentID;
            }
            private set
            {
                if (value == null) _contactPaymentID = 0;
                else _contactPaymentID = (int)value;
            }
        }

        private int _contactID;
        public int? contactID
        {
            get
            {
                if (_contactID == 0) return null;
                else return _contactID;
            }
            private set
            {
                if (value == null) _contactID = 0;
                else _contactID = (int)value;
            }
        }

        private int _conferenceID;
        public int? conferenceID
        {
            get
            {
                if (_conferenceID == 0) return null;
                else return _conferenceID;
            }
            private set
            {
                if (value == null) _conferenceID = 0;
                else _conferenceID = (int)value;
            }
        }

        private int _paymentTypeID;
        public PaymentType? paymentType
        {
            get
            {
                if (_paymentTypeID == 0) return null;
                else return (PaymentType)_paymentTypeID;
            }
            private set
            {
                if (value == null) _paymentTypeID = 0;
                else _paymentTypeID = (int)value;
            }
        }

        private decimal _paymentAmount;
        public decimal? paymentAmount
        {
            get
            {
                return _paymentAmount;
            }
            private set
            {
                if (value == null) _paymentAmount = 0;
                else _paymentAmount = (decimal)value;
            }
        }

        private string _ccNumber;
        public string ccNumber
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_ccNumber)) return null;
                else return _ccNumber;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _ccNumber = String.Empty;
                else
                {
                    // Remove any non numerical values
                    var temp = value;
                    temp.Replace("-", "").Replace(" ", "").Replace("/", "");

                    // Mask credit card number with Xs
                    _ccNumber = String.Concat(temp.Substring(0, 4),
                                              "".PadLeft(temp.Length - 8, 'X'),
                                              temp.Substring(temp.Length - 4));
                }
            }
        }

        private string _ccExpiry;
        public string ccExpiry
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_ccExpiry)) return null;
                else return _ccExpiry;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _ccExpiry = String.Empty;
                else _ccExpiry = value;
            }
        }

        private string _ccName;
        public string ccName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_ccName)) return null;
                else return _ccName;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _ccName = String.Empty;
                else _ccName = value;
            }
        }

        private string _ccPhone;
        public string ccPhone
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_ccPhone)) return null;
                else return _ccPhone;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _ccPhone = String.Empty;
                else _ccPhone = value;
            }
        }

        private bool _ccManual;
        public bool? ccManual
        {
            get
            {
                return _ccManual;
            }
            private set
            {
                if (value == null) _ccManual = false;
                else _ccManual = (bool)value;
            }
        }

        private string _ccTransactionRef;
        public string ccTransactionRef
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_ccTransactionRef)) return null;
                else return _ccTransactionRef;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _ccTransactionRef = String.Empty;
                else _ccTransactionRef = value;
            }
        }

        private bool _ccRefund;
        public bool? ccRefund
        {
            get
            {
                return _ccRefund;
            }
            private set
            {
                if (value == null) _ccRefund = false;
                else _ccRefund = (bool)value;
            }
        }

        private string _chequeDrawer;
        public string chequeDrawer
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_chequeDrawer)) return null;
                else return _chequeDrawer;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _chequeDrawer = String.Empty;
                else _chequeDrawer = value;
            }
        }

        private string _chequeBank;
        public string chequeBank
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_chequeBank)) return null;
                else return _chequeBank;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _chequeBank = String.Empty;
                else _chequeBank = value;
            }
        }

        private string _chequeBranch;
        public string chequeBranch
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_chequeBranch)) return null;
                else return _chequeBranch;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _chequeBranch = String.Empty;
                else _chequeBranch = value;
            }
        }

        private string _paypalTransactionRef;
        public string paypalTransactionRef
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_paypalTransactionRef)) return null;
                else return _paypalTransactionRef;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _paypalTransactionRef = String.Empty;
                else _paypalTransactionRef = value;
            }
        }

        private string _comment;
        public string comment
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_comment)) return null;
                else return _comment;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _comment = String.Empty;
                else _comment = value;
            }
        }

        private DateTime _paymentDate;
        public DateTime? paymentDate
        {
            get
            {
                if (_paymentDate == DateTime.MinValue) return null;
                else return _paymentDate;
            }
            private set
            {
                if (value == null) _paymentDate = DateTime.MinValue;
                else _paymentDate = (DateTime)value;
            }
        }

        private int _paymentByID;
        public int? paymentByID
        {
            get
            {
                if (_paymentByID == 0) return null;
                else return _paymentByID;
            }
            private set
            {
                if (value == null) _paymentByID = 0;
                else _paymentByID = (int)value;
            }
        }

        private int _bankAccID;
        public int? bankAccID
        {
            get
            {
                if (_bankAccID == 0) return null;
                else return _bankAccID;
            }
            private set
            {
                if (value == null) _bankAccID = 0;
                else _bankAccID = (int)value;
            }
        }

        #endregion
    }
}