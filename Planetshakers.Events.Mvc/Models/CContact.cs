﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class CContact
    {
        // contact id
        public int id { get; set; }

        // personal details
        public ContactPersonalDetails personal { get; set; }

        // church details
        public ContactChurchDetails church { get; set; }

        // emergency details
        public ContactEmergencyDetails emergency { get; set; }
    }
}