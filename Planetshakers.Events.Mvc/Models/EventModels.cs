﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using Planetshakers.Events.Mvc.Controllers;

namespace Planetshakers.Events.Mvc.Models
{
    public class EventIndexModel
    {
        public List<Event> Events { get; set; }
    }

    public class EventRegisterModel
    {
        public Event SelectedEvent { get; set; }

        public int EventId { get { return SelectedEvent.id; } }

        // A flag that determines if user has just purchased new registrations
        public bool recentlyRegistered { get; set; }

        public string showCurrentRego
        {
            get
            {
                if (recentlyRegistered) return "block";
                else return "none";
            }
        }

        // A flag that determines whether or not the user has any existing registrations
        public bool hasRegistrations
        {
            get {
                return CurrentSingleRegistrations.Any() ||
                       CurrentFriendRegistrations.Any() ||
                       CurrentFamilyRegistrations.Any() ||
                       CurrentGroupRegistrations.Any();
            }
                    
        }
        public List<CurrentRegistration> CurrentSingleRegistrations { get; set; }
        public List<CurrentRegistration> CurrentFriendRegistrations { get; set; }
        public List<CurrentRegistration> CurrentFamilyRegistrations { get; set; }
        public List<CurrentRegistration> CurrentGroupRegistrations { get; set; }
    }

}
