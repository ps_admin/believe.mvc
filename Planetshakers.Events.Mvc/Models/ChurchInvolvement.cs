﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class ChurchInvolvement
    {
        // church involvement id
        public int id { get; set; }

        // name
        public string name { get; set; }

        // whether or not it is active for this contact
        public bool is_selected { get; set; }
    }
}