﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Planetshakers.Events.Mvc.Controllers;

namespace Planetshakers.Events.Mvc.Models
{
    public class RegistrationConfirmationEmailModel
    {
        // Logged in contact
        public int ContactId { get; set; }

        // Name of contact
        public string FullName { get; set; }

        // Items in the cart
        public List<RegistrationConfirmationItem> Items { get; set; }

        // Event 
        public Event Event { get; set; }

        // Total cost
        public decimal GrandTotal { get { return Items.Sum(r => r.price); } }
    }


    public class PasswordResetEmailModel
    {
        // Contact's Email
        public string Email { get; set; }

        public string EncryptedToken { get; set; }

        public string FullName { get; set; }
    }
}