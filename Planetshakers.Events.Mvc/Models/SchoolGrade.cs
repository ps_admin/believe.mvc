﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class SchoolGrade
    {
        // country id
        public int id { get; set; }

        // name
        public string name { get; set; }
    }
}