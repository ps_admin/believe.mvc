﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Planetshakers.Events.Mvc.Controllers;

namespace Planetshakers.Events.Mvc.Models
{
    public class EventGroupInvitation
    {
        #region Constructors

        public EventGroupInvitation()
        {
            groupInvitationID = null;
            groupLeaderID = null;
            registrationTypeID = null;
            venueID = null;
            receiverEmail = null;
            expirationDate = null;
            accepted = null;
            acceptedBy = null;
            acceptedOn = null;
            identifier = null;
        }

        public EventGroupInvitation(int invitation_id)
        {
            using(PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var invitation = (from gi in context.Events_GroupInvitation
                                  where gi.GroupInvitation_ID == invitation_id
                                  select gi).FirstOrDefault();
                if (invitation != null)
                {
                    ParseObject(invitation);
                }
                else
                {
                    throw new Exception("Invitation not found.");
                }
            }
        }

        public EventGroupInvitation(Guid unique_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var invitation = (from gi in context.Events_GroupInvitation
                                  where gi.GUID == unique_id
                                  orderby gi.GroupInvitation_ID descending
                                  select gi).FirstOrDefault();

                if (invitation != null)
                {
                    ParseObject(invitation);
                }
                else
                {
                    throw new Exception("Invitation not found.");
                }

            }
        }

        public EventGroupInvitation(Events_GroupInvitation obj)
        { 
            ParseObject(obj); 
        }

        #endregion
        #region Public Methods

        public void NewInvitation(int group_leader_id, int registration_type_id, 
                                  int venue_id, string email)
        {
            groupLeaderID = group_leader_id;
            registrationTypeID = registration_type_id;
            venueID = venue_id;
            receiverEmail = email;
            expirationDate = DateTime.Now.AddDays(365);
        }

        public bool AcceptInvitation(int contact_id)
        {
            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Check if the contact is already registered for the event.
                    /* var registered = (from r in context.Events_Registration
                                      where r.Venue_ID == venueID &&
                                            r.Contact_ID == contact_id
                                      select r).ToList();
                    if (registered.Any())
                        throw new Exception("Contact is already registered for the event.");*/

                    // Make sure contacts have Common_ContactExternal and COmmon_ContactExternalEvents Objects
                    var contact_external = (from cce in context.Common_ContactExternal
                                            where cce.Contact_ID == contact_id
                                            select cce).ToList();

                    // if none, create
                    if (!contact_external.Any())
                    {
                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.Contact_ID = contact_id;
                        new_external_contact.ChurchAddress1 = "";
                        new_external_contact.ChurchAddress2 = "";
                        new_external_contact.ChurchCountry_ID = null;
                        new_external_contact.ChurchEmail = "";
                        new_external_contact.ChurchName = "";
                        new_external_contact.ChurchNumberOfPeople = "";
                        new_external_contact.ChurchPhone = "";
                        new_external_contact.ChurchPostcode = "";
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = "";
                        new_external_contact.ChurchSuburb = "";
                        new_external_contact.ChurchWebsite = "";
                        new_external_contact.Denomination_ID = 49; // 49 == N/A
                        new_external_contact.SeniorPastorName = "";
                        new_external_contact.LeaderName = "";
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;

                        context.Common_ContactExternal.Add(new_external_contact);
                        context.SaveChanges();
                    }


                    var contact_event = (from ccee in context.Common_ContactExternalEvents
                                         where ccee.Contact_ID == contact_id
                                         select ccee);

                    // if none, create
                    if (!contact_event.Any())
                    {

                        // create new common_contactExternalEvents
                        var new_contact_event = new Common_ContactExternalEvents();
                        new_contact_event.Contact_ID = contact_id;
                        new_contact_event.EmergencyContactName = "";
                        new_contact_event.EmergencyContactPhone = "";
                        new_contact_event.EmergencyContactRelationship = "";
                        new_contact_event.MedicalInformation = "";
                        new_contact_event.MedicalAllergies = "";
                        new_contact_event.AccessibilityInformation = "";
                        new_contact_event.EventComments = "";

                        context.Common_ContactExternalEvents.Add(new_contact_event);
                        context.SaveChanges();
                    }

                    var group_list = (from egc in context.Events_GroupContact
                                      where egc.GroupLeader_ID == groupLeaderID
                                      select egc.Contact_ID).ToList();

                    if (!group_list.Contains(contact_id))
                    {
                        var new_group_contact = new Events_GroupContact();
                        new_group_contact.GroupLeader_ID = (int)groupLeaderID;
                        new_group_contact.Contact_ID = contact_id;

                        context.Events_GroupContact.Add(new_group_contact);
                        context.SaveChanges();
                    }

                
                    // Create new registration
                    var registration = new EventRegistration();
                    registration.AllocateGroupRegistration(
                        contact_id: contact_id,
                        group_leader_id: (int)groupLeaderID,
                        venue_id: (int)venueID,
                        registration_type_id: (int)registrationTypeID,
                        accept_terms: true,
                        registered_by_id: (int)groupLeaderID);
                    registration.Save();



                    // Mark invitation as accepted
                    accepted = true;
                    acceptedBy = contact_id;
                    acceptedOn = DateTime.Now;
                    Save();

                    // Send Notification Email
                    var email_model = new GroupInvitationNotificationEmailModel();
                    email_model.GroupLeaderID = (int)groupLeaderID;
                    email_model.InvitationRecipientEmail = (string)receiverEmail;
                    email_model.Event = new Event((int)venueID);
                    email_model.RegistrantID = contact_id;

                    new MailController().InvitationAcceptNotification(email_model);

                    var contact = (from cc in context.Common_Contact
                                   where cc.Contact_ID == contact_id
                                   select cc).FirstOrDefault();

                    var conference = (from ev in context.Events_Venue
                                      join ec in context.Events_Conference
                                      on ev.Conference_ID equals ec.Conference_ID
                                      where ev.Venue_ID == venueID
                                      select new Event
                                      {
                                          id = ec.Conference_ID,
                                          venueID = ev.Venue_ID,
                                          name = ec.ConferenceName,
                                          date = ec.ConferenceDate,
                                          location = ec.ConferenceLocation,
                                          venueLocation = ev.VenueName,
                                          shortname = ec.ConferenceNameShort
                                      }).FirstOrDefault();

                    var confirmationBarcode = new ConfirmationBarcode()
                    {
                        Subject = "Quick Check In - " + conference.name,
                        EventName = conference.name,
                        EventDate = conference.date.ToString(),
                        EventVenue = conference.location,
                        VenueLocation = conference.venueLocation,
                        Email = contact.Email,
                        RegistrationNumber = registration.registrationID.ToString(),
                        RegistrationTypeID = (int)registration.registrationTypeID
                    };

                    // return true if no exceptions
                    return true;
                }
            }
            
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeclineInvitation()
        {
            try
            {
                accepted = false;
                Save();

                // Send Notification Email
                var email_model = new GroupInvitationNotificationEmailModel();
                email_model.GroupLeaderID = (int)groupLeaderID;
                email_model.InvitationRecipientEmail = (string)receiverEmail;
                email_model.Event = new Event((int)venueID);
                new MailController().InvitationDeclineNotification(email_model);

                // Return true if no exceptions
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RevokeInvitation()
        {
            try
            {
                DbDelete();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save changes
        /// </summary>
        public void Save()
        {
            Validate();
            if (_groupInvitationID <= 0)
            {   
                try
                {
                    DbCreate();
                    sendGroupInvitation();
                }
                catch (Exception ex)
                {
                    // If email fails to send, delete invitation from database
                    if (groupInvitationID != null)
                    {
                        DbDelete();
                    }
                    throw ex;
                }
                
            }
            else
            {
                try
                {
                    DbUpdate();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void sendGroupInvitation()
        {
            // Create email and send
            // this shouls not be here?
            var email_model = new GroupInvitationEmailModel();
            email_model.Email = receiverEmail;
            email_model.Event = new Event((int)venueID);
            email_model.GroupLeaderID = (int)groupLeaderID;
            email_model.InvitationID = (Guid)identifier;
            email_model.RegistrationTypeID = (int)registrationTypeID;
            
            new MailController().GroupInvitation(email_model);
        }

        #endregion
        #region Helpers

        /// <summary>
        /// Used to validate data before pushing it up to the database
        /// </summary>
        private void Validate()
        {
            if (_groupLeaderID == 0) throw new Exception("Group Leader ID is undefined");
            if (_registrationTypeID == 0) throw new Exception("Registration type ID is undefined");
            if (_venueID == 0) throw new Exception("Venue ID is undefined");
            if (String.IsNullOrWhiteSpace(_receiverEmail)) throw new Exception("Email is undefined");
            // TODO: more validation (if needed)
        }

        /// <summary>
        /// Parses data from an Events_GroupInvitation object
        /// </summary>
        /// <param name="obj"></param>
        private void ParseObject(Events_GroupInvitation obj)
        {
            // Initialise variables
            _groupInvitationID = obj.GroupInvitation_ID;
            _groupLeaderID = obj.GroupLeader_ID;
            _registrationTypeID = obj.RegistrationType_ID;
            _venueID = obj.Venue_ID;
            _receiverEmail = obj.ReceiverEmail;
            _expirationDate = obj.ExpirationDate;
            _accepted = obj.Accepted;
            _acceptedBy = obj.AcceptedBy;
            _acceptedOn = obj.AcceptedOn;
            _identifier = obj.GUID;
        }

        #endregion
        #region Database Related

        /// <summary>
        /// Creates a new record on the database
        /// </summary>
        private void DbCreate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var new_invitation = new Events_GroupInvitation();

                // Initialise variables
                new_invitation.GroupLeader_ID = _groupLeaderID;
                new_invitation.RegistrationType_ID = _registrationTypeID;
                new_invitation.Venue_ID = _venueID;
                new_invitation.ReceiverEmail = _receiverEmail;
                new_invitation.ExpirationDate = _expirationDate;
                new_invitation.Accepted = _accepted;
                new_invitation.AcceptedBy = _acceptedBy;
                new_invitation.AcceptedOn = _acceptedOn;
                new_invitation.GUID = _identifier;

                context.Events_GroupInvitation.Add(new_invitation);
                context.SaveChanges();

                // Update id in the class
                _groupInvitationID = new_invitation.GroupInvitation_ID;
            }
        }

        /// <summary>
        /// Updates an existing record on the database
        /// </summary>
        private void DbUpdate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var existing = (from gi in context.Events_GroupInvitation
                                where gi.GroupInvitation_ID == _groupInvitationID
                                select gi).FirstOrDefault();

                // Should NEVER be null
                if (existing != null)
                {
                    // Update columns
                    existing.GroupLeader_ID = _groupLeaderID;
                    existing.RegistrationType_ID = _registrationTypeID;
                    existing.ReceiverEmail = _receiverEmail;
                    existing.Venue_ID = _venueID;
                    existing.ExpirationDate = _expirationDate;
                    existing.Accepted = _accepted;
                    existing.AcceptedBy = _acceptedBy;
                    existing.AcceptedOn = _acceptedOn;
                    existing.GUID = _identifier;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Failed to update invitation with id = " + _groupInvitationID);
                }
            }
        }

        /// <summary>
        /// Delete record from the database
        /// </summary>
        private void DbDelete()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var to_delete = (from gi in context.Events_GroupInvitation
                                 where gi.GroupInvitation_ID == _groupInvitationID
                                 select gi).FirstOrDefault();
                context.Events_GroupInvitation.Remove(to_delete);
                context.SaveChanges();
            }
        }

        #endregion
        #region Variables and Get/Set Methods

        private int _groupInvitationID;
        public int? groupInvitationID
        {
            get
            {
                if (_groupInvitationID == 0) return null;
                else return _groupInvitationID;
            }
            private set
            {
                if (value == null) _groupInvitationID = 0;
                else _groupInvitationID = (int)value;
            }
        }

        private int _groupLeaderID;
        public int? groupLeaderID
        {
            get
            {
                if (_groupLeaderID == 0) return null;
                else return _groupLeaderID;
            }
            private set
            {
                if (value == null) _groupLeaderID = 0;
                else _groupLeaderID = (int)value;
            }
        }

        private int _registrationTypeID;
        public int? registrationTypeID
        {
            get
            {
                if (_registrationTypeID == 0) return null;
                else return _registrationTypeID;
            }
            private set
            {
                if (value == null) _registrationTypeID = 0;
                else _registrationTypeID = (int)value;
            }
        }

        private int _venueID;
        public int? venueID
        {
            get
            {
                if (_venueID == 0) return null;
                else return _venueID;
            }
            private set
            {
                if (value == null) _venueID = 0;
                else _venueID = (int)value;
            }
        }

        private string _receiverEmail;
        public string receiverEmail
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_receiverEmail)) return null;
                else return _receiverEmail;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _receiverEmail = String.Empty;
                else _receiverEmail = value;
            }
        }

        private DateTime _expirationDate;
        public DateTime? expirationDate
        {
            get
            {
                if (_expirationDate == DateTime.MinValue) return null;
                else return _expirationDate;
            }
            private set
            {
                if (value == null) _expirationDate = DateTime.MinValue;
                else _expirationDate = (DateTime)value;
            }
        }

        private bool? _accepted;
        public bool? accepted
        {
            get
            {
                return _accepted;
            }
            set
            {
                _accepted = value;
            }
        }

        private int? _acceptedBy;
        public int? acceptedBy
        {
            get
            {
                return _acceptedBy;
            }
            set
            {
                _acceptedBy = value;
            }
        }

        private DateTime? _acceptedOn;
        public DateTime? acceptedOn
        {
            get
            {
                return _acceptedOn;
            }
            set
            {
                _acceptedOn = value;
            }
        }

        private Guid _identifier;
        public Guid? identifier
        {
            get
            {
                return _identifier;
            }
            set
            {
                if (value == null) _identifier = Guid.NewGuid();
                else _identifier = (Guid)value;
            }
        }

        #endregion

    }
}