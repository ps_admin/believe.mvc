﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Events.Mvc.Models.Objects
{
    public class ipregistry
    {
        public string ip { get; set; }
        public string type { get; set; }
        public string hostname { get; set; }
        public carrier carrier { get; set; }
        public connection connection { get; set; }
        public currency currency { get; set; }
        public location location { get; set; }
        public security security { get; set; }
        public time_zone time_zone { get; set; }
    }

    public class carrier
    {
        public string name { get; set; }
        public string mcc { get; set; }
        public string mnc { get; set; }
    }

    public class connection
    {
        public int? asn { get; set; }
        public string domain { get; set; }
        public string organization { get; set; }
        public string route { get; set; }
        public string type { get; set; }
    }

    public class currency
    {
        public string code { get; set; }
        public string name { get; set; }
        public string name_native { get; set; }
        public string plural { get; set; }
        public string plural_native { get; set; }
        public string symbol { get; set; }
        public string symbol_native { get; set; }
        public currformat format { get; set; }
    }
}

public class currformat
{
    public prefixsuffix negative { get; set; }
    public prefixsuffix positive { get; set; }
}

public class prefixsuffix
{
    public string prefix { get; set; }
    public string suffix { get; set; }
}

public class location
{
    public continent continent { get; set; }
    public country country { get; set; }
    public region region { get; set; }
    public string city { get; set; }
    public string postal { get; set; }
    public double? latitude { get; set; }
    public double? longitude { get; set; }
    public language language { get; set; }
    public bool in_eu { get; set; }
}

public class continent
{
    public string code { get; set; }
    public string name { get; set; }
}

public class country
{
    public double? area { get; set; }
    public IList<string> borders { get; set; }
    public string capital { get; set; }
    public string code { get; set; }
    public string name { get; set; }
    public int? population { get; set; }
    public double? population_density { get; set; }
    public flag flag { get; set; }
    public IList<language> languages { get; set; }
    public string tld { get; set; }
}
public class flag
{
    public string emoji { get; set; }
    public string emoji_unicode { get; set; }
    public string emojitwo { get; set; }
    public string noto { get; set; }
    public string twemoji { get; set; }
    public string wikimedia { get; set; }
}


public class language
{
    public string code { get; set; }
    public string name { get; set; }
    public string native { get; set; }

}

public class region
{
    public string code { get; set; }
    public string name { get; set; }
}

public class security
{
    public bool is_bogon { get; set; }
    public bool is_cloud_provider { get; set; }
    public bool is_tor { get; set; }
    public bool is_tor_exit { get; set; }
    public bool is_proxy { get; set; }
    public bool is_anonymous { get; set; }
    public bool is_abuser { get; set; }
    public bool is_attacker { get; set; }
    public bool is_threat { get; set; }
}

public class time_zone
{
    public string id { get; set; }
    public string abbreviation { get; set; }
    public string current_time { get; set; }
    public string name { get; set; }
    public int? offset { get; set; }
    public bool in_daylight_saving { get; set; }
}
