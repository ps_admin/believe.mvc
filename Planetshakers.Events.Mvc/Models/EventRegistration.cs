﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class EventRegistration
    {
        #region Constructors
        
        public EventRegistration()
        {
            registrationID = null;
            contactID = null;
            groupLeaderID = null;
            familyRegistration = null;
            registeredByFriend = null;
            venueID = null;
            registrationTypeID = null;
            discount = null;
            electiveID = null;
            accommodation = null;
            accommodationID = null;
            catering = null;
            cateringNeeds = null;
            leadershipBreakfast = null;
            leadershipSummit = null;
            ulgID = null;
            parentGuardian = null;
            parentGuardianPhone = null;
            acceptTermsRego = null;
            acceptTermsParent = null;
            acceptTermsCreche = null;
            registrationDate = null;
            registeredByID = null;
            attended = null;
            volunteer = null;
            volunteerDepartmentID = null;
            comboRegistrationID = null;
        }

        public EventRegistration(Events_Registration obj)
        {
            ParseObject(obj);
        }

        public EventRegistration(int registration_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var temp = (from r in context.Events_Registration
                            where r.Registration_ID == registration_id
                            select r).FirstOrDefault();

                // Should NEVER be null
                if (temp!= null)
                {
                    ParseObject(temp);
                }
                else
                {
                    throw new Exception("Registration with id = " + registration_id + "not found");
                }
            }
        }

        #endregion
        #region Public Methods

        /// <summary>
        /// The base of a single registration, does not include additional things such as
        /// accommodation/catering/etc
        /// </summary>
        /// <param name="contact_id"></param>
        /// <param name="venue_id"></param>
        /// <param name="registration_type_id"></param>
        /// <param name="accept_terms"></param>
        /// <param name="registered_by_id"></param>
        public void NewSingleRegistration(int contact_id, int venue_id, 
                                          int registration_type_id, bool accept_terms,
                                          int registered_by_id)
        {
            contactID = contact_id;
            venueID = venue_id;
            registrationTypeID = registration_type_id;
            acceptTermsRego = accept_terms;
            registeredByID = registered_by_id;
            registrationDate = DateTime.Now;
        }

        /// <summary>
        /// The base of a group registration, does not include additional things such as
        /// accomodation/catering/etc
        /// </summary>
        /// <param name="contact_id"></param>
        /// <param name="group_leader_id"></param>
        /// <param name="venue_id"></param>
        /// <param name="registration_type_id"></param>
        /// <param name="accept_terms"></param>
        /// <param name="registered_by_id"></param>
        public void AllocateGroupRegistration(int contact_id, int group_leader_id, 
                                              int venue_id, int registration_type_id, 
                                              bool accept_terms, int registered_by_id)
        {
            contactID = contact_id;
            groupLeaderID = group_leader_id;
            venueID = venue_id;
            registrationTypeID = registration_type_id;
            acceptTermsRego = accept_terms;
            registeredByID = registered_by_id;
            registrationDate = DateTime.Now;
        }

        /// <summary>
        /// Adds an accomodation to the registration
        /// </summary>
        /// <param name="accommodation_id"></param>
        public void IncludeAccomodation(int accommodation_id)
        {
            accommodation = true;
            accommodationID = accommodation_id;
        }

        /// <summary>
        /// Remove allocated accommodation, does nothing if no 
        /// accommodation is currently allocated.
        /// </summary>
        public void RemoveAccomodation()
        {
            accommodation = null;
            accommodationID = null;
        }

        /// <summary>
        /// Adds catering to the registration
        /// </summary>
        /// <param name="catering_needs"></param>
        public void IncludeCatering(string catering_needs)
        {
            catering = true;
            cateringNeeds = catering_needs;
        }

        /// <summary>
        /// Remove catering, does nothing if currently has no catering.
        /// </summary>
        public void RemoveCatering()
        {
            catering = null;
            cateringNeeds = null;
        }

        /// <summary>
        /// Includes leadership breakfast into the registration
        /// </summary>
        public void IncludeLeadershipBreakfast()
        {
            leadershipBreakfast = true;
        }

        /// <summary>
        /// Removes leadership breakfast from the registration
        /// </summary>
        public void RemoveLeadershipBreakfast()
        {
            leadershipBreakfast = false;
        }

        /// <summary>
        /// Includes leadership summit into the registration
        /// </summary>
        public void IncludeLeadershipSummit()
        {
            leadershipSummit = true;
        }

        /// <summary>
        /// Removes leadership summit from the registration
        /// </summary>
        public void RemoveLeadershipSummit()
        {
            leadershipSummit = false;
        }

        public bool Delete()
        {
            try
            {
                DbDelete();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save()
        {
            try
            {
                Validate();
                if (_registrationID <= 0) DbCreate();
                else DbUpdate();
            }
            catch (Exception ex)
            {
                // Handle the exception, or throw it again
                throw ex;
            }
        }

        #endregion
        #region Helpers

        /// <summary>
        /// Used to validate data before pushing it up to the database
        /// </summary>
        private void Validate()
        {
            if (_contactID == 0) throw new Exception("Contact ID is undefined");
            if (_venueID == 0) throw new Exception("Venue ID is undefined");
            if (_registrationTypeID == 0) throw new Exception("Registration Type ID is undefined");
            if (_registeredByID == 0) throw new Exception("Registered By ID is undefined");
            // TODO: more validation (if needed)
        }

        /// <summary>
        /// Parse data from an Events_Registration object
        /// </summary>
        /// <param name="obj"></param>
        private void ParseObject(Events_Registration obj)
        {
            _registrationID = obj.Registration_ID;
            _contactID = obj.Contact_ID;
            _groupLeaderID = obj.GroupLeader_ID;
            _familyRegistration = obj.FamilyRegistration;
            _registeredByFriend = obj.RegisteredByFriend_ID ;
            _venueID = obj.Venue_ID;
            _registrationTypeID = obj.RegistrationType_ID;
            _discount = obj.RegistrationDiscount;
            _electiveID = obj.Elective_ID;
            _accommodation = obj.Accommodation;
            _accommodationID = obj.Accommodation_ID;
            _catering = obj.Catering;
            _cateringNeeds = obj.CateringNeeds;
            _leadershipBreakfast = obj.LeadershipBreakfast;
            _leadershipSummit = obj.LeadershipSummit;
            _ulgID = obj.ULG_ID;
            _parentGuardian = obj.ParentGuardian;
            _parentGuardianPhone = obj.ParentGuardianPhone;
            _acceptTermsRego = obj.AcceptTermsRego;
            _acceptTermsParent = obj.AcceptTermsParent;
            _acceptTermsCreche = obj.AcceptTermsCreche;
            _registrationDate = obj.RegistrationDate;
            _registeredByID = obj.RegisteredBy_ID;
            _attended = obj.Attended;
            _volunteer = obj.Volunteer;
            _volunteerDepartmentID = obj.VolunteerDepartment_ID;
            _comboRegistrationID = obj.ComboRegistration_ID;
        }

        #endregion
        #region Database related

        private void DbCreate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var new_registration = new Events_Registration();

                // Initialise variable
                new_registration.Contact_ID = _contactID;
                new_registration.GroupLeader_ID = _groupLeaderID;
                new_registration.FamilyRegistration = _familyRegistration;
                new_registration.RegisteredByFriend_ID = _registeredByFriend;
                new_registration.Venue_ID = _venueID;
                new_registration.RegistrationType_ID = _registrationTypeID;
                new_registration.RegistrationDiscount = _discount;
                new_registration.Elective_ID = _electiveID;
                new_registration.Accommodation = _accommodation;
                new_registration.Accommodation_ID = _accommodationID;
                new_registration.Catering = _catering;
                new_registration.CateringNeeds = _cateringNeeds;
                new_registration.LeadershipBreakfast = _leadershipBreakfast;
                new_registration.LeadershipSummit = _leadershipSummit;
                new_registration.ULG_ID = _ulgID;
                new_registration.ParentGuardian = _parentGuardian;
                new_registration.ParentGuardianPhone = _parentGuardianPhone;
                new_registration.AcceptTermsRego = _acceptTermsRego;
                new_registration.AcceptTermsParent = _acceptTermsParent;
                new_registration.AcceptTermsCreche = _acceptTermsCreche;
                new_registration.RegistrationDate = _registrationDate;
                new_registration.RegisteredBy_ID = _registeredByID;
                new_registration.Attended = _attended;
                new_registration.Volunteer = _volunteer;
                new_registration.VolunteerDepartment_ID = _volunteerDepartmentID;
                new_registration.ComboRegistration_ID = _comboRegistrationID;

                context.Events_Registration.Add(new_registration);
                context.SaveChanges();

                // Update id in the class.
                _registrationID = new_registration.Registration_ID;
            }
        }

        private void DbUpdate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var existing = (from r in context.Events_Registration
                                where r.Registration_ID == _registrationID
                                select r).FirstOrDefault();

                if (existing != null)
                {
                    // Update Columns
                    existing.Contact_ID = _contactID;
                    existing.GroupLeader_ID = _groupLeaderID;
                    existing.FamilyRegistration = _familyRegistration;
                    existing.RegisteredByFriend_ID = _registeredByFriend;
                    existing.Venue_ID = _venueID;
                    existing.RegistrationType_ID = _registrationTypeID;
                    existing.RegistrationDiscount = _discount;
                    existing.Elective_ID = _electiveID;
                    existing.Accommodation = _accommodation;
                    existing.Accommodation_ID = _accommodationID;
                    existing.Catering = _catering;
                    existing.CateringNeeds = _cateringNeeds;
                    existing.LeadershipBreakfast = _leadershipBreakfast;
                    existing.LeadershipSummit = _leadershipSummit;
                    existing.ULG_ID = _ulgID;
                    existing.ParentGuardian = _parentGuardian;
                    existing.ParentGuardianPhone = _parentGuardianPhone;
                    existing.AcceptTermsRego = _acceptTermsRego;
                    existing.AcceptTermsParent = _acceptTermsParent;
                    existing.AcceptTermsCreche = _acceptTermsCreche;
                    existing.RegistrationDate = _registrationDate;
                    existing.RegisteredBy_ID = _registeredByID;
                    existing.Attended = _attended;
                    existing.Volunteer = _volunteer;
                    existing.VolunteerDepartment_ID = _volunteerDepartmentID;
                    existing.ComboRegistration_ID = _comboRegistrationID;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Failed to update registration with id = " + _registrationID);
                }
            }
        }

        private void DbDelete()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var rego = (from r in context.Events_Registration
                            where r.Registration_ID == _registrationID
                            select r).FirstOrDefault();

                context.Events_Registration.Remove(rego);
                context.SaveChanges();
            }
        }

        #endregion
        #region Variables and Get/Set Methods

        private int _registrationID;
        public int? registrationID
        {
            get
            {
                if (_registrationID == 0) return null;
                else return _registrationID;
            }
            private set
            {
                if (value == null) _registrationID = 0;
                else _registrationID = (int)value;
            }
        }

        private int _contactID;
        public int? contactID
        {
            get
            {
                if (_contactID == 0) return null;
                else return _contactID;
            }
            private set
            {
                if (value == null) _contactID = 0;
                else _contactID = (int)value;
            }
        }

        private int? _groupLeaderID;
        public int? groupLeaderID
        {
            get
            {
                return _groupLeaderID;
            }
            private set
            {
                _groupLeaderID = value;
            }
        }

        private bool _familyRegistration;
        public bool? familyRegistration
        {
            get
            {
                return _familyRegistration;
            }
            private set
            {
                if (value == null) _familyRegistration = false;
                else _familyRegistration = (bool)value;
            }
        }

        private int? _registeredByFriend;
        public int? registeredByFriend
        {
            get
            {
                return _registeredByFriend;
            }
            private set
            {
                _registeredByFriend = value;
            }
        }

        private int _venueID;
        public int? venueID
        {
            get
            {
                if (_venueID == 0) return null;
                else return _venueID;
            }
            private set
            {
                if (value == null) _venueID = 0;
                else _venueID = (int)value;
            }
        }

        private int _registrationTypeID;
        public int? registrationTypeID
        {
            get
            {
                if (_registrationTypeID == 0) return null;
                else return _registrationTypeID;
            }
            private set
            {
                if (value == null) _registrationTypeID = 0;
                else _registrationTypeID = (int)value;
            }
        }

        private decimal _discount;
        public decimal? discount
        {
            get
            {
                return _discount;
            }
            private set
            {
                if (value == null) _discount = 0;
                else _discount = (int)value;
            }
        }

        private int? _electiveID;
        public int? electiveID
        {
            get
            {
                return _electiveID;
            }
            private set
            {
                _electiveID = value;
            }
        }

        private bool _accommodation;
        public bool? accommodation
        {
            get
            {
                return _accommodation;
            }
            private set
            {
                if (value == null) _accommodation = false;
                else _accommodation = (bool)value;
            }
        }

        private int? _accommodationID;
        public int? accommodationID
        {
            get
            {
                return _accommodationID;
            }
            private set
            {
                _accommodationID = value;
            }
        }

        private bool _catering;
        public bool? catering
        {
            get
            {
                return _catering;
            }
            private set
            {
                if (value == null) _catering = false;
                else _catering = (bool)value;
            }
        }

        private string _cateringNeeds;
        public string cateringNeeds
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_cateringNeeds)) return null;
                else return _cateringNeeds;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _cateringNeeds = String.Empty;
                else _cateringNeeds = value;
            }
        }

        private bool _leadershipBreakfast;
        public bool? leadershipBreakfast
        {
            get
            {
                return _leadershipBreakfast;
            }
            private set
            {
                if (value == null) _leadershipBreakfast = false;
                else _leadershipBreakfast = (bool)value;
            }
        }

        private bool _leadershipSummit;
        public bool? leadershipSummit
        {
            get
            {
                return _leadershipSummit;
            }
            private set
            {
                if (value == null) _leadershipSummit = false;
                else _leadershipSummit = (bool)value;
            }
        }

        private int? _ulgID;
        public int? ulgID
        {
            get
            {
                return _ulgID;
            }
            private set
            {
                _ulgID = value;
            }
        }

        private string _parentGuardian;
        public string parentGuardian
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_parentGuardian)) return null;
                else return _parentGuardian;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _parentGuardian = String.Empty;
                else _parentGuardian = value;
            }
        }

        private string _parentGuardianPhone;
        public string parentGuardianPhone
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_parentGuardianPhone)) return null;
                else return _parentGuardianPhone;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _parentGuardianPhone = String.Empty;
                else _parentGuardianPhone = value;
            }
        }

        private bool _acceptTermsRego;
        public bool? acceptTermsRego
        {
            get
            {
                return _acceptTermsRego;
            }
            private set
            {
                if (value == null) _acceptTermsRego = false;
                else _acceptTermsRego = (bool)value;
            }
        }

        private bool _acceptTermsParent;
        public bool? acceptTermsParent
        {
            get
            {
                return _acceptTermsParent;
            }
            private set
            {
                if (value == null) _acceptTermsParent = false;
                else _acceptTermsParent = (bool)value;
            }
        }

        private bool _acceptTermsCreche;
        public bool? acceptTermsCreche
        {
            get
            {
                return _acceptTermsCreche;
            }
            private set
            {
                if (value == null) _acceptTermsCreche = false;
                else _acceptTermsCreche = (bool)value;
            }
        }

        private DateTime _registrationDate;
        public DateTime? registrationDate
        {
            get
            {
                if (_registrationDate == DateTime.MinValue) return null;
                else return _registrationDate;
            }
            private set
            {
                if (value == null) _registrationDate = DateTime.MinValue;
                else _registrationDate = (DateTime)value;
            }
        }

        private int _registeredByID;
        public int? registeredByID
        {
            get
            {
                if (_registeredByID == 0) return null;
                else return _registeredByID;
            }
            private set
            {
                if (value == null) _registeredByID = 0;
                else _registeredByID = (int)value;
            }
        }

        private bool _attended;
        public bool? attended
        {
            get
            {
                return _attended;
            }
            private set
            {
                if (value == null) _attended = false;
                else _attended = (bool)value;
            }
        }

        private bool _volunteer;
        public bool? volunteer
        {
            get
            {
                return _volunteer;
            }
            private set
            {
                if (value == null) _volunteer = false;
                else _volunteer = (bool)value;
            }
        }

        private int? _volunteerDepartmentID;
        public int? volunteerDepartmentID
        {
            get
            {
                return _volunteerDepartmentID;
            }
            private set
            {
                _volunteerDepartmentID = value;
            }
        }

        private int? _comboRegistrationID;
        public int? comboRegistrationID
        {
            get
            {
                return _comboRegistrationID;
            }
            private set
            {
                _comboRegistrationID = value;
            }
        }

        #endregion
    }
}