//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Mvc.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Events_GroupSummaryBulkPurchaseView
    {
        public int GroupLeader_ID { get; set; }
        public int Venue_ID { get; set; }
        public Nullable<int> AccommodationQuantity { get; set; }
        public Nullable<int> CateringQuantity { get; set; }
        public Nullable<int> LeadershipBreakfastQuantity { get; set; }
        public Nullable<decimal> AccommodationCost { get; set; }
        public Nullable<decimal> CateringCost { get; set; }
        public Nullable<decimal> LeadershipBreakfastCost { get; set; }
    }
}
