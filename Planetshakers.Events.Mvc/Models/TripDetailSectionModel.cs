﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class TripDetailViewModel
    {
        public int Event_ID { get; set; }
        public int Venue_ID { get; set; }
        public string EventName { get; set; }
        public int Contact_ID { get; set; }
        public string GUID { get; set; }
        public List<HostEvent> HostEventList { get; set; }
        public string URL { get; set; }
        public Team Team { get; set; }
        public TripDetailModel Profile { get; set; }
        public DocumentUploadViewModel DocumentUploads { get; set; }
        public void populateViewModel(int Contact_ID, int Event_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                this.Contact_ID = Contact_ID;
                var today = DateTime.Now;
                this.Event_ID = Event_ID;
                this.GUID = (from c in context.Common_Contact where c.Contact_ID == Contact_ID select c.GUID).FirstOrDefault().ToString();
                this.URL = "missions.believeglobal.org/Sponsorship/Donation?Event_ID=" + Event_ID + "&GUID=" + this.GUID;
                
                if (this.Event_ID != null)
                {
                    #region Team
                    var team = (from tc in context.Events_TeamContact
                                join t in context.Events_Team on tc.Team_ID equals t.Team_ID
                                where tc.Contact_ID == Contact_ID && tc.Inactive == false && t.Inactive == false && t.Event_ID == Event_ID
                                select new Team()
                                {
                                    Team_ID = t.Team_ID,
                                    TeamName = t.TeamName
                                }).FirstOrDefault();
                    if (team != null)
                    {
                        team.TeamMembers = (from tc in context.Events_TeamContact
                                            join c in context.Common_Contact on tc.Contact_ID equals c.Contact_ID
                                            where tc.Team_ID == team.Team_ID && tc.Inactive == false
                                            select new TeamMember()
                                            {
                                                Contact_ID = c.Contact_ID,
                                                FirstName = c.FirstName,
                                                LastName = c.LastName,
                                                Mobile = c.Mobile,
                                                Email = c.Email,
                                                RoleNames = (from cr in context.Events_TeamContactRole
                                                             join r in context.Events_TeamRole on cr.Role_ID equals r.TeamRole_ID
                                                             where cr.TeamContact_ID == tc.TeamContact_ID && cr.Active == true
                                                             select r.TeamRole).ToList()
                                            }).ToList();
                        foreach(var member in team.TeamMembers)
                        {
                            if (member.RoleNames.Count > 0)
                            {
                                foreach (var role in member.RoleNames)
                                {
                                    member.RoleString += role + ", ";
                                }
                                member.RoleString = member.RoleString.Substring(0, member.RoleString.Length - 2);
                            } else
                            {
                                member.RoleString = "";
                            }
                        }
                        this.Team = team;
                    } else
                    {
                        this.Team = new Team();
                    }

                    #endregion
                    #region Host/Upcoming Events
                    //hostevent
                    var courses = (from i in context.Church_CourseInstance
                                   join c in context.Church_Course on i.Course_ID equals c.Course_ID
                                   join e in context.Church_ContactCourseEnrolment on i.CourseInstance_ID equals e.CourseInstance_ID
                                   where i.Event_ID == Event_ID
                                   && e.Contact_ID == Contact_ID
                                   && i.StartDate <= today && i.EndDate > today
                                   select new HostEvent()
                                   {
                                       HostEvent_ID = c.Course_ID,
                                       HostEventName = c.Name,
                                       InstanceName = i.Name,
                                       Instance_ID = i.CourseInstance_ID,
                                       Completed = e.Completed,
                                       PointsRequired = c.PointsRequiredForCompletion
                                   }).ToList();
                    foreach (var item in courses)
                    {
                        item.SessionList = (from s in context.Church_CourseSession
                                            where s.CourseInstance_ID == item.Instance_ID
                                            select new Session()
                                            {
                                                Session_ID = s.CourseSession_ID,
                                                Instance_ID = s.CourseInstance_ID,
                                                SessionDate = s.Date,
                                                Passed = (s.Date < today) ? true : false,
                                                Attended = (from a in context.Church_CourseEnrolmentAttendance where a.Contact_ID == Contact_ID && a.CourseSession_ID == s.CourseSession_ID select a).Any(),
                                                Topics = (from st in context.Church_CourseSessionTopic
                                                          join t in context.Church_CourseTopic on st.CourseTopic_ID equals t.CourseTopic_ID
                                                          where st.CourseSession_ID == s.CourseSession_ID
                                                          select t).ToList()
                                            }).ToList();
                        foreach (var session in item.SessionList)
                        {
                            if(session.Topics.Count > 0)
                            {
                                foreach (var topic in session.Topics)
                                {
                                    session.pointWeighting += topic.PointWeighting;
                                    session.TopicString += topic.Name + ", ";
                                }
                                session.TopicString = session.TopicString.Substring(0, session.TopicString.Length - 2);
                            }
                        }
                    }
                    this.HostEventList = courses;
                    #endregion
                    #region Trip Details
                    var tripDetails = (from c in context.Events_ContactProfile
                                       where c.Contact_ID == Contact_ID && c.Event_ID == Event_ID
                                       select new TripDetailModel
                                       {
                                           Contact_ID = c.Contact_ID,
                                           ContactProfile_ID = c.ContactProfile_ID,
                                           AboutMyself = c.AboutMyself,
                                           PhotoURL = c.PictureURL,
                                           TripDetails = c.TripDetail
                                       }).FirstOrDefault();
                    if(tripDetails != null)
                    {
                        FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
                        var photoModel = fileModel.GetFile(Contact_ID.ToString(), tripDetails.PhotoURL);
                        tripDetails.PhotoURL = photoModel.DownloadUrl;
                    } else
                    {
                        tripDetails = new TripDetailModel() { Contact_ID = 0 };
                    }
                    this.Profile = tripDetails;
                    #endregion
                }
                else
                {
                    this.Profile = new TripDetailModel { Contact_ID = 0 };
                    this.HostEventList = new List<HostEvent>();
                    this.Team = new Team();
                    this.URL = "";
                }        
            }
        }
    }
    public class TripDetailModel
    {
        public int ContactProfile_ID { get; set; }
        public int Contact_ID { get; set; }
        public int Event_ID { get; set; }
        public string AboutMyself { get; set; }
        public string TripDetails { get; set; }
        public string PhotoURL { get; set; }

        public bool updateContactProfile()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                if (this.ContactProfile_ID != 0)
                {
                    var contactProfile = (from c in context.Events_ContactProfile
                                          where c.ContactProfile_ID == this.ContactProfile_ID
                                          select c).FirstOrDefault();
                    contactProfile.AboutMyself = this.AboutMyself;
                    contactProfile.TripDetail = this.TripDetails;
                    context.SaveChanges();
                } else
                {
                    var newContactProfile = new Events_ContactProfile
                    {
                        Contact_ID = this.Contact_ID,
                        Event_ID = this.Event_ID,
                        AboutMyself = this.AboutMyself,
                        TripDetail = this.TripDetails
                    };
                    context.Events_ContactProfile.Add(newContactProfile);
                    context.SaveChanges();
                }
                return true;
            }
        }


        public void populate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var profile = (from c in context.Events_ContactProfile
                               where c.Contact_ID == this.Contact_ID && c.Event_ID == this.Event_ID
                               select c).FirstOrDefault();
                if (profile != null)
                {
                    this.ContactProfile_ID = profile.ContactProfile_ID;

                    FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
                    var photoModel = fileModel.GetFile(Contact_ID.ToString(), profile.PictureURL);
                    this.PhotoURL =  photoModel.DownloadUrl;
                    this.AboutMyself = profile.AboutMyself;
                    this.TripDetails = profile.TripDetail;
                } 
            }
        }
    }
    public class HostEvent
    {
        public int HostEvent_ID { get; set; }
        public int Instance_ID { get; set; }
        public string HostEventName { get; set; }
        public string InstanceName { get; set; }
        public List<Session> SessionList { get; set; }
        public int PointsRequired { get; set; }
        public bool Completed { get; set; }

    }
    public class Session
    {
        public int Session_ID { get; set; }
        public int Instance_ID { get; set; }
        public DateTime SessionDate { get; set; }
        public int pointWeighting { get; set; }
        public List<Church_CourseTopic> Topics { get; set; }
        public string TopicString { get; set; }
        public bool Attended { get; set; }
        public bool Passed { get; set; }
    }
    public class Team
    {
        public int Team_ID { get; set; }
        public string TeamName { get; set; }
        public List<TeamMember> TeamMembers { get; set; }
    }
    public class TeamMember
    {
        public int Contact_ID { get; set;}
        public string FirstName { get; set; }
        public string LastName { get; set; }    
        public int Role_ID { get; set; }
        public List<string> RoleNames { get; set; }
        public string RoleString { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
    }
}