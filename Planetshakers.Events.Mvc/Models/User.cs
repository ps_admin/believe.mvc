﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class User
    {
        public int ContactID { get; set; }

        public string Barcode { get; set; }

        public string Email { get; set; }
        public string FirstName { get; set; }

        public bool PasswordResetRequired { get; set; }

        public int PastorOfRegionId { get; set; }

        public bool IsRegionalPastor
        {
            get { return PastorOfRegionId > 0; }
        }

    }
}