﻿using Newtonsoft.Json;
using Planetshakers.Events.Mvc.Models.Objects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Mvc.Models
{
    public class Geolocation
    {
        // This is code to help with Geolocation. Please use both methods below to detect IP as some browsers will mask the first method.
        // 
        // string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        // if (string.IsNullOrEmpty(ipAddress))
        // {
        //     ipAddress = Request.ServerVariables["REMOTE_ADDR"];
        // }

        static string ipregistryAPI = ConfigurationManager.AppSettings["ipregistryAPI"];

        public string GeolocateIP(string ipAddress)
        {

            string origincountry;
            string url = "https://api.ipregistry.co/" + ipAddress + "?key=" + ipregistryAPI;
            HttpWebRequest action = (HttpWebRequest)WebRequest.Create(url);
            action.Method = "GET";
            try
            {
                HttpWebResponse webResponse = (HttpWebResponse)action.GetResponse();
                Stream stream = webResponse.GetResponseStream();
                StreamReader Reader = new StreamReader(stream);
                string ServerResponse;

                using (webResponse)
                {
                    using (stream)
                    {
                        ServerResponse = Reader.ReadToEnd();
                        var jsonResult = JsonConvert.DeserializeObject<ipregistry>(ServerResponse);
                        origincountry = jsonResult.location.country.code ?? "";
                        return origincountry;
                    }
                }


            }
            catch (WebException ex)
            {
                Stream stream = ex.Response.GetResponseStream();
                StreamReader Reader = new StreamReader(stream);
                string ServerResponse;
                ServerResponse = Reader.ReadToEnd();
                var jsonResult = JsonConvert.DeserializeObject<Dictionary<string, string>>(ServerResponse);
                string errorcode = jsonResult["code"].ToString();
                string errormessage = jsonResult["message"].ToString();
                origincountry = "?";

                return origincountry;
            }
        }
    }
}