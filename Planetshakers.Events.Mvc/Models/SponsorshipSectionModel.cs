﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class SponsorshipViewModel
    {
        public void getContact_ID(string GUID)
        {
            using(PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var contact = (from c in context.Common_Contact where c.GUID.ToString() == GUID select c).FirstOrDefault();
                this.Contact_ID = contact.Contact_ID;
                this.Contact_Name = contact.FirstName + " " + contact.LastName;
            }
        }
        public int Contact_ID { get; set; }
        public string Contact_Name { get; set; }
        public int Event_ID { get; set; }
        public decimal Goal { get; set; }
        public decimal Paid { get; set; }
        public decimal Remaining { get; set; }
        public string percentage { get; set; }
        public TripDetailModel profile { get; set; }
        public void getDetails()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //total - goal and paid
                //find add on and minus from both 
                var addOns = (from rt in context.Events_RegistrationType
                              join r in context.Events_Registration on rt.RegistrationType_ID equals r.RegistrationType_ID
                              where rt.Conference_ID == this.Event_ID && r.Contact_ID == Contact_ID && rt.AddOnRegistrationType == true
                              select rt).ToList();
                var addOnSum = addOns.Select(x => x.RegistrationCost).Sum();
                var addOn_IDs = addOns.Select(x => x.RegistrationType_ID).ToList();
                this.Goal = (from rt in context.Events_RegistrationType
                                 join r in context.Events_Registration on rt.RegistrationType_ID equals r.RegistrationType_ID
                                 where rt.Conference_ID == this.Event_ID && r.Contact_ID == Contact_ID
                                 select rt.RegistrationCost).Sum() - addOnSum;
                //dont minus add ons that have not been paid for 
                var paidList = (from gp in context.Events_GroupPayment
                            where gp.GroupLeader_ID == this.Contact_ID && gp.Conference_ID == this.Event_ID && gp.PaymentCompleted == true
                            select gp).ToList();// - addOns;
                decimal minusAddOns = 0;
                foreach (var addon in addOns)
                {
                    var paidFor = paidList.Where(x => x.PaymentAmount == addon.RegistrationCost && x.Comment == "Payment for Add On: " + addon.RegistrationType).Any();
                    if (paidFor)
                    {
                        minusAddOns += addon.RegistrationCost;
                    }
                }
                var paid = paidList.Select(x => x.PaymentAmount).Sum() - minusAddOns;

                var sponsorRecipient = (from v in context.Events_VariablePayment
                                        where v.Conference_ID == this.Event_ID && v.OutgoingContact_ID == this.Contact_ID && v.IncomingContact_ID == null && v.PaymentCompleted == true
                                        select v.PaymentAmount).ToList().Sum();
                var sponsorDonor = (from v in context.Events_VariablePayment
                                        where v.Conference_ID == this.Event_ID && v.OutgoingContact_ID == null && v.IncomingContact_ID == this.Contact_ID && v.PaymentCompleted == true
                                    select v.PaymentAmount).ToList().Sum();
                var sponsorAmount = (sponsorRecipient == null) ? 0 : (decimal)(sponsorRecipient);
                this.Paid = paid + sponsorAmount;
                this.Remaining = this.Goal - paid - sponsorAmount;
                this.percentage = ((1 - this.Remaining / this.Goal)*100).ToString() + "%";

                var profile = new TripDetailModel()
                {
                    Contact_ID = this.Contact_ID,
                    Event_ID = this.Event_ID
                };
                profile.populate();
                this.profile = profile;

            }
        }
        //sponsorship details

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public bool Annonymous { get; set; }
        public double sponsoringAmount { get; set; }
    }

    public class SponsorDetails
    {
        public int Contact_ID { get; set; }
        public int Event_ID { get; set; }
        public decimal Goal { get; set; }
        public decimal Remaining { get; set; }
        public decimal Paid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public bool Annonymous { get; set; }
        public double sponsoringAmount { get; set; }
        public int Sponsorship_ID { get; set; }
    }
}