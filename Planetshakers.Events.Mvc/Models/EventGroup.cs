﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class EventGroup
    {
        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="group_leader_id">Contact ID of the group leader</param>
        public EventGroup(int group_leader_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Search if group exists
                var groupSearch = (from eg in context.Events_Group
                                   where eg.GroupLeader_ID == group_leader_id
                                   select eg);

                if (groupSearch.Any())
                {
                    var group = groupSearch.First();
                    // Group exists, load data
                    _groupLeaderID = group.GroupLeader_ID;
                    _groupName = group.GroupName;
                    _groupColeaders = group.GroupColeaders;
                    _groupYouthLeaderName = group.YouthLeaderName;
                    _groupSizeID = group.GroupSize_ID;
                    _groupCreditStatus = group.GroupCreditStatus_ID;
                    _groupType = group.GroupType_ID;
                    _comments = group.Comments;
                    _detailsVerified = group.DetailsVerified;
                    _deleted = group.Deleted;
                    _creationDate = group.CreationDate;
                    _createdByID = group.CreatedBy_ID;
                    _modificationDate = group.ModificationDate;
                    _modifiedBy_ID = group.ModifiedBy_ID;
                    _identifier = group.GUID;
                }
                else
                {
                    var contactInfo = (from cc in context.Common_Contact
                                       where cc.Contact_ID == group_leader_id
                                       select cc).First();

                    // Group does not exist, create empty with default values
                    groupLeaderID = contactInfo.Contact_ID;
                    groupName = contactInfo.FirstName + " " + contactInfo.LastName;
                    groupColeaders = null;
                    groupYouthLeaderName = null;
                    groupSize = null;
                    groupCreditStatus = null;
                    groupType = null;
                    comments = null;
                    detailsVerified = null;
                    deleted = null;
                    creationDate = DateTime.Now;
                    createdByID = group_leader_id;
                    modificationDate = DateTime.Now;
                    modifiedBy_ID = group_leader_id;
                    identifier = Guid.NewGuid();
                }
            }
        }

        #endregion
        #region Public Methods

        #region Set Methods

        public void SetGroupSize(int qty)
        {
            groupSize = qty;
        }

        public void SetCreditStatus(GroupCreditStatus status)
        {
            groupCreditStatus = status;
        }

        public void SetGroupType(GroupType type)
        {
            groupType = type;
        }
        #endregion

        /// <summary>
        /// Add bulk purchases
        /// </summary>
        /// <param name="venue_id"></param>
        /// <param name="accomodation_qty"></param>
        /// <param name="catering_qty"></param>
        /// <param name="leadership_qty"></param>
        /// <param name="planetkids_qty"></param>
        /// <param name="planetkids_type"></param>
        public void AddBulkPurchase(int venue_id, int accomodation_qty,
                                    int catering_qty, int leadership_qty,
                                    int planetkids_qty, int? planetkids_type)
        {
            var new_bulk_purchase = new EventGroupBulkPurchase();
            new_bulk_purchase.NewBulkPurchase(
                group_leader_id: groupLeaderID,
                venue_id: venue_id,
                accomodation_qty: accomodation_qty,
                catering_qty: catering_qty,
                leadership_qty: leadership_qty,
                planetkids_qty: planetkids_qty,
                planetkids_type: planetkids_type);

            // Add bulk purchase to list
            bulkPurchases.Add(new_bulk_purchase);
        }

        /// <summary>
        /// Add registrations under the group
        /// </summary>
        /// <param name="venue_id"></param>
        /// <param name="registration_type_id"></param>
        /// <param name="qty"></param>
        public EventGroupBulkRegistration AddRegistrations(int venue_id, 
            int registration_type_id, int qty)
        {
            var new_registrations = new EventGroupBulkRegistration();
            new_registrations.NewGroupRegistration(
                group_leader_id: groupLeaderID,
                venue_id: venue_id,
                registration_type_id: registration_type_id,
                qty: qty);

            // Add registration to list
            bulkRegistrations.Add(new_registrations);

            return new_registrations;
        }

        /// <summary>
        /// Add a new credit card payment
        /// </summary>
        /// <param name="conference_id"></param>
        /// <param name="payment_amount"></param>
        /// <param name="cc_number"></param>
        /// <param name="cc_expiry"></param>
        /// <param name="cc_name"></param>
        /// <param name="cc_phone"></param>
        /// <param name="cc_transactionRef"></param>
        /// <param name="payment_by"></param>
        public void AddCreditCardPayment(int conference_id, decimal payment_amount,
                                                      string cc_number, string cc_expiry,
                                                      string cc_name, string cc_phone,
                                                      string cc_transactionRef, int payment_by,
                                                      int bank_acc_id)
        {
            var new_payment = new EventGroupPayment();
            new_payment.NewCCPayment(
                group_leader_id: groupLeaderID,
                conference_id: conference_id,
                payment_amount: payment_amount,
                cc_number: cc_number,
                cc_expiry: cc_expiry,
                cc_name: cc_name,
                cc_phone: cc_phone,
                cc_transactionRef: cc_transactionRef,
                payment_by: payment_by,
                bank_acc_id: bank_acc_id);

            // Add payment into list
            payments.Add(new_payment);
        }

        public EventRegistration AllocateRegistration(int contact_id, 
            int venue_id, int registration_type_id)
        {
            // Make sure registration type is still available
            if (GetUnallocatedQty(venue_id, registration_type_id) <= 0)
                throw new Exception("Registration type with id " + registration_type_id + " is not available");

            // Create a new registration
            var new_registration = new EventRegistration();
            new_registration.AllocateGroupRegistration(contact_id, groupLeaderID,
                venue_id, registration_type_id, true, groupLeaderID);

            registrations.Add(new_registration);
            return new_registration;
        }

        /// <summary>
        /// Retrieve allocated registrations for a specific venue
        /// </summary>
        /// <param name="venue_id"></param>
        /// <returns></returns>
        public List<EventRegistration> GetAllocatedRegistrations(int venue_id)
        {
            return registrations.Where(x => x.venueID == venue_id).ToList();
        }


        /// <summary>
        /// Returns the total number of allocated registrations
        /// </summary>
        /// <param name="venue_id"></param>
        /// <returns></returns>
        public int GetAllocatedQty(int venue_id)
        {
            return registrations.Where(x => x.venueID == venue_id).Count();
        }

        /// <summary>
        /// Returns the total number of allocated registration of a certain type
        /// </summary>
        /// <param name="venue_id"></param>
        /// <param name="registration_type_id"></param>
        /// <returns></returns>
        public int GetAllocatedQty(int venue_id, int registration_type_id)
        {
            return registrations.Where(x => x.venueID == venue_id &&
                                            x.registrationTypeID == registration_type_id)
                                .Count();
        }

        /// <summary>
        /// Retrieve a list of unallocated registrations for a specific venue
        /// </summary>
        /// <param name="venue_id"></param>
        /// <returns></returns>
        public List<EventGroupBulkRegistration> GetUnallocatedRegistrations(int venue_id)
        {
            return bulkRegistrations.Where(x => x.venueID == venue_id).ToList();
        }

        /// <summary>
        /// Get total number of unallocated registrations for a specific event venue
        /// </summary>
        /// <param name="venue_id"></param>
        /// <returns></returns>
        public int GetUnallocatedQty(int venue_id)
        {
            // Sum up number of purchased registrations          
            int qty_purchased = bulkRegistrations.Where(x => x.venueID == venue_id)
                                                 .Sum(x => (int)x.quantity);

            // Sum up number of allocated registrations
            int qty_allocated = registrations.Where(x => x.venueID == venue_id).Count();

            // Return the number of unallocated registrations
            return qty_purchased - qty_allocated;
        }

        /// <summary>
        /// Used to determine how many registrations have yet to be allocated
        /// </summary>
        /// <param name="venue_id"></param>
        /// <param name="registration_type_id"></param>
        /// <returns></returns>
        public int GetUnallocatedQty(int venue_id, int registration_type_id)
        {
            // Retrieve number of registrations purchased
            int qty_purchased = bulkRegistrations.Where(x => x.registrationTypeID == registration_type_id &&
                                                             x.venueID == venue_id).Sum(x => (int)x.quantity);

            // Retrieve number of allocated registrations
            int qty_allocated = registrations.Where(x => x.registrationTypeID == registration_type_id &&
                                                         x.venueID == venue_id).Count();

            // Return the number of unallocated registrations
            return qty_purchased - qty_allocated;
        }

        /// <summary>
        /// Used to determine how many registrations have yet to be allocated
        /// </summary>
        /// <param name="br"></param>
        /// <returns></returns>
        public int GetUnallocatedQty(EventGroupBulkRegistration br)
        {
            return GetUnallocatedQty((int)br.venueID, (int)br.registrationTypeID);
        }

        public List<EventGroupInvitation> GetInvitations(int venue_id)
        {
            return invitations.Where(x => x.venueID == venue_id &&
                                          x.accepted == null &&
                                          x.expirationDate > DateTime.Now).ToList();
        }

        /// <summary>
        /// Used to determine how many invations have been sent for a specific registration type
        /// </summary>
        /// <param name="venue_id"></param>
        /// <param name="registration_type_id"></param>
        /// <returns></returns>
        public int GetInvitationQty(int venue_id, int registration_type_id)
        {
            // Retrieve number of invitations sent that are currently pending/active
            return invitations.Where(x => x.venueID == venue_id &&
                                          x.registrationTypeID == registration_type_id &&
                                          x.accepted == null &&
                                          x.expirationDate > DateTime.Now).Count();
        }

        public bool SendInvitation(string email, int registration_type_id)
        {
            try
            {
                var venue_id = bulkRegistrations
                           .Where(x => x.registrationTypeID == registration_type_id)
                           .Select(y => y.venueID).FirstOrDefault();

                var new_invitation = new EventGroupInvitation();
                new_invitation.NewInvitation(
                    group_leader_id: groupLeaderID,
                    registration_type_id: registration_type_id,
                    venue_id: (int)venue_id,
                    email: email
                    );

                invitations.Add(new_invitation);
                return true;
            }
            catch
            {
                // Something went wrong.
                return false;
            }
        }

        /// <summary>
        /// Indicates than a recipient has accepted an invitation
        /// </summary>
        /// <returns></returns>
        public bool AcceptInvitation(int contact_id, Guid invitation_id)
        {
            var invitation = invitations.Where(x => x.identifier == invitation_id)
                                        .FirstOrDefault();

            if (invitation != null)
            {
                var inv_search = invitations.Where(x => x.identifier == invitation_id)
                                            .FirstOrDefault();
                if (inv_search == null)
                    throw new Exception("Invitation with id " + invitation_id + " not found.");
                else if (inv_search.accepted != null)
                    throw new Exception("Invitation with id " + invitation_id + " has already been responded to");
                else if (inv_search.expirationDate < DateTime.Now)
                    throw new Exception("Invitation has expired");
                                
                return inv_search.AcceptInvitation(contact_id);
            }
            else
            {
                throw new Exception("Invitation with id " + invitation_id + " not found.");
            }
        }

        /// <summary>
        /// Save and push changes to the database
        /// </summary>
        /// <param name="contact_id">ID of the person making changes</param>
        public void Save(int contact_id)
        {
            try
            {
                Validate();
                DbCreateOrUpdate();

                // Save the related objects
                bulkRegistrations.ForEach(x => x.Save());
                bulkPurchases.ForEach(x => x.Save());
                payments.ForEach(x => x.Save());
                registrations.ForEach(x => x.Save());
                invitations.ForEach(x => x.Save());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion
        #region Helpers

        private void Validate()
        {
            // TODO: more validation (if needed)
        }

        /// <summary>
        /// Parses data from and Events_Group object
        /// </summary>
        /// <param name="obj"></param>
        private void ParseObject(Events_Group obj)
        {
            // Initialise variables
            _groupLeaderID = obj.GroupLeader_ID;
            _groupName = obj.GroupName;
            _groupColeaders = obj.GroupColeaders;
            _groupYouthLeaderName = obj.YouthLeaderName;
            _groupSizeID = obj.GroupSize_ID;
            _groupCreditStatus = obj.GroupCreditStatus_ID;
            _groupType = obj.GroupType_ID;
            _comments = obj.Comments;
            _detailsVerified = obj.DetailsVerified;
            _deleted = obj.Deleted;
            _creationDate = obj.CreationDate;
            _createdByID = obj.CreatedBy_ID;
            _modificationDate = obj.ModificationDate;
            _modifiedBy_ID = obj.ModifiedBy_ID;
            _identifier = obj.GUID;
        }

        #endregion
        #region Database Related

        /// <summary>
        /// Creates a new record on the database
        /// </summary>
        private void DbCreateOrUpdate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var existing = (from g in context.Events_Group
                                where g.GroupLeader_ID == _groupLeaderID
                                select g).FirstOrDefault();

                if (existing != null)
                {
                    existing.GroupName = _groupName;
                    existing.GroupColeaders = _groupColeaders;
                    existing.YouthLeaderName = _groupYouthLeaderName;
                    existing.GroupSize_ID = _groupSizeID;
                    existing.GroupCreditStatus_ID = _groupCreditStatus;
                    existing.GroupType_ID = _groupType;
                    existing.Comments = _comments;
                    existing.DetailsVerified = _detailsVerified;
                    existing.Deleted = _deleted;
                    existing.CreationDate = _creationDate;
                    existing.CreatedBy_ID = _createdByID;
                    existing.ModificationDate = _modificationDate;
                    existing.ModifiedBy_ID = _modifiedBy_ID;
                    existing.GUID = _identifier;

                    context.SaveChanges();
                }
                else
                {
                    // create new group
                    var new_group = new Events_Group();

                    new_group.GroupLeader_ID = _groupLeaderID;
                    new_group.GroupName = _groupName;
                    new_group.GroupColeaders = _groupColeaders;
                    new_group.YouthLeaderName = _groupYouthLeaderName;
                    new_group.GroupSize_ID = _groupSizeID;
                    new_group.GroupCreditStatus_ID = _groupCreditStatus;
                    new_group.GroupType_ID = _groupType;
                    new_group.Comments = _comments;
                    new_group.DetailsVerified = _detailsVerified;
                    new_group.Deleted = _deleted;
                    new_group.CreationDate = _creationDate;
                    new_group.CreatedBy_ID = _createdByID;
                    new_group.ModificationDate = _modificationDate;
                    new_group.ModifiedBy_ID = _modifiedBy_ID;
                    new_group.GUID = _identifier;

                    context.Events_Group.Add(new_group);
                    context.SaveChanges();
                }
            }
        }

        #endregion
        #region Variables and Get/Set Methods

        private int _groupLeaderID;
        public int groupLeaderID
        {
            get
            {
                return _groupLeaderID;
            }
            private set
            {
                _groupLeaderID = value;
            }
        }

        private string _groupName;
        public string groupName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_groupName)) return null;
                else return _groupName;
            }
            private set 
            {
                if (String.IsNullOrWhiteSpace(value)) _groupName = String.Empty;
                else _groupName = value;
            }
        }

        private string _groupColeaders;
        public string groupColeaders
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_groupColeaders)) return null;
                else return _groupColeaders;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _groupColeaders = String.Empty;
                else _groupColeaders = value;
            }
        }

        private string _groupYouthLeaderName;
        public string groupYouthLeaderName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_groupYouthLeaderName)) return null;
                else return _groupYouthLeaderName;
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _groupYouthLeaderName = String.Empty;
                else _groupYouthLeaderName = value;
            }
        }

        private int? _groupSizeID;
        private int? groupSize
        {
            set
            {
                if (value > 500) _groupSizeID = 9;
                else if (value > 200) _groupSizeID = 8;
                else if (value > 150) _groupSizeID = 7;
                else if (value > 100) _groupSizeID = 6;
                else if (value > 75) _groupSizeID = 5;
                else if (value > 50) _groupSizeID = 4;
                else if (value > 20) _groupSizeID = 3;
                else if (value > 10) _groupSizeID = 2;
                else if (value > 0) _groupSizeID = 1;
                else _groupSizeID = null;
            }
        }

        private int _groupCreditStatus;
        public GroupCreditStatus? groupCreditStatus
        {
            get
            {
                if (_groupCreditStatus == 0) return null;
                else return (GroupCreditStatus)_groupCreditStatus;
            }
            private set
            {
                if (value == null) _groupCreditStatus = 0;
                else _groupCreditStatus = (int)value;
            }
        }

        private int _groupType;
        public GroupType? groupType
        {
            get
            {
                if (_groupType == 0) return null;
                else return (GroupType)_groupType;
            }
            private set
            {
                if (value == null) _groupType = 0;
                else _groupType = (int)value;
            }
        }

        private string _comments;
        public string comments
        {
            get 
            {
                if (String.IsNullOrWhiteSpace(_comments)) return null;
                else return _comments; 
            }
            private set
            {
                if (String.IsNullOrWhiteSpace(value)) _comments = String.Empty;
                else _comments = value;
            }
        }

        private bool _detailsVerified;
        public bool? detailsVerified
        {
            get 
            {
                return _detailsVerified; 
            }
            private set 
            {
                if (value == null) _detailsVerified = false;
                else _detailsVerified = (bool)value; 
            }
        }

        private bool _deleted;
        public bool? deleted
        {
            get
            {
                return _deleted;
            }
            private set
            {
                if (value == null) _deleted = false;
                else _deleted = (bool)value;
            }
        }

        private DateTime _creationDate;
        public DateTime? creationDate
        {
            get
            {
                if (_creationDate == DateTime.MinValue) return null;
                else return _creationDate;
            }
            private set
            {
                if (value == null) _creationDate = DateTime.MinValue;
                else _creationDate = (DateTime)value;
            }
        }

        private int _createdByID;
        public int? createdByID
        {
            get
            {
                if (_createdByID == 0) return null;
                else return _createdByID;
            }
            private set
            {
                if (value == null) _createdByID = 0;
                else _createdByID = (int)value;
            }
        }

        private DateTime? _modificationDate;
        public DateTime? modificationDate
        {
            get
            {
                return _modificationDate;
            }
            private set
            {
                _modificationDate = value;
            }
        }

        private int? _modifiedBy_ID;
        public int? modifiedBy_ID
        {
            get
            {
                return _modifiedBy_ID;
            }
            private set
            {
                _modifiedBy_ID = value;
            }
        }

        private Guid _identifier;
        public Guid? identifier
        {
            get
            {
                if (_identifier == Guid.Empty) return null;
                else return _identifier;
            }
            private set
            {
                if (value == null) _identifier = Guid.Empty;
                else _identifier = (Guid)value;
            }
        }

        /// <summary>
        /// List of bulk registrations attached to this account.
        /// </summary>
        private List<EventGroupBulkRegistration> _bulkRegistrations;
        public List<EventGroupBulkRegistration> bulkRegistrations
        {
            get
            {
                if (_bulkRegistrations == null)
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var temp = (from br in context.Events_GroupBulkRegistration
                                    where br.GroupLeader_ID == groupLeaderID && br.Deleted == false
                                    select br).ToList();

                        // Retrieve existing bulk registrations and turn into a list
                        _bulkRegistrations = new List<EventGroupBulkRegistration>();
                        foreach (var br in temp)
                        {
                            _bulkRegistrations.Add(new EventGroupBulkRegistration(br));
                        }
                    }
                }
                return _bulkRegistrations;
            }
        }

        /// <summary>
        /// List of bulk purchases (accomodation, catering, leadership breakfast)
        /// </summary>
        private List<EventGroupBulkPurchase> _bulkPurchases;
        public List<EventGroupBulkPurchase> bulkPurchases
        {
            get
            {
                if (_bulkPurchases == null)
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var temp = (from bp in context.Events_GroupBulkPurchase
                                    where bp.GroupLeader_ID == groupLeaderID
                                    select bp).ToList();

                        // Retrieve existing bulk purchases and turn into a list
                        _bulkPurchases = new List<EventGroupBulkPurchase>();
                        foreach(var bp in temp)
                        {
                            _bulkPurchases.Add(new EventGroupBulkPurchase(bp));
                        }
                    }
                }
                return _bulkPurchases;
            }
        }

        /// <summary>
        /// List of payments made in this group
        /// </summary>
        private List<EventGroupPayment> _payments;
        public List<EventGroupPayment> payments
        {
            get
            {
                if (_payments == null)
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var temp = (from gp in context.Events_GroupPayment
                                    where gp.GroupLeader_ID == groupLeaderID
                                    select gp).ToList();

                        // Retrieve existing payments and turn into a list
                        _payments = new List<EventGroupPayment>();
                        foreach(var gp in temp)
                        {
                            _payments.Add(new EventGroupPayment(gp));
                        }
                        
                    }
                }
                return _payments;
            }
        }

        /// <summary>
        /// Registrations allocated under this group
        /// </summary>
        private List<EventRegistration> _registrations;
        public List<EventRegistration> registrations
        {
            get
            {
                if (_registrations == null)
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var temp = (from er in context.Events_Registration
                                    where er.GroupLeader_ID == groupLeaderID
                                    select er).ToList();

                        // Retrieve registrations under the group and turn into a list
                        _registrations = new List<EventRegistration>();
                        foreach(var er in temp)
                        {
                            _registrations.Add(new EventRegistration(er));
                        }
                    }
                }
                return _registrations;
            }
        }

        private List<EventGroupInvitation> _invitations;
        public List<EventGroupInvitation> invitations
        {
            get
            {
                if (_invitations == null)
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // Retrieve invitations under the group and turn into a list
                        var temp = (from gi in context.Events_GroupInvitation
                                    where gi.GroupLeader_ID == groupLeaderID
                                    select gi).ToList();

                        _invitations = new List<EventGroupInvitation>();
                        foreach (var gi in temp)
                        {
                            _invitations.Add(new EventGroupInvitation(gi));
                        }
                    }
                }
                return _invitations;
            }
        }

        #endregion
    }
}