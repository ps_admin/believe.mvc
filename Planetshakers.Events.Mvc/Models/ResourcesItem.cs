﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class ResourcesItem
    {
        public string DownloadUrl { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public bool ShowInResources { get; set; }
        public int Index { get; set; }
    }
}