﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class EventGroupBulkPurchase
    {
        #region Constructors

        /// <summary>
        /// Initializer with default values
        /// </summary>
        public EventGroupBulkPurchase()
        {
            groupBulkPurchaseID = null;
            groupLeaderID = null;
            venueID = null;
            accomodationQty = null;
            cateringQty = null;
            leadershipBreakfastQty = null;
            planetKidsQty = null;
            planetKidsRegoType = null;
            dateAdded = null;
        }

        public EventGroupBulkPurchase(Events_GroupBulkPurchase obj)
        {
            ParseObject(obj);
        }

        public EventGroupBulkPurchase(int group_bulk_purchase_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var temp = (from bp in context.Events_GroupBulkPurchase
                            where bp.GroupBulkPurchase_ID == group_bulk_purchase_id
                            select bp).FirstOrDefault();
                
                //Should NEVER be null;
                if (temp != null)
                {
                    ParseObject(temp);
                }
                else
                {
                    throw new Exception("Group Bulk Purchase with id = " +
                                        group_bulk_purchase_id + " not found");
                }
            }
        }

        #endregion
        #region Public Methods

        /// <summary>
        /// Creates a new bulk registration entry
        /// </summary>
        /// <param name="group_leader_id">Contact ID of the group leader</param>
        /// <param name="venue_id">Venue ID</param>
        /// <param name="accomodation_qty">Number of accomodation purchased</param>
        /// <param name="catering_qty">Number of catering purchased</param>
        /// <param name="leadership_qty">Number of Leadership Breakfast purchased</param>
        /// <param name="planetkids_qty">Number of Planetkids registration purchased</param>
        /// <param name="planetkids_type">Type of Planetkids registration</param>
        public void NewBulkPurchase(int group_leader_id, int venue_id,
                                    int accomodation_qty, int catering_qty,
                                    int leadership_qty, int planetkids_qty,
                                    int? planetkids_type)
        {
            groupLeaderID = group_leader_id;
            venueID = venue_id;
            accomodationQty = accomodation_qty;
            cateringQty = catering_qty;
            leadershipBreakfastQty = leadership_qty;
            planetKidsQty = planetkids_qty;
            planetKidsRegoType = planetkids_type;
            dateAdded = DateTime.Now;
        }

        /// <summary>
        /// Save changes
        /// </summary>
        public void Save()
        {
            try
            {
                Validate();
                if (_groupBulkPurchaseID <= 0) DbCreate();
                else DbUpdate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        #region Helpers

        /// <summary>
        /// Used to validate data before pushing it up to the database
        /// </summary>
        private void Validate()
        {
            if (_groupLeaderID == 0) throw new Exception("Group Leader ID is undefined");
            if (_venueID == 0) throw new Exception("Venue ID is undefined");
            // TODO: more validation (if needed)
        }

        /// <summary>
        /// Parses data from an Events_GroupBulkPurchase object
        /// </summary>
        /// <param name="obj"></param>
        private void ParseObject(Events_GroupBulkPurchase obj)
        {
            _groupBulkPurchaseID = obj.GroupBulkPurchase_ID;
            _groupLeaderID = obj.GroupLeader_ID;
            _venueID = obj.Venue_ID;
            _accomodationQty = obj.AccommodationQuantity;
            _cateringQty = obj.CateringQuantity;
            _leadershipBreakfastQty = obj.LeadershipBreakfastQuantity;
            _planetKidsQty = obj.PlanetkidsQuantity;
            _planetKidsRegoType = obj.PlanetKidsRegistrationType_ID;
            _dateAdded = obj.DateAdded;
        }

        #endregion
        #region Database related

        /// <summary>
        /// Creates a new record on the database
        /// </summary>
        private void DbCreate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var new_groupBulkPurchase = new Events_GroupBulkPurchase();

                new_groupBulkPurchase.GroupLeader_ID = _groupLeaderID;
                new_groupBulkPurchase.Venue_ID = _venueID;
                new_groupBulkPurchase.AccommodationQuantity = _accomodationQty;
                new_groupBulkPurchase.CateringQuantity = _cateringQty;
                new_groupBulkPurchase.LeadershipBreakfastQuantity = _leadershipBreakfastQty;
                new_groupBulkPurchase.PlanetkidsQuantity = _planetKidsQty;
                new_groupBulkPurchase.PlanetKidsRegistrationType_ID = _planetKidsRegoType;
                new_groupBulkPurchase.DateAdded = _dateAdded;

                context.Events_GroupBulkPurchase.Add(new_groupBulkPurchase);
                context.SaveChanges();

                // Update id in the class.
                _groupBulkPurchaseID = new_groupBulkPurchase.GroupBulkPurchase_ID;
            }
        }

        /// <summary>
        /// Updates an existing record on the database
        /// </summary>
        private void DbUpdate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var existing = (from bp in context.Events_GroupBulkPurchase
                                where bp.GroupBulkPurchase_ID == _groupBulkPurchaseID
                                select bp).FirstOrDefault();

                // Update Columns
                if (existing != null)
                {
                    existing.GroupLeader_ID = _groupLeaderID;
                    existing.Venue_ID = _venueID;
                    existing.AccommodationQuantity = _accomodationQty;
                    existing.CateringQuantity = _cateringQty;
                    existing.LeadershipBreakfastQuantity = _leadershipBreakfastQty;
                    existing.PlanetkidsQuantity = _planetKidsQty;
                    existing.PlanetKidsRegistrationType_ID = _planetKidsRegoType;
                    existing.DateAdded = _dateAdded;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Failed to update group bulk purchase with id = " 
                                        + _groupBulkPurchaseID);
                }
            }
        }

        #endregion
        #region Variables and Get/Set Methods

        private int _groupBulkPurchaseID;
        public int? groupBulkPurchaseID
        {
            get
            {
                if (_groupBulkPurchaseID == 0) return null;
                else  return _groupBulkPurchaseID;
            }
            private set
            {
                if (value == null) _groupBulkPurchaseID = 0;
                else _groupBulkPurchaseID = (int)value;
            }
        }

        private int _groupLeaderID;
        public int? groupLeaderID
        {
            get
            {
                if (_groupLeaderID == 0) return null;
                else return _groupLeaderID;
            }
            private set
            {
                if (value == null) _groupLeaderID = 0;
                else _groupLeaderID = (int)value;
            }
        }

        private int _venueID;
        public int? venueID
        {
            get
            {
                if (_venueID == 0) return null;
                else return _venueID;
            }
            private set
            {
                if (value == null) _venueID = 0;
                else _venueID = (int)value;
            }
        }

        private int _accomodationQty;
        public int? accomodationQty
        {
            get
            {
                return _accomodationQty;
            }
            private set
            {
                if (value == null) _accomodationQty = 0;
                else _accomodationQty = (int)value;
            }
        }

        private int _cateringQty;
        public int? cateringQty
        {
            get
            {
                return _cateringQty;
            }
            private set
            {
                if (value == null) _cateringQty = 0;
                else _cateringQty = (int)value;
            }
        }

        private int _leadershipBreakfastQty;
        public int? leadershipBreakfastQty
        {
            get
            {
                return _leadershipBreakfastQty;
            }
            private set
            {
                if (value == null) _leadershipBreakfastQty = 0;
                else _leadershipBreakfastQty = (int)value;
            }
        }

        private int _planetKidsQty;
        public int? planetKidsQty
        {
            get
            {
                return _planetKidsQty;
            }
            private set
            {
                if (value == null) _planetKidsQty = 0;
                else _planetKidsQty = (int)value;
            }
        }

        private int? _planetKidsRegoType;
        public int? planetKidsRegoType
        {
            get
            {
                return _planetKidsRegoType;
            }
            private set
            {
                _planetKidsRegoType = value;
            }
        }

        private DateTime _dateAdded;
        public DateTime? dateAdded
        {
            get
            {
                if (_dateAdded == DateTime.MinValue) return null;
                else return _dateAdded;
            }
            private set
            {
                if (value == null) _dateAdded = DateTime.MinValue;
                else _dateAdded = (DateTime)value;
            }
        }

        #endregion
    }
}