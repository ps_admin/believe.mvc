﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Planetshakers.Events.Mvc.Controllers;

namespace Planetshakers.Events.Mvc.Models
{
    public class LoginPromptModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        // Event 
        public Event Event { get; set; }
    }

    public class CartModel
    {
        public string ErrorMessage { get; set; }

        // Logged in contact
        public int ContactId { get; set; }

        // Items in the cart
        public List<CartItem> Cart { get; set; }

        // Event 
        public Event Event { get; set; }

        // Total cost
        public decimal GrandTotal { get { return Cart.Sum(r => r.subtotal); } }

        // CC Info
        public CCPaymentModel CCPayment { get; set; }

    }

    public class CCPaymentModel
    {
        /*
        [Display(Name = "Credit Card Type")]
        public int CCType { get; set; }
        public SelectList CCTypeList { get; set; }
        */
        [Required]
        [Display(Name = "Credit Card Number")]       
        public string CCNumber { get; set; }
        public string getCCNumber(bool masked)
        {
            if (masked)
            {
                return String.Concat(CCNumber.Substring(0,4),
                                     "".PadLeft(CCNumber.Length - 8, 'X'), 
                                     CCNumber.Substring(CCNumber.Length - 4));
            }
            else
            {
                return CCNumber.Replace("-", "").Replace(" ", "").Replace("/", "");
            }
        }

        [Required]
        [Display(Name = "Credit Card Expiry")]
        public int CCExpiryMonth { get; set; }
        public SelectList CCExpMonthList { get; set; }
        [Required]
        public int CCExpiryYear { get; set; }
        public SelectList CCExpYearList { get; set; }

        public string getCCExpiry()
        {
            return CCExpiryMonth.ToString("00") + CCExpiryYear.ToString("00");
        }

        [Required]
        [Display(Name = "CVC2")]
        public string CCCVC { get; set; }

        [Required]
        [Display(Name = "Cardholder's Name")]
        public string CCName { get; set; }

        [Required]
        [Display(Name = "Cardholder's Phone #")]
        public string CCPhone { get; set; }


    }
}