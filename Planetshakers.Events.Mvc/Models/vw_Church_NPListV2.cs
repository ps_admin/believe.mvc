//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Mvc.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Church_NPListV2
    {
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
        public Nullable<int> State_ID { get; set; }
        public string State { get; set; }
        public Nullable<int> Country_ID { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool DoNotIncludeEmail1InMailingList { get; set; }
        public string Email2 { get; set; }
        public bool DoNotIncludeEmail2InMailingList { get; set; }
        public System.Guid GUID { get; set; }
        public Nullable<int> Expr1 { get; set; }
        public Nullable<int> ChurchStatus_ID { get; set; }
        public string Church_Status { get; set; }
        public Nullable<int> Campus_ID { get; set; }
        public string Campus { get; set; }
        public Nullable<int> Ministry_ID { get; set; }
        public string Ministry { get; set; }
        public Nullable<int> Region_ID { get; set; }
        public string Region { get; set; }
        public string PostalAddress1 { get; set; }
        public string PostalAddress2 { get; set; }
        public string PostalSuburb { get; set; }
        public string PostalPostcode { get; set; }
        public Nullable<int> PostalState_ID { get; set; }
        public string Expr2 { get; set; }
        public Nullable<int> PostalCountry_ID { get; set; }
        public string Expr3 { get; set; }
        public Nullable<int> NPNC_ID { get; set; }
        public Nullable<int> NPNCType_ID { get; set; }
        public Nullable<System.DateTime> FirstContactDate { get; set; }
        public Nullable<System.DateTime> FirstFollowupCallDate { get; set; }
        public Nullable<System.DateTime> FirstFollowupVisitDate { get; set; }
        public Nullable<int> InitialStatus_ID { get; set; }
        public Nullable<int> NCDecisionType_ID { get; set; }
        public Nullable<int> NCTeamMember_ID { get; set; }
        public Nullable<int> CampusDecision_ID { get; set; }
        public Nullable<int> Expr5 { get; set; }
        public Nullable<int> Expr6 { get; set; }
        public Nullable<int> Expr7 { get; set; }
        public Nullable<int> ULG_ID { get; set; }
        public Nullable<System.DateTime> ULGDateOfIssue { get; set; }
        public Nullable<int> Carer_ID { get; set; }
        public Nullable<int> OutcomeStatus_ID { get; set; }
        public Nullable<System.DateTime> OutcomeDate { get; set; }
        public Nullable<int> OutcomeInactive_ID { get; set; }
        public Nullable<System.Guid> Expr8 { get; set; }
        public Nullable<int> Comment_ID { get; set; }
        public Nullable<int> Expr4 { get; set; }
        public Nullable<int> CommentBy_ID { get; set; }
        public string Comment { get; set; }
        public Nullable<int> CommentType_ID { get; set; }
        public Nullable<int> CommentSource_ID { get; set; }
        public Nullable<int> Expr9 { get; set; }
        public Nullable<System.DateTime> lastcomment { get; set; }
    }
}
