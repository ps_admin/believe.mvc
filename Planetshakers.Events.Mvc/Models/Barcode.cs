﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class Registration
    {
        public int Registration_ID { get; set; }
        public int Contact_ID { get; set; }
        public int Venue_ID { get; set; }
        public string Event_Name { get; set; }
        public string Barcode { get; set; }

        public int RegistrationType_ID { get; set; }
        public string RegistrationType { get; set; }
    }

    public class RegistrationViewModel
    {
        public List<Registration> Registration_List { get; set; }
    }
}