﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class ContactPersonalDetails
    {
        // salutation
        public int salutation_id { get; set; }

        // firstname of the contact
        public string firstname { get; set; }

        // lastname of the contact
        public string lastname { get; set; }

        // date of birth
        public DateTime? dob { get; set; }

        // gender
        public string gender { get; set; }

        // Address line 1
        public string address1 { get; set; }

        // Address line 2
        public string address2 { get; set; }

        // Suburb
        public string suburb { get; set; }

        // State ID
        public int? state_id { get; set; }
        public string state { get; set; }

        // State Other
        public string state_other { get; set; }

        // Postcode
        public string postcode { get; set; }

        // Country ID
        public int? country_id { get; set; }

        // Mobile
        public string mobile { get; set; }

        // Email
        public string email { get; set; }

        public string email2 { get; set; }
    }
}