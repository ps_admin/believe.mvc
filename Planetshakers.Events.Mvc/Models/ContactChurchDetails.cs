﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class ContactChurchDetails
    {
        // church name
        public string name { get; set; }

        // church denomination
        public int? denomination_id { get; set; }

        // senior pastor's name
        public string senior_pastor { get; set; }

        // leader's name
        public string leader { get; set; }

        // Congregation size
        public string congregation_size { get; set; }

        // Address line 1
        public string address1 { get; set; }

        // Address line 2
        public string address2 { get; set; }

        // Suburb
        public string suburb { get; set; }

        // State
        public string state { get; set; }

        // postcode
        public string postcode { get; set; }

        // country
        public int? country_id { get; set; }

        // phone
        public string phone { get; set; }

        // website
        public string website { get; set; }

        // email
        public string email { get; set; }
    }
}