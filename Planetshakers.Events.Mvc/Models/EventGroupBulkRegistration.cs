﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class EventGroupBulkRegistration
    {
        #region Constructors

        /// <summary>
        /// Constructor with default values
        /// </summary>
        public EventGroupBulkRegistration()
        {
            groupBulkRegoID = null;
            groupLeaderID = null;
            venueID = null;
            registrationTypeID = null;
            quantity = null;
            dateAdded = null;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="obj"></param>
        public EventGroupBulkRegistration(Events_GroupBulkRegistration obj)
        {
            ParseObject(obj);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="group_bulk_registration_id"></param>
        public EventGroupBulkRegistration(int group_bulk_registration_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var temp = (from br in context.Events_GroupBulkRegistration
                            where br.GroupBulkRegistration_ID == group_bulk_registration_id && br.Deleted == false
                            select br).FirstOrDefault();

                // Should NEVER be null
                if (temp != null)
                {
                    ParseObject(temp);
                }
                else
                {
                    throw new Exception("Group Bulk Registration with id = " + 
                                        group_bulk_registration_id + " not found");
                }

            }
        }

        #endregion
        #region Public Methods

        /// <summary>
        /// Creates a new group bulk registration entry
        /// </summary>
        /// <param name="group_leader_id">Group Leader ID</param>
        /// <param name="venue_id">Venue ID</param>
        /// <param name="registration_type_id">Registration Type ID</param>
        /// <param name="qty">Quantity of registration purchased</param>
        public void NewGroupRegistration(int group_leader_id, int venue_id,
                                         int registration_type_id, int qty)
        {
            groupLeaderID = group_leader_id;
            venueID = venue_id;
            registrationTypeID = registration_type_id;
            quantity = qty;
            dateAdded = DateTime.Now;
        }

        public decimal GetSubTotal()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // get single price
                var single_price = (from rt in context.Events_RegistrationType
                                    where rt.RegistrationType_ID == registrationTypeID
                                    select rt.RegistrationCost).FirstOrDefault();

                return single_price * _quantity;
            }
        }

        public bool Delete()
        {
            try
            {
                DbDelete();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save changes
        /// </summary>
        public void Save()
        {
            try
            {
                Validate();
                if (_groupBulkRegoID <= 0) DbCreate();
                else DbUpdate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        #region Helpers

        /// <summary>
        /// Used to validate data before pushing it up to the database
        /// </summary>
        private void Validate()
        {
            if (_groupLeaderID == 0) throw new Exception("Group Leader ID is undefined");
            if (_venueID == 0) throw new Exception("Venue ID is undefined");
            if (_registrationTypeID == 0) throw new Exception("Registration Type is undefined");
            if (_quantity <= 0) throw new Exception("Quantity must be greater than 0");
            // TODO: more validation (if needed)
        }

        /// <summary>
        /// Parses data from an Events_GroupBulkRegistration object
        /// </summary>
        /// <param name="obj"></param>
        private void ParseObject(Events_GroupBulkRegistration obj)
        {
            // Initialise variables
            _groupBulkRegoID = obj.GroupBulkRegistration_ID;
            _groupLeaderID = obj.GroupLeader_ID;
            _venueID = obj.Venue_ID;
            _registrationTypeID = obj.RegistrationType_ID;
            _quantity = obj.Quantity;
            _dateAdded = obj.DateAdded;
        }

        #endregion
        #region Database related

        /// <summary>
        /// Creates a new record on the database
        /// </summary>
        private void DbCreate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var new_groupBulkRego = new Events_GroupBulkRegistration();

                // Initialise variables
                new_groupBulkRego.GroupLeader_ID = _groupLeaderID;
                new_groupBulkRego.Venue_ID = _venueID;
                new_groupBulkRego.RegistrationType_ID = _registrationTypeID;
                new_groupBulkRego.Quantity = _quantity;
                new_groupBulkRego.DateAdded = _dateAdded;
                new_groupBulkRego.Deleted = false;

                context.Events_GroupBulkRegistration.Add(new_groupBulkRego);
                context.SaveChanges();

                // Update id in the class.
                _groupBulkRegoID = new_groupBulkRego.GroupBulkRegistration_ID;
            }
        }

        /// <summary>
        /// Updates an existing record on the database
        /// </summary>
        private void DbUpdate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var existing = (from br in context.Events_GroupBulkRegistration
                                where br.GroupBulkRegistration_ID == _groupBulkRegoID && br.Deleted == false
                                select br).FirstOrDefault();

                // Should NEVER be null
                if (existing != null)
                {
                    // Update Columns
                    existing.GroupLeader_ID = _groupLeaderID;
                    existing.Venue_ID = _venueID;
                    existing.RegistrationType_ID = _registrationTypeID;
                    existing.Quantity = _quantity;
                    existing.DateAdded = _dateAdded;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Failed to update group registration with id = " +
                                        _groupBulkRegoID);
                }
            }
        }

        private void DbDelete()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var to_delete = (from gbr in context.Events_GroupBulkRegistration
                                 where gbr.GroupBulkRegistration_ID == _groupBulkRegoID
                                 select gbr).FirstOrDefault();
                context.Events_GroupBulkRegistration.Remove(to_delete);
                context.SaveChanges();
            }
        }

        #endregion
        #region Variables and Get/Set Methods

        private int _groupBulkRegoID;
        public int? groupBulkRegoID
        {
            get
            {
                if (_groupBulkRegoID == 0) return null;
                else return _groupBulkRegoID;
            }
            private set
            {
                if (value == null) _groupBulkRegoID = 0;
                else _groupBulkRegoID = (int)value;
            }
        }

        private int _groupLeaderID;
        public int? groupLeaderID
        {
            get
            {
                if (_groupLeaderID == 0) return null;
                else return _groupLeaderID;
            }
            private set
            {
                if (value == null) _groupLeaderID = 0;
                else _groupLeaderID = (int)value;
            }
        }

        private int _venueID;
        public int? venueID
        {
            get
            {
                if (_venueID == 0) return null;
                else return _venueID;
            }
            private set
            {
                if (value == null) _venueID = 0;
                else _venueID = (int)value;
            }
        }

        private int _registrationTypeID;
        public int? registrationTypeID
        {
            get
            {
                if (_registrationTypeID == 0) return null;
                else return _registrationTypeID;
            }
            private set
            {
                if (value == null) _registrationTypeID = 0;
                else _registrationTypeID = (int)value;
            }
        }

        private int _quantity;
        public int? quantity
        {
            get
            {
                return _quantity;
            }
            private set
            {
                if (value == null) _quantity = 0;
                else _quantity = (int)value;
            }
        }

        private DateTime _dateAdded;
        public DateTime? dateAdded
        {
            get 
            {
                if (_dateAdded == DateTime.MinValue) return null;
                else return _dateAdded; 
            }
            private set
            {
                if (value == null) _dateAdded = DateTime.MinValue;
                else _dateAdded = (DateTime)value;
            }
        }

        #endregion
    }
}