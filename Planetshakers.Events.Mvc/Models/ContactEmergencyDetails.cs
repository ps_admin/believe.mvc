﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class ContactEmergencyDetails
    {
        // Emergency contact name
        public string name { get; set; }

        // Emergency contact Phone
        public string phone { get; set; }

        // Emergency contact relationship
        public string relationship { get; set; }

        // Medical information
        public string medical_info { get; set; }

        // Medical allergies
        public string medical_allergies { get; set; }
    }
}