﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class State
    {
        // state id
        public int id { get; set; }

        // id of the country this state belongs to
        public int country_id { get; set; }

        // name
        public string name { get; set; }
    }
}