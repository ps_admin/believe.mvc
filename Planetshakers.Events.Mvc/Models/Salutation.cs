﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class Salutation
    {
        // salutation id
        public int id { get; set; }

        // salutation name
        public string name { get; set; }

    }
}