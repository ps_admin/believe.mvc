//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Mvc.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Common_ContactAddressViewV2
    {
        public int Contact_ID { get; set; }
        public string StreetAddress { get; set; }
        public string StreetAddressMultiLine { get; set; }
        public string PostalAddress { get; set; }
        public string InternalExternal { get; set; }
    }
}
