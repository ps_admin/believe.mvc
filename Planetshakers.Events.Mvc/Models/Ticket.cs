﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Mvc.Models
{
    public class TicketModel
    {
        public TicketModel()
        {

        }

        public TicketModel(string firstname, string email)
        {
            this.Tickets = new List<Ticket>();
            this.FirstName = firstname;
            this.Email = email;
        }

        public List<Ticket> Tickets { get; set; }

        public string FirstName { get; set; }
        public string Email { get; set; }
    }

    public class SGConcert2020
    {
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }
    }

    public class SGRegistrationReceipt
    {
        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("ReferenceNumber")]
        public string ReferenceNumber { get; set; }

        [JsonProperty("RegistrationType")]
        public string RegistrationType { get; set; }

        [JsonProperty("Quantity")]
        public int Quantity { get; set; }

        [JsonProperty("Price")]
        public string Price { get; set; }

        [JsonProperty("Total")]
        public string Total { get; set; }
    }

    public class Ticket
    {
        public int reg_barcode { get; set; }
    }

    public class SponsorshipEmailModel
    {
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("EventName")]
        public string EventName { get; set; }

        [JsonProperty("Total")]
        public string Total { get; set; }
    }
}