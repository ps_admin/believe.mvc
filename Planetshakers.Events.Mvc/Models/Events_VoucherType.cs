//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Mvc.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Events_VoucherType
    {
        public Events_VoucherType()
        {
            this.Events_Voucher = new HashSet<Events_Voucher>();
        }
    
        public int VoucherType_ID { get; set; }
        public string VoucherTypeName { get; set; }
        public int MaxUses { get; set; }
        public bool Inactive { get; set; }
    
        public virtual ICollection<Events_Voucher> Events_Voucher { get; set; }
    }
}
