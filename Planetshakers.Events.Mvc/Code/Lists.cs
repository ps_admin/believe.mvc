﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Planetshakers.Events.Mvc.Models;
using Planetshakers.Events.Mvc.Controllers;
using Planetshakers.Business.External.Events;
using Amazon.S3.Model;

namespace Planetshakers.Events.Mvc
{
    public static class Lists
    {
        // Retrieve list of genders (not from the database)
        public static List<SelectListItem> get_genders()
        {
            var gendersList = new List<SelectListItem>();

            gendersList.Add(new SelectListItem()
            {
                Text = "Male",
                Value = "Male",
            });
            gendersList.Add(new SelectListItem()
            {
                Text = "Female",
                Value = "Female",
            });

            return gendersList;
        }

        // Retrieve list of grades from the database
        public static List<SelectListItem> getGrades()
        {
            if (HttpContext.Current.Session != null &&
                HttpContext.Current.Session["school_grades"] != null)
                return HttpContext.Current.Session["school_grades"] as List<SelectListItem>;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var schoolGrade = (from sg in context.Events_ChildSchoolGrade
                                   orderby sg.SortOrder
                                   select new SchoolGrade
                                   {
                                       id = sg.ChildSchoolGrade_ID,
                                       name = sg.Grade
                                   }).ToList();

                // Convert into select list
                var schoolGradeList = schoolGrade.Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.id.ToString()
                }).ToList();

                HttpContext.Current.Session["school_grades"] = schoolGradeList;

                return schoolGradeList;
            }
        }

        // Retrieve list of salutations from the database
        public static List<SelectListItem> getSalutations()
        {
            // only continue if salutations have not been previously retrieved
            if (HttpContext.Current.Session != null &&
                HttpContext.Current.Session["salutations"] != null)
                return HttpContext.Current.Session["salutations"] as List<SelectListItem>;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var salutations = (from cs in context.Common_Salutation
                                   orderby cs.SortOrder
                                   select new Salutation
                                   {
                                       id = cs.Salutation_ID,
                                       name = cs.Salutation
                                   }).ToList();

                // Convert into select list
                var salutationList = salutations.Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.id.ToString()
                }).ToList();

                HttpContext.Current.Session["salutations"] = salutationList;

                return salutationList;
            }
        }

        // Retrieve list of countries from the database
        public static List<SelectListItem> getCountries()
        {
            // only continue if countries have not been previously retrieved
            if (HttpContext.Current.Session != null &&
                HttpContext.Current.Session["countries"] != null)
                return HttpContext.Current.Session["countries"] as List<SelectListItem>;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var countries = (from cgc in context.Common_GeneralCountry
                                 orderby cgc.SortOrder
                                 select new Country
                                 {
                                     id = cgc.Country_ID,
                                     name = cgc.Country,
                                     currency = cgc.Currency
                                 }).ToList();

                // Convert into select list
                var countriesList = countries.Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.id.ToString()
                }).ToList();

                HttpContext.Current.Session["countries"] = countriesList;
                return countriesList;
            }
        }

        // Retrieve list of States from the database
        public static List<SelectListItem> getStates()
        {
            // only continue if states have not been previously retrieved
            if (HttpContext.Current.Session != null &&
                HttpContext.Current.Session["states"] != null)
                return HttpContext.Current.Session["states"] as List<SelectListItem>;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var states = (from cgs in context.Common_GeneralState
                              where cgs.Country_ID == 11
                              orderby cgs.SortOrder
                              select new State
                              {
                                  id = cgs.State_ID,
                                  country_id = cgs.Country_ID,
                                  name = cgs.State
                              }).ToList();

                // Convert into select list
                var statesList = states.Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.id.ToString()
                }).ToList();

                HttpContext.Current.Session["states"] = statesList;
                return statesList;
            }
        }

        // Retrieve list of Church Denominations
        public static List<SelectListItem> getChurchDenominations()
        {
            // only continue if denominations have not been previously retrieved
            if (HttpContext.Current.Session != null &&
                HttpContext.Current.Session["denominations"] != null)
                return HttpContext.Current.Session["denominations"] as List<SelectListItem>;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var denominations = (from d in context.Common_Denomination
                                     orderby d.Denomination
                                     select d).ToList();

                // Convert into select list
                var denominationList = denominations.Select(x => new SelectListItem
                {
                    Text = x.Denomination,
                    Value = x.Denomination_ID.ToString()
                }).ToList();

                HttpContext.Current.Session["denominations"] = denominationList;
                return denominationList;
            }
        }

        // Retrieve list of Church Involvement Categories
        public static List<ChurchInvolvement> getChurchInvolvement(int? contact_id)
        {
            // do not store this into session.

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var churchInvolvement = (from ci in context.External_ChurchInvolvement
                                         orderby ci.Name
                                         select new ChurchInvolvement
                                         {
                                             id = ci.ChurchInvolvement_ID,
                                             name = ci.Name
                                         }).ToList();

                if (contact_id == null) return churchInvolvement;

                foreach (var ci in churchInvolvement)
                {
                    var contactInvolved = (from cci in context.External_ContactChurchInvolvement
                                           where cci.ChurchInvolvement_ID == ci.id &&
                                                 cci.Contact_ID == contact_id
                                           select cci).ToList();
                    if (contactInvolved.Any()) ci.is_selected = true;
                }

                return churchInvolvement;
            }
        }

        #region Events

        /* Retrieve details for the events to be shown on the website and stores it in the session*/
        /*UPDATE: 2012-04-14 included isIndex as part of input, 
         * If accessing retrieve events from index page then check visibility, else dont check
         */
        public static List<Event> RetrieveEvents(int? contact_id, bool register)
        {
            var ps_events = new List<Event>();
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // IMPORTANT : Use this to retrieve events dynamically in the future
                // retrieve events that should be shown on the web and have registrations available
                /*var venues = (from c  in context.Events_Conference
                              join v  in context.Events_Venue 
                                      on c.Conference_ID equals v.Conference_ID
                              join rt in context.Events_RegistrationType 
                                      on c.Conference_ID equals rt.Conference_ID
                              where c.ShowConferenceOnWeb == true &&
                                   (c.AllowOnlineRegistrationSingle || 
                                    c.AllowOnlineRegistrationGroup) &&
                                    rt.StartDate <= DateTime.Now && 
                                    rt.EndDate >= DateTime.Now
                              select v).Distinct().ToList();*/

                // events will now be shown until the end of the conference,
                // if no registrations available, only the view registrations window is available.
                var venues = (from c in context.Events_Conference
                              join v in context.Events_Venue on c.Conference_ID equals v.Conference_ID
                              join rt in context.Events_RegistrationType on c.Conference_ID equals rt.Conference_ID
                              where c.ShowConferenceOnWeb == true &&
                                    (c.AllowOnlineRegistrationSingle || c.AllowOnlineRegistrationGroup) &&
                                    v.ConferenceEndDate >= DateTime.Now &&
                                    rt.StartDate <= DateTime.Now
                              select v).Distinct().ToList();

                ps_events = new List<Event>();
                foreach (var venue in venues)
                {
                    ps_events.Add(new Event(venue.Venue_ID));
                }
                ps_events = ps_events.OrderBy(x => x.sort_order).ToList();

                //mark any duplicated allocation as incomplete (based on outgoing contact id, not event id) 
                var sponsorshipAllocations = (from s in context.Events_VariablePayment
                                              where s.IncomingContact_ID == null && s.OutgoingContact_ID == contact_id 
                                              && (s.PaymentCompleted ?? false) && s.Refund == false && s.CCTransactionReference != null
                                              select s).GroupBy(x => x.CCTransactionReference);
                foreach (var transaction in sponsorshipAllocations)
                {
                    if (transaction.Count() > 1)
                    {
                        //duplicated allocation
                        foreach (var item in transaction.Skip(1))
                        {
                            item.PaymentCompleted = false;
                            item.PaymentCompletedDate = null;
                            item.Comment = "Duplicated Allocation | updated: " + DateTime.Now.ToString("dd-MMM-yy HH:mm");
                        }
                    }
                }
                context.SaveChanges();

                foreach (Event ps_event in ps_events)
                {

                    if (contact_id != 0)
                    {
                        ps_event.allocatedToUser = (from eg in context.Events_Registration
                                                    where eg.Venue_ID == ps_event.venueID && eg.Contact_ID == contact_id
                                                    select eg).Any();

                        ps_event.boughtByUser = (from egr in context.Events_GroupBulkRegistration
                                                 where egr.Venue_ID == ps_event.venueID && egr.GroupLeader_ID == contact_id
                                                 && egr.Deleted == false
                                                 select egr).Any();
                        ps_event.ApplicationProgress = (from p in context.Volunteer_PendingRole
                                                        join r in context.Events_Registration on p.Registration_ID equals r.Registration_ID
                                                        join a in context.Volunteer_Application on p.Application_ID equals a.Application_ID
                                                        join pr in context.Volunteer_ApplicationProgress on a.Application_ID equals pr.VolunteerApplication_ID
                                                        where r.Venue_ID == ps_event.venueID && a.Contact_ID == contact_id
                                                        select pr.Status_ID).FirstOrDefault();


                        if (ps_event.boughtByUser)
                        {
                            

                            //add funding goals 
                            //remove cost and payment of add-ons 
                            var addOnRegoTypeCost = (from r in context.Events_GroupBulkRegistration
                                                     join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                                     where r.GroupLeader_ID == contact_id
                                                            && r.Venue_ID == ps_event.venueID
                                                            && r.Deleted == false
                                                            && rt.AddOnRegistrationType == true
                                                     select r.Quantity * rt.RegistrationCost).DefaultIfEmpty().Sum();

                            var totalCost = (from r in context.Events_GroupBulkRegistration
                                             join t in context.Events_RegistrationType on r.RegistrationType_ID equals t.RegistrationType_ID
                                             where r.GroupLeader_ID == contact_id
                                                 && r.Venue_ID == ps_event.venueID
                                                 && r.Deleted == false
                                             select r.Quantity * t.RegistrationCost).DefaultIfEmpty().Sum() - addOnRegoTypeCost;

                            var totalPaid = (from p in context.Events_GroupPayment
                                             where p.GroupLeader_ID == contact_id
                                                 && p.Conference_ID == ps_event.id
                                                 && p.PaymentCompleted == true
                                                 && (p.ExpiredIntent == false || p.ExpiredIntent == null)
                                             select p.PaymentAmount).DefaultIfEmpty().Sum() - addOnRegoTypeCost;

                            var sponsorRecipient = (from v in context.Events_VariablePayment
                                                    where v.Conference_ID == ps_event.id && v.PaymentCompleted == true  
                                                        && v.OutgoingContact_ID == contact_id
                                                        && v.IncomingContact_ID == null
                                                    select v.PaymentAmount).ToList().Sum();
                            var sponsorDonor = (from v in context.Events_VariablePayment
                                                    where v.Conference_ID == ps_event.id
                                                        && v.OutgoingContact_ID == null
                                                        && v.IncomingContact_ID == contact_id
                                                        && v.PaymentCompleted == true
                                                select v.PaymentAmount).ToList().Sum();

                            ps_event.outstandingBalance = Math.Round((double)totalCost - (double)totalPaid - (double)sponsorRecipient, 2);
                            ps_event.totalCost = (double)totalCost;
                            ps_event.totalPayment = ps_event.totalCost - ps_event.outstandingBalance;
                            ps_event.addOnPayments = (double)addOnRegoTypeCost;
                            ps_event.show = true;
                        }
                        else
                        {
                            ps_event.show = (register) ? true : false;
                        }

                        ps_event.available_registration_types = (from rt in context.Events_RegistrationType
                                                                 where rt.Conference_ID == ps_event.id
                                                                 && (!rt.RegistrationType.Contains("DONATED")
                                                                 || !rt.RegistrationType.Contains("REFUNDED")
                                                                 || !rt.RegistrationType.Contains("CREDITED"))
                                                                 select rt).Any();
                    }
                    else
                    {
                        ps_event.allocatedToUser = false;
                        ps_event.boughtByUser = false;
                        ps_event.available_registration_types = false;
                        ps_event.actioned = false;
                        ps_event.show = (register) ? true : false;
                    }

                    var sold_single_registrations = (from er in context.Events_Registration
                                                     where er.Venue_ID == ps_event.venueID && er.GroupLeader_ID == null
                                                     select er).GroupBy(l => l.RegistrationType_ID).Select(rt => new RegistrationType
                                                     {
                                                         id = rt.Key,
                                                         sold = rt.Count()
                                                     }).ToList();

                    var sold_group_registrations = (from egbr in context.Events_GroupBulkRegistration
                                                    join r in context.Events_RegistrationType on egbr.RegistrationType_ID equals r.RegistrationType_ID
                                                    select egbr).GroupBy(l => l.RegistrationType_ID).Select(rt => new RegistrationType
                                                    {
                                                        id = rt.Key,
                                                        sold = rt.Sum(q => q.Quantity),
                                                    }).ToList();

                    // Retrieve single registrations for the event
                    var single_registrations = new List<RegistrationType>();

                    if (ps_event.ApplicationProgress != 0)
                    {
                        single_registrations = (from ert in context.Events_RegistrationType
                                                join erv in context.Events_RegistrationTypeVenue
                                                         on ert.RegistrationType_ID equals erv.RegistrationType_ID
                                                where erv.Venue_ID == ps_event.venueID &&
                                                      ert.StartDate <= DateTime.Now && ert.EndDate >= DateTime.Now
                                                orderby ert.SortOrder
                                                select new RegistrationType
                                                {
                                                    id = ert.RegistrationType_ID,
                                                    name = ert.RegistrationType,
                                                    cost = ert.RegistrationCost,
                                                    limit = ert.Limit
                                                }).ToList();
                    }
                    else
                    {
                        single_registrations = (from ert in context.Events_RegistrationType
                                                join erv in context.Events_RegistrationTypeVenue
                                                         on ert.RegistrationType_ID equals erv.RegistrationType_ID
                                                where erv.Venue_ID == ps_event.venueID && ert.ApplicationRegistrationType == true &&
                                                      ert.StartDate <= DateTime.Now && ert.EndDate >= DateTime.Now
                                                orderby ert.SortOrder
                                                select new RegistrationType
                                                {
                                                    id = ert.RegistrationType_ID,
                                                    name = ert.RegistrationType,
                                                    cost = ert.RegistrationCost,
                                                    limit = ert.Limit
                                                }).ToList();
                    }



                    // calculate remaining rego type 
                    foreach (var sr in single_registrations)
                    {
                        var item = sold_single_registrations.Find(x => x.id == sr.id);
                        if (item != null)
                        {
                            sr.limit -= (int)item.sold;
                        }

                        var item2 = sold_group_registrations.Find(x => x.id == sr.id);
                        if (item2 != null)
                        {
                            sr.limit -= (int)item2.sold;
                        }
                    }


                    // Initialize event registration categories
                    ps_event.rego_categories = new List<RegistrationCategory>();


                    // If any single registrations found, include into the event object
                    if (single_registrations.Any())
                    {
                        ps_event.rego_categories.Add(new RegistrationCategory
                        {
                            name = "Registration",
                            is_group_rego = false,
                            group_id = 0,
                            group_min_qty = 1,
                            rego_types = single_registrations
                        });
                    }
                }
            }
            if (contact_id == 0 || !register)
            {
                return ps_events.Where(x => x.show == true).ToList();
            }
            else
            {
                return ps_events;
            }
        }

        #endregion
        #region Registrations

        // Retrieves a list of registrations belonging the specified contact
        public static List<CurrentRegistration> GetSingleRegistrations()
        {
            // only continue if event data has not been retrieved.
            /*if (HttpContext.Current.Session != null &&
                HttpContext.Current.Session["registrations_single"] != null) 
                    return HttpContext.Current.Session["registrations_single"] as List<CurrentRegistration>;*/

            var registrations = new List<CurrentRegistration>();

            // Retrieve logged in contact from session
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)HttpContext.Current.Session["Contact"];
            // Returns empty list when contact is not logged in
            if (contact == null || !System.Web.HttpContext.Current.User.Identity.IsAuthenticated) return registrations;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                registrations = (from er in context.Events_Registration
                                 join cc in context.Common_Contact on er.Contact_ID equals cc.Contact_ID
                                 join ert in context.Events_RegistrationType on er.RegistrationType_ID equals ert.RegistrationType_ID
                                 join ec in context.Events_Conference on ert.Conference_ID equals ec.Conference_ID
                                 join c in context.Common_GeneralCountry on ec.Country_ID equals c.Country_ID
                                 where er.Contact_ID == contact.Id
                                 select new CurrentRegistration
                                 {
                                     registration_name = ert.RegistrationType,
                                     registration_id = er.Registration_ID,
                                     group_leader_id = er.GroupLeader_ID,
                                     registration_type_id = er.RegistrationType_ID,
                                     registration_date = er.RegistrationDate,
                                     event_id = ec.Conference_ID,
                                     event_name = ec.ConferenceName,
                                     venue_id = er.Venue_ID,
                                     contact_id = cc.Contact_ID,
                                     contact_name = cc.FirstName + " " + cc.LastName,
                                     currency = c.Currency,
                                     cost_total = ert.RegistrationCost
                                 }).ToList();

                foreach (var reg in registrations)
                {
                    if (reg.group_leader_id != null)
                    {
                        reg.group_leader = (from cc in context.Common_Contact
                                            where cc.Contact_ID == reg.group_leader_id
                                            select cc.FirstName + " " + cc.LastName).FirstOrDefault();
                    }
                }
            }

            /*HttpContext.Current.Session["registrations_single"] = registrations;*/
            return registrations;
        }

        // Retrieves a list of friend registrations that has been registered by the specified contact
        public static List<CurrentRegistration> GetFriendRegistrations()
        {
            // only continue if event data has not been retrieved.
            /*if (HttpContext.Current.Session != null &&
                HttpContext.Current.Session["registrations_friend"] != null)
                return HttpContext.Current.Session["registrations_friend"] as List<CurrentRegistration>;*/

            var registrations = new List<CurrentRegistration>();

            // Retrieve logged in contact from session
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)HttpContext.Current.Session["Contact"];
            // Returns empty list when contact is not logged in
            if (contact == null || !System.Web.HttpContext.Current.User.Identity.IsAuthenticated) return registrations;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // friend registrations
                registrations = (from er in context.Events_Registration
                                 join cc in context.Common_Contact on er.Contact_ID equals cc.Contact_ID
                                 join ert in context.Events_RegistrationType on er.RegistrationType_ID equals ert.RegistrationType_ID
                                 join ec in context.Events_Conference on ert.Conference_ID equals ec.Conference_ID
                                 join c in context.Common_GeneralCountry on ec.Country_ID equals c.Country_ID
                                 where er.RegisteredByFriend_ID == contact.Id
                                 select new CurrentRegistration
                                 {
                                     registration_name = ert.RegistrationType,
                                     registration_id = er.Registration_ID,
                                     registration_type_id = er.RegistrationType_ID,
                                     registration_date = er.RegistrationDate,
                                     event_id = ec.Conference_ID,
                                     event_name = ec.ConferenceName,
                                     venue_id = er.Venue_ID,
                                     contact_id = cc.Contact_ID,
                                     contact_name = cc.FirstName + " " + cc.LastName,
                                     currency = c.Currency,
                                     cost_total = ert.RegistrationCost
                                 }).ToList();
            }

            /*HttpContext.Current.Session["registrations_friend"] = registrations;*/
            return registrations;
        }

        // Retrieves all registrations belonging to the contact's family
        public static List<CurrentRegistration> GetFamilyRegistrations()
        {
            // only continue if event data has not been retrieved.
            /*if (HttpContext.Current.Session != null &&
                HttpContext.Current.Session["registrations_family"] != null) 
                return HttpContext.Current.Session["registrations_family"] as List<CurrentRegistration>;*/

            var registrations = new List<CurrentRegistration>();

            // Retrieve logged in contact from session
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)HttpContext.Current.Session["Contact"];
            // Returns empty list when contact is not logged in
            if (contact == null || !System.Web.HttpContext.Current.User.Identity.IsAuthenticated) return registrations;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // family registrations 
                registrations = (from cc in context.Common_Contact
                                 join cf in context.Common_Family on cc.Family_ID equals cf.Family_ID
                                 join fm in context.Common_Contact on cf.Family_ID equals fm.Family_ID
                                 join fmt in context.Common_FamilyMemberType on fm.FamilyMemberType_ID equals fmt.FamilyMemberType_ID
                                 join er in context.Events_Registration on fm.Contact_ID equals er.Contact_ID
                                 join ert in context.Events_RegistrationType on er.RegistrationType_ID equals ert.RegistrationType_ID
                                 join ec in context.Events_Conference on ert.Conference_ID equals ec.Conference_ID
                                 join c in context.Common_GeneralCountry on ec.Country_ID equals c.Country_ID
                                 where cc.Contact_ID == contact.Id
                                 select new CurrentRegistration
                                 {
                                     registration_name = ert.RegistrationType,
                                     registration_id = er.Registration_ID,
                                     registration_type_id = er.RegistrationType_ID,
                                     registration_date = er.RegistrationDate,
                                     event_id = ec.Conference_ID,
                                     event_name = ec.ConferenceName,
                                     venue_id = er.Venue_ID,
                                     contact_id = fm.Contact_ID,
                                     contact_name = fm.FirstName + " " + fm.LastName,
                                     currency = c.Currency,
                                     cost_total = ert.RegistrationCost,
                                     family_member_type = fmt.Name
                                 }).ToList();
            }

            /*HttpContext.Current.Session["registrations_family"] = registrations;*/
            return registrations;
        }

        // Retrieves all group registrations purchased by contact
        public static List<CurrentRegistration> GetGroupRegistrations()
        {
            // only continue if event data has not been retrieved.
            /*if (HttpContext.Current.Session != null &&
                HttpContext.Current.Session["registrations_group"] != null)
                return HttpContext.Current.Session["registrations_group"] as List<CurrentRegistration>;*/

            var registrations = new List<CurrentRegistration>();

            // Retrieve logged in contact from session
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)HttpContext.Current.Session["Contact"];
            // Returns empty list when contact is not logged in
            if (contact == null || !System.Web.HttpContext.Current.User.Identity.IsAuthenticated) return registrations;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // group registrations
                registrations = (from ger in context.Events_GroupBulkRegistration
                                 join ert in context.Events_RegistrationType on ger.RegistrationType_ID equals ert.RegistrationType_ID
                                 join ec in context.Events_Conference on ert.Conference_ID equals ec.Conference_ID
                                 join c in context.Common_GeneralCountry on ec.Country_ID equals c.Country_ID
                                 where ger.GroupLeader_ID == contact.Id && ger.Deleted == false
                                 select new CurrentRegistration
                                 {
                                     registration_name = ert.RegistrationType,
                                     registration_id = ger.GroupBulkRegistration_ID,
                                     registration_type_id = ert.RegistrationType_ID,
                                     registration_date = ger.DateAdded,
                                     event_id = ec.Conference_ID,
                                     event_name = ec.ConferenceName,
                                     venue_id = ger.Venue_ID,
                                     currency = c.Currency,
                                     cost_total = ert.RegistrationCost * ger.Quantity,
                                     quantity = ger.Quantity
                                 }).ToList();
            }

            /*HttpContext.Current.Session["registrations_group"] = registrations;*/
            return registrations;
        }

        #endregion
    }
}
