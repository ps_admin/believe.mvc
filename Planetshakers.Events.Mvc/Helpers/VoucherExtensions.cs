﻿using Planetshakers.Events.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Planetshakers.Events.Mvc.Helpers
{
    public static class VoucherExtensions
    {
        internal static readonly char[] chars =
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();

        public static string GetUniqueKey(int length)
        {
            byte[] data = new byte[4 * length];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                var rnd = BitConverter.ToUInt32(data, i * 4);
                var idx = rnd % chars.Length;

                result.Append(chars[idx]);
            }

            return result.ToString();
        }

        public static bool VoucherDuplicated(string voucherCode)
        {
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var checkVoucherDuplicate = (from ev in entity.Events_Voucher
                                             where ev.VoucherCode == voucherCode
                                             select ev);

                if (checkVoucherDuplicate.Count() == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}