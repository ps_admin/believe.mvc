﻿$('#confirmedpassword').keyup(function () {
    var newpassword = $('#newpassword').val();
    var confirmedpassword = $('#confirmedpassword').val();

    if (newpassword !== confirmedpassword) {
        $('#passwordmatchcheck').show();
        $('#password-submit').prop('disabled', true);
    }
    if (newpassword === confirmedpassword) {
        $('#passwordmatchcheck').hide();
    }
    if (newpassword.length >= 8 && newpassword == confirmedpassword) {
        $('#passwordmatchcheck').hide();
        $('#password-submit').prop('disabled', false);
    }
});

$('#newpassword').keyup(function () {
    var newpassword = $('#newpassword').val();
    var confirmedpassword = $('#confirmedpassword').val();

    if ($("#newpassword").val().length < 8) {
        $("#passwordlencheck").show();
        $('#password-submit').prop('disabled', true);
    }
    else {
        $("#passwordlencheck").hide();
    }
    if (newpassword !== confirmedpassword) {
        $('#passwordmatchcheck').show();
        $('#password-submit').prop('disabled', true);
    }
    if (newpassword === confirmedpassword) {
        $('#passwordmatchcheck').hide();
    }
    if (newpassword.length >= 8 && newpassword == confirmedpassword) {
        $('#passwordmatchcheck').hide();
        $('#password-submit').prop('disabled', false);
    }

});
