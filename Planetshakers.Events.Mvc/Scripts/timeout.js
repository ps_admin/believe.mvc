﻿var seconds = 60;
var wait_time = 30;
var currentdatetime = new Date();

window.onload = function () {

    //Only enable checking and setting of cookie if user is logged in
    if (isLoggedIn()) {
        setCookie("username");
        checkCookie();
    }

};

function checkSession() {

    $('#session_timeout').modal('show');
    $('#session_timeout').css('z-index', 2147483647);
    var stripe = $('.stripe_checkout_app');
    if (stripe != null) {
        stripe.css('z-index', 2147483646);
    }
    updateClock();
}

function isLoggedIn() {

    var loggedIn = false;

    $.ajax({
        type: "POST",
        url: "/Events/isLoggedIn",
        async: false,
        data: {},
        success: function (json) {
            loggedIn = json.loggedIn;
        },
        error: function () {
            console.log("There is error checking log in");
            return;
        }
    });

    return loggedIn;
}

// Update countdown timer
function updateClock() {

    var url = window.location.href.toLowerCase();

    if (isLoggedIn()) {
        document.getElementById("timer").innerHTML = seconds + ' seconds';
        seconds = seconds - 1;
        currentdatetime.setTime(currentdatetime.getTime() + 1000);

        if (seconds >= 0) {
            $(document).on('hide.bs.modal', '#session_timeout', function () {
                reloadWindow();
            });
            setTimeout(updateClock, 1000);
        } else {
            window.location.href = '/Account/LogOut';
        }
    }

}

function reloadWindow() {
    location.reload(true);
}

function setCookie(cname) {

    var dt = new Date();
    dt.setTime(dt.getTime() + (wait_time * 60 * 1000));
    var newCookie = cname + "=" + dt.getTime() + ";" + "expires=" + dt.toUTCString() + ";path=/";
    document.cookie = newCookie;

    $.ajax({
        type: "POST",
        url: "/Account/ExtendCookie",
        data: {},
        success: function (json) {
            return;
        },
        error: function () {
            console.log("There is error extending cookie");
            return;
        }
    });
}

function deleteCookie(cname) {

    var dt = new Date();
    dt.setTime(dt.getTime() - 100000000);
    var newCookie = cname + "=" + dt.getTime() + ";" + "expires=" + dt.toUTCString() + ";path=/";
    document.cookie = newCookie;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1);
        if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
    }
    return "";
}

function checkCookie() {

    $.ajax({
        type: "POST",
        url: "/Account/CheckSession",
        data: {},
        success: function (json) {
            if (!json.alive) {

                window.location.href = '/Account/LogOut';
                return;

            }
            else {

                currentdatetime = new Date();
                var expiryTime = getCookie("username");
                if (expiryTime - currentdatetime.getTime() > 60000) {
                    // After 1000ms, check cookie again.
                    setTimeout(checkCookie, 1000);
                    return;
                }
                else {
                    checkSession();
                    return;
                }
            }
        },
        error: function () {
            console.log("There is error checking cookie");
            return;
        }
    });
}