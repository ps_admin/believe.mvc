using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Events.Mvc.Filters;
using Planetshakers.Events.Mvc.Models;
using Planetshakers.Business.External.Events;
using System.Windows.Forms;
using System.Data.Entity.SqlServer;
using static Planetshakers.Events.Mvc.Helpers.VoucherExtensions;
using Planetshakers.Events.Mvc.Helpers;
using log4net;
using System.Security.Cryptography;

namespace Planetshakers.Events.Mvc.Controllers
{
    public class EventsController : Controller
    {
        private ILog pxlogger = LogManager.GetLogger("GeneralLogger");

        public const string MSG_USR_NOT_AUTHENTICATED = "There was an error while authenticating your e-mail / password. Please try again.";

        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            //For SSO use
            string PSCookies = Planetshakers.Business.MWebMain.PSCookieAliveValue(Request);

            if (PSCookies != string.Empty)
            {
                Planetshakers.Business.MWebMain.LogMessage("Planetshakers cookie has found in register logon.");
                LoginWithCookie(PSCookies);
            }

            var model = new EventIndexModel()
            {
                Events = Lists.RetrieveEvents(getContactID(), false).Where(x => x.visible).ToList()
            };

            if (Session["Contact"] != null)
            {
                var contact_id = getContactID();
                using (PlanetshakersEntities entity = new PlanetshakersEntities())
                {
                    model.Barcodes = (from er in entity.Events_Registration
                                      join ev in entity.Events_Venue
                                          on er.Venue_ID equals ev.Venue_ID
                                      where er.Contact_ID == contact_id && DateTime.Today <= ev.ConferenceEndDate
                                      select new Registration
                                      {
                                          Registration_ID = er.Registration_ID,
                                          Contact_ID = er.Contact_ID,
                                          Barcode = "https://www.barcodesinc.com/generator_files/image.php?code=" + SqlFunctions.StringConvert((double)er.Registration_ID).Trim() + "&style=197&type=C128B",
                                          Venue_ID = er.Venue_ID
                                      }).ToList();


                    // Retrieve all event details
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.contact_id = contact_id;

                    return View(model);
                }
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;

                return View(model);
            }

        }

        public void LoginWithCookie(string PSCookies)
        {
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                int contact_id = Convert.ToInt32(PSCookies);
                Common_Contact contact = (from cc in entity.Common_Contact where cc.Contact_ID == contact_id select cc).FirstOrDefault();

                try
                {
                    if (contact != null)
                    {
                        Common_ContactExternal ce = (from cce in entity.Common_ContactExternal
                                                     where cce.Contact_ID == contact.Contact_ID
                                                     select cce).FirstOrDefault();

                        if (ce == null)
                        {
                            // create new common_contactExternalEvents
                            //var new_contact_event = new Common_ContactExternalEvents();
                            //new_contact_event.EmergencyContactName = "";
                            //new_contact_event.EmergencyContactPhone = "";
                            //new_contact_event.EmergencyContactRelationship = "";
                            //new_contact_event.MedicalInformation = "";
                            //new_contact_event.MedicalAllergies = "";
                            //new_contact_event.AccessibilityInformation = "";
                            //new_contact_event.EventComments = "";

                            // create new external contact
                            var new_external_contact = new Common_ContactExternal();
                            new_external_contact.ChurchAddress1 = "";
                            new_external_contact.ChurchAddress2 = "";
                            new_external_contact.ChurchCountry_ID = null;
                            new_external_contact.ChurchEmail = "";
                            new_external_contact.ChurchName = "";
                            new_external_contact.ChurchNumberOfPeople = "";
                            new_external_contact.ChurchPhone = "";
                            new_external_contact.ChurchPostcode = "";
                            new_external_contact.ChurchState_ID = null;
                            new_external_contact.ChurchStateOther = "";
                            new_external_contact.ChurchSuburb = "";
                            new_external_contact.ChurchWebsite = "";
                            new_external_contact.Denomination_ID = 49;
                            new_external_contact.SeniorPastorName = "";
                            new_external_contact.LeaderName = "";
                            new_external_contact.Deleted = false;
                            new_external_contact.Inactive = false;

                            entity.Common_ContactExternal.Add(new_external_contact);

                            contact.Common_ContactExternal = new_external_contact;

                            entity.SaveChanges();
                        }

                        Authenticator.Contact c = new Authenticator.Contact();
                        c.Id = contact.Contact_ID;
                        c.FirstName = contact.FirstName;
                        c.LastName = contact.LastName;
                        c.Email = contact.Email;

                        // Store contact information on session
                        Session["Contact"] = c;
                    }
                    else
                    {
                        ViewBag.Error = MSG_USR_NOT_AUTHENTICATED;
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.Error = MSG_USR_NOT_AUTHENTICATED;
                    throw ex;
                }
            }
        }

        [PSAuthorize]
        public ActionResult Registrations(int id, string returnUrl)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {

                ViewBag.ReturnUrl = returnUrl;

                // Retrieve logged in contact from session
                var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                var model = new EventSummaryModel()
                {
                    Contact_ID = contact.Id,
                    SelectedEvent = Lists.RetrieveEvents(getContactID(), false).Where(e => e.venueID == id).First(),
                    Group = new EventGroup(contact.Id),
                    basicDetails = new BasicDetailsPartialModel(),
                    credentialDetails = new CredentialDetailsPartialModel(),
                    personalDetails = new PersonalDetailsPartialModel(),
                    contactDetails = new ContactDetailsPartialModel(),
                    churchDetails = new ChurchDetailsPartialModel(),
                    churchInvolvementDetails = new ChurchInvolvementPartialModel()
                    {
                        AvailableCInvolvement = Lists.getChurchInvolvement(null)
                    },
                    emergencyDetails = new EmergencyDetailsPartialModel(),
                    addOnModel = new AddOnModel()
                    {
                        Contact_ID = contact.Id,
                        Venue_ID = id
                    }
                };
                model.SelectedEvent.totalCost = model.SelectedEvent.totalCost + model.SelectedEvent.addOnPayments;
                model.SelectedEvent.totalPayment = model.SelectedEvent.totalPayment + model.SelectedEvent.addOnPayments;
                
                var payments = (from gp in context.Events_GroupPayment
                                join pt in context.Common_PaymentType on gp.PaymentType_ID equals pt.PaymentType_ID
                                join ps in context.Common_PaymentSource on gp.PaymentSource_ID equals ps.PaymentSource_ID
                                join cc in context.Common_Contact on gp.GroupLeader_ID equals cc.Contact_ID
                                where gp.GroupLeader_ID == contact.Id && gp.Conference_ID == model.SelectedEvent.id
                                && (gp.ExpiredIntent != true || gp.ExpiredIntent == null) && gp.PaymentCompleted == true
                                select new Payment
                                {
                                    ID = gp.GroupPayment_ID,
                                    ContactName = cc.FirstName + " " + cc.LastName,
                                    PaymentType_ID = gp.PaymentType_ID,
                                    PaymentType = pt.PaymentType,
                                    PaymentSource_ID = gp.PaymentSource_ID,
                                    PaymentSource = ps.PaymentSource,
                                    TransactionRef = gp.CCTransactionRef,
                                    Amount = gp.PaymentAmount,
                                    StartDate = gp.PaymentStartDate,
                                    CompletedDate = gp.PaymentCompletedDate,
                                    Completed = gp.PaymentCompleted,
                                    
                                }).ToList();

                model.PaymentList = payments;

                //get variable payments - sponsorship recipients
                //TODO: and donor - show everything, including names 
                model.sponsorshipList = (from v in context.Events_VariablePayment
                                         join t in context.Common_PaymentType on v.PaymentType_ID equals t.PaymentType_ID
                                         join c1 in context.Common_Contact on v.IncomingContact_ID equals c1.Contact_ID into subc1 from c1 in subc1.DefaultIfEmpty()
                                         join c2 in context.Common_Contact on v.PaymentBy_ID equals c2.Contact_ID into subc2 from c2 in subc2.DefaultIfEmpty()
                                         where v.Conference_ID == model.SelectedEvent.id
                                                && (v.OutgoingContact_ID == contact.Id/* || v.IncomingContact_ID == contact.Id*/)
                                                && v.PaymentCompleted == true
                                         select new Payment
                                         {
                                             ID = v.VariablePayment_ID,
                                             PaymentType = t.PaymentType,
                                             CompletedDate = v.PaymentDate,
                                             Amount = (decimal)v.PaymentAmount,
                                             SponsorshipStatus = (v.OutgoingContact_ID == null) ? "Donated" : "Received",
                                             PaidBy = (v.OutgoingContact_ID == null) ? c1.FirstName + " " + c1.LastName : c2.FirstName + " " + c2.LastName,
                                             Anonymous = v.Anonymous
                                         }).ToList();

                if (TempData["invitation_success"] != null)
                    ViewBag.Success = TempData["invitation_success"].ToString();
                if (TempData["invitation_error"] != null)
                    ViewBag.Error = TempData["invitation_error"].ToString();

                Response.Cache.SetNoTransforms();

                return View(model);
            }
        }

        [HttpGet]
        public PartialViewResult SomeAction()
        {
            return PartialView("UnallocatedAlert", "Shared");
        }

        [AllowAnonymous]
        public ActionResult Register(int id, string returnUrl)
        {
            var contact_id = getContactID();

            // Retrieve conference details
            List<Event> events = Lists.RetrieveEvents(contact_id, true);
            Dictionary<int, int> SumRego = new Dictionary<int, int>();

            // Check whether user has just purchased new registrations
            var recent_reg = false;
            if (TempData["recently_registered"] != null) recent_reg = (bool)TempData["recently_registered"];

            if (events.Count <= 0)
            {
                return View("ClosedRegistration");
            }
            else
            {
                if (events.Where(e => e.venueID == id).FirstOrDefault() == null)
                {
                    return View("ClosedRegistration");
                } else
                {
                    var model = new EventRegisterModel()
                    {
                        SelectedEvent = events.Where(e => e.venueID == id).First(),
                        recentlyRegistered = recent_reg,
                        Voucher = new Voucher()
                    };

                    // Check if event is full and if it is, bounce them back to home page
                    if (model.SelectedEvent.remainingRegosVenue <= 0)
                    {
                        return RedirectToAction("Index");
                    }

                    //model.promoCode = getPromoCode(contact_id);
                    //if (model.promoCode != null)
                    //    model.Voucher = GetVoucher(model.promoCode, model.SelectedEvent.id);

                    //var deposit_balance = new RegistrationType();

                    ViewBag.ReturnUrl = returnUrl;
                    return View(model);

                }
            }
        }

        //Find each group rego and single allocation and change their registration type to correspond donated rego type
        public ActionResult DonateRegistration(int GroupLeader_ID, int Venue_ID)
        {
            pxlogger.Info("Initiate Rego Donations for ContactID: " + GroupLeader_ID.ToString() + " & VenueID: " + Venue_ID.ToString());

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {

                decimal totalDonatedAmount = 0;

                var Conference_ID = (from ev in entity.Events_Venue
                                     where ev.Venue_ID == Venue_ID
                                     select ev.Conference_ID).FirstOrDefault();

                var existingRegistrationsTypes = (from rt in entity.Events_RegistrationType
                                                  where rt.Conference_ID == Conference_ID
                                                  select rt).ToList();

                //For single rego use
                var existingSingleRego = (from er in entity.Events_Registration
                                          where er.Venue_ID == Venue_ID && er.Contact_ID == GroupLeader_ID && er.GroupLeader_ID == null
                                          select er).FirstOrDefault();

                if (existingSingleRego != null)
                {
                    totalDonatedAmount += existingRegistrationsTypes.Find(x => x.RegistrationType_ID == existingSingleRego.RegistrationType_ID).RegistrationCost;

                    var regoTypeName = FindRegistrationTypeName(existingSingleRego.RegistrationType_ID);
                    var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("DONATED"));
                    existingSingleRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                    entity.SaveChanges();
                }
                else
                {
                    //For group rego use
                    var existingGroupRego = (from gr in entity.Events_GroupBulkRegistration
                                             where gr.GroupLeader_ID == GroupLeader_ID && gr.Venue_ID == Venue_ID && gr.Deleted == false
                                             select gr).ToList();

                    var existingAllocatedRego = (from er in entity.Events_Registration
                                                 where er.Venue_ID == Venue_ID && er.GroupLeader_ID == GroupLeader_ID
                                                 select er).ToList();

                    foreach (var groupRego in existingGroupRego)
                    {
                        totalDonatedAmount += groupRego.Quantity * existingRegistrationsTypes.Find(x => x.RegistrationType_ID == groupRego.RegistrationType_ID).RegistrationCost;

                        var regoTypeName = FindRegistrationTypeName(groupRego.RegistrationType_ID);
                        var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("DONATED"));
                        groupRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                        entity.SaveChanges();
                    }

                    foreach (var allocatedRego in existingAllocatedRego)
                    {
                        var regoTypeName = FindRegistrationTypeName(allocatedRego.RegistrationType_ID);
                        var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("DONATED"));
                        allocatedRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                        entity.SaveChanges();
                    }
                }

                //Send donated confirmation email

                DonatedEmailModel dynamicTemplate = new DonatedEmailModel();
                var contact = (from cc in entity.Common_Contact
                               where cc.Contact_ID == GroupLeader_ID
                               select cc).FirstOrDefault();

                dynamicTemplate.Currency = (from ec in entity.Events_Conference
                                            join ba in entity.Common_BankAccount
                                            on ec.BankAccount_ID equals ba.BankAccount_ID
                                            where ec.Conference_ID == Conference_ID
                                            select ba.Currency).FirstOrDefault();
                dynamicTemplate.Email = contact.Email;
                dynamicTemplate.FullName = contact.FirstName + " " + contact.LastName;
                dynamicTemplate.TotalAmount = totalDonatedAmount.ToString("0.00");

                new MailController().SendDonatedConfirmationEmail(dynamicTemplate);
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        //Find each group rego and single allocation and change their registration type to correspond credit rego type
        public ActionResult CreditRegistration(int GroupLeader_ID, int Venue_ID)
        {
            pxlogger.Info("Initiate Rego Credits for ContactID: " + GroupLeader_ID.ToString() + " & VenueID: " + Venue_ID.ToString());

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                decimal totalCreditAmount = 0;


                var Conference_ID = (from ev in entity.Events_Venue
                                     where ev.Venue_ID == Venue_ID
                                     select ev.Conference_ID).FirstOrDefault();

                var existingRegistrationsTypes = (from rt in entity.Events_RegistrationType
                                                  where rt.Conference_ID == Conference_ID
                                                  select rt).ToList();

                //For single rego use
                var existingSingleRego = (from er in entity.Events_Registration
                                          where er.Venue_ID == Venue_ID && er.Contact_ID == GroupLeader_ID && er.GroupLeader_ID == null
                                          select er).FirstOrDefault();

                if (existingSingleRego != null)
                {
                    totalCreditAmount += existingRegistrationsTypes.Find(x => x.RegistrationType_ID == existingSingleRego.RegistrationType_ID).RegistrationCost;

                    var regoTypeName = FindRegistrationTypeName(existingSingleRego.RegistrationType_ID);
                    var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("CREDITED"));
                    existingSingleRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                    entity.SaveChanges();
                }
                else
                {
                    //For group rego use
                    var existingGroupRego = (from gr in entity.Events_GroupBulkRegistration
                                             where gr.GroupLeader_ID == GroupLeader_ID && gr.Venue_ID == Venue_ID && gr.Deleted == false
                                             select gr).ToList();

                    var existingAllocatedRego = (from er in entity.Events_Registration
                                                 where er.Venue_ID == Venue_ID && er.GroupLeader_ID == GroupLeader_ID
                                                 select er).ToList();

                    foreach (var groupRego in existingGroupRego)
                    {
                        totalCreditAmount += groupRego.Quantity * existingRegistrationsTypes.Find(x => x.RegistrationType_ID == groupRego.RegistrationType_ID).RegistrationCost;

                        var regoTypeName = FindRegistrationTypeName(groupRego.RegistrationType_ID);
                        var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("CREDITED"));
                        groupRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                        entity.SaveChanges();
                    }

                    foreach (var allocatedRego in existingAllocatedRego)
                    {
                        var regoTypeName = FindRegistrationTypeName(allocatedRego.RegistrationType_ID);
                        var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("CREDITED"));
                        allocatedRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                        entity.SaveChanges();
                    }
                }

                //Send credited confirmation email
                CreditedEmailModel dynamicTemplate = new CreditedEmailModel();
                var contact = (from cc in entity.Common_Contact
                               where cc.Contact_ID == GroupLeader_ID
                               select cc).FirstOrDefault();

                // Create voucher code 
                var voucher = createVoucher(GroupLeader_ID, totalCreditAmount, null);

                pxlogger.Info("Rego Credits Voucher Code generated: " + voucher.code + " for ContactID: " + GroupLeader_ID.ToString() + " & VenueID: " + Venue_ID.ToString());

                // Send confirmation email
                dynamicTemplate.Currency = (from ec in entity.Events_Conference
                                            join ba in entity.Common_BankAccount
                                            on ec.BankAccount_ID equals ba.BankAccount_ID
                                            where ec.Conference_ID == Conference_ID
                                            select ba.Currency).FirstOrDefault();
                dynamicTemplate.Email = contact.Email;
                dynamicTemplate.FullName = contact.FirstName + " " + contact.LastName;
                dynamicTemplate.TotalAmount = totalCreditAmount.ToString("0.00");
                dynamicTemplate.VoucherCode = voucher.code;

                new MailController().SendCreditedConfirmationEmail(dynamicTemplate);
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public Voucher createVoucher(int contact_id, decimal totalCreditAmount, int? predecessor_id)
        {
            DateTime today = DateTime.Now;
            DateTime year20 = DateTime.Now.AddYears(20);

            using (var entity = new PlanetshakersEntities())
            {
                var contact = (from cc in entity.Common_Contact
                               where cc.Contact_ID == contact_id
                               select cc).FirstOrDefault();

                string voucherCode = GetUniqueKey(9);

                // Safety check for voucher code duplication. Will regenerate if a duplication is detected.
                while (VoucherDuplicated(voucherCode))
                {
                    voucherCode = GetUniqueKey(9);
                }

                // Save the voucher
                Events_Voucher events_Voucher = new Events_Voucher
                {
                    VoucherType_ID = 1,
                    VoucherCode = voucherCode,
                    VoucherValue = totalCreditAmount,
                    ValueIsPercentage = false,
                    Contact_ID = null,
                    CurrentUses = 0,
                    ValidStartDT = today,
                    ExpiryDT = year20,
                    VoucherIssuedDT = today,
                    ModifiedBy = contact_id,
                    ModifiedDT = today,
                    Inactive = false,
                    Deleted = false,
                    PredecessorVoucher_ID = (predecessor_id)
                };

                entity.Events_Voucher.Add(events_Voucher);
                entity.SaveChanges();

                var voucher = new Voucher
                {
                    code = voucherCode,
                    value = totalCreditAmount,
                    contact_id = contact_id
                };

                return voucher;
            }
        }

        //Find each group rego and single allocation and change their registration type to correspond donated rego type
        public ActionResult DonateRegistrationBNE(int GroupLeader_ID, int Venue_ID)
        {
            pxlogger.Info("Initiate Rego Donations for ContactID: " + GroupLeader_ID.ToString() + " & VenueID: " + Venue_ID.ToString());

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {

                decimal totalDonatedAmount = 0;

                var Conference_ID = (from ev in entity.Events_Venue
                                     where ev.Venue_ID == Venue_ID
                                     select ev.Conference_ID).FirstOrDefault();

                var existingRegistrationsTypes = (from rt in entity.Events_RegistrationType
                                                  where rt.Conference_ID == Conference_ID
                                                  select rt).ToList();

                //For single rego use
                var existingSingleRego = (from er in entity.Events_Registration
                                          where er.Venue_ID == Venue_ID && er.Contact_ID == GroupLeader_ID && er.GroupLeader_ID == null
                                          select er).FirstOrDefault();



                if (existingSingleRego != null)
                {
                    totalDonatedAmount += existingRegistrationsTypes.Find(x => x.RegistrationType_ID == existingSingleRego.RegistrationType_ID).RegistrationCost;

                    var regoTypeName = FindRegistrationTypeName(existingSingleRego.RegistrationType_ID);
                    var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("DONATED"));
                    existingSingleRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                    entity.SaveChanges();
                }
                else
                {
                    //For group rego use
                    var existingGroupRego = (from gr in entity.Events_GroupBulkRegistration
                                             where gr.GroupLeader_ID == GroupLeader_ID && gr.Venue_ID == Venue_ID && gr.Deleted == false
                                             select gr).ToList();

                    var existingAllocatedRego = (from er in entity.Events_Registration
                                                 where er.Venue_ID == Venue_ID && er.GroupLeader_ID == GroupLeader_ID
                                                 select er).ToList();

                    foreach (var groupRego in existingGroupRego)
                    {
                        totalDonatedAmount += groupRego.Quantity * existingRegistrationsTypes.Find(x => x.RegistrationType_ID == groupRego.RegistrationType_ID).RegistrationCost;

                        var regoTypeName = FindRegistrationTypeName(groupRego.RegistrationType_ID);
                        var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("DONATED"));
                        groupRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                        entity.SaveChanges();
                    }

                    foreach (var allocatedRego in existingAllocatedRego)
                    {
                        var regoTypeName = FindRegistrationTypeName(allocatedRego.RegistrationType_ID);
                        var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("DONATED"));
                        allocatedRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                        entity.SaveChanges();
                    }
                }

                //Send donated confirmation email

                DonatedEmailModel dynamicTemplate = new DonatedEmailModel();
                var contact = (from cc in entity.Common_Contact
                               where cc.Contact_ID == GroupLeader_ID
                               select cc).FirstOrDefault();

                dynamicTemplate.Currency = (from ec in entity.Events_Conference
                                            join ba in entity.Common_BankAccount
                                            on ec.BankAccount_ID equals ba.BankAccount_ID
                                            where ec.Conference_ID == Conference_ID
                                            select ba.Currency).FirstOrDefault();
                dynamicTemplate.Email = contact.Email;
                dynamicTemplate.FullName = contact.FirstName + " " + contact.LastName;
                dynamicTemplate.TotalAmount = totalDonatedAmount.ToString("0.00");

                new MailController().SendDonatedConfirmationEmail(dynamicTemplate);
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        //Find each group rego and single allocation and change their registration type to correspond credit rego type
        public ActionResult CreditRegistrationBNE(int GroupLeader_ID, int Venue_ID)
        {
            pxlogger.Info("Initiate Rego Credits for ContactID: " + GroupLeader_ID.ToString() + " & VenueID: " + Venue_ID.ToString());

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                decimal totalCreditAmount = 0;
                DateTime today = DateTime.Now;
                DateTime year20 = DateTime.Now.AddYears(20);

                var Conference_ID = (from ev in entity.Events_Venue
                                     where ev.Venue_ID == Venue_ID
                                     select ev.Conference_ID).FirstOrDefault();

                var existingRegistrationsTypes = (from rt in entity.Events_RegistrationType
                                                  where rt.Conference_ID == Conference_ID
                                                  select rt).ToList();

                //For single rego use
                var existingSingleRego = (from er in entity.Events_Registration
                                          where er.Venue_ID == Venue_ID && er.Contact_ID == GroupLeader_ID && er.GroupLeader_ID == null
                                          select er).FirstOrDefault();

                if (existingSingleRego != null)
                {
                    totalCreditAmount += existingRegistrationsTypes.Find(x => x.RegistrationType_ID == existingSingleRego.RegistrationType_ID).RegistrationCost;

                    var regoTypeName = FindRegistrationTypeName(existingSingleRego.RegistrationType_ID);
                    var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("CREDITED"));
                    existingSingleRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                    entity.SaveChanges();
                }
                else
                {
                    //For group rego use
                    var existingGroupRego = (from gr in entity.Events_GroupBulkRegistration
                                             where gr.GroupLeader_ID == GroupLeader_ID && gr.Venue_ID == Venue_ID && gr.Deleted == false
                                             select gr).ToList();

                    var existingAllocatedRego = (from er in entity.Events_Registration
                                                 where er.Venue_ID == Venue_ID && er.GroupLeader_ID == GroupLeader_ID
                                                 select er).ToList();

                    foreach (var groupRego in existingGroupRego)
                    {
                        totalCreditAmount += groupRego.Quantity * existingRegistrationsTypes.Find(x => x.RegistrationType_ID == groupRego.RegistrationType_ID).RegistrationCost;

                        var regoTypeName = FindRegistrationTypeName(groupRego.RegistrationType_ID);
                        var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("CREDITED"));
                        groupRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                        entity.SaveChanges();
                    }

                    foreach (var allocatedRego in existingAllocatedRego)
                    {
                        var regoTypeName = FindRegistrationTypeName(allocatedRego.RegistrationType_ID);
                        var newRegoType = existingRegistrationsTypes.Find(x => x.RegistrationType.Contains(regoTypeName) && x.RegistrationType.Contains("CREDITED"));
                        allocatedRego.RegistrationType_ID = newRegoType.RegistrationType_ID;
                        entity.SaveChanges();
                    }
                }

                //Send credited confirmation email

                CreditedEmailModel dynamicTemplate = new CreditedEmailModel();
                var contact = (from cc in entity.Common_Contact
                               where cc.Contact_ID == GroupLeader_ID
                               select cc).FirstOrDefault();

                string voucherCode = GetUniqueKey(9);

                // Safety check for voucher code duplication. Will regenerate if a duplication is detected.
                while (VoucherDuplicated(voucherCode))
                {
                    voucherCode = GetUniqueKey(9);
                }

                pxlogger.Info("Rego Credits Voucher Code generated: " + voucherCode + " for ContactID: " + GroupLeader_ID.ToString() + " & VenueID: " + Venue_ID.ToString());

                // Save the voucher
                Events_Voucher events_Voucher = new Events_Voucher
                {
                    VoucherType_ID = 1,
                    VoucherCode = voucherCode,
                    VoucherValue = totalCreditAmount,
                    ValueIsPercentage = false,
                    CurrentUses = 0,
                    ValidStartDT = today,
                    ExpiryDT = year20,
                    VoucherIssuedDT = today,
                    ModifiedBy = GroupLeader_ID,
                    ModifiedDT = today,
                    Inactive = false,
                    Deleted = false
                };

                entity.Events_Voucher.Add(events_Voucher);
                entity.SaveChanges();

                // Send confirmation email

                dynamicTemplate.Currency = (from ec in entity.Events_Conference
                                            join ba in entity.Common_BankAccount
                                            on ec.BankAccount_ID equals ba.BankAccount_ID
                                            where ec.Conference_ID == Conference_ID
                                            select ba.Currency).FirstOrDefault();
                dynamicTemplate.Email = contact.Email;
                dynamicTemplate.FullName = contact.FirstName + " " + contact.LastName;
                dynamicTemplate.TotalAmount = totalCreditAmount.ToString("0.00");
                dynamicTemplate.VoucherCode = voucherCode;

                new MailController().SendCreditedConfirmationEmail(dynamicTemplate);
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        //Find registration type name given id
        public string FindRegistrationTypeName(int RegistrationType_ID)
        {
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var regoTypeName = (from er in entity.Events_RegistrationType
                                    where er.RegistrationType_ID == RegistrationType_ID
                                    select er.RegistrationType).FirstOrDefault();

                return regoTypeName;
            }
        }

        [HttpPost]
        public ActionResult TermsAndConditions(int Event_ID)
        {
            var content = "";
            var fileName = "TnC_" + Event_ID.ToString() + ".md";
            FileModel fileModel = new FileModel("s3-amazon-believe-general");
            var file = fileModel.GetFile("TermsAndConditions", fileName);
            if (file.DownloadUrl != null)
            {
                content = WebOperations.ParseUrlToString(file.DownloadUrl);
                content = WebOperations.ParseMarkdown(content);
            }
            return Json(new { content = content });
        }

        #region AJAX/Json calls

        // Registers for an event, and redirects to the summary page.
        public ActionResult SubmitRegistration(int RegistrationType_ID, int group_ID, int venue_id, string returnUrl)
        {
            // Retrieve conference details
            List<Event> events = Lists.RetrieveEvents(getContactID(), true);

            var selectedEvent = events.Where(e => e.venueID == venue_id).First();

            // get logged in user
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

            var regGroup = selectedEvent.rego_categories.Where(c => c.group_id == group_ID).First();

            var cart = new List<CartItem>();
            var regType = new RegistrationType();
            if (isAddOnRegistrationType(RegistrationType_ID))
            {
                regType = getRegoType(RegistrationType_ID);
            } else
            {
                regType = regGroup.rego_types.Where(rt => rt.id == RegistrationType_ID).First();
            }
            

            // if registration type not found.  Request is invalid, return error
            if (regType == null)
                return Json(new { isSuccess = false, value = "Invalid registration submitted, please try again" });

            cart.Add(new CartItem
            {
                reg_name = regType.name,
                reg_id = regType.id,
                event_id = selectedEvent.id,
                event_venue_id = selectedEvent.venueID,
                event_name = selectedEvent.name,
                quantity = 1,
                single_price = regType.cost
            });


            // At this point, registration is valid.
            var model = new CartModel()
            {
                Event = selectedEvent,
                Cart = cart,
            };

            Session["Cart"] = model;

            // create redirect here
            // if contact is null, redirect to login prompt
            // if contact is NOT null, redirect to summary page straight
            return Json(new { isSuccess = true, value = Url.Action("LoginPrompt", "Purchase", new { returnUrl = returnUrl }) });
        }

        //public ActionResult SubmitSponsorship(int venue_id, decimal amount, string returnUrl)
        public ActionResult SubmitSponsorship(int Contact_ID, int Event_ID, string FirstName, string LastName, string Mobile, string Email, bool Anonymous, decimal sponsoringAmount, string returnUrl)
        {
            pxlogger.Info("Sponsorship Trigger");
            //check balance to avoid over paid
            var canSponsor = checkBalance(Contact_ID, Event_ID, sponsoringAmount);
            if (canSponsor)
            {
                List<Event> events = Lists.RetrieveEvents(Contact_ID, false);

                var selectedEvent = events.Where(e => e.id == Event_ID).First();

                var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                //Creating new cart item to put into model so afterwards can pass to session
                var cart = new SponsorshipCartItem
                {
                    event_id = selectedEvent.id,
                    event_venue_id = selectedEvent.venueID,
                    event_name = selectedEvent.name,
                    totalSponsoredAmount = sponsoringAmount,
                    Anonymous = Anonymous
                };
                var model = new SponsorshipCartModel()
                {
                    Event = selectedEvent,
                    Cart = cart,
                    FirstName = FirstName,
                    LastName = LastName,
                    Mobile = Mobile,
                    Email = Email,
                    SponsoringToContactID = Contact_ID
                };
                Session["SponsorshipCart"] = model;
                return Json(new { isSuccess = true, value = Url.Action("LoginSponsorshipPrompt", "Purchase", new { returnUrl = returnUrl }) });
            }
            else
            {
                return Json(new { isSuccess = false });
            }
        }
        [HttpPost]
        public bool checkBalance(int Contact_ID, int Event_ID, decimal Amount = 0)
        {
            using(PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var payable = false;
                var addOnRegoTypeCost = (from r in context.Events_GroupBulkRegistration
                                         join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                         where r.GroupLeader_ID == Contact_ID
                                                && rt.Conference_ID == Event_ID
                                                && r.Deleted == false
                                                && rt.AddOnRegistrationType == true
                                         select r.Quantity * rt.RegistrationCost).DefaultIfEmpty().Sum();

                var Cost = (from r in context.Events_GroupBulkRegistration
                                 join t in context.Events_RegistrationType on r.RegistrationType_ID equals t.RegistrationType_ID
                                 where r.GroupLeader_ID == Contact_ID
                                     && t.Conference_ID == Event_ID
                                     && r.Deleted == false
                                 select r.Quantity * t.RegistrationCost).DefaultIfEmpty().Sum() - addOnRegoTypeCost;

                var totalPaid = (from p in context.Events_GroupPayment
                                 where p.GroupLeader_ID == Contact_ID
                                     && p.Conference_ID == Event_ID
                                     && p.PaymentCompleted == true
                                     && (p.ExpiredIntent == false || p.ExpiredIntent == null)
                                 select p.PaymentAmount).DefaultIfEmpty().Sum() - addOnRegoTypeCost;

                var sponsorRecipient = (from v in context.Events_VariablePayment
                                        where v.Conference_ID == Event_ID
                                            && v.OutgoingContact_ID == Contact_ID
                                            && v.IncomingContact_ID == null
                                            && v.PaymentCompleted == true
                                        select v.PaymentAmount).ToList().Sum();
                var sponsorDonor = (from v in context.Events_VariablePayment
                                    where v.Conference_ID == Event_ID
                                        && v.OutgoingContact_ID == null
                                        && v.IncomingContact_ID == Contact_ID
                                    select v.PaymentAmount).ToList().Sum();

                var outstandingBalance = Math.Round((double)Cost - (double)totalPaid - (double)sponsorRecipient, 2);
                outstandingBalance = (Amount != 0) ? outstandingBalance - (double)Amount : outstandingBalance;
                payable = (outstandingBalance >= 0) ? true : false;
                return payable;
            }            
        }
        #endregion

        public int getContactID()
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            if (contact != null)
            {
                return contact.Id;
            }
            else
            {
                return 0;
            }
        }

        public bool isAddOnRegistrationType(int RegoType_ID)
        {
            using (var context = new PlanetshakersEntities())
            {
                var isAddOn = (from rt in context.Events_RegistrationType
                               where rt.RegistrationType_ID == RegoType_ID && rt.AddOnRegistrationType == true
                               select rt).Any();
                return isAddOn;
            }
        }

        public RegistrationType getRegoType(int RegoType_ID)
        {
            using (var context = new PlanetshakersEntities())
            {
                var rego = (from rt in context.Events_RegistrationType
                            where rt.RegistrationType_ID == RegoType_ID
                            select new RegistrationType
                            {
                                name = rt.RegistrationType,
                                id = rt.RegistrationType_ID,
                                cost = rt.RegistrationCost
                            }).FirstOrDefault();
                return rego;
            }
        }

        public string getPromoCode(int contact_id)
        {
            var now = DateTime.Now;
            using (var context = new PlanetshakersEntities())
            {
                var promo = (from vc in context.Events_Voucher
                             join vt in context.Events_VoucherType
                                on vc.VoucherType_ID equals vt.VoucherType_ID
                             where (vc.ModifiedBy == contact_id)
                             && vc.CurrentUses < vt.MaxUses
                             && vc.ValidStartDT < now
                             && now < vc.ExpiryDT
                             && !vc.Inactive
                             && !vc.Deleted
                             select vc.VoucherCode).FirstOrDefault();

                return promo;
            }
        }

        public decimal GetVoucherValue(string code, int conference_id)
        {
            var now = DateTime.Now;
            decimal discount = 0;
            using (var context = new PlanetshakersEntities())
            {
                var voucher = (from vc in context.Events_Voucher
                               join vt in context.Events_VoucherType
                                   on vc.VoucherType_ID equals vt.VoucherType_ID
                               where vc.VoucherCode == code
                               && vc.CurrentUses < vt.MaxUses
                               && vc.ValidStartDT < now
                               && now < vc.ExpiryDT
                               && !vc.Inactive
                               && !vc.Deleted
                               select vc).FirstOrDefault();

                var scope = (from vs in context.Events_VoucherScope
                             where vs.Voucher_ID == voucher.Voucher_ID
                             select vs).ToList();

                if (scope.Count > 0)
                {
                    var inScope = scope.Where(v => v.Conference_ID == conference_id).Any();

                    if (!inScope)
                        discount = -1;
                    else
                        discount = (decimal)voucher.VoucherValue;
                }
                else
                {
                    discount = (decimal)voucher.VoucherValue;
                }

                return discount;
            }
        }

        public Voucher GetVoucher(string code, int conferenceID)
        {
            var now = DateTime.Now;
            using (var context = new PlanetshakersEntities())
            {
                var voucher = (from vc in context.Events_Voucher
                               join vt in context.Events_VoucherType
                                   on vc.VoucherType_ID equals vt.VoucherType_ID
                               where vc.VoucherCode == code
                               && vc.CurrentUses < vt.MaxUses
                               && vc.ValidStartDT < now
                               && now < vc.ExpiryDT
                               && !vc.Inactive
                               && !vc.Deleted
                               select new Voucher
                               {
                                   id = vc.Voucher_ID,
                                   code = vc.VoucherCode,
                                   value = (decimal)vc.VoucherValue
                               }).FirstOrDefault();

                if (voucher != null)
                {
                    var scope = (from vs in context.Events_VoucherScope
                                 where vs.Voucher_ID == voucher.id
                                 select vs).ToList();

                    if (scope.Count > 0)
                    {
                        var inScope = scope.Where(v => v.Conference_ID == conferenceID).Any();

                        if (!inScope)
                            voucher = null;
                    }
                }

                return voucher;
            }
        }

        public ActionResult isLoggedIn()
        {
            var loggedIn = getContactID() != 0;

            return Json(new { loggedIn }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TakeAction()
        {
            var contact_id = getContactID();

            var rt_ids = new List<int>();


            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                rt_ids = (from ert in entity.Events_RegistrationType
                          where ert.RegistrationType_ID <= 3026 && ert.Conference_ID == 128
                          select ert.RegistrationType_ID).ToList();

                var eventList = (from er in entity.Events_Registration
                                 where rt_ids.Contains(er.RegistrationType_ID) && er.Contact_ID == contact_id && er.Venue_ID == 156 && er.GroupLeader_ID == null
                                 select er).Any();

                var bulkList = (from br in entity.Events_GroupBulkRegistration
                                where rt_ids.Contains(br.RegistrationType_ID) && br.GroupLeader_ID == contact_id && br.Venue_ID == 156
                                select br).Any();


                if ((eventList || bulkList) && contact_id != 0)
                {
                    return Json(new { actionRequired = true }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { actionRequired = false }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult TransferRegos(int contact_id, int new_venue_id, int venue_id)
        {
            var conferenceName = "";
            using (var ctx = new PlanetshakersEntities())
            {
                var conf_id = (from v in ctx.Events_Venue
                               where v.Venue_ID == venue_id
                               select v.Conference_ID).FirstOrDefault();

                var new_conf_id = (from v in ctx.Events_Venue
                                   where v.Venue_ID == new_venue_id
                                   select v.Conference_ID).FirstOrDefault();

                var list_br = (from br in ctx.Events_GroupBulkRegistration
                               where br.GroupLeader_ID == contact_id && br.Venue_ID == venue_id && br.Deleted == false
                               select br).ToList();

                foreach (var br in list_br)
                {
                    var regotype_id_tb = 0;
                    #region Bulk Rego Switch Cases
                    if (new_venue_id == 171)
                    {
                        // switch cases for PS22 MELB - 1
                        switch (br.RegistrationType_ID)
                        {
                            case (3188): // Adult - Super Early Bird
                                regotype_id_tb = 3423;
                                break;
                            case (3189): // Adult - Early Bird
                                regotype_id_tb = 3424;
                                break;
                            case (3190): // Adult - Full Registration
                                regotype_id_tb = 3425;
                                break;
                            case (3194): // PK - Early Childhood
                                regotype_id_tb = 3427;
                                break;
                            case (3195): // PK - Primary
                                regotype_id_tb = 3428;
                                break;
                            case (3196): // PK - Special Needs
                                regotype_id_tb = 3429;
                                break;
                            case (3199): // PK - Early Childhood FAMILY SEB
                                regotype_id_tb = 3463;
                                break;
                            case (3202): // PK - Early Childhood FAMILY EB
                                regotype_id_tb = 3464;
                                break;
                            case (3203): // PK - Early Childhood FAMILY FR
                                regotype_id_tb = 3465;
                                break;
                            case (3204): // PK - Primary FAMILY SEB
                                regotype_id_tb = 3466;
                                break;
                            case (3198): // PK - Primary FAMILY EB
                                regotype_id_tb = 3467;
                                break;
                            case (3205): // PK - Primary FAMILY FR
                                regotype_id_tb = 3468;
                                break;
                            case (3206): // PK - Special Needs FAMILY SEB
                                regotype_id_tb = 3469;
                                break;
                            case (3207): // PK - Special Needs FAMILY EB
                                regotype_id_tb = 3470;
                                break;
                            case (3197): // PK - Special Needs FAMILY FR
                                regotype_id_tb = 3471;
                                break;
                            case (3363): // PK - Babies
                                regotype_id_tb = 3426;
                                break;
                            case (3213): // Adult - FAMILY SEB
                                regotype_id_tb = 3472;
                                break;
                            case (3214): // Adult - FAMILY EB
                                regotype_id_tb = 3473;
                                break;
                            case (3215): // Adult - FAMILY FR
                                regotype_id_tb = 3474;
                                break;
                            case (3308): // Adult - EB COMBO
                                regotype_id_tb = 3579;
                                break;
                            default:
                                break;
                        }
                    }
                    else if (new_venue_id == 172)
                    {
                        // switch cases for  PS22 MELB - 2
                        switch (br.RegistrationType_ID)
                        {
                            case (3188): // Adult - Super Early Bird
                                regotype_id_tb = 3371;
                                break;
                            case (3189): // Adult - Early Bird
                                regotype_id_tb = 3372;
                                break;
                            case (3190): // Adult - Full Registration
                                regotype_id_tb = 3373;
                                break;
                            case (3194): // PK - Early Childhood
                                regotype_id_tb = 3375;
                                break;
                            case (3195): // PK - Primary
                                regotype_id_tb = 3376;
                                break;
                            case (3196): // PK - Special Needs
                                regotype_id_tb = 3377;
                                break;
                            case (3199): // PK - Early Childhood FAMILY SEB
                                regotype_id_tb = 3411;
                                break;
                            case (3202): // PK - Early Childhood FAMILY EB
                                regotype_id_tb = 3412;
                                break;
                            case (3203): // PK - Early Childhood FAMILY FR
                                regotype_id_tb = 3413;
                                break;
                            case (3204): // PK - Primary FAMILY SEB
                                regotype_id_tb = 3408;
                                break;
                            case (3198): // PK - Primary FAMILY EB
                                regotype_id_tb = 3409;
                                break;
                            case (3205): // PK - Primary FAMILY FR
                                regotype_id_tb = 3410;
                                break;
                            case (3206): // PK - Special Needs FAMILY SEB
                                regotype_id_tb = 3414;
                                break;
                            case (3207): // PK - Special Needs FAMILY EB
                                regotype_id_tb = 3415;
                                break;
                            case (3197): // PK - Special Needs FAMILY FR
                                regotype_id_tb = 3416;
                                break;
                            case (3363): // PK - Babies
                                regotype_id_tb = 3374;
                                break;
                            case (3213): // Adult - FAMILY SEB
                                regotype_id_tb = 3420;
                                break;
                            case (3214): // Adult - FAMILY EB
                                regotype_id_tb = 3421;
                                break;
                            case (3215): // Adult - FAMILY FR
                                regotype_id_tb = 3422;
                                break;
                            case (3308): // Adult - EB COMBO
                                regotype_id_tb = 3580;
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        // exit error

                    }
                    #endregion

                    // transfer bulk purchased regos

                    var new_br = new Events_GroupBulkRegistration();
                    new_br.GroupLeader_ID = br.GroupLeader_ID;
                    new_br.Venue_ID = new_venue_id;
                    new_br.RegistrationType_ID = regotype_id_tb;
                    new_br.Quantity = br.Quantity;
                    new_br.DateAdded = DateTime.Now;
                    new_br.Deleted = false;

                    ctx.Events_GroupBulkRegistration.Add(new_br);

                    br.Deleted = true;
                }

                var list_r = (from r in ctx.Events_Registration
                              where r.GroupLeader_ID == contact_id && r.Venue_ID == venue_id
                              select r).ToList();

                foreach (var r in list_r)
                {
                    var regotype_id_tb = 0;
                    #region Rego Allocation Switch Cases
                    if (new_venue_id == 171)
                    {
                        // switch cases for PS22 MELB - 1
                        switch (r.RegistrationType_ID)
                        {
                            case (3188): // Adult - Super Early Bird
                                regotype_id_tb = 3423;
                                break;
                            case (3189): // Adult - Early Bird
                                regotype_id_tb = 3424;
                                break;
                            case (3190): // Adult - Full Registration
                                regotype_id_tb = 3425;
                                break;
                            case (3194): // PK - Early Childhood
                                regotype_id_tb = 3427;
                                break;
                            case (3195): // PK - Primary
                                regotype_id_tb = 3428;
                                break;
                            case (3196): // PK - Special Needs
                                regotype_id_tb = 3429;
                                break;
                            case (3199): // PK - Early Childhood FAMILY SEB
                                regotype_id_tb = 3463;
                                break;
                            case (3202): // PK - Early Childhood FAMILY EB
                                regotype_id_tb = 3464;
                                break;
                            case (3203): // PK - Early Childhood FAMILY FR
                                regotype_id_tb = 3465;
                                break;
                            case (3204): // PK - Primary FAMILY SEB
                                regotype_id_tb = 3466;
                                break;
                            case (3198): // PK - Primary FAMILY EB
                                regotype_id_tb = 3467;
                                break;
                            case (3205): // PK - Primary FAMILY FR
                                regotype_id_tb = 3468;
                                break;
                            case (3206): // PK - Special Needs FAMILY SEB
                                regotype_id_tb = 3469;
                                break;
                            case (3207): // PK - Special Needs FAMILY EB
                                regotype_id_tb = 3470;
                                break;
                            case (3197): // PK - Special Needs FAMILY FR
                                regotype_id_tb = 3471;
                                break;
                            case (3213): // Adult - FAMILY SEB
                                regotype_id_tb = 3472;
                                break;
                            case (3214): // Adult - FAMILY EB
                                regotype_id_tb = 3473;
                                break;
                            case (3215): // Adult - FAMILY FR
                                regotype_id_tb = 3474;
                                break;
                            case (3308): // Adult - EB COMBO
                                regotype_id_tb = 3579;
                                break;
                            default:
                                break;
                        }
                    }
                    else if (new_venue_id == 172)
                    {
                        // switch cases for  PS22 MELB - 2
                        switch (r.RegistrationType_ID)
                        {
                            case (3188): // Adult - Super Early Bird
                                regotype_id_tb = 3371;
                                break;
                            case (3189): // Adult - Early Bird
                                regotype_id_tb = 3372;
                                break;
                            case (3190): // Adult - Full Registration
                                regotype_id_tb = 3373;
                                break;
                            case (3194): // PK - Early Childhood
                                regotype_id_tb = 3375;
                                break;
                            case (3195): // PK - Primary
                                regotype_id_tb = 3376;
                                break;
                            case (3196): // PK - Special Needs
                                regotype_id_tb = 3377;
                                break;
                            case (3199): // PK - Early Childhood FAMILY SEB
                                regotype_id_tb = 3411;
                                break;
                            case (3202): // PK - Early Childhood FAMILY EB
                                regotype_id_tb = 3412;
                                break;
                            case (3203): // PK - Early Childhood FAMILY FR
                                regotype_id_tb = 3413;
                                break;
                            case (3204): // PK - Primary FAMILY SEB
                                regotype_id_tb = 3408;
                                break;
                            case (3198): // PK - Primary FAMILY EB
                                regotype_id_tb = 3409;
                                break;
                            case (3205): // PK - Primary FAMILY FR
                                regotype_id_tb = 3410;
                                break;
                            case (3206): // PK - Special Needs FAMILY SEB
                                regotype_id_tb = 3414;
                                break;
                            case (3207): // PK - Special Needs FAMILY EB
                                regotype_id_tb = 3415;
                                break;
                            case (3197): // PK - Special Needs FAMILY FR
                                regotype_id_tb = 3416;
                                break;
                            case (3213): // Adult - FAMILY SEB
                                regotype_id_tb = 3420;
                                break;
                            case (3214): // Adult - FAMILY EB
                                regotype_id_tb = 3421;
                                break;
                            case (3215): // Adult - FAMILY FR
                                regotype_id_tb = 3422;
                                break;
                            case (3308): // Adult - EB COMBO
                                regotype_id_tb = 3580;
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        // exit error
                    }
                    #endregion

                    var check_child_rego = (from ce in ctx.Events_RegistrationChildEnrolment
                                            where ce.Registration_ID == r.Registration_ID
                                            select ce).ToList();

                    var check_child_info = (from ci in ctx.Events_RegistrationChildInformation
                                            where ci.Registration_ID == r.Registration_ID
                                            select ci).ToList();

                    if (check_child_info.Count > 0)
                    {
                        foreach (var i in check_child_info)
                        {
                            ctx.Events_RegistrationChildInformation.Remove(i);
                        }
                    }

                    if (check_child_rego.Count > 0)
                    {
                        foreach (var c in check_child_rego)
                        {
                            var get_child_answers = (from a in ctx.Events_ChildEnrolmentAnswer
                                                     where a.ChildEnrolment_ID == c.ChildEnrolment_ID
                                                     select a).ToList();

                            foreach (var ca in get_child_answers)
                            {
                                ctx.Events_ChildEnrolmentAnswer.Remove(ca);
                            }

                            ctx.Events_RegistrationChildEnrolment.Remove(c);
                        }
                    }

                    // transfer allocated regos
                    var new_r = new Events_Registration();
                    new_r.Contact_ID = r.Contact_ID;
                    new_r.GroupLeader_ID = r.GroupLeader_ID;
                    new_r.FamilyRegistration = r.FamilyRegistration;
                    new_r.RegisteredByFriend_ID = r.RegisteredByFriend_ID;
                    new_r.Venue_ID = new_venue_id;
                    new_r.RegistrationType_ID = regotype_id_tb;
                    new_r.RegistrationDiscount = r.RegistrationDiscount;
                    new_r.Elective_ID = r.Elective_ID;
                    new_r.Accommodation = r.Accommodation;
                    new_r.Accommodation_ID = r.Accommodation_ID;
                    new_r.Catering = r.Catering;
                    new_r.CateringNeeds = r.CateringNeeds;
                    new_r.LeadershipBreakfast = r.LeadershipBreakfast;
                    new_r.LeadershipSummit = r.LeadershipSummit;
                    new_r.ULG_ID = r.ULG_ID;
                    new_r.ParentGuardian = r.ParentGuardian;
                    new_r.ParentGuardianPhone = r.ParentGuardianPhone;
                    new_r.AcceptTermsRego = r.AcceptTermsRego;
                    new_r.AcceptTermsParent = r.AcceptTermsParent;
                    new_r.AcceptTermsCreche = r.AcceptTermsCreche;
                    new_r.RegistrationDate = r.RegistrationDate;
                    new_r.RegisteredBy_ID = r.RegisteredBy_ID;
                    new_r.Attended = r.Attended;
                    new_r.Volunteer = r.Volunteer;
                    new_r.VolunteerDepartment_ID = r.VolunteerDepartment_ID;
                    new_r.ComboRegistration_ID = r.ComboRegistration_ID;

                    ctx.Events_Registration.Add(new_r);

                    ctx.Events_Registration.Remove(r);
                }

                var list_gp = (from gp in ctx.Events_GroupPayment
                               where gp.GroupLeader_ID == contact_id
                               && gp.Conference_ID == conf_id
                               && (gp.ExpiredIntent == false || gp.ExpiredIntent == null)
                               select gp).ToList();

                foreach (var gp in list_gp)
                {
                    // refund original payment
                    var re_gp = new Events_GroupPayment();
                    re_gp.GroupLeader_ID = gp.GroupLeader_ID;
                    re_gp.Conference_ID = gp.Conference_ID;
                    re_gp.PaymentType_ID = 11;
                    re_gp.PaymentAmount = gp.PaymentAmount * -1;
                    re_gp.CCNumber = gp.CCNumber;
                    re_gp.CCExpiry = gp.CCExpiry;
                    re_gp.CCName = gp.CCName;
                    re_gp.CCPhone = gp.CCPhone;
                    re_gp.CCManual = gp.CCManual;
                    re_gp.CCTransactionRef = gp.CCTransactionRef;
                    re_gp.CCRefund = true;
                    re_gp.ChequeDrawer = gp.ChequeDrawer;
                    re_gp.ChequeBank = gp.ChequeBank;
                    re_gp.ChequeBranch = gp.ChequeBranch;
                    re_gp.PaypalTransactionRef = gp.PaypalTransactionRef;
                    re_gp.Comment = gp.Comment;
                    re_gp.PaymentStartDate = DateTime.Now;
                    re_gp.PaymentCompleted = gp.PaymentCompleted;
                    re_gp.PaymentCompletedDate = DateTime.Now;
                    re_gp.PaymentBy_ID = gp.PaymentBy_ID;
                    re_gp.BankAccount_ID = gp.BankAccount_ID;
                    re_gp.Voucher_ID = gp.Voucher_ID;
                    re_gp.ExpiredIntent = null;

                    ctx.Events_GroupPayment.Add(re_gp);

                    // insert new payment
                    var new_gp = new Events_GroupPayment();
                    new_gp.GroupLeader_ID = gp.GroupLeader_ID;
                    new_gp.Conference_ID = new_conf_id;
                    new_gp.PaymentType_ID = 11;
                    new_gp.PaymentAmount = gp.PaymentAmount;
                    new_gp.CCNumber = gp.CCNumber;
                    new_gp.CCExpiry = gp.CCExpiry;
                    new_gp.CCName = gp.CCName;
                    new_gp.CCPhone = gp.CCPhone;
                    new_gp.CCManual = gp.CCManual;
                    new_gp.CCTransactionRef = gp.CCTransactionRef;
                    new_gp.CCRefund = false;
                    new_gp.ChequeDrawer = gp.ChequeDrawer;
                    new_gp.ChequeBank = gp.ChequeBank;
                    new_gp.ChequeBranch = gp.ChequeBranch;
                    new_gp.PaypalTransactionRef = gp.PaypalTransactionRef;
                    new_gp.Comment = gp.Comment;
                    new_gp.PaymentStartDate = DateTime.Now;
                    new_gp.PaymentCompleted = gp.PaymentCompleted;
                    new_gp.PaymentCompletedDate = DateTime.Now;
                    new_gp.PaymentBy_ID = gp.PaymentBy_ID;
                    new_gp.BankAccount_ID = gp.BankAccount_ID;
                    new_gp.Voucher_ID = gp.Voucher_ID;
                    new_gp.ExpiredIntent = null;

                    ctx.Events_GroupPayment.Add(new_gp);
                }

                ctx.SaveChanges();

                conferenceName = (from c in ctx.Events_Conference
                                  where c.Conference_ID == new_conf_id
                                  select c.ConferenceName).FirstOrDefault();
            }

            return Json(
                new
                {
                    isSuccess = true,
                    message = "Transfer succesful.",
                    conferenceName = conferenceName
                });
        }
        [HttpPost]
        public ActionResult checkConferenceAvailability(int Contact_ID, int fromVenue_ID, int toVenue_ID)
        {
            using (var ctx = new PlanetshakersEntities())
            {

                var conf_id = (from v in ctx.Events_Venue
                               where v.Venue_ID == fromVenue_ID
                               select v.Conference_ID).FirstOrDefault();

                var new_conf_id = (from v in ctx.Events_Venue
                                   where v.Venue_ID == toVenue_ID
                                   select v.Conference_ID).FirstOrDefault();

                var list_br = (from br in ctx.Events_GroupBulkRegistration
                               where br.GroupLeader_ID == Contact_ID && br.Venue_ID == fromVenue_ID && br.Deleted == false
                               select br).ToList();
                //count number of each rego type the user holds 

                var counter = new Dictionary<int, int>();
                foreach (var br in list_br)
                {
                    var regotype_id_tb = 0;
                    #region Bulk Rego Switch Cases
                    if (toVenue_ID == 171)
                    {
                        // switch cases for PS22 MELB - 1
                        switch (br.RegistrationType_ID)
                        {
                            case (3188): // Adult - Super Early Bird
                                regotype_id_tb = 3423;
                                break;
                            case (3189): // Adult - Early Bird
                                regotype_id_tb = 3424;
                                break;
                            case (3190): // Adult - Full Registration
                                regotype_id_tb = 3425;
                                break;
                            case (3194): // PK - Early Childhood
                                regotype_id_tb = 3427;
                                break;
                            case (3195): // PK - Primary
                                regotype_id_tb = 3428;
                                break;
                            case (3196): // PK - Special Needs
                                regotype_id_tb = 3429;
                                break;
                            case (3199): // PK - Early Childhood FAMILY SEB
                                regotype_id_tb = 3463;
                                break;
                            case (3202): // PK - Early Childhood FAMILY EB
                                regotype_id_tb = 3464;
                                break;
                            case (3203): // PK - Early Childhood FAMILY FR
                                regotype_id_tb = 3465;
                                break;
                            case (3204): // PK - Primary FAMILY SEB
                                regotype_id_tb = 3466;
                                break;
                            case (3198): // PK - Primary FAMILY EB
                                regotype_id_tb = 3467;
                                break;
                            case (3205): // PK - Primary FAMILY FR
                                regotype_id_tb = 3468;
                                break;
                            case (3206): // PK - Special Needs FAMILY SEB
                                regotype_id_tb = 3469;
                                break;
                            case (3207): // PK - Special Needs FAMILY EB
                                regotype_id_tb = 3470;
                                break;
                            case (3197): // PK - Special Needs FAMILY FR
                                regotype_id_tb = 3471;
                                break;
                            case (3363): // PK - Babies
                                regotype_id_tb = 3426;
                                break;
                            case (3213): // Adult - FAMILY SEB
                                regotype_id_tb = 3472;
                                break;
                            case (3214): // Adult - FAMILY EB
                                regotype_id_tb = 3473;
                                break;
                            case (3215): // Adult - FAMILY FR
                                regotype_id_tb = 3474;
                                break;
                            case (3308): // Adult - EB COMBO
                                regotype_id_tb = 3579;
                                break;
                            default:
                                break;
                        }
                    }
                    //else if (toVenue_ID == 172)
                    //{
                    //    // switch cases for  PS22 MELB - 2
                    //    switch (br.RegistrationType_ID)
                    //    {
                    //        case (3188): // Adult - Super Early Bird
                    //            regotype_id_tb = 3371;
                    //            break;
                    //        case (3189): // Adult - Early Bird
                    //            regotype_id_tb = 3372;
                    //            break;
                    //        case (3190): // Adult - Full Registration
                    //            regotype_id_tb = 3373;
                    //            break;
                    //        case (3194): // PK - Early Childhood
                    //            regotype_id_tb = 3375;
                    //            break;
                    //        case (3195): // PK - Primary
                    //            regotype_id_tb = 3376;
                    //            break;
                    //        case (3196): // PK - Special Needs
                    //            regotype_id_tb = 3377;
                    //            break;
                    //        case (3199): // PK - Early Childhood FAMILY SEB
                    //            regotype_id_tb = 3411;
                    //            break;
                    //        case (3202): // PK - Early Childhood FAMILY EB
                    //            regotype_id_tb = 3412;
                    //            break;
                    //        case (3203): // PK - Early Childhood FAMILY FR
                    //            regotype_id_tb = 3413;
                    //            break;
                    //        case (3204): // PK - Primary FAMILY SEB
                    //            regotype_id_tb = 3408;
                    //            break;
                    //        case (3198): // PK - Primary FAMILY EB
                    //            regotype_id_tb = 3409;
                    //            break;
                    //        case (3205): // PK - Primary FAMILY FR
                    //            regotype_id_tb = 3410;
                    //            break;
                    //        case (3206): // PK - Special Needs FAMILY SEB
                    //            regotype_id_tb = 3414;
                    //            break;
                    //        case (3207): // PK - Special Needs FAMILY EB
                    //            regotype_id_tb = 3415;
                    //            break;
                    //        case (3197): // PK - Special Needs FAMILY FR
                    //            regotype_id_tb = 3416;
                    //            break;
                    //        case (3363): // PK - Babies
                    //            regotype_id_tb = 3374;
                    //            break;
                    //        case (3213): // Adult - FAMILY SEB
                    //            regotype_id_tb = 3420;
                    //            break;
                    //        case (3214): // Adult - FAMILY EB
                    //            regotype_id_tb = 3421;
                    //            break;
                    //        case (3215): // Adult - FAMILY FR
                    //            regotype_id_tb = 3422;
                    //            break;
                    //        case (3308): // Adult - EB COMBO
                    //            regotype_id_tb = 3580;
                    //            break;
                    //        default:
                    //            break;
                    //    }
                    //}
                    else
                    {
                        // exit error
                    }
                    #endregion
                    if (counter.ContainsKey(regotype_id_tb))
                    {
                        counter[regotype_id_tb]++;
                    }
                    else
                    {
                        counter[regotype_id_tb] = 1;
                    }
                }
                foreach (var item in counter)
                {
                    var capacity = (from rt in ctx.Events_RegistrationType
                                    where rt.RegistrationType_ID == item.Key
                                    select rt.Limit).First();
                    var Registered = (from r in ctx.Events_GroupBulkRegistration
                                      where r.RegistrationType_ID == item.Key
                                      select r.Quantity).ToList().Sum();
                    var capacityLeft = capacity - Registered;

                    if (capacityLeft - item.Value <= 0)
                    {
                        return Json(new { available = false }, JsonRequestBehavior.AllowGet);
                    }
                }


                return Json(new { available = true }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult getFundingGoalTable(int Event_ID)
        {
            FundingGoalViewModel model = new FundingGoalViewModel();
            model.Event_ID = Event_ID;
            model.Contact_ID = getContactID();
            return View("_BalanceModal", model);
        }
    }

    /* Datamodel for the event */
    #region Data Model
    public class Event
    {
        #region Constructors

        public Event()
        {

        }

        public Event(int venue_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var conference = (from c in context.Events_Conference
                                  join v in context.Events_Venue
                                         on c.Conference_ID equals v.Conference_ID
                                  where v.Venue_ID == venue_id
                                  select c).FirstOrDefault();

                var venue = (from v in context.Events_Venue
                             where v.Venue_ID == venue_id
                             select v).FirstOrDefault();


                var bankAccount = new CCPaymentModel().BankAccount;

                // Updated to use BankAccount_ID from Events_Conference
                bankAccount = (from ba in context.Common_BankAccount
                               where ba.BankAccount_ID == conference.BankAccount_ID
                               select ba).FirstOrDefault();

                if (conference != null && venue != null)
                {
                    id = conference.Conference_ID;
                    name = conference.ConferenceName;
                    shortname = conference.ConferenceNameShort;
                    tagline = conference.ConferenceTagline;
                    if (String.IsNullOrWhiteSpace(venue.VenueDate))
                    {
                        date = conference.ConferenceDate;
                    }
                    else
                    {
                        date = venue.VenueDate;
                    }
                    location = conference.ConferenceLocation;
                    venueLocation = venue.VenueName + " " + venue.VenueLocation;
                    conference_type = (MEnumerators.EnumConferenceType)conference.ConferenceType;
                    venueID = venue_id;
                    currency = bankAccount.Currency;
                    sort_order = conference.SortOrder;
                    visible = (bool)conference.Visibility;
                }
                else
                {
                    throw new Exception("Event not found");
                }
            }
        }

        #endregion

        // ID of the conference / event
        public int id { get; set; }

        // name of the event
        public string name { get; set; }

        // short name of the event
        public string shortname { get; set; }

        // event tagline
        public string tagline { get; set; }

        // date of the event
        public string date { get; set; }

        // location of the event
        public string location { get; set; }
        public bool show { get; set; }
        public int venueID { get; set; }
        public bool allocatedToUser { get; set; }
        public bool boughtByUser { get; set; }
        public int Registration_ID { get; set; }
        public int ApplicationProgress { get; set; }

        public double outstandingBalance { get; set; }
        public double totalCost { get; set; }
        public double totalPayment { get; set; }
        public double addOnPayments { get; set; }

        //User has done refunding or donating or any actions required to their tickets before
        public bool actioned { get; set; }

        // location of the venue
        public string venueLocation { get; set; }

        // current registrants count
        public int? currRegistrants
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // For single regos
                    //int existingSingleRego = (from er in context.Events_Registration
                    //                          where er.Venue_ID == venueID && er.GroupLeader_ID == null
                    //                          select er).DefaultIfEmpty().Count();

                    // exclude add on and application type 
                    // For group regos
                    int existingGroupRego = (from gr in context.Events_GroupBulkRegistration
                                             join rt in context.Events_RegistrationType on gr.RegistrationType_ID equals rt.RegistrationType_ID
                                             where gr.Venue_ID == venueID && gr.Deleted == false && !rt.AddOnRegistrationType && !rt.ApplicationRegistrationType
                                             select gr.Quantity).DefaultIfEmpty().Sum();

                    int applicationInProgress = (from gr in context.Events_GroupBulkRegistration
                                                 join rt in context.Events_RegistrationType on gr.RegistrationType_ID equals rt.RegistrationType_ID
                                                 join a in context.Volunteer_Application on rt.RegistrationType_ID equals a.RegistrationType_ID
                                                 where gr.Venue_ID == venueID && rt.ApplicationRegistrationType && !a.Actioned && gr.Deleted == false && gr.GroupLeader_ID == a.Contact_ID
                                                 select gr).Count();

                    int currentrego = applicationInProgress + existingGroupRego;

                    return currentrego;
                }
            }
        }

        // maximum available registrations (Venue Level)
        public int maxRegistrantsVenue
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var maxReg = (from ev in context.Events_Venue
                                  where ev.Venue_ID == venueID
                                  select ev.MaxRegistrants).FirstOrDefault();

                    return maxReg;
                }
            }
        }

        // remaining Rego (Venue Level)
        public int remainingRegosVenue
        {
            get
            {
                return maxRegistrantsVenue - currRegistrants.GetValueOrDefault(0);
            }
        }

        // conference type
        public MEnumerators.EnumConferenceType conference_type { get; set; }

        // path of the banner image
        public string banner_img
        {
            get
            {
                //return "/Content/themes/planetshakers/img/banner_" + venueID + ".jpg";
                return "/Content/themes/planetshakers/img/banner_1.png";
            }
        }

        // path to the promo image
        //public string promo_img
        //{
        //    get
        //    {
        //        //return "/Content/themes/planetshakers/img/promo_" + venueID + ".jpg";
        //        return "/Content/themes/planetshakers/img/promo_0.jpg";
        //    }
        //}

        // event currency
        public string currency { get; set; }

        // List of available registration categories
        public List<RegistrationCategory> rego_categories { get; set; }

        public bool available_registration_types { get; set; }

        // function to generate header for the index
        public string generateHeader()
        {
            if (String.IsNullOrWhiteSpace(tagline)) return name;
            else return name + " - " + tagline;
        }

        public int sort_order { get; set; }
        public List<RegistrationType> registrationList { get; set; }

        public bool visible { get; set; }

    }

    public class RegistrationCategory
    {
        // Name of the registration category (should generally be Single/Group)
        public string name { get; set; }

        // Determines whether this is a group registration
        public bool is_group_rego { get; set; }

        // The id which groups up the registration together (based on the RegistrationTypeQuantitiyGroupId in the database)
        public int? group_id { get; set; }

        // Determines the minimum qty before the group bonuses apply (set to 1 for single registration}
        public int group_min_qty { get; set; }

        // List of registration types under this category
        public List<RegistrationType> rego_types { get; set; }

    }

    public class RegistrationType
    {
        // registration type id
        public int id { get; set; }

        // name of the registration type
        public string name { get; set; }
        public int? purchased { get; set; }

        public int? sold { get; set; }

        // maximum capacity
        public int? limit { get; set; }

        // cost per registration
        private decimal _cost;
        public decimal cost
        {
            get
            {
                return Math.Round(_cost, 2);
            }
            set
            {
                this._cost = value;
            }
        }
    }

    public class RegistrationGroupAllocated
    {
        public int registrationID { get; set; }
        public int contactID { get; set; }
        public string fullName { get; set; }
        public int registrationTypeID { get; set; }
        public string registrationType { get; set; }
        public DateTime registrationDate { get; set; }
        public bool medicalDetailsUpdated { get; set; }
        public bool attended { get; set; }
    }

    public class RegistrationGroupUnallocated
    {
        public int registrationTypeID { get; set; }
        public string registrationType { get; set; }
        public int quantity { get; set; }
        public int allocated { get; set; }
        public int unallocated
        {
            get
            {
                return quantity - allocated;
            }
        }
        public int pendingInvitations { get; set; }
    }

    public class RegistrationPendingInvitation
    {
        public int invitationID { get; set; }
        public string recipientEmail { get; set; }
        public string registrationType { get; set; }
        public DateTime expiry { get; set; }
    }

    public class CurrentRegistration
    {
        // registration id
        public int registration_id { get; set; }

        // registration name
        public string registration_name { get; set; }

        // group leader id
        public int? group_leader_id { get; set; }

        // group leader name
        public string group_leader { get; set; }

        // registration type id
        public int registration_type_id { get; set; }

        // registration date
        public DateTime registration_date { get; set; }

        // Id of the conference/event
        public int event_id { get; set; }

        // Name of the conference/event
        public string event_name { get; set; }

        // venue id
        public int venue_id { get; set; }

        // registrant's id
        public int? contact_id { get; set; }

        // registrant's name
        public string contact_name { get; set; }

        // Currency
        public string currency { get; set; }

        // Total cost of the registration
        private decimal _cost_total;
        public decimal cost_total
        {
            get
            {
                return Math.Round(_cost_total, 2);
            }
            set
            {
                this._cost_total = value;
            }
        }

        // Amount paid for the registration
        public decimal balance_paid { get; set; }

        // Amount due
        public decimal balance_due { get { return cost_total - balance_paid; } }

        // only if 
        public string family_member_type { get; set; }

        // quantity purchased (for group registrations)
        public int? quantity { get; set; }

    }

    public class JsonRegistrations
    {
        public int reg_id { get; set; }
        public int quantity { get; set; }
    }

    public class CartItem
    {
        // cart item id
        public int cart_item_id { get; set; }

        // registration type id
        public int reg_id { get; set; }

        // registration name
        public string reg_name { get; set; }

        // event id
        public int event_id { get; set; }

        // event venue
        public int event_venue_id { get; set; }

        // event name
        public string event_name { get; set; }

        // quantity to purchase
        public int quantity { get; set; }

        // single price
        private decimal _single_price;
        public decimal single_price
        {
            get
            {
                return Math.Round(_single_price, 2);
            }
            set
            {
                this._single_price = value;
            }
        }

        // subtotal
        public decimal subtotal { get { return Math.Round(quantity * single_price, 2); } }
    }

    //Used to record sponsorship cart item to direct it to payment.
    public class SponsorshipCartItem
    {
        public int event_id { get; set; }

        // event venue
        public int event_venue_id { get; set; }

        // event name
        public string event_name { get; set; }

        // total sponsorship amount
        public decimal totalSponsoredAmount { get; set; }
        public bool Anonymous { get; set; }
    }


    public class Voucher
    {
        public int id { get; set; }

        public string code { get; set; }

        public int contact_id { get; set; }

        public decimal value { get; set; }

        public decimal percentage_value { get; set; }

        public decimal applied_value { get; set; }
    }
    #endregion

}

