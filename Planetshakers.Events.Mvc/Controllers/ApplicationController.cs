﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.SqlServer;
using Planetshakers.Events.Mvc.Models;
using Planetshakers.Events.Mvc.Filters;

namespace Planetshakers.Events.Mvc.Controllers
{
    public class ApplicationController : Controller
    {
        #region Application
        [PSAuthorize]
        public ActionResult ApplicationForm(int id)
        {
            //id is venue_id
            var contact_id = getContactID();
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //find the registered registration type id based on contact and venue,
                //contact should only be registered to one regotype that requires application process 
                ApplicationFormModel model = (from a in context.Volunteer_Application
                                              join p in context.Volunteer_PendingRole on a.Application_ID equals p.Application_ID
                                              join r in context.Events_Registration on p.Registration_ID equals r.Registration_ID
                                              join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                              where r.Venue_ID == id && r.Contact_ID == contact_id && rt.ApplicationRegistrationType
                                                 && a.Actioned == false && a.Approved == false
                                                 && p.Actioned == false && p.Deleted == false
                                              select new ApplicationFormModel
                                              {
                                                  Application_ID = a.Application_ID,
                                                  RegistrationType_ID = r.RegistrationType_ID,
                                                  Contact_ID = contact_id,
                                                  Venue_ID = id
                                                  
                                              }).FirstOrDefault();
                var ContactDetails = (from cc in context.Common_Contact
                                      where cc.Contact_ID == contact_id
                                      select new ContactQuestion
                                      {
                                          FirstName = cc.FirstName,
                                          LastName = cc.LastName,
                                          DOB = cc.DateOfBirth ?? DateTime.MinValue,
                                          Email = cc.Email,
                                          Phone = cc.Mobile,
                                          Gender = cc.Gender,
                                          Address1 = cc.Address1,
                                          Address2 = cc.Address2,
                                          Suburb = cc.Suburb,
                                          Postcode = cc.Postcode,
                                          Country_ID = cc.Country_ID,
                                          State_ID = cc.State_ID
                                      }).FirstOrDefault();
                model.ContactQuestion = ContactDetails;
                var question = (from eq in context.Volunteer_EventQuestion
                                join q in context.Volunteer_ApplicationQuestion
                                on eq.Question_ID equals q.Question_ID
                                where eq.Inactive == false && q.Active == true && eq.RegistrationType_ID == model.RegistrationType_ID
                                orderby q.SortOrder
                                select new Question
                                {
                                    question_ID = q.Question_ID,
                                    sortOrder = q.SortOrder,
                                    question = q.Question,
                                    type = q.Type,
                                    required = q.Required,
                                    answer_ID = (from a in context.Volunteer_ApplicationAnswer where a.Question_ID == q.Question_ID && a.Application_ID == model.Application_ID select a.Answer_ID).FirstOrDefault(),
                                    answer = (from a in context.Volunteer_ApplicationAnswer where a.Question_ID == q.Question_ID && a.Application_ID == model.Application_ID select a.Answer).FirstOrDefault()

                                }).ToList();
                var i = 1;
                foreach (var q in question)
                {
                    q.question = i++ + ". " + q.question;
                }
                model.Questions = question;

                //referrals and emergency are by default created already at different stages of application
                //model.Referrals = new List<ReferralQuestion>();
                var ApplicationReferrals = (from a in context.Volunteer_ApplicationReferral
                                            where a.Application_ID == model.Application_ID
                                            select new ReferralQuestion
                                            {
                                                ApplicationReferral_ID = a.ApplicationReferral_ID,
                                                FullName = a.FullName,
                                                ReferralType = a.ReferralType,
                                                //Relationship = a.Relationship,
                                                Email = a.Email,
                                                Phone = a.Phone
                                            }).ToList();
                model.Referrals = ApplicationReferrals;

                //if this contact is create while sponsorship for someone, it will not have emeregency contacts pre-created 
                //need to insert into emergency contact table first
                var emergencyList = (from e in context.Events_EmergencyContact
                                     where e.Contact_ID == contact_id
                                     select new EmergencyContact()
                                     {
                                         EmergencyContact_ID = e.EmergencyContact_ID,
                                         FullName = e.EmergencyContactName,
                                         Mobile = e.EmergencyContactPhone,
                                         Relationship = e.EmergencyContactRelationship
                                     }).ToList();
                if (emergencyList.Count == 0)
                {
                    var ec1 = new Events_EmergencyContact() { Contact_ID = contact_id, EmergencyContactName = "", EmergencyContactPhone = "", EmergencyContactRelationship = "", LastModifiedDate = DateTime.Now };
                    var ec2 = new Events_EmergencyContact() { Contact_ID = contact_id, EmergencyContactName = "", EmergencyContactPhone = "", EmergencyContactRelationship = "", LastModifiedDate = DateTime.Now };
                    context.Events_EmergencyContact.Add(ec1);
                    context.Events_EmergencyContact.Add(ec2);
                    context.SaveChanges();

                    emergencyList = (from e in context.Events_EmergencyContact
                                         where e.Contact_ID == contact_id
                                         select new EmergencyContact()
                                         {
                                             EmergencyContact_ID = e.EmergencyContact_ID,
                                             FullName = e.EmergencyContactName,
                                             Mobile = e.EmergencyContactPhone,
                                             Relationship = e.EmergencyContactRelationship
                                         }).ToList();
                }


                model.EmergencyContacts = emergencyList;
                //if (emergencyList.Count > 0)
                //{
                //    //model.EmergencyContacts = emergencyList; 
                //    while (model.EmergencyContacts.Count < 2)
                //    {
                //        model.EmergencyContacts.Add(new EmergencyContact());
                //    }
                //}
                //else
                //{
                //    model.EmergencyContacts.Add(new EmergencyContact());
                //    model.EmergencyContacts.Add(new EmergencyContact());
                //}

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult getApplicantDetails(int Application_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var campus = (from a in context.Volunteer_ApplicationAnswer
                               where a.Application_ID == Application_ID && a.Question_ID == 7
                               select a.Answer).FirstOrDefault();
                var size = (from a in context.Volunteer_ApplicationAnswer
                               where a.Application_ID == Application_ID && a.Question_ID == 6
                               select a.Answer).FirstOrDefault();
                return Json(new {campus = campus, size = size });
            }
        }

        public ActionResult SubmitApplication(ApplicationFormModel model, FormCollection form)
        {
            var temp = form;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                string Command = Request.Params["Save"];
                var saveToDraftFlag = false;
                if (Command == "Save As Draft")
                {
                    saveToDraftFlag = true;
                    model.saveAsDraft = true;
                }
                else
                {
                    model.saveAsDraft = false;
                }
                #region Application Question
                var question = (from eq in context.Volunteer_EventQuestion
                                join q in context.Volunteer_ApplicationQuestion
                                on eq.Question_ID equals q.Question_ID
                                where eq.Inactive == false && q.Active == true && eq.RegistrationType_ID == model.RegistrationType_ID
                                orderby q.SortOrder
                                select q).ToList();

                foreach (var q in question)
                {
                    var answer = form[q.Question_ID.ToString()];
                    if (q.Type != "Multiple-Choice")
                    {
                        if (q.Type == "Checkbox" && answer == null)
                        {
                            answer = "No";
                        }
                        else if ((q.Type == "Paragraph" || q.Type == "Short Answer") && answer == null)
                        {
                            answer = "";
                        }
                    }
                    else
                    {
                        //Multiple-Choice
                        if (q.Question_ID == 6)
                        {
                            answer = form["shirtSize"];
                        }
                        else if (q.Question_ID == 7)
                        {
                            answer = form["txtCampus"];
                            if(answer == null)
                            {
                                answer = "NA";
                            }
                        }
                    }
                    var answerExist = (from a in context.Volunteer_ApplicationAnswer
                                  where a.Application_ID == model.Application_ID && a.Question_ID == q.Question_ID
                                       select a).FirstOrDefault();
                    if (answerExist != null)
                    {
                        answerExist.Answer = answer;
                    }
                    else
                    {
                        var newAnswer = new Volunteer_ApplicationAnswer();
                        newAnswer.Application_ID = model.Application_ID;
                        newAnswer.Question_ID = q.Question_ID;
                        newAnswer.Answer = answer;
                        context.Volunteer_ApplicationAnswer.Add(newAnswer);
                    }
                }
                #endregion

                #region Application Referral 

                //if doesnt exist already
                //add referral into database
                //otherwise just update
                model.Referrals[0].ReferralType = "Pastor";
                foreach(var referral in model.Referrals)
                {
                    if (referral.ApplicationReferral_ID != 0)
                    {
                        var existingReferral = (from r in context.Volunteer_ApplicationReferral
                                                where r.ApplicationReferral_ID == referral.ApplicationReferral_ID
                                                select r).FirstOrDefault();
                        existingReferral.ReferralType = (referral.ReferralType == null) ? "" : referral.ReferralType;
                        existingReferral.FullName = (referral.FullName == null) ? "" : referral.FullName;
                        //existingReferral.Relationship = (referral.Relationship == null) ? "" : referral.Relationship;
                        existingReferral.Relationship = "";
                        existingReferral.ReferralDate = DateTime.Now;
                        existingReferral.Phone = (referral.Phone == null) ? "" : referral.Phone;
                        existingReferral.Email = (referral.Email == null) ? "" : referral.Email;
                    }
                    else
                    {
                        var newreferral = new Volunteer_ApplicationReferral();
                        newreferral.Application_ID = model.Application_ID;
                        newreferral.ReferralType = (referral.ReferralType == null) ? "" : referral.ReferralType;
                        newreferral.FullName = (referral.FullName == null) ? "" : referral.FullName;
                        //existingReferral.Relationship = (referral.Relationship == null) ? "" : referral.Relationship;
                        newreferral.Relationship = "";
                        newreferral.ReferralDate = DateTime.Now;
                        newreferral.Phone = (referral.Phone == null) ? "" : referral.Phone;
                        newreferral.Email = (referral.Email == null) ? "" : referral.Email;
                        context.Volunteer_ApplicationReferral.Add(newreferral);
                    }
                }
                #endregion

                #region Application - Emergency Contacts
                //update or add emergency contact into database
                foreach (var item in model.EmergencyContacts)
                {
                    if(item.EmergencyContact_ID == 0)
                    {
                        Events_EmergencyContact econtact = new Events_EmergencyContact()
                        {
                            Contact_ID = model.Contact_ID,
                            EmergencyContactName = (item.FullName == null)? "" : item.FullName,
                            EmergencyContactPhone = (item.Mobile == null) ? "" : item.Mobile,
                            EmergencyContactRelationship = (item.Relationship == null) ? "" : item.Relationship,
                            Comment = "",
                            LastModifiedDate = DateTime.Now
                        };
                        context.Events_EmergencyContact.Add(econtact);
                    } else
                    {
                        var econtact = (from e in context.Events_EmergencyContact
                                        where e.EmergencyContact_ID == item.EmergencyContact_ID
                                        select e).FirstOrDefault();
                        econtact.EmergencyContactName = (item.FullName == null) ? "" : item.FullName;
                        econtact.EmergencyContactPhone = (item.Mobile == null) ? "" : item.Mobile;
                        econtact.EmergencyContactRelationship = (item.Relationship == null) ? "" : item.Relationship;
                        econtact.LastModifiedDate = DateTime.Now;
                    }
                }
                #endregion

                #region update Contact Details 
                //update common_contact
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == model.Contact_ID
                               select cc).FirstOrDefault();
                contact.FirstName = model.ContactQuestion.FirstName;
                contact.LastName = model.ContactQuestion.LastName;
                contact.Gender = (model.ContactQuestion.Gender == null) ? contact.Gender : model.ContactQuestion.Gender;
                contact.DateOfBirth = model.ContactQuestion.DOB;
                contact.Mobile = (model.ContactQuestion.Phone == null) ? contact.Mobile : model.ContactQuestion.Phone;
                contact.Address1 = (model.ContactQuestion.Address1 == null) ? contact.Address1 : model.ContactQuestion.Address1;
                contact.Address2 = (model.ContactQuestion.Address2 == null) ? contact.Address2 : "";
                contact.Suburb = (model.ContactQuestion.Suburb == null) ? contact.Suburb : model.ContactQuestion.Suburb;
                contact.Postcode = (model.ContactQuestion.Postcode == null) ? contact.Postcode : model.ContactQuestion.Postcode;
                contact.Country_ID = model.ContactQuestion.Country_ID;
                contact.State_ID = model.ContactQuestion.State_ID;

                #endregion

                var progress = (from p in context.Volunteer_ApplicationProgress
                                where p.VolunteerApplication_ID == model.Application_ID
                                select p).FirstOrDefault();
                if (saveToDraftFlag)
                {
                    progress.Status_ID = 5; // save as draft
                    context.SaveChanges();
                } else
                {
                    progress.Status_ID = 2; //application form submitted
                    context.SaveChanges();

                    var tripName = (from a in context.Volunteer_Application
                                    join rt in context.Events_RegistrationType on a.RegistrationType_ID equals rt.RegistrationType_ID
                                    join c in context.Events_Conference on rt.Conference_ID equals c.Conference_ID
                                    where a.Application_ID == model.Application_ID
                                    select c.ConferenceName).FirstOrDefault();
                    ApplicationReceivedEmailModel emailmodel = new ApplicationReceivedEmailModel()
                    {
                        Email = contact.Email,
                        FirstName = contact.FirstName,
                        TripName = tripName
                    };

                    new MailController().SendApplicationRecievedEmail(emailmodel);
                }
            }
            return View("Success", model);
        }

        #endregion

        #region Document Upload
        public ActionResult UploadDocumentDetails()
        {
            var DocumentType_ID = Convert.ToInt32(Request.Form["DocumentType_ID"]);
            var Event_ID = Convert.ToInt32(Request.Form["Event_ID"]);
            var Contact_ID = Convert.ToInt32(Request.Form["Contact_ID"]);
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var file = Request.Files["docFile"];
                var contentType = file.ContentType.Split('/').ToList();
                var orgName = file.FileName;

                var record = (from d in context.Volunteer_ApplicationDocument
                              where d.Contact_ID == Contact_ID && d.Event_ID == Event_ID && d.DocumentType_ID == DocumentType_ID
                              select d).FirstOrDefault();
                if (record == null)
                {
                    var newRecord = new Volunteer_ApplicationDocument();
                    newRecord.Contact_ID = Contact_ID;
                    newRecord.DocumentType_ID = DocumentType_ID;
                    newRecord.Event_ID = Event_ID;
                    newRecord.UploadDateTime = DateTime.Now;
                    newRecord.UpdatedDateTime = DateTime.Now;
                    newRecord.UploadedBy_ID = Contact_ID;
                    newRecord.UpdatedBy_ID = Contact_ID;
                    newRecord.Approved = false;
                    newRecord.Deleted = false;
                    newRecord.OriginalFilename = orgName;

                    var fileName = SaveFile(Contact_ID, Event_ID, DocumentType_ID);
                    newRecord.S3Filename = fileName;
                    context.Volunteer_ApplicationDocument.Add(newRecord);
                } else
                {
                    DeleteExistingFile(record.S3Filename, Contact_ID);
                    var fileName = SaveFile(Contact_ID, Event_ID, DocumentType_ID);
                    record.OriginalFilename = orgName;
                    record.S3Filename = fileName;
                    record.UpdatedDateTime = DateTime.Now;
                    record.UpdatedBy_ID = Contact_ID;
                    record.Approved = false;
                }
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = ex.Message });
                    throw ex;
                }
            }
            return Json(new { success = true, message = "Document Uploaded." });
        }

        public string SaveFile(int Contact_ID, int Event_ID, int DocumentType)
        {
            FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
            var file = Request.Files["docFile"];
            var fileName = "";


            if (file != null)
            {
                if (file.ContentLength != 0)
                {
                    var stream = file.InputStream;

                    //var item = fileModel.GetFile("ProofUploads", submittedContact + ".*");

                    // Do something with the stream.
                    var contentType = file.ContentType.Split('/').ToList();
                    var extension = contentType[contentType.Count - 1].ToString();
                    extension = (extension == "plain") ? "txt" : extension;
                    fileName = Event_ID.ToString() + "_" + DocumentType.ToString() + "." + extension;
                    fileModel.UploadFile(Contact_ID.ToString(), fileName, stream);
                }
            }
            return fileName;

        }

        public void DeleteExistingFile(string existingFileName, int Contact_ID)
        {
            // create new model
            FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
            fileModel.DeleteFile(Contact_ID + "/" + existingFileName);
        }

        [HttpPost]
        public ActionResult DownloadFile(int Document_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var doc = (from d in context.Volunteer_ApplicationDocument
                           where d.Document_ID == Document_ID
                           select d).FirstOrDefault();
                if (doc != null)
                {
                    FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
                    ResourcesItem item = fileModel.GetFile(doc.Contact_ID.ToString(), doc.S3Filename);
                    return Json(new { url = item.DownloadUrl, name = doc.OriginalFilename });
                } else
                {
                    return Json(new { });
                }
            }
        }

        #endregion

        #region helper function
        public int getContactID()
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            if (contact != null)
            {
                return contact.Id;
            }
            else
            {
                return 0;
            }
        }
        #endregion
    }
}
