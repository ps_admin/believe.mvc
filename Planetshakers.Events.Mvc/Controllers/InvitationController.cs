﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Events.Mvc.Models;
using Planetshakers.Events.Mvc.Filters;
using Planetshakers.Business.External.Events;
using System.Configuration;
using System.Threading.Tasks;

namespace Planetshakers.Events.Mvc.Controllers
{
    public class InvitationController : Controller
    {
        [AllowAnonymous]
        public ActionResult Error()
        {
            ViewBag.Message = TempData["errorMessage"].ToString();

            return View();
        }

        [AllowAnonymous]
        public ActionResult Success()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ThankYou()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AcceptInvitation(Guid unique_id)
        {
            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var invitation = new EventGroupInvitation(unique_id);

                    // if no valid invitation found, throw exception and redirect to error page
                    if (invitation == null)
                    {
                        throw new Exception("Invitation cannot be found");
                    }
                    else if (invitation.accepted != null)
                    {
                        if (invitation.accepted == true)
                        {
                            throw new Exception("Invitation has already been accepted");
                        }
                        else if (invitation.accepted == false)
                        {
                            throw new Exception("Invitation has already been declined");
                        }
                        else
                        {
                            throw new Exception("Invitation has already been accepted or declined");
                        }
                    } 
                    else if (invitation.expirationDate < DateTime.Now)
                    {
                        throw new Exception("Invitation expired");
                    }

                    // Store invitation data into session
                    Session["Invitation"] = invitation;

                    // Retrieve event details
                    var ps_event = new Event((int)invitation.venueID);
                    var model = new InvitationLoginPromptModel()
                    {
                        Email = invitation.receiverEmail,
                        Event = ps_event
                    };

                    return View(model);
                }
            }
            catch (Exception ex)
            {

                TempData["errorMessage"] = ex.Message;

                // Redirect to error page.
                return RedirectToAction("Error", "Invitation");

                throw ex;
            }
        }

        [AllowAnonymous]
        public ActionResult DeclineInvitation(Guid unique_id)
        {

            var invitation = new EventGroupInvitation(unique_id);
            invitation.DeclineInvitation();

            return RedirectToAction("ThankYou", "Invitation");
        }

        [AllowAnonymous]
        public ActionResult RegisterChild(Guid unique_id)
        {
            try
            {
                using(PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var invitation = new EventGroupInvitation(unique_id);

                    // If no valid invitation found, throw exception and redirect to error page
                    if (invitation == null ||
                        invitation.accepted != null ||
                        invitation.expirationDate < DateTime.Now)
                    {
                        throw new Exception("Invitation has expired or doesn't exist. Please inform your group leader.");
                    }

                    // Store invitation data into session
                    Session["Invitation"] = invitation;

                    // Retrieve event details
                    var model = new InvitationRegisterChildModel()
                    {
                        basicDetails = new BasicDetailsPartialModel(),
                        personalDetails = new PersonalDetailsPartialModel(),
                        contactDetails = new ContactDetailsPartialModel(),
                        churchDetails = new ChurchDetailsPartialModel(),
                        emergencyDetails = new EmergencyDetailsPartialModel()
                    };

                    return View(model);
                }
            }
            catch (Exception ex)
            {
                // Redirect to error page.
                return RedirectToAction("Error", "Invitation");
                throw ex;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult RegisterChild(InvitationRegisterChildModel model, string returnUrl)
        {
            var invitation = Session["Invitation"] as EventGroupInvitation;

            // Can only proceed with a valid invitation in the session, if it doesn't redirect to error page
            if (invitation == null)
            {
                return RedirectToAction("Error", "Invitation");
            }
            
            try
            {
                #region Validation

                // Validate the data
                if (String.IsNullOrWhiteSpace(model.basicDetails.Firstname))
                {
                    throw new Exception("First name is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Lastname))
                {
                    throw new Exception("Last name is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Mobile))
                {
                    throw new Exception("Mobile is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Email))
                {
                    throw new Exception("Email is required");
                }
                if (String.IsNullOrWhiteSpace(model.emergencyDetails.EmergencyContactName))
                {
                    throw new Exception("Emergency contact name is required");
                }
                if (String.IsNullOrWhiteSpace(model.emergencyDetails.EmergencyContactPhone))
                {
                    throw new Exception("Emergency contact phone is required");
                }
                if (String.IsNullOrWhiteSpace(model.emergencyDetails.EmergencyContactRelationship))
                {
                    throw new Exception("Emergency contact relationship is required");
                }
                #endregion

                if (ModelState.IsValid)
                {
                    // Retrieve logged in contact from session
                    //var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                    //model.Group = new EventGroup(contact.Id);

                    // Create a new account (with no password)                       
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // create new common_contactExternalEvents
                        //var new_contact_event = new Common_ContactExternalEvents();
                        //new_contact_event.EmergencyContactName = model.emergencyDetails.EmergencyContactName;
                        //new_contact_event.EmergencyContactPhone = model.emergencyDetails.EmergencyContactPhone;
                        //new_contact_event.EmergencyContactRelationship = model.emergencyDetails.EmergencyContactRelationship;
                        //new_contact_event.MedicalInformation = model.emergencyDetails.MedicalInfo;
                        //new_contact_event.MedicalAllergies = model.emergencyDetails.MedicalAllergies;
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "Planetkids";

                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.ChurchAddress1 = "";
                        new_external_contact.ChurchAddress2 = "";
                        new_external_contact.ChurchCountry_ID = null;
                        new_external_contact.ChurchEmail = "";
                        new_external_contact.ChurchName = "";
                        new_external_contact.ChurchNumberOfPeople = "";
                        new_external_contact.ChurchPhone = "";
                        new_external_contact.ChurchPostcode = "";
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = "";
                        new_external_contact.ChurchSuburb = "";
                        new_external_contact.ChurchWebsite = "";
                        new_external_contact.Denomination_ID = 49;
                        new_external_contact.SeniorPastorName = "";
                        new_external_contact.LeaderName = "";
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;
                        //new_external_contact.Common_ContactExternalEvents = new_contact_event;

                        // create new contact object
                        var new_contact = new Common_Contact();
                        new_contact.Salutation_ID = 1;
                        new_contact.FirstName = model.basicDetails.Firstname;
                        new_contact.LastName = model.basicDetails.Lastname;
                        new_contact.Gender = "";
                        new_contact.DateOfBirth = model.basicDetails._dob;
                        new_contact.Address1 = "";
                        new_contact.Address2 = "";
                        new_contact.Suburb = model.basicDetails.Suburb;
                        new_contact.Postcode = "";
                        new_contact.State_ID = null;
                        new_contact.StateOther = "";
                        new_contact.Country_ID = model.basicDetails.Country;
                        new_contact.Phone = "";
                        new_contact.PhoneWork = "";
                        new_contact.Fax = "";
                        new_contact.Mobile = model.basicDetails.Mobile;
                        new_contact.Email = "";
                        new_contact.Email2 = model.basicDetails.Email;
                        new_contact.DoNotIncludeEmail1InMailingList = false;
                        new_contact.DoNotIncludeEmail2InMailingList = false;
                        new_contact.Family_ID = null;
                        new_contact.FamilyMemberType_ID = null;
                        new_contact.Password = "";
                        new_contact.VolunteerPoliceCheck = "";
                        new_contact.VolunteerPoliceCheckDate = null;
                        new_contact.VolunteerWWCC = "";
                        new_contact.VolunteerWWCCDate = null;
                        new_contact.VolunteerWWCCType_ID = null;
                        new_contact.CreationDate = DateTime.Now;
                        new_contact.CreatedBy_ID = 100;
                        new_contact.ModificationDate = DateTime.Now;
                        new_contact.ModifiedBy_ID = null;
                        new_contact.GUID = Guid.NewGuid();
                        new_contact.AnotherLanguage_ID = null;
                        new_contact.Nationality_ID = null;
                        new_contact.Common_ContactExternal = new_external_contact;

                        context.Common_Contact.Add(new_contact);
                        context.SaveChanges();

                        // Allocate registration to group
                        var group = new EventGroup((int)invitation.groupLeaderID);
                        var new_rego = group.AllocateRegistration(
                                            new_contact.Contact_ID,
                                            (int)invitation.venueID,
                                            (int)invitation.registrationTypeID);
                        group.Save(new_contact.Contact_ID);                        

                        // Add aditional child information
                        var new_child_info = new Events_RegistrationChildInformation();
                        new_child_info.Registration_ID = (int)new_rego.registrationID;
                        new_child_info.CrecheChildSchoolGrade_ID = model.basicDetails.Grade;
                        new_child_info.ChildsFriend = "";
                        new_child_info.DropOffPickUpName = "";
                        new_child_info.DropOffPickUpRelationship = "";
                        new_child_info.DropOffPickUpContactNumber = "";
                        new_child_info.ChildrensChurchPastor = "";
                        new_child_info.ConsentFormSent = false;
                        new_child_info.ConsentFormSentDate = null;
                        new_child_info.ConsentFormReturned = false;
                        new_child_info.ConsentFormReturnedDate = null;

                        context.Events_RegistrationChildInformation.Add(new_child_info);
                        context.SaveChanges();

                        // Update Invitation
                        invitation.accepted = true;
                        invitation.acceptedBy = new_contact.Contact_ID;
                        invitation.acceptedOn = DateTime.Now;
                        invitation.Save();

                        // Send Notification Email
                        var email_model = new GroupInvitationNotificationEmailModel();
                        email_model.GroupLeaderID = (int)invitation.groupLeaderID;
                        email_model.InvitationRecipientEmail = (string)invitation.receiverEmail;
                        email_model.Event = new Event((int)invitation.venueID);
                        email_model.RegistrantID = new_contact.Contact_ID;
                        new MailController().InvitationAcceptNotification(email_model);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View(model);
            }

            // Successfully allocated, redirect to success page
            return RedirectToAction("Success", "Invitation", new { msg = "Thank you for registering! Your leader has been notified!" });
        }

        [PSAuthorize]
        public ActionResult Allocate()
        {
            var invitation = Session["Invitation"] as EventGroupInvitation;

            // Can only proceed with a valid invitation in the session, if it doesn't redirect to error page
            if (invitation == null)
            {
                return RedirectToAction("Error", "Invitation");
            }
            else
            {
                // Do some allocation and redirect to summary page
                var group = new EventGroup((int)invitation.groupLeaderID);

                // Retrieve logged in user
                var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                // Allocate registration
                group.AcceptInvitation(contact.Id, (Guid)invitation.identifier);

                // Clear session
                Session["Invitation"] = null;

                // Redirect to summary page.
                return RedirectToAction("Registrations", "Event");
            }
        }

        [HttpPost]
        [PSAuthorize]
        public ActionResult AllocatePrevious(int selected, int group_leader_id, int registration_type_id)
        {
            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    #region Validation

                    if (selected <= 0)
                    {
                        throw new Exception("Selection is required");
                    }
                    if (group_leader_id <= 0)
                    {
                        throw new Exception("Group leader ID is required");
                    }
                    if (registration_type_id <= 0)
                    {
                        throw new Exception("Registration type ID is required");
                    }

                    #endregion

                    var contact = (from cc in context.Common_Contact
                                   where cc.Contact_ID == selected
                                   select cc).First();

                    string email = (String.IsNullOrEmpty(contact.Email) ? contact.Email2 : contact.Email);                                  
                                                            
                    // retrieve venue_id
                    var venue_id = (from r in context.Events_RegistrationType
                                    join rv in context.Events_RegistrationTypeVenue
                                            on r.RegistrationType_ID equals rv.RegistrationType_ID
                                    where r.RegistrationType_ID == registration_type_id
                                    select rv.Venue_ID).FirstOrDefault();


                    // Check for current registration
                    var registered = (from r in context.Events_Registration
                                      where r.Venue_ID == venue_id &&
                                            r.Contact_ID == selected
                                      select r).ToList();

                    // If existing, throw exception
                    if (registered.Any())
                    {
                        ViewBag.Message = "This person has registered for the event";
                        return View("Error");
                        throw new Exception("Contact is already registered for the event");
                    }

                    // Create new registration
                    var registration = new EventRegistration();
                    registration.AllocateGroupRegistration(
                        contact_id: selected,
                        group_leader_id: (int)group_leader_id,
                        venue_id: (int)venue_id,
                        registration_type_id: (int)registration_type_id,
                        accept_terms: true,
                        registered_by_id: (int)group_leader_id);
                    registration.Save();

                    var conference = (from ev in context.Events_Venue
                                      join ec in context.Events_Conference
                                      on ev.Conference_ID equals ec.Conference_ID
                                      where ev.Venue_ID == venue_id
                                      select new Event
                                      {
                                          id = ec.Conference_ID,
                                          venueID = ev.Venue_ID,
                                          name = ec.ConferenceName,
                                          date = ec.ConferenceDate,
                                          location = ec.ConferenceLocation,
                                          venueLocation = ev.VenueName,
                                          shortname = ec.ConferenceNameShort
                                      }).FirstOrDefault();

                    var confirmationBarcode = new ConfirmationBarcode()
                    {
                        Subject = "Quick Check In - " + conference.name,
                        EventName = conference.name,
                        EventDate = conference.date.ToString(),
                        EventVenue = conference.location,
                        VenueLocation = conference.venueLocation,
                        Email = (String.IsNullOrEmpty(contact.Email)) ? contact.Email2 : contact.Email,
                        RegistrationNumber = registration.registrationID.ToString(),
                        RegistrationTypeID = (int)registration.registrationTypeID
                    };

                    if (!String.IsNullOrEmpty(confirmationBarcode.Email))
                    {
                        new MailController().ConfirmationBarcodeEmail(confirmationBarcode);
                    }

                    // Prepare return model
                    var group = new EventGroup(group_leader_id);
                    EventSummaryModel returnModel = new EventSummaryModel();
                    returnModel.SelectedEvent = new Event(venue_id);
                    returnModel.Group = group;

                    return Json(new
                    {
                        isSuccess = true,
                        curRegos = returnModel.CurrentSingleRegistrations,
                        message = (String.IsNullOrEmpty(email)) ? "Allocation successful." : "Allocation successful and email sent!",
                        allocated = returnModel.AllocatedRegistrations,
                        allocatedKids = returnModel.AllocatedKidsRegistrations,
                        allocatedQty = returnModel.GroupAllocatedCount,
                        unallocated = returnModel.UnallocatedRegistrations,
                        unallocatedQty = returnModel.GroupUnallocatedCount,
                        invites = returnModel.PendingInvitations
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = "Invitation failed to send - " + ex.Message
                });
            }
        }

        #region AJAX/Json Calls

        [HttpPost]
        [PSAuthorize]
        public ActionResult SendInvitation(string email, int group_leader_id, int registration_type_id)
        {
            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    #region Validation

                    if (String.IsNullOrWhiteSpace(email))
                    {
                        throw new Exception("Email is required");
                    }
                    if (group_leader_id <= 0)
                    {
                        throw new Exception("Group leader ID is required");
                    }
                    if (registration_type_id <= 0)
                    {
                        throw new Exception("Registration type ID is required");
                    }

                    #endregion

                    var group = new EventGroup(group_leader_id);
                    group.SendInvitation(email, registration_type_id);
                    group.Save(group_leader_id);

                    // retrieve venue_id
                    var venue_id = (from r in context.Events_RegistrationType
                                    join rv in context.Events_RegistrationTypeVenue
                                            on r.RegistrationType_ID equals rv.RegistrationType_ID
                                    where r.RegistrationType_ID == registration_type_id
                                    select rv.Venue_ID).FirstOrDefault();

                    // Prepare return model
                    EventSummaryModel returnModel = new EventSummaryModel();
                    returnModel.SelectedEvent = new Event(venue_id);
                    returnModel.Group = group;

                    return Json(new
                    {
                        isSuccess = true,
                        curRegos = returnModel.CurrentSingleRegistrations,
                        message = "Email invitation sent.",
                        allocated = returnModel.AllocatedRegistrations,
                        allocatedQty = returnModel.GroupAllocatedCount,
                        unallocated = returnModel.UnallocatedRegistrations,
                        unallocatedQty = returnModel.GroupUnallocatedCount,
                        invites = returnModel.PendingInvitations
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = "Invitation failed to send - " + ex.Message
                });
            }
        }

        [HttpPost]
        [PSAuthorize]
        public ActionResult RevokeInvitation(int invitation_id)
        {
            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var invitation = new EventGroupInvitation(invitation_id);
                    invitation.RevokeInvitation();

                    // Prepare the return data
                    EventSummaryModel returnModel = new EventSummaryModel();

                    // Retrieve selected event
                    var selected_event = new Event((int)invitation.venueID);
                    returnModel.SelectedEvent = selected_event;
                    returnModel.Group = new EventGroup((int)invitation.groupLeaderID);

                    return Json(new
                    {
                        isSuccess = true,
                        curRegos = returnModel.CurrentSingleRegistrations,
                        allocated = returnModel.AllocatedRegistrations,
                        allocatedQty = returnModel.GroupAllocatedCount,
                        unallocated = returnModel.UnallocatedRegistrations,
                        unallocatedQty = returnModel.GroupUnallocatedCount,
                        invites = returnModel.PendingInvitations
                    });
                }
            }
            catch 
            {
                return Json(new
                {
                    isSuccess = false,
                    message = "Failed to revoke invitation"
                });
            }
        }

        #endregion
    }
}
