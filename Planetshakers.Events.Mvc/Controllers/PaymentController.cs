﻿using Planetshakers.Events.Mvc.Filters;
using Planetshakers.Events.Mvc.Models;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace Planetshakers.Events.Mvc.Controllers
{
    public class PaymentController : Controller
    {
        private ILog pxlogger = LogManager.GetLogger("GeneralLogger");

        [PSAuthorize]
        // Payment checkout for Purchase
        public ActionResult Summary(string promo, string returnUrl, string error = null)
        {
            var model = Session["Cart"] as CartModel;

            if (model == null)
            {
                model = new CartModel()
                {
                    Cart = null
                };
            }

            var contact = getContactDetails();

            if (contact is null)
                return RedirectToAction("LogOn", "Account");
            else
                model.ContactId = contact.Contact_ID;

            

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //check if contact is registered for this event 
                //proceed payment if not, otherwise just return to dashboard 
                //bypass if this payment is for add on
                var regotype_id = model.Cart[0].reg_id;
                var isAddOn = isAddOnRegistrationType(regotype_id);

                var event_id = model.Event.id;
                var regoTypes = (from rt in context.Events_RegistrationType
                                 where rt.Conference_ID == event_id
                                 select rt.RegistrationType_ID).ToList();
                var register = (from r in context.Events_Registration
                                where r.Contact_ID == model.ContactId && regoTypes.Contains(r.RegistrationType_ID)
                                select r).ToList();
                var registered = (register.Count > 0) ? true : false; 

                if(registered && !isAddOn)
                {
                    return RedirectToAction("Index", "Events");
                }

                // Setup the CCPayment model            
                if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();
                var bankAccount = new Common_BankAccount();

                /* Retrieve Bank Account information to transfer funds into */
                bankAccount = (from ba in context.Common_BankAccount
                               join ec in context.Events_Conference
                               on ba.BankAccount_ID equals ec.BankAccount_ID
                               where ec.Conference_ID == model.Event.id
                               select ba).FirstOrDefault();

                model.CCPayment.BankAccount = bankAccount;
            }

            // check for total and voucher code
            if (model.GrandTotal <= 0 && !String.IsNullOrEmpty(model.Voucher.code))
            {
                model.CCPayment.payment_id = InsertPayment(String.Empty, true);

                Session["Cart"] = model;
                Session["TransactionReference"] = null;
                Session["ClientSecret"] = null;

            }
            else if (model.GrandTotal > 0)
            {
                StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;

                var invoice = createStripePayment(model);

                Session["TransactionReference"] = invoice.PaymentIntentId;

                // remove error message for session model
                Session["Cart"] = model;

                model.ErrorMessage = error;

                Session["ClientSecret"] = invoice.PaymentIntent.ClientSecret;

                ViewBag.StripePublishableKey = model.CCPayment.BankAccount.PublishableKey;
            }

            return View(model);
        }

        [PSAuthorize]
        // Payment checkout for Purchase
        //not used?
        public ActionResult BalanceSummary(int event_id, string returnUrl, string error = null)
        {
            var model = new CartModel(event_id);

            if (model == null)
            {
                model = new CartModel()
                {
                    Cart = null
                };
            }

            var contact = getContactDetails();

            if (contact is null)
                return RedirectToAction("LogOn", "Account");
            else
                model.ContactId = contact.Contact_ID;

            // Setup the CCPayment model            
            if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var bankAccount = new Common_BankAccount();

                /* Retrieve Bank Account information to transfer funds into */
                bankAccount = (from ba in context.Common_BankAccount
                               join ec in context.Events_Conference
                               on ba.BankAccount_ID equals ec.BankAccount_ID
                               where ec.Conference_ID == model.Event.id
                               select ba).FirstOrDefault();

                model.CCPayment.BankAccount = bankAccount;

                var hasRegos = (from g in context.Events_GroupBulkRegistration
                                where g.GroupLeader_ID == contact.Contact_ID && g.Venue_ID == model.Event.venueID && g.Deleted == false
                                select g).Any();
                if (hasRegos)
                {
                    var totalCost = (from r in context.Events_GroupBulkRegistration
                                     join t in context.Events_RegistrationType
                                     on r.RegistrationType_ID equals t.RegistrationType_ID
                                     where r.GroupLeader_ID == contact.Contact_ID
                                     && r.Venue_ID == model.Event.venueID
                                     && r.Deleted == false
                                     select r.Quantity * t.RegistrationCost).DefaultIfEmpty().Sum();

                    var totalPaid = (from p in context.Events_GroupPayment
                                     where p.GroupLeader_ID == contact.Contact_ID
                                     && p.Conference_ID == model.Event.id
                                     && p.PaymentCompleted == true
                                     && (p.ExpiredIntent == false || p.ExpiredIntent == null)
                                     select p.PaymentAmount).DefaultIfEmpty().Sum();

                    model.Cart.Add(new CartItem
                    {
                        reg_id = -1,
                        reg_name = "Outstanding Balance",
                        event_id = model.Event.id,
                        event_venue_id = model.Event.venueID,
                        event_name = model.Event.name,
                        quantity = 1,
                        single_price = Math.Round(totalCost - totalPaid, 2)
                    });
                }
            }

            Session["Cart"] = model;

            // check for total and voucher code
            if (model.GrandTotal <= 0 && !String.IsNullOrEmpty(model.Voucher.code))
            {
                model.CCPayment.payment_id = InsertPayment(String.Empty, true);

                Session["Cart"] = model;
                Session["TransactionReference"] = null;
                Session["ClientSecret"] = null;
            }
            else if (model.GrandTotal > 0)
            {
                StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;

                var invoice = createStripePayment(model);

                Session["TransactionReference"] = invoice.PaymentIntentId;

                // remove error message for session model
                Session["Cart"] = model;

                model.ErrorMessage = error;

                Session["ClientSecret"] = invoice.PaymentIntent.ClientSecret;

                ViewBag.StripePublishableKey = model.CCPayment.BankAccount.PublishableKey;
            }

            return View("Summary", model);
        }

        public ActionResult OutstandingBalance(int event_id, double amount)
        {
            pxlogger.Info("Payment Trigger");
            EventsController _eventController = new EventsController();
            var model = new CartModel(event_id);

            var contact = getContactDetails();

            if (contact is null)
                return RedirectToAction("LogOn", "Account");
            else
                model.ContactId = contact.Contact_ID;

            var canSponsor = _eventController.checkBalance(model.ContactId, event_id, (decimal)amount);
            if (canSponsor)
            {
                // Setup the CCPayment model            
                if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

                var previouspayments = new List<Events_GroupPayment>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var bankAccount = new Common_BankAccount();

                    /* Retrieve Bank Account information to transfer funds into */
                    bankAccount = (from ba in context.Common_BankAccount
                                   join ec in context.Events_Conference
                                   on ba.BankAccount_ID equals ec.BankAccount_ID
                                   where ec.Conference_ID == event_id
                                   select ba).FirstOrDefault();

                    model.CCPayment.BankAccount = bankAccount;

                    previouspayments = (from gp in context.Events_GroupPayment
                                        where gp.Conference_ID == event_id
                                        && gp.GroupLeader_ID == contact.Contact_ID
                                        && gp.PaymentCompleted == true
                                        && (gp.ExpiredIntent == false || gp.ExpiredIntent == null)
                                        select gp).ToList();
                }
                model.Cart.Add(new CartItem
                {
                    reg_id = -1,
                    reg_name = "Outstanding Balance",
                    event_id = model.Event.id,
                    event_venue_id = model.Event.venueID,
                    event_name = model.Event.name,
                    quantity = 1,
                    single_price = Convert.ToDecimal(String.Format("{0:0.00}", amount))
                });
                Session["Cart"] = model;
                StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;

                var invoice = createStripePayment(model);

                Session["TransactionReference"] = invoice.PaymentIntentId;

                Session["Cart"] = model;

                Session["ClientSecret"] = invoice.PaymentIntent.ClientSecret;

                ViewBag.StripePublishableKey = model.CCPayment.BankAccount.PublishableKey;

                foreach (var item in previouspayments)
                {
                    model.previousCart.Add(new CartItem
                    {
                        reg_id = -1,
                        reg_name = model.Event.name,
                        event_id = model.Event.id,
                        event_venue_id = model.Event.venueID,
                        event_name = model.Event.name,
                        quantity = 1,
                        single_price = item.PaymentAmount
                    });
                }

                return View("Summary", model);
            } else
            {
                //TODO: error message - out of balance
                return View("Error");
            }
            
        }

        // Payment checkout for Sponsorship
        //public ActionResult SponsorshipSummary(SponsorshipViewModel inputModel)
        //{
        //    var model = new CartModel(inputModel.Event_ID);

        //    model.ContactId = inputModel.Contact_ID;

        //    // Setup the CCPayment model            
        //    if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        var bankAccount = new Common_BankAccount();

        //        /* Retrieve Bank Account information to transfer funds into */
        //        bankAccount = (from ba in context.Common_BankAccount
        //                       join ec in context.Events_Conference
        //                       on ba.BankAccount_ID equals ec.BankAccount_ID
        //                       where ec.Conference_ID == inputModel.Event_ID
        //                       select ba).FirstOrDefault();

        //        model.CCPayment.BankAccount = bankAccount;                
        //    }
        //    model.Cart.Add(new CartItem
        //    {
        //        reg_id = -1,
        //        reg_name = "Sponsorship",
        //        event_id = model.Event.id,
        //        event_venue_id = model.Event.venueID,
        //        event_name = model.Event.name,
        //        quantity = 1,
        //        single_price = Convert.ToDecimal(String.Format("{0:0.00}", inputModel.sponsoringAmount))
        //    });
        //    Session["Cart"] = model;
        //    StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;
        //    model.SponsorDetails = new SponsorDetails()
        //    {
        //        Contact_ID = inputModel.Contact_ID,
        //        Event_ID = inputModel.Event_ID,
        //        FirstName = inputModel.FirstName,
        //        LastName = inputModel.LastName,
        //        Email = inputModel.Email,
        //        Mobile = inputModel.Mobile,
        //        Annonymous = inputModel.Annonymous,
        //        sponsoringAmount = inputModel.sponsoringAmount
        //    };

        //    var invoice = createStripePayment(model);

        //    Session["TransactionReference"] = invoice.PaymentIntentId;

        //    Session["Cart"] = model;

        //    Session["ClientSecret"] = invoice.PaymentIntent.ClientSecret;

        //    ViewBag.StripePublishableKey = model.CCPayment.BankAccount.PublishableKey;


        //    return View("Summary", model);
        //}

        public ActionResult SponsorshipSummary(string returnUrl, string error = null)
        {
            ViewBag.returnUrl = returnUrl;
            var model = Session["SponsorshipCart"] as SponsorshipCartModel;

            if (model == null)
            {
                model = new SponsorshipCartModel()
                {
                    Cart = null
                };
            }

            // Retrieve logged in contact from session
            var contact = model.ContactId;

            if (contact == null)
                return RedirectToAction("LoginSponsorshipPrompt", new { returnUrl = returnUrl });
            else
                model.ContactId = contact;


            // Setup the CCPayment model            
            if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var bankAccount = new Common_BankAccount();

                /* Retrieve Bank Account information to transfer funds into */
                bankAccount = (from ba in context.Common_BankAccount
                               join ec in context.Events_Conference
                               on ba.BankAccount_ID equals ec.BankAccount_ID
                               where ec.Conference_ID == model.Event.id
                               select ba).FirstOrDefault();

                model.CCPayment.BankAccount = bankAccount;
            }

            StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;

            // create stripe customer
            var customerOptions = new CustomerCreateOptions
            {
                Email = model.Email,
                Name = model.FirstName + " " + model.LastName
            };

            var customerService = new CustomerService();
            var customer = customerService.Create(customerOptions);


            // create stripe product
            var productOptions = new ProductCreateOptions
            {
                Name = model.Event.name + " - Sponsorship",
                Description = model.Event.name
            };

            var productService = new ProductService();
            var product = productService.Create(productOptions);

            // create stripe invoice item
            var invoiceItemOptions = new InvoiceItemCreateOptions
            {
                Customer = customer.Id,
                PriceData = new InvoiceItemPriceDataOptions
                {
                    Currency = model.CCPayment.BankAccount.Currency,
                    Product = product.Id,
                    UnitAmount = Convert.ToInt32(model.GrandTotal * 100)
                },
                Quantity = 1
            };

            var invoiceItemService = new InvoiceItemService();
            var invoiceItem = invoiceItemService.Create(invoiceItemOptions);

            // create stripe invoice
            var invoiceOptions = new InvoiceCreateOptions
            {
                Customer = customer.Id,
                Description = model.Event.name + " - Sponsorship",
                AutoAdvance = false
            };

            var invoiceService = new InvoiceService();
            var invoice = invoiceService.Create(invoiceOptions);

            // finalise invoice
            var invoiceFinalizeOptions = new InvoiceFinalizeOptions();
            invoice = invoiceService.FinalizeInvoice(invoice.Id, invoiceFinalizeOptions);

            // get paymentintent for invoice
            var service = new PaymentIntentService();
            invoice.PaymentIntent = service.Get(invoice.PaymentIntentId);
            
            model.CCPayment.transactionReference = invoice.PaymentIntentId;

            model.CCPayment.payment_id = InsertVariablePayment(model.CCPayment.transactionReference);

            // add details to payment intent
            var paymentIntentOptions = new PaymentIntentUpdateOptions
            {
                Description = model.Event.name + " - Sponsorship",
                Metadata = new Dictionary<string, string>
                {
                    { "integration_check", "accept_a_payment" },
                    { "Recipient_ID", model.SponsoringToContactID.ToString() },
                    { "VariablePayment_ID", model.CCPayment.payment_id.ToString() },
                    { "Type", "Sponsorship" },
                    { "Anonymous", model.Cart.Anonymous.ToString() },
                }
            };

            service.Update(invoice.PaymentIntentId, paymentIntentOptions);           

            Session["TransactionReference"] = invoice.PaymentIntentId;

            // remove error message for session model
            Session["SponsorshipCart"] = model;

            model.ErrorMessage = error;

            ViewData["Client-Secret"] = invoice.PaymentIntent.ClientSecret;

            ViewBag.StripePublishableKey = model.CCPayment.BankAccount.PublishableKey;

            return View(model);
        }

        public Invoice createStripePayment(CartModel model)
        {
            // create stripe customer
            var customerOptions = new CustomerCreateOptions
            {
                Email = model.Email.Trim(' '),
                Name = model.FullName
            };

            var customerService = new CustomerService();
            var customer = customerService.Create(customerOptions);

            foreach (var item in model.Cart)
            {
                // create stripe product
                var productOptions = new ProductCreateOptions
                {
                    Name = model.Event.name + " - " + item.reg_name,
                    Description = model.Event.name
                };

                var productService = new ProductService();
                var product = productService.Create(productOptions);

                // create stripe invoice item
                var invoiceItemOptions = new InvoiceItemCreateOptions
                {
                    Customer = customer.Id,
                    PriceData = new InvoiceItemPriceDataOptions
                    {
                        Currency = model.CCPayment.BankAccount.Currency,
                        Product = product.Id,
                        UnitAmount = Convert.ToInt32(item.single_price * 100)
                    },
                    Quantity = item.quantity
                };

                var invoiceItemService = new InvoiceItemService();
                var invoiceItem = invoiceItemService.Create(invoiceItemOptions);
            }

            // create stripe invoice
            var invoiceOptions = new InvoiceCreateOptions
            {
                Customer = customer.Id,
                Description = model.Event.name,
                AutoAdvance = false
            };

            // create coupon
            if (model.Voucher != null)
            {
                var couponOptions = new CouponCreateOptions
                {
                    Currency = model.CCPayment.BankAccount.Currency,
                    AmountOff = Convert.ToInt32(model.Voucher.value * 100),
                    Duration = "once"
                };

                var couponService = new CouponService();
                Coupon coupon = couponService.Create(couponOptions);

                invoiceOptions.Discounts = new List<InvoiceDiscountOptions> {
                    new InvoiceDiscountOptions
                    {
                        Coupon = coupon.Id
                    }
                };
            }

            var invoiceService = new InvoiceService();
            var invoice = invoiceService.Create(invoiceOptions);

            // finalise invoice
            var invoiceFinalizeOptions = new InvoiceFinalizeOptions();
            invoice = invoiceService.FinalizeInvoice(invoice.Id, invoiceFinalizeOptions);

            // get paymentintent for invoice
            var service = new PaymentIntentService();
            invoice.PaymentIntent = service.Get(invoice.PaymentIntentId);

            // add details to payment intent
            var paymentIntentOptions = new PaymentIntentUpdateOptions
            {
                Description = model.Event.name,
                Metadata = new Dictionary<string, string>
                {
                    { "integration_check", "accept_a_payment" },
                    { "Type", "Payment" },
                }
            };

            service.Update(invoice.PaymentIntentId, paymentIntentOptions);

            model.CCPayment.transactionReference = invoice.PaymentIntentId;

            model.CCPayment.payment_id = InsertPayment(model.CCPayment.transactionReference, true);

            return invoice;
        }

        public ActionResult addRegistration(int venue_id, int contact_id)
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var cartModel = Session["Cart"] as CartModel;
            var transactionRef = Session["TransactionReference"] as String;

            #region Validate Parameters
            if (Session["Cart"] == null)
            {
                return RedirectToAction("Summary", "Payment", new { error = "Error trying to retrieve cart. If this persists, please contact info@believeglobal.org." });
            }
            else if (contact == null && cartModel.SponsorDetails == null)
            {
                return RedirectToAction("Summary", "Payment", new { error = "Error trying to retrieve account. Please contact info@believeglobal.org for assistance." });
            }
            else if (cartModel.Event.id <= 0)
            {
                return RedirectToAction("Summary", "Payment", new { error = "Event error. Please contact info@believeglobal.org for assistance." });
            }
            else if (String.IsNullOrEmpty(transactionRef) && cartModel.Voucher == null)
            {
                return RedirectToAction("Summary", "Payment", new { error = "Transaction reference not found. Please contact info@believeglobal.org for assistance." });
            }
            #endregion

            if (cartModel.ContactId <= 0)
            {
                cartModel.ContactId = contact.Id;
            }

            cartModel.CCPayment.transactionReference = transactionRef;
            var isAddOn = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {

                cartModel.CCPayment.BankAccount = (from ev in context.Events_Venue
                                                   join ec in context.Events_Conference
                                                   on ev.Conference_ID equals ec.Conference_ID
                                                   join ba in context.Common_BankAccount
                                                   on ec.BankAccount_ID equals ba.BankAccount_ID
                                                   where ev.Venue_ID == venue_id
                                                   select ba).FirstOrDefault();

                //do not add application and any registration allocation if its addon rego type
                var regotype_id = cartModel.Cart[0].reg_id;
                isAddOn = isAddOnRegistrationType(regotype_id);
                if (!isAddOn)
                {
                    if (cartModel.Cart.Any(x => x.reg_id > 0))
                    {
                        // Insert payment and registration into database
                        InsertEventRegistration(true);
                    }
                    var payment_id = markPaymentCompleted(cartModel.CCPayment.payment_id, transactionRef, "purchase");
                    //add in application form
                    EventSummaryModel returnModel = new EventSummaryModel();
                    if (cartModel.Cart[0].reg_id != -1)
                    {
                        returnModel.Group = new EventGroup(cartModel.ContactId);
                        var new_registration = returnModel.Group.AllocateRegistration(cartModel.ContactId, venue_id, cartModel.Cart[0].reg_id);
                        returnModel.Group.Save(cartModel.ContactId);
                        _ = addApplication(contact_id, venue_id);

                        //send email for deposit received
                        var tripName = (from v in context.Events_Venue
                                        join c in context.Events_Conference on v.Conference_ID equals c.Conference_ID
                                        where v.Venue_ID == venue_id
                                        select c.ConferenceName).FirstOrDefault();
                        DepositReceivedEmailModel emailmodel = new DepositReceivedEmailModel()
                        {
                            Email = contact.Email,
                            FirstName = contact.FirstName,
                            TripName = tripName
                        };
                        new MailController().SendDepositRecievedEmail(emailmodel);
                    }
                } else
                {
                    _ = markPaymentCompleted(cartModel.CCPayment.payment_id, transactionRef, "purchase");
                }
            }

            string Category = "Add Rego Complete";
            string Criteria = "Venue ID : " + cartModel.Cart.First().event_venue_id.ToString();
            string ContactID = cartModel.ContactId.ToString();
            Business.MWebMain.LogAction(ref Category, ref Criteria, ContactID);

            if (String.IsNullOrEmpty(transactionRef) && cartModel.Voucher != null && cartModel.GrandTotal <= 0)
            {
                return RedirectToAction("Confirmation", "Purchase", new { venue_ID = cartModel.Event.venueID });
            }
            else
            {
                if (cartModel.SponsorDetails != null)
                {
                    return Json(
                                    new
                                    {
                                        message = "Registration and payment completed",
                                        link = Url.Action("SponsorshipConfirmation", "Purchase", new { venue_ID = cartModel.Event.venueID })
                                    });
                }
                else
                {
                    return Json(
                                    new
                                    {
                                        message = "Registration and payment completed",
                                        link = Url.Action("Confirmation", "Purchase", new { venue_ID = cartModel.Event.venueID, isAddon = isAddOn })
                                    });
                }
            }
        }
        public bool addApplication(int Contact_ID, int venue_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var Registration_ID = (from r in context.Events_Registration
                                       where r.Contact_ID == Contact_ID && r.Venue_ID == venue_ID
                                       select r.Registration_ID).FirstOrDefault();

                var RegistrationType_ID = (from r in context.Events_Registration
                                           where r.Registration_ID == Registration_ID
                                           select r.RegistrationType_ID).FirstOrDefault();

                var Application = new Volunteer_Application();
                Application.Contact_ID = Contact_ID;
                Application.RegistrationType_ID = RegistrationType_ID;
                Application.DateRequested = DateTime.Now;
                Application.Actioned = false;
                Application.ActionedBy = null;
                Application.DateActioned = null;
                Application.Approved = false;
                context.Volunteer_Application.Add(Application);

                var pending = new Volunteer_PendingRole();
                pending.Application_ID = Application.Application_ID;
                pending.Registration_ID = Registration_ID;
                pending.CreateDateTime = DateTime.Now;
                pending.Actioned = false;
                pending.ActionedDateTime = null;
                pending.CreatedBy = Contact_ID;
                pending.Deleted = false;
                context.Volunteer_PendingRole.Add(pending);

                //add application progress
                var progress = new Volunteer_ApplicationProgress();
                progress.VolunteerApplication_ID = Application.Application_ID;
                progress.Status_ID = 1;
                progress.ModifiedDate = DateTime.Now;
                context.Volunteer_ApplicationProgress.Add(progress);

                //default insert 2 referrals to application
                var referral = new Volunteer_ApplicationReferral()
                {
                    Application_ID = Application.Application_ID,
                    ReferralType = "",
                    FullName = "",
                    Relationship = "",
                    ReferralDate = DateTime.Now,
                    Phone = "",
                    Email = "",
                    Comment = null,
                    Approved = false
                };
                var referral2 = new Volunteer_ApplicationReferral()
                {
                    Application_ID = Application.Application_ID,
                    ReferralType = "",
                    FullName = "",
                    Relationship = "",
                    ReferralDate = DateTime.Now,
                    Phone = "",
                    Email = "",
                    Comment = null,
                    Approved = false
                };
                context.Volunteer_ApplicationReferral.Add(referral);
                context.Volunteer_ApplicationReferral.Add(referral2);
                context.SaveChanges();
            }
            return true;
        }
        public ActionResult addSponsorship(int venue_id, int contact_id)
        {
            //there was a null reference exception - session contact or sponsorshipCart is null? 
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var model = Session["SponsorshipCart"] as SponsorshipCartModel;
            model.CCPayment.transactionReference = Session["TransactionReference"] as String;

            string Category = "Stripe Add Sponsorship";
            string Criteria = "Venue ID : " + model.Cart.event_venue_id.ToString();
            string ContactID = model.ContactId.ToString();
            //string Criteria = "Venue ID : " + venue_id.ToString();
            //string ContactID = contact_id.ToString();
            Business.MWebMain.LogAction(ref Category, ref Criteria, ContactID);

            #region Validate parameters
            if (model == null)
            {
                throw new Exception("An unknown error occured.");
            }

            if (Session["SponsorshipCart"] == null || contact == null)
            {
                return RedirectToAction("SponsorshipSummary", "Payment", new { error = "Error retrieving cart. If this persists, please contact info@believeglobal.org." });
            }

            if (model.Event.id <= 0)
            {
                return RedirectToAction("SponsorshipSummary", "Payment", new { error = "Event error. Please contact info@believeglobal.org for assistance." });
            }

            if (model.ContactId > 0)
            {
                model.ContactId = contact.Id;
            }
            else
            {
                return RedirectToAction("SponsorshipSummary", "Payment", new { error = "Authentication error. Please contact info@believeglobal.org for assistance." });
            }
            #endregion

            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var payment_id = markPaymentCompleted(model.CCPayment.payment_id, model.CCPayment.transactionReference, "sponsorship");
                    //automatically allocate amount to person
                    if (payment_id != 0)
                    {
                        _ = insertOutgoingVariablePayment(model.SponsoringToContactID, payment_id, model.Cart.Anonymous);
                    }

                    #region Send Sponsorship Email Confirmation
                    // Prepare the sponsorship confirmation email model
                    //var email_model = new SponsorshipConfirmationEmailModel()
                    //{
                    //    Email = contact.Email,
                    //    ContactId = contact.Id,
                    //    Event = model.Event,
                    //    GrandTotal = model.GrandTotal
                    //};

                    //// Send sponsorship email
                    //new MailController().SponsorshipConfirmation(email_model);
                    #endregion

                    // Clear the Cart 
                    TempData["recently_registered"] = true;
                    Session["SponsorshipCart"] = model;

                    return Json(
                        new
                        {
                            message = "Registration and payment completed",
                            link = Url.Action("SponsorshipConfirmation", "Purchase", new { venue_ID = model.Event.venueID, Contact_ID = model.SponsoringToContactID })
                        });
                }
            }
            catch (Exception ex)
            {
                return View("Error");
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult loadStripePayment()
        {
            var model = Session["Cart"] as CartModel;
            var transactionReference = Session["transactionReference"] as String;
            var stripeBypass = false;

            stripeBypass = model.Voucher != null && String.IsNullOrEmpty(transactionReference) && model.GrandTotal == 0;

            ViewData["Cart"] = model;

            if (stripeBypass && Session["ClientSecret"] == null)
            {
                Session["transactionReference"] = null;
                return PartialView("_CheckoutButton", model);
            }
            else
            {
                ViewData["ClientSecret"] = Session["ClientSecret"];
                return PartialView("_StripePayment", model);
            }
        }

        [HttpPost]
        public ActionResult loadBalancePayment()
        {
            var model = Session["Cart"] as CartModel;
            var transactionReference = Session["transactionReference"] as String;
            var stripeBypass = false;

            stripeBypass = model.Voucher != null && String.IsNullOrEmpty(transactionReference) && model.GrandTotal == 0;

            ViewData["Cart"] = model;

            ViewData["ClientSecret"] = Session["ClientSecret"];
            return PartialView("_BalancePayment", model);
        }

        [HttpPost]
        public ActionResult Checkout()
        {
            var model = Session["Cart"] as CartModel;

            var contact = getContactDetails();

            return RedirectToAction("addRegistration", new { venue_id = model.Event.venueID, contact_id = contact.Contact_ID });
        }

        #region Private Functions
        private Common_Contact getContactDetails()
        {

            var contact = Session["Contact"] as Planetshakers.Events.Mvc.Authenticator.Contact;

            // Retrieve logged in contact from session
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Retrieve contact details (if available)
                var ps_contact = (from cc in context.Common_Contact
                                  where cc.Contact_ID == contact.Id
                                  select cc).First();
                return ps_contact;
            }
        }


        private void SendEventsEmail(string payment_id, bool is_group_rego)
        {
            var user_id = getContactDetails().Contact_ID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Retrieve some cart data from session
                var cartModel = Session["Cart"] as CartModel;

                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == cartModel.ContactId
                               select cc).FirstOrDefault();

                var email_model = new RegistrationReceipt()
                {
                    Email = contact.Email,
                    banner_img = "https://register.planetshakers.com/Content/themes/planetshakers/img/banner_" + cartModel.Event.venueID + ".jpg",
                    Items = new List<RegistrationConfirmationItem>(),
                    FullName = contact.FirstName + " " + contact.LastName,
                    Subject = "Registration Confirmation - " + cartModel.Event.name,
                    EventName = cartModel.Event.name,
                    EventDate = cartModel.Event.date,
                    EventVenue = cartModel.Event.location,
                    Currency = cartModel.Event.currency,
                    Promo = (cartModel.Voucher != null ? cartModel.Voucher : null),
                    ReceiptNumber = payment_id,
                    PaymentType = (cartModel.CCPayment != null ? cartModel.CCPayment.PaymentType : "N/A"),
                    TransactionReference = (cartModel.CCPayment != null ? (!String.IsNullOrEmpty(cartModel.CCPayment.transactionReference) ? cartModel.CCPayment.transactionReference : "N/A") : "N/A")
                };

                // create an entry for the registration
                foreach (CartItem item in cartModel.Cart)
                {
                    email_model.Items.Add(new RegistrationConfirmationItem
                    {
                        reference_number = payment_id,
                        reg_type_id = item.reg_id,
                        reg_count = item.quantity,
                        qty = item.quantity,
                        price = item.single_price
                    });
                }

                // Prepare to send 
                if (!String.IsNullOrEmpty(email_model.Email))
                {
                    new MailController().RegistrationReceiptEmail(email_model);
                }
            }
        }

        /* CreateEventLog not in use
        private void CreateEventLog(int contact_id, bool is_group_rego, string action, string item)
        {
            var cartModel = Session["Cart"] as CartModel;
            var user_id = getContactDetails().Contact_ID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                if (is_group_rego)
                {
                    var new_history = new Events_GroupHistory();
                    new_history.GroupLeader_ID = contact_id;
                    new_history.Conference_ID = cartModel.Event.id;
                    new_history.Venue_ID = cartModel.Event.venue_id;
                    new_history.DateChanged = DateTime.Now;
                    new_history.User_ID = user_id;
                    new_history.Action = action;
                    new_history.Table = String.Empty;
                    new_history.FieldName = String.Empty;
                    new_history.Item = item;
                    new_history.OldValue = String.Empty;
                    new_history.OldText = String.Empty;
                    new_history.NewValue = String.Empty;
                    new_history.NewText = String.Empty;
                    context.Events_GroupHistory.Add(new_history);
                }
                else
                {
                    var new_history = new Common_ContactHistory();
                    new_history.Contact_ID = contact_id;
                    new_history.Module = "Events";
                    new_history.Desc = String.Empty;
                    new_history.Desc2 = String.Empty;
                    new_history.DateChanged = DateTime.Now;
                    new_history.User_ID = user_id;
                    new_history.Action = action;
                    new_history.Item = item;
                    new_history.Table = String.Empty;
                    new_history.FieldName = String.Empty;
                    new_history.OldValue = String.Empty;
                    new_history.OldText = String.Empty;
                    new_history.NewValue = String.Empty;
                    new_history.NewText = String.Empty;
                    context.Common_ContactHistory.Add(new_history);
                }

                context.SaveChanges();
            }
        }*/

        private void InsertEventRegistration(bool isGroup)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var cartModel = Session["Cart"] as CartModel;

                        #region Create Contact External (if doesn't exist)
                        Common_ContactExternal ce = (from ce1 in context.Common_ContactExternal
                                                     where ce1.Contact_ID == cartModel.ContactId
                                                     select ce1).FirstOrDefault();
                        if (ce == null)
                        {
                            ce = new Common_ContactExternal();
                            ce.Contact_ID = cartModel.ContactId;
                            ce.ChurchName = "";
                            ce.Denomination_ID = 49;
                            ce.SeniorPastorName = "";
                            ce.ChurchAddress1 = "";
                            ce.ChurchAddress2 = "";
                            ce.ChurchSuburb = "";
                            ce.ChurchPostcode = "";
                            ce.ChurchState_ID = null;
                            ce.ChurchStateOther = "";
                            ce.ChurchCountry_ID = null;
                            ce.ChurchWebsite = "";
                            ce.LeaderName = "";
                            ce.ChurchNumberOfPeople = "";
                            ce.ChurchPhone = "";
                            ce.ChurchEmail = "";
                            ce.ContactDetailsVerified = true;
                            ce.Deleted = false;
                            ce.Inactive = false;

                            context.Common_ContactExternal.Add(ce);
                            context.SaveChanges();
                        }

                        Common_ContactExternalEvents cee = (from cee1 in context.Common_ContactExternalEvents
                                                            where cee1.Contact_ID == cartModel.ContactId
                                                            select cee1).FirstOrDefault();
                        if (cee == null)
                        {
                            //cee = new Common_ContactExternalEvents();
                            //cee.Contact_ID = cartModel.ContactId;
                            //cee.EmergencyContactName = "";
                            //cee.EmergencyContactPhone = "";
                            //cee.EmergencyContactRelationship = "";
                            //cee.MedicalAllergies = "";
                            //cee.MedicalInformation = "";
                            //cee.AccessibilityInformation = "";
                            //cee.EventComments = "";
                            //context.Common_ContactExternalEvents.Add(cee);
                            //context.SaveChanges();
                        }

                        Common_Contact cc = (from cc1 in context.Common_Contact
                                             where cc1.Contact_ID == cartModel.ContactId
                                             select cc1).FirstOrDefault();

                        #endregion

                        if (isGroup)
                        {
                            #region Create Group (if doesn't exist)
                            var isGroupLeader = ((from eg in context.Events_Group
                                                  where eg.GroupLeader_ID == cartModel.ContactId
                                                  select eg).FirstOrDefault() != null ? true : false);

                            if (!isGroupLeader)
                            {
                                var new_group = new Events_Group();
                                new_group.GroupLeader_ID = cartModel.ContactId;
                                new_group.GroupName = cc.FirstName + " " + cc.LastName;
                                new_group.GroupColeaders = String.Empty;
                                new_group.YouthLeaderName = String.Empty;
                                new_group.GroupSize_ID = null;
                                new_group.GroupCreditStatus_ID = 1;
                                new_group.GroupType_ID = 1;
                                new_group.Comments = String.Empty;
                                new_group.DetailsVerified = false;
                                new_group.Deleted = false;
                                new_group.CreationDate = DateTime.Now;
                                new_group.CreatedBy_ID = getContactDetails().Contact_ID;
                                new_group.ModificationDate = null;
                                new_group.ModifiedBy_ID = null;
                                new_group.GUID = Guid.NewGuid();

                                context.Events_Group.Add(new_group);
                                context.SaveChanges();
                            }

                            #endregion

                            #region Insert Group Bulk Registrations into database

                            foreach (var i in cartModel.Cart)
                            {
                                createGroupRegistration(cartModel.ContactId, cartModel.Event.venueID, i.reg_id, i.quantity);
                            }

                            #endregion

                        }
                        else
                        {
                            #region Create New Registration or Add To Bulk Registration
                            var isRegistered = ((from er in context.Events_Registration
                                                 where er.Contact_ID == cartModel.ContactId
                                                         && er.Venue_ID == cartModel.Event.venueID
                                                 select er).FirstOrDefault() != null ? true : false);

                            if (!isRegistered)
                            {
                                foreach (var i in cartModel.Cart)
                                {
                                    var new_rego = new Events_Registration();
                                    new_rego.Contact_ID = cartModel.ContactId;
                                    new_rego.GroupLeader_ID = null;
                                    new_rego.FamilyRegistration = false;
                                    new_rego.RegisteredByFriend_ID = null;
                                    new_rego.Venue_ID = cartModel.Event.venueID;
                                    new_rego.RegistrationType_ID = i.reg_id;
                                    new_rego.RegistrationDiscount = 0;
                                    new_rego.Elective_ID = null;
                                    new_rego.Accommodation = false;
                                    new_rego.Accommodation_ID = null;
                                    new_rego.Catering = false;
                                    new_rego.CateringNeeds = "";
                                    new_rego.LeadershipBreakfast = false;
                                    new_rego.LeadershipSummit = false;
                                    new_rego.ULG_ID = null;
                                    new_rego.ParentGuardian = "";
                                    new_rego.ParentGuardianPhone = "";
                                    new_rego.AcceptTermsRego = true;
                                    new_rego.AcceptTermsParent = false;
                                    new_rego.AcceptTermsCreche = false;
                                    new_rego.RegistrationDate = DateTime.Now;
                                    new_rego.RegisteredBy_ID = cartModel.ContactId;
                                    new_rego.Attended = false;
                                    new_rego.Volunteer = false;
                                    new_rego.VolunteerDepartment_ID = null;
                                    new_rego.ComboRegistration_ID = null;

                                    context.Events_Registration.Add(new_rego);

                                    context.SaveChanges();
                                    i.cart_item_id = new_rego.Registration_ID;
                                }
                            }
                            else
                            {
                                var isGroupLeader = ((from eg in context.Events_Group
                                                      where eg.GroupLeader_ID == cartModel.ContactId
                                                      select eg).FirstOrDefault() != null ? true : false);

                                if (!isGroupLeader)
                                {
                                    var new_group = new Events_Group();
                                    new_group.GroupLeader_ID = cartModel.ContactId;
                                    new_group.GroupName = cc.FirstName + " " + cc.LastName;
                                    new_group.GroupColeaders = String.Empty;
                                    new_group.YouthLeaderName = String.Empty;
                                    new_group.GroupSize_ID = null;
                                    new_group.GroupCreditStatus_ID = 1;
                                    new_group.GroupType_ID = 1;
                                    new_group.Comments = String.Empty;
                                    new_group.DetailsVerified = false;
                                    new_group.Deleted = false;
                                    new_group.CreationDate = DateTime.Now;
                                    new_group.CreatedBy_ID = getContactDetails().Contact_ID;
                                    new_group.ModificationDate = null;
                                    new_group.ModifiedBy_ID = null;
                                    new_group.GUID = Guid.NewGuid();

                                    context.Events_Group.Add(new_group);
                                }

                                context.SaveChanges();

                                foreach (var i in cartModel.Cart)
                                {
                                    var new_bulk_rego = new Events_GroupBulkRegistration();
                                    new_bulk_rego.GroupLeader_ID = cartModel.ContactId;
                                    new_bulk_rego.Venue_ID = cartModel.Event.venueID;
                                    new_bulk_rego.RegistrationType_ID = i.reg_id;
                                    new_bulk_rego.Quantity = i.quantity;
                                    new_bulk_rego.DateAdded = DateTime.Now;
                                    new_bulk_rego.Deleted = false;

                                    context.Events_GroupBulkRegistration.Add(new_bulk_rego);
                                    context.SaveChanges();

                                    i.cart_item_id = new_bulk_rego.GroupBulkRegistration_ID;
                                }
                            }

                            Session["Cart"] = cartModel;
                            #endregion    
                        }
                    }
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = "Error creating registrations";
                    throw e;
                }
            }
        }

        #region Only for BW21
        private int createPayment(decimal cost, bool refund, int paymenttype_id, string transactionRef, int contact_id, int conf_id, int bankaccount_id, string comment)
        {
            var now = DateTime.Now;

            using (var ctx = new PlanetshakersEntities())
            {
                var new_group_payment = new Events_GroupPayment()
                {
                    GroupLeader_ID = contact_id,
                    Conference_ID = conf_id,
                    PaymentType_ID = paymenttype_id,
                    PaymentAmount = (refund) ? cost * -1 : cost,
                    CCNumber = String.Empty,
                    CCExpiry = String.Empty,
                    CCName = String.Empty,
                    CCPhone = String.Empty,
                    CCTransactionRef = transactionRef,
                    CCRefund = refund,
                    ChequeDrawer = String.Empty,
                    ChequeBank = String.Empty,
                    ChequeBranch = String.Empty,
                    PaypalTransactionRef = String.Empty,
                    Comment = String.Empty,
                    PaymentStartDate = now,
                    PaymentCompleted = false,
                    PaymentBy_ID = contact_id,
                    BankAccount_ID = bankaccount_id
                };

                ctx.Events_GroupPayment.Add(new_group_payment);
                ctx.SaveChanges();

                return new_group_payment.GroupPayment_ID;
            }
        }
        #endregion
        private void createGroupRegistrations(List<int> regotype_ids, int contact_id, int venue_id, int qty)
        {
            foreach (int regotype_id in regotype_ids)
            {
                createGroupRegistration(contact_id, venue_id, regotype_id, qty);
            }
        }
        private void createGroupRegistration(int contact_id, int venue_id, int rt_id, int qty)
        {
            var now = DateTime.Now;

            using (var ctx = new PlanetshakersEntities())
            {
                var new_bulk_rego = new Events_GroupBulkRegistration();
                new_bulk_rego.GroupLeader_ID = contact_id;
                new_bulk_rego.Venue_ID = venue_id;
                new_bulk_rego.RegistrationType_ID = rt_id;
                new_bulk_rego.Quantity = qty;
                new_bulk_rego.DateAdded = now;
                new_bulk_rego.Deleted = false;

                ctx.Events_GroupBulkRegistration.Add(new_bulk_rego);
                ctx.SaveChanges();
            }
        }

        private int InsertPayment(string transactionRef, bool isGroup)
        {
            var payment_id = 0;
            var cartModel = Session["Cart"] as CartModel;

            if (ModelState.IsValid)
            {
                try
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var bank_account_id = (from c in context.Events_Conference
                                               where c.Conference_ID == cartModel.Event.id
                                               select c.BankAccount_ID).FirstOrDefault();

                        #region Create Contact External (if doesn't exist)
                        Common_ContactExternal ce = (from ce1 in context.Common_ContactExternal
                                                     where ce1.Contact_ID == cartModel.ContactId
                                                     select ce1).FirstOrDefault();
                        if (ce == null)
                        {
                            ce = new Common_ContactExternal();
                            ce.Contact_ID = cartModel.ContactId;
                            ce.ChurchName = "";
                            ce.Denomination_ID = 49;
                            ce.SeniorPastorName = "";
                            ce.ChurchAddress1 = "";
                            ce.ChurchAddress2 = "";
                            ce.ChurchSuburb = "";
                            ce.ChurchPostcode = "";
                            ce.ChurchState_ID = null;
                            ce.ChurchStateOther = "";
                            ce.ChurchCountry_ID = null;
                            ce.ChurchWebsite = "";
                            ce.LeaderName = "";
                            ce.ChurchNumberOfPeople = "";
                            ce.ChurchPhone = "";
                            ce.ChurchEmail = "";
                            ce.ContactDetailsVerified = true;
                            ce.Deleted = false;
                            ce.Inactive = false;

                            context.Common_ContactExternal.Add(ce);
                            context.SaveChanges();
                        }

                        Common_ContactExternalEvents cee = (from cee1 in context.Common_ContactExternalEvents
                                                            where cee1.Contact_ID == cartModel.ContactId
                                                            select cee1).FirstOrDefault();
                        if (cee == null)
                        {
                            //cee = new Common_ContactExternalEvents();
                            //cee.Contact_ID = cartModel.ContactId;
                            //cee.EmergencyContactName = "";
                            //cee.EmergencyContactPhone = "";
                            //cee.EmergencyContactRelationship = "";
                            //cee.MedicalAllergies = "";
                            //cee.MedicalInformation = "";
                            //cee.AccessibilityInformation = "";
                            //cee.EventComments = "";
                            //context.Common_ContactExternalEvents.Add(cee);
                            //context.SaveChanges();
                        }

                        Common_Contact cc = (from cc1 in context.Common_Contact
                                             where cc1.Contact_ID == cartModel.ContactId
                                             select cc1).FirstOrDefault();

                        #endregion

                        if (isGroup)
                        {
                            #region Create Group (if doesn't exist)
                            var isGroupLeader = ((from eg in context.Events_Group
                                                  where eg.GroupLeader_ID == cartModel.ContactId
                                                  select eg).FirstOrDefault() != null ? true : false);

                            if (!isGroupLeader)
                            {
                                var new_group = new Events_Group();
                                new_group.GroupLeader_ID = cartModel.ContactId;
                                new_group.GroupName = cc.FirstName + " " + cc.LastName;
                                new_group.GroupColeaders = String.Empty;
                                new_group.YouthLeaderName = String.Empty;
                                new_group.GroupSize_ID = null;
                                new_group.GroupCreditStatus_ID = 1;
                                new_group.GroupType_ID = 1;
                                new_group.Comments = String.Empty;
                                new_group.DetailsVerified = false;
                                new_group.Deleted = false;
                                new_group.CreationDate = DateTime.Now;
                                new_group.CreatedBy_ID = getContactDetails().Contact_ID;
                                new_group.ModificationDate = null;
                                new_group.ModifiedBy_ID = null;
                                new_group.GUID = Guid.NewGuid();

                                context.Events_Group.Add(new_group);
                                context.SaveChanges();
                            }

                            #endregion

                            #region Insert Group Payment Log into database
                            //payment source is 1 if its deposit (application) otherwise applicant (outstandings)
                            var regoType_ID = cartModel.Cart[0].reg_id;
                            var paymentSource_ID = (isApplicationRego(regoType_ID)) ? 1 : 2;
                            var paymenttype_id = 3; //Credit Card
                            //add comment if payment is for add on 
                            var comment = (isAddOnRegistrationType(regoType_ID)) ? "Payment for Add On: " + getRegistrationTypeName(regoType_ID) : "";

                            if (cartModel.GrandTotal <= 0 && cartModel.Voucher != null)
                            { // Voucher - not used
                                paymenttype_id = 10; 

                                payment_id = createGroupPayment(cartModel, paymenttype_id, paymentSource_ID, transactionRef);
                                cartModel.CCPayment.paymenttype_id = paymenttype_id;

                            }
                            else if (cartModel.GrandTotal > 0 && cartModel.Voucher != null && !String.IsNullOrEmpty(transactionRef))
                            { // voucher + cc - not used
                                createGroupPayment(cartModel, paymenttype_id = 10, paymentSource_ID, String.Empty); // insert voucher payment

                                payment_id = createGroupPayment(cartModel, paymenttype_id = 3, paymentSource_ID, transactionRef); // insert card payment
                                cartModel.CCPayment.paymenttype_id = paymenttype_id;
                            }
                            else
                            { // cc
                                payment_id = createGroupPayment(cartModel, paymenttype_id, paymentSource_ID, transactionRef, comment);
                                cartModel.CCPayment.paymenttype_id = paymenttype_id;
                            }
                            #endregion
                        }
                        else
                        {
                            #region Insert Contact Payment Log to Database           
                            var new_contact_payment = new Events_ContactPayment();
                            new_contact_payment.Contact_ID = cartModel.ContactId;
                            new_contact_payment.Conference_ID = cartModel.Event.id;
                            new_contact_payment.PaymentType_ID = 3; // Credit Card
                            new_contact_payment.PaymentAmount = cartModel.GrandTotal;
                            new_contact_payment.CCNumber = String.Empty;
                            new_contact_payment.CCExpiry = String.Empty;
                            new_contact_payment.CCName = String.Empty;
                            new_contact_payment.CCPhone = String.Empty;
                            new_contact_payment.CCManual = false;
                            new_contact_payment.CCTransactionRef = transactionRef;
                            new_contact_payment.CCRefund = false;
                            new_contact_payment.ChequeDrawer = String.Empty;
                            new_contact_payment.ChequeBank = String.Empty;
                            new_contact_payment.ChequeBranch = String.Empty;
                            new_contact_payment.PaypalTransactionRef = String.Empty;
                            new_contact_payment.Comment = String.Empty;
                            new_contact_payment.PaymentDate = DateTime.Now;
                            new_contact_payment.PaymentBy_ID = cartModel.ContactId;
                            new_contact_payment.BankAccount_ID = 4;

                            context.Events_ContactPayment.Add(new_contact_payment);
                            context.SaveChanges();
                            #endregion

                            payment_id = new_contact_payment.ContactPayment_ID;
                        }
                    }
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = "Error creating payment log";
                    throw e;
                }
            }

            Session["Cart"] = cartModel;

            return payment_id;
        }

        private int InsertVariablePayment(string transactionRef)
        {
            var payment_id = 0;

            if (ModelState.IsValid)
            {
                try
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var cartModel = Session["SponsorshipCart"] as SponsorshipCartModel;

                        var bank_account_id = (from c in context.Events_Conference
                                               where c.Conference_ID == cartModel.Event.id
                                               select c.BankAccount_ID).FirstOrDefault();

                        #region Insert Group Payment Log into database
                        var new_variable_payment = new Events_VariablePayment();
                        new_variable_payment.IncomingContact_ID = cartModel.ContactId;
                        new_variable_payment.Conference_ID = cartModel.Event.id;
                        new_variable_payment.PaymentType_ID = 3; // Credit Card
                        new_variable_payment.PaymentAmount = cartModel.GrandTotal;
                        new_variable_payment.CCTransactionReference = transactionRef;
                        new_variable_payment.Refund = false;
                        new_variable_payment.OutgoingContact_ID = null;
                        new_variable_payment.Comment = String.Empty;
                        new_variable_payment.PaymentDate = DateTime.Now;
                        new_variable_payment.PaymentCompletedDate = null;
                        new_variable_payment.PaymentCompleted = false;
                        new_variable_payment.PaymentBy_ID = cartModel.ContactId;
                        new_variable_payment.BankAccount_ID = bank_account_id;

                        context.Events_VariablePayment.Add(new_variable_payment);
                        context.SaveChanges();
                        #endregion

                        payment_id = new_variable_payment.VariablePayment_ID;
                    }
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = "Error creating payment log";
                    throw e;
                }
            }

            return payment_id;
        }

        public bool insertOutgoingVariablePayment(int outGoingContact_ID, int payment_id, bool Anonymous)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var incomingVariablePayment = (from v in context.Events_VariablePayment where v.VariablePayment_ID == payment_id select v).FirstOrDefault();
                var outgoingPayment = new Events_VariablePayment()
                {
                    IncomingContact_ID = null,
                    Conference_ID = incomingVariablePayment.Conference_ID,
                    OutgoingContact_ID = outGoingContact_ID,
                    PaymentType_ID = incomingVariablePayment.PaymentType_ID,
                    PaymentAmount = incomingVariablePayment.PaymentAmount,
                    CCTransactionReference = "ref: " + incomingVariablePayment.CCTransactionReference,
                    Refund = false,
                    Comment = "Auto transfer of payment_id: " + payment_id,
                    PaymentDate = DateTime.Now,
                    PaymentBy_ID = (int)incomingVariablePayment.IncomingContact_ID,
                    BankAccount_ID = incomingVariablePayment.BankAccount_ID,
                    PaymentCompletedDate = DateTime.Now,
                    PaymentCompleted = true,
                    Anonymous = Anonymous
                };
                incomingVariablePayment.Anonymous = Anonymous;
                context.Events_VariablePayment.Add(outgoingPayment);
                context.SaveChanges();
                return true;
            }
        }

        private int createGroupPayment(CartModel model, int paymenttype_id, int paymentSource_ID, string transactionRef, string Comment = "")
        {
            using (var ctx = new PlanetshakersEntities())
            {
                var new_payment = new Events_GroupPayment();
                
                    new_payment.GroupLeader_ID = model.ContactId;
                    new_payment.PaymentBy_ID = model.ContactId;
                

                new_payment.Conference_ID = model.Event.id;

                if (paymenttype_id == 10 && model.Voucher != null) // insert payment by voucher
                {
                    new_payment.PaymentType_ID = paymenttype_id;
                    new_payment.PaymentAmount = model.Voucher.applied_value;
                    new_payment.Voucher_ID = model.Voucher.id;

                    new_payment.CCTransactionRef = String.Empty;
                }
                else
                {
                    new_payment.PaymentType_ID = paymenttype_id;
                    new_payment.PaymentAmount = model.GrandTotal;

                    new_payment.CCTransactionRef = transactionRef;
                }

                new_payment.PaymentSource_ID = paymentSource_ID;
                new_payment.Comment = Comment;
                new_payment.PaymentStartDate = DateTime.Now;
                new_payment.PaymentCompleted = false;

                new_payment.CCNumber = String.Empty;
                new_payment.CCExpiry = String.Empty;
                new_payment.CCName = String.Empty;
                new_payment.CCPhone = String.Empty;
                new_payment.CCManual = false;
                new_payment.CCRefund = false;
                new_payment.ChequeDrawer = String.Empty;
                new_payment.ChequeBank = String.Empty;
                new_payment.ChequeBranch = String.Empty;
                new_payment.PaypalTransactionRef = String.Empty;
                new_payment.BankAccount_ID = model.CCPayment.BankAccount.BankAccount_ID;


                ctx.Events_GroupPayment.Add(new_payment);
                ctx.SaveChanges();

                return new_payment.GroupPayment_ID;
            }
        }

        private int markPaymentCompleted(int payment_id, string transactionRef, string type)
        {
            var now = DateTime.Now;
            using (var context = new PlanetshakersEntities())
            {
                if (type == "sponsorship")
                {
                    var sponsor = new Events_VariablePayment();

                    // if there is no transaction reference, refer to payment id
                    if (!String.IsNullOrEmpty(transactionRef) && !transactionRef.Contains("ref:"))
                    {
                        sponsor = (from vp in context.Events_VariablePayment
                                   where vp.CCTransactionReference == transactionRef
                                   select vp).FirstOrDefault();
                    }
                    else
                    {
                        sponsor = (from vp in context.Events_VariablePayment
                                   where vp.VariablePayment_ID == payment_id
                                   select vp).FirstOrDefault();
                    }

                    if (sponsor != null)
                    {
                        sponsor.PaymentCompletedDate = now;
                        sponsor.PaymentCompleted = true;

                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception();
                    }

                    return sponsor.VariablePayment_ID;
                }
                else if (type == "purchase")
                {
                    var payment = new Events_GroupPayment();

                    // if there is no transaction reference, refer to payment id
                    if (!String.IsNullOrEmpty(transactionRef) && !transactionRef.Contains("ref:"))
                    {
                        payment = (from gp in context.Events_GroupPayment
                                   where gp.CCTransactionRef == transactionRef
                                   select gp).FirstOrDefault();
                    }
                    else
                    {
                        payment = (from gp in context.Events_GroupPayment
                                   where gp.GroupPayment_ID == payment_id
                                   select gp).FirstOrDefault();
                    }
                    if (payment != null)
                    {
                        payment.PaymentCompletedDate = now;
                        payment.PaymentCompleted = true;

                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception();
                    }
                    return payment.GroupPayment_ID;
                }
                return 0;
            }
        }
        #endregion

        private bool isApplicationRego(int RegistrationType_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var isApplication = (from rt in context.Events_RegistrationType
                                     where rt.RegistrationType_ID == RegistrationType_ID
                                     select rt.ApplicationRegistrationType).FirstOrDefault();
                return isApplication;
            }
        }

        private bool isAddOnRegistrationType(int RegoType_ID)
        {
            using (var context = new PlanetshakersEntities())
            {
                var isAddOn = (from rt in context.Events_RegistrationType
                               where rt.RegistrationType_ID == RegoType_ID && rt.AddOnRegistrationType == true
                               select rt).Any();
                return isAddOn;
            }
        }
        private string getRegistrationTypeName(int RegistrationType_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var name = (from rt in context.Events_RegistrationType
                                     where rt.RegistrationType_ID == RegistrationType_ID
                                     select rt.RegistrationType).FirstOrDefault();
                return name;
            }
        }
    }
}