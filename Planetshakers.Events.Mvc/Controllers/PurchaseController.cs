﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Business;
using Planetshakers.Events.Mvc.Models;
using Planetshakers.Events.Mvc.Filters;
using log4net;
using Stripe;
using Stripe.Checkout;
using System.Configuration;
using System.Web.Security;
using System.Threading.Tasks;
using SelectPdf;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;

namespace Planetshakers.Events.Mvc.Controllers
{
    public class PurchaseController : Controller
    {
        // Initiate Logger
        private ILog pxlogger = LogManager.GetLogger("GeneralLogger");

        MailController mailController = new MailController();

        public ActionResult LoginPrompt(string returnUrl)
        {
            var cartModel = Session["Cart"] as CartModel;
            ViewBag.returnUrl = returnUrl;
            // Ideally should always have the cart model by this point, if not present redirect to event index page

            if (cartModel == null)
            {
                return RedirectToAction("Index", "Events");
            }

            // If user is logged in, proceed to summary page
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            if (contact != null)
                return RedirectToAction("Summary", "Payment", new { returnUrl = returnUrl });

            // Otherwise display page as requested
            var model = new LoginPromptModel()
            {
                Event = cartModel.Event
            };
            return View(model);
        }

        [PSAuthorize]
        public ActionResult Summary(string returnUrl, string error = null)
        {
            ViewBag.returnUrl = returnUrl;
            var model = Session["Cart"] as CartModel;

            if (model == null)
            {
                model = new CartModel()
                {
                    Cart = null
                };
            }

            // Retrieve logged in contact from session
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

            if (contact == null)
                return RedirectToAction("LoginPrompt", new { returnUrl = returnUrl });

            // Update model
            model.ContactId = contact.Id;

            // Setup the CCPayment model            
            if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

            // remove error message for session model
            // Session["Cart"] = model;

            model.ErrorMessage = error;

            /* Retrieve Bank Account information via Conference_ID to pull the correct publishable key for Stripe */
            /*using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                model.CCPayment.BankAccount = (from ec in context.Events_Conference
                                               join ba in context.Common_BankAccount
                                                on ec.BankAccount_ID equals ba.BankAccount_ID
                                               where ec.Conference_ID == model.Event.id
                                               select ba).FirstOrDefault();
            }

            var successUrl = "http://" + HttpContext.Request.Url.Authority + "/Purchase/addRegistration?venue_ID=" + model.Event.venueID;
            var cancelUrl = HttpContext.Request.Url.AbsoluteUri;

            StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;

            var options = new Stripe.Checkout.SessionCreateOptions
            {
                PaymentMethodTypes = new List<string> { "card" },
                LineItems = new List<SessionLineItemOptions> { },
                Mode = "payment",
                PaymentIntentData = new SessionPaymentIntentDataOptions { },
                SuccessUrl = successUrl,
                CancelUrl = cancelUrl
            };
            
            foreach (var item in model.Cart)
            {
                options.LineItems.Add(
                    new SessionLineItemOptions 
                    {
                        PriceData = new SessionLineItemPriceDataOptions
                        {
                            Currency = model.CCPayment.BankAccount.Currency,
                            ProductData = new SessionLineItemPriceDataProductDataOptions
                            {
                                Name = model.Event.name + " - " + item.reg_name
                            },
                            UnitAmount = Convert.ToInt32(item.single_price) * 100
                        },
                        Quantity = item.quantity
                    });
            }

            var service = new Stripe.Checkout.SessionService();
            options.PaymentIntentData.Description = model.Event.name;
            Stripe.Checkout.Session session = service.Create(options);

            model.CCPayment.transactionReference = session.PaymentIntentId;

            Session["TransactionReference"] = session.PaymentIntentId;

            ViewData["SessionID"] = session.Id;*/

            // remove error message for session model
            Session["Cart"] = model;

            //return View(model);

            return RedirectToAction("Summary", "Payment", new { returnUrl = returnUrl });
        }

        public ActionResult addRegistration(int venue_id)
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var cartModel = Session["Cart"] as CartModel;
            //var session = Session["Stripe"] as Stripe.Checkout.Session;
            var transactionRef = Session["TransactionReference"] as String;

            if (Session["Cart"] == null)
            {
                return RedirectToAction("Summary", "Purchase", new { error = "Error trying to retrieve cart. If this persists, please contact info@believeglobal.org." });
            } else if (contact == null)
            {
                return RedirectToAction("Summary", "Purchase", new { error = "Error trying to retrieve account. Please contact info@believeglobal.org for assistance." });
            }
            else if (cartModel.Event.id <= 0)
            {
                return RedirectToAction("Summary", "Purchase", new { error = "Event error. Please contact info@believeglobal.org for assistance." });
            } else if (String.IsNullOrEmpty(transactionRef))
            {
                return RedirectToAction("Summary", "Purchase", new { error = "Transaction reference not found. Please contact info@believeglobal.org for assistance." });
            }

            if (cartModel.ContactId <= 0)
            {
                cartModel.ContactId = contact.Id;
            }

            cartModel.CCPayment.transactionReference = transactionRef;

            string Category = "Add Rego Start";
            string Criteria = "Venue ID : " + cartModel.Cart.First().event_venue_id.ToString();
            string ContactID = cartModel.ContactId.ToString();
            Business.MWebMain.LogAction(ref Category, ref Criteria, ContactID);

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {

                cartModel.CCPayment.BankAccount = (from ev in context.Events_Venue
                                                   join ec in context.Events_Conference
                                                   on ev.Conference_ID equals ec.Conference_ID
                                                   join ba in context.Common_BankAccount
                                                   on ec.BankAccount_ID equals ba.BankAccount_ID
                                                   where ev.Venue_ID == venue_id
                                                   select ba).FirstOrDefault();

                #region Create Group (if it doesn't exist)
                // create or update group
                var group = new EventGroup(cartModel.ContactId);
                group.SetGroupSize(cartModel.Quantity);
                group.SetCreditStatus(GroupCreditStatus.NonCredit);
                group.SetGroupType(GroupType.MissionGroup);

                try
                {
                    group.Save(100);
                }
                catch (Exception ex)
                {
                    // Notify developer of error
                    var dev_email = new DevErrorNotificationEmailModel();
                    dev_email.ex = ex;
                    // new MailController().DevErrorNotification(dev_email).Deliver();

                    throw new Exception("An error occured while creating the registrations.  Please email info@believeglobal.org for further support.");
                }

                #endregion

                #region Create Registrations

                // Add registrations
                // add application
                var recently_added = new List<EventGroupBulkRegistration>();
                foreach (CartItem item in cartModel.Cart)
                {
                    // create an entry for the registration
                    pxlogger.Info("Creating entry for registration.");

                    recently_added.Add(
                        group.AddRegistrations(item.event_venue_id, item.reg_id, item.quantity)
                    );
                }

                try
                {
                    // Save changes
                    group.Save(100);
                }
                catch (Exception ex)
                {
                    // Notify developer of error
                    var dev_email = new DevErrorNotificationEmailModel();
                    dev_email.ex = ex;
                    // new MailController().DevErrorNotification(dev_email).Deliver();

                    throw new Exception("An error occured while creating the registrations.  Please email info@believeglobal.org for further support.");
                }

                #endregion

                #region Insert Payments into database

                // check regotype_id, if not -1 -> deposit, otherwise default to applicant
                // payment type is always credit card (through stripe)
                // Create new  group payment
                var group_payment = new Events_GroupPayment();
                group_payment.GroupLeader_ID = cartModel.ContactId;
                group_payment.Conference_ID = cartModel.Event.id;
                group_payment.PaymentType_ID = (int)PaymentType.CreditCard; // Credit Card Payment
                group_payment.PaymentAmount = cartModel.GrandTotal;
                group_payment.CCNumber = String.Empty;
                group_payment.CCExpiry = String.Empty;
                group_payment.CCName = String.Empty;
                group_payment.CCPhone = String.Empty;
                group_payment.CCManual = false;
                group_payment.CCTransactionRef = cartModel.CCPayment.transactionReference;
                group_payment.CCRefund = false;
                group_payment.ChequeDrawer = String.Empty;
                group_payment.ChequeBank = String.Empty;
                group_payment.ChequeBranch = String.Empty;
                group_payment.PaypalTransactionRef = String.Empty;
                group_payment.Comment = String.Empty;
                group_payment.PaymentStartDate = DateTime.Now;
                group_payment.PaymentCompleted = true;
                group_payment.PaymentBy_ID = cartModel.ContactId;
                group_payment.BankAccount_ID = cartModel.CCPayment.BankAccount.BankAccount_ID; // Stripe Church for PlanetBusiness otherwise Stripe Ministries

                context.Events_GroupPayment.Add(group_payment);

                try
                {
                    // Save changes                                                               
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    // Notify developer of error
                    var dev_email = new DevErrorNotificationEmailModel();
                    dev_email.ex = ex;
                    dev_email.extraNotes = "transactionRef :: " + cartModel.CCPayment.transactionReference + " | Contact_ID :: " + cartModel.ContactId;
                    // new MailController().DevErrorNotification(dev_email).Deliver();

                    throw new Exception("An internal error has occured.  Please email info@believeglobal.org for further support.");
                }
                #endregion

                #region Email Confirmation
                // Prepare the confirmation email model
                pxlogger.Info("Preparing confirmation email model.");
                var email_model = new RegistrationReceipt()
                {
                    Email = contact.Email,
                    banner_img = "https://register.planetshakers.com/Content/themes/planetshakers/img/banner_" + cartModel.Event.venueID + ".jpg",
                    Items = new List<RegistrationConfirmationItem>(),
                    FullName = contact.FirstName + " " + contact.LastName,
                    Subject = "Registration Confirmation - " + cartModel.Event.name,
                    EventName = cartModel.Event.name,
                    EventDate = cartModel.Event.date,
                    EventVenue = cartModel.Event.location,
                    Currency = cartModel.Event.currency,
                    Promo = (cartModel.Voucher != null ? cartModel.Voucher : null),
                    ReceiptNumber = group_payment.GroupPayment_ID.ToString(),
                    PaymentType = (cartModel.CCPayment != null ? cartModel.CCPayment.PaymentType : "N/A"),
                    TransactionReference = (group_payment != null ? (!String.IsNullOrEmpty(group_payment.CCTransactionRef) ? group_payment.CCTransactionRef : "N/A") : "N/A")
                };

                // Add in the registrations to the email
                foreach (CartItem item in cartModel.Cart)
                {
                    email_model.Items.Add(new RegistrationConfirmationItem
                    {
                        reference_number = group_payment.GroupPayment_ID.ToString(),
                        reg_type_id = item.reg_id,
                        reg_count = item.quantity,
                        qty = item.quantity,
                        price = item.single_price
                    });
                }

                // Prepare to send email
                pxlogger.Info("Sending Tax Invoice...");
                
                new MailController().RegistrationReceiptEmail(email_model);

                #endregion
            }

            Category = "Add Rego Complete";
            Criteria = "Venue ID : " + cartModel.Cart.First().event_venue_id.ToString();
            ContactID = cartModel.ContactId.ToString();
            Business.MWebMain.LogAction(ref Category, ref Criteria, ContactID);

            return RedirectToAction("Confirmation", "Purchase", new { venue_ID = cartModel.Event.venueID });
        }

        public ActionResult cancelRegistration(int venue_ID, string returnUrl)
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var cartModel = Session["Cart"] as CartModel;
            var transactionRef = Session["TransactionReference"] as String;

            StripeConfiguration.ApiKey = cartModel.CCPayment.BankAccount.SecretKey;

            var service = new PaymentIntentService();
            service.Cancel(transactionRef);

            return RedirectToAction("LoginPrompt", new { returnUrl = returnUrl });
        }

        [HttpPost]
        [PSAuthorize]
        public ActionResult Payment(CCPaymentModel model, string stripeEmail, string stripeToken)
        {
            try
            {
                #region Validate parameters
                if (model == null)
                {
                    throw new Exception("An unknown error occured.");
                }

                if (stripeToken == null)
                {
                    throw new Exception("An unknown error occured.");
                }

                if (stripeEmail == null)
                {
                    throw new Exception("An unknown error occured.");
                }
                #endregion

                // Retrieve logged in contact details
                var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                if (Session["Cart"] == null || contact == null)
                {
                    return RedirectToAction("Summary", "Purchase", new { error = "Error retrieving cart. If this persists, please contact info@believeglobal.org." });
                }

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Retrieve some cart data from session
                    // Need to add more checks here
                    var cartModel = Session["Cart"] as CartModel;

                    if (cartModel.Event.id <= 0)
                    {
                        return RedirectToAction("Summary", "Purchase", new { error = "Event error. Please contact info@believeglobal.org for assistance." });
                    }

                    if (cartModel.ContactId <= 0)
                    {
                        cartModel.ContactId = contact.Id;
                    }

                    #region Check whether or not this contact has an External and ExternalEvent object and create
                    int cID = cartModel.ContactId;

                    if (cID <= 0)
                    {
                        return RedirectToAction("Summary", "Purchase", new { error = "Authentication error. Please contact info@believeglobal.org for assistance." });
                    }

                    var has_external = (from ce in context.Common_ContactExternal
                                        where ce.Contact_ID == cID
                                        select ce).FirstOrDefault();

                    if (has_external == null)
                    {
                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.Contact_ID = cartModel.ContactId;
                        new_external_contact.ChurchAddress1 = "";
                        new_external_contact.ChurchAddress2 = "";
                        new_external_contact.ChurchCountry_ID = null;
                        new_external_contact.ChurchEmail = "";
                        new_external_contact.ChurchName = "";
                        new_external_contact.ChurchNumberOfPeople = "";
                        new_external_contact.ChurchPhone = "";
                        new_external_contact.ChurchPostcode = "";
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = "";
                        new_external_contact.ChurchSuburb = "";
                        new_external_contact.ChurchWebsite = "";
                        new_external_contact.Denomination_ID = 49;
                        new_external_contact.SeniorPastorName = "";
                        new_external_contact.LeaderName = "";
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;

                        context.Common_ContactExternal.Add(new_external_contact);
                        context.SaveChanges();
                    }

                    var has_external_event = (from cee in context.Common_ContactExternalEvents
                                              where cee.Contact_ID == cID
                                              select cee).ToList();

                    if (!has_external_event.Any())
                    {
                        // create new common_contactExternalEvents
                        //var new_contact_event = new Common_ContactExternalEvents();
                        //new_contact_event.Contact_ID = cartModel.ContactId;
                        //new_contact_event.EmergencyContactName = "";
                        //new_contact_event.EmergencyContactPhone = "";
                        //new_contact_event.EmergencyContactRelationship = "";
                        //new_contact_event.MedicalInformation = "";
                        //new_contact_event.MedicalAllergies = "";
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "";

                        //context.Common_ContactExternalEvents.Add(new_contact_event);
                        //context.SaveChanges();
                    }

                    #endregion

                    /* Retrieve Bank Account information via Conference_ID to transfer funds into */
                    var bankAccount = (from ec in context.Events_Conference
                                       join ba in context.Common_BankAccount
                                          on ec.BankAccount_ID equals ba.BankAccount_ID
                                       where ec.Conference_ID == cartModel.Event.id
                                       select ba).FirstOrDefault();

                    model.BankAccount = bankAccount;

                    cartModel.Event.currency = bankAccount.Currency;

                    if (bankAccount == null || String.IsNullOrEmpty(bankAccount.Currency))
                    {
                        ViewBag.Message = "Something went wrong.";
                        return View("Error");
                    }

                    bool is_group_rego = true;
                    var venue_id = cartModel.Cart.First().event_venue_id;

                    // Decide on whether this purchase is a group or single registration.
                    // Criterias:
                    //  - Bought 1 rego and no previous regos, single rego
                    //  - Bought 1 rego and previous rego found, turn all regos to group
                    //  - Bought more than 1 rego, group rego (convert any previously purchased single regos to group)

                    // Removed to allow group registrations for all.
                    /*if (cartModel.Quantity == 1)
                    {
                        // check for previous registrations
                        var single_regos = (from r in context.Events_Registration
                                            where r.Contact_ID == cartModel.ContactId &&
                                                  r.GroupLeader_ID == null &&
                                                  r.Venue_ID == venue_id
                                            select r).ToList();

                        var group_regos = (from r in context.Events_GroupBulkRegistration
                                           where r.GroupLeader_ID == cartModel.ContactId &&
                                                 r.Venue_ID == venue_id
                                           select r).ToList();

                        if (single_regos.Any() || group_regos.Any()) { is_group_rego = true; }
                    }
                    else
                    {
                        is_group_rego = true;
                    }*/


                    // Get connection string from web.config
                    var con = ConfigurationManager.ConnectionStrings["PlanetshakersEntities"].ConnectionString;

                    // Check for Group Registrations
                    if (is_group_rego)
                    {
                        /* ================= Group Payment Process ================= */

                        #region Process Payment
                        // Setup variables to retrieve responses from the bank
                        string eventName = "";
                        foreach (CartItem item in cartModel.Cart)
                        {
                            for (var i = 0; i < item.quantity; i++)
                            {
                                eventName = Convert.ToString(item.event_name);
                            }
                        }

                        string transactionResponse = String.Empty;
                        string transactionReference = String.Empty;

                        bool paymentSuccess = false;

                        // Instantiate stripe service models
                        var customers = new CustomerService();
                        var charges = new ChargeService();

                        // CHANGES REQUIRED
                        /*StripeConfiguration.SetApiKey(bankAccount.SecretKey);

                        // Create stripe token
                        var customer = customers.Create(new CustomerCreateOptions
                        {
                            Email = stripeEmail,
                            SourceToken = stripeToken
                        });

                        // Create stripe charge
                        var amount = Convert.ToInt32(cartModel.GrandTotal) * 100;
                        var charge = charges.Create(new ChargeCreateOptions
                        {
                            Amount = amount,       // amount must be at least $0.50
                            Description = "Event Registration for " + cartModel.Event.name.ToString(),
                            Currency = bankAccount.Currency,
                            CustomerId = customer.Id
                        });

                        if (charge.Status == "succeeded")
                        {
                            paymentSuccess = true;
                        }
                        else
                        {
                            throw new StripeException();
                        }

                        transactionReference = charge.Id.ToString();*/
                        #endregion                                               

                        #region Post Processing
                        if (paymentSuccess)
                        {
                            pxlogger.Info("Payment Success");
                            /* At this point, transaction is successful.
                             * Create all the required rows for registrations and payments
                             */

                            string Category = "Stripe Payment";
                            string Criteria = "Venue ID : " + cartModel.Cart.First().event_venue_id.ToString();
                            string ContactID = cartModel.ContactId.ToString();
                            Business.MWebMain.LogAction(ref Category, ref Criteria, ContactID);


                            #region Create Group (if it doesn't exist)
                            // create or update group
                            var group = new EventGroup(cartModel.ContactId);
                            group.SetGroupSize(cartModel.Quantity);
                            group.SetCreditStatus(GroupCreditStatus.NonCredit);
                            group.SetGroupType(GroupType.MissionGroup);

                            try
                            {
                                group.Save(100);
                            }
                            catch (Exception ex)
                            {
                                // Notify developer of error
                                var dev_email = new DevErrorNotificationEmailModel();
                                dev_email.ex = ex;
                                // new MailController().DevErrorNotification(dev_email).Deliver();

                                throw new Exception("An error occured while creating the registrations.  Please email info@believeglobal.org for further support.");
                            }

                            #endregion

                            #region Convert old single registrations to group

                            // check for previous registrations
                            var single_regos = (from r in context.Events_Registration
                                                where r.Contact_ID == cartModel.ContactId &&
                                                      r.GroupLeader_ID == null &&
                                                      r.Venue_ID == venue_id
                                                select r).ToList();

                            if (single_regos.Any())
                            {
                                // retrieve registration types
                                var rego_types = single_regos.Select(x => x.RegistrationType_ID)
                                                             .Distinct().ToList();
                                // Create bulk registrations
                                foreach (var rego_type in rego_types)
                                {
                                    // Retrieve registration date
                                    var reg_date = single_regos
                                                   .Where(x => x.RegistrationType_ID == rego_type)
                                                   .Select(x => x.RegistrationDate).First();

                                    // retrieve number of rego
                                    var qty = single_regos.Where(x => x.RegistrationType_ID == rego_type).Count();

                                    // create regos
                                    var new_bulk_regos = new Events_GroupBulkRegistration();
                                    new_bulk_regos.GroupLeader_ID = group.groupLeaderID;
                                    new_bulk_regos.Venue_ID = venue_id;
                                    new_bulk_regos.RegistrationType_ID = rego_type;
                                    new_bulk_regos.Quantity = qty;
                                    new_bulk_regos.DateAdded = reg_date;
                                    new_bulk_regos.Deleted = false;

                                    context.Events_GroupBulkRegistration.Add(new_bulk_regos);

                                    /*if (con.Contains("DEV_Planetshakers") || con.Contains("PlanetshakersSG"))
                                    {
                                        var group_ticket = new Events_GroupTicket();
                                        group_ticket.Events_GroupBulkRegistration = new_bulk_regos;
                                        group_ticket.Attended = false;

                                        context.Events_GroupTicket.Add(group_ticket);
                                    }*/

                                    context.SaveChanges();
                                    pxlogger.Info("   ++ TypeID:" + rego_type + " x " + qty);
                                }

                                // Take out one registration and allocate to group leader (if any)           
                                var realloc = single_regos[0];
                                single_regos.RemoveAt(0);
                                realloc.GroupLeader_ID = group.groupLeaderID;
                                realloc.RegisteredBy_ID = group.groupLeaderID;
                                context.SaveChanges();

                                // Delete registrations
                                single_regos.ForEach(x => pxlogger.Info("   -- [" + x.Registration_ID + "] TypeID:" + x.RegistrationType_ID));
                                foreach (var single_rego in single_regos) context.Events_Registration.Remove(single_rego);
                                context.SaveChanges();
                            }
                            #endregion

                            #region Convert old single payments to group

                            // retrieve payments that were from single registrations and
                            // convert them to group payments
                            var contact_payment = (from cp in context.Events_ContactPayment
                                                   join ev in context.Events_Venue
                                                           on cp.Conference_ID equals ev.Conference_ID
                                                   where ev.Venue_ID == venue_id &&
                                                         cp.Contact_ID == cartModel.ContactId
                                                   select cp).ToList();

                            // Create group payments
                            foreach (var payment in contact_payment)
                            {
                                var new_group_payment = new Events_GroupPayment();
                                new_group_payment.GroupLeader_ID = payment.Contact_ID;
                                new_group_payment.Conference_ID = payment.Conference_ID;
                                new_group_payment.PaymentType_ID = payment.PaymentType_ID;
                                new_group_payment.PaymentAmount = payment.PaymentAmount;
                                new_group_payment.CCNumber = payment.CCNumber;
                                new_group_payment.CCExpiry = payment.CCExpiry;
                                new_group_payment.CCName = payment.CCName;
                                new_group_payment.CCPhone = payment.CCPhone;
                                new_group_payment.CCManual = payment.CCManual;
                                new_group_payment.CCTransactionRef = payment.CCTransactionRef;
                                new_group_payment.CCRefund = payment.CCRefund;
                                new_group_payment.ChequeDrawer = payment.ChequeDrawer;
                                new_group_payment.ChequeBank = payment.ChequeBank;
                                new_group_payment.ChequeBranch = payment.ChequeBranch;
                                new_group_payment.PaypalTransactionRef = payment.PaypalTransactionRef;
                                new_group_payment.Comment = payment.Comment;
                                new_group_payment.PaymentCompletedDate = payment.PaymentDate;
                                new_group_payment.PaymentCompleted = true;
                                new_group_payment.PaymentBy_ID = payment.PaymentBy_ID;
                                new_group_payment.BankAccount_ID = payment.BankAccount_ID;

                                // Create new group payment and delete old single payment
                                context.Events_GroupPayment.Add(new_group_payment);
                                context.SaveChanges();
                                pxlogger.Info("   ++ [" + new_group_payment.GroupPayment_ID + "] $" + new_group_payment.PaymentAmount);

                                // Remove single payments
                                context.Events_ContactPayment.Remove(payment);
                                pxlogger.Info("   -- [" + payment.ContactPayment_ID + "] $" + payment.PaymentAmount);
                                context.SaveChanges();
                            }

                            #endregion

                            #region Create Registrations

                            // Add registrations
                            var recently_added = new List<EventGroupBulkRegistration>();
                            foreach (CartItem item in cartModel.Cart)
                            {
                                // create an entry for the registration
                                pxlogger.Info("Creating entry for registration.");

                                recently_added.Add(
                                    group.AddRegistrations(item.event_venue_id, item.reg_id, item.quantity)
                                );

                                #region Boom Camp Exception
                                // exceptions for boom camp full registration - create complimentary boom camp rego
                                if (item.reg_id == 2874 || item.reg_id == 2876)
                                {
                                    #region Convert old single registrations to group
                                    // check for previous registrations
                                    var singles = (from r in context.Events_Registration
                                                   where r.Contact_ID == cartModel.ContactId &&
                                                         r.GroupLeader_ID == null &&
                                                         r.Venue_ID == 142
                                                   select r).ToList();

                                    if (singles.Any())
                                    {
                                        // retrieve registration types
                                        var rego_types = singles.Select(x => x.RegistrationType_ID)
                                                                     .Distinct().ToList();
                                        // Create bulk registrations
                                        foreach (var rego_type in rego_types)
                                        {
                                            // Retrieve registration date
                                            var reg_date = singles
                                                           .Where(x => x.RegistrationType_ID == rego_type)
                                                           .Select(x => x.RegistrationDate).First();

                                            // retrieve number of rego
                                            var qty = singles.Where(x => x.RegistrationType_ID == rego_type).Count();

                                            // create regos
                                            var new_bulk_regos = new Events_GroupBulkRegistration();
                                            new_bulk_regos.GroupLeader_ID = group.groupLeaderID;
                                            new_bulk_regos.Venue_ID = 142;
                                            new_bulk_regos.RegistrationType_ID = rego_type;
                                            new_bulk_regos.Quantity = qty;
                                            new_bulk_regos.DateAdded = reg_date;
                                            new_bulk_regos.Deleted = false;

                                            context.Events_GroupBulkRegistration.Add(new_bulk_regos);
                                            context.SaveChanges();

                                            pxlogger.Info("   ++ TypeID:" + rego_type + " x " + qty);
                                        }

                                        // Delete registrations
                                        singles.ForEach(x => pxlogger.Info("   -- [" + x.Registration_ID + "] TypeID:" + x.RegistrationType_ID));
                                        foreach (var single_rego in singles) context.Events_Registration.Remove(single_rego);
                                        context.SaveChanges();
                                    }
                                    #endregion

                                    recently_added.Add(group.AddRegistrations(142, 2889, item.quantity)); // PS20 Boom Camp Complimentary Registration                                    
                                }
                                #endregion
                            }

                            try
                            {
                                // Save changes
                                group.Save(100);
                            }
                            catch (Exception ex)
                            {
                                // Notify developer of error
                                var dev_email = new DevErrorNotificationEmailModel();
                                dev_email.ex = ex;
                                // new MailController().DevErrorNotification(dev_email).Deliver();

                                throw new Exception("An error occured while creating the registrations.  Please email info@believeglobal.org for further support.");
                            }

                            /*if (con.Contains("DEV_Planetshakers") || con.Contains("PlanetshakersSG"))
                            {
                                foreach (var item in recently_added)
                                {
                                    var group_ticket = new Events_GroupTicket();
                                    group_ticket.GroupBulkRegistration_ID = (from egbr in context.Events_GroupBulkRegistration
                                                                             where egbr.GroupLeader_ID == contact.Id && egbr.Venue_ID == cartModel.Event.venueID
                                                                             orderby egbr.GroupBulkRegistration_ID descending
                                                                             select egbr.GroupBulkRegistration_ID).FirstOrDefault();
                                    group_ticket.Attended = false;

                                    context.Events_GroupTicket.Add(group_ticket);
                                    context.SaveChanges();
                                }
                            }*/
                            #endregion

                            #region Insert Payments into database

                            // Create new  group payment
                            var group_payment = new Events_GroupPayment();
                            group_payment.GroupLeader_ID = cartModel.ContactId;
                            group_payment.Conference_ID = cartModel.Event.id;
                            group_payment.PaymentType_ID = (int)PaymentType.CreditCard; // Credit Card Payment
                            group_payment.PaymentAmount = cartModel.GrandTotal;
                            group_payment.CCNumber = String.Empty;
                            group_payment.CCExpiry = String.Empty;
                            group_payment.CCName = String.Empty;
                            group_payment.CCPhone = String.Empty;
                            group_payment.CCManual = false;
                            group_payment.CCTransactionRef = transactionReference;
                            group_payment.CCRefund = false;
                            group_payment.ChequeDrawer = String.Empty;
                            group_payment.ChequeBank = String.Empty;
                            group_payment.ChequeBranch = String.Empty;
                            group_payment.PaypalTransactionRef = String.Empty;
                            group_payment.Comment = String.Empty;
                            group_payment.PaymentStartDate = DateTime.Now;
                            group_payment.PaymentCompleted = true;
                            group_payment.PaymentBy_ID = cartModel.ContactId;
                            group_payment.BankAccount_ID = bankAccount.BankAccount_ID; // Stripe Church for PlanetBusiness otherwise Stripe Ministries

                            context.Events_GroupPayment.Add(group_payment);

                            try
                            {
                                // Save changes                                                               
                                context.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                // Notify developer of error
                                var dev_email = new DevErrorNotificationEmailModel();
                                dev_email.ex = ex;
                                dev_email.extraNotes = "transactionRef :: " + transactionReference + " | Name :: " + model.CCName + " | Phone :: " + model.CCPhone;
                                // new MailController().DevErrorNotification(dev_email).Deliver();

                                throw new Exception("An internal error has occured.  Please email info@believeglobal.org for further support.");
                            }
                            #endregion

                            #region Email Confirmation
                            // Prepare the confirmation email model
                            pxlogger.Info("Preparing confirmation email model.");
                            var email_model = new RegistrationReceipt()
                            {
                                Email = contact.Email,
                                banner_img = "https://register.planetshakers.com/Content/themes/planetshakers/img/banner_" + cartModel.Event.venueID + ".jpg",
                                Items = new List<RegistrationConfirmationItem>(),
                                FullName = contact.FirstName + " " + contact.LastName,
                                Subject = "Registration Confirmation - " + cartModel.Event.name,
                                EventName = cartModel.Event.name,
                                EventDate = cartModel.Event.date,
                                EventVenue = cartModel.Event.location,
                                Currency = cartModel.Event.currency,
                                Promo = (cartModel.Voucher != null ? cartModel.Voucher : null),
                                ReceiptNumber = group_payment.GroupPayment_ID.ToString(),
                                PaymentType = (cartModel.CCPayment != null ? cartModel.CCPayment.PaymentType : "N/A"),
                                TransactionReference = (group_payment != null ? (!String.IsNullOrEmpty(group_payment.CCTransactionRef) ? group_payment.CCTransactionRef : "N/A") : "N/A")
                            };

                            // Add in the registrations to the email
                            foreach (CartItem item in cartModel.Cart)
                            {
                                email_model.Items.Add(new RegistrationConfirmationItem
                                {
                                    reference_number = group_payment.GroupPayment_ID.ToString(),
                                    reg_type_id = item.reg_id,
                                    reg_count = item.quantity,
                                    qty = item.quantity,
                                    price = item.single_price
                                });
                            }

                            // Prepare to send email
                            pxlogger.Info("Sending Tax Invoice...");                                                    

                            if (con.Contains("PlanetshakersSG") && venue_id == 22)
                            {
                                new MailController().SendSGTaxInvoice(email_model);

                                var ticket_model = new TicketModel(contact.FirstName, contact.Email);

                                foreach (var item in cartModel.Cart)
                                {
                                    for (var i=0; i < item.quantity; i++)
                                    {
                                        var new_rego = new Events_Registration();
                                        new_rego.Contact_ID = cartModel.ContactId;
                                        new_rego.GroupLeader_ID = cartModel.ContactId;
                                        new_rego.FamilyRegistration = false;
                                        new_rego.RegisteredByFriend_ID = null;
                                        new_rego.Venue_ID = item.event_venue_id;
                                        new_rego.RegistrationType_ID = item.reg_id;
                                        new_rego.RegistrationDiscount = 0;
                                        new_rego.Elective_ID = null;
                                        new_rego.Accommodation = false;
                                        new_rego.Accommodation_ID = null;
                                        new_rego.Catering = false;
                                        new_rego.CateringNeeds = "";
                                        new_rego.LeadershipBreakfast = false;
                                        new_rego.LeadershipSummit = false;
                                        new_rego.ULG_ID = null;
                                        new_rego.ParentGuardian = "";
                                        new_rego.ParentGuardianPhone = "";
                                        new_rego.AcceptTermsRego = true;
                                        new_rego.AcceptTermsParent = false;
                                        new_rego.AcceptTermsCreche = false;
                                        new_rego.RegistrationDate = DateTime.Now;
                                        new_rego.RegisteredBy_ID = cartModel.ContactId;
                                        new_rego.Attended = false;
                                        new_rego.Volunteer = false;
                                        new_rego.VolunteerDepartment_ID = null;
                                        new_rego.ComboRegistration_ID = null;                                        

                                        // create an entry for the registration
                                        pxlogger.Info("Creating entry for registration.");

                                        context.Events_Registration.Add(new_rego);

                                        context.SaveChanges();

                                        ticket_model.Tickets.Add(new Ticket { reg_barcode = new_rego.Registration_ID });
                                    }
                                }

                                new MailController().SendTicket(ticket_model);
                            } else
                            {
                                new MailController().RegistrationReceiptEmail(email_model);
                            }

                            #endregion
                        }
                        else
                        {
                            // Remove registrations that have been added
                            throw new Exception("An error occured while processing the payment. "
                                                + transactionResponse);
                        }
                        #endregion
                    }
                    else
                    {
                        /* ================= Single Payment Process ================= */

                        #region Process Payment

                        // Log Payment
                        string Category = "Stripe Payment";
                        string Criteria = "Venue ID : " + cartModel.Cart.First().event_venue_id.ToString();
                        String ContactID = cartModel.ContactId.ToString();
                        Business.MWebMain.LogAction(ref Category, ref Criteria, ContactID);

                        // Setup variables to retrieve responses from the bank
                        var eventName = "";
                        foreach (CartItem item in cartModel.Cart)
                        {
                            for (var i = 0; i < item.quantity; i++)
                            {
                                eventName = Convert.ToString(item.event_name);
                            }
                        }

                        var transactionResponse = String.Empty;
                        var transactionReference = String.Empty;

                        var paymentSuccess = false;

                        //stripe code
                        var customers = new CustomerService();
                        var charges = new ChargeService();

                        /*StripeConfiguration.SetApiKey(bankAccount.SecretKey);

                        //TODO: Should get customer if exist, create only when does not exist
                        var customer = customers.Create(new CustomerCreateOptions
                        {
                            Email = stripeEmail,
                            SourceToken = stripeToken
                        });

                        var amount = Convert.ToInt32(cartModel.GrandTotal) * 100;
                        var charge = charges.Create(new ChargeCreateOptions
                        {
                            Amount = amount,  // amount must be at least 50 cents
                            Description = "Event Registration for " + cartModel.Event.name.ToString(),
                            Currency = bankAccount.Currency,
                            CustomerId = customer.Id
                        });

                        if (charge.Status == "succeeded")
                        {
                            paymentSuccess = true;
                        }
                        else
                        {
                            //TODO: Throw error code, cancel charge and notify customer
                            throw new StripeException();
                        }

                        transactionReference = charge.Id.ToString();*/
                        #endregion

                        #region Post Processing
                        if (paymentSuccess)
                        {
                            pxlogger.Info("Payment success. Add registration and payment to database.");

                            /* At this point, transaction is successful.
                             * Create all the required rows for registrations and payments */

                            #region Create Registrations (Created here so we can check for mistakes before payment)
                            var reg_info = cartModel.Cart.First();

                            var new_rego = new Events_Registration();
                            new_rego.Contact_ID = cartModel.ContactId;
                            new_rego.GroupLeader_ID = null;
                            new_rego.FamilyRegistration = false;
                            new_rego.RegisteredByFriend_ID = null;
                            new_rego.Venue_ID = reg_info.event_venue_id;
                            new_rego.RegistrationType_ID = reg_info.reg_id;
                            new_rego.RegistrationDiscount = 0;
                            new_rego.Elective_ID = null;
                            new_rego.Accommodation = false;
                            new_rego.Accommodation_ID = null;
                            new_rego.Catering = false;
                            new_rego.CateringNeeds = "";
                            new_rego.LeadershipBreakfast = false;
                            new_rego.LeadershipSummit = false;
                            new_rego.ULG_ID = null;
                            new_rego.ParentGuardian = "";
                            new_rego.ParentGuardianPhone = "";
                            new_rego.AcceptTermsRego = true;
                            new_rego.AcceptTermsParent = false;
                            new_rego.AcceptTermsCreche = false;
                            new_rego.RegistrationDate = DateTime.Now;
                            new_rego.RegisteredBy_ID = cartModel.ContactId;
                            new_rego.Attended = false;
                            new_rego.Volunteer = false;
                            new_rego.VolunteerDepartment_ID = null;
                            new_rego.ComboRegistration_ID = null;

                            // create an entry for the registration
                            pxlogger.Info("Creating entry for registration.");

                            context.Events_Registration.Add(new_rego);
                            context.SaveChanges();

                            #region Insert Payments into database 
                            var new_contact_payment = new Events_ContactPayment();

                            new_contact_payment.Contact_ID = cartModel.ContactId;
                            new_contact_payment.Conference_ID = cartModel.Event.id;
                            new_contact_payment.PaymentType_ID = (int)PaymentType.CreditCard; // Credit Card Payment
                            new_contact_payment.PaymentAmount = cartModel.GrandTotal;
                            new_contact_payment.CCNumber = String.Empty;
                            new_contact_payment.CCExpiry = String.Empty;
                            new_contact_payment.CCName = String.Empty;
                            new_contact_payment.CCPhone = String.Empty;
                            new_contact_payment.CCManual = false;
                            new_contact_payment.CCTransactionRef = transactionReference;
                            new_contact_payment.CCRefund = false;
                            new_contact_payment.ChequeDrawer = String.Empty;
                            new_contact_payment.ChequeBank = String.Empty;
                            new_contact_payment.ChequeBranch = String.Empty;
                            new_contact_payment.PaypalTransactionRef = String.Empty;
                            new_contact_payment.Comment = String.Empty;
                            new_contact_payment.PaymentDate = DateTime.Now;
                            new_contact_payment.PaymentBy_ID = cartModel.ContactId;
                            new_contact_payment.BankAccount_ID = (bankAccount.BankAccount_ID); //Stripe Church for PlanetBusiness otherwise Stripe Ministries

                            context.Events_ContactPayment.Add(new_contact_payment);

                            try
                            {
                                // Save changes
                                context.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                // Notify developer of error
                                var dev_email = new DevErrorNotificationEmailModel();
                                dev_email.ex = ex;
                                dev_email.extraNotes = "transactionRef :: " + transactionReference + " | Name :: " + model.CCName + " | Phone :: " + model.CCPhone;
                                // new MailController().DevErrorNotification(dev_email).Deliver();

                                throw new Exception("An internal error has occured.  Please email info@believeglobal.org for further support.");
                            }
                            #endregion

                            #region Email Confirmation

                            /* prepare the confirmation email model */
                            pxlogger.Info("Preparing Confirmation email model");
                            var conference = (from ev in context.Events_Venue
                                              join ec in context.Events_Conference
                                              on ev.Conference_ID equals ec.Conference_ID
                                              where ev.Venue_ID == venue_id
                                              select new Event
                                              {
                                                  id = ec.Conference_ID,
                                                  venueID = ev.Venue_ID,
                                                  name = ec.ConferenceName,
                                                  date = ec.ConferenceDate,
                                                  location = ec.ConferenceLocation,
                                                  venueLocation = ev.VenueName,
                                                  shortname = ec.ConferenceNameShort
                                              }).FirstOrDefault();

                            var confirmationBarcode = new ConfirmationBarcode()
                            {
                                Subject = "Quick Check In - " + conference.name,
                                EventName = conference.name,
                                EventDate = conference.date.ToString(),
                                EventVenue = conference.location,
                                VenueLocation = conference.venueLocation,
                                Email = contact.Email,
                                RegistrationNumber = new_rego.Registration_ID.ToString(),
                                RegistrationTypeID = (int)new_rego.RegistrationType_ID
                            };

                            // Prepare to send 
                            pxlogger.Info("Sending Email ...");
                            if (!String.IsNullOrEmpty(confirmationBarcode.Email))
                            {
                                new MailController().ConfirmationBarcodeEmail(confirmationBarcode);
                            }

                            #endregion

                            #endregion
                        }
                        else
                        {
                            // Remove registrations that have been added
                            throw new Exception("An error occured while processing the payment. "
                                                + transactionResponse);
                        }
                        #endregion                                                
                    }

                    // Clear the Cart 
                    pxlogger.Info("Emptying Cart ...");
                    Session["Cart"] = null; // why is this even here?

                    TempData["recently_registered"] = true;
                    pxlogger.Info("reloading page ...");
                    pxlogger.Info("------------------------------------------------------------------------------------------------------");

                    // Update session.
                    Session["Cart"] = cartModel; // if this is going to be here?

                    // Force website to re-retrieve group registrations
                    Session["registrations_group"] = null;

                    /*if (cartModel.Event.venueID == 150 || cartModel.Event.venueID == 151)
                    {
                        BConference2019 dynamicTemplateData = new BConference2019() { FirstName = contact.FirstName };

                        if (cartModel.Event.venueID == 150)
                        {
                            _ = mailController.SendMailViaPsApi(contact.Email, "d-276156c8148c40adb0980bb00f8b9802", dynamicTemplateData);
                        }
                        if (cartModel.Event.venueID == 151)
                        {
                            _ = mailController.SendMailViaPsApi(contact.Email, "d-8cf7fdea61a24f069e44f2f9a352e5f1", dynamicTemplateData);
                        }

                    }*/

                    return RedirectToAction("Confirmation", "Purchase", new { venue_ID = cartModel.Event.venueID });
                }
            }
            catch (Exception ex)
            {
                var cartModel = Session["Cart"] as CartModel;
                cartModel.CCPayment = model;
                Session["Cart"] = cartModel;
                pxlogger.Info(ex.Message);
                return RedirectToAction("Summary", "Purchase", new { error = ex.Message });
            }
        }

        public ActionResult Confirmation(int venue_ID, bool isAddon = false)
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var cart = Session["Cart"] as CartModel;
            var model = new ConfirmationModel()
            {
                isAddOn = isAddon,
            };
            
            ViewBag.venueID = venue_ID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                ViewBag.EventName = (from ec in context.Events_Conference
                                     join ev in context.Events_Venue
                                        on ec.Conference_ID equals ev.Conference_ID
                                     where ev.Venue_ID == venue_ID
                                     select ec.ConferenceName).FirstOrDefault();
            }

            ViewBag.RegType_ID = cart.Cart.First().reg_id;
            ViewBag.GroupLeader_ID = contact.Id;

            return View(model);
        }
        
        public ActionResult LoginSponsorshipPrompt(string returnUrl)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var cartModel = Session["SponsorshipCart"] as SponsorshipCartModel;
                ViewBag.returnUrl = returnUrl;
                // Ideally should always have the cart model by this point, if not present redirect to event index page

                if (cartModel == null)
                {
                    return RedirectToAction("Index", "Events");
                }
                //find out contact by email 
                //if doesnt exist, create a common_contact
                var contact = (from contact_id in context.Common_Contact where contact_id.Email == cartModel.Email select contact_id).FirstOrDefault();
                if (contact == null)
                {
                    //create a common contact
                    var new_contact = new Common_Contact();
                    new_contact.Salutation_ID = 1;
                    new_contact.FirstName = cartModel.FirstName;
                    new_contact.LastName = cartModel.LastName;
                    new_contact.Gender = "";
                    new_contact.DateOfBirth = null;
                    new_contact.Address1 = "";
                    new_contact.Address2 = "";
                    new_contact.Suburb = "";
                    new_contact.Postcode = "";
                    new_contact.State_ID = null;
                    new_contact.StateOther = "";
                    new_contact.Country_ID = null;
                    new_contact.Phone = "";
                    new_contact.PhoneWork = "";
                    new_contact.Fax = "";
                    new_contact.Mobile = cartModel.Mobile;
                    new_contact.Email = cartModel.Email;
                    new_contact.Email2 = "";
                    new_contact.DoNotIncludeEmail1InMailingList = false;
                    new_contact.DoNotIncludeEmail2InMailingList = false;
                    new_contact.Family_ID = null;
                    new_contact.FamilyMemberType_ID = null;
                    new_contact.Password = ""; 
                    new_contact.VolunteerPoliceCheck = "";
                    new_contact.VolunteerPoliceCheckDate = null;
                    new_contact.VolunteerWWCC = "";
                    new_contact.VolunteerWWCCDate = null;
                    new_contact.VolunteerWWCCType_ID = null;
                    new_contact.CreationDate = DateTime.Now;
                    new_contact.CreatedBy_ID = 100;
                    new_contact.ModificationDate = DateTime.Now;
                    new_contact.ModifiedBy_ID = null;
                    new_contact.GUID = Guid.NewGuid();
                    new_contact.AnotherLanguage_ID = null;
                    new_contact.Nationality_ID = null;
                    context.Common_Contact.Add(new_contact);
                    context.SaveChanges();
                    cartModel.ContactId = new_contact.Contact_ID;
                } else
                {
                    //assign contact id
                    cartModel.ContactId = contact.Contact_ID;
                }

                // Store contact information on session
                Authenticator.Contact c = new Authenticator.Contact();
                c.Id = cartModel.ContactId;
                c.FirstName = cartModel.FirstName;
                c.LastName = cartModel.LastName;
                c.Email = cartModel.Email;
                Session["Contact"] = c;

                // If user is logged in, proceed to summary page
                //var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
                //if (contact != null)
                return RedirectToAction("SponsorshipSummary", "Payment", new { returnUrl = returnUrl });

                //// Otherwise display page as requested
                //var model = new LoginPromptModel()
                //{
                //    Event = cartModel.Event
                //};
                //return View("LoginPrompt", model);

            }
        }

        public ActionResult SponsorshipSummary(string returnUrl, string error = null)
        {
            ViewBag.returnUrl = returnUrl;
            var model = Session["SponsorshipCart"] as SponsorshipCartModel;

            if (model == null)
            {
                model = new SponsorshipCartModel()
                {
                    Cart = null
                };
            }

            // Retrieve logged in contact from session
            var contact = Session["Contact"] as Planetshakers.Events.Mvc.Authenticator.Contact;

            if (contact == null)
                return RedirectToAction("LoginSponsorshipPrompt", new { returnUrl = returnUrl });


            // Update model
            model.ContactId = contact.Id;

            // Setup the CCPayment model            
            if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

            // remove error message for session model
            Session["SponsorshipCart"] = model;

            model.ErrorMessage = error;

            /* Retrieve Bank Account information via Conference_ID to pull the correct publishable key for Stripe */
            /*using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                model.CCPayment.BankAccount = (from ec in context.Events_Conference
                                               join ba in context.Common_BankAccount
                                                on ec.BankAccount_ID equals ba.BankAccount_ID
                                               where ec.Conference_ID == model.Event.id
                                               select ba).FirstOrDefault();
            }


            var successUrl = "http://" + HttpContext.Request.Url.Authority + "/Purchase/addSponsorship?venue_ID=" + model.Event.venueID;
            var cancelUrl = HttpContext.Request.Url.AbsoluteUri;

            StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;

            var options = new Stripe.Checkout.SessionCreateOptions
            {
                PaymentMethodTypes = new List<string> { "card" },
                LineItems = new List<SessionLineItemOptions> { },
                Mode = "payment",
                PaymentIntentData = new SessionPaymentIntentDataOptions { },
                SuccessUrl = successUrl,
                CancelUrl = cancelUrl
            };
            if (model.Cart != null)
            {
                options.LineItems.Add(
                    new SessionLineItemOptions
                    {
                        PriceData = new SessionLineItemPriceDataOptions
                        {
                            Currency = model.CCPayment.BankAccount.Currency,
                            ProductData = new SessionLineItemPriceDataProductDataOptions
                            {
                                Name = model.Event.name + " - Sponsorship"
                            },
                            UnitAmount = Convert.ToInt32(model.Cart.totalSponsoredAmount) * 100
                        },
                        Quantity = 1
                    });
            }

            var service = new Stripe.Checkout.SessionService();
            options.PaymentIntentData.Description = model.Event.name + " -  Sponsorship";
            Stripe.Checkout.Session session = service.Create(options);

            model.CCPayment.transactionReference = session.PaymentIntentId;

            Session["BankAccount"] = model.CCPayment.BankAccount;
            Session["TransactionReference"] = session.PaymentIntentId;

            ViewData["SessionID"] = session.Id;*/

            // remove error message for session model
            Session["Cart"] = model;

            //return View(model);
            return RedirectToAction("SponsorshipSummary", "Payment", new { returnUrl = returnUrl });
        }

        public ActionResult addSponsorship(int venue_id)
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var model = Session["SponsorshipCart"] as SponsorshipCartModel;
            model.CCPayment.transactionReference = Session["TransactionReference"] as String;

            #region Validate parameters
            if (model == null)
            {
                throw new Exception("An unknown error occured.");
            }

            if (Session["SponsorshipCart"] == null || contact == null)
            {
                return RedirectToAction("SponsorshipSummary", "Purchase", new { error = "Error retrieving cart. If this persists, please contact info@believeglobal.org." });
            }

            if (model.Event.id <= 0)
            {
                return RedirectToAction("SponsorshipSummary", "Purchase", new { error = "Event error. Please contact info@believeglobal.org for assistance." });
            }

            if (model.ContactId > 0)
            {
                model.ContactId = contact.Id;
            }
            else
            {
                return RedirectToAction("SponsorshipSummary", "Purchase", new { error = "Authentication error. Please contact info@believeglobal.org for assistance." });
            }
            #endregion

            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    #region Check whether or not this contact has an External and ExternalEvent object and create
                    var has_external = (from ce in context.Common_ContactExternal
                                        where ce.Contact_ID == contact.Id
                                        select ce).FirstOrDefault();

                    if (has_external == null)
                    {
                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.Contact_ID = model.ContactId;
                        new_external_contact.ChurchAddress1 = "";
                        new_external_contact.ChurchAddress2 = "";
                        new_external_contact.ChurchCountry_ID = null;
                        new_external_contact.ChurchEmail = "";
                        new_external_contact.ChurchName = "";
                        new_external_contact.ChurchNumberOfPeople = "";
                        new_external_contact.ChurchPhone = "";
                        new_external_contact.ChurchPostcode = "";
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = "";
                        new_external_contact.ChurchSuburb = "";
                        new_external_contact.ChurchWebsite = "";
                        new_external_contact.Denomination_ID = 49;
                        new_external_contact.SeniorPastorName = "";
                        new_external_contact.LeaderName = "";
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;

                        context.Common_ContactExternal.Add(new_external_contact);
                        context.SaveChanges();
                    }

                    var has_external_event = (from cee in context.Common_ContactExternalEvents
                                              where cee.Contact_ID == contact.Id
                                              select cee).ToList();

                    if (!has_external_event.Any())
                    {
                        // create new common_contactExternalEvents
                        //var new_contact_event = new Common_ContactExternalEvents();
                        //new_contact_event.Contact_ID = contact.Id;
                        //new_contact_event.EmergencyContactName = "";
                        //new_contact_event.EmergencyContactPhone = "";
                        //new_contact_event.EmergencyContactRelationship = "";
                        //new_contact_event.MedicalInformation = "";
                        //new_contact_event.MedicalAllergies = "";
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "";

                        //context.Common_ContactExternalEvents.Add(new_contact_event);
                        //context.SaveChanges();
                    }

                    #endregion

                    #region Retrieve Bank Account
                    /* Retrieve Bank Account information via Conference_ID to transfer funds into */
                    var bankAccount = (from ec in context.Events_Conference
                                       join ev in context.Events_Venue
                                          on ec.Conference_ID equals ev.Conference_ID
                                       join ba in context.Common_BankAccount
                                          on ec.BankAccount_ID equals ba.BankAccount_ID
                                       where ev.Venue_ID == venue_id
                                       select ba).FirstOrDefault();

                    model.CCPayment.BankAccount = bankAccount;
                    model.Event.currency = bankAccount.Currency;

                    if (bankAccount == null || String.IsNullOrEmpty(bankAccount.Currency))
                    {
                        ViewBag.Message = "Error trying to retrieve bank account.";
                        return View("Error");
                    }
                    #endregion

                    #region Post processing
                    // Get connection string from web.config
                    var con = ConfigurationManager.ConnectionStrings["PlanetshakersEntities"].ConnectionString;

                    // Setup variables to retrieve responses from the bank
                    var item = model.Cart;

                    pxlogger.Info("Payment Success");

                    string Category = "Stripe SponsorshipPayment";
                    string Criteria = "Venue ID : " + model.Cart.event_venue_id.ToString();
                    string ContactID = model.ContactId.ToString();
                    Business.MWebMain.LogAction(ref Category, ref Criteria, ContactID);

                    #region Insert Variable Payments into database
                    // Create new  group payment
                    var variablePayment = new Events_VariablePayment();
                    variablePayment.IncomingContact_ID = contact.Id;
                    variablePayment.Conference_ID = model.Event.id;
                    variablePayment.PaymentType_ID = (int)PaymentType.CreditCard; // Credit Card Payment
                    variablePayment.PaymentAmount = model.GrandTotal;
                    variablePayment.CCTransactionReference = model.CCPayment.transactionReference;
                    variablePayment.Refund = false;
                    variablePayment.OutgoingContact_ID = null;
                    variablePayment.Comment = String.Empty;
                    variablePayment.PaymentDate = DateTime.Now;
                    variablePayment.PaymentBy_ID = contact.Id;
                    variablePayment.BankAccount_ID = bankAccount.BankAccount_ID; // Stripe Church for PlanetBusiness otherwise Stripe Ministries
                    context.Events_VariablePayment.Add(variablePayment);

                    try
                    {
                        // Save changes                                                               
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        // Notify developer of error
                        var dev_email = new DevErrorNotificationEmailModel();
                        dev_email.ex = ex;
                        dev_email.extraNotes = "transactionRef :: " + model.CCPayment.transactionReference + " | Name :: " + contact.FirstName;
                        // new MailController().DevErrorNotification(dev_email).Deliver();

                        throw new Exception(ex.Message);
                    }
                    #endregion

                    #region Send Sponsorship Email Confirmation
                    // Prepare the confirmation email model
                    pxlogger.Info("Preparing confirmation email model.");
                    var email_model = new SponsorshipConfirmationEmailModel()
                    {
                        Email = contact.Email,
                        ContactId = contact.Id,
                        Event = model.Event,
                        GrandTotal = model.GrandTotal
                    };

                    // Prepare to send email
                    pxlogger.Info("Sending Tax Invoice...");

                    new MailController().SponsorshipConfirmation(email_model);

                    #endregion

                    #endregion

                    // Clear the Cart 
                    TempData["recently_registered"] = true;
                    Session["SponsorshipCart"] = model; 

                    return RedirectToAction("SponsorshipConfirmation", "Purchase", new { venue_ID = model.Event.venueID });
                }
            }
            catch (Exception ex)
            {
                return View("Error");
                throw ex;
            }

        }

        [HttpPost]
        [PSAuthorize]
        public ActionResult SponsorshipPayment(CCPaymentModel model, string stripeEmail, string stripeToken)
        {
            try
            {
                #region Validate parameters
                if (model == null)
                {
                    throw new Exception("An unknown error occured.");
                }

                if (stripeToken == null)
                {
                    throw new Exception("An unknown error occured.");
                }

                if (stripeEmail == null)
                {
                    throw new Exception("An unknown error occured.");
                }
                #endregion

                // Retrieve logged in contact details
                var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                if (Session["SponsorshipCart"] == null || contact == null)
                {
                    return RedirectToAction("SponsorshipSummary", "Purchase", new { error = "Error retrieving cart. If this persists, please contact info@believeglobal.org." });
                }

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Retrieve some cart data from session

                    var cartModel = Session["SponsorshipCart"] as SponsorshipCartModel;

                    if (cartModel.Event.id <= 0)
                    {
                        return RedirectToAction("SponsorshipSummary", "Purchase", new { error = "Event error. Please contact info@believeglobal.org for assistance." });
                    }

                    if (cartModel.ContactId <= 0)
                    {
                        cartModel.ContactId = contact.Id;
                    }

                    #region Check whether or not this contact has an External and ExternalEvent object and create
                    int cID = cartModel.ContactId;

                    if (cID <= 0)
                    {
                        return RedirectToAction("SponsorshipSummary", "Purchase", new { error = "Authentication error. Please contact info@believeglobal.org for assistance." });
                    }

                    var has_external = (from ce in context.Common_ContactExternal
                                        where ce.Contact_ID == cID
                                        select ce).FirstOrDefault();

                    if (has_external == null)
                    {
                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.Contact_ID = cartModel.ContactId;
                        new_external_contact.ChurchAddress1 = "";
                        new_external_contact.ChurchAddress2 = "";
                        new_external_contact.ChurchCountry_ID = null;
                        new_external_contact.ChurchEmail = "";
                        new_external_contact.ChurchName = "";
                        new_external_contact.ChurchNumberOfPeople = "";
                        new_external_contact.ChurchPhone = "";
                        new_external_contact.ChurchPostcode = "";
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = "";
                        new_external_contact.ChurchSuburb = "";
                        new_external_contact.ChurchWebsite = "";
                        new_external_contact.Denomination_ID = 49;
                        new_external_contact.SeniorPastorName = "";
                        new_external_contact.LeaderName = "";
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;

                        context.Common_ContactExternal.Add(new_external_contact);
                        context.SaveChanges();
                    }

                    var has_external_event = (from cee in context.Common_ContactExternalEvents
                                              where cee.Contact_ID == cID
                                              select cee).ToList();

                    if (!has_external_event.Any())
                    {
                        // create new common_contactExternalEvents
                        //var new_contact_event = new Common_ContactExternalEvents();
                        //new_contact_event.Contact_ID = cartModel.ContactId;
                        //new_contact_event.EmergencyContactName = "";
                        //new_contact_event.EmergencyContactPhone = "";
                        //new_contact_event.EmergencyContactRelationship = "";
                        //new_contact_event.MedicalInformation = "";
                        //new_contact_event.MedicalAllergies = "";
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "";

                        //context.Common_ContactExternalEvents.Add(new_contact_event);
                        //context.SaveChanges();
                    }

                    #endregion

                    /* Retrieve Bank Account information via Conference_ID to transfer funds into */
                    var bankAccount = (from ec in context.Events_Conference
                                       join ba in context.Common_BankAccount
                                          on ec.BankAccount_ID equals ba.BankAccount_ID
                                       where ec.Conference_ID == cartModel.Event.id
                                       select ba).FirstOrDefault();

                    model.BankAccount = bankAccount;
                    cartModel.Event.currency = bankAccount.Currency;

                    if (bankAccount == null || String.IsNullOrEmpty(bankAccount.Currency))
                    {
                        ViewBag.Message = "Something went wrong.";
                        return View("Error");
                    }

                    var venue_id = cartModel.Cart.event_venue_id;

                    // Get connection string from web.config
                    var con = ConfigurationManager.ConnectionStrings["PlanetshakersEntities"].ConnectionString;
 
                    /* ================= Sponsorship Payment Process ================= */

                    #region Process Sponsorship Payment
                    // Setup variables to retrieve responses from the bank
                    string eventName = "";
                    var item = cartModel.Cart;
                    eventName = Convert.ToString(item.event_name);
                    string transactionResponse = String.Empty;
                    string transactionReference = String.Empty;

                    bool paymentSuccess = false;

                    // Instantiate stripe service models
                    var customers = new CustomerService();
                    var charges = new ChargeService();

                    /*StripeConfiguration.SetApiKey(bankAccount.SecretKey);

                    // Create stripe token
                    var customer = customers.Create(new CustomerCreateOptions
                    {
                        Email = stripeEmail,
                        SourceToken = stripeToken
                    });

                    // Create stripe charge
                    var amount = Convert.ToInt32(cartModel.GrandTotal) * 100;
                    var charge = charges.Create(new ChargeCreateOptions
                    {
                        Amount = amount,       // amount must be at least $0.50
                        Description = "Sponsorship for " + cartModel.Event.name.ToString(),
                        Currency = bankAccount.Currency,
                        CustomerId = customer.Id
                    });

                    if (charge.Status == "succeeded")
                    {
                        paymentSuccess = true;
                    }
                    else
                    {
                        throw new StripeException();
                    }

                    transactionReference = charge.Id.ToString();*/
                    #endregion

                    #region Post processing
                    if (paymentSuccess)
                    {
                        pxlogger.Info("Payment Success");
                        /* At this point, transaction is successful.
                            * Create all the required rows for registrations and payments
                            */

                        string Category = "Stripe SponsorshipPayment";
                        string Criteria = "Venue ID : " + cartModel.Cart.event_venue_id.ToString();
                        string ContactID = cartModel.ContactId.ToString();
                        Business.MWebMain.LogAction(ref Category, ref Criteria, ContactID);

                        #region Insert Variable Payments into database

                        // Create new  group payment
                        var variablePayment = new Events_VariablePayment();
                        variablePayment.IncomingContact_ID = cartModel.ContactId;
                        variablePayment.Conference_ID = cartModel.Event.id;
                        variablePayment.PaymentType_ID = (int)PaymentType.CreditCard; // Credit Card Payment
                        variablePayment.PaymentAmount = cartModel.GrandTotal;
                        variablePayment.CCTransactionReference = transactionReference;
                        variablePayment.Refund = false;
                        variablePayment.OutgoingContact_ID = null;
                        variablePayment.Comment = String.Empty;
                        variablePayment.PaymentDate = DateTime.Now;
                        variablePayment.PaymentBy_ID = cartModel.ContactId;
                        variablePayment.BankAccount_ID = bankAccount.BankAccount_ID; // Stripe Church for PlanetBusiness otherwise Stripe Ministries
                        context.Events_VariablePayment.Add(variablePayment);

                        try
                        {
                            // Save changes                                                               
                            context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            // Notify developer of error
                            var dev_email = new DevErrorNotificationEmailModel();
                            dev_email.ex = ex;
                            dev_email.extraNotes = "transactionRef :: " + transactionReference + " | Name :: " + model.CCName + " | Phone :: " + model.CCPhone;
                            // new MailController().DevErrorNotification(dev_email).Deliver();

                            throw new Exception(ex.Message);
                        }
                        #endregion

                        #region Sponsorship Email Confirmation
                        // Prepare the confirmation email model
                        pxlogger.Info("Preparing confirmation email model.");
                        var email_model = new SponsorshipConfirmationEmailModel()
                        {
                            Email = contact.Email,
                            ContactId = cartModel.ContactId,
                            Event = cartModel.Event,
                            GrandTotal = cartModel.GrandTotal
                        };

                        // Prepare to send email
                        pxlogger.Info("Sending Tax Invoice...");

                        new MailController().SponsorshipConfirmation(email_model);
                        #endregion
                    }
                    else
                    {
                        // Remove registrations that have been added
                        throw new Exception("An error occured while processing the payment. "
                                            + transactionResponse);
                    }
                    #endregion 

                    // Clear the Cart 
                    pxlogger.Info("Emptying Cart ...");
                    TempData["recently_registered"] = true;
                    pxlogger.Info("reloading page ...");
                    pxlogger.Info("------------------------------------------------------------------------------------------------------");

                    // Update session.
                    Session["SponsorshipCart"] = cartModel; // if this is going to be here?

                    return RedirectToAction("SponsorshipConfirmation", "Purchase", new { venue_ID = cartModel.Event.venueID });
                }
            }
            catch (Exception ex)
            {
                var cartModel = Session["SponsorshipCart"] as SponsorshipCartModel;
                cartModel.CCPayment = model;
                Session["SponsorshipCart"] = cartModel;
                pxlogger.Info(ex.Message);
                return RedirectToAction("SponsorshipSummary", "Purchase", new { error = "Sponsorship amount must be greater than $1.00" });
            }
        }

        public ActionResult SponsorshipConfirmation(int venue_ID, int Contact_ID)
        {
            //var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var cart = Session["SponsorshipCart"] as SponsorshipCartModel;

            ViewBag.venueID = venue_ID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                ViewBag.EventName = (from ec in context.Events_Conference
                                     join ev in context.Events_Venue
                                        on ec.Conference_ID equals ev.Conference_ID
                                     where ev.Venue_ID == venue_ID
                                     select ec.ConferenceName).FirstOrDefault();
                SponsorshipConfirmModel model = (from ec in context.Events_Conference
                                                 join ev in context.Events_Venue
                                                    on ec.Conference_ID equals ev.Conference_ID
                                                 where ev.Venue_ID == venue_ID
                                                 select new SponsorshipConfirmModel
                                                 {
                                                     EventName = ec.ConferenceName,
                                                     Event_ID = ec.Conference_ID
                                                 }).FirstOrDefault();
                model.GUID = (from c in context.Common_Contact where c.Contact_ID == Contact_ID select c.GUID).FirstOrDefault();
                return View(model);
            }
        }

        public ActionResult SponsorshipTermsAndConditions(int Conference_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                /* Retrieve Bank Account information via Conference_ID to transfer funds into */
                var bankAccount = (from ec in context.Events_Conference
                                   join ba in context.Common_BankAccount
                                   on ec.BankAccount_ID equals ba.BankAccount_ID
                                   where ec.Conference_ID == Conference_ID
                                   select ba).FirstOrDefault();

                if (bankAccount != null)
                {
                    ViewBag.currency = bankAccount.Currency;
                }
                else
                {
                    ViewBag.currency = "AUD";
                }
            }
            return View();
        }

        #region Select Lists

        private List<SelectListItem> getCCExpiryMonths()
        {
            // only continue if months have not been previously retrieved
            if (Session != null && Session["exp_months"] != null) return Session["exp_months"] as List<SelectListItem>;

            var monthsList = new List<SelectListItem>();

            // A placeholder
            monthsList.Add(new SelectListItem()
            {
                Text = "Month",
                Value = "0"
            });

            for (int i = 1; i <= 12; i++)
            {
                monthsList.Add(new SelectListItem()
                {
                    Text = i.ToString("00"),
                    Value = i.ToString()
                });
            }

            Session["exp_months"] = monthsList;
            return monthsList;
        }

        private List<SelectListItem> getCCExpiryYears()
        {
            // only continue if years have not been previously retrieved
            if (Session != null && Session["exp_years"] != null) return Session["exp_years"] as List<SelectListItem>;

            var yearsList = new List<SelectListItem>();

            // A placeholder
            yearsList.Add(new SelectListItem()
            {
                Text = "Year",
                Value = "0"
            });

            for (int i = DateTime.Today.Year; i <= DateTime.Today.Year + 20; i++)
            {
                yearsList.Add(new SelectListItem()
                {
                    Text = i.ToString().Substring(2, 2),
                    Value = i.ToString().Substring(2, 2)
                });
            }

            Session["exp_years"] = yearsList;
            return yearsList;
        }

        #endregion

        #region AJAX Calls

        public bool validateContact()
        {
            /*var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

            if (contact == null)
            {
                return false;
            } else
            {
                return true;
            }*/

            return false;
        }

        [HttpPost]
        public JsonResult selfAllocate()
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var cart = Session["Cart"] as CartModel;

            var ID = contact.Id;
            var venue_ID = cart.Event.venueID;
            var regType_ID = cart.Cart.First().reg_id;

            switch (regType_ID)
            {
                // planetUNI deposit
                case 3324:
                    regType_ID = 3325; // full rego w deposit
                    break;
                default:
                    break;
            }

            EventSummaryModel returnModel = new EventSummaryModel();
            returnModel.Group = new EventGroup(ID);
            returnModel.SelectedEvent = new Event(venue_ID);

            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var check = (from r in context.Events_Registration
                                 where r.Contact_ID == ID && r.Venue_ID == venue_ID
                                 select r.Registration_ID).Any();

                    if (check)
                    {
                        return Json(new
                                {
                                    isSuccess = true,
                                    message = "You have an existing allocation.",
                                    curRegos = returnModel.CurrentSingleRegistrations,
                                    allocated = returnModel.AllocatedRegistrations,
                                    allocatedQty = returnModel.GroupAllocatedCount,
                                    unallocated = returnModel.UnallocatedRegistrations,
                                    unallocatedQty = returnModel.GroupUnallocatedCount,
                                    invites = returnModel.PendingInvitations
                                }, JsonRequestBehavior.AllowGet);
                    }

                    var selected_event = new Event(venue_ID);

                    // Allocate registration
                    var new_registration = returnModel.Group.AllocateRegistration(ID,
                                                           venue_ID,
                                                           regType_ID);
                    returnModel.Group.Save(ID);
                    returnModel.SelectedEvent = selected_event;

                    var conference = (from ev in context.Events_Venue
                                      join ec in context.Events_Conference
                                      on ev.Conference_ID equals ec.Conference_ID
                                      where ev.Venue_ID == venue_ID
                                      select new Event
                                      {
                                          id = ec.Conference_ID,
                                          venueID = ev.Venue_ID,
                                          name = ec.ConferenceName,
                                          date = ec.ConferenceDate,
                                          location = ec.ConferenceLocation,
                                          venueLocation = ev.VenueName,
                                          shortname = ec.ConferenceNameShort
                                      }).FirstOrDefault();

                    var confirmationBarcode = new ConfirmationBarcode()
                    {
                        Subject = "Quick Check In - " + conference.name,
                        EventName = conference.name,
                        EventDate = conference.date.ToString(),
                        EventVenue = conference.location,
                        VenueLocation = conference.venueLocation,
                        Email = contact.Email,
                        RegistrationNumber = new_registration.registrationID.ToString(),
                        RegistrationTypeID = (int)new_registration.registrationTypeID
                    };

                    if (!String.IsNullOrEmpty(confirmationBarcode.Email))
                    {
                        new MailController().ConfirmationBarcodeEmail(confirmationBarcode);
                    }

                    // Return true
                    return Json(new
                    {
                        isSuccess = true,
                        message = "You have allocated a registration to yourself!",
                        curRegos = returnModel.CurrentSingleRegistrations,
                        allocated = returnModel.AllocatedRegistrations,
                        allocatedQty = returnModel.GroupAllocatedCount,
                        unallocated = returnModel.UnallocatedRegistrations,
                        unallocatedQty = returnModel.GroupUnallocatedCount,
                        invites = returnModel.PendingInvitations
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = "Cannot allocate registration. Please try again via 'View Registration'."
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult allocateSon(string firstname, string email, string lastname)
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var cart = Session["Cart"] as CartModel;

            var ID = contact.Id;
            var venue_ID = cart.Event.venueID;
            var regType_ID = cart.Cart.First().reg_id;

            EventSummaryModel returnModel = new EventSummaryModel();
            returnModel.Group = new EventGroup(ID);
            returnModel.SelectedEvent = new Event(venue_ID);

            var son_regotypes = new List<int>() { 2925, 2927, 2929, 2930, 2931, 2932 };

            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var get_son_regotypes = (from egr in context.vw_Events_GroupRegistrationView
                                                where egr.Venue_ID == venue_ID 
                                                    && egr.GroupLeader_ID == ID
                                                    && son_regotypes.Contains(egr.RegistrationType_ID)
                                                    && egr.UnAllocated >= 1
                                                select egr).ToList();

                    var son_regtype_id = get_son_regotypes.First().RegistrationType_ID;

                    var selected_event = new Event(venue_ID);

                    #region Create new External Contact
                    // create new common_contactExternalEvents
                    //var new_contact_event = new Common_ContactExternalEvents();
                    //new_contact_event.EmergencyContactName = "";
                    //new_contact_event.EmergencyContactPhone = "";
                    //new_contact_event.EmergencyContactRelationship = "";
                    //new_contact_event.MedicalInformation = "";
                    //new_contact_event.MedicalAllergies = "";
                    //new_contact_event.AccessibilityInformation = "";
                    //new_contact_event.EventComments = "";

                    // create new external contact
                    var new_external_contact = new Common_ContactExternal();
                    new_external_contact.ChurchAddress1 = "";
                    new_external_contact.ChurchAddress2 = "";
                    new_external_contact.ChurchCountry_ID = null;
                    new_external_contact.ChurchEmail = "";
                    new_external_contact.ChurchName = "";
                    new_external_contact.ChurchNumberOfPeople = "";
                    new_external_contact.ChurchPhone = "";
                    new_external_contact.ChurchPostcode = "";
                    new_external_contact.ChurchState_ID = null;
                    new_external_contact.ChurchStateOther = "";
                    new_external_contact.ChurchSuburb = "";
                    new_external_contact.ChurchWebsite = "";
                    new_external_contact.Denomination_ID = 49;
                    new_external_contact.SeniorPastorName = "";
                    new_external_contact.LeaderName = "";
                    new_external_contact.Deleted = false;
                    new_external_contact.Inactive = false;
                    //new_external_contact.Common_ContactExternalEvents = new_contact_event;

                    // create new contact object
                    var new_contact = new Common_Contact();
                    new_contact.Salutation_ID = 1;
                    new_contact.FirstName = firstname;
                    new_contact.LastName = lastname;
                    new_contact.Gender = "";
                    new_contact.DateOfBirth = null;
                    new_contact.Address1 = "";
                    new_contact.Address2 = "";
                    new_contact.Suburb = "";
                    new_contact.Postcode = "";
                    new_contact.State_ID = null;
                    new_contact.StateOther = "";
                    new_contact.Country_ID = null;
                    new_contact.Phone = "";
                    new_contact.PhoneWork = "";
                    new_contact.Fax = "";
                    new_contact.Mobile = "";
                    new_contact.Email = "";
                    new_contact.Email2 = email;
                    new_contact.DoNotIncludeEmail1InMailingList = false;
                    new_contact.DoNotIncludeEmail2InMailingList = false;
                    new_contact.Family_ID = null;
                    new_contact.FamilyMemberType_ID = null;
                    new_contact.Password = "";
                    new_contact.VolunteerPoliceCheck = "";
                    new_contact.VolunteerPoliceCheckDate = null;
                    new_contact.VolunteerWWCC = "";
                    new_contact.VolunteerWWCCDate = null;
                    new_contact.VolunteerWWCCType_ID = null;
                    new_contact.CreationDate = DateTime.Now;
                    new_contact.CreatedBy_ID = 101;
                    new_contact.ModificationDate = DateTime.Now;
                    new_contact.ModifiedBy_ID = null;
                    new_contact.GUID = Guid.NewGuid();
                    new_contact.AnotherLanguage_ID = null;
                    new_contact.Nationality_ID = null;
                    new_contact.Common_ContactExternal = new_external_contact;

                    context.Common_Contact.Add(new_contact);
                    context.SaveChanges();
                    #endregion

                    // Allocate registration
                    returnModel.Group.AllocateRegistration(new_contact.Contact_ID,
                                                           venue_ID,
                                                           son_regtype_id);
                    returnModel.Group.Save(ID);
                    returnModel.SelectedEvent = selected_event;

                    return Json(new {
                        isSuccess = true,
                        message = "You have allocated a registration!",
                        curRegos = returnModel.CurrentSingleRegistrations,
                        allocated = returnModel.AllocatedRegistrations,
                        allocatedQty = returnModel.GroupAllocatedCount,
                        unallocated = returnModel.UnallocatedRegistrations,
                        unallocatedQty = returnModel.GroupUnallocatedCount,
                        invites = returnModel.PendingInvitations
                    }, JsonRequestBehavior.AllowGet);
                }                
            }
            catch  (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult updateAddress(string street, string unit, string suburb, string postcode, int state_id)
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            bool updated = false;

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                try 
                {
                    var person = (from cc in entity.Common_Contact
                                  where cc.Contact_ID == contact.Id
                                  select cc).FirstOrDefault();

                    person.Address1 = street;
                    person.Address2 = unit;
                    person.Suburb = suburb;
                    person.Postcode = postcode;
                    person.State_ID = state_id;

                    entity.SaveChanges();
                    updated = true;

                } catch (Exception ex)
                {
                    throw ex;
                }
            }

            return Json( new {updated}, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult updateMedical(string name, string mobile, string rel, string info, string allergies)
        //{
        //    var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
        //    bool updated = false;

        //    using (PlanetshakersEntities entity = new PlanetshakersEntities())
        //    {
        //        try
        //        {
        //            var person = (from cc in entity.Common_ContactExternalEvents
        //                          where cc.Contact_ID == contact.Id
        //                          select cc).FirstOrDefault();

        //            person.EmergencyContactName = name;
        //            person.EmergencyContactPhone = mobile;
        //            person.EmergencyContactRelationship = rel;
        //            person.MedicalInformation = info;
        //            person.MedicalAllergies = allergies;
        //            person.LastModifiedDate = DateTime.Now;

        //            entity.SaveChanges();
        //            updated = true;

        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }

        //    return Json(new { updated }, JsonRequestBehavior.AllowGet);
        //}

        public bool checkAllocation()
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var cart = Session["Cart"] as CartModel;

            var ID = contact.Id;
            var venue_ID = cart.Event.venueID;

            var hasRego = false;

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                hasRego = (from er in entity.Events_Registration
                           where er.Contact_ID == ID && er.Venue_ID == venue_ID
                           select er.Contact_ID).Any();
            }

            return hasRego;
        }

        public bool hasAvailableRego()
        {
            var hasAvailable = false;

            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            var cart = Session["Cart"] as CartModel;

            var ID = contact.Id;
            var venue_ID = cart.Event.venueID;

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {

            }

            return hasAvailable;
        }


        #endregion
    }
}
