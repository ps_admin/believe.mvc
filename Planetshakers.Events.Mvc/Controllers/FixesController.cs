﻿using Planetshakers.Events.Mvc.Filters;
using Planetshakers.Events.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Planetshakers.Events.Mvc.Helpers.VoucherExtensions;


namespace Planetshakers.Events.Mvc.Controllers
{
    public class FixesController : Controller
    {
        [HttpPost]
        [PSAuthorize]
        public ActionResult CorrectCredit(int VenueID, int ConfID)
        {

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                decimal totalCreditAmount = 0;
                DateTime today = DateTime.Now;
                DateTime year20 = DateTime.Now.AddYears(20);
                string voucherCode = "";

                int[] creditRegoTypes = (from rt in entity.Events_RegistrationType
                                             where rt.RegistrationType.Contains("CREDITED")
                                             && rt.Conference_ID == ConfID
                                             select rt.RegistrationType_ID).ToArray();

                //For single rego use

                var SingleRegoCIDs = entity.Events_Registration.Where(x => x.Venue_ID == VenueID)
                                                               .Where(y => creditRegoTypes.Contains(y.RegistrationType_ID))
                                                               .Where(z => z.GroupLeader_ID == null).ToList();

                foreach (var rego in SingleRegoCIDs)
                {
                    totalCreditAmount = entity.Events_RegistrationType.Where(x => x.RegistrationType_ID == rego.RegistrationType_ID).Sum(x => x.RegistrationCost);

                    voucherCode = GetUniqueKey(9);

                    // Safety check for voucher code duplication. Will regenerate if a duplication is detected.
                    while (VoucherDuplicated(voucherCode))
                    {
                        voucherCode = GetUniqueKey(9);
                    }

                    Events_Voucher events_Voucher = new Events_Voucher
                    {
                        VoucherType_ID = 1,
                        VoucherCode = voucherCode,
                        VoucherValue = totalCreditAmount,
                        ValueIsPercentage = false,
                        Contact_ID = null,
                        CurrentUses = 0,
                        ValidStartDT = today,
                        ExpiryDT = year20,
                        VoucherIssuedDT = today,
                        ModifiedBy = rego.Contact_ID,
                        ModifiedDT = today,
                        Inactive = false,
                        Deleted = false
                    };

                    entity.Events_Voucher.Add(events_Voucher);
                    entity.SaveChanges();
                }

                //For group rego use

                var GroupRegoCIDs = entity.Events_GroupBulkRegistration.Where(x => x.Venue_ID == VenueID)
                                                                       .Where(y => creditRegoTypes.Contains(y.RegistrationType_ID))
                                                                       .Select(z => z.GroupLeader_ID).Distinct().ToList();

                foreach (var CID in GroupRegoCIDs)
                {
                    totalCreditAmount = 0;
                    var existingGroupRego = (from gr in entity.Events_GroupBulkRegistration
                                             where gr.GroupLeader_ID == CID && gr.Venue_ID == VenueID
                                             select gr).ToList();

                    foreach (var groupRego in existingGroupRego)
                    {
                        totalCreditAmount += entity.Events_RegistrationType.Where(x => x.RegistrationType_ID == groupRego.RegistrationType_ID)
                                                                           .Sum(x => x.RegistrationCost * groupRego.Quantity);
                    }

                    voucherCode = GetUniqueKey(9);

                    // Safety check for voucher code duplication. Will regenerate if a duplication is detected.
                    while (VoucherDuplicated(voucherCode))
                    {
                        voucherCode = GetUniqueKey(9);
                    }

                    Events_Voucher events_Voucher = new Events_Voucher
                    {
                        VoucherType_ID = 1,
                        VoucherCode = voucherCode,
                        VoucherValue = totalCreditAmount,
                        ValueIsPercentage = false,
                        Contact_ID = null,
                        CurrentUses = 0,
                        ValidStartDT = today,
                        ExpiryDT = year20,
                        VoucherIssuedDT = today,
                        ModifiedBy = CID,
                        ModifiedDT = today,
                        Inactive = false,
                        Deleted = false
                    };

                    entity.Events_Voucher.Add(events_Voucher);
                    entity.SaveChanges();
                }

            }

            return Content("OK");
        }
    }
}