using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using Planetshakers.Events.Mvc.Models;
using Planetshakers.Events.Mvc.Filters;
using System.Configuration;
using Newtonsoft.Json;
using Planetshakers.Events.Mvc.Helpers;

namespace Planetshakers.Events.Mvc.Controllers
{
    public class AccountController : Controller
    {
        static int iTmp = 0;
        static string gP = "";
        static string sUsr = "";
        static string sCode = "";
        private static readonly string _recaptchaSecretKey = AzureServices.KeyVault_RetreiveSecret("believe-apps", "reCaptchaSecretKey-missions");

        [AllowAnonymous]
        public ActionResult SendConfirmationEmail()
        {
            //new MailController().RegistrationConfirmation().DeliverAsync();
            return View();
        }

        public const string MSG_USR_NOT_AUTHENTICATED = "There was an error while authenticating your e-mail / password. Please try again.";

        #region Account Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            LoginModel model = new LoginModel();

            string PSCookies = Planetshakers.Business.MWebMain.PSCookieAliveValue(Request);

            if (PSCookies != string.Empty)
            {
                Planetshakers.Business.MWebMain.LogMessage("Planetshakers cookie has found in register logon.");
                using (PlanetshakersEntities entity = new PlanetshakersEntities())
                {
                    int contact_id = Convert.ToInt32(PSCookies);
                    Common_Contact user = (from cc in entity.Common_Contact where cc.Contact_ID == contact_id select cc).FirstOrDefault();

                    string email = user.Email;

                    String ModuleName = "";
                    ModuleName = HttpContext.Request.Url.AbsoluteUri.ToString();
                    //if ((ModuleName.Contains("localhost") == true) ||  (ModuleName.Contains("registerdev") == true))
                    //{
                    //    email = email.Remove(email.Length - 1, 1);
                    //}
                    model.Email = email;
                    model.Password = user.Password;
                    return performLogin(model, returnUrl);
                }
            }

            ViewBag.ReturnUrl = returnUrl;

            if (!String.IsNullOrEmpty((String)TempData["successResetPass"]))
            {
                ViewBag.SuccessfulReset = (String)TempData["successResetPass"];
                TempData["successResetPass"] = null;
            }

            if (iTmp == 1)
            {
                iTmp = 0;
                gP = "";
                return RedirectToAction("Index", "Events");
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model, string returnUrl, string socialLogin)
        {
            var captcha_required = false;
            var verified = false;
            if (captcha_required)
            {
                var recaptcha = Request.Form["g-recaptcha-response"];
                verified = !String.IsNullOrEmpty(recaptcha);
            }
            else
            {
                verified = true;
            }
            ViewBag.ReturnUrl = returnUrl;
            
            if (verified)
            {
                if (!String.IsNullOrEmpty((String)TempData["successResetPass"]))
                {
                    ViewBag.SuccessfulReset = (String)TempData["successResetPass"];
                    TempData["successResetPass"] = null;
                }

                return performLogin(model, returnUrl);
            }
            else
            {
                ViewBag.Error = "Please verify that you are not a robot";
                ModelState.AddModelError("", "Please verify that you are not a robot");
                return View();
            }
        }

        private ActionResult performLogin(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                bool succeed = false;
                int attempts = 0;

                // Due to times where connection may fail, login attempts are done 10 times
                while (!succeed && attempts < 10)
                {
                    try
                    {
                        int id = 0;
                        String ModuleName = "";

                        ModuleName = HttpContext.Request.Url.AbsoluteUri.ToString();


                        // Try logging in via basic authentication
                        if (id == 0)
                        {
                            using (PlanetshakersEntities context = new PlanetshakersEntities())
                            {
                                Common_Contact contact = (from cc in context.Common_Contact where cc.Email.ToLower() == model.Email.ToLower() select cc).FirstOrDefault();

                                try
                                {
                                    if (contact != null && contact.Password == model.Password)
                                    {
                                        Common_ContactExternal ce = (from cce in context.Common_ContactExternal
                                                                     where cce.Contact_ID == contact.Contact_ID
                                                                     select cce).FirstOrDefault();

                                        if (ce == null)
                                        {
                                            // create new common_contactExternalEvents
                                            //var new_contact_event = new Common_ContactExternalEvents();
                                            //new_contact_event.EmergencyContactName = "";
                                            //new_contact_event.EmergencyContactPhone = "";
                                            //new_contact_event.EmergencyContactRelationship = "";
                                            //new_contact_event.MedicalInformation = "";
                                            //new_contact_event.MedicalAllergies = "";
                                            //new_contact_event.AccessibilityInformation = "";
                                            //new_contact_event.EventComments = "";

                                            // create new external contact
                                            var new_external_contact = new Common_ContactExternal();
                                            new_external_contact.ChurchAddress1 = "";
                                            new_external_contact.ChurchAddress2 = "";
                                            new_external_contact.ChurchCountry_ID = null;
                                            new_external_contact.ChurchEmail = "";
                                            new_external_contact.ChurchName = "";
                                            new_external_contact.ChurchNumberOfPeople = "";
                                            new_external_contact.ChurchPhone = "";
                                            new_external_contact.ChurchPostcode = "";
                                            new_external_contact.ChurchState_ID = null;
                                            new_external_contact.ChurchStateOther = "";
                                            new_external_contact.ChurchSuburb = "";
                                            new_external_contact.ChurchWebsite = "";
                                            new_external_contact.Denomination_ID = 49;
                                            new_external_contact.SeniorPastorName = "";
                                            new_external_contact.LeaderName = "";
                                            new_external_contact.Deleted = false;
                                            new_external_contact.Inactive = false;

                                            context.Common_ContactExternal.Add(new_external_contact);

                                            contact.Common_ContactExternal = new_external_contact;

                                            context.SaveChanges();
                                        }

                                        Authenticator.Contact c = new Authenticator.Contact();
                                        c.Id = contact.Contact_ID;
                                        c.FirstName = contact.FirstName;
                                        c.LastName = contact.LastName;
                                        c.Email = contact.Email;

                                        id = c.Id;

                                        // Store contact information on session
                                        Session["Contact"] = c;
                                    }
                                    else
                                    {
                                        ViewBag.Error = MSG_USR_NOT_AUTHENTICATED;
                                    }

                                }
                                catch (Exception ex)
                                {
                                    ViewBag.Error = MSG_USR_NOT_AUTHENTICATED;
                                    throw ex;
                                }
                            }
                        }

                        // If authentication is successfull
                        if (id > 0)
                        {
                            string Category = "Login Click";
                            string criteria = "None";
                            String ContactID = id.ToString();
                            Business.MWebMain.LogAction(ref Category, ref criteria, ContactID);

                            succeed = true;

                            //Log user in
                            var user = FindUserDetails(id);
                            FormsAuthentication.SetAuthCookie(Helpers.JsonConverter.Serialize(user), false);
                            //FormsAuthentication.SetAuthCookie(model.Email, false);

                            // If invitation exists, allocate
                            var invitation = Session["Invitation"] as EventGroupInvitation;

                            if (invitation != null)
                            {
                                try
                                {
                                    var group = new EventGroup((int)invitation.groupLeaderID);
                                    group.AcceptInvitation(id, (Guid)invitation.identifier);
                                    TempData["invitation_success"] = "Invitation Accepted! Please remember to grab your event passes from your group leader!";
                                }
                                catch (Exception ex)
                                {
                                    TempData["invitation_error"] = "Failed to accept invitation - " + ex.Message;
                                }

                                using (PlanetshakersEntities context = new PlanetshakersEntities())
                                {
                                    // set the return url
                                    returnUrl = string.Format("{0}://{1}{2}",
                                        Url.RequestContext.HttpContext.Request.Url.Scheme,
                                        Url.RequestContext.HttpContext.Request.Url.Authority,
                                        Url.Action("Registrations", "Events", new { id = invitation.venueID }));
                                }
                                // Clear the session
                                Session["Invitation"] = null;
                            }

                            //Write cookie for SSO
                            Business.MWebMain.WriteCookie("Missions-Believe", Business.MWebMain.Encrypt(id.ToString()), Response);

                            var cart = Session["Cart"] as CartModel;
                            if (cart != null)
                            {
                                // check if existing voucher has been applied
                                // if exist but not applied, prompt to apply voucher code & direct to register page

                                if (cart.Voucher == null)
                                {
                                    var voucher_code = new EventsController().getPromoCode(id);

                                    if (!String.IsNullOrEmpty(voucher_code))
                                    {
                                        return RedirectToAction("Register", "Events", new { id = cart.Event.venueID, returnUrl = returnUrl });
                                    }
                                }

                                return RedirectToAction("Summary", "Payment", new { returnUrl = returnUrl });
                            }
                            if (Session["SponsorshipCart"] != null)
                            {
                                return RedirectToAction("SponsorshipSummary", "Payment", new { returnUrl = returnUrl });
                            }

                            if (!String.IsNullOrWhiteSpace(returnUrl))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Events");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = MSG_USR_NOT_AUTHENTICATED;
                        throw ex;
                    }
                    attempts++;
                }
            }

            // At this point, authentication failed. Redisplay form
            return View();
        }

        //Same to sp mobile pcr account log in
        public User FindUserDetails(int Contact_ID)
        {
            var user = new User();
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                user = (from cc in context.Common_Contact
                        join cci in context.Common_ContactInternal
                        on cc.Contact_ID equals cci.Contact_ID
                        join cud in context.Common_UserDetail
                        on cc.Contact_ID equals cud.Contact_ID
                        join pr in context.Church_RegionalPastor
                        on cc.Contact_ID equals pr.Contact_ID into temp
                        where cc.Contact_ID == Contact_ID
                        from subtemp in temp
                        select new User
                        {
                            ContactID = Contact_ID,
                            Barcode = "https://barcode.tec-it.com/barcode.ashx?data=" + cci.Barcode + "&code=Code128&dpi=128",
                            Email = cc.Email,
                            FirstName = cc.FirstName,
                            PastorOfRegionId = subtemp.Region_ID != null ? (int)subtemp.Region_ID : 0
                        }).FirstOrDefault();
            }

            return user;
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Planetshakers.Business.MWebMain.DeleteAllCookies(Request, Response);

            //Clear out all cookies, else social login was loggin with old login always.
            string[] myCookies = Request.Cookies.AllKeys;
            if (myCookies != null)
            {

                foreach (string cookie in myCookies)
                {
                    if (cookie != "__RequestVerificationToken")
                    {
                        Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                    }
                }
            }

            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult ForgetPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgetPassword(String Email)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Common_Contact c = null;
                try
                {
                    c = (from cc in context.Common_Contact
                         where cc.Email.ToLower() == Email.ToLower()
                         select cc).First();
                }
                catch (Exception ex)
                {
                    TempData["userNotRegistered"] = "The email provided is not registered yet. Please register via the link provided on the home page";
                    ViewBag.forgetPass = "The email provided is not registered yet. Please register via the link provided on the home page";
                    return View();
                    throw ex;
                }

                using (MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider())
                {
                    String firstName = c.FirstName;
                    String lastName = c.LastName;
                    byte[] key = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(firstName));
                    byte[] ivKey = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(lastName));

                    String keyString = HashBuilder(firstName);//ASCIIEncoding.UTF8.GetString(key);
                    String ivKeyString = HashBuilder(lastName);//ASCIIEncoding.UTF8.GetString(ivKey);

                    int keySize = 256;
                    String encriptedString = Encrypt(DateTime.Now.ToString(), key, ivKey, keySize);

                    var email_model = new PasswordResetEmailModel()
                    {
                        Email = Email,
                        EncryptedToken = encriptedString,
                        FullName = c.FirstName + " " + c.LastName
                    };
                    new MailController().PasswordReset(email_model).Deliver();

                }
                ViewBag.forgetPass = " An email has been sent to your provided email address, please follow the instruction given on the email.";
            }

            return View();
        }

        /* reset password method 
         * this method will be activated once the user go to the link provided on the email that was generated from forgetpassword method
         */

        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            if (Session["ContactReset"] != null)
                return View();
            else
            {
                TempData.Add("userNotFound", "There is a problem with the link provided");
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(String NewPassword, string ConfirmedPassword)
        {
            Common_Contact c = (Common_Contact)Session["ContactReset"];
            using (PlanetshakersEntities ps = new PlanetshakersEntities())
            {
                Common_Contact con = (from cc in ps.Common_Contact
                                      where cc.Contact_ID == c.Contact_ID
                                      select cc).FirstOrDefault();

                if (NewPassword.Equals(ConfirmedPassword))
                {
                    con.Password = NewPassword;
                    ps.SaveChanges();
                    TempData["successResetPass"] = "Password has been succesfully changed!";
                    Session["ContactReset"] = null;
                    return RedirectToAction("Login");
                }
                else
                {
                    ViewBag.passResetResult = "Password change was unsuccesful, Please try again";
                    return View();
                }
            }

        }

        public ActionResult RetrieveLogin(String email, String token)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Common_Contact contact = null;

                try
                {
                    contact = (from cc in context.Common_Contact
                               where cc.Email == email
                               select cc).First();
                }
                catch (Exception ex)
                {
                    TempData.Add("userNotFound", "Cant find username " + email);
                    return RedirectToAction("Login");
                    throw ex;
                }

                if (contact != null)
                {
                    using (MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider())
                    {
                        String firstName = contact.FirstName;
                        String lastName = contact.LastName;
                        String currentpassword = contact.Password;
                        byte[] key = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(firstName));
                        byte[] ivKey = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(lastName));

                        //String keyString = Convert.ToBase64String(key);
                        //String ivKeyString = Convert.ToBase64String(ivKey);

                        int keySize = 256;

                        String decriptedString = Decrypt(token, key, ivKey, keySize);
                        DateTime timeCreated = DateTime.Parse(decriptedString);
                        DateTime current = DateTime.Now;
                        TimeSpan diff = current.Subtract(timeCreated);

                        // diff represents the expiry time span of the link given to the user
                        // can be changed to different values 

                        if (diff.Minutes <= 10)
                        {

                            using (PlanetshakersEntities ps = new PlanetshakersEntities())
                            {
                                if (contact == null)
                                {
                                    TempData.Add("userNotFound", "There is a problem with the link provided");
                                    return RedirectToAction("Login");
                                }
                                else
                                {
                                    Session.Add("ContactReset", contact);
                                    return RedirectToAction("ResetPassword");
                                }
                            }

                        }
                        else
                        {
                            TempData.Add("linkExpired", "The link has been expired please request for a new link");
                            return RedirectToAction("Login");
                        }
                    }
                }
                else
                {
                    TempData.Add("userNotFound", "Cant find username " + email);
                    return RedirectToAction("Login");
                }
            }
        }

        #endregion

        #region Timeout

        [HttpPost]
        public ActionResult CheckSession()
        {
            //for now - until the cookie problem is fixed
            //bool alive = false;
            bool alive = true;
            string PSCookies = Planetshakers.Business.MWebMain.PSCookieAliveValue(Request);

            if (PSCookies != string.Empty)
            {
                alive = true;
            }

            return Json(new { alive = alive }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ExtendCookie()
        {
            var cookie = Request.Cookies[".BELIEVEAUTH"];

            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Set(cookie);
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Account Update
        [PSAuthorize]
        public ActionResult Update(string returnUrl)
        {

            //string PSCookies = Planetshakers.Business.MWebMain.PSCookieAliveValue(Request);
            //if (PSCookies != string.Empty)
            //{
            // Retrieve logged in contact details 
            var contact = getContactDetails();

            // Populate viewmodel
            var viewModel = new UpdateModel()
            {
                // Personal details
                ContactId = contact.id,
                Salutation = contact.personal.salutation_id,
                Firstname = contact.personal.firstname,
                Lastname = contact.personal.lastname,
                Gender = contact.personal.gender,
                Address1 = contact.personal.address1,
                Address2 = contact.personal.address2,
                Suburb = contact.personal.suburb,
                State_ID = contact.personal.state_id,
                Postcode = contact.personal.postcode,
                Mobile = contact.personal.mobile,
                Email = contact.personal.email,
                DoB = contact.personal.dob,

                // Church Details
                ChurchName = contact.church.name,
                ChurchSeniorPastor = contact.church.senior_pastor,
                ChurchLeader = contact.church.leader,
                ChurchSize = contact.church.congregation_size,
                ChurchAddress1 = contact.church.address1,
                ChurchAddress2 = contact.church.address2,
                ChurchSuburb = contact.church.suburb,
                ChurchState = contact.church.state,
                ChurchPostcode = contact.church.postcode,
                ChurchPhone = contact.church.phone,
                ChurchWebsite = contact.church.website,
                ChurchEmail = contact.church.email,
                AvailableCInvolvement = Lists.getChurchInvolvement(contact.id)
            };
            var emergencyList = GetEmergencyContacts(contact.id);
            viewModel.EmergencyContacts = emergencyList;

            viewModel.SelectedCInvolvement = viewModel.AvailableCInvolvement
                                                .Where(x => x.is_selected)
                                                .Select(x => x.id).ToArray();

            if (contact.personal.dob.HasValue)
                viewModel.DoB = (DateTime)contact.personal.dob;
            if (contact.personal.country_id.HasValue)
                viewModel.Country = (int)contact.personal.country_id;
            if (contact.church.denomination_id.HasValue)
                viewModel.ChurchDenomination = (int)contact.church.denomination_id;
            if (contact.church.country_id.HasValue)
                viewModel.ChurchCountry = (int)contact.church.country_id;


            ViewBag.ReturnUrl = returnUrl;
            if (TempData["updateAlert"] != null) ViewBag.Alert = TempData["updateAlert"].ToString();
            if (TempData["activeTab"] != null)
            {
                ViewBag.ActiveTab = TempData["activeTab"].ToString();
            }
            else
            {
                ViewBag.ActiveTab = "#tab-personal";
            }
            return View(viewModel);
        }

        [HttpPost]
        [PSAuthorize]
        [MultipleButton(Name = "update", Argument = "emergency")]
        public ActionResult UpdateEmergencyDetails(UpdateModel model, string returnUrl)
        {
            // Only continue if contact id is not null
            if (model.ContactId != 0)
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    foreach(var item in model.EmergencyContacts)
                    {
                        if(item.EmergencyContact_ID == 0)
                        {                            
                            var newEC = new Events_EmergencyContact
                            {
                                Contact_ID = model.ContactId,
                                EmergencyContactName = item.FullName,
                                EmergencyContactPhone = item.Mobile,
                                EmergencyContactRelationship = item.Relationship,
                                LastModifiedDate = DateTime.Now,
                                Comment = ""
                            };
                            context.Events_EmergencyContact.Add(newEC);
                        }
                        else
                        {
                            var emergencyContact = (from e in context.Events_EmergencyContact
                                                    where e.EmergencyContact_ID == item.EmergencyContact_ID
                                                    select e).FirstOrDefault();
                            if (emergencyContact != null)
                            {
                                emergencyContact.EmergencyContactName = (item.FullName != null) ? item.FullName : "";
                                emergencyContact.EmergencyContactPhone = (item.Mobile != null) ? item.Mobile : "";
                                emergencyContact.EmergencyContactRelationship = (item.Relationship != null) ? item.Relationship : "";
                                emergencyContact.LastModifiedDate = DateTime.Now;
                            }

                        }
                    }
                    context.SaveChanges();                    
                }
                TempData["updateAlert"] = "Emergency details updated!";
                TempData["activeTab"] = "#tab-emergency";
            }
            return RedirectToAction("Update");
        }

        [HttpPost]
        [PSAuthorize]
        [MultipleButton(Name = "update", Argument = "personal")]
        public ActionResult UpdatePersonalDetails(UpdateModel model, string returnUrl)
        {
            // Only continue if view model validation is correct.
            if (model.ContactId != 0)
            {
                try
                {
                    #region Validation

                    if (String.IsNullOrWhiteSpace(model.Firstname))
                        throw new Exception("Firstname is required.");

                    if (String.IsNullOrWhiteSpace(model.Lastname))
                        throw new Exception("Lastname is required.");

                    if (String.IsNullOrWhiteSpace(model.Suburb))
                        throw new Exception("Suburb/City is required");

                    //if (model.State_ID == 0)
                    //    throw new Exception("State is required");

                    if (String.IsNullOrWhiteSpace(model.Postcode))
                        throw new Exception("Zip/Postcode is required");

                    if (String.IsNullOrWhiteSpace(model.Mobile))
                        throw new Exception("Mobile is required");

                    #endregion

                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // Retrieve contact to be updated
                        var ps_contact = (from cc in context.Common_Contact
                                          where cc.Contact_ID == model.ContactId
                                          select cc).First();

                        // Update values
                        ps_contact.Salutation_ID = model.Salutation;
                        ps_contact.FirstName = model.Firstname;
                        ps_contact.LastName = model.Lastname;
                        ps_contact.Address1 = model.Address1;
                        ps_contact.Address2 = model.Address2;
                        ps_contact.Suburb = model.Suburb;
                        ps_contact.State_ID = model.State_ID;
                        ps_contact.Postcode = model.Postcode;
                        ps_contact.Country_ID = model.Country;
                        ps_contact.Mobile = model.Mobile;
                        ps_contact.Gender = model.Gender;
                        ps_contact.DateOfBirth = model.DoB;

                        // submit changes
                        context.SaveChanges();
                    }
                    TempData["updateAlert"] = "Personal details updated!";
                }
                catch (Exception ex)
                {
                    TempData["updateAlert"] = ex.Message;
                }
                TempData["activeTab"] = "#tab-personal";
            }
            return RedirectToAction("Update");
        }

        [HttpPost]
        [PSAuthorize]
        [MultipleButton(Name = "update", Argument = "church")]
        public ActionResult UpdateChurchDetails(UpdateModel model, string returnUrl)
        {
            // Only continue if view model validation is correct.
            if (model.ContactId != 0)
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Retrieve external contact details
                    var contact_external = (from cce in context.Common_ContactExternal
                                            where cce.Contact_ID == model.ContactId
                                            select cce).ToList();
                    if (contact_external.Any())
                    {
                        // Update values
                        contact_external.First().ChurchName = model.ChurchName;
                        contact_external.First().Denomination_ID = model.ChurchDenomination;
                        contact_external.First().SeniorPastorName = model.ChurchSeniorPastor;
                        contact_external.First().LeaderName = model.ChurchLeader;
                        contact_external.First().ChurchNumberOfPeople = model.ChurchSize;
                        contact_external.First().ChurchAddress1 = model.ChurchAddress1;
                        contact_external.First().ChurchAddress2 = model.ChurchAddress2;
                        contact_external.First().ChurchSuburb = model.ChurchSuburb;
                        contact_external.First().ChurchStateOther = model.ChurchState;
                        contact_external.First().ChurchPostcode = model.ChurchPostcode;
                        contact_external.First().ChurchCountry_ID = model.ChurchCountry;
                        contact_external.First().ChurchPhone = model.ChurchPhone;
                        contact_external.First().ChurchWebsite = model.ChurchWebsite;
                        contact_external.First().ChurchEmail = model.ChurchEmail;
                    }
                    else
                    {
                        // Create
                        var new_external = new Common_ContactExternal();
                        new_external.ChurchName = model.ChurchName;
                        new_external.Denomination_ID = model.ChurchDenomination;
                        new_external.SeniorPastorName = model.ChurchSeniorPastor;
                        new_external.LeaderName = model.ChurchLeader;
                        new_external.ChurchNumberOfPeople = model.ChurchSize;
                        new_external.ChurchAddress1 = model.ChurchAddress1;
                        new_external.ChurchAddress2 = model.ChurchAddress2;
                        new_external.ChurchSuburb = model.ChurchSuburb;
                        new_external.ChurchStateOther = model.ChurchState;
                        new_external.ChurchPostcode = model.ChurchPostcode;
                        new_external.ChurchCountry_ID = model.ChurchCountry;
                        new_external.ChurchPhone = model.ChurchPhone;
                        new_external.ChurchWebsite = model.ChurchWebsite;
                        new_external.ChurchEmail = model.ChurchEmail;

                        context.Common_ContactExternal.Add(new_external);
                    }

                    // Update church involvement
                    model.AvailableCInvolvement = Lists.getChurchInvolvement(model.ContactId);
                    if (model.SelectedCInvolvement != null)
                    {
                        foreach (var ci in model.AvailableCInvolvement)
                        {
                            var isInvolved = (from cci in context.External_ContactChurchInvolvement
                                              where cci.Contact_ID == model.ContactId &&
                                                     cci.ChurchInvolvement_ID == ci.id
                                              select cci).ToList();
                            if (model.SelectedCInvolvement.Contains(ci.id))
                            {
                                // Check if entry exists, if it doesn't, create
                                if (!isInvolved.Any())
                                {
                                    var new_involvement = new External_ContactChurchInvolvement
                                    {
                                        Contact_ID = model.ContactId,
                                        ChurchInvolvement_ID = ci.id,
                                        DateAdded = DateTime.Now
                                    };
                                    context.External_ContactChurchInvolvement.Add(new_involvement);
                                }
                            }
                            else
                            {
                                // Check if entry exists, if it does, delete
                                if (isInvolved.Any())
                                {
                                    context.External_ContactChurchInvolvement
                                        .Remove(isInvolved.First());
                                }
                            }
                        }
                    }
                    // submit changes
                    context.SaveChanges();
                }
                TempData["updateAlert"] = "Church details updated!";
                TempData["activeTab"] = "#tab-church";
            }
            return RedirectToAction("Update");
        }



        [HttpPost]
        [PSAuthorize]
        [MultipleButton(Name = "update", Argument = "credential")]
        public ActionResult UpdateCredentialDetails(UpdateModel model, string returnUrl)
        {
            // Only continue if view model validation is correct
            if (model.ContactId != 0)
            {
                try
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // Retrieve contact details
                        var contact = (from cc in context.Common_Contact
                                       where cc.Contact_ID == model.ContactId
                                       select cc).FirstOrDefault();

                        if (contact == null)
                        {
                            throw new Exception("An internal error has occured. Contact not found.");
                        }
                        else
                        {
                            // Verify password
                            if (model.CurrentPassword != contact.Password)
                            {
                                throw new Exception("Current password does not match");
                            }

                            // Verify new password
                            if (model.NewPassword != model.ConfirmNewPassword)
                            {
                                throw new Exception("New Passwords do not match");
                            }

                            // Update password
                            contact.Password = model.NewPassword;
                            context.SaveChanges();

                            TempData["updateAlert"] = "Credentials updated!";
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["updateAlert"] = ex.Message;
                }

                TempData["activeTab"] = "#tab-credential";
            }
            return RedirectToAction("Update");
        }

        #endregion

        #region Allocation
        [HttpPost]
        [PSAuthorize]
        public ActionResult Allocate(EventSummaryModel model)
        {
            try
            {
                #region Validation

                // Validate the data
                if (String.IsNullOrWhiteSpace(model.personalDetails.Firstname))
                {
                    throw new Exception("First name is required");
                }
                if (String.IsNullOrWhiteSpace(model.personalDetails.Lastname))
                {
                    throw new Exception("Last name is required");
                }
                if (String.IsNullOrWhiteSpace(model.contactDetails.Suburb))
                {
                    throw new Exception("Suburb/City is required");
                }
                if (String.IsNullOrWhiteSpace(model.contactDetails.StateOther))
                {
                    throw new Exception("State is required");
                }
                if (String.IsNullOrWhiteSpace(model.contactDetails.Postcode))
                {
                    throw new Exception("Zip/Postcode is required");
                }
                if (String.IsNullOrWhiteSpace(model.contactDetails.Mobile))
                {
                    throw new Exception("Mobile is required");
                }
                if (String.IsNullOrWhiteSpace(model.contactDetails.Email))
                {
                    throw new Exception("Email is required");
                }

                #endregion

                // do something
                if (ModelState.IsValid)
                {
                    // Retrieve logged in contact from session
                    var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                    model.Group = new EventGroup(contact.Id);

                    // Create a new account (with no password)

                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // create new common_contactExternalEvents
                        //var new_contact_event = new Common_ContactExternalEvents();
                        //new_contact_event.EmergencyContactName = model.emergencyDetails.EmergencyContactName;
                        //new_contact_event.EmergencyContactPhone = model.emergencyDetails.EmergencyContactPhone;
                        //new_contact_event.EmergencyContactRelationship = model.emergencyDetails.EmergencyContactRelationship;
                        //new_contact_event.MedicalInformation = model.emergencyDetails.MedicalInfo;
                        //new_contact_event.MedicalAllergies = model.emergencyDetails.MedicalAllergies;
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "";
                        //new_contact_event.LastModifiedDate = DateTime.Now;

                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.ChurchAddress1 = model.churchDetails.ChurchAddress1;
                        new_external_contact.ChurchAddress2 = model.churchDetails.ChurchAddress2;
                        new_external_contact.ChurchCountry_ID = model.churchDetails.ChurchCountry;
                        new_external_contact.ChurchEmail = model.churchDetails.ChurchEmail;
                        new_external_contact.ChurchName = model.churchDetails.ChurchName;
                        new_external_contact.ChurchNumberOfPeople = model.churchDetails.ChurchSize;
                        new_external_contact.ChurchPhone = model.churchDetails.ChurchPhone;
                        new_external_contact.ChurchPostcode = model.churchDetails.ChurchPostcode;
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = model.churchDetails.ChurchState;
                        new_external_contact.ChurchSuburb = model.churchDetails.ChurchSuburb;
                        new_external_contact.ChurchWebsite = model.churchDetails.ChurchWebsite;
                        new_external_contact.Denomination_ID = model.churchDetails.ChurchDenomination;
                        new_external_contact.SeniorPastorName = model.churchDetails.ChurchSeniorPastor;
                        new_external_contact.LeaderName = model.churchDetails.ChurchLeader;
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;
                        //new_external_contact.Common_ContactExternalEvents = new_contact_event;

                        // create new contact object
                        var new_contact = new Common_Contact();
                        new_contact.Salutation_ID = model.personalDetails.Salutation;
                        new_contact.FirstName = model.personalDetails.Firstname;
                        new_contact.LastName = model.personalDetails.Lastname;
                        new_contact.Gender = model.personalDetails.Gender;
                        new_contact.DateOfBirth = model.personalDetails._dob;
                        new_contact.Address1 = model.contactDetails.Address1;
                        new_contact.Address2 = model.contactDetails.Address2;
                        new_contact.Suburb = model.contactDetails.Suburb;
                        new_contact.Postcode = model.contactDetails.Postcode;
                        new_contact.State_ID = null;
                        new_contact.StateOther = model.contactDetails.StateOther;
                        new_contact.Country_ID = model.contactDetails.Country;
                        new_contact.Phone = "";
                        new_contact.PhoneWork = "";
                        new_contact.Fax = "";
                        new_contact.Mobile = model.contactDetails.Mobile;
                        new_contact.Email = "";
                        new_contact.Email2 = model.contactDetails.Email;
                        new_contact.DoNotIncludeEmail1InMailingList = false;
                        new_contact.DoNotIncludeEmail2InMailingList = false;
                        new_contact.Family_ID = null;
                        new_contact.FamilyMemberType_ID = null;
                        new_contact.Password = "";
                        new_contact.VolunteerPoliceCheck = "";
                        new_contact.VolunteerPoliceCheckDate = null;
                        new_contact.VolunteerWWCC = "";
                        new_contact.VolunteerWWCCDate = null;
                        new_contact.VolunteerWWCCType_ID = null;
                        new_contact.CreationDate = DateTime.Now;
                        new_contact.CreatedBy_ID = 101;
                        new_contact.ModificationDate = DateTime.Now;
                        new_contact.ModifiedBy_ID = null;
                        new_contact.GUID = Guid.NewGuid();
                        new_contact.AnotherLanguage_ID = null;
                        new_contact.Nationality_ID = null;
                        new_contact.Common_ContactExternal = new_external_contact;

                        context.Common_Contact.Add(new_contact);
                        context.SaveChanges();

                        // Update church involvement
                        if (model.churchInvolvementDetails != null)
                        {
                            model.churchInvolvementDetails.AvailableCInvolvement =
                                Lists.getChurchInvolvement(new_contact.Contact_ID);
                            foreach (var ci in model.churchInvolvementDetails.AvailableCInvolvement)
                            {
                                var isInvolved = (from cci in context.External_ContactChurchInvolvement
                                                  where cci.Contact_ID == new_contact.Contact_ID &&
                                                         cci.ChurchInvolvement_ID == ci.id
                                                  select cci).ToList();
                                if (model.churchInvolvementDetails.SelectedCInvolvement.Contains(ci.id))
                                {
                                    // Check if entry exists, if it doesn't, create
                                    if (!isInvolved.Any())
                                    {
                                        var new_involvement = new External_ContactChurchInvolvement
                                        {
                                            Contact_ID = new_contact.Contact_ID,
                                            ChurchInvolvement_ID = ci.id,
                                            DateAdded = DateTime.Now
                                        };
                                        context.External_ContactChurchInvolvement.Add(new_involvement);
                                    }
                                }
                                else
                                {
                                    // Check if entry exists, if it does, delete
                                    if (isInvolved.Any())
                                    {
                                        context.External_ContactChurchInvolvement
                                            .Remove(isInvolved.First());
                                    }
                                }
                            }
                            context.SaveChanges();
                        }

                        // Allocate registration to group
                        model.Group.AllocateRegistration(new_contact.Contact_ID,
                                                   model.SelectedEvent.venueID,
                                                   model.registrationTypeID);
                        model.Group.Save(contact.Id);

                        var group_list = (from egc in context.Events_GroupContact
                                          where egc.GroupLeader_ID == contact.Id
                                          select egc.Contact_ID).ToList();

                        if (!group_list.Contains(contact.Id))
                        {
                            var new_group_contact = new Events_GroupContact();
                            new_group_contact.GroupLeader_ID = (int)contact.Id;
                            new_group_contact.Contact_ID = new_contact.Contact_ID;

                            context.Events_GroupContact.Add(new_group_contact);
                            context.SaveChanges();
                        }
                    }
                }

                return Json(new
                {
                    isSuccess = true,
                    curRegos = model.CurrentSingleRegistrations,
                    allocated = model.AllocatedRegistrations,
                    allocatedKids = model.AllocatedKidsRegistrations,
                    allocatedQty = model.GroupAllocatedCount,
                    unallocated = model.UnallocatedRegistrations,
                    unallocatedQty = model.GroupUnallocatedCount,
                    invites = model.PendingInvitations
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = ex.Message
                });
            }
        }


        [HttpPost]
        [PSAuthorize]
        public ActionResult BasicAllocate(EventSummaryModel model)
        {
            try
            {
                #region Validation

                // Validate the data
                if (String.IsNullOrWhiteSpace(model.basicDetails.Firstname))
                {
                    throw new Exception("First name is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Lastname))
                {
                    throw new Exception("Last name is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Mobile))
                {
                    throw new Exception("Mobile is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Email))
                {
                    throw new Exception("Email is required");
                }

                #endregion
                // do something
                if (ModelState.IsValid)
                {
                    // Retrieve logged in contact from session
                    var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                    model.Group = new EventGroup(contact.Id);

                    // Create a new account (with no password)

                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // create new common_contactExternalEvents
                        //var new_contact_event = new Common_ContactExternalEvents();
                        //new_contact_event.EmergencyContactName = "";
                        //new_contact_event.EmergencyContactPhone = "";
                        //new_contact_event.EmergencyContactRelationship = "";
                        //new_contact_event.MedicalInformation = "";
                        //new_contact_event.MedicalAllergies = "";
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "";

                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.ChurchAddress1 = "";
                        new_external_contact.ChurchAddress2 = "";
                        new_external_contact.ChurchCountry_ID = null;
                        new_external_contact.ChurchEmail = "";
                        new_external_contact.ChurchName = model.basicDetails.Church;
                        new_external_contact.ChurchNumberOfPeople = "";
                        new_external_contact.ChurchPhone = "";
                        new_external_contact.ChurchPostcode = "";
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = "";
                        new_external_contact.ChurchSuburb = "";
                        new_external_contact.ChurchWebsite = "";
                        new_external_contact.Denomination_ID = 49;
                        new_external_contact.SeniorPastorName = "";
                        new_external_contact.LeaderName = "";
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;
                        //new_external_contact.Common_ContactExternalEvents = new_contact_event;

                        // create new contact object
                        var new_contact = new Common_Contact();
                        new_contact.Salutation_ID = 1;
                        new_contact.FirstName = model.basicDetails.Firstname;
                        new_contact.LastName = model.basicDetails.Lastname;
                        new_contact.Gender = "";
                        new_contact.DateOfBirth = model.basicDetails._dob;
                        new_contact.Address1 = "";
                        new_contact.Address2 = "";
                        new_contact.Suburb = "";
                        new_contact.Postcode = model.basicDetails.Postcode;
                        new_contact.State_ID = null;
                        new_contact.StateOther = "";
                        new_contact.Country_ID = model.basicDetails.Country;
                        new_contact.Phone = "";
                        new_contact.PhoneWork = "";
                        new_contact.Fax = "";
                        new_contact.Mobile = model.basicDetails.Mobile;
                        new_contact.Email = "";
                        new_contact.Email2 = model.basicDetails.Email;
                        new_contact.DoNotIncludeEmail1InMailingList = false;
                        new_contact.DoNotIncludeEmail2InMailingList = false;
                        new_contact.Family_ID = null;
                        new_contact.FamilyMemberType_ID = null;
                        new_contact.Password = "";
                        new_contact.VolunteerPoliceCheck = "";
                        new_contact.VolunteerPoliceCheckDate = null;
                        new_contact.VolunteerWWCC = "";
                        new_contact.VolunteerWWCCDate = null;
                        new_contact.VolunteerWWCCType_ID = null;
                        new_contact.CreationDate = DateTime.Now;
                        new_contact.CreatedBy_ID = 101;
                        new_contact.ModificationDate = DateTime.Now;
                        new_contact.ModifiedBy_ID = null;
                        new_contact.GUID = Guid.NewGuid();
                        new_contact.AnotherLanguage_ID = null;
                        new_contact.Nationality_ID = null;
                        new_contact.Common_ContactExternal = new_external_contact;

                        context.Common_Contact.Add(new_contact);
                        context.SaveChanges();

                        // Allocate registration to group
                        model.Group.AllocateRegistration(new_contact.Contact_ID,
                                                   model.SelectedEvent.venueID,
                                                   model.registrationTypeID);
                        model.Group.Save(contact.Id);

                        var group_list = (from egc in context.Events_GroupContact
                                          where egc.GroupLeader_ID == contact.Id
                                          select egc.Contact_ID).ToList();

                        if (!group_list.Contains(contact.Id))
                        {
                            var new_group_contact = new Events_GroupContact();
                            new_group_contact.GroupLeader_ID = (int)contact.Id;
                            new_group_contact.Contact_ID = new_contact.Contact_ID;

                            context.Events_GroupContact.Add(new_group_contact);
                            context.SaveChanges();
                        }
                    }
                }

                return Json(new
                {
                    isSuccess = true,
                    message = "Allocation successful.",
                    curRegos = model.CurrentSingleRegistrations,
                    allocated = model.AllocatedRegistrations,
                    allocatedKids = model.AllocatedKidsRegistrations,
                    allocatedQty = model.GroupAllocatedCount,
                    unallocated = model.UnallocatedRegistrations,
                    unallocatedQty = model.GroupUnallocatedCount,
                    invites = model.PendingInvitations
                });

            }

            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = ex.Message
                });
            }
        }

        [HttpPost]
        [PSAuthorize]
        public ActionResult AllocateKid(EventSummaryModel model)
        {
            try
            {
                #region Validation
                // Validate the data
                if (String.IsNullOrWhiteSpace(model.personalDetails.Firstname))
                {
                    throw new Exception("First name is required");
                }
                if (String.IsNullOrWhiteSpace(model.personalDetails.Lastname))
                {
                    throw new Exception("Last name is required");
                }
                if (String.IsNullOrWhiteSpace(model.contactDetails.Suburb))
                {
                    throw new Exception("Suburb/City is required");
                }
                if (String.IsNullOrWhiteSpace(model.contactDetails.StateOther))
                {
                    throw new Exception("State is required");
                }
                if (String.IsNullOrWhiteSpace(model.contactDetails.Postcode))
                {
                    throw new Exception("Zip/Postcode is required");
                }
                if (String.IsNullOrWhiteSpace(model.contactDetails.Mobile))
                {
                    throw new Exception("Mobile is required");
                }
                if (String.IsNullOrWhiteSpace(model.emergencyDetails.EmergencyContactName))
                {
                    throw new Exception("Emergency contact name is required");
                }
                if (String.IsNullOrWhiteSpace(model.emergencyDetails.EmergencyContactPhone))
                {
                    throw new Exception("Emergency contact phone is required");
                }
                if (String.IsNullOrWhiteSpace(model.emergencyDetails.EmergencyContactRelationship))
                {
                    throw new Exception("Emergency contact relationship is required");
                }
                #endregion

                // do something
                if (ModelState.IsValid)
                {
                    // Retrieve logged in contact from session
                    var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                    model.Group = new EventGroup(contact.Id);

                    // Create a new account (with no password)                       
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // create new common_contactExternalEvents
                        //var new_contact_event = new Common_ContactExternalEvents();
                        //new_contact_event.EmergencyContactName = model.emergencyDetails.EmergencyContactName;
                        //new_contact_event.EmergencyContactPhone = model.emergencyDetails.EmergencyContactPhone;
                        //new_contact_event.EmergencyContactRelationship = model.emergencyDetails.EmergencyContactRelationship;
                        //new_contact_event.MedicalInformation = model.emergencyDetails.MedicalInfo;
                        //new_contact_event.MedicalAllergies = model.emergencyDetails.MedicalAllergies;
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "Planetkids";
                        //new_contact_event.LastModifiedDate = DateTime.Now;

                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.ChurchAddress1 = model.churchDetails.ChurchAddress1;
                        new_external_contact.ChurchAddress2 = model.churchDetails.ChurchAddress2;
                        new_external_contact.ChurchCountry_ID = model.churchDetails.ChurchCountry;
                        new_external_contact.ChurchEmail = model.churchDetails.ChurchEmail;
                        new_external_contact.ChurchName = model.churchDetails.ChurchName;
                        new_external_contact.ChurchNumberOfPeople = model.churchDetails.ChurchSize;
                        new_external_contact.ChurchPhone = model.churchDetails.ChurchPhone;
                        new_external_contact.ChurchPostcode = model.churchDetails.ChurchPostcode;
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = model.churchDetails.ChurchState;
                        new_external_contact.ChurchSuburb = model.churchDetails.ChurchSuburb;
                        new_external_contact.ChurchWebsite = model.churchDetails.ChurchWebsite;
                        new_external_contact.Denomination_ID = model.churchDetails.ChurchDenomination;
                        new_external_contact.SeniorPastorName = model.churchDetails.ChurchSeniorPastor;
                        new_external_contact.LeaderName = model.churchDetails.ChurchLeader;
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;
                        //new_external_contact.Common_ContactExternalEvents = new_contact_event;

                        // create new contact object
                        var new_contact = new Common_Contact();
                        new_contact.Salutation_ID = model.personalDetails.Salutation;
                        new_contact.FirstName = model.personalDetails.Firstname;
                        new_contact.LastName = model.personalDetails.Lastname;
                        new_contact.Gender = model.personalDetails.Gender;
                        new_contact.DateOfBirth = model.personalDetails._dob;
                        new_contact.Address1 = model.contactDetails.Address1;
                        new_contact.Address2 = model.contactDetails.Address2;
                        new_contact.Suburb = model.contactDetails.Suburb;
                        new_contact.Postcode = model.contactDetails.Postcode;
                        new_contact.State_ID = null;
                        new_contact.StateOther = model.contactDetails.StateOther;
                        new_contact.Country_ID = model.contactDetails.Country;
                        new_contact.Phone = "";
                        new_contact.PhoneWork = "";
                        new_contact.Fax = "";
                        new_contact.Mobile = model.contactDetails.Mobile;
                        new_contact.Email = "";
                        new_contact.Email2 = "";
                        new_contact.DoNotIncludeEmail1InMailingList = false;
                        new_contact.DoNotIncludeEmail2InMailingList = false;
                        new_contact.Family_ID = null;
                        new_contact.FamilyMemberType_ID = null;
                        new_contact.Password = "";
                        new_contact.VolunteerPoliceCheck = "";
                        new_contact.VolunteerPoliceCheckDate = null;
                        new_contact.VolunteerWWCC = "";
                        new_contact.VolunteerWWCCDate = null;
                        new_contact.VolunteerWWCCType_ID = null;
                        new_contact.CreationDate = DateTime.Now;
                        new_contact.CreatedBy_ID = 101;
                        new_contact.ModificationDate = DateTime.Now;
                        new_contact.ModifiedBy_ID = null;
                        new_contact.GUID = Guid.NewGuid();
                        new_contact.AnotherLanguage_ID = null;
                        new_contact.Nationality_ID = null;
                        new_contact.Common_ContactExternal = new_external_contact;

                        context.Common_Contact.Add(new_contact);
                        context.SaveChanges();

                        // Allocate registration to group
                        var new_rego = model.Group.AllocateRegistration(
                                            new_contact.Contact_ID,
                                            model.SelectedEvent.venueID,
                                            model.registrationTypeID);

                        model.Group.Save(contact.Id);

                        var group_list = (from egc in context.Events_GroupContact
                                          where egc.GroupLeader_ID == contact.Id
                                          select egc.Contact_ID).ToList();

                        if (!group_list.Contains(contact.Id))
                        {
                            var new_group_contact = new Events_GroupContact();
                            new_group_contact.GroupLeader_ID = (int)contact.Id;
                            new_group_contact.Contact_ID = new_contact.Contact_ID;

                            context.Events_GroupContact.Add(new_group_contact);
                            context.SaveChanges();
                        }


                        // Add aditional child information
                        var new_child_info = new Events_RegistrationChildInformation();
                        new_child_info.Registration_ID = (int)new_rego.registrationID;
                        new_child_info.CrecheChildSchoolGrade_ID = model.personalDetails.Grade;
                        new_child_info.ChildsFriend = "";
                        new_child_info.DropOffPickUpName = "";
                        new_child_info.DropOffPickUpRelationship = "";
                        new_child_info.DropOffPickUpContactNumber = "";
                        new_child_info.ChildrensChurchPastor = "";
                        new_child_info.ConsentFormSent = false;
                        new_child_info.ConsentFormSentDate = null;
                        new_child_info.ConsentFormReturned = false;
                        new_child_info.ConsentFormReturnedDate = null;

                        context.Events_RegistrationChildInformation.Add(new_child_info);
                        context.SaveChanges();
                    }
                }

                return Json(new
                {
                    isSuccess = true,
                    curRegos = model.CurrentSingleRegistrations,
                    allocated = model.AllocatedRegistrations,
                    allocatedKids = model.AllocatedKidsRegistrations,
                    allocatedQty = model.GroupAllocatedCount,
                    unallocated = model.UnallocatedRegistrations,
                    unallocatedQty = model.GroupUnallocatedCount,
                    invites = model.PendingInvitations
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = ex.Message
                });
            }
        }

        [HttpPost]
        [PSAuthorize]
        public ActionResult BasicAllocateKid(EventSummaryModel model)
        {
            try
            {
                #region Validation

                // Validate the data
                if (String.IsNullOrWhiteSpace(model.basicDetails.Firstname))
                {
                    throw new Exception("First name is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Lastname))
                {
                    throw new Exception("Last name is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Mobile))
                {
                    throw new Exception("Mobile is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Email))
                {
                    throw new Exception("Email is required");
                }
                if (String.IsNullOrWhiteSpace(model.emergencyDetails.EmergencyContactName))
                {
                    throw new Exception("Emergency contact name is required");
                }
                if (String.IsNullOrWhiteSpace(model.emergencyDetails.EmergencyContactPhone))
                {
                    throw new Exception("Emergency contact phone is required");
                }
                if (String.IsNullOrWhiteSpace(model.emergencyDetails.EmergencyContactRelationship))
                {
                    throw new Exception("Emergency contact relationship is required");
                }
                #endregion

                // do something
                if (ModelState.IsValid)
                {
                    // Retrieve logged in contact from session
                    var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

                    model.Group = new EventGroup(contact.Id);

                    // Create a new account (with no password)                       
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // create new common_contactExternalEvents
                        //var new_contact_event = new Common_ContactExternalEvents();
                        //new_contact_event.EmergencyContactName = model.emergencyDetails.EmergencyContactName;
                        //new_contact_event.EmergencyContactPhone = model.emergencyDetails.EmergencyContactPhone;
                        //new_contact_event.EmergencyContactRelationship = model.emergencyDetails.EmergencyContactRelationship;
                        //new_contact_event.MedicalInformation = model.emergencyDetails.MedicalInfo;
                        //new_contact_event.MedicalAllergies = model.emergencyDetails.MedicalAllergies;
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "Planetkids";
                        //new_contact_event.LastModifiedDate = DateTime.Now;

                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.ChurchAddress1 = "";
                        new_external_contact.ChurchAddress2 = "";
                        new_external_contact.ChurchCountry_ID = null;
                        new_external_contact.ChurchEmail = "";
                        new_external_contact.ChurchName = "";
                        new_external_contact.ChurchNumberOfPeople = "";
                        new_external_contact.ChurchPhone = "";
                        new_external_contact.ChurchPostcode = "";
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = "";
                        new_external_contact.ChurchSuburb = "";
                        new_external_contact.ChurchWebsite = "";
                        new_external_contact.Denomination_ID = 49;
                        new_external_contact.SeniorPastorName = "";
                        new_external_contact.LeaderName = "";
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;
                        //new_external_contact.Common_ContactExternalEvents = new_contact_event;

                        // create new contact object
                        var new_contact = new Common_Contact();
                        new_contact.Salutation_ID = 1;
                        new_contact.FirstName = model.basicDetails.Firstname;
                        new_contact.LastName = model.basicDetails.Lastname;
                        new_contact.Gender = "";
                        new_contact.DateOfBirth = model.basicDetails._dob;
                        new_contact.Address1 = "";
                        new_contact.Address2 = "";
                        new_contact.Suburb = model.basicDetails.Suburb;
                        new_contact.Postcode = "";
                        new_contact.State_ID = null;
                        new_contact.StateOther = "";
                        new_contact.Country_ID = model.basicDetails.Country;
                        new_contact.Phone = "";
                        new_contact.PhoneWork = "";
                        new_contact.Fax = "";
                        new_contact.Mobile = model.basicDetails.Mobile;
                        new_contact.Email = "";
                        new_contact.Email2 = model.basicDetails.Email;
                        new_contact.DoNotIncludeEmail1InMailingList = false;
                        new_contact.DoNotIncludeEmail2InMailingList = false;
                        new_contact.Family_ID = null;
                        new_contact.FamilyMemberType_ID = null;
                        new_contact.Password = "";
                        new_contact.VolunteerPoliceCheck = "";
                        new_contact.VolunteerPoliceCheckDate = null;
                        new_contact.VolunteerWWCC = "";
                        new_contact.VolunteerWWCCDate = null;
                        new_contact.VolunteerWWCCType_ID = null;
                        new_contact.CreationDate = DateTime.Now;
                        new_contact.CreatedBy_ID = 101;
                        new_contact.ModificationDate = DateTime.Now;
                        new_contact.ModifiedBy_ID = null;
                        new_contact.GUID = Guid.NewGuid();
                        new_contact.AnotherLanguage_ID = null;
                        new_contact.Nationality_ID = null;
                        new_contact.Common_ContactExternal = new_external_contact;

                        context.Common_Contact.Add(new_contact);
                        context.SaveChanges();

                        // Allocate registration to group
                        var new_rego = model.Group.AllocateRegistration(
                                            new_contact.Contact_ID,
                                            model.SelectedEvent.venueID,
                                            model.registrationTypeID);

                        model.Group.Save(contact.Id);

                        var group_list = (from egc in context.Events_GroupContact
                                          where egc.GroupLeader_ID == contact.Id
                                          select egc.Contact_ID).ToList();

                        if (!group_list.Contains(contact.Id))
                        {
                            var new_group_contact = new Events_GroupContact();
                            new_group_contact.GroupLeader_ID = (int)contact.Id;
                            new_group_contact.Contact_ID = new_contact.Contact_ID;

                            context.Events_GroupContact.Add(new_group_contact);
                            context.SaveChanges();
                        }

                        // Add aditional child information
                        var new_child_info = new Events_RegistrationChildInformation();
                        new_child_info.Registration_ID = (int)new_rego.registrationID;
                        new_child_info.CrecheChildSchoolGrade_ID = model.basicDetails.Grade;
                        new_child_info.ChildsFriend = "";
                        new_child_info.DropOffPickUpName = "";
                        new_child_info.DropOffPickUpRelationship = "";
                        new_child_info.DropOffPickUpContactNumber = "";
                        new_child_info.ChildrensChurchPastor = "";
                        new_child_info.ConsentFormSent = false;
                        new_child_info.ConsentFormSentDate = null;
                        new_child_info.ConsentFormReturned = false;
                        new_child_info.ConsentFormReturnedDate = null;

                        context.Events_RegistrationChildInformation.Add(new_child_info);
                        context.SaveChanges();
                    }
                }

                return Json(new
                {
                    isSuccess = true,
                    curRegos = model.CurrentSingleRegistrations,
                    allocated = model.AllocatedRegistrations,
                    allocatedKids = model.AllocatedKidsRegistrations,
                    allocatedQty = model.GroupAllocatedCount,
                    unallocated = model.UnallocatedRegistrations,
                    unallocatedQty = model.GroupUnallocatedCount,
                    invites = model.PendingInvitations
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = ex.Message
                });
            }
        }

        [HttpPost]
        [PSAuthorize]
        public ActionResult AllocateSelf(int registrationTypeID)
        {
            // Retrieve logged in contact details (group leader)
            var contact = getContactDetails();
            EventSummaryModel returnModel = new EventSummaryModel();
            returnModel.Group = new EventGroup(contact.id);

            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Retrieve selected event
                    var venue_id = (from ev in context.Events_RegistrationTypeVenue
                                    where ev.RegistrationType_ID == registrationTypeID
                                    select ev.Venue_ID).FirstOrDefault();

                    var selected_event = new Event(venue_id);

                    // Allocate registration
                    var new_registration = returnModel.Group.AllocateRegistration(contact.id,
                                                           venue_id,
                                                           registrationTypeID);
                    returnModel.Group.Save(contact.id);
                    returnModel.SelectedEvent = selected_event;

                    var conference = (from ev in context.Events_Venue
                                      join ec in context.Events_Conference
                                      on ev.Conference_ID equals ec.Conference_ID
                                      where ev.Venue_ID == venue_id
                                      select new Event
                                      {
                                          id = ec.Conference_ID,
                                          venueID = ev.Venue_ID,
                                          name = ec.ConferenceName,
                                          date = ec.ConferenceDate,
                                          location = ec.ConferenceLocation,
                                          venueLocation = ev.VenueName,
                                          shortname = ec.ConferenceNameShort
                                      }).FirstOrDefault();

                    var confirmationBarcode = new ConfirmationBarcode()
                    {
                        Subject = "Quick Check In - " + conference.name,
                        EventName = conference.name,
                        EventDate = conference.date.ToString(),
                        EventVenue = conference.location,
                        VenueLocation = conference.venueLocation,
                        Email = (String.IsNullOrEmpty(contact.personal.email)) ? contact.personal.email2 : contact.personal.email,
                        RegistrationNumber = new_registration.registrationID.ToString(),
                        RegistrationTypeID = (int)new_registration.registrationTypeID
                    };

                    if (!String.IsNullOrEmpty(confirmationBarcode.Email))
                    {
                        new MailController().ConfirmationBarcodeEmail(confirmationBarcode);
                    }

                    // Return true
                    return Json(new
                    {
                        isSuccess = true,
                        curRegos = returnModel.CurrentSingleRegistrations,
                        allocated = returnModel.AllocatedRegistrations,
                        allocatedKids = returnModel.AllocatedKidsRegistrations,
                        allocatedQty = returnModel.GroupAllocatedCount,
                        unallocated = returnModel.UnallocatedRegistrations,
                        unallocatedQty = returnModel.GroupUnallocatedCount,
                        invites = returnModel.PendingInvitations
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = ex.Message
                });
            }
        }

        #region Sorta temporary (for ps16) sould be removed aferwards

        /// <summary>
        /// Changes the registration type.  If registration is under a group, it will change the 
        /// </summary>
        /// <param name="registrationID"></param>
        /// <param name="registrationTypeID"></param>
        /// <returns></returns>
        public ActionResult ChangeRegistrationType(int registrationID, int newRegistrationTypeID)
        {
            try
            {
                if (newRegistrationTypeID != 1521 &&
                    newRegistrationTypeID != 1522 &&
                    newRegistrationTypeID != 1523 &&
                    newRegistrationTypeID != 1524) throw new Exception("Invalid request");

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Retrieve the registration
                    var registration = (from er in context.Events_Registration
                                        where er.Registration_ID == registrationID
                                        select er).First();

                    // Stop and return error if registration is not found
                    if (registration == null) throw new Exception("Registration not found.");

                    // Store previous registration type if we need to update bulk purchases
                    var prevType = registration.RegistrationType_ID;
                    // Change registration type
                    registration.RegistrationType_ID = newRegistrationTypeID;

                    // If registration is part of a group, update the group
                    if (registration.GroupLeader_ID != null)
                    {
                        var prevRegos = (from br in context.Events_GroupBulkRegistration
                                         where br.RegistrationType_ID == prevType &&
                                               br.GroupLeader_ID == registration.GroupLeader_ID &&
                                               br.Quantity >= 1
                                         select br).ToList();

                        // Abort if previous regos not found.
                        if (!prevRegos.Any()) throw new Exception("Bulk Registration not found.");

                        var prevRego = prevRegos.First();

                        if (prevRego.RegistrationType_ID != 1521 &&
                            prevRego.RegistrationType_ID != 1522 &&
                            prevRego.RegistrationType_ID != 1523 &&
                            prevRego.RegistrationType_ID != 1524) throw new Exception("Invalid request");

                        // Reduce quantity of previous type
                        if (prevRego.Quantity == 1)
                        {
                            context.Events_GroupBulkRegistration.Remove(prevRego);
                        }
                        else
                        {
                            prevRego.Quantity -= 1;
                        }

                        // Search for regos of the new type
                        var newTypeRegos = (from br in context.Events_GroupBulkRegistration
                                            where br.RegistrationType_ID == newRegistrationTypeID &&
                                                  br.GroupLeader_ID == registration.GroupLeader_ID
                                            select br).ToList();

                        // If none found, create
                        if (!newTypeRegos.Any())
                        {
                            var newBulkRego = new Events_GroupBulkRegistration();
                            newBulkRego.GroupLeader_ID = prevRego.GroupLeader_ID;
                            newBulkRego.Venue_ID = prevRego.Venue_ID;
                            newBulkRego.RegistrationType_ID = newRegistrationTypeID;
                            newBulkRego.Quantity = 1;
                            newBulkRego.DateAdded = prevRego.DateAdded;
                            newBulkRego.Deleted = false;

                            context.Events_GroupBulkRegistration.Add(newBulkRego);
                        }
                        else
                        {
                            newTypeRegos.First().Quantity += 1;
                        }
                    }

                    // Process the database
                    context.SaveChanges();

                    // Prepare the return values

                    // Retrieve logged in contact details
                    var contact = getContactDetails();
                    var returnModel = new EventSummaryModel();
                    returnModel.Group = new EventGroup(contact.id);
                    returnModel.SelectedEvent = new Event(registration.Venue_ID);

                    // Return true
                    return Json(new
                    {
                        isSuccess = true,
                        curRegos = returnModel.CurrentSingleRegistrations,
                        bulkRegos = returnModel.CurrentGroupRegistrations,
                        allocated = returnModel.AllocatedRegistrations,
                        allocatedQty = returnModel.GroupAllocatedCount,
                        unallocated = returnModel.UnallocatedRegistrations,
                        unallocatedQty = returnModel.GroupUnallocatedCount,
                        invites = returnModel.PendingInvitations
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = ex.Message
                });
            }
        }
        #endregion

        [HttpPost]
        [PSAuthorize]
        public ActionResult Unallocate(int registrationID)
        {
            bool unallocateSuccessful = false;

            // Retrieve logged in contact details (group leader)
            var contact = getContactDetails();
            EventSummaryModel returnModel = new EventSummaryModel();

            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Only unallocate if person has not attended
                    var rego = (from r in context.Events_Registration
                                where r.Registration_ID == registrationID
                                select r).FirstOrDefault();
                    if (rego != null && !rego.Attended)
                    {
                        // Retrieve selected event
                        var venue_id = (from r in context.Events_Registration
                                        where r.Registration_ID == registrationID
                                        select r.Venue_ID).FirstOrDefault();

                        var selected_event = new Event(venue_id);

                        // Check for any child comments
                        var child_info = (from rc in context.Events_RegistrationChildInformation
                                          where rc.Registration_ID == registrationID
                                          select rc).FirstOrDefault();

                        // Delete comments if any exists.
                        if (child_info != null)
                        {
                            context.Events_RegistrationChildInformation.Remove(child_info);
                            context.SaveChanges();
                        }

                        // Remove registration
                        context.Events_Registration.Remove(rego);
                        context.SaveChanges();

                        returnModel.SelectedEvent = selected_event;
                        returnModel.Group = new EventGroup(contact.id);

                        unallocateSuccessful = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(new
            {
                isSuccess = unallocateSuccessful,
                curRegos = returnModel.CurrentSingleRegistrations,
                allocated = returnModel.AllocatedRegistrations,
                allocatedKids = returnModel.AllocatedKidsRegistrations,
                allocatedQty = returnModel.GroupAllocatedCount,
                unallocated = returnModel.UnallocatedRegistrations,
                unallocatedQty = returnModel.GroupUnallocatedCount,
                invites = returnModel.PendingInvitations
            });
        }

        #endregion

        #region Account Register
        [AllowAnonymous]
        public ActionResult Register(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            // Populate viewmodel
            // Populate viewmodel
            var model = new RegisterModel
            {
                basicDetails = new BasicDetailsPartialModel(),
                credentialDetails = new CredentialDetailsPartialModel(),
                personalDetails = new PersonalDetailsPartialModel(),
                contactDetails = new ContactDetailsPartialModel(),
                churchDetails = new ChurchDetailsPartialModel(),
                churchInvolvementDetails = new ChurchInvolvementPartialModel()
                {
                    AvailableCInvolvement = Lists.getChurchInvolvement(null)
                },
                emergencyDetails = new EmergencyDetailsPartialModel()
            };

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(RegisterModel model, string returnUrl)
        {
            try
            {
                #region Validation

                // Validate the data
                if (String.IsNullOrWhiteSpace(model.basicDetails.Firstname))
                {
                    throw new Exception("First name is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Lastname))
                {
                    throw new Exception("Last name is required");
                }
                if (String.IsNullOrWhiteSpace(model.basicDetails.Mobile))
                {
                    throw new Exception("Mobile is required");
                }

                #endregion

                //Only continue if view model validation is correct.
                if (ModelState.IsValid)
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // Check if email has been taken
                        var contact = (from cc in context.Common_Contact
                                       where cc.Email == model.credentialDetails.Email.Trim()
                                       select cc).FirstOrDefault();

                        // If email has been taken, return an error message
                        if (contact != null)
                        {
                            throw new Exception(model.credentialDetails.Email + " is registered with another account, please use a different email.");
                        }

                        // create new common_contactExternalEvents
                        //var new_contact_event = new Common_ContactExternalEvents();
                        //new_contact_event.EmergencyContactName = "";
                        //new_contact_event.EmergencyContactPhone = "";
                        //new_contact_event.EmergencyContactRelationship = "";
                        //new_contact_event.MedicalInformation = "";
                        //new_contact_event.MedicalAllergies = "";
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "";


                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.ChurchAddress1 = "";
                        new_external_contact.ChurchAddress2 = "";
                        new_external_contact.ChurchCountry_ID = null;
                        new_external_contact.ChurchEmail = "";
                        new_external_contact.ChurchName = model.basicDetails.Church;
                        new_external_contact.ChurchNumberOfPeople = "";
                        new_external_contact.ChurchPhone = "";
                        new_external_contact.ChurchPostcode = "";
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = "";
                        new_external_contact.ChurchSuburb = "";
                        new_external_contact.ChurchWebsite = "";
                        new_external_contact.Denomination_ID = 49;
                        new_external_contact.SeniorPastorName = "";
                        new_external_contact.LeaderName = "";
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;
                        //new_external_contact.Common_ContactExternalEvents = new_contact_event;

                        // create new contact object
                        var new_contact = new Common_Contact();
                        new_contact.Salutation_ID = 1;
                        new_contact.FirstName = model.basicDetails.Firstname;
                        new_contact.LastName = model.basicDetails.Lastname;
                        new_contact.Gender = "";
                        new_contact.DateOfBirth = model.basicDetails._dob;
                        new_contact.Address1 = "";
                        new_contact.Address2 = "";
                        new_contact.Suburb = "";
                        new_contact.Postcode = model.basicDetails.Postcode;
                        new_contact.State_ID = null;
                        new_contact.StateOther = "";
                        new_contact.Country_ID = model.basicDetails.Country;
                        new_contact.Phone = "";
                        new_contact.PhoneWork = "";
                        new_contact.Fax = "";
                        new_contact.Mobile = model.basicDetails.Mobile;
                        new_contact.Email = model.credentialDetails.Email.Trim();
                        new_contact.Email2 = "";
                        new_contact.DoNotIncludeEmail1InMailingList = false;
                        new_contact.DoNotIncludeEmail2InMailingList = false;
                        new_contact.Family_ID = null;
                        new_contact.FamilyMemberType_ID = null;
                        new_contact.Password = model.credentialDetails.Password; ;
                        new_contact.VolunteerPoliceCheck = "";
                        new_contact.VolunteerPoliceCheckDate = null;
                        new_contact.VolunteerWWCC = "";
                        new_contact.VolunteerWWCCDate = null;
                        new_contact.VolunteerWWCCType_ID = null;
                        new_contact.CreationDate = DateTime.Now;
                        new_contact.CreatedBy_ID = 100;
                        new_contact.ModificationDate = DateTime.Now;
                        new_contact.ModifiedBy_ID = null;
                        new_contact.GUID = Guid.NewGuid();
                        new_contact.AnotherLanguage_ID = null;
                        new_contact.Nationality_ID = null;
                        new_contact.Common_ContactExternal = new_external_contact;


                        // Create new Events and Conference mailing list by post
                        var new_mailing_list_Post = new Common_CombinedContactMailingList();
                        new_mailing_list_Post.Contact_ID = new_contact.Contact_ID;
                        new_mailing_list_Post.MailingList_ID = 1;
                        new_mailing_list_Post.MailingListType_ID = 1;
                        new_mailing_list_Post.SubscriptionActive = true;
                        new_mailing_list_Post.Common_Contact = new_contact;

                        context.Common_CombinedContactMailingList.Add(new_mailing_list_Post);

                        // Create new Events and Conference mailing list by email
                        var new_mailing_list_Email = new Common_CombinedContactMailingList();
                        new_mailing_list_Email.Contact_ID = new_contact.Contact_ID;
                        new_mailing_list_Email.MailingList_ID = 1;
                        new_mailing_list_Email.MailingListType_ID = 2;
                        new_mailing_list_Email.SubscriptionActive = true;
                        new_mailing_list_Email.Common_Contact = new_contact;

                        context.Common_CombinedContactMailingList.Add(new_mailing_list_Email);

                        // Create new Events and Conference mailing list by sms
                        var new_mailing_list_SMS = new Common_CombinedContactMailingList();
                        new_mailing_list_SMS.Contact_ID = new_contact.Contact_ID;
                        new_mailing_list_SMS.MailingList_ID = 1;
                        new_mailing_list_SMS.MailingListType_ID = 3;
                        new_mailing_list_SMS.SubscriptionActive = true;
                        new_mailing_list_SMS.Common_Contact = new_contact;

                        context.Common_CombinedContactMailingList.Add(new_mailing_list_SMS);

                        context.Common_Contact.Add(new_contact);
                        context.SaveChanges();


                        //default insert 2 emergency contact when create 
                        var newEmergencyContact = new Events_EmergencyContact()
                        {
                            Contact_ID = new_contact.Contact_ID,
                            EmergencyContactName = "",
                            EmergencyContactPhone = "",
                            EmergencyContactRelationship = "",
                            Comment = "",
                            LastModifiedDate = DateTime.Now
                        };
                        var newEmergencyContact2 = new Events_EmergencyContact()
                        {
                            Contact_ID = new_contact.Contact_ID,
                            EmergencyContactName = "",
                            EmergencyContactPhone = "",
                            EmergencyContactRelationship = "",
                            Comment = "",
                            LastModifiedDate = DateTime.Now
                        };
                        context.Events_EmergencyContact.Add(newEmergencyContact);
                        context.Events_EmergencyContact.Add(newEmergencyContact2);
                        context.SaveChanges();

                        //send email for creating an account
                        AccountCreationEmailModel emailmodel = new AccountCreationEmailModel()
                        {
                            FirstName = new_contact.FirstName,
                            Email = new_contact.Email
                        };
                        new MailController().SendAccountCreationEmail(emailmodel);


                        // Login and redirect to Summary
                        var loginModel = new LoginModel()
                        {
                            Email = model.credentialDetails.Email,
                            Password = model.credentialDetails.Password
                        };

                        // Redirect if a returnUrl is stated

                        if (returnUrl != null)
                        {
                            return Redirect(returnUrl);
                        }

                        return performLogin(loginModel, "");
                    }


                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
            }
            // do something.
            return View(model);
        }

        #endregion

        #region Helper Functions
        // Retrieve details of currently logged in contact
        public CContact getContactDetails()
        {
            // Retrieve logged in contact from session
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];

            // Extract more details from the db
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var ps_contact = new CContact();
                ps_contact.id = contact.Id;

                // Retrieve personal details (if available)
                var personal = (from cc in context.Common_Contact
                                from ccee in context.Common_ContactExternalEvents
                                      .Where(ce => ce.Contact_ID == cc.Contact_ID)
                                      .DefaultIfEmpty()
                                //join s in context.Common_GeneralState on cc.State_ID equals s.State_ID into subs from s in subs.DefaultIfEmpty()
                                where cc.Contact_ID == contact.Id
                                select new ContactPersonalDetails
                                {
                                    salutation_id = cc.Salutation_ID,
                                    firstname = cc.FirstName,
                                    lastname = cc.LastName,
                                    dob = cc.DateOfBirth,
                                    gender = cc.Gender,
                                    address1 = cc.Address1,
                                    address2 = cc.Address2,
                                    suburb = cc.Suburb,
                                    state_id = cc.State_ID,
                                    //state = s.State,
                                    state_other = cc.StateOther,
                                    postcode = cc.Postcode,
                                    country_id = cc.Country_ID,
                                    mobile = cc.Mobile,
                                    email = cc.Email,
                                    email2 = cc.Email2
                                }).First();

                // Retrieve church details
                var church_details = (from cce in context.Common_ContactExternal
                                      where cce.Contact_ID == ps_contact.id
                                      select new ContactChurchDetails
                                      {
                                          name = cce.ChurchName,
                                          denomination_id = cce.Denomination_ID,
                                          senior_pastor = cce.SeniorPastorName,
                                          leader = cce.LeaderName,
                                          congregation_size = cce.ChurchNumberOfPeople,
                                          address1 = cce.ChurchAddress1,
                                          address2 = cce.ChurchAddress2,
                                          suburb = cce.ChurchSuburb,
                                          state = cce.ChurchStateOther,
                                          postcode = cce.ChurchPostcode,
                                          country_id = cce.ChurchCountry_ID,
                                          phone = cce.ChurchPhone,
                                          website = cce.ChurchWebsite,
                                          email = cce.ChurchEmail
                                      }).FirstOrDefault();


                // Retrieve emergency details
                var emergency_details = (from ccee in context.Common_ContactExternalEvents
                                         where ccee.Contact_ID == ps_contact.id
                                         select new ContactEmergencyDetails
                                         {
                                             name = ccee.EmergencyContactName,
                                             phone = ccee.EmergencyContactPhone,
                                             relationship = ccee.EmergencyContactRelationship,
                                             medical_info = ccee.MedicalInformation,
                                             medical_allergies = ccee.MedicalAllergies
                                         }).FirstOrDefault();

                ps_contact.personal = personal;
                ps_contact.church = church_details;
                ps_contact.emergency = emergency_details;
                return ps_contact;
            }
        }
        public List<EmergencyContact> GetEmergencyContacts(int Contact_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from e in context.Events_EmergencyContact
                            where e.Contact_ID == Contact_ID
                            select new EmergencyContact
                            {
                                FullName = e.EmergencyContactName,
                                Mobile = e.EmergencyContactPhone,
                                Relationship = e.EmergencyContactRelationship,
                                EmergencyContact_ID = e.EmergencyContact_ID
                            }).ToList();
                while(list.Count < 2)
                {
                    list.Add(new EmergencyContact());
                }
                return list;
            }
        }

        #endregion

        #region Encryption
        private static string Encrypt(String plainStr, byte[] key, byte[] ivKey, int keySize)
        {
            AesCryptoServiceProvider aesEncryption = new AesCryptoServiceProvider();
            aesEncryption.IV = ivKey;
            aesEncryption.Key = key;
            byte[] encrypted;
            ICryptoTransform crypto = aesEncryption.CreateEncryptor(aesEncryption.Key, aesEncryption.IV);
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, crypto, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {

                        //Write all data to the stream.
                        swEncrypt.Write(plainStr);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
            return Convert.ToBase64String(encrypted);
        }

        /* decryption method to decript the part of the link that was given to the user n password reset action
         * 
         */

        private static string Decrypt(String encryptedText, byte[] key, byte[] ivKey, int keySize)
        {
            encryptedText = encryptedText.Replace(" ", "+");
            AesCryptoServiceProvider aesEncryption = new AesCryptoServiceProvider();
            aesEncryption.IV = ivKey;
            aesEncryption.Key = key;
            String plaintext = null;
            ICryptoTransform decrypto = aesEncryption.CreateDecryptor(aesEncryption.Key, aesEncryption.IV);
            byte[] encryptedByte = Convert.FromBase64String(encryptedText);
            using (MemoryStream msDecrypt = new MemoryStream(encryptedByte))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decrypto, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {

                        // Read the decrypted bytes from the decrypting stream 
                        // and place them in a string.
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            return plaintext;
        }

        private static string HashBuilder(String s)
        {
            using (MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider())
            {
                byte[] b = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(s));
                StringBuilder strBuild = new StringBuilder();
                for (int i = 0; i < b.Length; i++)
                {
                    strBuild.Append(b[i].ToString("x2"));
                }
                return strBuild.ToString();
            }
        }

        #endregion

        [HttpPost]
        public ActionResult verifyCaptcha(string token)
        {
            const string url = "https://www.google.com/recaptcha/api/siteverify";

            string urlToPost = $"{url}?secret={_recaptchaSecretKey}&response={token}";

            var response = new reCaptchaResponse();
            //var responseString = "";

            using (var httpClient = new HttpClient())
            {
                try
                {
                    response = JsonConvert.DeserializeObject<reCaptchaResponse>(httpClient.GetStringAsync(urlToPost).Result);
                    //responseString = httpClient.GetStringAsync(urlToPost).Result;
                }
                catch
                {
                    //TODO: Error handling
                }
            }

            return Json(response);
        }
    }

    public class reCaptchaResponse
    {
        public bool success { get; set; }
        public string challenge_ts { get; set; }
        public decimal score { get; set; }
        public string hostname { get; set; }
        public string action { get; set; }
        //public List<string> error_codes { get; set; }

    }
}
