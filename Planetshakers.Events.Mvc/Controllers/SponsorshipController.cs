﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Events.Mvc.Models;

namespace Planetshakers.Events.Mvc.Controllers
{
    public class SponsorshipController : Controller
    {
        [AllowAnonymous]
        public ActionResult Donation(int Event_ID, string GUID)
        {
            SponsorshipViewModel model = new SponsorshipViewModel();
            model.getContact_ID(GUID);
            model.Event_ID = Event_ID;
            model.getDetails();
            return View(model);
        }

    }
}