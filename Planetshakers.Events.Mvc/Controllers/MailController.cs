﻿using ActionMailer.Net.Mvc;
using Planetshakers.Events.Mvc.Models;
using Newtonsoft.Json;
using SelectPdf;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Web.Configuration;
using System.Threading.Tasks;
using Planetshakers.Events.Mvc.Helpers;

namespace Planetshakers.Events.Mvc.Controllers
{
    public class MailController : MailerBase
    {
        JsonSerializerSettings _serializerSettings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.None,
            TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
        };

        private static readonly string _psApiKey = AzureServices.KeyVault_RetreiveSecret("believe-apps", "PsApiKey") ?? "";
        private static readonly string _tokenSalt = AzureServices.KeyVault_RetreiveSecret("believe-apps", "TokenSalt") ?? "";

        public void SponsorshipConfirmation(SponsorshipConfirmationEmailModel model)
        {
            try
            {
                model.FullName = getContactFullName(model.ContactId);
                string templateID = ConfigurationManager.AppSettings["SendGridTemplateSponsorshipConfirmation"];
                _ = SendConferenceMailViaPsApi(model.Email, templateID, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Replace registration confirmation email with registration receipt email
        public void RegistrationReceiptEmail(RegistrationReceipt dynamicTemplateData)
        {
            try
            {
                var con = ConfigurationManager.ConnectionStrings["PlanetshakersEntities"].ConnectionString;
                string templateID = ConfigurationManager.AppSettings["SendGridTemplateReceiptEmail"];

                if (con.Contains("PlanetshakersSG")) { 
                    templateID = ConfigurationManager.AppSettings["SendGridBoomTemplateTaxInvoiceSG"];
                }

                _ = SendConferenceMailViaPsApi(dynamicTemplateData.Email, templateID, dynamicTemplateData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Using dynamic template in substitute of ConfirmationBarcode function
        public void ConfirmationBarcodeEmail(ConfirmationBarcode dynamicTemplateData)
        {
            try
            {
                var con = ConfigurationManager.ConnectionStrings["PlanetshakersEntities"].ConnectionString;
                //string templateID = ConfigurationManager.AppSettings["SendGridTemplateRegistrationConfirmation2"];
                string templateID = "";
                dynamicTemplateData.ContactDetails = "Email us at info@believeglobal.org.";
                
                _ = SendConferenceMailViaPsApi(dynamicTemplateData.Email, templateID, dynamicTemplateData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmailResult PasswordReset(PasswordResetEmailModel model)
        {
            From = "info@believeglobal.org";
            To.Add(model.Email);
            Subject = "Password Recovery";

            return Email("PasswordReset", model);
        }

        public EmailResult DevErrorNotification(DevErrorNotificationEmailModel model)
        {
            From = "admin@planetshakers.com";
            To.Add("itteam@planetshakers.com");
            Subject = "Register Website :: " + model.ex.Message;

            return Email("DevErrorNotification", model);
        }

        public void GroupInvitation(GroupInvitationEmailModel model)
        {
            try
            {
                string templateID = ConfigurationManager.AppSettings["SendGridTemplateInvitationwChildID"];

                model.EventName = model.Event.name;
                model.EventVenue = model.Event.location;
                model.EventDate = model.Event.date;

                _ = SendConferenceMailViaPsApi(model.Email, templateID, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InvitationAcceptNotification(GroupInvitationNotificationEmailModel model)
        {
            try
            {
                string templateID = ConfigurationManager.AppSettings["SendGridTemplateAcceptNotification"];
                model.EventName = model.Event.name;

                _ = SendConferenceMailViaPsApi(model.GroupLeaderEmail, templateID, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InvitationDeclineNotification(GroupInvitationNotificationEmailModel model)
        {
            try
            {
                string templateID = ConfigurationManager.AppSettings["SendGridTemplateDeclineNotification"];
                model.EventName = model.Event.name;

                _ = SendConferenceMailViaPsApi(model.GroupLeaderEmail, templateID, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SendMailViaPsApi(string RecipientEmail, string TemplateId, object dynamicTemplateData)
        {
            objMailApi objMailApi = new objMailApi()
            {
                AuthKey = hashHMACSHA256(_psApiKey),
                DynamicTemplateData = dynamicTemplateData,
                RecipientEmail = RecipientEmail,
                TemplateId = TemplateId,
                SenderEmail = "info@believeglobal.org",
                SenderEmailName = "Believe Global"
            };

            string PostData = JsonConvert.SerializeObject(objMailApi, _serializerSettings);
            var URL = "https://api.planetshakers.com/API/ApiMail/SendMail";
            string ServerResponse = await getJSON(URL, PostData);

            if (ServerResponse == "ERROR. Signature Mismatch.")
            {
                throw new UnauthorizedAccessException(ServerResponse);
            }
            else if (ServerResponse != "\"ok\"")
            {
                throw new HttpException(ServerResponse);
            }
        }

        public async Task SendMailViaPsApi(string RecipientEmail, string TemplateId, object dynamicTemplateData, string attachment, string attachmentFileName)
        { 
            objMailApi objMailApi = new objMailApi()
            {
                AuthKey = hashHMACSHA256(_psApiKey),
                DynamicTemplateData = dynamicTemplateData,
                RecipientEmail = RecipientEmail,
                TemplateId = TemplateId,
                SenderEmail = "info@believeglobal.org",
                SenderEmailName = "Believe Global",
                Attachment = attachment,
                AttachmentFileName = attachmentFileName
            };

            string PostData = JsonConvert.SerializeObject(objMailApi, _serializerSettings);
            var URL = "https://api.planetshakers.com/API/ApiMail/SendMail";
            string ServerResponse = await getJSON(URL, PostData);

            if (ServerResponse == "ERROR. Signature Mismatch.")
            {
                throw new UnauthorizedAccessException(ServerResponse);
            }
            else if (ServerResponse != "\"ok\"")
            {
                throw new HttpException(ServerResponse);
            }
        }

        public async Task SendConferenceMailViaPsApi(string RecipientEmail, string TemplateId, object dynamicTemplateData)
        {
            objMailApi objMailApi = new objMailApi()
            {
                AuthKey = hashHMACSHA256(_psApiKey),
                DynamicTemplateData = dynamicTemplateData,
                RecipientEmail = RecipientEmail,
                TemplateId = TemplateId,
                SenderEmail = "conference@planetshakers.com",
                SenderEmailName = "Planetshakers"
            };

            string PostData = JsonConvert.SerializeObject(objMailApi, _serializerSettings);
            var URL = "https://api.planetshakers.com/API/ApiMail/SendMail";
            string ServerResponse = await getJSON(URL, PostData);

            if (ServerResponse == "ERROR. Signature Mismatch.")
            {
                throw new UnauthorizedAccessException(ServerResponse);
            }
            else if (ServerResponse != "\"ok\"")
            {
                throw new HttpException(ServerResponse);
            }
        }
         
        public void SendSGTaxInvoice(RegistrationReceipt model)
        {
            try
            {
                SGRegistrationReceipt dynamicTemplateData = new SGRegistrationReceipt
                {
                    FullName = model.FullName,
                    ReferenceNumber = model.Items.First().reference_number.ToString(),
                    RegistrationType = model.Items.First().reg_name,
                    Quantity = model.Items.First().reg_count,
                    Price = "SGD " + model.Items.First().price,
                    Total = "SGD " + model.GrandTotal
                };

                _ = SendConferenceMailViaPsApi(model.Email, "d-a135de382ab74c08b28736df7424be18", dynamicTemplateData);

            } catch (Exception ex)
            {
                throw ex;
            }
        }        

        public void SendTicket(TicketModel model)
        {
            try
            {
                HtmlToPdf converter = new HtmlToPdf();
                PdfDocument doc = new PdfDocument();

                var html = getEmailBody();

                foreach (var ticket in model.Tickets)
                {
                    string html_var = html.Replace("[rego_barcode]", ticket.reg_barcode.ToString());

                    PdfDocument page = converter.ConvertHtmlString(html_var);

                    doc.Append(page);
                }

                var docBytes = Convert.ToBase64String(doc.Save());
                var fileName = "PSSG21_ticket.pdf";

                SGConcert2020 dynamicTemplateData = new SGConcert2020 { FirstName = model.FirstName };

                _ = SendMailViaPsApi(model.Email, "d-4bdbc2d5a470456f930efa312dc5e753", dynamicTemplateData, docBytes, fileName);

                doc.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendDonatedConfirmationEmail(DonatedEmailModel dynamicTemplateData)
        {
            try
            {
                //_ = SendMailViaPsApi(dynamicTemplateData.Email, "d-162fbd0f3a1540d298272a41afa08844", dynamicTemplateData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendCreditedConfirmationEmail(CreditedEmailModel dynamicTemplateData)
        {
            try
            {
                //_ = SendMailViaPsApi(dynamicTemplateData.Email, "d-da23646a053f4791856bcf4ad2106366", dynamicTemplateData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //for paying deposit / EOI
        public void SendDepositRecievedEmail(DepositReceivedEmailModel dynamicTemplateData)
        {
            try
            {
                _ = SendMailViaPsApi(dynamicTemplateData.Email, "d-d988372451704871aecb5b6f65ce9f22", dynamicTemplateData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        //for submit application
        public void SendApplicationRecievedEmail(ApplicationReceivedEmailModel dynamicTemplateData)
        {
            try
            {
                _ = SendMailViaPsApi(dynamicTemplateData.Email, "d-15d59a4af2c147dbabe2a9a4f4676b7e", dynamicTemplateData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //for creating an account
        public void SendAccountCreationEmail(AccountCreationEmailModel dynamicTemplateData)
        {
            try
            {
                _ = SendMailViaPsApi(dynamicTemplateData.Email, "d-ded2867786254b49bd93f4cf7a53e2c9", dynamicTemplateData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        private string getEmailBody()
        {
            string html = string.Empty;
            string path = Path.Combine(HttpRuntime.AppDomainAppPath, "Views/Mail/Ticket.html.cshtml");

            using (StreamReader reader = new StreamReader(path))
            {
                html = reader.ReadToEnd();
            }

            return html;
        }

        #region helpers
        private string getContactEmail(int contact_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                return (from cc in context.Common_Contact
                        where cc.Contact_ID == contact_id
                        select cc.Email).First();
            }
        }

        private string getContactFullName(int contact_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                return (from cc in context.Common_Contact
                        where cc.Contact_ID == contact_id
                        select cc.FirstName + " " + cc.LastName).First();
            }
        }

        private string hashHMACSHA256(string message)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();

            byte[] byteSalt = encoding.GetBytes(_tokenSalt);
            byte[] byteMessage = encoding.GetBytes(message);

            HMACSHA256 hmacSHA256 = new HMACSHA256(byteSalt);
            byte[] hashMessage = hmacSHA256.ComputeHash(byteMessage);

            return BitConverter.ToString(hashMessage).Replace("-", "");
        }

        public async System.Threading.Tasks.Task<string> getJSON(string URL, string jsondata)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            System.Net.WebRequest json = System.Net.WebRequest.Create(URL);
            byte[] postBytes = Encoding.UTF8.GetBytes(jsondata);
            json.Method = "POST";
            json.ContentType = "application/json";
            json.ContentLength = postBytes.Length;

            System.IO.Stream requestStream = json.GetRequestStream();
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            System.Net.WebResponse Response = await json.GetResponseAsync();
            System.IO.Stream stream = Response.GetResponseStream();
            System.IO.StreamReader Reader = new System.IO.StreamReader(stream);
            string ServerResponse = "";

            using (Response)
            using (stream)
                ServerResponse = Reader.ReadToEnd();

            return ServerResponse;
        }

        private static string RenderViewToString(ControllerContext context,
                                    string viewPath,
                                    object model = null,
                                    bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }

        #endregion
    }

    public class RegistrationConfirmationItem
    {
        // reference number
        public string reference_number { get; set; }

        public int reg_type_id { get; set; }

        public int reg_count { get; set; }

        // registration name
        public string reg_name
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    return (from rt in context.Events_RegistrationType
                            where rt.RegistrationType_ID == reg_type_id
                            select rt.RegistrationType ?? "Outstanding Balance").FirstOrDefault();
                }
            }
        }

        public int qty { get; set; }

        // single price
        private decimal _price;
        public decimal price
        {
            get
            {
                return Math.Round(_price, 2);
            }
            set
            {
                this._price = value;
            }
        }
    }

    #region PS API Classes
    public class objApi
    {
        public string AuthKey { get; set; }
    }

    public class objMailApi : objApi
    {
        public string RecipientEmail { get; set; }
        public string TemplateId { get; set; }
        public object DynamicTemplateData { get; set; }
        public string SenderEmail { get; set; }
        public string SenderEmailName { get; set; }
        public string Attachment { get; set; }
        public string AttachmentFileName { get; set; }
    }

    #endregion

}
