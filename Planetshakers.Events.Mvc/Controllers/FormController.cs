﻿using Planetshakers.Events.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Mvc.Controllers
{
    public class FormController : Controller
    {
        public ActionResult ChildEnrolment(int rego_id)
        {
            var model = new ChildEnrolmentModel(rego_id);

            return View(model);
        }

        public ActionResult SubmitChildEnrolment(ChildEnrolmentModel model, FormCollection form)
        {
            model.setChildEnrolmentQuestion(model.childEnrolment.ChildEnrolment_ID);

            // Get current user 
            var user = Session["Contact"] as Authenticator.Contact;

            model.childEnrolment.SignedBy = user.Id;
            model.childEnrolment.DateSigned = DateTime.Now;

            model.updateChildDetails(model.childEnrolment);
            model.updateChildEnrolment(model.childEnrolment);

            // Get answer for each question and update on the database
            foreach (var q in model.childEnrolmentQuestions)
            {
                var answer = form["question_" + q.question_ID.ToString()];

                if (answer != null)
                {
                    model.updateChildEnrolmentAnswer(model.childEnrolment.ChildEnrolment_ID, q.question_ID, (int)q.answer_ID, answer);
                }
            }

            ViewBag.venueID = model.venue_ID;

            return View("Success");
        }

        public ActionResult MedicalForm(int rego_id)
        {
            var model = new MedicalReq();

            using (var entity = new PlanetshakersEntities())
            {
                var medInfo = (from er in entity.Events_Registration
                               join cee in entity.Common_ContactExternalEvents
                               on er.Contact_ID equals cee.Contact_ID
                               where er.Registration_ID == rego_id
                               select cee).FirstOrDefault();

                if (medInfo != null)
                {
                    model.contact_id = medInfo.Contact_ID;
                    model.emergency_mobile = medInfo.EmergencyContactPhone;
                    model.emergency_name = medInfo.EmergencyContactName;
                    model.emergency_rel = medInfo.EmergencyContactRelationship;
                    model.allergies = medInfo.MedicalAllergies;
                    model.info = medInfo.MedicalInformation;
                }
                else
                {

                    var contact = (from cc in entity.Common_Contact
                                   where cc.Contact_ID == medInfo.Contact_ID
                                   select cc).FirstOrDefault();

                    var external = (from ce in entity.Common_ContactExternal
                                    where ce.Contact_ID == medInfo.Contact_ID
                                    select ce).FirstOrDefault();

                    var new_contact_event = new Common_ContactExternalEvents();

                    if (external != null)
                    {
                        // create new common_contactExternalEvents
                        //new_contact_event.EmergencyContactName = "";
                        //new_contact_event.EmergencyContactPhone = "";
                        //new_contact_event.EmergencyContactRelationship = "";
                        //new_contact_event.MedicalInformation = "";
                        //new_contact_event.MedicalAllergies = "";
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "";

                        //entity.Common_ContactExternalEvents.Add(new_contact_event);
                        //entity.SaveChanges();
                    }
                    else
                    {
                        // create new common_contactExternalEvents
                        //new_contact_event.EmergencyContactName = "";
                        //new_contact_event.EmergencyContactPhone = "";
                        //new_contact_event.EmergencyContactRelationship = "";
                        //new_contact_event.MedicalInformation = "";
                        //new_contact_event.MedicalAllergies = "";
                        //new_contact_event.AccessibilityInformation = "";
                        //new_contact_event.EventComments = "";

                        // create new external contact
                        var new_external_contact = new Common_ContactExternal();
                        new_external_contact.ChurchAddress1 = "";
                        new_external_contact.ChurchAddress2 = "";
                        new_external_contact.ChurchCountry_ID = null;
                        new_external_contact.ChurchEmail = "";
                        new_external_contact.ChurchName = "";
                        new_external_contact.ChurchNumberOfPeople = "";
                        new_external_contact.ChurchPhone = "";
                        new_external_contact.ChurchPostcode = "";
                        new_external_contact.ChurchState_ID = null;
                        new_external_contact.ChurchStateOther = "";
                        new_external_contact.ChurchSuburb = "";
                        new_external_contact.ChurchWebsite = "";
                        new_external_contact.Denomination_ID = 49;
                        new_external_contact.SeniorPastorName = "";
                        new_external_contact.LeaderName = "";
                        new_external_contact.Deleted = false;
                        new_external_contact.Inactive = false;
                        //new_external_contact.Common_ContactExternalEvents = new_contact_event;

                        entity.Common_ContactExternal.Add(new_external_contact);

                        contact.Common_ContactExternal = new_external_contact;

                        entity.SaveChanges();
                    }

                    model.contact_id = medInfo.Contact_ID;
                    model.emergency_mobile = new_contact_event.EmergencyContactPhone;
                    model.emergency_name = new_contact_event.EmergencyContactName;
                    model.emergency_rel = new_contact_event.EmergencyContactRelationship;
                    model.allergies = new_contact_event.MedicalAllergies;
                    model.info = new_contact_event.MedicalInformation;
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult SubmitMedicalForm(int contact_id, string name, string mobile, string rel, string info, string allergies)
        {
            bool updated = false;

            using (var entity = new PlanetshakersEntities())
            {
                try
                {
                    var person = (from cc in entity.Common_ContactExternalEvents
                                  where cc.Contact_ID == contact_id
                                  select cc).FirstOrDefault();

                    person.EmergencyContactName = name;
                    person.EmergencyContactPhone = mobile;
                    person.EmergencyContactRelationship = rel;
                    person.MedicalInformation = info;
                    person.MedicalAllergies = allergies;
                    person.LastModifiedDate = DateTime.Now;

                    entity.SaveChanges();
                    updated = true;
                }
                catch (Exception ex)
                {
                    updated = false;
                    throw ex;
                }
            }

            return Json(new { updated }, JsonRequestBehavior.AllowGet);
        }

        #region Private Functions
        private Events_RegistrationChildEnrolment getChildEnrolment(int ce_id)
        {
            var child_enrolment = new Events_RegistrationChildEnrolment();

            using (var entity = new PlanetshakersEntities())
            {
                child_enrolment = (from ce in entity.Events_RegistrationChildEnrolment
                                   where ce.ChildEnrolment_ID == ce_id
                                   select ce).FirstOrDefault();
            }

            return child_enrolment;
        }
        #endregion
    }
}