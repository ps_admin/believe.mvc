﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Events.Mvc.Models;
using Planetshakers.Events.Mvc.Filters;

namespace Planetshakers.Events.Mvc.Controllers
{
    [PSAuthorize]
    public class TripDetailsController : Controller
    {
        public ActionResult TripDetails(int id)
        {
            TripDetailViewModel model = new TripDetailViewModel();
            //get event id from venue id
            int Event_ID = getEvent_ID(id);
            model.populateViewModel(getContactID(), Event_ID);
            model.DocumentUploads = new DocumentUploadViewModel()
            {
                Contact_ID = getContactID(),
                Venue_ID = id
            };
            return View(model);
        }
        public int getContactID()
        {
            var contact = (Planetshakers.Events.Mvc.Authenticator.Contact)Session["Contact"];
            if (contact != null)
            {
                return contact.Id;
            }
            else
            {
                return 0;
            }
        }

        public int getEvent_ID(int Venue_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var event_ID = (from v in context.Events_Venue
                                where v.Venue_ID == Venue_ID
                                select v.Conference_ID).FirstOrDefault();
                return event_ID;
            }
        }

        [HttpPost]
        public ActionResult updateContactProfile(int Event_ID, int Contact_ID, int ContactProfile_ID, string AboutMyself, string TripDetails)
        {
            TripDetailModel model = new TripDetailModel()
            {
                Event_ID  = Event_ID,
                ContactProfile_ID = ContactProfile_ID,
                Contact_ID = Contact_ID,
                AboutMyself = AboutMyself,
                TripDetails = TripDetails
            };
            var success = model.updateContactProfile();
            return Json(new { success = success});
        }

        [HttpPost]
        public ActionResult UploadDp()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var Event_ID = Convert.ToInt32(Request.Form["Event_ID"]);
                var Contact_ID = Convert.ToInt32(Request.Form["Contact_ID"]);
                var file = Request.Files["picFile"];
                var contentType = file.ContentType.Split('/').ToList();

                var record = (from c in context.Events_ContactProfile
                              where c.Contact_ID == Contact_ID && c.Event_ID == Event_ID
                              select c).FirstOrDefault();

                if(record == null)
                {
                    var newProfile = new Events_ContactProfile()
                    {
                        Contact_ID = Contact_ID,
                        Event_ID = Event_ID
                    };
                    var fileName = SaveFile(Contact_ID, Event_ID);
                    newProfile.PictureURL = fileName;
                    context.Events_ContactProfile.Add(newProfile);
                } else
                {
                    DeleteExistingFile(record.PictureURL, Contact_ID);
                    var fileName = SaveFile(Contact_ID, Event_ID);
                    record.PictureURL = fileName;
                }

                try
                {
                    context.SaveChanges();
                } catch
                {
                    return Json(new { success = false });
                }

                return Json(new { FileName = record.PictureURL, ContactProfile_ID = record.ContactProfile_ID, success = true });
            }
        }

        public string SaveFile(int Contact_ID, int Event_ID)
        {
            FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
            var file = Request.Files["picFile"];
            var fileName = "";


            if (file != null)
            {
                if (file.ContentLength != 0)
                {
                    var stream = file.InputStream;

                    //var item = fileModel.GetFile("ProofUploads", submittedContact + ".*");

                    // Do something with the stream.
                    var contentType = file.ContentType.Split('/').ToList();
                    var extension = contentType[contentType.Count - 1].ToString();
                    extension = (extension == "plain") ? "txt" : extension;
                    fileName = Event_ID.ToString() + "_DisplayPicture." + extension;
                    fileModel.UploadFile(Contact_ID.ToString(), fileName, stream);
                }
            }
            return fileName;

        }


        public void DeleteExistingFile(string existingFileName, int Contact_ID)
        {
            // create new model
            FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
            fileModel.DeleteFile(Contact_ID + "/" + existingFileName);
        }

        [HttpPost]
        public string getPicURL(int Contact_ID, int Event_ID, string FileName)
        {
            FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
            var photoModel = fileModel.GetFile(Contact_ID.ToString(), FileName);
            return photoModel.DownloadUrl;

        }

        public FileResult AustralianChristianChurchesChildProtectionPolicy()
        {
            string ReportURL = Path.Combine(HttpRuntime.AppDomainAppPath, "Views/TripDetails/AustralianChristianChurchesChildProtectionPolicy.pdf");
            byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
            return File(FileBytes, "application/pdf");
        }
    }
}