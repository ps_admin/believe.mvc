﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Mvc.Models
{
    public class ConfirmationModel
    {
        public bool isAddOn { get; set; }
    }

    public class MedicalReq
    {
        public int contact_id { get; set; }
        public string emergency_name { get; set; }
        public string emergency_mobile { get; set; }
        public string emergency_rel { get; set; }
        public string info { get; set; }
        public string allergies { get; set; }

    }
}