﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Planetshakers.Events.Mvc.Controllers;


namespace Planetshakers.Events.Mvc.Models
{
    public class EventIndexModel
    {
        public List<Event> Events { get; set; }
        public List<Registration> Barcodes { get; set; }
        public bool actionButton { get; set; }
        public bool getActionButton(int contact_id)
        {

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var rt_ids = (from ert in entity.Events_RegistrationType
                              where ert.RegistrationType_ID <= 3026 && ert.Conference_ID == 128
                              select ert.RegistrationType_ID).ToList();

                var eventList = (from er in entity.Events_Registration
                                 where rt_ids.Contains(er.RegistrationType_ID) && er.Contact_ID == contact_id && er.Venue_ID == 156 && er.GroupLeader_ID == null
                                 select er).Any();

                var bulkList = (from br in entity.Events_GroupBulkRegistration
                                where rt_ids.Contains(br.RegistrationType_ID) && br.GroupLeader_ID == contact_id && br.Venue_ID == 156
                                select br).Any();


                if ((eventList || bulkList) && contact_id != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    public class EventBarcodeModel
    {
        public List<Registration> Barcodes { get; set; }
    }

    public class Allocations
    {
        public int Contact_ID {get; set;}
        public string FullName { get; set; }
        public string Email { get; set; }
        public string GUID { get; set; }
    }

    public class EventSummaryModel
    {
        public Event SelectedEvent { get; set; }
        public int Contact_ID { get; set; }
        public EventGroup Group { get; set; }
        public string Barcode { get; set; }        
        public bool HasGroupRegistrations
        {
            get
            {
                if (SelectedEvent == null || Group == null) return false;

                var unallocated = Group.GetUnallocatedQty(SelectedEvent.venueID);
                var allocated = Group.GetAllocatedQty(SelectedEvent.venueID);

                if (unallocated <= 0 && allocated <= 0)
                    return false;
                else
                    return true;
            }
        }

        private List<Allocations> _PreviousAllocations;
        public SelectList PreviousAllocations
        {
            get
            {
                if (SelectedEvent == null || Group == null) return null;

                if (_PreviousAllocations == null)
                {
                    _PreviousAllocations = new List<Allocations>();

                    // Need a way to get contact id of the current user
                    int user_ID = Group.groupLeaderID;
                    
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        /*var registered = (from er in context.Events_Registration
                                         where er.Venue_ID == SelectedEvent.venueID
                                      select er.Contact_ID).ToList();*/

                        //_PreviousAllocations = new List<Allocations>();
                        var list = (from egc in context.Events_GroupContact
                                                join cc in context.Common_Contact
                                                    on egc.Contact_ID equals cc.Contact_ID
                                                where egc.GroupLeader_ID == user_ID
                                                    && cc.Contact_ID != user_ID                                              
                                                select new Allocations 
                                                { 
                                                    Contact_ID = cc.Contact_ID,
                                                    FullName = cc.FirstName + " " + cc.LastName,
                                                    Email = cc.Email                                                   
                                                }).Distinct().ToList();

                        /*foreach (var x in list)
                        {
                            if (!registered.Contains(x.Contact_ID))
                            {
                                _PreviousAllocations.Add(x);
                            }
                        }*/

                        _PreviousAllocations = list.OrderBy(x => x.FullName).ToList();
                    }
                }

                return new SelectList(_PreviousAllocations,"Contact_ID","FullName");
            }
        }
        public int SelectedAllocation { get; set; }

        private List<RegistrationGroupAllocated> _AllocatedRegistrations;
        public List<RegistrationGroupAllocated> AllocatedRegistrations
        {
            get
            {
                if (SelectedEvent == null || Group == null) return null;

                if (_AllocatedRegistrations == null)
                {
                    _AllocatedRegistrations = new List<RegistrationGroupAllocated>();
                    foreach (var rego in Group.GetAllocatedRegistrations(SelectedEvent.venueID))
                    {
                        using (PlanetshakersEntities context = new PlanetshakersEntities())
                        {
                            _AllocatedRegistrations.Add(
                                (from er in context.Events_Registration
                                 join cc in context.Common_Contact
                                         on er.Contact_ID equals cc.Contact_ID
                                 join rt in context.Events_RegistrationType
                                         on er.RegistrationType_ID equals rt.RegistrationType_ID
                                 where er.Registration_ID == rego.registrationID
                                 select new RegistrationGroupAllocated{
                                     registrationID = er.Registration_ID,
                                     fullName = cc.FirstName + " " +cc.LastName,
                                     registrationTypeID = rt.RegistrationType_ID,
                                     registrationType = rt.RegistrationType,
                                     registrationDate = er.RegistrationDate,
                                     attended = er.Attended
                                 }).First());
                        }
                    }
                }
                return _AllocatedRegistrations;
            }
        }
        public int GroupAllocatedCount
        {
            get
            {
                if (SelectedEvent == null || Group == null) return 0;
                return Group.GetAllocatedQty(SelectedEvent.venueID);
            }
        }
        public bool MedicalDetailsUpdated 
        {
            get 
            {
                if (SelectedEvent == null || Group == null) return false;

                int user_ID = Group.groupLeaderID;

                using (var ctx = new PlanetshakersEntities())
                {
                    var regos = (from er in ctx.Events_Registration
                                 join ert in ctx.Events_RegistrationType
                                 on er.RegistrationType_ID equals ert.RegistrationType_ID
                                 join ee in ctx.Common_ContactExternalEvents
                                 on er.Contact_ID equals ee.Contact_ID into ree
                                 from subee in ree.DefaultIfEmpty()
                                 where er.Venue_ID == SelectedEvent.venueID 
                                 && ert.ApplicationRegistrationType 
                                 && er.GroupLeader_ID == user_ID
                                 select new { er = er, ee = subee }).ToList();

                    var year = DateTime.Today.AddDays(-DateTime.Today.DayOfYear+1);

                    var updated = !regos.Where(r => r.ee.LastModifiedDate == null || r.ee.LastModifiedDate <= year).Any();

                    return updated;
                }
            }
        }

        private List<RegistrationGroupAllocated> _AllocatedYouthRegistrations;
        public List<RegistrationGroupAllocated> AllocatedYouthRegistrations
        {
            get
            {
                if (SelectedEvent == null || Group == null) return null;

                if (_AllocatedYouthRegistrations == null)
                {
                    _AllocatedYouthRegistrations = new List<RegistrationGroupAllocated>();
                    foreach (var rego in Group.GetAllocatedRegistrations(SelectedEvent.venueID))
                    {
                        using (PlanetshakersEntities context = new PlanetshakersEntities())
                        {
                            var youth_rego = (from er in context.Events_Registration
                                                join cc in context.Common_Contact
                                                        on er.Contact_ID equals cc.Contact_ID
                                                join rt in context.Events_RegistrationType
                                                        on er.RegistrationType_ID equals rt.RegistrationType_ID
                                                where er.Registration_ID == rego.registrationID && rt.ApplicationRegistrationType
                                                select new RegistrationGroupAllocated
                                                {
                                                    registrationID = er.Registration_ID,
                                                    contactID = cc.Contact_ID,
                                                    fullName = cc.FirstName + " " + cc.LastName,
                                                    registrationTypeID = rt.RegistrationType_ID,
                                                    registrationType = rt.RegistrationType,
                                                    registrationDate = er.RegistrationDate,
                                                    attended = er.Attended
                                                }).FirstOrDefault();

                            if (youth_rego != null)
                            {
                                var check_external = (from ee in context.Common_ContactExternalEvents
                                                      where ee.Contact_ID == youth_rego.contactID
                                                      select ee).Any();

                                if (!check_external)
                                    createExternalEventsContact(youth_rego.contactID);

                                var this_year = DateTime.Today.AddDays(-DateTime.Today.DayOfYear + 1);

                                youth_rego.medicalDetailsUpdated = (from ee in context.Common_ContactExternalEvents
                                                                    where ee.Contact_ID == youth_rego.contactID
                                                                    select ee.LastModifiedDate).Where(d => d != null && d > this_year).Any();
                            }

                            if (youth_rego != null)
                                _AllocatedYouthRegistrations.Add(youth_rego);
                        }
                    }
                }

                return _AllocatedYouthRegistrations;
            }
        }

        private List<RegistrationGroupAllocated> _AllocatedKidsRegistrations;
        public List<RegistrationGroupAllocated> AllocatedKidsRegistrations
        {
            get
            {
                if (SelectedEvent == null || Group == null) return null;

                if (_AllocatedKidsRegistrations == null)
                {
                    _AllocatedKidsRegistrations = new List<RegistrationGroupAllocated>();
                    foreach (var rego in Group.GetAllocatedRegistrations(SelectedEvent.venueID))
                    {
                        using (PlanetshakersEntities context = new PlanetshakersEntities())
                        {
                            var kid_rego = (from er in context.Events_Registration
                                             join cc in context.Common_Contact
                                                     on er.Contact_ID equals cc.Contact_ID
                                             join rt in context.Events_RegistrationType
                                                     on er.RegistrationType_ID equals rt.RegistrationType_ID
                                             where er.Registration_ID == rego.registrationID && rt.IsCollegeRegistrationType
                                             select new RegistrationGroupAllocated
                                             {
                                                 registrationID = er.Registration_ID,
                                                 fullName = cc.FirstName + " " + cc.LastName,
                                                 registrationTypeID = rt.RegistrationType_ID,
                                                 registrationType = rt.RegistrationType,
                                                 registrationDate = er.RegistrationDate,
                                                 attended = er.Attended
                                             }).FirstOrDefault();

                            if (kid_rego != null)
                                _AllocatedKidsRegistrations.Add(kid_rego);
                        }
                    }
                }

                return _AllocatedKidsRegistrations;
            }
        }
        public int YouthAllocatedCount
        {
            get
            {
                if (SelectedEvent == null || Group == null) return 0;

                int count = 0;

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _RegistrationTypes = (from rt in context.Events_RegistrationType
                                              where rt.Conference_ID == SelectedEvent.id
                                              && rt.ApplicationRegistrationType
                                              select rt.RegistrationType_ID).ToList();

                    foreach (var id in _RegistrationTypes)
                    {
                        count += Group.GetAllocatedQty(SelectedEvent.venueID, id);
                    }
                }

                return count;
            }
        }
        public int KidsAllocatedCount
        {
            get
            {
                if (SelectedEvent == null || Group == null) return 0;

                int count = 0;

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _RegistrationTypes = (from rt in context.Events_RegistrationType
                                              where rt.Conference_ID == SelectedEvent.id
                                              && rt.IsCollegeRegistrationType
                                              select rt.RegistrationType_ID).ToList();
                    
                    foreach (var id in _RegistrationTypes)
                    {
                        count += Group.GetAllocatedQty(SelectedEvent.venueID, id);
                    }
                }

                return count;
            }
        }

        private List<RegistrationGroupUnallocated> _UnallocatedRegistrations;
        public List<RegistrationGroupUnallocated> UnallocatedRegistrations
        {
            get
            {
                if (SelectedEvent == null || Group == null) return null;

                if (_UnallocatedRegistrations == null)
                {
                    _UnallocatedRegistrations = new List<RegistrationGroupUnallocated>();
                    var bulk_regos = Group.GetUnallocatedRegistrations(SelectedEvent.venueID);
                    foreach(var reg in bulk_regos)
                    {
                        // Only continue if registration type has not been taken into account previously
                        if (_UnallocatedRegistrations
                                .Where(x => x.registrationTypeID == reg.registrationTypeID).Any())
                        {
                            // Skip and go to the next
                            continue;
                        }

                        using (PlanetshakersEntities context = new PlanetshakersEntities())
                        {
                            var new_reg = (from br in Group.GetUnallocatedRegistrations(SelectedEvent.venueID)
                                           join rt in context.Events_RegistrationType
                                                   on br.registrationTypeID equals rt.RegistrationType_ID
                                           where br.registrationTypeID == reg.registrationTypeID
                                           group br by new {rt.RegistrationType_ID, 
                                                            rt.RegistrationType} into g
                                           select new RegistrationGroupUnallocated
                                           {
                                               registrationTypeID = g.Key.RegistrationType_ID,
                                               registrationType = g.Key.RegistrationType,
                                               quantity = g.Sum(x => (int)x.quantity)
                                           }).First();

                            new_reg.allocated =
                                Group.GetAllocatedQty(SelectedEvent.venueID, 
                                                 (int)reg.registrationTypeID);

                            new_reg.pendingInvitations =
                                Group.GetInvitationQty(SelectedEvent.venueID,
                                                 (int)reg.registrationTypeID);

                            _UnallocatedRegistrations.Add(new_reg);
                        }
                    }
                }

                return _UnallocatedRegistrations;
            }
        }
        public int GroupUnallocatedCount
        {
            get
            {
                if (SelectedEvent == null || Group == null) return 0;
                return Group.GetUnallocatedQty(SelectedEvent.venueID);
            }
        }

        private List<RegistrationPendingInvitation> _PendingInvitations;
        public List<RegistrationPendingInvitation> PendingInvitations
        {
            get
            {
                if (SelectedEvent == null || Group == null) return null;

                if (_PendingInvitations == null)
                {
                    _PendingInvitations = new List<RegistrationPendingInvitation>();
                    foreach (var inv in Group.GetInvitations(SelectedEvent.venueID))
                    {
                        using (PlanetshakersEntities context = new PlanetshakersEntities())
                        {
                            _PendingInvitations.Add(
                                (from er in context.Events_RegistrationType
                                 where er.RegistrationType_ID == inv.registrationTypeID
                                 select new RegistrationPendingInvitation
                                 {
                                     invitationID = (int)inv.groupInvitationID,
                                     recipientEmail = inv.receiverEmail,
                                     registrationType = er.RegistrationType,
                                     expiry = (DateTime)inv.expirationDate
                                 }).First());
                        }
                    }
                }

                return _PendingInvitations;
            }
        }
        public List<CurrentRegistration> CurrentSingleRegistrations 
        {
            get
            {
                return Lists.GetSingleRegistrations()
                            .Where(r => r.venue_id == SelectedEvent.venueID)
                            .ToList();
            }
        }
        public List<CurrentRegistration> CurrentFriendRegistrations
        {
            get
            {
                return Lists.GetFriendRegistrations()
                            .Where(r => r.venue_id == SelectedEvent.venueID)
                            .ToList();
            }
        }
        public List<CurrentRegistration> CurrentFamilyRegistrations 
        { 
            get
            {
                return Lists.GetFamilyRegistrations()
                            .Where(r => r.venue_id == SelectedEvent.venueID)
                                    .ToList();
            }    
        }
        public List<CurrentRegistration> CurrentGroupRegistrations 
        { 
            get
            {
                return Lists.GetGroupRegistrations()
                            .Where(r => r.venue_id == SelectedEvent.venueID)
                            .ToList();
            }
        }
        // Used for allocations
        public int registrationTypeID { get; set; }
        public BasicDetailsPartialModel basicDetails { get; set; }
        public CredentialDetailsPartialModel credentialDetails { get; set; }
        public PersonalDetailsPartialModel personalDetails { get; set; }
        public ContactDetailsPartialModel contactDetails { get; set; }
        public ChurchDetailsPartialModel churchDetails { get; set; }
        public ChurchInvolvementPartialModel churchInvolvementDetails { get; set; }
        public EmergencyDetailsPartialModel emergencyDetails { get; set; }
        public void createExternalEventsContact(int contactID)
        {
            using (var ctx = new PlanetshakersEntities())
            {
                // create new common_contactExternalEvents
                //var new_contact_event = new Common_ContactExternalEvents();
                //new_contact_event.EmergencyContactName = "";
                //new_contact_event.EmergencyContactPhone = "";
                //new_contact_event.EmergencyContactRelationship = "";
                //new_contact_event.MedicalInformation = "";
                //new_contact_event.MedicalAllergies = "";
                //new_contact_event.AccessibilityInformation = "";
                //new_contact_event.EventComments = "";


                // create new external contact
                var new_external_contact = new Common_ContactExternal();
                new_external_contact.ChurchAddress1 = "";
                new_external_contact.ChurchAddress2 = "";
                new_external_contact.ChurchCountry_ID = null;
                new_external_contact.ChurchEmail = "";
                new_external_contact.ChurchName = basicDetails.Church;
                new_external_contact.ChurchNumberOfPeople = "";
                new_external_contact.ChurchPhone = "";
                new_external_contact.ChurchPostcode = "";
                new_external_contact.ChurchState_ID = null;
                new_external_contact.ChurchStateOther = "";
                new_external_contact.ChurchSuburb = "";
                new_external_contact.ChurchWebsite = "";
                new_external_contact.Denomination_ID = 49;
                new_external_contact.SeniorPastorName = "";
                new_external_contact.LeaderName = "";
                new_external_contact.Deleted = false;
                new_external_contact.Inactive = false;
                //new_external_contact.Common_ContactExternalEvents = new_contact_event;

                var contact = (from cc in ctx.Common_Contact
                               where cc.Contact_ID == contactID
                               select cc).FirstOrDefault();

                contact.Common_ContactExternal = new_external_contact;

                ctx.SaveChanges();
            }
        }
        public DocumentUploadViewModel DocumentUploads { get; set; }
        public AddOnModel addOnModel { get; set; }
        public List<Payment> PaymentList { get; set; }
        public List<Payment> sponsorshipList { get; set; }
    }


    public class EventRegisterModel
    {
        public Event SelectedEvent { get; set; }

        public string promoCode { get; set; }

        public int VenueId { get { return SelectedEvent.venueID; } }

        // A flag that determines if user has just purchased new registrations
        public bool recentlyRegistered { get; set; }

        public int remainingCount { get { return getLimit(SelectedEvent.venueID); } }

        public Voucher Voucher { get; set; }

        public string showCurrentRego
        {
            get
            {
                if (recentlyRegistered) return "block";
                else return "none";
            }
        }

        // A flag that determines whether or not the user has any existing registrations
        public bool hasRegistrations
        {
            get {
                return CurrentSingleRegistrations.Any() ||
                       CurrentFriendRegistrations.Any() ||
                       CurrentFamilyRegistrations.Any() ||
                       CurrentGroupRegistrations.Any();
            }
                    
        }
        public List<CurrentRegistration> CurrentSingleRegistrations
        {
            get
            {
                return Lists.GetSingleRegistrations()
                            .Where(r => r.event_id == SelectedEvent.id)
                            .ToList();
            }
        }
        public List<CurrentRegistration> CurrentFriendRegistrations
        {
            get
            {
                return Lists.GetFriendRegistrations()
                            .Where(r => r.event_id == SelectedEvent.id)
                            .ToList();
            }
        }
        public List<CurrentRegistration> CurrentFamilyRegistrations
        {
            get
            {
                return Lists.GetFamilyRegistrations()
                            .Where(r => r.event_id == SelectedEvent.id)
                                    .ToList();
            }
        }
        public List<CurrentRegistration> CurrentGroupRegistrations
        {
            get
            {
                return Lists.GetGroupRegistrations()
                            .Where(r => r.event_id == SelectedEvent.id)
                            .ToList();
            }
        }

        public int registrationCount(int qtyGroupId)
        {
            var qtyGroups = SelectedEvent.rego_categories.Where(x => x.group_id == qtyGroupId).ToList();
            var count = 0;

            foreach (var qtyGroup in qtyGroups)
            {
                List<int> registrationTypes = qtyGroup.rego_types.Select(rt => rt.id).ToList();

                count += CurrentSingleRegistrations
                         .Where(x => registrationTypes.Contains(x.registration_type_id))
                         .Count();
                count += (int)CurrentGroupRegistrations
                         .Where(x => registrationTypes.Contains(x.registration_type_id))
                         .Sum(y => y.quantity == null ? 0 : y.quantity);
            }

            return count;
        }

        private int getLimit(int Venue_ID)
        {
            var limit = 0;

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var curr = (from gbr in entity.Events_GroupBulkRegistration
                            where gbr.Venue_ID == Venue_ID 
                            select gbr.Quantity).DefaultIfEmpty(0).Sum();

                curr += (from r in entity.Events_Registration
                         where r.Venue_ID == Venue_ID && r.GroupLeader_ID == null
                         select r.Registration_ID).Count();

                var total = (from ev in entity.Events_Venue
                             where ev.Venue_ID == Venue_ID
                             select ev.MaxRegistrants).Single();

                limit = (int)(total - curr);
            }

            return limit;
        }
    }

    public class EventAllocateModel
    {
        #region Personal Details

        private int _salutation;
        private string _firstname;
        private string _lastname;
        private DateTime _dob;
        private string _gender;
        private string _address1;
        private string _address2;
        private string _suburb;
        private string _state;
        private string _postcode;
        private int _country;
        private string _mobile;
        private string _email;

        [Required]
        [Display(Name = "Title")]
        public int Salutation
        {
            get { return _salutation; }
            set { _salutation = value; }
        }
        public SelectList SalutationList { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string Firstname
        {
            get { return String.IsNullOrEmpty(_firstname) ? "" : _firstname; }
            set { _firstname = value; }
        }

        [Required]
        [Display(Name = "Last Name")]
        public string Lastname
        {
            get { return String.IsNullOrEmpty(_lastname) ? "" : _lastname; }
            set { _lastname = value; }
        }

        [Display(Name = "Date of Birth")]
        public string DoB
        {
            get
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("en-AU", true);
                if (_dob != null)
                {
                    return ((DateTime)_dob).ToString("d", culture);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("en-AU", true);
                _dob = DateTime.Parse(value, culture);
            }
        }

        [Display(Name = "Gender")]
        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        [Display(Name = "Address")]
        public string Address1
        {
            get { return String.IsNullOrEmpty(_address1) ? "" : _address1; }
            set { _address1 = value; }
        }
        public string Address2
        {
            get { return String.IsNullOrEmpty(_address2) ? "" : _address2; }
            set { _address2 = value; }
        }

        [Required]
        [Display(Name = "Suburb / City")]
        public string Suburb
        {
            get { return String.IsNullOrEmpty(_suburb) ? "" : _suburb; }
            set { _suburb = value; }
        }

        [Required]
        [Display(Name = "State")]
        public string StateOther
        {
            get { return _state; }
            set { _state = value; }
        }

        [Required]
        [Display(Name = "Zip / Postcode")]
        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }

        [Required]
        [Display(Name = "Country")]
        public int Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public SelectList CountryList { get; set; }

        [Required]
        [Display(Name = "Mobile")]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        #endregion
        #region Church Details

        private string _churchName;
        private int _churchDenomination;
        private string _churchSeniorPastor;
        private string _churchLeader;
        private string _churchSize;
        private string _churchAddress1;
        private string _churchAddress2;
        private string _churchSuburb;
        private string _churchState;
        private string _churchPostcode;
        private int _churchCountry;
        private string _churchPhone;
        private string _churchWebsite;
        private string _churchEmail;

        [Display(Name = "Name")]
        public string ChurchName
        {
            get { return String.IsNullOrEmpty(_churchName) ? "" : _churchName; }
            set { _churchName = value; }
        }

        [Display(Name = "Denomination")]
        public int ChurchDenomination
        {
            get { return _churchDenomination; }
            set { _churchDenomination = value; }
        }
        public SelectList ChurchDenominationList { get; set; }

        [Display(Name = "Senior Pastor's Name")]
        public string ChurchSeniorPastor
        {
            get { return String.IsNullOrEmpty(_churchSeniorPastor) ? "" : _churchSeniorPastor; }
            set { _churchSeniorPastor = value; }
        }

        [Display(Name = "Leader's Name")]
        public string ChurchLeader
        {
            get { return String.IsNullOrEmpty(_churchLeader) ? "" : _churchLeader; }
            set { _churchLeader = value; }
        }

        [Display(Name = "Congregation size")]
        public string ChurchSize
        {
            get { return String.IsNullOrEmpty(_churchSize) ? "" : _churchSize; }
            set { _churchSize = value; }
        }

        [Display(Name = "Address")]
        public string ChurchAddress1
        {
            get { return String.IsNullOrEmpty(_churchAddress1) ? "" : _churchAddress1; }
            set { _churchAddress1 = value; }
        }
        public string ChurchAddress2
        {
            get { return String.IsNullOrEmpty(_churchAddress2) ? "" : _churchAddress2; }
            set { _churchAddress2 = value; }
        }

        [Display(Name = "Suburb")]
        public string ChurchSuburb
        {
            get { return String.IsNullOrEmpty(_churchSuburb) ? "" : _churchSuburb; }
            set { _churchSuburb = value; }
        }

        [Display(Name = "State")]
        public string ChurchState
        {
            get { return String.IsNullOrEmpty(_churchState) ? "" : _churchState; }
            set { _churchState = value; }
        }

        [Display(Name = "Zip / Postcode")]
        public string ChurchPostcode
        {
            get { return String.IsNullOrEmpty(_churchPostcode) ? "" : _churchPostcode; }
            set { _churchPostcode = value; }
        }

        [Display(Name = "Country")]
        public int ChurchCountry
        {
            get { return _churchCountry; }
            set { _churchCountry = value; }
        }

        [Display(Name = "Phone")]
        public string ChurchPhone
        {
            get { return String.IsNullOrEmpty(_churchPhone) ? "" : _churchPhone; }
            set { _churchPhone = value; }
        }

        [Display(Name = "Website")]
        public string ChurchWebsite
        {
            get { return String.IsNullOrEmpty(_churchWebsite) ? "" : _churchWebsite; }
            set { _churchWebsite = value; }
        }

        [Display(Name = "Email")]
        public string ChurchEmail
        {
            get { return String.IsNullOrEmpty(_churchEmail) ? "" : _churchEmail; }
            set { _churchEmail = value; }
        }

        public List<ChurchInvolvement> AvailableCInvolvement { get; set; }
        public int[] SelectedCInvolvement { get; set; }

        #endregion
        #region Emergency Details

        private string _emergencyContactName;
        private string _emergencyContactPhone;
        private string _emergencyContactRelationship;
        private string _medicalInfo;
        private string _medicalAllergies;

        [Display(Name = "Emergency Contact Name")]
        public string EmergencyContactName
        {
            get
            {
                return String.IsNullOrEmpty(_emergencyContactName) ?
                       "" : _emergencyContactName;
            }
            set { _emergencyContactName = value; }
        }

        [Display(Name = "Emergency Contact Phone")]
        public string EmergencyContactPhone
        {
            get
            {
                return String.IsNullOrEmpty(_emergencyContactPhone) ?
                       "" : _emergencyContactPhone;
            }
            set { _emergencyContactPhone = value; }
        }

        [Display(Name = "Emergency Contact Relationship")]
        public string EmergencyContactRelationship
        {
            get
            {
                return String.IsNullOrEmpty(_emergencyContactRelationship) ?
                        "" : _emergencyContactRelationship;
            }
            set { _emergencyContactRelationship = value; }
        }

        [Display(Name = "Special Requirement (food alergies, vegetarian, etc.)")]
        public string MedicalInfo
        {
            get { return String.IsNullOrEmpty(_medicalInfo) ? "" : _medicalInfo; }
            set { _medicalInfo = value; }
        }

        [Display(Name = "Medical Allergies")]
        public string MedicalAllergies
        {
            get
            {
                return String.IsNullOrEmpty(_medicalAllergies) ?
                       "" : _medicalAllergies;
            }
            set { _medicalAllergies = value; }
        }

        #endregion
    }

    
}
