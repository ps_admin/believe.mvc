﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Planetshakers.Events.Mvc.Controllers;
using Microsoft.Ajax.Utilities;

namespace Planetshakers.Events.Mvc.Models
{
    public class LoginPromptModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        // Event 
        public Event Event { get; set; }
    }

    public class CartModel
    {
        public CartModel()
        {
            Event = new Event();
            CCPayment = new CCPaymentModel();
            Cart = new List<CartItem>();
            previousCart = new List<CartItem>();
        }

        public CartModel(int event_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Event = (from ec in context.Events_Conference
                        join ev in context.Events_Venue
                        on ec.Conference_ID equals ev.Conference_ID
                        join ba in context.Common_BankAccount
                        on ec.BankAccount_ID equals ba.BankAccount_ID
                        where ec.Conference_ID == event_id
                        select new Event
                        {
                            id = ec.Conference_ID,
                            venueID = ev.Venue_ID,
                            name = ec.ConferenceName,
                            date = ec.ConferenceDate,
                            location = ev.VenueName,
                            venueLocation = ev.VenueLocation,
                            currency = ba.Currency
                        }).FirstOrDefault();

                Cart = new List<CartItem>();
                previousCart = new List<CartItem>();
            }
        }

        public CartModel(int contact_id, Event evnt)
        {
            ContactId = contact_id;
            Event = evnt;
        }

        public string ErrorMessage { get; set; }

        // Logged in contact
        public int ContactId { get; set; }

        // get name of contactId
        public string FullName
        {
            get
            {
                var name = String.Empty;

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    name = (from cc in context.Common_Contact
                            where cc.Contact_ID == ContactId
                            select cc.FirstName + " " + cc.LastName).FirstOrDefault();
                }

                return name;
            }
        }

        // get email of contactId
        public string Email
        {
            get
            {
                var name = String.Empty;

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Need to account for people who may not have an email
                    name = (from cc in context.Common_Contact
                            where cc.Contact_ID == ContactId
                            select (String.IsNullOrEmpty(cc.Email) ? cc.Email2 : cc.Email)).FirstOrDefault();
                }

                return name;
            }
        }

        // Items in the cart
        public List<CartItem> Cart { get; set; }
        public List<CartItem> previousCart { get; set; }

        // Event 
        public Event Event { get; set; }

        // Total cost
        public decimal GrandTotal 
        { 
            get 
            {
                var total = Cart.Sum(r => r.subtotal) - (Voucher != null ? Voucher.value : 0);

                return (total > 0 ? total : 0);
            } 
        }

        public decimal RemainingCredit
        {
            get
            {
                var credits = Cart.Sum(r => r.subtotal) - (Voucher != null ? Voucher.value : 0);

                return (credits < 0 ? credits * -1 : 0);
            }
        }

        // Total number of registrations
        public int Quantity { get { return Cart.Sum(r => r.quantity); } }

        public Voucher Voucher { get; set; }

        // This is used to prevent multiple transactionsfrom executing
        public bool ProcessingPayment { get; set; }

        // CC Info
        public CCPaymentModel CCPayment { get; set; }

        //for sponsorship payments
        public SponsorDetails SponsorDetails { get; set; }
    }
    

    public class SponsorshipCartModel
    {
        public int SponsoringToContactID { get; set; }
        public string ErrorMessage { get; set; }

        // Logged in contact
        public int ContactId { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        // Items in the cart
        public SponsorshipCartItem Cart { get; set; }

        // Event 
        public Event Event { get; set; }

        // Total cost
        public decimal GrandTotal { get { return Cart.totalSponsoredAmount; } }

        // This is used to prevent multiple transactionsfrom executing
        public bool ProcessingPayment { get; set; }

        // CC Info
        public CCPaymentModel CCPayment { get; set; }
    }

    

    public class CCPaymentModel
    {
        public int payment_id { get; set; }

        public int paymenttype_id { get; set; }

        public string PaymentType 
        {
            get
            {
                using (var context = new PlanetshakersEntities())
                {
                   var type = (from pt in context.Common_PaymentType
                                where pt.PaymentType_ID == paymenttype_id
                                select pt.PaymentType).SingleOrDefault();

                    return type;
                }
            }
        }        

        public Common_BankAccount BankAccount { get; set; }

        public string transactionReference { get; set; }

        [Display(Name = "Credit Card Number")]
        public string CCNumber { get; set; }
        public string getCCNumber(bool masked)
        {
            if (masked)
            {
                return String.Concat(CCNumber.Substring(0, 4),
                                     "".PadLeft(CCNumber.Length - 8, 'X'),
                                     CCNumber.Substring(CCNumber.Length - 4));
            }
            else
            {
                return CCNumber.Replace("-", "").Replace(" ", "").Replace("/", "");
            }
        }


        [Display(Name = "Credit Card Expiry")]
        public int CCExpiryMonth { get; set; }
        public SelectList CCExpMonthList { get; set; }


        public int CCExpiryYear { get; set; }
        public SelectList CCExpYearList { get; set; }

        public string getCCExpiry()
        {
            return CCExpiryMonth.ToString("00") + CCExpiryYear.ToString("00");
        }

        [Display(Name = "CVC2")]
        public string CCCVC { get; set; }

        [Display(Name = "Cardholder's Name")]
        public string CCName { get; set; }

        [Display(Name = "Cardholder's Phone #")]
        public string CCPhone { get; set; }
    }
}