﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Events.Mvc.Controllers;
using Newtonsoft.Json;

namespace Planetshakers.Events.Mvc.Models
{
    public class DonatedEmailModel{

        public string Email { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("TotalAmount")]
        public string TotalAmount { get; set; }

        [JsonProperty("Currency")]
        public string Currency { get; set; }
    }

    public class CreditedEmailModel
    {

        public string Email { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("TotalAmount")]
        public string TotalAmount { get; set; }

        [JsonProperty("Currency")]
        public string Currency { get; set; }

        [JsonProperty("VoucherCode")]
        public string VoucherCode { get; set; }
    }

    public class ConfirmationBarcodeEmailModel
    {   
        // Name of contact
        public string FullName { get; set; }

        // Event
        public Event Event { get; set; }

        public int ContactId { get; set; }

        public int VenueId { get; set; }

        // Registration Number
        private int _RegistrationNumber { get; set; }
        public int RegistrationNumber
        {
            get
            {
                
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                 {
                     _RegistrationNumber = (from r in context.Events_Registration
                                                where r.Contact_ID == ContactId &&
                                                    r.Venue_ID == VenueId
                                                select r.Registration_ID).First();
                 }

                return _RegistrationNumber;
            }
        }

        public string Email { get; set; }
    }

    public class RegistrationReceipt
    {
        public string Email { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Subject")]
        public string Subject { get; set; }

        [JsonProperty("banner_img")]
        public string banner_img { get; set; }
        
        public List<RegistrationConfirmationItem> Items { get; set; }

        public Voucher Promo { get; set; }

        [JsonProperty("ItemList")]
        public string ItemList 
        { 
            get
            {
                var list = "";
                foreach (RegistrationConfirmationItem item in Items)
                {
                    list += "<tr>";
                    list += "<td>" + item.reg_name + "</td>";
                    list += "<td>" + item.qty + "</td>";
                    list += "<td>" + item.price + "</td>";
                    list += "<td>" + Currency + " " + item.price * item.qty + "</td>";
                    list += "</tr>";
                }

                if (Promo != null)
                {
                    list += "<tr>";
                    list += "<td>Voucher code - " + Promo.code + "</td>";
                    list += "<td>1</td>";
                    list += "<td>" + Promo.value + "</td>";
                    list += "<td>- " + Currency + " " + Promo.applied_value + "</td>";
                    list += "</tr>";
                }

                return list;
            }
        }

        [JsonProperty("EventName")]
        public string EventName { get; set; }

        [JsonProperty("EventDate")]
        public string EventDate { get; set; }

        [JsonProperty("EventVenue")]
        public string EventVenue { get; set; }

        [JsonProperty("ReceiptNumber")]
        public string ReceiptNumber { get; set; }

        [JsonProperty("PaymentType")]
        public string PaymentType { get; set; }

        [JsonProperty("TransactionReference")]
        public string TransactionReference { get; set; }

        [JsonProperty("Currency")]
        public string Currency { get; set; }
        
        public decimal _GrandTotal 
        {
            get
            {
                var total = Items.Sum(r => r.price * r.qty) - (Promo != null ? Promo.value : 0);

                if (total <= 0)
                {
                    return 0;
                }
                else
                {
                    return total;
                }
            }
        }

        [JsonProperty("GrandTotal")]
        public string GrandTotal { get { return _GrandTotal.ToString("0.00"); } }
    }

    public class ConfirmationBarcode
    {
        public int Contact_ID { get; set; }

        public string Email { get; set; }

        [JsonProperty("Subject")]
        public string Subject { get; set; }

        [JsonProperty("EventName")]
        public string EventName { get; set; }

        [JsonProperty("EventDate")]
        public string EventDate { get; set; }

        [JsonProperty("EventVenue")]
        public string EventVenue { get; set; }

        [JsonProperty("VenueLocation")]
        public string VenueLocation { get; set; }

        public int RegistrationTypeID { get; set; }

        [JsonProperty("RegistrationType")]
        public string RegistrationType 
        { 
            get 
            {
                using (var ctx = new PlanetshakersEntities())
                {
                    var rt = (from ert in ctx.Events_RegistrationType
                              where ert.RegistrationType_ID == RegistrationTypeID
                              select ert.RegistrationType).FirstOrDefault();

                    return rt;
                }
            }
        }

        [JsonProperty("RegistrationNumber")]
        public string RegistrationNumber { get; set; }

        [JsonProperty("ContactDetails")]
        public string ContactDetails { get; set; }
    }

    public class PasswordResetEmailModel
    {
        // Contact's Email
        public string Email { get; set; }

        public string EncryptedToken { get; set; }

        public string FullName { get; set; }
    }

    public class DevErrorNotificationEmailModel
    {
        public Exception ex { get; set; }

        public string extraNotes { get; set; }
    }

    public class SponsorshipConfirmationEmailModel
    {
        // Logged in contact
        public int ContactId { get; set; }

        // Name of contact
        [JsonProperty("FullName")]
        public string FullName { get; set; }

        // Email of contact
        public string Email { get; set; }

        // Event 
        public Event Event { get; set; }

        // Total sponsorshipAmount
        public decimal GrandTotal { get; set; }

        [JsonProperty("EventName")]
        public string EventName { get {
                return Event.name;
            } 
        }

        [JsonProperty("Total")]
        public string Total { get {
                return Event.currency + " " + GrandTotal.ToString("0.00");
            } 
        }
    }

    public class GroupInvitationEmailModel
    {
        private UrlHelper helper = new UrlHelper(HttpContext.Current.Request.RequestContext);

        private string host = HttpContext.Current.Request.Url.Host;
        public int GroupLeaderID { get; set; }

        private string _GroupLeaderName { get; set; }

        [JsonProperty("GroupLeader")]
        public string GroupLeaderName { 
            get
            {
                if (String.IsNullOrWhiteSpace(_GroupLeaderName))
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        _GroupLeaderName = (from cc in context.Common_Contact
                                            where cc.Contact_ID == GroupLeaderID
                                            select cc.FirstName + " " + cc.LastName
                                            ).FirstOrDefault();
                    }
                }

                return _GroupLeaderName;
            }
        }

        public Event Event { get; set; }

        [JsonProperty("EventName")]
        public string EventName { get; set; }

        [JsonProperty("EventVenue")]
        public string EventVenue{ get; set; }

        [JsonProperty("EventDate")]
        public string EventDate { get; set; }

        [JsonProperty("EmailAddress")]
        public string Email { get; set; }

        public Guid InvitationID { get; set; }

        public int RegistrationTypeID { get; set; }

        private string _RegistrationType { get; set; }

        [JsonProperty("RegistrationType")]
        public string RegistrationType
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_RegistrationType))
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        _RegistrationType = (from rt in context.Events_RegistrationType
                                             where rt.RegistrationType_ID == RegistrationTypeID
                                             select rt.RegistrationType).First();
                    }
                }
                return _RegistrationType;
            }
        }

        [JsonProperty("AcceptLink")]
        public string AcceptLink
        {
            get
            {
                return helper.Action("AcceptInvitation", 
                                     "Invitation",
                                     new System.Web.Routing.RouteValueDictionary(
                                     new { unique_id = InvitationID }),"https",
                                     host);
            }       
        }

        [JsonProperty("DeclineLink")]
        public string DeclineLink
        {
            get
            {
                return helper.Action("DeclineInvitation", "Invitation",
                                     new System.Web.Routing.RouteValueDictionary(
                                     new { unique_id = InvitationID }), "https",
                                     host);
            }
        }

        [JsonProperty("ChildLink")]
        public string ChildLink
        {
            get
            {
                return helper.Action("RegisterChild", "Invitation",
                                     new System.Web.Routing.RouteValueDictionary(
                                     new { unique_id = InvitationID }), "https",
                                     host); 
            }
        }

        // Reject Url
    }

    public class GroupInvitationNotificationEmailModel
    {
        private UrlHelper helper = new UrlHelper(HttpContext.Current.Request.RequestContext);

        public int GroupLeaderID { get; set; }

        private string _GroupLeaderName { get; set; }

        [JsonProperty("GroupLeader")]
        public string GroupLeaderName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_GroupLeaderName))
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        _GroupLeaderName = (from cc in context.Common_Contact
                                            where cc.Contact_ID == GroupLeaderID
                                            select cc.FirstName + " " + cc.LastName
                                            ).FirstOrDefault();
                    }
                }

                return _GroupLeaderName;
            }
        }

        private string _GroupLeaderEmail { get; set; }

        [JsonProperty("GroupLeaderEmail")]
        public string GroupLeaderEmail 
        { 
            get
            {
                if (String.IsNullOrWhiteSpace(_GroupLeaderEmail))
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        _GroupLeaderEmail = (from cc in context.Common_Contact
                                             where cc.Contact_ID == GroupLeaderID
                                             select cc.Email).First();
                    }
                }
                return _GroupLeaderEmail;
            }
        }

        public Event Event { get; set; }

        [JsonProperty("EventName")]
        public string EventName { get; set; }

        [JsonProperty("EmailAddress")]
        public string InvitationRecipientEmail { get; set; }

        public int RegistrantID { get; set; }

        private string _RegistrantName { get; set; }

        [JsonProperty("FullName")]
        public string RegistrantName { 
            get 
            {
                if (String.IsNullOrWhiteSpace(_RegistrantName))
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        _RegistrantName = (from cc in context.Common_Contact
                                           where cc.Contact_ID == RegistrantID
                                           select cc.FirstName + " " + cc.LastName
                                           ).FirstOrDefault();
                    }
                }

                return _RegistrantName;
            } 
        }

        [JsonProperty("AllocateMoreLink")]
        public string AllocateMoreLink
        {
            get
            {
                return helper.Action("Registrations", "Events",
                                     new System.Web.Routing.RouteValueDictionary(
                                     new { id = Event.id }), "http",
                                     HttpContext.Current.Request.Url.Host); 
            }
        }
    }

    public class ApplicationReceivedEmailModel
    {
        public string FirstName { get; set; }
        public string TripName { get; set; }
        public string Email { get; set; }
    }
    public class AccountCreationEmailModel
    {
        public string FirstName { get; set; }
        public string Email { get; set; }
    }

    public class DepositReceivedEmailModel
    {
        public string FirstName { get; set; }
        public string TripName { get; set; }
        public string Email { get; set; }
    }
    //public class ApplicationApprovedEmailModel
    //{
    //    public string TripName { get; set; }
    //    public string TripDashBoard { get; set; }
    //    public string TripPaymentProgress { get; set; }
    //    public string Email { get; set; }
    //}
}
