﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Mvc.Models
{
    public class FundingGoalViewModel
    {
        //should only return one registrationType - that has a funding milestones
        public int RegistrationType_ID
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var venue_ID = (from v in context.Events_Venue
                                    where v.Conference_ID == Event_ID
                                    select v.Venue_ID).FirstOrDefault();
                    var RegistrationType_ID = (from r in context.Events_GroupBulkRegistration
                                    join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                    where r.Venue_ID == venue_ID && r.GroupLeader_ID == Contact_ID && rt.AddOnRegistrationType == false && rt.ApplicationRegistrationType == false
                                    select r.RegistrationType_ID).FirstOrDefault();

                    return RegistrationType_ID;
                }
            }

        }
        public int Event_ID { get; set; }
        public int Contact_ID { get; set; }
        public List<FundingGoal> FundingGoalList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from fg in context.Events_FundingGoal
                                where fg.RegistrationType_ID == RegistrationType_ID
                                orderby fg.DueDate
                                select new FundingGoal
                                {
                                    ID = fg.FundingGoal_ID,
                                    Milestone = fg.Milestone,
                                    CumulativeTotal = fg.CumulativeTotal,
                                    DueDate = fg.DueDate,
                                    RegistrationType_ID = RegistrationType_ID
                                }).ToList();
                    return list;
                }
            }
        }
    }
    public class FundingGoal
    {
        public int ID { get; set; }
        public int RegistrationType_ID { get; set; }
        public string Milestone { get; set; }
        public int CumulativeTotal { get; set; }
        public DateTime DueDate { get; set; }

    }

}