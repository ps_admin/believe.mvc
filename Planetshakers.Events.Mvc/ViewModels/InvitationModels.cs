﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Planetshakers.Events.Mvc.Controllers;

namespace Planetshakers.Events.Mvc.Models
{
    public class InvitationLoginPromptModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        // Event 
        public Event Event { get; set; }
    }

    public class InvitationRegisterModel
    {
        public CredentialDetailsPartialModel credentialDetails { get; set; }
        public PersonalDetailsPartialModel personalDetails { get; set; }
        public ContactDetailsPartialModel contactDetails { get; set; }
        public ChurchDetailsPartialModel churchDetails { get; set; }
        public ChurchInvolvementPartialModel churchInvolvementDetails { get; set; }
        public EmergencyDetailsPartialModel emergencyDetails { get; set; }
    }

    public class InvitationRegisterChildModel
    {
        public BasicDetailsPartialModel basicDetails { get; set; }
        public PersonalDetailsPartialModel personalDetails { get; set; }
        public ContactDetailsPartialModel contactDetails { get; set; }
        public ChurchDetailsPartialModel churchDetails { get; set; }
        public EmergencyDetailsPartialModel emergencyDetails { get; set; }
    }
}