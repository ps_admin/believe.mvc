﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Mvc.Models
{
    public class ChildEnrolmentModel
    {
        public ChildEnrolmentModel()
        {
            childEnrolment = new ChildEnrolment();
        }

        public ChildEnrolmentModel(int Registration_ID)
        {
            int childEnrolment_ID = getChildEnrolmentForm(Registration_ID);

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var contact = (from ce in entity.Events_RegistrationChildEnrolment
                               join cc in entity.Common_Contact
                               on ce.Contact_ID equals cc.Contact_ID
                               where ce.ChildEnrolment_ID == childEnrolment_ID
                               select new ChildEnrolment
                               {
                                   ChildEnrolment_ID = childEnrolment_ID,
                                   Registration_ID = Registration_ID,
                                   Contact_ID = cc.Contact_ID,
                                   FirstName = cc.FirstName,
                                   LastName = cc.LastName,
                                   DateOfBirth = (cc.DateOfBirth.HasValue) ? cc.DateOfBirth.Value : DateTime.Today,
                                   ChildSchoolGrade_ID = ce.ChildSchoolGrade_ID,
                                   ParentName = ce.ParentName,
                                   ParentEmail = ce.ParentEmail,
                                   ParentMobile = ce.ParentMobile,
                                   EmergencyContactName = ce.EmergencyContactName,
                                   EmergencyContactNumber = ce.EmergencyContactNumber,
                                   EmergencyContactRelationship = ce.EmergencyContactRelationship,
                                   EmergencyContactName2 = ce.EmergencyContactName2,
                                   EmergencyContactNumber2 = ce.EmergencyContactNumber2,
                                   EmergencyContactRelationship2 = ce.EmergencyContactRelationship2,
                                   Comments = ce.Comments,
                                   SignedBy =  ce.SignedBy,
                                   DateSigned = ce.DateSigned
                               }).FirstOrDefault();

                childEnrolment = contact;

                var questions = (from q in entity.Events_ChildEnrolmentQuestion
                                 join a in entity.Events_ChildEnrolmentAnswer 
                                 on q.Question_ID equals a.Question_ID into ques
                                 from qa in ques.DefaultIfEmpty()
                                 where qa.ChildEnrolment_ID == childEnrolment_ID
                                 select new Question
                                 {
                                     question_ID = q.Question_ID,
                                     question = q.Question,
                                     type = q.Type,
                                     required = q.Required,
                                     answer_ID = qa.Answer_ID,
                                     answer = qa.Answer,
                                     sortOrder = q.SortOrder
                                 }).ToList();

                if (questions != null)
                {
                    childEnrolmentQuestions = questions;

                    medicalQuestions = questions.Where(x => x.sortOrder >= 100 && x.sortOrder < 200).ToList();
                    additionalQuestions = questions.Where(x => x.sortOrder >= 200 && x.sortOrder < 300).ToList();
                    nappyQuestions = questions.Where(x => x.sortOrder >= 300 && x.sortOrder < 400).ToList();

                    medicalNeed = medicalQuestions.Where(q => q.answer != String.Empty).Any();
                    allergyNeed = medicalQuestions.Where(q => q.answer != String.Empty).Any();
                    additionalNeed = additionalQuestions.Where(q => q.answer != String.Empty).Any();
                    nappyNeed = nappyQuestions.Where(q => q.answer != String.Empty).Any();
                }

                var room = (from er in entity.Events_Registration
                            join ert in entity.Events_RegistrationType
                                on er.RegistrationType_ID equals ert.RegistrationType_ID
                            where er.Registration_ID == Registration_ID
                            select ert.RegistrationType).FirstOrDefault();

                childEnrolment.RoomAllocation = room;
            }
        }

        public ChildEnrolmentModel(int Registration_ID, int Contact_ID, int User_ID)
        {

        }

        /*public ChildEnrolmentModel(int Registration_ID, int User_ID)
        {
            var child = new ChildEnrolment();
            child.Registration_ID = Registration_ID;
            child.SignedBy = User_ID;

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                child = (from ce in entity.Events_RegistrationChildEnrolment
                         join cc in entity.Common_Contact
                            on ce.Contact_ID equals cc.Contact_ID
                         where ce.Registration_ID == Registration_ID && ce.SignedBy == User_ID
                         select new ChildEnrolment
                         {
                             Contact_ID = ce.Contact_ID,
                             ChildSchoolGrade_ID = ce.ChildSchoolGrade_ID,
                             FirstName = cc.FirstName,
                             LastName = cc.LastName,
                             DateOfBirth = (DateTime) cc.DateOfBirth,
                             ParentName = ce.ParentName,
                             ParentEmail = ce.ParentEmail,
                             ParentMobile = ce.ParentMobile,
                             EmergencyContactName = ce.EmergencyContactName,
                             EmergencyContactNumber = ce.EmergencyContactNumber,
                             EmergencyContactRelationship = ce.EmergencyContactRelationship,
                             DateSigned = (DateTime) ce.DateSigned
                         }).FirstOrDefault();

                var rego = (from er in entity.Events_Registration
                            where er.Registration_ID == Registration_ID
                            select er).FirstOrDefault();

                if (child == null)
                {
                    // throw error
                }
                else
                {
                    childEnrolment = child;
                }
            }
        }*/

        public ChildEnrolment childEnrolment { get; set; }

        public int venue_ID 
        {
            get 
            {
                int id = 0;
            
                using (PlanetshakersEntities entity = new PlanetshakersEntities())
                {
                    id = (from er in entity.Events_Registration
                          where er.Registration_ID == childEnrolment.Registration_ID
                          select er.Venue_ID).FirstOrDefault();
                }

                return id;
            } 
        }

        public bool medicalNeed { get; set; }

        public bool allergyNeed { get; set; }

        public bool additionalNeed { get; set; }

        public bool nappyNeed { get; set; }

        public List<Question> childEnrolmentQuestions { get; set; }

        public List<Question> medicalQuestions { get; set; }

        public List<Question> additionalQuestions { get; set; }

        public List<Question> nappyQuestions { get; set; }

        public SelectList schoolGrade_list
        {
            get
            {
                return new SelectList(_schoolGrade_list, "id", "name");
            }
        }

        private List<SchoolGrade> _schoolGrade_list
        {
            get
            {
                var list = new List<SchoolGrade>();

                using (PlanetshakersEntities entity = new PlanetshakersEntities())
                {
                    list = (from sg in entity.Events_ChildSchoolGrade
                            orderby sg.SortOrder
                            select new SchoolGrade
                            {
                                id = sg.ChildSchoolGrade_ID,
                                name = sg.Grade
                            }).ToList();
                }

                return list;
            }
        }

        private int getChildEnrolmentForm(int rego_id)
        {
            int id = 0;
            childEnrolment = new ChildEnrolment();

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                // Check for existing child enrolment form
                var has_form = (from ce in entity.Events_RegistrationChildEnrolment
                                where ce.Registration_ID == rego_id
                                select ce).Any();

                if (has_form)
                {
                    // Find child enrolment form by registration id
                    id = (from ce in entity.Events_RegistrationChildEnrolment
                          where ce.Registration_ID == rego_id
                          select ce.ChildEnrolment_ID).FirstOrDefault();

                    childEnrolment.ChildEnrolment_ID = id;
                } else
                {
                    // Create new child enrolment form 
                    var contact_id = (from er in entity.Events_Registration
                                      where er.Registration_ID == rego_id
                                      select er.Contact_ID).FirstOrDefault();

                    var new_childEnrolment = new Events_RegistrationChildEnrolment();
                    new_childEnrolment.Registration_ID = rego_id;
                    new_childEnrolment.Contact_ID = contact_id;
                    new_childEnrolment.ChildSchoolGrade_ID = 1;

                    new_childEnrolment.ParentName = String.Empty;
                    new_childEnrolment.ParentMobile = String.Empty;
                    new_childEnrolment.ParentEmail = String.Empty;

                    new_childEnrolment.EmergencyContactName = String.Empty;
                    new_childEnrolment.EmergencyContactNumber = String.Empty;
                    new_childEnrolment.EmergencyContactRelationship = String.Empty;

                    new_childEnrolment.EmergencyContactName2 = String.Empty;
                    new_childEnrolment.EmergencyContactNumber2 = String.Empty;
                    new_childEnrolment.EmergencyContactRelationship2 = String.Empty;

                    new_childEnrolment.Comments = String.Empty;

                    entity.Events_RegistrationChildEnrolment.Add(new_childEnrolment);

                    var answers = new List<Events_ChildEnrolmentAnswer>();

                    var questions = (from eq in entity.Events_ChildEnrolmentQuestion
                                        where eq.Active == true
                                        select eq).ToList();

                    answers = questions.Select(a => new Events_ChildEnrolmentAnswer()
                                {
                                    ChildEnrolment_ID = new_childEnrolment.ChildEnrolment_ID,
                                    Question_ID = a.Question_ID,
                                    Answer = String.Empty
                                }).ToList();

                    entity.Events_ChildEnrolmentAnswer.AddRange(answers);
                    entity.SaveChanges();

                    id = new_childEnrolment.ChildEnrolment_ID;
                }
            }

            return id;
        }

        public void setChildEnrolmentQuestion(int childEnrolment_ID)
        {
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                childEnrolmentQuestions = (from q in entity.Events_ChildEnrolmentQuestion
                                           join a in entity.Events_ChildEnrolmentAnswer
                                            on q.Question_ID equals a.Question_ID into ans
                                           from qa in ans.DefaultIfEmpty()
                                           select new Question
                                           {
                                               question_ID = q.Question_ID,
                                               question = q.Question,
                                               type = q.Type,
                                               required = q.Required,
                                               sortOrder = q.SortOrder,
                                               answer_ID = qa.Answer_ID,
                                               answer = qa.Answer
                                           }).ToList();
            }
        }

        public void updateChildDetails(ChildEnrolment child)
        {
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var contact = (from cc in entity.Common_Contact
                               where cc.Contact_ID == child.Contact_ID
                               select cc).FirstOrDefault();

                var external = (from ccee in entity.Common_ContactExternalEvents
                                where ccee.Contact_ID == child.Contact_ID
                                select ccee).FirstOrDefault();

                if (child.DateOfBirth != null && child.DateOfBirth >= DateTime.Today.AddYears(-18))
                    contact.DateOfBirth = child.DateOfBirth;

                if (child.AdditionalInformation != null)
                    external.EventComments = child.AdditionalInformation;

                entity.SaveChanges();
            }
        }

        public void updateChildEnrolment(ChildEnrolment enrolment)
        {
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var childDetails = (from ce in entity.Events_RegistrationChildEnrolment
                                    where ce.ChildEnrolment_ID == enrolment.ChildEnrolment_ID
                                    select ce).FirstOrDefault();

                childDetails.ChildSchoolGrade_ID = enrolment.ChildSchoolGrade_ID;
                childDetails.ParentName = enrolment.ParentName;
                childDetails.ParentEmail = enrolment.ParentEmail;
                childDetails.ParentMobile = enrolment.ParentMobile;

                childDetails.EmergencyContactName = enrolment.EmergencyContactName;
                childDetails.EmergencyContactNumber = enrolment.EmergencyContactNumber;
                childDetails.EmergencyContactRelationship = enrolment.EmergencyContactRelationship;

                childDetails.EmergencyContactName2 = enrolment.EmergencyContactName2;
                childDetails.EmergencyContactNumber2 = enrolment.EmergencyContactNumber2;
                childDetails.EmergencyContactRelationship2 = enrolment.EmergencyContactRelationship2;

                childDetails.Comments = enrolment.Comments;

                if (enrolment.Verified)
                {
                    childDetails.SignedBy = enrolment.SignedBy;
                    childDetails.DateSigned = enrolment.DateSigned;
                } else
                {
                    childDetails.SignedBy = null;
                    childDetails.DateSigned = null;
                }

                entity.SaveChanges();
            }
        }

        public void updateChildEnrolmentAnswer(int childEnrolment_ID, int question_ID, int answer_ID, string answer)
        {
            var new_answer = new Events_ChildEnrolmentAnswer();

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                new_answer = (from ea in entity.Events_ChildEnrolmentAnswer
                              where ea.ChildEnrolment_ID == childEnrolment_ID
                              && ea.Question_ID == question_ID
                              && ea.Answer_ID == answer_ID
                              select ea).FirstOrDefault();

                if (new_answer != null)
                {
                    new_answer.Answer = answer;

                    entity.SaveChanges();
                }
            }
        }
    }

    public class Question
    {
        public int question_ID { get; set; }
        public int? sortOrder { get; set; }
        public string question { get; set; }
        public string type { get; set; }
        public bool required { get; set; }
        public int? answer_ID { get; set; }
        public string answer { get; set; }
    }
}