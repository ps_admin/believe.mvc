﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;


namespace Planetshakers.Events.Mvc.Models
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public string ErrorMessage { get; set; }
    }

    public class UpdateModel
    {
        public int ContactId { get; set; }
        #region Personal Details

        private int _salutation;
        private string _firstname;
        private string _lastname;
        //public DateTime _dob;
        private string _gender;
        private string _address1;
        private string _address2;
        private string _suburb;
        private int? _state;
        private string _postcode;
        private int _country;
        private string _mobile;
        private string _email;
        public DateTime? DoB { get; set; }

        [Required]
        [Display(Name = "Title")]
        public int Salutation {
            get { return _salutation; }
            set { _salutation = value; }
        }
        private SelectList _salutationList { get; set; }
        public SelectList SalutationList
        {
            get
            {
                if (_salutationList == null)
                {
                    _salutationList = new SelectList(Lists.getSalutations(), "Value", "Text");
                }
                return _salutationList;
            }
        }

        [Required]
        [Display(Name = "First Name")]
        public string Firstname {
            get { return String.IsNullOrEmpty(_firstname) ? "" : _firstname; }
            set { _firstname = value; }
        }

        [Required]
        [Display(Name = "Last Name")]
        public string Lastname {
            get { return String.IsNullOrEmpty(_lastname) ? "" : _lastname; }
            set { _lastname = value; } 
        }

        //[Display(Name = "Date of Birth")]
        //public DateTime DoB
        //{
        //    get
        //    {
        //        IFormatProvider culture = new System.Globalization.CultureInfo("en-AU", true);
        //        if (_dob != null)
        //        {
        //            return _dob;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    set
        //    {
        //        IFormatProvider culture = new System.Globalization.CultureInfo("en-AU", true);
        //        _dob = DateTime.Parse(value, culture);
        //    }
        //}


        [Display(Name = "Gender")]
        public string Gender {
            get { return _gender; }
            set { _gender = value; }
        }

        [Display(Name = "Address")]
        public string Address1 {
            get { return String.IsNullOrEmpty(_address1) ? "" : _address1; }
            set { _address1 = value; }
        }
        public string Address2
        {
            get { return String.IsNullOrEmpty(_address2) ? "" : _address2; }
            set { _address2 = value; }
        }

        [Required]
        [Display(Name = "Suburb / City")]
        public string Suburb {
            get { return String.IsNullOrEmpty(_suburb) ? "" : _suburb; }
            set { _suburb = value; }
        }
        
        [Required]
        [Display(Name = "State")]
        public int? State_ID {
            get { return _state; }
            set { _state = value; } 
        }

        [Required]
        [Display(Name = "Zip / Postcode")]
        public string Postcode {
            get { return _postcode; }
            set { _postcode = value; }
        }

        [Required]
        [Display(Name = "Country")]
        public int Country {
            get { return _country; }
            set { _country = value; }
        }
        private SelectList _countryList;
        public SelectList CountryList
        {
            get
            {
                if (_countryList == null)
                {
                    _countryList = new SelectList(Lists.getCountries(), "Value", "Text");
                }
                return _countryList;
            }
        }

        [Required]
        [Display(Name = "Mobile")]
        public string Mobile {
            get { return _mobile; }
            set { _mobile = value; }
        }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email {
            get { return _email; }
            set { _email = value; } 
        }

        public List<SelectListItem> StateList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Common_GeneralState
                                where c.Country_ID == 11
                                select new SelectListItem
                                {
                                    Text = c.State,
                                    Value = c.State_ID.ToString()
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> GenderList
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem { Text = "Male", Value = "Male" });
                list.Add(new SelectListItem { Text = "Female", Value = "Female" });
                return list;
            }
        }

        #endregion
        #region Church Details

        private string _churchName;
        private int _churchDenomination;
        private string _churchSeniorPastor;
        private string _churchLeader;
        private string _churchSize;
        private string _churchAddress1;
        private string _churchAddress2;
        private string _churchSuburb;
        private string _churchState;
        private string _churchPostcode;
        private int _churchCountry;
        private string _churchPhone;
        private string _churchWebsite;
        private string _churchEmail;

        [Display(Name = "Name")]
        public string ChurchName {
            get { return String.IsNullOrEmpty(_churchName) ? "" : _churchName; }
            set { _churchName = value; }
        }

        [Display(Name = "Denomination")]
        public int ChurchDenomination {
            get { return _churchDenomination; }
            set { _churchDenomination = value; }
        }
        private SelectList _churchDenominationList { get; set; }
        public SelectList ChurchDenominationList
        {
            get
            {
                if (_churchDenominationList == null)
                {
                    _churchDenominationList = new SelectList(Lists.getChurchDenominations(), "Value", "Text");
                }
                return _churchDenominationList;
            }
        }

        [Display(Name = "Senior Pastor's Name")]
        public string ChurchSeniorPastor {
            get { return String.IsNullOrEmpty(_churchSeniorPastor) ? "" : _churchSeniorPastor; }
            set { _churchSeniorPastor = value; }
        }

        [Display(Name = "Leader's Name")]
        public string ChurchLeader {
            get { return String.IsNullOrEmpty(_churchLeader) ? "" : _churchLeader; }
            set { _churchLeader = value; }
        }

        [Display(Name = "Congregation size")]
        public string ChurchSize {
            get { return String.IsNullOrEmpty(_churchSize) ? "" : _churchSize; }
            set { _churchSize = value; } 
        }

        [Display(Name = "Address")]
        public string ChurchAddress1 {
            get { return String.IsNullOrEmpty(_churchAddress1) ? "" : _churchAddress1; }
            set { _churchAddress1 = value; } 
        }
        public string ChurchAddress2 {
            get { return String.IsNullOrEmpty(_churchAddress2) ? "" : _churchAddress2; }
            set { _churchAddress2 = value; } 
        }

        [Display(Name = "Suburb")]
        public string ChurchSuburb {
            get { return String.IsNullOrEmpty(_churchSuburb) ? "" : _churchSuburb; }
            set { _churchSuburb = value; }
        }

        [Display(Name = "State")]
        public string ChurchState {
            get { return String.IsNullOrEmpty(_churchState) ? "" : _churchState; } 
            set {_churchState = value; }
        }

        [Display(Name = "Zip / Postcode")]
        public string ChurchPostcode {
            get { return String.IsNullOrEmpty(_churchPostcode) ? "" : _churchPostcode; }
            set { _churchPostcode = value; }
        }

        [Display(Name = "Country")]
        public int ChurchCountry {
            get { return _churchCountry; }
            set { _churchCountry = value; }
        }

        [Display(Name = "Phone")]
        public string ChurchPhone {
            get { return String.IsNullOrEmpty(_churchPhone) ? "" : _churchPhone; }
            set { _churchPhone = value; }
        }

        [Display(Name = "Website")]
        public string ChurchWebsite {
            get { return String.IsNullOrEmpty(_churchWebsite) ? "" : _churchWebsite; }
            set { _churchWebsite = value; }
        }

        [Display(Name = "Email")]
        public string ChurchEmail {
            get { return String.IsNullOrEmpty(_churchEmail)?"":_churchEmail; }
            set { _churchEmail = value; }
        }

        public List<ChurchInvolvement> AvailableCInvolvement { get; set; }
        public int[] SelectedCInvolvement { get; set; }

        #endregion
        #region Emergency Details

        public List<EmergencyContact> EmergencyContacts { get; set; }        

        #endregion
        #region Credential Details

        [Display(Name = "Current Password")]
        public string CurrentPassword { get; set; }
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }
        [Display(Name = "Confirm New Password")]
        public string ConfirmNewPassword { get; set; }

        #endregion

       
    }

    public class RegisterModel
    {
        public BasicDetailsPartialModel basicDetails { get; set; }
        public CredentialDetailsPartialModel credentialDetails { get; set; }
        public PersonalDetailsPartialModel personalDetails { get; set; }
        public ContactDetailsPartialModel contactDetails { get; set; }
        public ChurchDetailsPartialModel churchDetails { get; set; }
        public ChurchInvolvementPartialModel churchInvolvementDetails { get; set; }
        public EmergencyDetailsPartialModel emergencyDetails { get; set; }      
    }

    #region Partial Models

    public class CredentialDetailsPartialModel
    {
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("Email", ErrorMessage = "Emails do not match")]
        [Display(Name = "Confirm Email")]
        public string ConfirmEmail { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Password must be a minimum of 8 characters long", MinimumLength = 8)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords do not match")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
    }

    public class PersonalDetailsPartialModel
    {
        private int _salutation;
        private string _firstname;
        private string _lastname;
        public DateTime? _dob;
        private string _gender;
        private int _grade;

        [Required]
        [Display(Name = "Title")]
        public int Salutation
        {
            get { return _salutation; }
            set { _salutation = value; }
        }
        private SelectList _salutationList { get; set; }
        public SelectList SalutationList
        {
            get
            {
                if (_salutationList == null)
                {
                    _salutationList = new SelectList(Lists.getSalutations(), "Value", "Text");
                }
                return _salutationList;
            }
        }

        [Required]
        [Display(Name = "First Name")]
        public string Firstname
        {
            get { return String.IsNullOrEmpty(_firstname) ? "" : _firstname; }
            set { _firstname = value; }
        }

        [Required]
        [Display(Name = "Last Name")]
        public string Lastname
        {
            get { return String.IsNullOrEmpty(_lastname) ? "" : _lastname; }
            set { _lastname = value; }
        }

        [Required]
        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        public string DoB {
            get
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("en-AU", true);
                if (_dob != null)
                {
                    return ((DateTime)_dob).ToString("d", culture);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("en-AU", true);
                _dob = DateTime.Parse(value, culture);
            }
         }

        [Required]
        [Display(Name = "Gender")]
        public string Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
            }
        }
        private SelectList _genderList { get; set; }
        public SelectList GenderList
        {
            get
            {
                if (_genderList == null)
                {
                    _genderList = new SelectList(Lists.get_genders(), "Value", "Text");
                }
                return _genderList;
            }
        }

        [Display(Name="Grade")]
        public int Grade
        {
            get { return _grade; }
            set { _grade = value; }
        }
        private SelectList _gradeList { get; set; }
        public SelectList GradeList
        {
            get
            {
                if (_gradeList == null)
                {
                    _gradeList = new SelectList(Lists.getGrades(), "Value", "Text");
                }
                return _gradeList;
            }
        }
    }

    public class ContactDetailsPartialModel
    {
        private string _address1;
        private string _address2;
        private string _suburb;
        private string _state;
        private string _postcode;
        private int _country;
        private string _mobile;
        private string _email;

        [Display(Name = "Address")]
        public string Address1
        {
            get { return String.IsNullOrEmpty(_address1) ? "" : _address1; }
            set { _address1 = value; }
        }
        public string Address2
        {
            get { return String.IsNullOrEmpty(_address2) ? "" : _address2; }
            set { _address2 = value; }
        }

        [Required]
        [Display(Name = "Suburb / City")]
        public string Suburb
        {
            get { return String.IsNullOrEmpty(_suburb) ? "" : _suburb; }
            set { _suburb = value; }
        }

        [Required]
        [Display(Name = "State")]
        public string StateOther
        {
            get { return _state; }
            set { _state = value; }
        }

        [Required]
        [Display(Name = "Zip / Postcode")]
        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }

        [Required]
        [Display(Name = "Country")]
        public int Country
        {
            get { return _country; }
            set { _country = value; }
        }

        private SelectList _countryList;
        public SelectList CountryList 
        {
            get
            {
                if (_countryList == null)
                {
                    _countryList = new SelectList(Lists.getCountries(), "Value", "Text");
                }
                return _countryList;
            }
        }

        [Required]
        [Display(Name = "Mobile")]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
    }

    public class ChurchDetailsPartialModel
    {
        private string _churchName;
        private int _churchDenomination;
        private string _churchSeniorPastor;
        private string _churchLeader;
        private string _churchSize;
        private string _churchAddress1;
        private string _churchAddress2;
        private string _churchSuburb;
        private string _churchState;
        private string _churchPostcode;
        private int _churchCountry;
        private string _churchPhone;
        private string _churchWebsite;
        private string _churchEmail;

        [Display(Name = "Name")]
        public string ChurchName
        {
            get { return String.IsNullOrEmpty(_churchName) ? "" : _churchName; }
            set { _churchName = value; }
        }

        [Display(Name = "Denomination")]
        public int ChurchDenomination
        {
            get { return _churchDenomination; }
            set { _churchDenomination = value; }
        }
        private SelectList _churchDenominationList { get; set; }
        public SelectList ChurchDenominationList
        {
            get
            {
                if (_churchDenominationList == null)
                {
                    _churchDenominationList = new SelectList(Lists.getChurchDenominations(), "Value", "Text");
                }
                return _churchDenominationList;
            }
        }

        [Display(Name = "Senior Pastor's Name")]
        public string ChurchSeniorPastor
        {
            get { return String.IsNullOrEmpty(_churchSeniorPastor) ? "" : _churchSeniorPastor; }
            set { _churchSeniorPastor = value; }
        }

        [Display(Name = "Leader's Name")]
        public string ChurchLeader
        {
            get { return String.IsNullOrEmpty(_churchLeader) ? "" : _churchLeader; }
            set { _churchLeader = value; }
        }

        [Display(Name = "Congregation size")]
        public string ChurchSize
        {
            get { return String.IsNullOrEmpty(_churchSize) ? "" : _churchSize; }
            set { _churchSize = value; }
        }

        [Display(Name = "Address")]
        public string ChurchAddress1
        {
            get { return String.IsNullOrEmpty(_churchAddress1) ? "" : _churchAddress1; }
            set { _churchAddress1 = value; }
        }
        public string ChurchAddress2
        {
            get { return String.IsNullOrEmpty(_churchAddress2) ? "" : _churchAddress2; }
            set { _churchAddress2 = value; }
        }

        [Display(Name = "Suburb")]
        public string ChurchSuburb
        {
            get { return String.IsNullOrEmpty(_churchSuburb) ? "" : _churchSuburb; }
            set { _churchSuburb = value; }
        }

        [Display(Name = "State")]
        public string ChurchState
        {
            get { return String.IsNullOrEmpty(_churchState) ? "" : _churchState; }
            set { _churchState = value; }
        }

        [Display(Name = "Zip / Postcode")]
        public string ChurchPostcode
        {
            get { return String.IsNullOrEmpty(_churchPostcode) ? "" : _churchPostcode; }
            set { _churchPostcode = value; }
        }

        [Display(Name = "Country")]
        public int ChurchCountry
        {
            get { return _churchCountry; }
            set { _churchCountry = value; }
        }
        private SelectList _countryList;
        public SelectList CountryList
        {
            get
            {
                if (_countryList == null)
                {
                    _countryList = new SelectList(Lists.getCountries(), "Value", "Text");
                }
                return _countryList;
            }
        }

        [Display(Name = "Phone")]
        public string ChurchPhone
        {
            get { return String.IsNullOrEmpty(_churchPhone) ? "" : _churchPhone; }
            set { _churchPhone = value; }
        }

        [Display(Name = "Website")]
        public string ChurchWebsite
        {
            get { return String.IsNullOrEmpty(_churchWebsite) ? "" : _churchWebsite; }
            set { _churchWebsite = value; }
        }

        [Display(Name = "Email")]
        public string ChurchEmail
        {
            get { return String.IsNullOrEmpty(_churchEmail) ? "" : _churchEmail; }
            set { _churchEmail = value; }
        }

    }

    public class ChurchInvolvementPartialModel
    {
        public List<ChurchInvolvement> AvailableCInvolvement { get; set; }
        public int[] SelectedCInvolvement { get; set; }
    }

    public class EmergencyDetailsPartialModel
    {
        private string _emergencyContactName;
        private string _emergencyContactPhone;
        private string _emergencyContactRelationship;
        private string _medicalInfo;
        private string _medicalAllergies;

        [Display(Name = "Emergency Contact Name")]
        public string EmergencyContactName
        {
            get
            {
                return String.IsNullOrEmpty(_emergencyContactName) ?
                       "" : _emergencyContactName;
            }
            set { _emergencyContactName = value; }
        }

        [Display(Name = "Emergency Contact Phone")]
        public string EmergencyContactPhone
        {
            get
            {
                return String.IsNullOrEmpty(_emergencyContactPhone) ?
                       "" : _emergencyContactPhone;
            }
            set { _emergencyContactPhone = value; }
        }

        [Display(Name = "Emergency Contact Relationship")]
        public string EmergencyContactRelationship
        {
            get
            {
                return String.IsNullOrEmpty(_emergencyContactRelationship) ?
                        "" : _emergencyContactRelationship;
            }
            set { _emergencyContactRelationship = value; }
        }

        [Display(Name = "Special Requirement (food alergies, vegetarian, etc.)")]
        public string MedicalInfo
        {
            get { return String.IsNullOrEmpty(_medicalInfo) ? "" : _medicalInfo; }
            set { _medicalInfo = value; }
        }

        [Display(Name = "Medical Allergies")]
        public string MedicalAllergies
        {
            get
            {
                return String.IsNullOrEmpty(_medicalAllergies) ?
                       "" : _medicalAllergies;
            }
            set { _medicalAllergies = value; }
        }
    }

    public class BasicDetailsPartialModel
    {
        private string _firstname;
        private string _lastname;
        public DateTime? _dob;
        private string _email;
        private string _mobile;
        private int _country;
        private string _suburb;
        private string _postcode;
        private string _church;
        private int _grade;

        [Required]
        [Display(Name = "First Name")]
        public string Firstname
        {
            get { return String.IsNullOrEmpty(_firstname) ? "" : _firstname; }
            set { _firstname = value; }
        }

        [Required]
        [Display(Name = "Last Name")]
        public string Lastname
        {
            get { return String.IsNullOrEmpty(_lastname) ? "" : _lastname; }
            set { _lastname = value; }
        }

        [Display(Name = "Date of Birth")]
        public string DoB
        {
            get
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("en-AU", true);
                if (_dob != null)
                {
                    return ((DateTime)_dob).ToString("d", culture);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    IFormatProvider culture = new System.Globalization.CultureInfo("en-AU", true);
                    _dob = DateTime.Parse(value, culture);
                }
            }
        }
        
        [Display(Name = "Grade")]
        public int Grade
        {
            get { return _grade; }
            set { _grade = value; }
        }
        private SelectList _gradeList { get; set; }
        public SelectList GradeList
        {
            get
            {
                if (_gradeList == null)
                {
                    _gradeList = new SelectList(Lists.getGrades(), "Value", "Text");
                }
                return _gradeList;
            }
        }

        [Display(Name = "Suburb / City")]
        public string Suburb
        {
            get { return String.IsNullOrEmpty(_suburb) ? "" : _suburb; }
            set { _suburb = value; }
        }

        [Display(Name = "Postcode")]
        public string Postcode
        {
            get { return String.IsNullOrEmpty(_postcode) ? "" : _postcode; }
            set { _postcode = value; }
        }

        [Display(Name = "Church")]
        public string Church
        {
            get { return String.IsNullOrEmpty(_church) ? "" : _church; }
            set { _church = value; }
        }

        [Required]
        [Display(Name = "Country")]
        public int Country
        {
            get { return _country; }
            set { _country = value; }
        }
        private SelectList _countryList;
        public SelectList CountryList
        {
            get
            {
                if (_countryList == null)
                {
                    _countryList = new SelectList(Lists.getCountries(), "Value", "Text");
                }
                return _countryList;
            }
        }

        [Required]
        [Display(Name = "Mobile")]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
    }

    #endregion
}
