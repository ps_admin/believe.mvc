﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Mvc.Models
{
    public class AddOnModel
    {
        public int Event_ID
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    return (from c in context.Events_Venue
                            where c.Venue_ID == Venue_ID
                            select c.Conference_ID).FirstOrDefault();
                }
            }
        }

        public int Venue_ID { get; set; }
        public int Contact_ID { get; set; }
        //list of add on rego types that has been allocated to the contact
        public List<AddOn> AddOnList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    //var today = DateTime.Now;
                    //var list = (from rt in context.Events_RegistrationType
                    //            where rt.Conference_ID == Event_ID && rt.AddOnRegistrationType == true && rt.StartDate <= today && rt.EndDate > today
                    //            select new AddOn
                    //            {
                    //                RegistrationType = rt.RegistrationType,
                    //                RegistrationType_ID = rt.RegistrationType_ID,
                    //                Cost = rt.RegistrationCost,
                    //                registered = (from r in context.Events_Registration where r.Contact_ID == Contact_ID && r.RegistrationType_ID == rt.RegistrationType_ID select r).Any()
                    //            }).ToList();
                    //return list;

                    var list = (from r in context.Events_Registration
                                join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                where r.Contact_ID == Contact_ID && rt.AddOnRegistrationType == true && rt.Conference_ID == Event_ID
                                select new AddOn
                                {
                                    RegistrationType = rt.RegistrationType,
                                    RegistrationType_ID = rt.RegistrationType_ID,
                                    Cost = rt.RegistrationCost
                                }).ToList();
                    var paymentList = (from gp in context.Events_GroupPayment
                                       where gp.Conference_ID == Event_ID && gp.GroupLeader_ID == Contact_ID
                                       select gp).ToList();
                    foreach (var addon in list)
                    {
                        addon.registered = paymentList.Where(x => x.PaymentAmount == addon.Cost && x.Comment == "Payment for Add On: " + addon.RegistrationType).Any();
                    }
                    return list;
                }
            }
        }
        public bool AddRegistration(int RegistrationType_ID)
        {
            return true;
        }

    }
    public class AddOn
    {
        public int RegistrationType_ID { get; set; }
        public decimal Cost { get; set; }
        public string RegistrationType { get; set; }
        public bool registered { get; set; }

    }

}