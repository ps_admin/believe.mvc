﻿using Planetshakers.Events.Mvc.Authenticator;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Planetshakers.Events.Mvc.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class PSAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthenticated = base.AuthorizeCore(httpContext);
            if (isAuthenticated) 
            {
                string cookieName = FormsAuthentication.FormsCookieName;
                var contact = (Contact)httpContext.Session["Contact"];

                if (!httpContext.User.Identity.IsAuthenticated ||
                    httpContext.Request.Cookies == null || 
                    httpContext.Request.Cookies[cookieName] == null ||
                    contact == null)
                {
                    FormsAuthentication.SignOut();
                    httpContext.Session["Contact"] = null;
                    return false;
                }

                /* Not used (this is initially for single sign on, but doesn't work)
                var authCookie = httpContext.Request.Cookies[cookieName];
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                // This is where you can read the userData part of the authentication
                // cookie and fetch the token
                string webServiceToken = authTicket.UserData;

                AuthenticatorClient authenticator = new AuthenticatorClient();

                if (!authenticator.ValidateToken(contact.Email, webServiceToken))
                {
                    // Since account is logged in, but web service token has expired, log user out
                    FormsAuthentication.SignOut();
                    httpContext.Session["Contact"] = null;
                    return false;
                } 
                */
            }
            return isAuthenticated;
        }
    }
}