﻿$(document).ready(function () {
    // Enables the footable addon
    $('.footable').footable();

    // Initialise the datetime picker
    $('.dob-field').datetimepicker({
        format: "Y-m-d",
        timepicker: false,
        maxDate: 0,
        scrollInput: false
    });

    InitialiseBindings();

    $('#manual_1').show();
    $('#manual_2').hide();
    $('#manual_3').hide();
    $('#manual_4').hide();
})

function InitialiseBindings() {    // Manual allocations
    $('#manual_1 .next-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_1').hide();
        $('#manual_2').show('slide', { direction: 'right' });
    });
    $('#manual_2 .next-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_2').hide();
        $('#manual_3').show('slide', { direction: 'right' });
    });
    $('#manual_3 .next-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_3').hide();
        $('#manual_4').show('slide', { direction: 'right' });
    });
    $('#manual_2 .prev-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_2').hide();
        $('#manual_1').show('slide', { direction: 'left' });
    });
    $('#manual_3 .prev-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_3').hide();
        $('#manual_2').show('slide', { direction: 'left' });
    });
    $('#manual_4 .prev-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_4').hide();
        $('#manual_3').show('slide', { direction: 'left' });
    });
}