﻿$(document).ready(function () {
    // Enables the footable addon
    $('.footable').footable();

    // Initialise the datetime picker
    $('.dob-field').datetimepicker({
        format: "Y-m-d",
        timepicker: false,
        maxDate: 0,
        scrollInput: false
    });

    if ($('#medicalNeed').prop("checked") || $('allergyNeed').prop("checked"))
        $('#medicalForm').show();
    else
        $('#medicalForm').hide();

    if ($('#additionalNeed').prop("checked"))
        $('#additionalForm').show();
    else
        $('#additionalForm').hide();

    if ($('#nappyNeed').prop("checked"))
        $('#nappyForm').show();
    else
        $('#nappyForm').hide();

});

$('#medicalNeed').click(function () {
    if ($(this).prop("checked") || $('allergyNeed').prop("checked"))
        $('#medicalForm').show();
    else
        $('#medicalForm').hide();
})

$('#allergyNeed').click(function () {
    if ($(this).prop("checked") || $('#medicalNeed').prop("checked"))
        $('#medicalForm').show();
    else
        $('#medicalForm').hide();
})

$('#additionalNeed').click(function () {
    if ($(this).prop("checked"))
        $('#additionalForm').show();
    else
        $('#additionalForm').hide();
})

$('#nappyNeed').click(function () {
    if ($(this).prop("checked"))
        $('#nappyForm').show();
    else
        $('#nappyForm').hide();
})