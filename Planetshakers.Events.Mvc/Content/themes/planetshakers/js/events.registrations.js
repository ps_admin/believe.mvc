﻿﻿$(document).ready(function ()
{
    // Enables the footable addon
    $('.footable').footable();
     

    // Initialise the datetime picker
    $('.dob-field').datetimepicker({
        format: "Y-m-d",
        timepicker: false,
        maxDate: 0,
        scrollInput: false
    });

    //TakeAction();
    InitialiseBindings();
    
 });

//make sure the newer modal has the backdrop background covering the older modal
$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function () {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

function InitialiseBindings()
{
    // Handles unallocation of registrations
    $('.unallocate-rego-btn').off().on('click', function (event)
    {
        event.preventDefault();

        // make sure this event is triggered only once
        $(this).off();

        // retrieve registration id
        var rego_id = $(this).attr('id').replace("RemoveButton_", "");

        // Remove entry from database
        var data = new Object();
        data.registrationID = rego_id;
        data = JSON.stringify(data);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Account/Unallocate',
            data: data,
            dataType: 'json',
            success: onUnallocateRegistrationCallback
        });
    });

    $('.allocate-self-btn').off().on('click', function (event)
    {
        event.preventDefault();

        // make sure this event is triggered only once
        $(this).off();        

        // retreive registration id
        var rego_id = $(this).attr('id').replace("AllocateSelf_", "");

        // allocate rego to self
        var data = new Object();
        data.registrationTypeID = rego_id;
        data = JSON.stringify(data);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Account/AllocateSelf',
            data: data,
            dataType: 'json',
            success: onAllocateSelfCallback,
            beforeSend: function () {
                $("#processing").show();
            },
            complete: function () {
                $("#processing").hide();
            }
        });
    });

    $('#submit_invitation').off().on('click',function (event)
    {
        event.preventDefault();

        // make sure this event is triggered only once
        $(this).off();

        $(this).attr('disabled', 'disabled');
        $('#submit-invi-btn-section').hide();
        $('#invitation_notification').hide();
        $('#invitation_info').show();
        $('#loading_send').show();

        $('#sending_invitation').removeAttr('style');

        var rego_id = $("[name='registrationTypeID']").val();
        var group_leader_id = $("#group_leader_id").val();
        var email = $("#invitation_email").val();

        var data = new Object();
        data.email = email;
        data.group_leader_id = group_leader_id;
        data.registration_type_id = rego_id;
        data = JSON.stringify(data);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Invitation/SendInvitation',
            data: data,
            dataType: 'json',
            success: onInvitationCallback
        });

        var valid = result.isSuccess;

        if (valid) {
            return true;
        }
        else {
            $(this).removeAttr('disabled');
            $('loading').hide();
            return false;
        }
    });

    $('#submit_allocation').off().on('click', function (event)
    {
        event.preventDefault();

        // make sure this event is triggered only once
        $(this).off();

        $(this).attr('disabled', 'disabled');
        $('#allocation_info').text('Allocating...');
        $('#submit-alloc-btn-section').hide();
        $('#loading_allocate').show();

        $('#allocation_info').removeAttr('style');

        var rego_id = $("[name='registrationTypeID']").val();
        var group_leader_id = $("#group_leader_id").val();

        var selected = $("#selected_allocation").val();

        var data = new Object();
        data.selected = selected;
        data.group_leader_id = group_leader_id;
        data.registration_type_id = rego_id;
        data = JSON.stringify(data);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Invitation/AllocatePrevious',
            data: data,
            dataType: 'json',
            success: onAllocateCallback
        });

        var valid = result.isSuccess;

        if (valid) {
            return true;
        }
        else {
            $(this).removeAttr('disabled');
            $('loading').hide();
            return false;
        }
    });


    $('.revoke-invitation-btn').off().on('click', function (event) {
        event.preventDefault();

        // make sure this event is triggered only once
        $(this).off();

        var inv_id = $(this).attr('id').replace("RevokeInvite_", "");

        // Remove invitation from database
        var data = new Object();
        data.invitation_id = inv_id;
        data = JSON.stringify(data);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Invitation/RevokeInvitation',
            data: data,
            dataType: 'json',
            success: onRevokeInvitationCallback
        });

    });

    $('.change-rego-select').off().on('change', function (event) {
        var registration_id = $(this).attr('id').replace("ChangeRego_", "");
        var new_type = $(this).val();

        console.log(registration_id + " - " + new_type);
        var data = new Object();
        data.registrationID = registration_id;
        data.newRegistrationTypeID = new_type;
        data = JSON.stringify(data);

        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/Account/ChangeRegistrationType',
            data: data,
            dataType: 'json',
            success: onChangeRegoCallback
        });
    });

    // =================================================================================
    // Allocation Modal UI controls
    // =================================================================================

    // Handle allocations
    $('.allocate-rego-btn').on('click', function (e) {
        e.preventDefault();

        // Retrieve the registration type id
        var rego_type_id = $(this).attr('id').replace("Allocate_", "");
        $("[name='registrationTypeID']").val(rego_type_id);

        // Hide tabs
        $('#allocate_previous').hide();
        $('#email_invitation').hide();
        $('#manual_allocation').hide();
        $('#kid_allocation').hide();

        // Clear forms
        clearAllocateForm();
        clearEmailForm();
        clearManualForm();
        clearKidForm();

        // Open modal
        $('#modal_allocation').modal('show');
    });

    $('#toggle_allocate_previous').on('click', function (e) {
        e.preventDefault();
        $('#toggle_allocate_previous').addClass('active');
        $('#toggle_email_invitation').removeClass('active');
        $('#toggle_manual_allocation').removeClass('active');
        $('#toggle_kid_allocation').removeClass('active');
        $('#allocate_previous').show('blind');
        $('#email_invitation').hide();
        $('#manual_allocation').hide();
        $('#kid_allocation').hide();

        $('#allocate_alert').hide();
        $('#manual_alert').hide();
        $('#kid_alert').hide();
        $('#invitation_notification').hide();
        $('#invitation_alert').hide();
    });

    $('#toggle_email_invitation').on('click', function (e) {
        e.preventDefault();
        $('#toggle_allocate_previous').removeClass('active');
        $('#toggle_email_invitation').addClass('active');
        $('#toggle_manual_allocation').removeClass('active');
        $('#toggle_kid_allocation').removeClass('active');
        $('#allocate_previous').hide();
        $('#email_invitation').show('blind');
        $('#manual_allocation').hide();
        $('#kid_allocation').hide();
                
        $('#allocate_alert').hide();
        $('#manual_alert').hide();
        $('#kid_alert').hide();
        $('#invitation_notification').hide();
        $('#invitation_alert').hide();
    });
    
    $('#toggle_manual_allocation').on('click', function (e) {
        e.preventDefault();
        $('#toggle_allocate_previous').removeClass('active');
        $('#toggle_email_invitation').removeClass('active');
        $('#toggle_manual_allocation').addClass('active');
        $('#toggle_kid_allocation').removeClass('active');
        $('#allocate_previous').hide();
        $('#email_invitation').hide();
        $('#manual_allocation').show('blind');
        $('#kid_allocation').hide();

        $('#manual_1').show();
        $('#manual_2').hide();
        $('#manual_3').hide();

        $('#allocate_alert').hide();
        $('#manual_alert').hide();
        $('#kid_alert').hide();
        $('#invitation_notification').hide();
        $('#invitation_alert').hide();
    });

    $('#toggle_kid_allocation').on('click', function (e) {
        e.preventDefault();
        $('#toggle_allocate_previous').removeClass('active');
        $('#toggle_email_invitation').removeClass('active');
        $('#toggle_manual_allocation').removeClass('active');
        $('#toggle_kid_allocation').addClass('active');
        $('#allocate_previous').hide();
        $('#email_invitation').hide();
        $('#manual_allocation').hide();
        $('#kid_allocation').show('blind');

        $('#kids_1').show();
        $('#kids_2').hide();
        $('#kids_3').hide();

        $('#allocate_alert').hide();
        $('#manual_alert').hide();
        $('#kid_alert').hide();
        $('#invitation_notification').hide();
        $('#invitation_alert').hide();
    });

    // Manual allocations
    $('#manual_1 .next-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_1').hide();
        $('#manual_2').show('slide', { direction: 'right' });
    });
    $('#manual_2 .next-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_2').hide();
        $('#manual_3').show('slide', { direction: 'right' });
    });
    $('#manual_2 .prev-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_2').hide();
        $('#manual_1').show('slide', { direction: 'left' });
    });
    $('#manual_3 .prev-btn').on('click', function (e) {
        e.preventDefault();
        $('#manual_3').hide();
        $('#manual_2').show('slide', { direction: 'left' });
    });

    // Kid allocations
    $('#kids_1 .next-btn').on('click', function (e) {
        e.preventDefault();
        $('#kids_1').hide();
        $('#kids_2').show('slide', { direction: 'right' });
    });
    $('#kids_2 .next-btn').on('click', function (e) {
        e.preventDefault();
        $('#kids_2').hide();
        $('#kids_3').show('slide', { direction: 'right' });
    });
    $('#kids_2 .prev-btn').on('click', function (e) {
        e.preventDefault();
        $('#kids_2').hide();
        $('#kids_1').show('slide', { direction: 'left' });
    });
    $('#kids_3 .prev-btn').on('click', function (e) {
        e.preventDefault();
        $('#kids_3').hide();
        $('#kids_2').show('slide', { direction: 'left' });
    });

    // Toggle Buttons
    $('.rego-toggle-btn').on('click', function (e) {
        $('.rego-toggle-btn').addClass('active');
        $('.alloc-toggle-btn').removeClass('active');
        $('.unalloc-toggle-btn').removeClass('active');
        $('.kids-toggle-btn').removeClass('active');

        $('#rego').show('blind');
        $('#allocated').hide('blind');
        $('#unallocated').hide('blind');
        $('#invitations').hide('blind');
        $('#enrolments').hide('blind');

        $('.footable').trigger('footable_resize');
        $('.footable').trigger('footable_redraw'); //force a redraw

    });
    $('.alloc-toggle-btn').on('click', function (e) {
        $('.rego-toggle-btn').removeClass('active');
        $('.alloc-toggle-btn').addClass('active');
        $('.unalloc-toggle-btn').removeClass('active');
        $('.kids-toggle-btn').removeClass('active');

        $('#rego').hide('blind');
        $('#allocated').show('blind');
        $('#unallocated').hide('blind');
        $('#invitations').hide('blind');
        $('#enrolments').hide('blind');

        $('.footable').trigger('footable_resize');
        $('.footable').trigger('footable_redraw'); //force a redraw
    });
    $('.unalloc-toggle-btn').on('click', function (e) {
        $('.rego-toggle-btn').removeClass('active');
        $('.alloc-toggle-btn').removeClass('active');
        $('.unalloc-toggle-btn').addClass('active');
        $('.kids-toggle-btn').removeClass('active');
        

        $('#rego').hide('blind');
        $('#allocated').hide('blind');
        $('#unallocated').show('blind');
        $('#invitations').show('blind');
        $('#enrolments').hide('blind');

        $('.footable').trigger('footable_resize');
        $('.footable').trigger('footable_redraw'); //force a redraw
    });

    $('.youth-toggle-btn').on('click', function (e) {
        $('.rego-toggle-btn').removeClass('active');
        $('.alloc-toggle-btn').removeClass('active');
        $('.unalloc-toggle-btn').removeClass('active');
        $('.youth-toggle-btn').addClass('active');
        $('.kids-toggle-btn').removeClass('active');

        $('#rego').hide('blind');
        $('#allocated').hide('blind');
        $('#unallocated').hide('blind');
        $('#invitations').hide('blind');
        $('#youthmedical').show('blind');
        $('#enrolments').hide('blind');

        $('.footable').trigger('footable_resize');
        $('.footable').trigger('footable_redraw'); //force a redraw
    });

    $('.kids-toggle-btn').on('click', function (e) {
        $('.rego-toggle-btn').removeClass('active');
        $('.alloc-toggle-btn').removeClass('active');
        $('.unalloc-toggle-btn').removeClass('active');
        $('.kids-toggle-btn').addClass('active');

        $('#rego').hide('blind');
        $('#allocated').hide('blind');
        $('#unallocated').hide('blind');
        $('#invitations').hide('blind');
        $('#youthmedical').hide('blind');
        $('#enrolments').show('blind');

        $('.footable').trigger('footable_resize');
        $('.footable').trigger('footable_redraw'); //force a redraw
    });

    // Script to smoothly go to a specific location on the page.
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });
 }

function clearAllocateForm() {
    $('#allocate_previous').find("input[type=text]").val("");
}

function clearEmailForm()
{
    $('#email_invitation').find("input[type=text]").val("");
}

function clearManualForm()
{
    // Clear text and textarea fields
    $('#manual_allocation').find("input[type=text], textarea").val("");
    // Clear checkboxes
    $('#manual_allocation').find("input[type=checkbox]").removeAttr('checked');
    // Reset select dropdowns
    $('#manual_allocation').find("select").prop('selectedIndex', 0);
}

function clearKidForm()
{
    // Clear text and textarea fields
    $('#kid_allocation').find("input[type=text], textarea").val("");
    // Clear checkboxes
    $('#kid_allocation').find("input[type=checkbox]").removeAttr('checked');
    // Reset select dropdowns
    $('#kid_allocation').find("select").prop('selectedIndex', 0);
}

function onInvitationCallback(result)
{
    if (result.isSuccess)
    {
        //$('#sending_invitation').hide();
        //$('#invitation_alert').hide();

        $('#invitation_info').hide();

        $('#invitation_notification').text(result.message);
        $('#invitation_notification').show();

        $('#loading_send').hide();
        $('#submit-invi-btn-section').show();
        $('#submit_invitation').removeAttr('disabled');

        updateUI(result);
        clearEmailForm();
        //$('#modal_allocation').foundation('reveal', 'close');        
    }
    else 
    {
        $('#invitation_notification').hide();
        $('#invitation_alert').show();
        $('#invitation_alert').text(result.message);
    }
    InitialiseBindings();
}

function onAllocate()
{
    $(this).off();
    $(this).attr('disabled', 'disabled');

    $('#manual_info').text('Allocating...');
    $('#submit-manual-btn-section').hide();
    $('#loading_manual').show();

    $('#manual_info').removeAttr('style');
}

function onAllocateCallback(result)
{
    if (result.isSuccess) {
        $('#allocation_info').hide();

        $('#allocation_success').text(result.message);
        $('#allocation_success').show();


        $('#loading_allocate').hide();

        $('#submit-alloc-btn-section').show();
        $('#submit_allocation').removeAttr('disabled');

        updateUI(result);
        clearManualForm();
    }
    else
    {
        $('#register_alert').show();
        $('#register_alert').text(result.message);
        // error message
    }
    InitialiseBindings();
}

function onAllocateManualCallback(result) {
    if (result.isSuccess) {
        $('#manual_info').hide();

        $('#manual_notification').text(result.message);
        $('#manual_notification').show();

        $('#loading_manual').hide();

        $('#submit-manual-btn-section').show();
        $('#submit_manual').removeAttr('disabled');

        updateUI(result);
        clearManualForm();
    }
    else {
        $('#register_alert').show();
        $('#register_alert').text(result.message);
        // error message
    }
    InitialiseBindings();
}
function showLoadingKidAllocation() {
    $("#kid_info").show();
    $("#loading").show();
    $("#submitKidAllocationBtn").hide();
}
function onAllocateKidCallback(result)
{
    if (result.isSuccess) {
        //$('#modal_allocation').foundation('reveal', 'close');
        $('#kid_info').hide();
        $('#kid_notification').text(result.message);
        $('#kid_notification').show();
        $('#loading').hide();
        $("#submitKidAllocationBtn").show();

        updateUI(result);
        clearKidForm();
        //$('#modal_allocation').foundation('reveal', 'close');
    }
    else
    {
        // error message
        $('#kid_alert').show();
        $('#kid_alert').text(result.message);
    }
    InitialiseBindings();
}

function onAllocateSelfCallback(result)
{
    if (result.isSuccess)
    {
        updateUI(result);
    }
    else
    {
        alert("failed");
    }
    InitialiseBindings();
}

function onUnallocateRegistrationCallback(result) {
    
    if (result.isSuccess === true) {        
        updateUI(result);
    } else {
        alert("failed");
    }
    InitialiseBindings();
}

function onChangeRegoCallback(result) {

    if (result.isSuccess === true) {
        updateUI(result);
    } else {
        alert("failed");
    }
    InitialiseBindings();
}

function onRevokeInvitationCallback(result) {

    if (result.isSuccess) {
        updateUI(result);
    }
    else 
    {
        alert(result.message);
    }
    InitialiseBindings();
}

function updateUI (result) {
    if (result.isSuccess)
    {
        updateSingleRegistrations(result.curRegos);
        updateBulkRegistrations(result.bulkRegos);
        updateAllocatedRows(result.allocated, result.allocatedKids);
        updateUnallocatedRows(result.unallocated, result.curRegos);
        updateRegisteredAlertBox(result.curRegos);
        updateAllocatedAlertBox(result.allocatedQty);
        updateUnallocatedAlertBox(result.unallocatedQty);
        updateInvitationRows(result.invites);
    }
}

function updateRegisteredAlertBox(curRegos)
{
    // Only update if there are changes
    if (curRegos === null) return;
    if (curRegos.length > 0) {
        $('#box-registered a').addClass('event-alert-positive');
        $('#box-registered a').removeClass('event-alert-negative');
        $('#box-registered i').removeClass('fa-exclamation-circle');
        $('#box-registered i').addClass('fa-ticket-alt');
        $('#box-registered-text').text("You are registered");
    }
    else {
        $('#box-registered a').removeClass('event-alert-positive');
        $('#box-registered a').addClass('event-alert-negative');
        $('#box-registered i').addClass('fa-exclamation-circle');
        $('#box-registered i').removeClass('fa-ticket-alt');
        $("#box-registered-text").text("You are not registered");
    }
}

function updateUnallocatedAlertBox(unallocatedQty)
{
    // Only update if there are changes
    if (unallocatedQty === null) return;

    if (unallocatedQty <= 0)
    {
        $('#box-unallocated a').addClass('event-alert-positive');
        $('#box-unallocated a').removeClass('event-alert-negative');
        $('#box-unallocated i').removeClass('fa-exclamation-circle');
        $('#box-unallocated i').addClass('fa-user');
        $('#box-unallocated #allocation-title').text("All registrations allocated!");
    }
    else
    {
        $('#box-unallocated a').removeClass('event-alert-positive');
        $('#box-unallocated a').addClass('event-alert-negative');
        $('#box-unallocated i').addClass('fa-exclamation-circle');
        $('#box-unallocated i').removeClass('fa-user');
        $('#box-unallocated #allocation-title').text(unallocatedQty + " Unallocated registrations");
    }
}

function updateAllocatedAlertBox(allocatedQty)
{
    // Only update if there are changes
    if (allocatedQty === null) return;

    if (allocatedQty <= 0) {
        $('#box-allocated a').removeClass('event-alert-positive');
        $('#box-allocated a').addClass('event-alert-negative');
        $('#box-allocated i').addClass('fa-exclamation-circle');
        $('#box-allocated i').removeClass('fa-users');
        $('#box-allocated #allocated-title').text("0 Registrations allocated");
    }
    else {
        $('#box-allocated a').addClass('event-alert-positive');
        $('#box-allocated a').removeClass('event-alert-negative');
        $('#box-allocated i').removeClass('fa-exclamation-circle');
        $('#box-allocated i').addClass('fa-users');
        $('#box-allocated #allocated-title').text(allocatedQty + " Registrations allocated");
    }
}

function updateSingleRegistrations(curRegos)
{
    // Only update if there are changes
    if (curRegos === null) return;

    if (curRegos.length > 0)
    {
        // Clear the table
        $('#single-reg-table tbody').empty();

        for (i = 0; i < curRegos.length; i++)
        {
            var row = '';
            row += "<tr>";
            row += "    <td>" + curRegos[i].registration_id + "</td>";

            if (curRegos[i].registration_type_id === 1521 ||
                curRegos[i].registration_type_id === 1522 ||
                curRegos[i].registration_type_id === 1523 ||
                curRegos[i].registration_type_id === 1524) {
                row += "<td>";
                row += '    <select class="change-rego-select" id="ChangeRego_' + curRegos[i].registration_id + '">';
                if (curRegos[i].registration_type_id === 1521) {
                    row += '        <option value="1521" selected>Adult PS16</option>';
                } else {
                    row += '        <option value="1521">Adult PS16</option>';
                }
                if (curRegos[i].registration_type_id === 1522) {
                    row += '        <option value="1522" selected>Youth PS16</option>';
                } else {
                    row += '        <option value="1522">Youth PS16</option>';
                }
                if (curRegos[i].registration_type_id === 1523) {
                    row += '        <option value="1523" selected>Child (Primary) PS16</option>';
                } else {
                    row += '        <option value="1523">Child (Primary) PS16</option>';
                }
                if (curRegos[i].registration_type_id === 1524) {
                    row += '        <option value="1524" selected>Child (Early Childhood) PS16</option>';
                } else {
                    row += '        <option value="1524">Child (Early Childhood) PS16</option>';
                }
                row += '    </select>';
                row += "</td>";
            }
            else {
                row += "<td>"+ curRegos[i].group_leader +"</td>"; 
            }
            row += "    <td>" + curRegos[i].registration_name + "</td>";
            row += "    <td>" + moment(curRegos[i].registration_date).format("DD/MM/YYYY") + "</td>";
            row += "    <td>" + curRegos[i].currency + " " + curRegos[i].cost_total + "</td>";
            row += "</tr>";
            $('#single-reg-table tbody').append(row);
        }

        $('#single-reg-table').trigger('footable_initialized');
        $('#single-reg-table').trigger('footable_resize');
        $('#single-reg-table').trigger('footable_redraw');

        $('#single_registrations').show();
    }
    else 
    {
        $('#single_registrations').hide();
    }
}

function updateBulkRegistrations(bulkRegos)
{
    // Only update if there are changes
    if (bulkRegos === null) return;

    if ($(bulkRegos).length > 0)
    {
        // Clear the table
        $('#bulk-rego-table tbody').empty();

        for (i = 0; i < bulkRegos.length; i++)
        {
            var row = '';
            row += "<tr>";
            row += "    <td>" + bulkRegos[i].registration_id + "</td>";
            row += "    <td>" + bulkRegos[i].event_name + " - " + bulkRegos[i].registration_name + "</td>";
            row += "    <td>" + bulkRegos[i].quantity + "</td>";
            row += "    <td>" + moment(bulkRegos[i].registration_date).format("DD/MM/YYYY") + "</td>";
            row += "    <td>" + bulkRegos[i].currency + " " + bulkRegos[i].cost_total + "</td>";
            row += "</tr>";

            $('#bulk-rego-table tbody').append(row);
        }
    }
    else 
    {
        $('#bulk-rego-table').hide();
    }
}

function updateAllocatedRows(allocated, allocatedKids)
{
    // Only update if there are changes
    if (allocated === null) return;

    // Clear the table
    $('#allocated-table tbody').empty();
    for (i = 0; i < allocated.length; i++)
    {
        var row = '';
        row += "<tr>";
        row += "<td>" + allocated[i].registrationID + "</td>";
        row += "<td>" + allocated[i].fullName + "</td>";
        if (allocated[i].registrationTypeID === 1521 ||
            allocated[i].registrationTypeID === 1522 ||
            allocated[i].registrationTypeID === 1523 ||
            allocated[i].registrationTypeID === 1524) {
            row +="<td>";
            row += '    <select class="change-rego-select" id="ChangeRego_' + allocated[i].registrationID + '">';
            if (allocated[i].registrationTypeID === 1521){
                row += '        <option value="1521" selected>Adult PS16</option>';
            } else {
                row += '        <option value="1521">Adult PS16</option>';
            }
            if (allocated[i].registrationTypeID === 1522){
                row += '        <option value="1522" selected>Youth PS16</option>';
            } else {
                row += '        <option value="1522">Youth PS16</option>';
            }        
            if (allocated[i].registrationTypeID === 1523){
                row += '        <option value="1523" selected>Child (Primary) PS16</option>';
            } else {
                row += '        <option value="1523">Child (Primary) PS16</option>';
            }
            if (allocated[i].registrationTypeID === 1524){
                row += '        <option value="1524" selected>Child (Early Childhood) PS16</option>';
            } else {
                row += '        <option value="1524">Child (Early Childhood) PS16</option>';
            }
            row += '    </select>';
            row += "</td>";
        }
        else
        {
            row += "<td>" + allocated[i].registrationType + "</td>";
        }
        row += "<td>" + moment(allocated[i].registrationDate).format("DD/MM/YYYY hh:mm:ss A") + "</td>";
        row += "<td>";
        if (!allocated[i].attended)
        {
            row += '    <button href="#"';
            row += '        class="btn secondary btn-primary dropdown-toggle" data-toggle="dropdown">';
            row += '        Actions';
            row += '    </button>';
            row += '        <ul id="AllocatedDropDown_' + allocated[i].registrationID + '"';
            row += '           class= "dropdown-menu dropdown-menu-right" style="border: 1px solid #555">';

            row += '            <li id="RemoveButton_' + allocated[i].registrationID + '" class="unallocate-rego-btn dropdown-item"><a href="#">Remove</a></li>';
            row += '        </ul>';
        }

        row += '</td>';
        row += '</tr>';
        $('#allocated-table tbody').append(row);
    }
    if (allocatedKids != null) {
        
        for (i = 0; i < allocatedKids.length; i++) {
            var row = '';
            row += "<tr>";
            row += "<td>" + allocatedKids[i].registrationID + "</td>";
            row += "<td>" + allocatedKids[i].fullName + "</td>";
            row += "<td>" + allocatedKids[i].registrationType + "</td>";
            row += "<td>" + moment(allocatedKids[i].registrationDate).format("DD/MM/YYYY hh:mm:ss A") + "</td>";
            row += "<td>";
            if (!allocatedKids[i].attended) {
                row += '    <button href="#"';
                row += '        class="btn secondary btn-primary dropdown-toggle" data-toggle="dropdown">';
                row += '        Actions';
                row += '    </button>';
                row += '        <ul id="AllocatedDropDown_' + allocatedKids[i].registrationID + '"';
                row += '           class= "dropdown-menu dropdown-menu-right" style="border: 1px solid #555">';
                row += '            <li id="RemoveButton_' + allocatedKids[i].registrationID + '" class="unallocate-rego-btn dropdown-item"><a href="#">Remove</a></li>';
                row += '            <li id="ChildEnrolment_' + allocatedKids.registrationID + '" class="unallocate-rego-btn dropdown-item"><a href="/Form/ChildEnrolment?rego_id=' + allocatedKids.registrationID + '">Child Enrolment</a></li><div class="dropdown-divider" style="border-bottom: 1px solid rgb(85, 85, 85);"></div>';
                row += '        </ul>';
            }

            row += '</td>';
            row += '</tr>';
            $('#allocated-table tbody').append(row);
        }
    }
    $('#allocated-table').trigger('footable_initialized');
    $('#allocated-table').trigger('footable_resize');
    $('#allocated-table').trigger('footable_redraw');
}

function updateUnallocatedRows(unallocated, curRegos)
{
    // Only update if there are changes
    if (unallocated === null && curRegos === null) return;

    // Clear the table
    $('#unallocated-table tbody').empty();

    for (i = 0; i < unallocated.length; i++)
    {
        var row = '';
        row += "<tr>";
        row += "<td>" + unallocated[i].registrationType + "</td>";
        row += "<td>" + unallocated[i].quantity + "</td>";
        row += "<td>" + unallocated[i].allocated + "</td>";
        row += "<td>" + unallocated[i].unallocated + "</td>";
        row += "<td>" + unallocated[i].pendingInvitations + "</td>";
        row += "<td>";
        if ((unallocated[i].unallocated - unallocated[i].pendingInvitations) > 0)
        {
            // changed to allow multiple allocation
            /*if (curRegos.length > 0)
            {
                row += '<a id="Allocate_' + unallocated[i].registrationTypeID + '" class="allocate-rego-btn btn btn-primary secondary" href="#">Allocate</a>';                
            }
            else
            {
                

            }*/

            row += '<button href="#" class="btn secondary btn-primary dropdown-toggle" data-toggle="dropdown">';
            row += 'Allocate';
            row += '</button>';
            row += '    <ul id="UnallocatedDropDown_' + unallocated[i].registrationTypeID + '"';
            row += '    class="dropdown-menu dropdown-menu-right" style="border: 1px solid #555;">';
            row += '        <li id="AllocateSelf_' + unallocated[i].registrationTypeID + '" class="allocate-self-btn dropdown-item"><a href="#">Self</a></li><div class="dropdown-divider" style="border-bottom: 1px solid rgb(85, 85, 85);"></div>';
            row += '        <li id="Allocate_' + unallocated[i].registrationTypeID + '" class="allocate-rego-btn dropdown-item"><a href="#">Others</a></li>';
            row += '    </ul>';
        }
        row += "</td>";
        row += "</tr>";
        $('#unallocated-table tbody').append(row);
    }

    $('#unallocated-table').trigger('footable_initialized');
    $('#unallocated-table').trigger('footable_resize');
    $('#unallocated-table').trigger('footable_redraw');
}

function updateInvitationRows(invites)
{
    // Only update if there are changes
    if (invites === null) return;

    // Clear the table
    $('#invitations-table tbody').empty();

    for (i = 0; i < invites.length; i++) {
        var row = '';
        row += "<tr>";
        row += "<td>" + invites[i].recipientEmail + "</td>";
        row += "<td>" + invites[i].registrationType + "</td>";
        row += "<td>" + moment(invites[i].expiry).format("DD/MM/YYYY") + "</td>";
        row += "<td>";
        row += '    <a id="RevokeInvite_' + invites[i].invitationID + '" class="revoke-invitation-btn secondary btn btn-primary" href="#">Revoke</a>';
        row += "</td>";
        row += "</tr>";
        $('#invitations-table tbody').append(row);
    }

    $('#invitations-table').trigger('footable_initialized');
    $('#invitations-table').trigger('footable_resize');
    $('#invitations-table').trigger('footable_redraw');
}

function ActionButtonAssign(groupleader_id, venue_id) {
    $("#DonateBtn").attr("onclick", "DonateConfirmation(" + groupleader_id + "," + venue_id + ")");
    $("#CreditBtn").attr("onclick", "CreditConfirmation(" + groupleader_id + "," + venue_id + ")");
}


function DonateConfirmation(groupleader_id, venue_id) {
    if (venue_id == 156) {
        $("#donate_info").hide();
        $("#donate_body").show();

        var footer = '';
        footer += '<button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">No</button>';
        footer += '<button type="button" class="btn btn-primary" id="DonateBtn_' + groupleader_id + '" onclick="DonateRegistration(' + groupleader_id + ',' + venue_id + ')">Yes</button>';
        $("#donateConfirmationFooter").html(footer);
        $("#donateConfirmation").modal('show');
    } else if (venue_id == 157) {
        $("#donate_info").hide();
        $("#donate_body").show();

        var footer = '';
        footer += '<button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">No</button>';
        footer += '<button type="button" class="btn btn-primary" id="DonateBtn_' + groupleader_id + '" onclick="DonateRegistrationBNE(' + groupleader_id + ',' + venue_id + ')">Yes</button>';
        $("#donateConfirmationFooter").html(footer);
        $("#donateConfirmation").modal('show');
    }
}

function CreditConfirmation(groupleader_id, venue_id) {
    if (venue_id == 156) {
        $("#credit_info").hide();
        $("#credit_body").show();

        var footer = '';
        footer += '<button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Cancel</button>';
        footer += '<button type="button" class="btn btn-primary" id="CreditBtn_' + groupleader_id + '" onclick="CreditRegistration(' + groupleader_id + ',' + venue_id + ')">Confirm</button>';
        $("#creditConfirmationFooter").html(footer);
        $("#creditConfirmation").modal('show');
    } else if (venue_id == 157) {
        $("#credit_info").hide();
        $("#credit_body").show();

        var footer = '';
        footer += '<button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Cancel</button>';
        footer += '<button type="button" class="btn btn-primary" id="CreditBtn_' + groupleader_id + '" onclick="CreditRegistrationBNE(' + groupleader_id + ',' + venue_id + ')">Confirm</button>';
        $("#creditConfirmationFooter").html(footer);
        $("#creditConfirmation").modal('show');
    }
}

function DonateRegistration(groupleader_id, venue_id) {
    
    $("#DonateBtn_" + groupleader_id).hide();
    $("#donate_info").show();
    $("#loading_donate").show();

    var data = new Object();
    data.GroupLeader_ID = groupleader_id;
    data.Venue_ID = venue_id;
    data = JSON.stringify(data);

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Events/DonateRegistration',
        data: data,
        dataType: 'json',
        success: function (json) {
            $("#donate_info").removeClass('alert-warning');
            $("#donate_info").addClass('alert-success');
            $("#donate_info").html("<strong>Thank you for donating all your registrations!</strong>");
            $("#donate_body").hide();
            $("#loading_donate").hide();
            $("#donateConfirmationFooter").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');

            $("#actionModal").modal('hide');
            $("#actionBtn_" + venue_id).hide();
        }
    });
}

function CreditRegistration(groupleader_id, venue_id) {

    $("#CreditBtn_" + groupleader_id).hide();
    $("#credit_info").show();
    $("#loading_credit").show();

    var data = new Object();
    data.GroupLeader_ID = groupleader_id;
    data.Venue_ID = venue_id;
    data = JSON.stringify(data);

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Events/CreditRegistration',
        data: data,
        dataType: 'json',
        success: function (json) {
            $("#credit_info").removeClass('alert-warning');
            $("#credit_info").addClass('alert-success');
            $("#credit_info").html("<strong>Thank you for crediting your registrations. An email with your voucher code has been sent to you!</strong>");
            $("#credit_body").hide();
            $("#loading_credit").hide();
            $("#creditConfirmationFooter").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');

            $("#actionModal").modal('hide');
            $("#actionBtn_" + venue_id).hide();
        }
    });
}

function ActionButtonAssignBNE(groupleader_id, venue_id) {
    $("#DonateBtnBNE").attr("onclick", "DonateConfirmation(" + groupleader_id + "," + venue_id + ")");
    $("#CreditBtnBNE").attr("onclick", "CreditConfirmation(" + groupleader_id + "," + venue_id + ")");
}

function DonateRegistrationBNE(groupleader_id, venue_id) {
    console.log('donate');
    $("#DonateBtnBNE_" + groupleader_id).hide();
    $("#donate_info").show();
    $("#loading_donate").show();

    var data = new Object();
    data.GroupLeader_ID = groupleader_id;
    data.Venue_ID = venue_id;
    data = JSON.stringify(data);

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Events/DonateRegistrationBNE',
        data: data,
        dataType: 'json',
        success: function (json) {
            $("#donate_info").removeClass('alert-warning');
            $("#donate_info").addClass('alert-success');
            $("#donate_info").html("<strong>Thank you for donating all your registrations!</strong>");
            $("#donate_body").hide();
            $("#loading_donate").hide();
            $("#donateConfirmationFooter").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');

            $("#actionModalBNE").modal('hide');
            $("#actionBtnBNE_" + venue_id).hide();
        }
    });
}

function CreditRegistrationBNE(groupleader_id, venue_id) {
    console.log('credit');
    $("#CreditBtnBNE_" + groupleader_id).hide();
    $("#credit_info").show();
    $("#loading_credit").show();

    var data = new Object();
    data.GroupLeader_ID = groupleader_id;
    data.Venue_ID = venue_id;
    data = JSON.stringify(data);

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: '/Events/CreditRegistrationBNE',
        data: data,
        dataType: 'json',
        success: function (json) {
            $("#credit_info").removeClass('alert-warning');
            $("#credit_info").addClass('alert-success');
            $("#credit_info").html("<strong>Thank you for crediting your registrations. An email with your voucher code has been sent to you!</strong>");
            $("#credit_body").hide();
            $("#loading_credit").hide();
            $("#creditConfirmationFooter").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');

            $("#actionModalBNE").modal('hide');
            $("#actionBtnBNE_" + venue_id).hide();
        }
    });
}