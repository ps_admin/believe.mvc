﻿$(document).ready(function () {
    $('.dob-field').datetimepicker({
        format: "Y-m-d",
        timepicker: false,
        maxDate: 0
    });
});