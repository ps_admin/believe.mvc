﻿$(document).ready(function () {

    //$('#myModal').modal('toggle');
});

$('#father_tab').click(function () {

    $('#send_invitation_modal').hide();
    $('#allocate_son').hide();
    $('#allocate_father').show();
});

$('#son_tab').click(function () {

    $('#allocate_father').hide();
    $('#send_invitation_modal').hide();
    $('#allocate_son').show();
});

$('#send_invitation').click(function () {

    var object = $('#send_invitation_modal');

    if (object.is(":visible")) {
        object.hide();
    } else {
        object.show();
    }
});

function selfAllocate() {
    $('#send_invitation_modal').hide();

    var hasRego = checkAllocation();

    if (!hasRego) {
        $.ajax({
            cache: false,
            type: "POST",
            async: true,
            url: "selfAllocate",
            data: {},
            success: function (results) {
                $('#yes_self').removeAttr('onclick');
                $('#yes_self').attr('disabled', 'true');

                $('#yes_self').hide();
                $('#no_others').hide();                

                $('#allocate_son').hide();                                                                                                               
                $('#allocate_father').hide();                

                if (results.unallocatedQty < 1) {
                    $('#yes_self').remove();
                    $('#no_others').remove();                    

                    $('#father_tab').remove();
                    $('#son_tab').remove();
                    $('#later').remove();

                    $('#allocate_son').remove();
                    $('#allocate_father').remove();    

                    $('#continue_next').show();
                }

                $('#continue_next').show();

                $('.alert.alert-success').text(results.message);
                $('.alert.alert-success').show();
            },
            error: function () {
                $('.alert.alert-warning').text('Allocation failed. Please contact conference@planetshakers.com to allocate.');
                $('.alert.alert-warning').show();
            }
        });
    } else {
        $('.alert.alert-warning').text('You already have an existing allocation.');
        $('.alert.alert-warning').show();
    }
}

function allocateSon() {
    var firstname = $('#son_firstname').val();
    var lastname = $('#son_lastname').val();
    var email = $('#son_email').val();

    if (firstname !== "" && lastname !== "") {

        $.ajax({
            cache: false,
            type: "POST",
            async: true,
            url: "allocateSon",
            data: { "firstname": firstname, "lastname": lastname, "email": email },
            success: function (results) {
                $('.alert.alert-success').show();                      

                if (results.unallocatedQty < 1) {
                    $('#father_tab').remove();
                    $('#son_tab').remove();
                    $('#no_others').remove();

                    $('#later').remove();

                    $('#allocate_son').remove();
                    $('#allocate_father').remove();    

                    $('#continue_next').show(); 
                }

                $('.alert.alert-success').text(results.message);
                $('.alert.alert-success').show();
            },
            error: function () {
                $('.alert.alert-warning').text('Something went wrong. Allocation failed.');
                $('.alert.alert-warning').show();
            }
        });
    } else {
        $('#allocate_son').show();
    }
}

function sendInvite() {
    var regtype_id = $("#registration_type_id").val();
    var group_id = $("#group_leader_id").val();
    var email = $("#invitation_email").val();

    var data = new Object();
    data.email = email;
    data.group_leader_id = group_id;
    data.registration_type_id = regtype_id;

    $.ajax({
        cache: false,
        type: "POST",
        async: true,
        url: "/Invitation/SendInvitation",
        data: data,
        dataType: 'json',
        success: function (results) {

            if (results.unallocatedQty < 1) {
                $('#father_tab').remove();
                $('#son_tab').remove();
                $('#no_others').remove();

                $('#later').remove();

                $('#allocate_son').remove();
                $('#allocate_father').remove();    

                $('#continue_next').show(); 
            }

            $('.alert.alert-success').text(results.message);
            $('.alert.alert-success').show();
        },
        error: function () {
            $('.alert.alert-warning').text("An error occurred.");
            $('.alert.alert-warning').show();
        }
    });
}

function displayAddress() {
    var country = $("#ddlCountry option:selected").val();
    $("#selectCountry").hide();
    console.log(country);

    if (country == 11) {
        $("#address_details").show();
    } else {
        $("#modalTitle").text("Self Allocation");
        $("modalContent2").show();
    }
}

function updateAddress() {
    var street = $("#confirm_street").val();
    var unit = $("#confirm_unit").val();
    var suburb = $("#confirm_suburb").val();
    var postcode = $("#confirm_postcode").val();
    var state_id = $("#confirm_state option:selected").val();

    var data = new Object();
    data.street = street;
    data.unit = unit;
    data.suburb = suburb;
    data.postcode = postcode;
    data.state_id = state_id;

    $.ajax({
        cache: false,
        type: "POST",
        async: true,
        url: "/Purchase/updateAddress",
        data: data,
        dataType: 'json',
        success: function (results) {
            $("#modalTitle").text("Self Allocation");
            $("#modalContent1").hide();
            $("#modalContent2").show();
        },
        error: function () {
            alert("Update failed. Please contact the registration team at info@believeglobal.org")
        }
    });
}

function updateMedical() {
    var name = $("#emergency_name").val();
    var mobile = $("#emergency_mobile").val();
    var rel = $("#emergency_rel").val();
    var info = $("#info").val();
    var allergies = $("#allergies").val();

    var data = new Object();
    data.name = name;
    data.mobile = mobile;
    data.rel = rel;
    data.info = info;
    data.allergies = allergies;

    console.log(data);
    $.ajax({
        cache: false,
        type: "POST",
        async: true,
        url: "/Purchase/updateMedical",
        data: data,
        dataType: 'json',
        success: function (results) {
            
            //$("#modalTitle").text("Self Allocation");
            //$("#modalContent1").hide();
            //$("#modalContent2").hide();
            $("#myModal").modal('hide');
        },
        error: function () {
            alert("Update failed. Please contact the registration team at info@believeglobal.org")
        }
    });
}


function closeModal() {
    $("#myModal").modal('hide');
}

function checkAllocation() {
    $.ajax({
        cache: false,
        async: true,
        url: "checkAllocation",
        data: {},
        success: function (check) {
            return check;
        },
        error: function (check) {
            return check;
        }
    });
}