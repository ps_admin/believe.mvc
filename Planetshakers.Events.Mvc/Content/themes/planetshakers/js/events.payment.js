﻿$(document).ready(function () {
    loadStripePayment();
});

function loadStripePayment() {

    var data = new Object();

    data = JSON.stringify(data);

    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        url: "/Payment/loadStripePayment",
        data: data,
        success: function (result) {
            console.log('stripe success');
            $('#stripe-payment').html(result);
        },
        error: function () {
            console.log("failed");
        }
    });
}

