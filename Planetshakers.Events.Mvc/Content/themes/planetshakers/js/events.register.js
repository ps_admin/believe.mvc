﻿$(document).ready(function () {
    $('.table').footable();

    $('.new-rego-header > a').click(function () {
        $('.current-rego').show('blind');
    });
    $('.current-rego-header > a').click(function () {
        $('.current-rego').hide('blind');
    });

    // This function expands the registration types table depending on which button is pressed.
    $('.reg-category-btn').click(function (event) {
        event.preventDefault();

        $('.reg-category-btn').not(this).addClass('secondary');
        $(this).toggleClass('secondary');

        // retrieve registration 
        var registration_group_id = $(this).attr('id').replace("_RegisterCategoryButton", "");
        $('.rego-groups').not('#' + registration_group_id + '_RegistrationGroup').fadeOut().promise().done(function () {
            $('#' + registration_group_id + '_RegistrationGroup').fadeToggle();
        });
    });

    $('#2925_Quantity').attr('disabled', true);
    $('#2930_Quantity').attr('disabled', true);

    if ($('#promo_code').val() === "") {
        $('#opt_in').prop('checked', false);
        $('#promo_code').hide();
        $('#voucher_value_section').hide();
        $('#applied_value_section').hide();
    } else {
        $('#opt_in').prop('checked', true);
        $('#promo_code').show();
        $('#voucher_value_section').show();
        $('#applied_value_section').show();
    }

    $('#promo_code').bind('change load', function (event) {
        var promo_code = $('#promo_code').val();

        var registration_group_id = $(this).parent().parent().parent().parent().attr('id').replace("_RegistrationGroup", "");

        calculateTotal(registration_group_id);;
    });

    

    // Function used to calculate total
    $('.reg-type-qty').bind('change load', function (event) {
        event.preventDefault();

        /* === Calculate Row Total === */
        // retrieve registration type id.
        var registration_type_id = $(this).attr('id').replace("_Quantity", "");
        var total = parseFloat($('#' + registration_type_id + '_Cost').text()) * $('#' + registration_type_id + '_Quantity').val();

        // Exception created for sponsorship registration
        if (registration_type_id == 'Sponsorship') {
            total = parseFloat($('#' + registration_type_id + '_Quantity').val());
            
            if (total > 0.00) {
                $('#' + registration_type_id + '_Total').val(total.toFixed(2));
                $('#' + registration_type_id + '_SubmitButton').removeClass('disabled').prop('disabled', false);
                return;
            }
        }

        // Using class instead of id to hack through a bug in footable where it duplicates the id when resized.
        $('.' + registration_type_id + '_Total').text(total.toFixed(2));

        /* === Calculate Group Total ===*/
        // retrieve registration group id
        var registration_group_id = $(this).parent().parent().parent().parent().parent().attr('id').replace("_RegistrationGroup", "");
        
        calculateTotal(registration_group_id);

        // check whether there are enough registration to enable/disable submit button 
        var total_reg = 0;
        $('.' + registration_group_id + '_RegQuantity').each(function () {
            total_reg += Number($(this).val());
        });

        // number of regos previously purchased
        var previous_purchase = Number($('#' + registration_group_id + '_QuantityPurchased').val());

        var min_reg = $('#' + registration_group_id + '_SubmitButton').attr('data-min');
        var max_reg = $('#' + registration_group_id + '_SubmitButton').attr('data-max');

        if ((total_reg >= min_reg && total_reg <= max_reg)
            || (total_reg > 0 && (previous_purchase + total_reg) >= min_reg && (previous_purchase + total_reg) <= max_reg)) {
            $('#' + registration_group_id + '_SubmitButton').removeClass('disabled').prop('disabled', false);
        } else {
            $('#' + registration_group_id + '_SubmitButton').addClass('disabled').prop('disabled', true);
        }

        if (registration_type_id == 2995 && total_reg % 10 !== 0) {
            alert("Quantity must be a multiple of 10.");
            $('#' + registration_group_id + "_SubmitButton").addClass('disabled').prop('disabled', true);
        }

        var registration_type = $(this).parent().prev().prev().text();

        var limit_reached = false;
        $('.' + registration_group_id + '_RegQuantity').each(function () {
            var max_regType = Number($(this).attr('data-max'));
 
            if (Number($(this).val()) > max_regType) {
                if (max_regType > 0) {
                    alert('Only ' + max_regType + ' registrations left for ' + registration_type);
                } else {
                    alert('No more registrations left for ' + registration_type);
                }
                $('#' + registration_group_id + "_SubmitButton").addClass('disabled').prop('disabled', true);
            } else {
                //$('#' + registration_group_id + '_SubmitButton').removeClass('disabled').prop('disabled', false);
            }
        });
        

    });
    // Expand first registration category
    if ($('.rego-categories > a')[0] != null) {
        $('.rego-categories > a')[0].click();
    }

    //$('#Sponsorship_SubmitButton').click(function () {
    //    event.preventDefault();
    //    var total = $('#Sponsorship_Total').val();

    //    var data = new Object();
    //    data.venue_id = $('#VenueId').val();
    //    data.Amount = total;
    //    data = JSON.stringify(data);

    //    var returnUrl = '';
    //    if ($('#returnurl').val() !== null) {
    //        returnUrl = '?returnUrl=' + $('#returnurl').val();
    //    }

    //    if (total < 1) {
    //        $("#sponsorshipAmountCheck").show();
    //    }
    //    else {
    //        $.ajax({
    //            type: 'POST',
    //            contentType: 'application/json; charset=utf-8',
    //            url: '/Events/SubmitSponsorship' + returnUrl,
    //            data: data,
    //            dataType: 'json',
    //            success: function (response) {
    //                if (response.isSuccess) {
    //                    window.location.href = response.value;
    //                } else {
    //                    alert("Failed! || " + response.value);
    //                }
    //            }
    //        });
    //    }

    //});

    $('.stripe-button-el').addClass('payment-btn');

    $('.payment-btn').on('click', function (event) {
        event.preventDefault;

        $.ajax({
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            url: '/Purchase/validateContact',
            success: function (value) {
                if (!value) {
                    alert("You have been logged out. Please login again.");
                    window.location.replace("https://register.planetshakers.com/Account/Login");
                }
            },
            error: function (value) {
                alert("An error occur while trying to take payment");
            }
        });
    });

});